<?php defined( 'BASEPATH' ) OR exit( 'No direct script access allowed' );

use Spipu\Html2Pdf\Html2Pdf;

class Sales extends CI_Controller
{
	/**
	 * Class constructor
	 */
	public function __construct()
	{
		parent::__construct();

		if ( ! $this->ion_auth->logged_in() ) redirect( 'login', 'refresh' );

		$this->load->model( "Sales_model", "sales" );
		$this->load->model( "Stock_model", "stock" );
		$this->load->model( "Clients_model", "clients" );
		$this->load->model( "Orders_model", "orders" );
		$this->load->model( "Pos_model", "pos" );
		$this->load->model( "Movements_model", "movements" );
		$this->load->model( "Print_orders_model", "print_orders" );
		$this->load->model( "Products_model", "products" );

		$this->data['tags']['controller_name'] = __( "Sales" );

		/**
		 * -----------------------------------------------------------------------------------------------
		 * BULK ACTIONS FOR SELECT IN LISTS
		 * -----------------------------------------------------------------------------------------------
		 */

		$bulk_options['-1'] = __( "Bulk actions" );
		if ( $this->uri->segment( 2 ) != 'trash' AND $this->permissions->can_trash() ) $bulk_options['trash'] = __( "Move to Trash" );
		if ( $this->uri->segment( 2 ) != 'trash' AND $this->permissions->can_edit() ) $bulk_options['mark_paid'] = __( "Mark Paid" );
		if ( $this->uri->segment( 2 ) != 'trash' AND $this->permissions->can_delete() ) $bulk_options['mark_unpaid'] = __( "Mark Unpaid" );

		if ( isset( $_GET['status'] ) )
		{
			if ( $this->uri->segment( 2 ) != 'trash' AND $this->permissions->can_edit() AND $_GET['status'] == "unbilled" ) $bulk_options['mark_billed'] = __( "Mark Billed" );
			if ( $this->uri->segment( 2 ) != 'trash' AND $this->permissions->can_edit() AND $_GET['status'] == "billed" ) $bulk_options['mark_unbilled'] = __( "Mark Unbilled" );			
		}

		if ( $this->uri->segment( 2 ) == 'trash' AND $this->permissions->can_restore() ) $bulk_options['restore'] = __( "Restore" );
		if ( $this->uri->segment( 2 ) == 'trash' AND $this->permissions->can_delete() ) $bulk_options['delete'] = __( "Delete" );
		
		$this->data['bulk_actions'] = array(
			'id'		=> 'bulk_actions',
			'name'		=> 'bulk_actions',
			'class'		=> 'form-control form-control-sm mb-1',
			'options'	=> $bulk_options,
		);

		$this->data['submit_bulk_actions'] = array(
			'id'			=> 'submit_bulk_actions',
			'name'			=> 'submit_bulk_actions',
			'class'			=> 'btn btn-secondary btn-sm btn-bulk mb-1',
			'value'			=> __( "Apply" ),
			'data-confirm'	=> __( "Are you sure you want to perform that action?\n\nIt may be definitive and unable to undo in the future." ),
		);

		/**
		 * -----------------------------------------------------------------------------------------------
		 * LIST TOOLS EXTENSION
		 * -----------------------------------------------------------------------------------------------
		 */
		
		$this->data['list_buttons_extension'] = array(
			'brief' => anchor(
				'sales/brief/%d',
				'<i class="fa fa-search"></i>',
				array(
					'title' => __( "Brief" ),
					'class' => 'brief btn btn-info',
				)
			),
			'mark_paid' => anchor(
				uri_string() . '?action=mark_paid&sale_id=%d&' . $this->elements->clear_query_string( array( 'action', 'sale_id' ), $this->input->server( 'QUERY_STRING' ) ),
				__( "Mark Paid" ),
				array( 
					'class' => 'dropdown-item' 
				)
			),
			'mark_unpaid' => anchor(
				uri_string() . '?action=mark_unpaid&sale_id=%d&' . $this->elements->clear_query_string( array( 'action', 'sale_id' ), $this->input->server( 'QUERY_STRING' ) ),
				__( "Mark Unpaid" ),
				array( 
					'class' => 'dropdown-item btn-confirm',
					'data-confirm' => __( "Are you sure you want to perform this action? The associated movement will be deleted permanently." ),
				)
			),
			'mark_billed' => anchor(
				uri_string() . '?action=mark_billed&sale_id=%d&' . $this->elements->clear_query_string( array( 'action', 'sale_id' ), $this->input->server( 'QUERY_STRING' ) ),
				__( "Mark Billed" ),
				array( 
					'class' => 'dropdown-item' 
				)
			),
			'mark_unbilled' => anchor(
				uri_string() . '?action=mark_unbilled&sale_id=%d&' . $this->elements->clear_query_string( array( 'action', 'sale_id' ), $this->input->server( 'QUERY_STRING' ) ),
				__( "Mark Unbilled" ),
				array( 
					'class' => 'dropdown-item',
					'data-confirm' => __( "Are you sure you want to perform this action?" ),
				)
			),
			'print' => anchor(
				'sales/bill/%d',
				__( "Print Bill" ),
				array( 
					'class' => 'dropdown-item',
					'target' => '_blank',
				)
			),
		);
	}

	/**
	 * Index redirecting to lists
	 * 
	 * @return void
	 */
	public function index()
	{
		redirect( 'sales/billed' );
	}

	/**
	 * Items billed list
	 * 
	 * @return output
	 */
	public function lists()
	{
		if ( $_POST )
		{
			if ( ! empty( $_POST['cb_ids'] ) )
			{
				switch ( $_POST['bulk_actions'] )
				{
					default: break;
					case 'trash': 
						if ( $this->permissions->can_trash() )
						{
							foreach ( $_POST['cb_ids'] AS $id )
							{
								if ( $this->sales->save( array( 'sale_id' => (int)$id, 'trash' => 1 ) ) )
								{
									// Send movements to trash
									if ( $this->sales->is_paid( (int)$id ) )
									{
										$movement = $this->movements->get( array(
											'where' => array(
												'transaction_type' => 'sales',
												'transaction_id' => (int)$id,
											)
										) );

										if ( ! empty( $movement[0] ) ) $this->movements->save( array( 'movement_id' => $movement[0]['movement_id'], 'trash' => 1 ) );
									}

									$this->logs->set_data( array( 'bulk_actions' => $_POST['bulk_actions'], 'cb_id' => $id ), "POST" )->set_result( TRUE )->register();
									$this->alerts->persist( TRUE )->alert( __( "Items were successfully trashed." ), __( "Items trashed" ), NULL, 'success' );
								}
								else
								{
									$this->logs->set_data( array( 'bulk_actions' => $_POST['bulk_actions'], 'cb_id' => $id ), "POST" )->set_result( FALSE )->register();
									$this->alerts->persist( TRUE )->alert( __( "There were some errors while trying to perform the task. Please try again." ), __( "Error" ), NULL, 'danger' );
								}
							}

							redirect( uri_string() . '?status=' . $_GET['status'] );
						}
						else
						{
							$this->alerts->alert( $this->assets->conf['no_permission']['message'], $this->assets->conf['no_permission']['title'], NULL, 'warning' );
						}
					break;
					case 'mark_billed' : 
						if ( $this->permissions->can_edit() )
						{
							foreach ( $_POST['cb_ids'] AS $id )
							{
								if ( $this->sales->save( array( 'sale_id' => (int)$id, 'billed' => 1 ) ) )
								{
									$this->logs->set_data( array( 'bulk_actions' => $_POST['bulk_actions'], 'cb_id' => $id ), "POST" )->set_result( TRUE )->register();
									$this->alerts->persist( TRUE )->alert( __( "Items were successfully marked billed." ), __( "Items edited" ), NULL, 'success' );
								}
								else
								{
									$this->logs->set_data( array( 'bulk_actions' => $_POST['bulk_actions'], 'cb_id' => $id ), "POST" )->set_result( FALSE )->register();
									$this->alerts->persist( TRUE )->alert( __( "There were some errors while trying to perform the task. Please try again." ), __( "Error" ), NULL, 'danger' );
								}
							}

							redirect( uri_string() . '?status=' . $_GET['status'] );
						}
						else
						{
							$this->alerts->alert( $this->assets->conf['no_permission']['message'], $this->assets->conf['no_permission']['title'], NULL, 'warning' );
						}
					break;
					case 'mark_unbilled' : 
						if ( $this->permissions->can_edit() )
						{
							foreach ( $_POST['cb_ids'] AS $id )
							{
								if ( $this->sales->save( array( 'sale_id' => (int)$id, 'billed' => 0 ) ) )
								{
									$this->logs->set_data( array( 'bulk_actions' => $_POST['bulk_actions'], 'cb_id' => $id ), "POST" )->set_result( TRUE )->register();
									$this->alerts->persist( TRUE )->alert( __( "Items were successfully marked unbilled." ), __( "Items edited" ), NULL, 'success' );
								}
								else
								{
									$this->logs->set_data( array( 'bulk_actions' => $_POST['bulk_actions'], 'cb_id' => $id ), "POST" )->set_result( FALSE )->register();
									$this->alerts->persist( TRUE )->alert( __( "There were some errors while trying to perform the task. Please try again." ), __( "Error" ), NULL, 'danger' );
								}
							}

							redirect( uri_string() . '?status=' . $_GET['status'] );
						}
						else
						{
							$this->alerts->alert( $this->assets->conf['no_permission']['message'], $this->assets->conf['no_permission']['title'], NULL, 'warning' );
						}
					break;
					case 'mark_paid' : 
						if ( $this->permissions->can_edit() )
						{
							foreach ( $_POST['cb_ids'] AS $id )
							{
								// Create associated movement
								$sale = $this->assets->fill_empty_vars( $this->sales->get( (int)$id ), 'sales' );

								if ( ! empty( $sale ) )
								{
									$new_movement = ( array(
										'date' 				=> ( $sale['type'] == "full" ) ? $sale['sale_date'] : date( 'Y-m-d' ),
										'type' 				=> 'income',
										'label'				=> ( $sale['type'] == "full" ) ? __( "Sale of products on full payment" ) : __( "Sale of products on credit payment" ),
										'book'				=> __( "Sales" ),
										'comments'			=> sprintf( __( "Client: %s\nBill number: %s" ), $sale['name'], $sale['bill_number'] ),
										'amount'			=> $sale['total_amount'],
										'transaction_type'	=> 'sales',
										'transaction_id'	=> $sale['sale_id'],
									) );

									if ( $new_movement_id = $this->movements->save( $new_movement ) )
									{
										$new_movement = $this->movements->get( $new_movement_id );
										$this->logs->set_data( array( 'bulk_actions' => $_POST['bulk_actions'], 'cb_id' => $id, 'movement' => $new_movement ), "POST" )->set_result( TRUE )->register();
										$this->alerts->persist( TRUE )->alert( __( "Items were successfully marked paid." ), __( "Items edited" ), NULL, 'success' );
									}
									else
									{
										$this->logs->set_data( array( 'bulk_actions' => $_POST['bulk_actions'], 'cb_id' => $id ), "POST" )->set_result( FALSE )->register();
										$this->alerts->persist( TRUE )->alert( __( "There were some errors while trying to perform the task. Please try again." ), __( "Error" ), NULL, 'danger' );
									}
								}
							}

							redirect( uri_string() . '?status=' . $_GET['status'] );
						}
						else
						{
							$this->alerts->alert( $this->assets->conf['no_permission']['message'], $this->assets->conf['no_permission']['title'], NULL, 'warning' );
						}
					break;
					case 'mark_unpaid' : 
						if ( $this->permissions->can_delete() )
						{
							foreach ( $_POST['cb_ids'] AS $id )
							{
								// Delete associated movement
								if ( $this->sales->is_paid( (int)$id ) )
								{
									$movement = $this->movements->get( array(
										'where' => array(
											'transaction_type' => 'sales',
											'transaction_id' => (int)$id,
										)
									) );

									if ( ! empty( $movement[0] ) )
									{
										if ( $this->movements->erase( $movement[0]['movement_id'] ) )
										{
											$this->logs->set_data( array( 'bulk_actions' => $_POST['bulk_actions'], 'cb_id' => $id ), "POST" )->set_result( TRUE )->register();
											$this->alerts->persist( TRUE )->alert( __( "Items were successfully marked unpaid." ), __( "Items deleted" ), NULL, 'success' );
										}
										else
										{
											$this->logs->set_data( array( 'bulk_actions' => $_POST['bulk_actions'], 'cb_id' => $id ), "POST" )->set_result( FALSE )->register();
											$this->alerts->persist( TRUE )->alert( __( "There were some errors while trying to perform the task. Please try again." ), __( "Error" ), NULL, 'danger' );
										}
									}
								}
							}

							redirect( uri_string() . '?status=' . $_GET['status'] );
						}
						else
						{
							$this->alerts->alert( $this->assets->conf['no_permission']['message'], $this->assets->conf['no_permission']['title'], NULL, 'warning' );
						}
					break;
				}
			}
		}

		if ( isset( $_GET['action'] ) AND isset( $_GET['sale_id'] ) )
		{
			if ( $_GET['action'] == "trash" AND $this->permissions->can_trash() )
			{
				if ( $this->sales->save( array( 'sale_id' => (int)$_GET['sale_id'], 'trash' => 1 ) ) )
				{
					// Send movements to trash
					if ( $this->sales->is_paid( (int)$_GET['sale_id'] ) )
					{
						$movement = $this->movements->get( array(
							'where' => array(
								'transaction_type' => 'sales',
								'transaction_id' => (int)$_GET['sale_id'],
							)
						) );

						if ( ! empty( $movement[0] ) ) $this->movements->save( array( 'movement_id' => $movement[0]['movement_id'], 'trash' => 1 ) );
					}

					$this->logs->set_data( $_GET, "GET" )->set_result( TRUE )->register();
					$this->alerts->alert( __( "Item was successfully trashed." ), __( "Item trashed" ), NULL, 'success' );
				}
				else
				{
					$this->logs->set_data( $_GET, "GET" )->set_result( FALSE )->register();
					$this->alerts->alert( __( "There were some errors while trying to perform the task. Please try again." ), __( "Error" ), NULL, 'danger' );
				}
			}
			else if ( $_GET['action'] == "mark_billed" AND $this->permissions->can_edit() )
			{
				if ( $this->sales->save( array( 'sale_id' => (int)$_GET['sale_id'], 'billed' => 1 ) ) )
				{
					$this->logs->set_data( $_GET, "GET" )->set_result( TRUE )->register();
					$this->alerts->alert( __( "Item was successfully marked billed." ), __( "Item edited" ), NULL, 'success' );
				}
				else
				{
					$this->logs->set_data( $_GET, "GET" )->set_result( FALSE )->register();
					$this->alerts->alert( __( "There were some errors while trying to perform the task. Please try again." ), __( "Error" ), NULL, 'danger' );
				}
			}
			else if ( $_GET['action'] == "mark_unbilled" AND $this->permissions->can_edit() )
			{
				if ( $this->sales->save( array( 'sale_id' => (int)$_GET['sale_id'], 'billed' => 0 ) ) )
				{
					$this->logs->set_data( $_GET, "GET" )->set_result( TRUE )->register();
					$this->alerts->alert( __( "Item was successfully marked unbilled." ), __( "Item edited" ), NULL, 'success' );
				}
				else
				{
					$this->logs->set_data( $_GET, "GET" )->set_result( FALSE )->register();
					$this->alerts->alert( __( "There were some errors while trying to perform the task. Please try again." ), __( "Error" ), NULL, 'danger' );
				}
			}
			else if ( $_GET['action'] == "mark_paid" AND $this->permissions->can_edit() )
			{
				// Create associated movement
				$sale = $this->assets->fill_empty_vars( $this->sales->get( (int)$_GET['sale_id'] ), 'sales' );

				if ( ! empty( $sale ) )
				{
					$new_movement = ( array(
						'date' 				=> ( $sale['type'] == "full" ) ? $sale['sale_date'] : date( 'Y-m-d' ),
						'type' 				=> 'income',
						'label'				=> ( $sale['type'] == "full" ) ? __( "Sale of products on full payment" ) : __( "Sale of products on credit payment" ),
						'book'				=> __( "Sales" ),
						'comments'			=> sprintf( __( "Client: %s\nBill number: %s" ), $sale['name'], $sale['bill_number'] ),
						'amount'			=> $sale['total_amount'],
						'transaction_type'	=> 'sales',
						'transaction_id'	=> $sale['sale_id'],
					) );

					if ( $new_movement_id = $this->movements->save( $new_movement ) )
					{
						$new_movement = $this->movements->get( $new_movement_id );
						$this->logs->set_data( array_merge( $_GET, array( 'movement' => $new_movement ) ), "GET" )->set_result( TRUE )->register();
						$this->alerts->alert( __( "Item was successfully marked unbilled." ), __( "Item edited" ), NULL, 'success' );
					}
					else
					{
						$this->logs->set_data( $_GET, "GET" )->set_result( FALSE )->register();
						$this->alerts->alert( __( "There were some errors while trying to perform the task. Please try again." ), __( "Error" ), NULL, 'danger' );
					}
				}
			}
			else if ( $_GET['action'] == "mark_unpaid" AND $this->permissions->can_delete() )
			{
				// Delete associated movement
				if ( $this->sales->is_paid( (int)$_GET['sale_id'] ) )
				{
					$movement = $this->movements->get( array(
						'where' => array(
							'transaction_type' => 'sales',
							'transaction_id' => (int)$_GET['sale_id'],
						)
					) );

					if ( ! empty( $movement[0] ) )
					{
						if ( $this->movements->erase( $movement[0]['movement_id'] ) )
						{
							$this->logs->set_data( $_GET, "GET" )->set_result( TRUE )->register();
							$this->alerts->alert( __( "Item was successfully marked unpaid." ), __( "Item deleted" ), NULL, 'success' );
						}
						else
						{
							$this->logs->set_data( $_GET, "GET" )->set_result( FALSE )->register();
							$this->alerts->alert( __( "There were some errors while trying to perform the task. Please try again." ), __( "Error" ), NULL, 'danger' );
						}
					}
				}
			}
			else
			{
				$this->alerts->alert( $this->assets->conf['no_permission']['message'], $this->assets->conf['no_permission']['title'], NULL, 'warning' );
			}
		}

		// Order
		if ( $this->input->get( 'order_by' ) AND $this->input->get( 'direction' ) )
		{
			$query_args['order_by'][$this->input->get( 'order_by' )] = $this->input->get( 'direction' );
		}
		else
		{
			$query_args['order_by']['sale_id'] = 'DESC';
		}

		// Status definition
		switch ( $_GET['status'] )
		{
			default: 
				$this->alerts->persist( TRUE )->alert( __( "A wrong status for sales were given." ), __( "Wrong status given" ), NULL, 'warning' );
				redirect( '/' );
				break;
			case 'billed': 
				$query_args['where']['billed'] = 1; 
				$this->data['tags']['page_title'] = __( "Billed Sales" );
				break;
			case 'unbilled': 
				$query_args['where']['billed'] = 0; 
				$this->data['tags']['page_title'] = __( "Unbilled Sales" );
				break;
			case 'unpaid': 
				$this->data['tags']['page_title'] = __( "Billed Unpaid Sales" );
				$query_args['where']['billed'] = 1; 
				$query_args['where']["NOT EXISTS ( SELECT * FROM " . $this->db->dbprefix( $this->assets->conf['tables']['movements'] ) . " WHERE transaction_type = 'sales' AND transaction_id = " . $this->db->dbprefix( $this->assets->conf['tables']['sales'] ) . ".sale_id )"] = NULL;
				break;
			case 'paid': 
				$this->data['tags']['page_title'] = __( "Billed Paid Sales" );
				$query_args['where']['billed'] = 1; 
				$query_args['where']["NOT EXISTS ( SELECT * FROM " . $this->db->dbprefix( $this->assets->conf['tables']['movements'] ) . " WHERE transaction_type = 'sales' AND transaction_id = " . $this->db->dbprefix( $this->assets->conf['tables']['sales'] ) . ".sale_id )"] = NULL;
				break;
		}

		$query_args['where']['trash'] = 0;
		$query_args['limit'] = $this->assets->conf['items_per_page'];
		$query_args['offset'] = $this->uri->segment( 3 );
		$this->data['sales'] = $this->sales->get( $query_args );
		$this->data['pagination'] = $this->templates->create_pagination( $this->assets->conf['tables']['sales'], $query_args );

		$this->templates->reverse_pagination( $this->data['sales'] );
		
		$this->assets->load_script( 'lists.min.js' );
		
		$this->templates->view( 'sales/lists', $this->data );
	}

	/**
	 * Items trash
	 * 
	 * @return output
	 */
	public function trash()
	{
		$this->permissions->block_not_admin();
		
		if ( $_POST )
		{
			if ( ! empty( $_POST['cb_ids'] ) )
			{
				switch ( $_POST['bulk_actions'] )
				{
					default: break;
					case 'restore': 
						if ( $this->permissions->can_restore() )
						{
							foreach ( $_POST['cb_ids'] AS $id )
							{
								if ( $this->sales->save( array( 'sale_id' => $id, 'trash' => 0 ) ) )
								{
									// Restore the movement
									$movement = $this->movements->get( array(
										'where' => array(
											'transaction_type' => 'sales',
											'transaction_id' => (int)$id,
										)
									) );

									if ( ! empty( $movement[0] ) ) $this->movements->save( array( 'movement_id' => $movement[0]['movement_id'], 'trash' => 0 ) );

									$this->logs->set_data( array( 'bulk_actions' => $_POST['bulk_actions'], 'cb_id' => $id ), "POST" )->set_result( TRUE )->register();
									$this->alerts->persist( TRUE )->alert( __( "Items were successfully restored." ), __( "Items restored" ), NULL, 'success' );
								}
								else
								{
									$this->logs->set_data( array( 'bulk_actions' => $_POST['bulk_actions'], 'cb_id' => $id ), "POST" )->set_result( FALSE )->register();
									$this->alerts->persist( TRUE )->alert( __( "There were some errors while trying to perform the task. Please try again." ), __( "Error" ), NULL, 'danger' );
								}
							}

							redirect( uri_string() );
						}
						else
						{
							$this->alerts->alert( $this->assets->conf['no_permission']['message'], $this->assets->conf['no_permission']['title'], NULL, 'warning' );
						}
					break;
					case 'delete':
						if ( $this->permissions->can_delete() )
						{
							foreach ( $_POST['cb_ids'] AS $id )
							{
								// Prior to deletion, let's restore the stock
								$this->stock->restore_stock_from_sale( $id );

								if ( $this->sales->erase( array( 'sale_id' => $id ) ) )
								{
									// Delete the movement
									$movement = $this->movements->get( array(
										'where' => array(
											'transaction_type' => 'sales',
											'transaction_id' => (int)$id,
										)
									) );

									if ( ! empty( $movement[0] ) ) $this->movements->erase( $movement[0]['movement_id'] );

									$this->logs->set_data( array( 'bulk_actions' => $_POST['bulk_actions'], 'cb_id' => $id ), "POST" )->set_result( TRUE )->register();
									$this->alerts->persist( TRUE )->alert( __( "Items were successfully deleted." ), __( "Items deleted" ), NULL, 'success' );
								}
								else
								{
									$this->logs->set_data( array( 'bulk_actions' => $_POST['bulk_actions'], 'cb_id' => $id ), "POST" )->set_result( FALSE )->register();
									$this->alerts->persist( TRUE )->alert( __( "There were some errors while trying to perform the task. Please try again." ), __( "Error" ), NULL, 'danger' );
								}
							}

							redirect( uri_string() );
						}
						else
						{
							$this->alerts->alert( $this->assets->conf['no_permission']['message'], $this->assets->conf['no_permission']['title'], NULL, 'warning' );
						}
					break;
				} 
			}
		}

		if ( isset( $_GET['action'] ) AND isset( $_GET['sale_id'] ) )
		{
			if ( $_GET['action'] == "restore" AND $this->permissions->can_restore() )
			{
				if ( $this->sales->save( array( 'sale_id' => (int)$_GET['sale_id'], 'trash' => 0 ) ) )
				{
					// Restore the movement
					$movement = $this->movements->get( array(
						'where' => array(
							'transaction_type' => 'sales',
							'transaction_id' => (int)$_GET['sale_id'],
						)
					) );

					if ( ! empty( $movement[0] ) ) $this->movements->save( array( 'movement_id' => $movement[0]['movement_id'], 'trash' => '0' ) );

					$this->logs->set_data( $_GET, "GET" )->set_result( TRUE )->register();
					$this->alerts->alert( __( "Item successfully restored." ), __( "Item restored" ), NULL, 'success' );
				}
				else
				{
					$this->logs->set_data( $_GET, "GET" )->set_result( FALSE )->register();
					$this->alerts->alert( __( "There were some errors while trying to perform the task. Please try again." ), __( "Error" ), NULL, 'danger' );				
				}
			}
			else if ( $_GET['action'] == "delete" AND $this->permissions->can_delete() )
			{
				// Prior to deletion, let's restore the stock
				$this->stock->restore_stock_from_sale( (int)$_GET['sale_id'] );

				if ( $this->sales->erase( (int)$_GET['sale_id'] ) )
				{
					// Erase the movement
					$movement = $this->movements->get( array(
						'where' => array(
							'transaction_type' => 'sales',
							'transaction_id' => (int)$_GET['sale_id'],
						)
					) );

					if ( ! empty( $movement[0] ) ) $this->movements->erase( $movement[0]['movement_id'] );

					$this->logs->set_data( $_GET, "GET" )->set_result( TRUE )->register();
					$this->alerts->alert( __( "Item successfully deleted." ), __( "Item deleted" ), NULL, 'success' );
				}
				else
				{
					$this->logs->set_data( $_GET, "GET" )->set_result( FALSE )->register();
					$this->alerts->alert( __( "There were some errors while trying to perform the task. Please try again." ), __( "Error" ), NULL, 'danger' );				
				}
			}
			else
			{
				$this->alerts->alert( $this->assets->conf['no_permission']['message'], $this->assets->conf['no_permission']['title'], NULL, 'warning' );
			}
		}

		// Order
		if ( $this->input->get( 'order_by' ) AND $this->input->get( 'direction' ) )
		{
			$query_args['order_by'][$this->input->get( 'order_by' )] = $this->input->get( 'direction' );
		}
		else
		{
			$query_args['order_by']['sale_id'] = 'DESC';
		}

		$query_args['where']['trash'] = 1;
		$query_args['limit'] = $this->assets->conf['items_per_page'];
		$query_args['offset'] = $this->uri->segment( 3 );
		$this->data['sales'] = $this->sales->get( $query_args );
		$this->data['pagination'] = $this->templates->create_pagination( $this->assets->conf['tables']['sales'], $query_args );

		$this->templates->reverse_pagination( $this->data['sales'] );
		
		$this->assets->load_script( 'lists.min.js' );
		
		$this->data['tags']['page_title'] = __( "Sales Trash" );
		$this->templates->view( 'sales/lists', $this->data );
	}

	/**
	 * Items combine unbilled page
	 * 
	 * @return output
	 */
	public function combine()
	{
		redirect( "/" );
	}

	/**
	 * Add sale page
	 *
	 * @return output
	 */
	public function add()
	{
		if ( $this->input->post() )
		{
			if ( ! $this->permissions->can_add() )
			{
				$this->alerts->persist( TRUE )->alert( $this->assets->conf['no_permission']['message'], $this->assets->conf['no_permission']['title'], NULL, 'warning' );
				redirect( uri_string() );
			}

			$this->form_validation->set_rules( 'pos_id', 					__( "POS" ),			'trim|required' );
			$this->form_validation->set_rules( 'sale_date', 				__( "Sale date" ),		'trim|required' );
			$this->form_validation->set_rules( 'type', 						__( "Type" ),			'trim|required' );
			$this->form_validation->set_rules( 'bill_number', 				__( "Bill number" ),	'trim|required' );
			$this->form_validation->set_rules( 'name', 						__( "Name" ),			'trim|required' );
			$this->form_validation->set_rules( 'meta_vars[payment_method]', __( "Payment Method" ),	'required' );
			$this->form_validation->set_rules( 'total_amount', 				__( "Amount" ), 		( ( $this->assets->conf['decimals'] > 0 ) ? 'decimal' : 'integer' ) . '|required|greater_than[0]' );

			if ( $this->input->post( 'type' ) == "credit" ) $this->form_validation->set_rules( 'expiration_date', __( "Due date" ), 'trim|required' );

			if ( $this->input->post( 'meta_vars[payment_method]' ) == "debit" ) $this->form_validation->set_rules( 'meta_vars[debit_card_company]', __( "Debit Card" ), 'trim|required' );
			if ( $this->input->post( 'meta_vars[payment_method]' ) == "credit" ) $this->form_validation->set_rules( 'meta_vars[credit_card_company]', __( "Credit Card" ), 'trim|required' );

			if ( $this->form_validation->run() )
			{
				if ( $sale_id = $this->sales->save( $this->input->post( NULL, TRUE ) ) )
				{
					// Subtract from stock
					$this->stock->subtract_stock_from_sale( $sale_id );

					$new_sale = $this->assets->fill_empty_vars( $this->sales->get( $sale_id, 'sales' ) );

					// Associate movement in case it is not a credit sale
					if ( $this->input->post( 'type' ) == "full" )
					{
						$new_movement_id = $this->movements->save( array(
							'date' 				=> $new_sale['sale_date'],
							'type' 				=> 'income',
							'label'				=> __( "Sale of products on full payment" ),
							'book'				=> __( "Sales" ),
							'comments'			=> sprintf( __( "Client: %s\nBill number: %s" ), $new_sale['name'], $new_sale['bill_number'] ),
							'amount'			=> $new_sale['total_amount'],
							'transaction_type'	=> 'sales',
							'transaction_id'	=> $sale_id,
						) );						

						$_POST['movement'] = $this->movements->get( $new_movement_id );
					}

					$_POST['sale_id'] = $sale_id;
					$this->logs->set_data( $this->input->post(), "POST" )->set_result( TRUE )->register();
					$this->alerts->persist( TRUE )->alert( __( "Item successfully added to the database." ), __( "Sale created" ), NULL, 'success' );

					redirect( "sales/edit/" . $sale_id );
				}
				else
				{
					$this->logs->set_data( $this->input->post(), "POST" )->set_result( FALSE )->register();
					$this->alerts->alert( __( "There were some errors while trying to perform the task. Please try again." ), __( "Sale discarded" ), NULL, 'danger' );
				}
			}
			else
			{
				$this->alerts->alert( __( "Errors appear below every field." ), __( "Item could not be stored into the database" ), NULL, 'danger' );
			}
		}

		$this->data['pos_id'] = array(
			'id'		=> 'pos_id',
			'name'		=> 'pos_id',
			'class'		=> 'form-control',
			'style'		=> 'width: 100%',
			'selected'	=> set_value( 'pos_id' ),
			'options'	=> $this->pos->build_dropdown(),
		);

		$this->data['sale_date'] = array(
			'type'		=> 'text',
			'id'		=> 'sale_date',
			'name'		=> 'sale_date',
			'class'		=> 'form-control input-date',
			'value'		=> set_value( 'sale_date', date( $this->assets->conf['date_format'] ) ),
		);

		$this->data['expiration_date'] = array(
			'type'		=> 'text',
			'id'		=> 'expiration_date',
			'name'		=> 'expiration_date',
			'class'		=> 'form-control input-date',
			'value'		=> set_value( 'expiration_date' ),
		);

		$this->data['seller_id'] = array(
			'id'		=> 'seller_id',
			'name'		=> 'seller_id',
			'class'		=> 'form-control',
			'style'		=> 'width: 100%',
			'selected'	=> set_value( 'seller_id', $this->ion_auth->user()->row()->id ),
			'options'	=> $this->ion_auth->build_dropdown(),
		);

		$this->data['total_amount'] = array(
			'type'		=> 'text',
			'id'		=> 'total_amount',
			'name'		=> 'total_amount',
			'class'		=> 'form-control form-control-lg input-currency',
			'readonly'	=> 'readonly',
			'value'		=> set_value( 'total_amount' ),
		);		

		// HIDDENS ----------------------------------------------
		
		$this->data['order_id'] = array(
			'type'		=> 'hidden',
			'id'		=> 'order_id',
			'name'		=> 'order_id',
			'value'		=> set_value( 'order_id' ),
		);

		$this->data['bill_number'] = array(
			'type'		=> 'hidden',
			'id'		=> 'bill_number',
			'name'		=> 'bill_number',
			'value'		=> set_value( 'bill_number' ),
		);

		$this->data['print_order'] = array(
			'type'		=> 'hidden',
			'id'		=> 'print_order',
			'name'		=> 'print_order',
			'value'		=> set_value( 'print_order' ),
		);

		$this->data['name'] = array(
			'type'		=> 'hidden',
			'id'		=> 'name',
			'name'		=> 'name',
			'value'		=> set_value( 'name' ),
		);

		$this->data['ruc'] = array(
			'type'		=> 'hidden',
			'id'		=> 'ruc',
			'name'		=> 'ruc',
			'value'		=> set_value( 'ruc' ),
		);

		$this->data['client_id'] = array(
			'type'		=> 'hidden',
			'id'		=> 'client_id',
			'name'		=> 'client_id',
			'value'		=> set_value( 'client_id' ),
		);

		$this->data['address'] = array(
			'type'		=> 'hidden',
			'id'		=> 'address',
			'name'		=> 'address',
			'value'		=> set_value( 'address' ),
		);

		$this->data['telephone'] = array(
			'type'		=> 'hidden',
			'id'		=> 'telephone',
			'name'		=> 'telephone',
			'value'		=> set_value( 'telephone' ),
		);

		// TAXES ---------------------------------------------

		foreach ( $this->assets->conf['bill_columns'] AS $key => $val )
		{
			$this->data['taxes_totals_' . $key] = array(
				'type'		=> 'hidden',
				'id'		=> 'taxes_totals_' . $key,
				'name'		=> 'taxes[totals][' . $key . ']',
				'value'		=> set_value( 'taxes[totals][' . $key . ']' ),
			);

			$this->data['taxes_liquidation_' . $key] = array(
				'type'		=> 'hidden',
				'id'		=> 'taxes_liquidation_' . $key,
				'name'		=> 'taxes[liquidation][' . $key . ']',
				'value'		=> set_value( 'taxes[liquidation][' . $key . ']' ),
			);
		}

		$this->data['taxes_liquidation_total'] = array(
			'type'		=> 'hidden',
			'id'		=> 'taxes_liquidation_total',
			'name'		=> 'taxes[liquidation][total]',
			'value'		=> set_value( 'taxes[liquidation][total]' ),
		);

		// META VARS -----------------------------------------

		$this->data['meta_vars_amount_given'] = array(
			'type'		=> 'text',
			'id'		=> 'meta_vars_amount_given',
			'name'		=> 'meta_vars[amount_given]',
			'class'		=> 'form-control form-control-lg input-currency',
			'value'		=> my_number_format( set_value( 'meta_vars[amount_given]' ) ),
		);

		$this->data['meta_vars_change'] = array(
			'type'		=> 'text',
			'id'		=> 'meta_vars_change',
			'name'		=> 'meta_vars[change]',
			'class'		=> 'form-control form-control-lg input-currency',
			'readonly'	=> 'readonly',
			'value'		=> my_number_format( set_value( 'meta_vars[change]' ) ),
		);

		$this->data['meta_sale_total'] = array(
			'type'		=> 'text',
			'id'		=> 'meta_sale_total',
			'class'		=> 'form-control form-control-lg input-currency',
			'readonly'	=> 'readonly',
			'value'		=> my_number_format( set_value( 'total_amount' ) ),
		);

		// MODAL FINISH SALE ---------------------------------

		$this->data['meta_amount_given_foreign'] = array(
			'type'		=> 'text',
			'id'		=> 'meta_amount_given_foreign',
			'class'		=> 'form-control input-currency-foreign',
		);

		$this->data['meta_sale_total_foreign'] = array(
			'type'		=> 'text',
			'id'		=> 'meta_sale_total_foreign',
			'class'		=> 'form-control input-currency-foreign',
			'readonly'	=> 'readonly',
		);

		$this->data['meta_change_foreign'] = array(
			'type'		=> 'text',
			'id'		=> 'meta_change_foreign',
			'class'		=> 'form-control input-currency-foreign',
			'readonly'	=> 'readonly',
		);

		// MODAL SEARCH CLIENT -------------------------------

		$this->data['modal_client_id'] = array(
			'id'			=> 'modal_client_id',
			'name'			=> 'modal_client_id',
			'class'			=> 'form-control',
			'style'			=> 'width: 100%',
			'placeholder'	=> __( "Start typing a surname, lastname, RUC or ID..." ),
			'options'		=> array(),
		);

		$this->data['modal_client_first_name'] = array(
			'type'		=> 'text',
			'id'		=> 'modal_client_first_name',
			'name'		=> 'modal_client_first_name',
			'class'		=> 'form-control',
		);

		$this->data['modal_client_last_name'] = array(
			'type'		=> 'text',
			'id'		=> 'modal_client_last_name',
			'name'		=> 'modal_client_last_name',
			'class'		=> 'form-control',
		);

		$this->data['modal_client_ruc'] = array(
			'type'		=> 'text',
			'id'		=> 'modal_client_ruc',
			'name'		=> 'modal_client_ruc',
			'class'		=> 'form-control input-ruc',
		);

		$this->data['modal_client_cellphone'] = array(
			'type'		=> 'text',
			'id'		=> 'modal_client_cellphone',
			'name'		=> 'modal_client_cellphone',
			'class'		=> 'form-control input-phone',
		);

		$this->data['modal_client_email'] = array(
			'type'		=> 'text',
			'id'		=> 'modal_client_email',
			'name'		=> 'modal_client_email',
			'class'		=> 'form-control',
		);

		// PRODUCT SELECTION ---------------------------------

		$this->data['product_selection'] = array(
			'id'			=> 'product_selection',
			'name'			=> 'product_selection',
			'class'			=> 'form-control',
			'style'			=> 'width: 100%',
			'placeholder'	=> __( "Search by product name or SKU..." ),
			'options'		=> array(),
		);

		// ADDING FROM AN ORDER OVERRIDE ---------------------

		if ( $this->uri->segment( 3 ) )
		{
			$order = $this->orders->get( $this->uri->segment( 3 ) );

			if ( ! empty( $order ) )
			{
				$client = $this->clients->get( $order['client_id'] );

				// Set the seller according to the 'owner' of the order
				if ( ! empty( $order['seller_id'] ) ) $this->data['seller_id']['selected'] = $order['seller_id'];

				$this->data['order_id']['value'] 	= $order['order_id'];
				$this->data['client_id']['value'] 	= $order['client_id'];

				$this->data['name']['value'] 		= ( ! empty( $client ) ) ? $this->clients->name( $client, 'full' ) : NULL;
				$this->data['address']['value'] 	= ( ! empty( $client ) ) ? $this->clients->address( $client, 'bill' ) : NULL;
				$this->data['ruc']['value'] 		= ( ! empty( $client['ruc'] ) ) ? $client['ruc'] : NULL;
				$this->data['telephone']['value'] 	= ( ! empty( $client['telephone'] ) ) ? $client['telephone'] : NULL;

				// Order meta preprocessing
				$order_meta = $this->orders->meta_get( $order['order_id'] );

				// Check if there's remaining, otherwise erase the item
				foreach ( $order_meta AS $k => $om ) if ( $om['quantity_remaining'] <= 0 ) unset( $order_meta[$k] );

				if ( ! empty( $order_meta ) ) $this->data['order_meta'] = $order_meta;

				if ( empty( $order_meta ) )
				{
					$update_order = array(
						'order_id' => $order['order_id'],
						'sold' => 1,
					);

					// Update the order status to sold
					if ( $this->orders->save( $update_order ) )
					{
						$this->logs->set_data( $update_order, "POST" )->set_result( TRUE )->register();
					}
					else
					{
						$this->logs->set_data( $update_order, "POST" )->set_result( FALSE )->register();
					}

					$this->alerts->persist( TRUE )->alert( __( "The order was completed already and does not have any more items to be sold." ), __( "Order Already Complete" ), NULL, 'warning' );
					redirect( 'sales/add' );
				}
			}
			else
			{
				$this->alerts->alert( __( "The order you are trying to include doesn't exists on the database." ), __( "Order Inexistant" ), NULL, 'warning' );
			}
		}

		// Pickadate
		$this->assets->load_style( base_url( 'assets/bower_components/pickadate/lib/compressed/themes/default.css' ) );
		$this->assets->load_style( base_url( 'assets/bower_components/pickadate/lib/compressed/themes/default.date.css' ) );
		$this->assets->load_script( base_url( 'assets/bower_components/pickadate/lib/compressed/picker.js' ) );
		$this->assets->load_script( base_url( 'assets/bower_components/pickadate/lib/compressed/picker.date.js' ) );	
		if ( $this->assets->conf['language'] !== "en_US" ) $this->assets->load_script( base_url( 'assets/bower_components/pickadate/lib/translations/' . $this->assets->conf['language'] . '.js' ) );

		// Inputmask
		$this->assets->load_script( base_url( 'assets/bower_components/inputmask/dist/jquery.inputmask.min.js' ) );

		// Select2
		$this->assets->load_style( site_url( 'assets/bower_components/select2/dist/css/select2.min.css' ) );
		$this->assets->load_script( site_url( 'assets/bower_components/select2/dist/js/select2.full.min.js' ) );
		$this->assets->load_script( site_url( 'assets/bower_components/select2/dist/js/i18n/' . substr( $this->assets->conf['language'], 0, 2 ) . '.js' ) );

		$this->assets->load_script( 'lists.min.js' );
		$this->assets->load_script( 'forms.min.js' );
		$this->assets->load_script( 'forms-sales.min.js' );
		
		$this->data['tags']['page_title'] = __( "Add Sale" );
		$this->templates->view( 'sales/form', $this->data );
	}

	/**
	 * Edit sale page
	 *
	 * @return output
	 */
	public function edit( $sale_id )
	{
		if ( $this->input->post() )
		{
			if ( ! $this->permissions->can_add() )
			{
				$this->alerts->persist( TRUE )->alert( $this->assets->conf['no_permission']['message'], $this->assets->conf['no_permission']['title'], NULL, 'warning' );
				redirect( uri_string() );
			}

			$this->form_validation->set_rules( 'pos_id', 					__( "POS" ),			'trim|required' );
			$this->form_validation->set_rules( 'sale_date', 				__( "Sale date" ),		'trim|required' );
			$this->form_validation->set_rules( 'type', 						__( "Type" ),			'trim|required' );
			$this->form_validation->set_rules( 'bill_number', 				__( "Bill number" ),	'trim|required' );
			$this->form_validation->set_rules( 'name', 						__( "Name" ),			'trim|required' );
			$this->form_validation->set_rules( 'meta_vars[payment_method]', __( "Payment Method" ),	'required' );
			$this->form_validation->set_rules( 'total_amount', 				__( "Amount" ), 		( ( $this->assets->conf['decimals'] > 0 ) ? 'decimal' : 'integer' ) . '|required|greater_than[0]' );

			if ( $this->input->post( 'type' ) == "credit" ) $this->form_validation->set_rules( 'expiration_date', __( "Due date" ), 'trim|required' );

			if ( $this->input->post( 'meta_vars[payment_method]' ) == "debit" ) $this->form_validation->set_rules( 'meta_vars[debit_card_company]', __( "Debit Card" ), 'trim|required' );
			if ( $this->input->post( 'meta_vars[payment_method]' ) == "credit" ) $this->form_validation->set_rules( 'meta_vars[credit_card_company]', __( "Credit Card" ), 'trim|required' );

			if ( $this->form_validation->run() )
			{
				if ( $this->sales->save( $this->input->post( NULL, TRUE ) ) )
				{
					// Restore to stock and then subtract again, to equalize levels
					$this->stock->restore_stock_from_sale( $sale_id );
					$this->stock->subtract_stock_from_sale( $sale_id );

					// Associate movement in case it exists, or in case it is not a credit sale
					$updated_sale 		= $this->assets->fill_empty_vars( $this->sales->get( $sale_id, 'sales' ) );
					$existing_movement 	= $this->movements->get( array(
						'where' => array(
							'transaction_id' 	=> $sale_id,
							'transaction_type' 	=> 'sales',
						)
					) );
										
					if ( ! empty( $existing_movement[0] ) )
					{
						$this->movements->save( array(
							'movement_id'		=> $existing_movement[0]['movement_id'],
							'date' 				=> $updated_sale['sale_date'],
							'comments'			=> sprintf( __( "Client: %s\nBill number: %s" ), $updated_sale['name'], $updated_sale['bill_number'] ),
							'amount'			=> $updated_sale['total_amount'],
						) );

						$_POST['movement'] = $this->movements->get( $existing_movement[0]['movement_id'] );
					}
					else if ( $this->input->post( "type" ) == "full" AND empty( $existing_movement ) )
					{
						$new_movement_id = $this->movements->save( array(
							'date' 				=> $updated_sale['sale_date'],
							'type' 				=> 'income',
							'label'				=> __( "Sale of products on full payment" ),
							'book'				=> __( "Sales" ),
							'comments'			=> sprintf( __( "Client: %s\nBill number: %s" ), $updated_sale['name'], $updated_sale['bill_number'] ),
							'amount'			=> $updated_sale['total_amount'],
							'transaction_type'	=> 'sales',
							'transaction_id'	=> $sale_id,
						) );

						$_POST['movement'] = $this->movements->get( $new_movement_id );
					}

					$this->logs->set_data( $this->input->post(), "POST" )->set_result( TRUE )->register();
					$this->alerts->persist( TRUE )->alert( __( "Item successfully updated into the database." ), __( "Sale updated" ), NULL, 'success' );

					redirect( "sales/edit/" . $sale_id );
				}
				else
				{
					$this->logs->set_data( $this->input->post(), "POST" )->set_result( FALSE )->register();
					$this->alerts->alert( __( "There were some errors while trying to perform the task. Please try again." ), __( "Sale not updated" ), NULL, 'danger' );
				}
			}
			else
			{
				$this->alerts->alert( __( "Errors appear below every field." ), __( "Item could not be stored into the database" ), NULL, 'danger' );
			}
		}

		$this->data['sale'] = $this->sales->get( $sale_id );

		if ( ! empty( $this->data['sale'] ) )
		{
			$this->data['pos_id'] = array(
				'id'		=> 'pos_id',
				'name'		=> 'pos_id',
				'class'		=> 'form-control',
				'style'		=> 'width: 100%',
				'selected'	=> $this->data['sale']['pos_id'],
				'options'	=> $this->pos->build_dropdown(),
			);

			$this->data['sale_date'] = array(
				'type'		=> 'text',
				'id'		=> 'sale_date',
				'name'		=> 'sale_date',
				'class'		=> 'form-control input-date',
				'value'		=> date( $this->assets->conf['date_format'], strtotime( $this->data['sale']['sale_date'] ) ),
			);

			$this->data['expiration_date'] = array(
				'type'		=> 'text',
				'id'		=> 'expiration_date',
				'name'		=> 'expiration_date',
				'class'		=> 'form-control input-date',
				'value'		=> $this->data['sale']['expiration_date'],
			);

			$this->data['seller_id'] = array(
				'id'		=> 'seller_id',
				'name'		=> 'seller_id',
				'class'		=> 'form-control',
				'style'		=> 'width: 100%',
				'selected'	=> $this->data['sale']['seller_id'],
				'options'	=> $this->ion_auth->build_dropdown(),
			);

			$this->data['total_amount'] = array(
				'type'		=> 'text',
				'id'		=> 'total_amount',
				'name'		=> 'total_amount',
				'class'		=> 'form-control form-control-lg input-currency',
				'readonly'	=> 'readonly',
				'value'		=> my_number_format( $this->data['sale']['total_amount'] ),
			);

			// HIDDENS ----------------------------------------------
			
			$this->data['sale_id'] = array(
				'type'		=> 'hidden',
				'id'		=> 'sale_id',
				'name'		=> 'sale_id',
				'value'		=> $this->data['sale']['sale_id'],
			);

			$this->data['order_id'] = array(
				'type'		=> 'hidden',
				'id'		=> 'order_id',
				'name'		=> 'order_id',
				'value'		=> $this->data['sale']['order_id'],
			);

			$this->data['bill_number'] = array(
				'type'		=> 'hidden',
				'id'		=> 'bill_number',
				'name'		=> 'bill_number',
				'value'		=> $this->data['sale']['bill_number'],
			);

			$this->data['print_order'] = array(
				'type'		=> 'hidden',
				'id'		=> 'print_order',
				'name'		=> 'print_order',
				'value'		=> $this->data['sale']['print_order'],
			);

			$this->data['name'] = array(
				'type'		=> 'hidden',
				'id'		=> 'name',
				'name'		=> 'name',
				'value'		=> $this->data['sale']['name'],
			);

			$this->data['ruc'] = array(
				'type'		=> 'hidden',
				'id'		=> 'ruc',
				'name'		=> 'ruc',
				'value'		=> $this->data['sale']['ruc'],
			);

			$this->data['client_id'] = array(
				'type'		=> 'hidden',
				'id'		=> 'client_id',
				'name'		=> 'client_id',
				'value'		=> $this->data['sale']['client_id'],
			);

			$this->data['address'] = array(
				'type'		=> 'hidden',
				'id'		=> 'address',
				'name'		=> 'address',
				'value'		=> $this->data['sale']['address'],
			);

			$this->data['telephone'] = array(
				'type'		=> 'hidden',
				'id'		=> 'telephone',
				'name'		=> 'telephone',
				'value'		=> $this->data['sale']['telephone'],
			);

			// TAXES ---------------------------------------------

			foreach ( $this->assets->conf['bill_columns'] AS $key => $val )
			{
				$this->data['taxes_totals_' . $key] = array(
					'type'		=> 'hidden',
					'id'		=> 'taxes_totals_' . $key,
					'name'		=> 'taxes[totals][' . $key . ']',
					'value'		=> $this->data['sale']['taxes']['totals'][$key],
				);

				$this->data['taxes_liquidation_' . $key] = array(
					'type'		=> 'hidden',
					'id'		=> 'taxes_liquidation_' . $key,
					'name'		=> 'taxes[liquidation][' . $key . ']',
					'value'		=> $this->data['sale']['taxes']['liquidation'][$key],
				);
			}

			$this->data['taxes_liquidation_total'] = array(
				'type'		=> 'hidden',
				'id'		=> 'taxes_liquidation_total',
				'name'		=> 'taxes[liquidation][total]',
				'value'		=> $this->data['sale']['taxes']['liquidation']['total'],
			);

			// META VARS -----------------------------------------

			$this->data['meta_vars_amount_given'] = array(
				'type'					=> 'text',
				'id'					=> 'meta_vars_amount_given',
				'name'					=> 'meta_vars[amount_given]',
				'class'					=> 'form-control form-control-lg input-currency',
				'value'					=> $this->data['sale']['meta_vars']['amount_given'],
				'data-original-value'	=> $this->data['sale']['meta_vars']['amount_given'],
			);

			$this->data['meta_vars_change'] = array(
				'type'		=> 'text',
				'id'		=> 'meta_vars_change',
				'name'		=> 'meta_vars[change]',
				'class'		=> 'form-control form-control-lg input-currency',
				'readonly'	=> 'readonly',
				'value'		=> $this->data['sale']['meta_vars']['change'],
			);

			$this->data['meta_sale_total'] = array(
				'type'		=> 'text',
				'id'		=> 'meta_sale_total',
				'class'		=> 'form-control form-control-lg input-currency',
				'readonly'	=> 'readonly',
				'value'		=> $this->data['sale']['total_amount'],
			);

			// MODAL FINISH SALE ---------------------------------

			$this->data['meta_amount_given_foreign'] = array(
				'type'		=> 'text',
				'id'		=> 'meta_amount_given_foreign',
				'class'		=> 'form-control input-currency-foreign',
			);

			$this->data['meta_sale_total_foreign'] = array(
				'type'		=> 'text',
				'id'		=> 'meta_sale_total_foreign',
				'class'		=> 'form-control input-currency-foreign',
				'readonly'	=> 'readonly',
			);

			$this->data['meta_change_foreign'] = array(
				'type'		=> 'text',
				'id'		=> 'meta_change_foreign',
				'class'		=> 'form-control input-currency-foreign',
				'readonly'	=> 'readonly',
			);

			// MODAL SEARCH CLIENT -------------------------------

			$this->data['modal_client_id'] = array(
				'id'			=> 'modal_client_id',
				'name'			=> 'modal_client_id',
				'class'			=> 'form-control',
				'style'			=> 'width: 100%',
				'placeholder'	=> __( "Start typing a surname, lastname, RUC or ID..." ),
				'options'		=> array(),
			);

			$this->data['modal_client_first_name'] = array(
				'type'		=> 'text',
				'id'		=> 'modal_client_first_name',
				'name'		=> 'modal_client_first_name',
				'class'		=> 'form-control',
			);

			$this->data['modal_client_last_name'] = array(
				'type'		=> 'text',
				'id'		=> 'modal_client_last_name',
				'name'		=> 'modal_client_last_name',
				'class'		=> 'form-control',
			);

			$this->data['modal_client_ruc'] = array(
				'type'		=> 'text',
				'id'		=> 'modal_client_ruc',
				'name'		=> 'modal_client_ruc',
				'class'		=> 'form-control input-ruc',
			);

			$this->data['modal_client_cellphone'] = array(
				'type'		=> 'text',
				'id'		=> 'modal_client_cellphone',
				'name'		=> 'modal_client_cellphone',
				'class'		=> 'form-control input-phone',
			);

			$this->data['modal_client_email'] = array(
				'type'		=> 'text',
				'id'		=> 'modal_client_email',
				'name'		=> 'modal_client_email',
				'class'		=> 'form-control',
			);

			// PRODUCT SELECTION ---------------------------------

			$this->data['product_selection'] = array(
				'id'			=> 'product_selection',
				'name'			=> 'product_selection',
				'class'			=> 'form-control',
				'style'			=> 'width: 100%',
				'placeholder'	=> __( "Search by product name or SKU..." ),
				'options'		=> array(),
			);

			// Pickadate
			$this->assets->load_style( base_url( 'assets/bower_components/pickadate/lib/compressed/themes/default.css' ) );
			$this->assets->load_style( base_url( 'assets/bower_components/pickadate/lib/compressed/themes/default.date.css' ) );
			$this->assets->load_script( base_url( 'assets/bower_components/pickadate/lib/compressed/picker.js' ) );
			$this->assets->load_script( base_url( 'assets/bower_components/pickadate/lib/compressed/picker.date.js' ) );	
			if ( $this->assets->conf['language'] !== "en_US" ) $this->assets->load_script( base_url( 'assets/bower_components/pickadate/lib/translations/' . $this->assets->conf['language'] . '.js' ) );

			// Inputmask
			$this->assets->load_script( base_url( 'assets/bower_components/inputmask/dist/jquery.inputmask.min.js' ) );

			// Select2
			$this->assets->load_style( site_url( 'assets/bower_components/select2/dist/css/select2.min.css' ) );
			$this->assets->load_script( site_url( 'assets/bower_components/select2/dist/js/select2.full.min.js' ) );
			$this->assets->load_script( site_url( 'assets/bower_components/select2/dist/js/i18n/' . substr( $this->assets->conf['language'], 0, 2 ) . '.js' ) );

			$this->assets->load_script( 'lists.min.js' );
			$this->assets->load_script( 'forms.min.js' );
			$this->assets->load_script( 'forms-sales.min.js' );
			
			$this->data['tags']['page_title'] = __( "Add Sale" );
			$this->templates->view( 'sales/form', $this->data );
		}
		else
		{
			$this->alerts->persist( TRUE )->alert( __( "The sale is not stored into the database." ), __( "Sale does not exist" ), NULL, 'danger' );
			redirect( 'sales/lists?status=unbilled' );
		}
	}

	/**
	 * Item brief page
	 * 
	 * @param  int $sale_id The sale id
	 * @return output
	 */
	public function brief( $sale_id )
	{
		$this->data['sale'] = $this->sales->get( $sale_id );

		if ( ! empty( $this->data['sale'] ) )
		{
			$this->data['sale'] = $this->assets->fill_empty_vars( $this->data['sale'] );
			$this->data['meta'] = $this->sales->meta_get( $this->data['sale']['sale_id'] );
			$this->data['pos'] = $this->pos->get( $this->data['sale']['pos_id'] );
			$this->data['tags']['page_title'] = __( "Sale Brief" );
			$this->templates->view( 'sales/brief', $this->data );
		}
		else
		{
			$this->alerts->persist( TRUE )->alert( __( "The sale is not stored into the database." ), __( "Sale does not exist" ), NULL, 'danger' );
			redirect( 'sales/lists' );
		}
	}

	/**
	 * Show the bill in pdf to print
	 * 
	 * @param  int $sale_id The Sale ID
	 * @return output
	 */
	public function bill( $sale_id )
	{
		$this->data['sale'] = $this->sales->get( $sale_id );

		$update = array( 
			'sale_id' => $sale_id,
			'billed' => 1,
		);

		if ( ! empty( $this->data['sale'] ) )
		{
			$this->data['print_order'] 	= $this->print_orders->get_by_number( $this->data['sale']['print_order'] );
			$this->data['pos'] 			= $this->pos->get( $this->data['sale']['pos_id'] );
			$this->data['metas'] 		= $this->sales->meta_get( $sale_id );

			if ( ! empty( $this->data['print_order'] ) AND ! empty( $this->data['metas'] ) )
			{
				$this->sales->save( $update );
				$this->logs->set_data( $update, "GET" )->set_result( TRUE )->register();

				$html2pdf = new Html2Pdf( 'P', 'A4' );
				$html2pdf->writeHTML( $this->load->view( 'sales/bill', $this->data, true ) );
				$html2pdf->output();
			}
			else if ( empty( $this->data['print_order'] ) )
			{
				$update['key'] = 'print_order_missing';
				$this->logs->set_data( $update, "GET" )->set_result( FALSE )->register();
				$this->alerts->persist( TRUE )->alert( __( "The print order is not stored into the database." ), __( "Print order does not exist" ), NULL, 'danger' );
				redirect( 'sales/lists' );
			}
			else if ( empty( $this->data['metas'] ) )
			{
				$update['key'] = 'metas_missing';
				$this->logs->set_data( $update, "GET" )->set_result( FALSE )->register();
				$this->alerts->persist( TRUE )->alert( __( "The meta data is not stored into the database." ), __( "Meta data does not exist" ), NULL, 'danger' );
				redirect( 'sales/lists' );
			}
		}
		else
		{
			$update['key'] = 'sale_missing';
			$this->logs->set_data( $update, "GET" )->set_result( FALSE )->register();
			$this->alerts->persist( TRUE )->alert( __( "The sale is not stored into the database." ), __( "Sale does not exist" ), NULL, 'danger' );
			redirect( 'sales/lists' );
		}
	}
}