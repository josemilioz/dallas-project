<?php defined( 'BASEPATH' ) OR exit( 'No direct script access allowed' );

class Pos extends CI_Controller
{
	/**
	 * Class constructor
	 */
	public function __construct()
	{
		parent::__construct();

		if ( ! $this->ion_auth->logged_in() ) redirect( 'login', 'refresh' );

		$this->load->model( "Pos_model", "pos" );
		$this->load->model( "Categories_model", "categories" );

		$this->data['tags']['controller_name'] = __( "Points of Sale" );

		/**
		 * -----------------------------------------------------------------------------------------------
		 * BULK ACTIONS FOR SELECT IN LISTS
		 * -----------------------------------------------------------------------------------------------
		 */

		$bulk_options['-1'] = __( "Bulk actions" );
		if ( $this->uri->segment( 2 ) != 'trash' AND $this->permissions->can_trash() ) $bulk_options['trash'] = __( "Move to Trash" );
		if ( $this->uri->segment( 2 ) == 'trash' AND $this->permissions->can_restore() ) $bulk_options['restore'] = __( "Restore" );
		if ( $this->uri->segment( 2 ) == 'trash' AND $this->permissions->can_delete() ) $bulk_options['delete'] = __( "Delete" );
		
		$this->data['bulk_actions'] = array(
			'id'		=> 'bulk_actions',
			'name'		=> 'bulk_actions',
			'class'		=> 'form-control form-control-sm mb-1',
			'options'	=> $bulk_options,
		);

		$this->data['submit_bulk_actions'] = array(
			'id'			=> 'submit_bulk_actions',
			'name'			=> 'submit_bulk_actions',
			'class'			=> 'btn btn-secondary btn-sm btn-bulk mb-1',
			'value'			=> __( "Apply" ),
			'data-confirm'	=> __( "Are you sure you want to perform that action?\n\nIt may be definitive and unable to undo in the future." ),
		);

		/**
		 * -----------------------------------------------------------------------------------------------
		 * LIST TOOLS EXTENSION
		 * -----------------------------------------------------------------------------------------------
		 */
		
		$this->data['list_buttons_extension'] = array(
			'brief' => anchor(
				'pos/brief/%d',
				'<i class="fa fa-search"></i>',
				array(
					'title' => __( "Brief" ),
					'class' => 'brief btn btn-info',
				)
			),
		);
	}

	/**
	 * Index redirecting to lists
	 * 
	 * @return void
	 */
	public function index()
	{
		redirect( 'pos/lists' );
	}

	/**
	 * Items list
	 * 
	 * @return output
	 */
	public function lists()
	{
		if ( $_POST )
		{
			if ( ! empty( $_POST['cb_ids'] ) )
			{
				switch ( $_POST['bulk_actions'] )
				{
					default: break;
					case 'trash': 
						if ( $this->permissions->can_trash() )
						{
							foreach ( $_POST['cb_ids'] AS $id )
							{
								if ( $this->pos->save( array( 'pos_id' => (int)$id, 'trash' => 1 ) ) )
								{
									$this->logs->set_data( array( 'bulk_actions' => $_POST['bulk_actions'], 'cb_id' => $id ), "POST" )->set_result( TRUE )->register();
									$this->alerts->persist( TRUE )->alert( __( "Items were successfully trashed." ), __( "Items trashed" ), NULL, 'success' );
								}
								else
								{
									$this->logs->set_data( array( 'bulk_actions' => $_POST['bulk_actions'], 'cb_id' => $id ), "POST" )->set_result( FALSE )->register();
									$this->alerts->persist( TRUE )->alert( __( "There were some errors while trying to perform the task. Please try again." ), __( "Error" ), NULL, 'danger' );
								}
							}

							redirect( uri_string() );
						}
						else
						{
							$this->alerts->alert( $this->assets->conf['no_permission']['message'], $this->assets->conf['no_permission']['title'], NULL, 'warning' );
						}
					break;
				} 
			}
		}

		if ( isset( $_GET['action'] ) AND isset( $_GET['pos_id'] ) )
		{
			if ( $_GET['action'] == "trash" AND $this->permissions->can_trash() )
			{
				if ( $this->pos->save( array( 'pos_id' => (int)$_GET['pos_id'], 'trash' => 1 ) ) )
				{
					$this->logs->set_data( $_GET, "GET" )->set_result( TRUE )->register();
					$this->alerts->alert( __( "Item was successfully trashed." ), __( "Item trashed" ), NULL, 'success' );
				}
				else
				{
					$this->logs->set_data( $_GET, "GET" )->set_result( FALSE )->register();
					$this->alerts->alert( __( "There were some errors while trying to perform the task. Please try again." ), __( "Error" ), NULL, 'danger' );
				}
			}
			else
			{
				$this->alerts->alert( $this->assets->conf['no_permission']['message'], $this->assets->conf['no_permission']['title'], NULL, 'warning' );
			}
		}

		// Order
		if ( $this->input->get( 'order_by' ) AND $this->input->get( 'direction' ) )
		{
			$query_args['order_by'][$this->input->get( 'order_by' )] = $this->input->get( 'direction' );
		}
		else
		{
			$query_args['order_by']['pos_id'] = 'DESC';
		}

		$query_args['where']['trash'] = 0;
		$query_args['limit'] = $this->assets->conf['items_per_page'];
		$query_args['offset'] = $this->uri->segment( 3 );
		$this->data['pos'] = $this->pos->get( $query_args );
		$this->data['pagination'] = $this->templates->create_pagination( $this->assets->conf['tables']['pos'], $query_args );

		$this->templates->reverse_pagination( $this->data['pos'] );
		
		$this->assets->load_script( 'lists.min.js' );
		
		$this->data['tags']['page_title'] = __( "POS List" );
		$this->data['tags']['extra'] = anchor( 'pos/add', '<i class="fa fa-plus-circle"></i> ' . __( "Add POS" ), array( 'class' => 'btn btn-success btn-sm ml-2' ) );
		$this->templates->view( 'pos/lists', $this->data );
	}

	/**
	 * Items trash
	 * 
	 * @return output
	 */
	public function trash()
	{
		$this->permissions->block_not_admin();
		
		if ( $_POST )
		{
			if ( ! empty( $_POST['cb_ids'] ) )
			{
				switch ( $_POST['bulk_actions'] )
				{
					default: break;
					case 'restore': 
						if ( $this->permissions->can_restore() )
						{
							foreach ( $_POST['cb_ids'] AS $id )
							{
								if ( $this->pos->save( array( 'pos_id' => $id, 'trash' => 0 ) ) )
								{
									$this->logs->set_data( array( 'bulk_actions' => $_POST['bulk_actions'], 'cb_id' => $id ), "POST" )->set_result( TRUE )->register();
									$this->alerts->persist( TRUE )->alert( __( "Items were successfully restored." ), __( "Items restored" ), NULL, 'success' );
								}
								else
								{
									$this->logs->set_data( array( 'bulk_actions' => $_POST['bulk_actions'], 'cb_id' => $id ), "POST" )->set_result( FALSE )->register();
									$this->alerts->persist( TRUE )->alert( __( "There were some errors while trying to perform the task. Please try again." ), __( "Error" ), NULL, 'danger' );
								}
							}

							redirect( uri_string() );
						}
						else
						{
							$this->alerts->alert( $this->assets->conf['no_permission']['message'], $this->assets->conf['no_permission']['title'], NULL, 'warning' );
						}
					break;
					case 'delete':
						if ( $this->permissions->can_delete() )
						{
							foreach ( $_POST['cb_ids'] AS $id )
							{
								if ( $this->pos->erase( array( 'pos_id' => $id ) ) )
								{
									$this->logs->set_data( array( 'bulk_actions' => $_POST['bulk_actions'], 'cb_id' => $id ), "POST" )->set_result( TRUE )->register();
									$this->alerts->persist( TRUE )->alert( __( "Items were successfully deleted." ), __( "Items deleted" ), NULL, 'success' );
								}
								else
								{
									$this->logs->set_data( array( 'bulk_actions' => $_POST['bulk_actions'], 'cb_id' => $id ), "POST" )->set_result( FALSE )->register();
									$this->alerts->persist( TRUE )->alert( __( "There were some errors while trying to perform the task. Please try again." ), __( "Error" ), NULL, 'danger' );
								}
							}

							redirect( uri_string() );
						}
						else
						{
							$this->alerts->alert( $this->assets->conf['no_permission']['message'], $this->assets->conf['no_permission']['title'], NULL, 'warning' );
						}
					break;
				} 
			}
		}

		if ( isset( $_GET['action'] ) AND isset( $_GET['pos_id'] ) )
		{
			if ( $_GET['action'] == "restore" AND $this->permissions->can_restore() )
			{
				if ( $this->pos->save( array( 'pos_id' => (int)$_GET['pos_id'], 'trash' => 0 ) ) )
				{
					$this->logs->set_data( $_GET, "GET" )->set_result( TRUE )->register();
					$this->alerts->alert( __( "Item successfully restored." ), __( "Item restored" ), NULL, 'success' );
				}
				else
				{
					$this->logs->set_data( $_GET, "GET" )->set_result( FALSE )->register();
					$this->alerts->alert( __( "There were some errors while trying to perform the task. Please try again." ), __( "Error" ), NULL, 'danger' );				
				}
			}
			else if ( $_GET['action'] == "delete" AND $this->permissions->can_delete() )
			{
				if ( $this->pos->erase( (int)$_GET['pos_id'] ) )
				{
					$this->logs->set_data( $_GET, "GET" )->set_result( TRUE )->register();
					$this->alerts->alert( __( "Item successfully deleted." ), __( "Item deleted" ), NULL, 'success' );
				}
				else
				{
					$this->logs->set_data( $_GET, "GET" )->set_result( FALSE )->register();
					$this->alerts->alert( __( "There were some errors while trying to perform the task. Please try again." ), __( "Error" ), NULL, 'danger' );				
				}
			}
			else
			{
				$this->alerts->alert( $this->assets->conf['no_permission']['message'], $this->assets->conf['no_permission']['title'], NULL, 'warning' );
			}
		}

		// Order
		if ( $this->input->get( 'order_by' ) AND $this->input->get( 'direction' ) )
		{
			$query_args['order_by'][$this->input->get( 'order_by' )] = $this->input->get( 'direction' );
		}
		else
		{
			$query_args['order_by']['pos_id'] = 'DESC';
		}

		$query_args['where']['trash'] = 1;
		$query_args['limit'] = $this->assets->conf['items_per_page'];
		$query_args['offset'] = $this->uri->segment( 3 );
		$this->data['pos'] = $this->pos->get( $query_args );
		$this->data['pagination'] = $this->templates->create_pagination( $this->assets->conf['tables']['pos'], $query_args );

		$this->templates->reverse_pagination( $this->data['pos'] );
		
		$this->assets->load_script( 'lists.min.js' );
		
		$this->data['tags']['page_title'] = __( "POS Trash" );
		$this->templates->view( 'pos/lists', $this->data );
	}

	/**
	 * Item add page
	 * 
	 * @return output
	 */
	public function add()
	{
		if ( $this->input->post() )
		{
			if ( ! $this->permissions->can_add() )
			{
				$this->alerts->persist( TRUE )->alert( $this->assets->conf['no_permission']['message'], $this->assets->conf['no_permission']['title'], NULL, 'warning' );
				redirect( uri_string() );
			}

			$this->form_validation->set_rules( 'name', __( "Name" ), 'trim|required' );

			if ( $this->form_validation->run() )
			{
				if ( $pos_id = $this->pos->save( $this->input->post( NULL, TRUE ) ) )
				{
					$_POST['pos_id'] = $pos_id;
					$this->logs->set_data( $this->input->post(), "POST" )->set_result( TRUE )->register();
					$this->alerts->persist( TRUE )->alert( __( "Item successfully added to the database." ), __( "POS created" ), NULL, 'success' );

					redirect( "pos/edit/" . $pos_id );
				}
				else
				{
					$this->logs->set_data( $this->input->post(), "POST" )->set_result( FALSE )->register();
					$this->alerts->alert( __( "There were some errors while trying to perform the task. Please try again." ), __( "POS discarded" ), NULL, 'danger' );
				}
			}
			else
			{
				$this->alerts->alert( __( "Errors appear below every field." ), __( "Item could not be stored into the database" ), NULL, 'danger' );
			}
		}

		$this->data['name'] = array(
			'type'		=> 'text',
			'id'		=> 'name',
			'name'		=> 'name',
			'class'		=> 'form-control form-control-lg',
			'value'		=> set_value( 'name' ),
		);

		$this->data['printer'] = array(
			'type'		=> 'text',
			'id'		=> 'printer',
			'name'		=> 'printer',
			'class'		=> 'form-control form-control-lg',
			'value'		=> set_value( 'printer' ),
		);

		$this->data['assigned_ip'] = array(
			'type'		=> 'text',
			'id'		=> 'assigned_ip',
			'name'		=> 'assigned_ip',
			'class'		=> 'form-control input-ip-address',
			'value'		=> set_value( 'assigned_ip' ),
		);

		$this->data['user_id'] = array(
			'id'		=> 'user_id',
			'name'		=> 'user_id',
			'class'		=> 'form-control',
			'style'		=> 'width: 100%',
			'options'	=> $this->ion_auth->build_dropdown(),
		);

		$categories_list = $this->categories->build_dropdown();

		$this->data['categories_list'] = array(
			'id'		=> 'categories_list',
			'name'		=> 'categories_list[]',
			'class'		=> 'form-control categories-list-dropdown',
			'multiple'	=> true,
			'options'	=> $categories_list,
		);

		$this->data['categories_enabled'] = array(
			'id'		=> 'categories_enabled',
			'name'		=> 'categories_enabled[]',
			'class'		=> 'form-control categories-list-dropdown',
			'multiple'	=> true,
			'options'	=> array(),
		);

		// Inputmask
		$this->assets->load_script( base_url( 'assets/bower_components/inputmask/dist/jquery.inputmask.min.js' ) );

		// Select2
		$this->assets->load_style( site_url( 'assets/bower_components/select2/dist/css/select2.min.css' ) );
		$this->assets->load_script( site_url( 'assets/bower_components/select2/dist/js/select2.full.min.js' ) );
		$this->assets->load_script( site_url( 'assets/bower_components/select2/dist/js/i18n/' . substr( $this->assets->conf['language'], 0, 2 ) . '.js' ) );

		$this->assets->load_script( 'forms.min.js' );
		$this->assets->load_script( 'forms-pos.min.js' );

		$this->data['tags']['page_title'] = __( "Add POS" );
		$this->templates->view( 'pos/form', $this->data );
	}

	/**
	 * Item edit page
	 * 
	 * @param  int $pos_id
	 * @return output
	 */
	public function edit( $pos_id )
	{
		if ( $this->input->post() )
		{
			if ( ! $this->permissions->can_edit() )
			{
				$this->alerts->persist( TRUE )->alert( $this->assets->conf['no_permission']['message'], $this->assets->conf['no_permission']['title'], NULL, 'warning' );
				redirect( uri_string() );
			}

			$this->form_validation->set_rules( 'pos_id', __( "POS ID" ), 'integer|required' );
			$this->form_validation->set_rules( 'name', __( "Name" ), 'trim|required' );

			if ( $this->form_validation->run() )
			{
				if ( $this->pos->save( $this->input->post( NULL, TRUE ) ) )
				{
					$this->logs->set_data( $this->input->post(), "POST" )->set_result( TRUE )->register();
					$this->alerts->persist( TRUE )->alert( __( "Item was successfully updated into the database." ), __( "POS updated" ), NULL, 'success' );
					redirect( "pos/edit/" . $pos_id );
				}
				else
				{
					$this->logs->set_data( $this->input->post(), "POST" )->set_result( FALSE )->register();
					$this->alerts->alert( __( "There were some errors while trying to perform the task. Please try again." ), __( "POS not updated" ), NULL, 'danger' );
				}
			}
			else
			{
				$this->alerts->alert( __( "Errors appear below every field." ), __( "Item could not be stored into the database" ), NULL, 'danger' );
			}
		}

		$this->data['pos'] = $this->pos->get( $pos_id );

		if ( ! empty( $this->data['pos'] ) )
		{
			$this->data['pos_id'] = array(
				'type'		=> 'hidden',
				'id'		=> 'pos_id',
				'name'		=> 'pos_id',
				'value'		=> $this->data['pos']['pos_id'],
			);

			$this->data['name'] = array(
				'type'		=> 'text',
				'id'		=> 'name',
				'name'		=> 'name',
				'class'		=> 'form-control form-control-lg',
				'value'		=> $this->data['pos']['name'],
			);

			$this->data['printer'] = array(
				'type'		=> 'text',
				'id'		=> 'printer',
				'name'		=> 'printer',
				'class'		=> 'form-control form-control-lg',
				'value'		=> $this->data['pos']['printer'],
			);

			$this->data['assigned_ip'] = array(
				'type'		=> 'text',
				'id'		=> 'assigned_ip',
				'name'		=> 'assigned_ip',
				'class'		=> 'form-control input-ip-address',
				'value'		=> $this->data['pos']['assigned_ip'],
			);

			$this->data['user_id'] = array(
				'id'		=> 'user_id',
				'name'		=> 'user_id',
				'class'		=> 'form-control',
				'style'		=> 'width: 100%',
				'options'	=> $this->ion_auth->build_dropdown(),
			);

			$categories_list = $this->categories->build_dropdown();
			$categories_enabled = $this->pos->get_categories( $this->data['pos']['pos_id'] );

			$this->data['categories_list'] = array(
				'id'		=> 'categories_list',
				'name'		=> 'categories_list[]',
				'class'		=> 'form-control categories-list-dropdown',
				'multiple'	=> true,
				'options'	=> $this->pos->clear_active_categories( $categories_list, $categories_enabled ),
			);

			$this->data['categories_enabled'] = array(
				'id'		=> 'categories_enabled',
				'name'		=> 'categories_enabled[]',
				'class'		=> 'form-control categories-list-dropdown',
				'multiple'	=> true,
				'options'	=> $categories_enabled,
			);

			// Inputmask
			$this->assets->load_script( base_url( 'assets/bower_components/inputmask/dist/jquery.inputmask.min.js' ) );

			// Select2
			$this->assets->load_style( site_url( 'assets/bower_components/select2/dist/css/select2.min.css' ) );
			$this->assets->load_script( site_url( 'assets/bower_components/select2/dist/js/select2.full.min.js' ) );
			$this->assets->load_script( site_url( 'assets/bower_components/select2/dist/js/i18n/' . substr( $this->assets->conf['language'], 0, 2 ) . '.js' ) );

			$this->assets->load_script( 'forms.min.js' );
			$this->assets->load_script( 'forms-pos.min.js' );

			$this->data['tags']['page_title'] = __( "Edit POS" );
			$this->templates->view( 'pos/form', $this->data );
		}
		else
		{
			$this->alerts->persist( TRUE )->alert( __( "The POS is not stored into the database." ), __( "POS does not exist" ), NULL, 'danger' );
			redirect( 'pos/lists' );
		}
	}

	/**
	 * Item brief page
	 * 
	 * @param  int $pos_id
	 * @return output
	 */
	public function brief( $pos_id )
	{
		$this->data['pos'] = $this->pos->get( $pos_id );

		if ( ! empty( $this->data['pos'] ) )
		{
			$this->data['pos'] = $this->assets->fill_empty_vars( $this->data['pos'] );
			$this->data['users'] = $this->pos->users_get( $this->data['pos']['pos_id'] );
			$this->data['tags']['page_title'] = __( "POS Brief" );
			$this->templates->view( 'pos/brief', $this->data );
		}
		else
		{
			$this->alerts->persist( TRUE )->alert( __( "The POS is not stored into the database." ), __( "POS does not exist" ), NULL, 'danger' );
			redirect( 'pos/lists' );			
		}
	}

}