<?php defined( 'BASEPATH' ) OR exit( 'No direct script access allowed' );

class Clients extends CI_Controller 
{
	/**
	 * Class constructor
	 */
	public function __construct()
	{
		parent::__construct();

		if ( ! $this->ion_auth->logged_in() ) redirect( 'login', 'refresh' );

		$this->data['tags']['controller_name'] = __( "Clients" );

		$this->load->model( "Clients_model", "clients" );

		/**
		 * -----------------------------------------------------------------------------------------------
		 * BULK ACTIONS FOR SELECT IN LISTS
		 * -----------------------------------------------------------------------------------------------
		 */

		$bulk_options['-1'] = __( "Bulk actions" );
		if ( $this->uri->segment( 2 ) != 'trash' AND $this->permissions->can_trash() ) $bulk_options['trash'] = __( "Move to Trash" );
		if ( $this->uri->segment( 2 ) == 'trash' AND $this->permissions->can_restore() ) $bulk_options['restore'] = __( "Restore" );
		if ( $this->uri->segment( 2 ) == 'trash' AND $this->permissions->can_delete() ) $bulk_options['delete'] = __( "Delete" );
		
		$this->data['bulk_actions'] = array(
			'id'		=> 'bulk_actions',
			'name'		=> 'bulk_actions',
			'class'		=> 'form-control form-control-sm mb-1',
			'options'	=> $bulk_options,
		);

		$this->data['submit_bulk_actions'] = array(
			'id'			=> 'submit_bulk_actions',
			'name'			=> 'submit_bulk_actions',
			'class'			=> 'btn btn-secondary btn-sm btn-bulk mb-1',
			'value'			=> __( "Apply" ),
			'data-confirm'	=> __( "Are you sure you want to perform that action?\n\nIt may be definitive and unable to undo in the future." ),
		);

		/**
		 * -----------------------------------------------------------------------------------------------
		 * LIST TOOLS EXTENSION
		 * -----------------------------------------------------------------------------------------------
		 */
		
		$this->data['list_buttons_extension'] = array(
			'brief' => anchor(
				'clients/brief/%d',
				'<i class="fa fa-search"></i>',
				array(
					'title' 				=> __( "Brief" ),
					'class' 				=> 'brief btn btn-info',
					// 'data-enlarge-modal' 	=> 1,
				)
			),
		);
	}

	/**
	 * Index redirecting to lists
	 * 
	 * @return void
	 */
	public function index()
	{
		redirect( 'clients/lists' );
	}

	/**
	 * Items list
	 * 
	 * @return output
	 */
	public function lists()
	{
		if ( $_POST )
		{
			if ( ! empty( $_POST['cb_ids'] ) )
			{
				switch ( $_POST['bulk_actions'] )
				{
					default: break;
					case 'trash': 
						if ( $this->permissions->can_trash() )
						{
							foreach ( $_POST['cb_ids'] AS $id )
							{
								if ( $this->clients->save( array( 'client_id' => (int)$id, 'trash' => 1 ) ) )
								{
									$this->logs->set_data( array( 'bulk_actions' => $_POST['bulk_actions'], 'cb_id' => $id ), "POST" )->set_result( TRUE )->register();
									$this->alerts->persist( TRUE )->alert( __( "Items were successfully trashed." ), __( "Items trashed" ), NULL, 'success' );
								}
								else
								{
									$this->logs->set_data( array( 'bulk_actions' => $_POST['bulk_actions'], 'cb_id' => $id ), "POST" )->set_result( FALSE )->register();
									$this->alerts->persist( TRUE )->alert( __( "There were some errors while trying to perform the task. Please try again." ), __( "Error" ), NULL, 'danger' );
								}
							}

							redirect( uri_string() );
						}
						else
						{
							$this->alerts->alert( $this->assets->conf['no_permission']['message'], $this->assets->conf['no_permission']['title'], NULL, 'warning' );
						}
					break;
				} 
			}
		}

		if ( isset( $_GET['action'] ) AND isset( $_GET['client_id'] ) )
		{
			if ( $_GET['action'] == "trash" AND $this->permissions->can_trash() )
			{
				if ( $this->clients->save( array( 'client_id' => (int)$_GET['client_id'], 'trash' => 1 ) ) )
				{
					$this->logs->set_data( $_GET, "GET" )->set_result( TRUE )->register();
					$this->alerts->alert( __( "Item was successfully trashed." ), __( "Item trashed" ), NULL, 'success' );
				}
				else
				{
					$this->logs->set_data( $_GET, "GET" )->set_result( FALSE )->register();
					$this->alerts->alert( __( "There were some errors while trying to perform the task. Please try again." ), __( "Error" ), NULL, 'danger' );
				}
			}
			else
			{
				$this->alerts->alert( $this->assets->conf['no_permission']['message'], $this->assets->conf['no_permission']['title'], NULL, 'warning' );
			}
		}

		// Order
		if ( $this->input->get( 'order_by' ) AND $this->input->get( 'direction' ) )
		{
			$query_args['order_by'][$this->input->get( 'order_by' )] = $this->input->get( 'direction' );
		}
		else
		{
			$query_args['order_by']['client_id'] = 'DESC';
		}

		$query_args['where']['trash'] = 0;		
		$query_args['limit'] = $this->assets->conf['items_per_page'];
		$query_args['offset'] = $this->uri->segment( 3 );
		$this->data['clients'] = $this->clients->get( $query_args );
		$this->data['pagination'] = $this->templates->create_pagination( $this->assets->conf['tables']['clients'], $query_args );

		$this->templates->reverse_pagination( $this->data['clients'] );
		
		$this->assets->load_script( 'lists.min.js' );
		
		$this->data['tags']['page_title'] = __( "Clients List" );
		$this->templates->view( 'clients/lists', $this->data );
	}

	/**
	 * Items trash
	 * 
	 * @return output
	 */
	public function trash()
	{
		$this->permissions->block_not_admin();
		
		if ( $_POST )
		{
			if ( ! empty( $_POST['cb_ids'] ) )
			{
				switch ( $_POST['bulk_actions'] )
				{
					default: break;
					case 'restore': 
						if ( $this->permissions->can_restore() )
						{
							foreach ( $_POST['cb_ids'] AS $id )
							{
								if ( $this->clients->save( array( 'client_id' => $id, 'trash' => 0 ) ) )
								{
									$this->logs->set_data( array( 'bulk_actions' => $_POST['bulk_actions'], 'cb_id' => $id ), "POST" )->set_result( TRUE )->register();
									$this->alerts->persist( TRUE )->alert( __( "Items were successfully restored." ), __( "Items restored" ), NULL, 'success' );
								}
								else
								{
									$this->logs->set_data( array( 'bulk_actions' => $_POST['bulk_actions'], 'cb_id' => $id ), "POST" )->set_result( FALSE )->register();
									$this->alerts->persist( TRUE )->alert( __( "There were some errors while trying to perform the task. Please try again." ), __( "Error" ), NULL, 'danger' );
								}
							}

							redirect( uri_string() );
						}
						else
						{
							$this->alerts->alert( $this->assets->conf['no_permission']['message'], $this->assets->conf['no_permission']['title'], NULL, 'warning' );
						}
					break;
					case 'delete':
						if ( $this->permissions->can_delete() )
						{
							foreach ( $_POST['cb_ids'] AS $id )
							{
								if ( $this->clients->erase( array( 'client_id' => $id ) ) )
								{
									$this->logs->set_data( array( 'bulk_actions' => $_POST['bulk_actions'], 'cb_id' => $id ), "POST" )->set_result( TRUE )->register();
									$this->alerts->persist( TRUE )->alert( __( "Items were successfully deleted." ), __( "Items deleted" ), NULL, 'success' );
								}
								else
								{
									$this->logs->set_data( array( 'bulk_actions' => $_POST['bulk_actions'], 'cb_id' => $id ), "POST" )->set_result( FALSE )->register();
									$this->alerts->persist( TRUE )->alert( __( "There were some errors while trying to perform the task. Please try again." ), __( "Error" ), NULL, 'danger' );
								}
							}

							redirect( uri_string() );
						}
						else
						{
							$this->alerts->alert( $this->assets->conf['no_permission']['message'], $this->assets->conf['no_permission']['title'], NULL, 'warning' );
						}
					break;
				} 
			}
		}

		if ( isset( $_GET['action'] ) AND isset( $_GET['client_id'] ) )
		{
			if ( $_GET['action'] == "restore" AND $this->permissions->can_restore() )
			{
				if ( $this->clients->save( array( 'client_id' => (int)$_GET['client_id'], 'trash' => 0 ) ) )
				{
					$this->logs->set_data( $_GET, "GET" )->set_result( TRUE )->register();
					$this->alerts->alert( __( "Item successfully restored." ), __( "Item restored" ), NULL, 'success' );
				}
				else
				{
					$this->logs->set_data( $_GET, "GET" )->set_result( FALSE )->register();
					$this->alerts->alert( __( "There were some errors while trying to perform the task. Please try again." ), __( "Error" ), NULL, 'danger' );				
				}
			}
			else if ( $_GET['action'] == "delete" AND $this->permissions->can_delete() )
			{
				if ( $this->clients->erase( (int)$_GET['client_id'] ) )
				{
					$this->logs->set_data( $_GET, "GET" )->set_result( TRUE )->register();
					$this->alerts->alert( __( "Item successfully deleted." ), __( "Item deleted" ), NULL, 'success' );
				}
				else
				{
					$this->logs->set_data( $_GET, "GET" )->set_result( FALSE )->register();
					$this->alerts->alert( __( "There were some errors while trying to perform the task. Please try again." ), __( "Error" ), NULL, 'danger' );				
				}
			}
			else
			{
				$this->alerts->alert( $this->assets->conf['no_permission']['message'], $this->assets->conf['no_permission']['title'], NULL, 'warning' );
			}
		}

		// Order
		if ( $this->input->get( 'order_by' ) AND $this->input->get( 'direction' ) )
		{
			$query_args['order_by'][$this->input->get( 'order_by' )] = $this->input->get( 'direction' );
		}
		else
		{
			$query_args['order_by']['client_id'] = 'DESC';
		}

		$query_args['where']['trash'] = 1;
		$query_args['limit'] = $this->assets->conf['items_per_page'];
		$query_args['offset'] = $this->uri->segment( 3 );
		$this->data['clients'] = $this->clients->get( $query_args );
		$this->data['pagination'] = $this->templates->create_pagination( $this->assets->conf['tables']['clients'], $query_args );
		
		$this->templates->reverse_pagination( $this->data['clients'] );
		
		$this->assets->load_script( 'lists.min.js' );
		
		$this->data['tags']['page_title'] = __( "Clients Trash" );
		$this->templates->view( 'clients/lists', $this->data );
	}

	/**
	 * Item add page
	 * 
	 * @return output
	 */
	public function add()
	{
		if ( $this->input->post() )
		{
			if ( ! $this->permissions->can_add() )
			{
				$this->alerts->persist( TRUE )->alert( $this->assets->conf['no_permission']['message'], $this->assets->conf['no_permission']['title'], NULL, 'warning' );
				redirect( uri_string() );
			}

			$this->form_validation->set_rules( 'first_name', __( "First Name" ), 'trim|required' );
			$this->form_validation->set_rules( 'ruc', __( "RUC or ID" ), 'required|valid_ruc|is_unique[' . $this->assets->conf['tables']['clients'] . '.ruc]' );

			if ( $this->form_validation->run() )
			{
				if ( $client_id = $this->clients->save( $this->input->post( NULL, TRUE ) ) )
				{
					// Associate in case there were any uploaded files
					if ( $this->input->post( 'file_id' ) )
					{
						$this->load->model( "Files_model", "files" );
						$this->files->save( array(
							'file_id' => $this->input->post( 'file_id' ),
							'registry_id' => $client_id,
						) );
					}

					$_POST['client_id'] = $client_id;
					$this->logs->set_data( $this->input->post(), "POST" )->set_result( TRUE )->register();
					$this->alerts->persist( TRUE )->alert( __( "Item successfully added to the database." ), __( "Client created" ), NULL, 'success' );

					redirect( "clients/edit/" . $client_id );
				}
				else
				{
					$this->logs->set_data( $this->input->post(), "POST" )->set_result( FALSE )->register();
					$this->alerts->alert( __( "There were some errors while trying to perform the task. Please try again." ), __( "Client discarded" ), NULL, 'danger' );
				}
			}
			else
			{
				$this->alerts->alert( __( "Errors appear below every field." ), __( "Item could not be stored into the database" ), NULL, 'danger' );
			}
		}

		$this->data['first_name'] = array(
			'type'		=> 'text',
			'id'		=> 'first_name',
			'name'		=> 'first_name',
			'class'		=> 'form-control form-control-lg',
			'value'		=> set_value( 'first_name' ),
		);

		$this->data['last_name'] = array(
			'type'		=> 'text',
			'id'		=> 'last_name',
			'name'		=> 'last_name',
			'class'		=> 'form-control form-control-lg',
			'value'		=> set_value( 'last_name' ),
		);

		$this->data['ruc'] = array(
			'type'		=> 'text',
			'id'		=> 'ruc',
			'name'		=> 'ruc',
			'class'		=> 'form-control input-ruc',
			'value'		=> set_value( 'ruc' ),
		);

		$this->data['birthday'] = array(
			'type'		=> 'text',
			'id'		=> 'birthday',
			'name'		=> 'birthday',
			'class'		=> 'form-control input-date-birthday',
			'value'		=> set_value( 'birthday' ),
		);

		$this->data['observations'] = array(
			'id'			=> 'observations',
			'name'			=> 'observations',
			'class'			=> 'form-control',
			'rows'			=> 3,
			'placeholder'	=> __( "Use this field to store any other information about the client..." ),
			'value'			=> set_value( 'observations' ),
		);

		$this->data['address'] = array(
			'type'		=> 'text',
			'id'		=> 'address',
			'name'		=> 'address',
			'class'		=> 'form-control',
			'value'		=> set_value( 'address' ),
		);

		$this->data['city'] = array(
			'type'		=> 'text',
			'id'		=> 'city',
			'name'		=> 'city',
			'class'		=> 'form-control',
			'value'		=> set_value( 'city' ),
		);

		$this->data['state'] = array(
			'type'		=> 'text',
			'id'		=> 'state',
			'name'		=> 'state',
			'class'		=> 'form-control',
			'value'		=> set_value( 'state' ),
		);

		$this->data['country'] = array(
			'type'		=> 'text',
			'id'		=> 'country',
			'name'		=> 'country',
			'class'		=> 'form-control',
			'value'		=> set_value( 'country' ),
		);

		$this->data['telephone'] = array(
			'type'		=> 'tel',
			'id'		=> 'telephone',
			'name'		=> 'telephone',
			'class'		=> 'form-control input-phone',
			'value'		=> set_value( 'telephone' ),
		);

		$this->data['cellphone'] = array(
			'type'		=> 'tel',
			'id'		=> 'cellphone',
			'name'		=> 'cellphone',
			'class'		=> 'form-control input-phone',
			'value'		=> set_value( 'cellphone' ),
		);

		$this->data['email'] = array(
			'type'		=> 'email',
			'id'		=> 'email',
			'name'		=> 'email',
			'class'		=> 'form-control',
			'value'		=> set_value( 'email' ),
		);

		$this->data['website'] = array(
			'type'		=> 'text',
			'id'		=> 'website',
			'name'		=> 'website',
			'class'		=> 'form-control',
			'value'		=> set_value( 'website' ),
		);

		// Pickadate
		$this->assets->load_style( base_url( 'assets/bower_components/pickadate/lib/compressed/themes/default.css' ) );
		$this->assets->load_style( base_url( 'assets/bower_components/pickadate/lib/compressed/themes/default.date.css' ) );
		$this->assets->load_script( base_url( 'assets/bower_components/pickadate/lib/compressed/picker.js' ) );
		$this->assets->load_script( base_url( 'assets/bower_components/pickadate/lib/compressed/picker.date.js' ) );	
		if ( $this->assets->conf['language'] !== "en_US" ) $this->assets->load_script( base_url( 'assets/bower_components/pickadate/lib/translations/' . $this->assets->conf['language'] . '.js' ) );

		// Inputmask
		$this->assets->load_script( base_url( 'assets/bower_components/inputmask/dist/jquery.inputmask.min.js' ) );

		// jQuery File Uploader assets
		$this->assets->load_style( site_url( 'assets/bower_components/jQuery-File-Upload/css/jquery.fileupload.css' ) );
		$this->assets->load_script( site_url( 'assets/bower_components/jQuery-File-Upload/js/vendor/jquery.ui.widget.js' ) );
		$this->assets->load_script( site_url( 'assets/bower_components/jQuery-File-Upload/js/jquery.iframe-transport.js' ) );
		$this->assets->load_script( site_url( 'assets/bower_components/jQuery-File-Upload/js/jquery.fileupload.js' ) );

		$this->assets->load_script( 'forms.min.js' );
		$this->assets->load_script( 'forms-clients.min.js' );

		$this->data['tags']['page_title'] = __( "Add Client" );
		$this->templates->view( 'clients/form', $this->data );
	}

	/**
	 * Item edit page
	 * 
	 * @param  int $client_id Client ID
	 * @return output
	 */
	public function edit( $client_id )
	{
		if ( $this->input->post() )
		{
			if ( ! $this->permissions->can_edit() )
			{
				$this->alerts->persist( TRUE )->alert( $this->assets->conf['no_permission']['message'], $this->assets->conf['no_permission']['title'], NULL, 'warning' );
				redirect( uri_string() );
			}

			$val_client = $this->clients->get( $client_id );

			$this->form_validation->set_rules( 'client_id', __( "Client ID" ), 'integer|required' );
			$this->form_validation->set_rules( 'first_name', __( "First Name" ), 'trim|required' );
			$this->form_validation->set_rules( 'ruc', __( "RUC or ID" ), 'required|valid_ruc' );

			if ( $this->input->post( 'ruc' ) != $val_client['ruc'] )
			{
				$this->form_validation->set_rules( 'ruc', __( "RUC or ID" ), 'required|valid_ruc|is_unique[' . $this->assets->conf['tables']['clients'] . '.ruc]' );
			}

			if ( $this->form_validation->run() )
			{
				if ( $this->clients->save( $this->input->post( NULL, TRUE ) ) )
				{
					$this->logs->set_data( $this->input->post(), "POST" )->set_result( TRUE )->register();
					$this->alerts->persist( TRUE )->alert( __( "Item was successfully updated into the database." ), __( "Client updated" ), NULL, 'success' );
					redirect( "clients/edit/" . $client_id );
				}
				else
				{
					$this->logs->set_data( $this->input->post(), "POST" )->set_result( FALSE )->register();
					$this->alerts->alert( __( "There were some errors while trying to perform the task. Please try again." ), __( "Client not updated" ), NULL, 'danger' );
				}
			}
			else
			{
				$this->alerts->alert( __( "Errors appear below every field." ), __( "Item could not be stored into the database" ), NULL, 'danger' );
			}
		}

		$this->data['client'] = $this->clients->get( $client_id );

		if ( ! empty( $this->data['client'] ) )
		{
			$this->data['client_id'] = array(
				'type'		=> 'hidden',
				'id'		=> 'client_id',
				'name'		=> 'client_id',
				'value'		=> $this->data['client']['client_id'],
			);

			$this->data['first_name'] = array(
				'type'		=> 'text',
				'id'		=> 'first_name',
				'name'		=> 'first_name',
				'class'		=> 'form-control form-control-lg',
				'value'		=> $this->data['client']['first_name'],
			);

			$this->data['last_name'] = array(
				'type'		=> 'text',
				'id'		=> 'last_name',
				'name'		=> 'last_name',
				'class'		=> 'form-control form-control-lg',
				'value'		=> $this->data['client']['last_name'],
			);			

			$this->data['ruc'] = array(
				'type'		=> 'text',
				'id'		=> 'ruc',
				'name'		=> 'ruc',
				'class'		=> 'form-control input-ruc',
				'value'		=> $this->data['client']['ruc'],
			);

			$this->data['birthday'] = array(
				'type'		=> 'text',
				'id'		=> 'birthday',
				'name'		=> 'birthday',
				'class'		=> 'form-control input-date-birthday',
				'value'		=> ( ! empty( $this->data['client']['birthday'] ) ) ? date( $this->assets->conf['date_format'], strtotime( $this->data['client']['birthday'] ) ) : NULL,
			);

			$this->data['observations'] = array(
				'id'			=> 'observations',
				'name'			=> 'observations',
				'class'			=> 'form-control',
				'rows'			=> 3,
				'placeholder'	=> __( "Use this field to store any other information about the client..." ),
				'value'			=> $this->data['client']['observations'],
			);

			$this->data['address'] = array(
				'type'		=> 'text',
				'id'		=> 'address',
				'name'		=> 'address',
				'class'		=> 'form-control',
				'value'		=> $this->data['client']['address'],
			);

			$this->data['city'] = array(
				'type'		=> 'text',
				'id'		=> 'city',
				'name'		=> 'city',
				'class'		=> 'form-control',
				'value'		=> $this->data['client']['city'],
			);

			$this->data['state'] = array(
				'type'		=> 'text',
				'id'		=> 'state',
				'name'		=> 'state',
				'class'		=> 'form-control',
				'value'		=> $this->data['client']['state'],
			);

			$this->data['country'] = array(
				'type'		=> 'text',
				'id'		=> 'country',
				'name'		=> 'country',
				'class'		=> 'form-control',
				'value'		=> $this->data['client']['country'],
			);

			$this->data['telephone'] = array(
				'type'		=> 'tel',
				'id'		=> 'telephone',
				'name'		=> 'telephone',
				'class'		=> 'form-control input-phone',
				'value'		=> $this->data['client']['telephone'],
			);

			$this->data['cellphone'] = array(
				'type'		=> 'tel',
				'id'		=> 'cellphone',
				'name'		=> 'cellphone',
				'class'		=> 'form-control input-phone',
				'value'		=> $this->data['client']['cellphone'],
			);

			$this->data['email'] = array(
				'type'		=> 'email',
				'id'		=> 'email',
				'name'		=> 'email',
				'class'		=> 'form-control',
				'value'		=> $this->data['client']['email'],
			);

			$this->data['website'] = array(
				'type'		=> 'text',
				'id'		=> 'website',
				'name'		=> 'website',
				'class'		=> 'form-control',
				'value'		=> $this->data['client']['website'],
			);

			// Pickadate
			$this->assets->load_style( base_url( 'assets/bower_components/pickadate/lib/compressed/themes/default.css' ) );
			$this->assets->load_style( base_url( 'assets/bower_components/pickadate/lib/compressed/themes/default.date.css' ) );
			$this->assets->load_script( base_url( 'assets/bower_components/pickadate/lib/compressed/picker.js' ) );
			$this->assets->load_script( base_url( 'assets/bower_components/pickadate/lib/compressed/picker.date.js' ) );	
			if ( $this->assets->conf['language'] !== "en_US" ) $this->assets->load_script( base_url( 'assets/bower_components/pickadate/lib/translations/' . $this->assets->conf['language'] . '.js' ) );

			// Inputmask
			$this->assets->load_script( base_url( 'assets/bower_components/inputmask/dist/jquery.inputmask.min.js' ) );

			// jQuery File Uploader assets
			$this->assets->load_style( site_url( 'assets/bower_components/jQuery-File-Upload/css/jquery.fileupload.css' ) );
			$this->assets->load_script( site_url( 'assets/bower_components/jQuery-File-Upload/js/vendor/jquery.ui.widget.js' ) );
			$this->assets->load_script( site_url( 'assets/bower_components/jQuery-File-Upload/js/jquery.iframe-transport.js' ) );
			$this->assets->load_script( site_url( 'assets/bower_components/jQuery-File-Upload/js/jquery.fileupload.js' ) );

			$this->assets->load_script( 'forms.min.js' );
			$this->assets->load_script( 'forms-clients.min.js' );

			$this->data['tags']['page_title'] = __( "Edit Client" );
			$this->templates->view( 'clients/form', $this->data );
		}
		else
		{
			$this->alerts->persist( TRUE )->alert( __( "The client is not stored into the database." ), __( "Client does not exist" ), NULL, 'danger' );
			redirect( 'clients/lists' );
		}
	}

	/**
	 * Item brief page
	 * 
	 * @param  int $client_id The client id
	 * @return output
	 */
	public function brief( $client_id )
	{
		$this->data['client'] = $this->clients->get( $client_id );

		if ( ! empty( $this->data['client'] ) )
		{
			if ( ! empty( $this->data['client']['birthday'] ) ) $this->data['client']['age'] = timespan( strtotime( $this->data['client']['birthday'] ), time(), 1 );
			if ( ! empty( $this->data['client']['birthday'] ) ) $this->data['client']['birthday'] = date( $this->assets->conf['date_format'], strtotime( $this->data['client']['birthday'] ) );
			
			$this->data['client'] = $this->assets->fill_empty_vars( $this->data['client'], 'clients' );
			$this->data['tags']['page_title'] = __( "Client Brief" );
			$this->templates->view( 'clients/brief', $this->data );
		}
		else
		{
			$this->alerts->persist( TRUE )->alert( __( "The client is not stored into the database." ), __( "Client does not exist" ), NULL, 'danger' );
			redirect( 'clients/lists' );
		}
	}
}