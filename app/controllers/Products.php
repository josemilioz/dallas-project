<?php defined( 'BASEPATH' ) OR exit( 'No direct script access allowed' );

class Products extends CI_Controller
{
	/**
	 * Class constructor
	 */
	public function __construct()
	{
		parent::__construct();

		if ( ! $this->ion_auth->logged_in() ) redirect( 'login', 'refresh' );

		$this->load->model( "Categories_model", "categories" );
		$this->load->model( "Products_model", "products" );

		$this->data['tags']['controller_name'] = __( "Products" );

		/**
		 * -----------------------------------------------------------------------------------------------
		 * BULK ACTIONS FOR SELECT IN LISTS
		 * -----------------------------------------------------------------------------------------------
		 */

		$bulk_options['-1'] = __( "Bulk actions" );
		if ( $this->uri->segment( 2 ) != 'trash' AND $this->permissions->can_trash() ) $bulk_options['trash'] = __( "Move to Trash" );
		if ( $this->uri->segment( 2 ) == 'trash' AND $this->permissions->can_restore() ) $bulk_options['restore'] = __( "Restore" );
		if ( $this->uri->segment( 2 ) == 'trash' AND $this->permissions->can_delete() ) $bulk_options['delete'] = __( "Delete" );
		
		$this->data['bulk_actions'] = array(
			'id'		=> 'bulk_actions',
			'name'		=> 'bulk_actions',
			'class'		=> 'form-control form-control-sm mb-1',
			'options'	=> $bulk_options,
		);

		$this->data['submit_bulk_actions'] = array(
			'id'			=> 'submit_bulk_actions',
			'name'			=> 'submit_bulk_actions',
			'class'			=> 'btn btn-secondary btn-sm btn-bulk mb-1',
			'value'			=> __( "Apply" ),
			'data-confirm'	=> __( "Are you sure you want to perform that action?\n\nIt may be definitive and unable to undo in the future." ),
		);

		/**
		 * -----------------------------------------------------------------------------------------------
		 * LIST TOOLS EXTENSION
		 * -----------------------------------------------------------------------------------------------
		 */
		
		$this->data['list_buttons_extension'] = array(
			'brief' => anchor(
				'products/brief/%d',
				'<i class="fa fa-search"></i>',
				array(
					'title' => __( "Brief" ),
					'class' => 'brief btn btn-info',
				)
			),
		);
	}

	/**
	 * Index redirecting to lists
	 * 
	 * @return void
	 */
	public function index()
	{
		redirect( 'products/lists' );
	}

	/**
	 * Items list
	 * 
	 * @return output
	 */
	public function lists()
	{
		if ( $_POST )
		{
			if ( ! empty( $_POST['cb_ids'] ) )
			{
				switch ( $_POST['bulk_actions'] )
				{
					default: break;
					case 'trash': 
						if ( $this->permissions->can_trash() )
						{
							foreach ( $_POST['cb_ids'] AS $id )
							{
								if ( $this->products->save( array( 'product_id' => (int)$id, 'trash' => 1 ) ) )
								{
									$this->logs->set_data( array( 'bulk_actions' => $_POST['bulk_actions'], 'cb_id' => $id ), "POST" )->set_result( TRUE )->register();
									$this->alerts->persist( TRUE )->alert( __( "Items were successfully trashed." ), __( "Items trashed" ), NULL, 'success' );
								}
								else
								{
									$this->logs->set_data( array( 'bulk_actions' => $_POST['bulk_actions'], 'cb_id' => $id ), "POST" )->set_result( FALSE )->register();
									$this->alerts->persist( TRUE )->alert( __( "There were some errors while trying to perform the task. Please try again." ), __( "Error" ), NULL, 'danger' );
								}
							}

							redirect( uri_string() );
						}
						else
						{
							$this->alerts->alert( $this->assets->conf['no_permission']['message'], $this->assets->conf['no_permission']['title'], NULL, 'warning' );
						}
					break;
				} 
			}
		}

		if ( isset( $_GET['action'] ) AND isset( $_GET['product_id'] ) )
		{
			if ( $_GET['action'] == "trash" AND $this->permissions->can_trash() )
			{
				if ( $this->products->save( array( 'product_id' => (int)$_GET['product_id'], 'trash' => 1 ) ) )
				{
					$this->logs->set_data( $_GET, "GET" )->set_result( TRUE )->register();
					$this->alerts->alert( __( "Item was successfully trashed." ), __( "Item trashed" ), NULL, 'success' );
				}
				else
				{
					$this->logs->set_data( $_GET, "GET" )->set_result( FALSE )->register();
					$this->alerts->alert( __( "There were some errors while trying to perform the task. Please try again." ), __( "Error" ), NULL, 'danger' );
				}
			}
			else
			{
				$this->alerts->alert( $this->assets->conf['no_permission']['message'], $this->assets->conf['no_permission']['title'], NULL, 'warning' );
			}
		}

		// Order
		if ( $this->input->get( 'order_by' ) AND $this->input->get( 'direction' ) )
		{
			$query_args['order_by'][$this->input->get( 'order_by' )] = $this->input->get( 'direction' );
		}
		else
		{
			$query_args['order_by']['product_id'] = 'DESC';
		}

		if ( $this->input->get( 'category_id' ) ) $query_args['where']['category_id'] = $this->input->get( 'category_id' );

		$query_args['where']['trash'] = 0;
		$query_args['limit'] = $this->assets->conf['items_per_page'];
		$query_args['offset'] = $this->uri->segment( 3 );
		$this->data['products'] = $this->products->get( $query_args );
		$this->data['pagination'] = $this->templates->create_pagination( $this->assets->conf['tables']['products'], $query_args );

		$this->templates->reverse_pagination( $this->data['products'] );
		
		$this->assets->load_script( 'lists.min.js' );
		
		$this->data['tags']['page_title'] = __( "Products List" );
		$this->templates->view( 'products/lists', $this->data );
	}

	/**
	 * Items trash
	 * 
	 * @return output
	 */
	public function trash()
	{
		$this->permissions->block_not_admin();
		
		if ( $_POST )
		{
			if ( ! empty( $_POST['cb_ids'] ) )
			{
				switch ( $_POST['bulk_actions'] )
				{
					default: break;
					case 'restore': 
						if ( $this->permissions->can_restore() )
						{
							foreach ( $_POST['cb_ids'] AS $id )
							{
								if ( $this->products->save( array( 'product_id' => (int)$id, 'trash' => 0 ) ) )
								{
									$this->logs->set_data( array( 'bulk_actions' => $_POST['bulk_actions'], 'cb_id' => $id ), "POST" )->set_result( TRUE )->register();
									$this->alerts->persist( TRUE )->alert( __( "Items were successfully restored." ), __( "Items restored" ), NULL, 'success' );
								}
								else
								{
									$this->logs->set_data( array( 'bulk_actions' => $_POST['bulk_actions'], 'cb_id' => $id ), "POST" )->set_result( FALSE )->register();
									$this->alerts->persist( TRUE )->alert( __( "There were some errors while trying to perform the task. Please try again." ), __( "Error" ), NULL, 'danger' );
								}
							}

							redirect( uri_string() );
						}
						else
						{
							$this->alerts->alert( $this->assets->conf['no_permission']['message'], $this->assets->conf['no_permission']['title'], NULL, 'warning' );
						}
					break;
					case 'delete':
						if ( $this->permissions->can_delete() )
						{
							foreach ( $_POST['cb_ids'] AS $id )
							{
								if ( $this->products->erase( (int)$id ) )
								{
									$this->logs->set_data( array( 'bulk_actions' => $_POST['bulk_actions'], 'cb_id' => $id ), "POST" )->set_result( TRUE )->register();
									$this->alerts->persist( TRUE )->alert( __( "Items were successfully deleted." ), __( "Items deleted" ), NULL, 'success' );
								}
								else
								{
									$this->logs->set_data( array( 'bulk_actions' => $_POST['bulk_actions'], 'cb_id' => $id ), "POST" )->set_result( FALSE )->register();
									$this->alerts->persist( TRUE )->alert( __( "There were some errors while trying to perform the task. Please try again." ), __( "Error" ), NULL, 'danger' );
								}
							}

							redirect( uri_string() );
						}
						else
						{
							$this->alerts->alert( $this->assets->conf['no_permission']['message'], $this->assets->conf['no_permission']['title'], NULL, 'warning' );
						}
					break;
				} 
			}
		}

		if ( isset( $_GET['action'] ) AND isset( $_GET['product_id'] ) )
		{
			if ( $_GET['action'] == "restore" AND $this->permissions->can_restore() )
			{
				if ( $this->products->save( array( 'product_id' => (int)$_GET['product_id'], 'trash' => 0 ) ) )
				{
					$this->logs->set_data( $_GET, "GET" )->set_result( TRUE )->register();
					$this->alerts->alert( __( "Item successfully restored." ), __( "Item restored" ), NULL, 'success' );
				}
				else
				{
					$this->logs->set_data( $_GET, "GET" )->set_result( FALSE )->register();
					$this->alerts->alert( __( "There were some errors while trying to perform the task. Please try again." ), __( "Error" ), NULL, 'danger' );				
				}
			}
			else if ( $_GET['action'] == "delete" AND $this->permissions->can_delete() )
			{
				if ( $this->products->erase( (int)$_GET['product_id'] ) )
				{
					$this->logs->set_data( $_GET, "GET" )->set_result( TRUE )->register();
					$this->alerts->alert( __( "Item successfully deleted." ), __( "Item deleted" ), NULL, 'success' );
				}
				else
				{
					$this->logs->set_data( $_GET, "GET" )->set_result( FALSE )->register();
					$this->alerts->alert( __( "There were some errors while trying to perform the task. Please try again." ), __( "Error" ), NULL, 'danger' );				
				}
			}
			else
			{
				$this->alerts->alert( $this->assets->conf['no_permission']['message'], $this->assets->conf['no_permission']['title'], NULL, 'warning' );
			}
		}

		// Order
		if ( $this->input->get( 'order_by' ) AND $this->input->get( 'direction' ) )
		{
			$query_args['order_by'][$this->input->get( 'order_by' )] = $this->input->get( 'direction' );
		}
		else
		{
			$query_args['order_by']['product_id'] = 'DESC';
		}

		$query_args['where']['trash'] = 1;
		$query_args['limit'] = $this->assets->conf['items_per_page'];
		$query_args['offset'] = $this->uri->segment( 3 );
		$this->data['products'] = $this->products->get( $query_args );
		$this->data['pagination'] = $this->templates->create_pagination( $this->assets->conf['tables']['products'], $query_args );

		$this->templates->reverse_pagination( $this->data['products'] );
		
		$this->assets->load_script( 'lists.min.js' );
		
		$this->data['tags']['page_title'] = __( "Products Trash" );
		$this->templates->view( 'products/lists', $this->data );
	}

	/**
	 * Item add page
	 * 
	 * @return output
	 */
	public function add()
	{
		if ( $this->input->post() )
		{
			if ( ! $this->permissions->can_add() )
			{
				$this->alerts->persist( TRUE )->alert( $this->assets->conf['no_permission']['message'], $this->assets->conf['no_permission']['title'], NULL, 'warning' );
				redirect( uri_string() );
			}

			$this->form_validation->set_rules( 'name',			__( "Name" ), 			'trim|required' );
			$this->form_validation->set_rules( 'category_id',	__( "Category" ), 		'required' );
			$this->form_validation->set_rules( 'currency',		__( "Currency" ), 		'trim|required' );
			$this->form_validation->set_rules( 'price',			__( "Price" ), 			'trim|required|greater_than[0]|decimal' );
			$this->form_validation->set_rules( 'tax',			__( "Tax" ), 			'trim|required' );
			$this->form_validation->set_rules( 'unit_measure',	__( "Unit measure" ), 	'trim|required' );
			$this->form_validation->set_rules( 'unit_quantity',	__( "Unit quantity" ), 	'trim|required|greater_than[0]|' . ( ( $this->assets->conf['qty_decimals'] > 0 ) ? 'decimal' : 'integer' ) );

			if ( $this->form_validation->run() )
			{
				if ( $product_id = $this->products->save( $this->input->post( NULL, TRUE ) ) )
				{
					// Associate in case there were any uploaded files
					if ( $this->input->post( 'file_id[]' ) )
					{
						$this->load->model( "Files_model", "files" );

						foreach ( $this->input->post( 'file_id[]' ) AS $file_id )
						{
							$this->files->save( array(
								'file_id' 		=> $file_id,
								'registry_id' 	=> $product_id,
							) );							
						}

						// Re-order pictures if they're present
						$this->files->reorder_filelist( $this->input->post( 'file_id[]' ) );
					}

					$_POST['product_id'] = $product_id;
					$this->logs->set_data( $this->input->post(), "POST" )->set_result( TRUE )->register();
					$this->alerts->persist( TRUE )->alert( __( "Item successfully added to the database." ), __( "Product created" ), NULL, 'success' );

					redirect( "products/edit/" . $product_id );
				}
				else
				{
					$this->logs->set_data( $this->input->post(), "POST" )->set_result( FALSE )->register();
					$this->alerts->alert( __( "There were some errors while trying to perform the task. Please try again." ), __( "Product discarded" ), NULL, 'danger' );
				}
			}
			else
			{
				$this->alerts->alert( __( "Errors appear below every field." ), __( "Item could not be stored into the database" ), NULL, 'danger' );
			}
		}

		$this->data['name'] = array(
			'type'		=> 'text',
			'id'		=> 'name',
			'name'		=> 'name',
			'class'		=> 'form-control form-control-lg',
			'value'		=> set_value( 'name' ),
		);

		$this->data['category_id'] = array(
			'id'		=> 'category_id',
			'name'		=> 'category_id',
			'class'		=> 'form-control',
			'selected'	=> set_value( 'category_id' ),
			'options'	=> $this->categories->build_dropdown(),
		);

		$this->data['description'] = array(
			'id'			=> 'description',
			'name'			=> 'description',
			'class'			=> 'form-control',
			'rows'			=> 3,
			'placeholder'	=> __( "Use this field to store any other information about the product..." ),
			'value'			=> set_value( 'description' ),
		);

		$this->data['currency'] = array(
			'id'		=> 'currency',
			'name'		=> 'currency',
			'class'		=> 'form-control',
			'selected'	=> set_value( 'currency' ),
			'options'	=> $this->assets->conf['currencies_ext'],
		);

		$this->data['price'] = array(
			'type'		=> 'text',
			'id'		=> 'price',
			'name'		=> 'price',
			'class'		=> 'form-control input-currency-foreign',
			'value'		=> set_value( 'price' ),
		);

		$this->data['unit_measure'] = array(
			'id'		=> 'unit_measure',
			'name'		=> 'unit_measure',
			'class'		=> 'form-control',
			'selected'	=> set_value( 'unit_measure' ),
			'options'	=> array_merge( array( '' => '&nbsp;' ), $this->assets->conf['units'] ),
		);

		$this->data['unit_quantity'] = array(
			'type'		=> 'text',
			'id'		=> 'unit_quantity',
			'name'		=> 'unit_quantity',
			'class'		=> 'form-control input-quantity',
			'value'		=> set_value( 'unit_quantity', 1 ),
		);

		$this->data['tax'] = array(
			'id'		=> 'tax',
			'name'		=> 'tax',
			'class'		=> 'form-control',
			'selected'	=> set_value( 'tax' ),
			'options'	=> array_merge( array( '' => '&nbsp;' ), $this->assets->conf['bill_columns'] ),
		);

		$this->data['min_stock_warning'] = array(
			'type'		=> 'text',
			'id'		=> 'min_stock_warning',
			'name'		=> 'min_stock_warning',
			'class'		=> 'form-control input-quantity',
			'value'		=> set_value( 'min_stock_warning', $this->assets->conf['stock']['min_warning'] ),
		);

		$this->data['min_stock_danger'] = array(
			'type'		=> 'text',
			'id'		=> 'min_stock_danger',
			'name'		=> 'min_stock_danger',
			'class'		=> 'form-control input-quantity',
			'value'		=> set_value( 'min_stock_danger', $this->assets->conf['stock']['min_danger'] ),
		);

		$this->data['free'] = array(
			'type'		=> 'checkbox',
			'id'		=> 'free',
			'name'		=> 'free',
			'class' 	=> 'form-check-input',
			'value'		=> '1',
		);

		// jQuery UI sortable - this way so we don't mess the BS tooltip
		$this->assets->load_script( base_url( 'assets/bower_components/jquery-ui/ui/data.js' ) );
		$this->assets->load_script( base_url( 'assets/bower_components/jquery-ui/ui/scroll-parent.js' ) );
		$this->assets->load_script( base_url( 'assets/bower_components/jquery-ui/ui/disable-selection.js' ) );
		$this->assets->load_script( base_url( 'assets/bower_components/jquery-ui/ui/widget.js' ) );
		$this->assets->load_script( base_url( 'assets/bower_components/jquery-ui/ui/widgets/mouse.js' ) );
		$this->assets->load_script( base_url( 'assets/bower_components/jquery-ui/ui/widgets/sortable.js' ) );

		// Inputmask
		$this->assets->load_script( base_url( 'assets/bower_components/inputmask/dist/jquery.inputmask.min.js' ) );

		// jQuery File Uploader assets
		$this->assets->load_style( site_url( 'assets/bower_components/jQuery-File-Upload/css/jquery.fileupload.css' ) );
		$this->assets->load_script( site_url( 'assets/bower_components/jQuery-File-Upload/js/vendor/jquery.ui.widget.js' ) );
		$this->assets->load_script( site_url( 'assets/bower_components/jQuery-File-Upload/js/jquery.iframe-transport.js' ) );
		$this->assets->load_script( site_url( 'assets/bower_components/jQuery-File-Upload/js/jquery.fileupload.js' ) );

		// Select2
		$this->assets->load_style( site_url( 'assets/bower_components/select2/dist/css/select2.min.css' ) );
		$this->assets->load_script( site_url( 'assets/bower_components/select2/dist/js/select2.full.min.js' ) );
		$this->assets->load_script( site_url( 'assets/bower_components/select2/dist/js/i18n/' . substr( $this->assets->conf['language'], 0, 2 ) . '.js' ) );

		$this->assets->load_script( 'forms.min.js' );
		$this->assets->load_script( 'forms-products.min.js' );

		$this->data['tags']['page_title'] = __( "Add Product" );
		$this->templates->view( 'products/form', $this->data );
	}

	/**
	 * Item edit page
	 * 
	 * @param  int $product_id
	 * @return output
	 */
	public function edit( $product_id )
	{
		if ( $this->input->post() )
		{
			if ( ! $this->permissions->can_edit() )
			{
				$this->alerts->persist( TRUE )->alert( $this->assets->conf['no_permission']['message'], $this->assets->conf['no_permission']['title'], NULL, 'warning' );
				redirect( uri_string() );
			}

			$this->form_validation->set_rules( 'product_id', 	__( "Product ID" ), 	'integer|required' );
			$this->form_validation->set_rules( 'category_id', 	__( "Category" ), 		'required' );
			$this->form_validation->set_rules( 'name', 			__( "Name" ), 			'trim|required' );
			$this->form_validation->set_rules( 'currency', 		__( "Currency" ), 		'trim|required' );
			$this->form_validation->set_rules( 'price', 		__( "Price" ), 			'trim|required|greater_than[0]|decimal' );
			$this->form_validation->set_rules( 'tax', 			__( "Tax" ), 			'trim|required' );
			$this->form_validation->set_rules( 'unit_measure', 	__( "Unit measure" ), 	'trim|required' );
			$this->form_validation->set_rules( 'unit_quantity', __( "Unit quantity" ), 	'trim|required|greater_than[0]|' . ( ( $this->assets->conf['qty_decimals'] > 0 ) ? 'decimal' : 'integer' ) );

			if ( $this->form_validation->run() )
			{
				if ( $this->products->save( $this->input->post( NULL, TRUE ) ) )
				{
					// Associate in case there were any uploaded files
					if ( $this->input->post( 'file_id[]' ) )
					{
						$this->load->model( "Files_model", "files" );

						foreach ( $this->input->post( 'file_id[]' ) AS $file_id )
						{
							$this->files->save( array(
								'file_id' 		=> $file_id,
								'registry_id' 	=> $product_id,
							) );
						}

						// Re-order pictures if they're present
						$this->files->reorder_filelist( $this->input->post( 'file_id[]' ) );
					}

					$this->logs->set_data( $this->input->post(), "POST" )->set_result( TRUE )->register();
					$this->alerts->persist( TRUE )->alert( __( "Item was successfully updated into the database." ), __( "Product updated" ), NULL, 'success' );
					redirect( "products/edit/" . $product_id );
				}
				else
				{
					$this->logs->set_data( $this->input->post(), "POST" )->set_result( FALSE )->register();
					$this->alerts->alert( __( "There were some errors while trying to perform the task. Please try again." ), __( "Product not updated" ), NULL, 'danger' );
				}
			}
			else
			{
				$this->alerts->alert( __( "Errors appear below every field." ), __( "Item could not be stored into the database" ), NULL, 'danger' );
			}
		}

		$this->data['product'] = $this->products->get( $product_id );

		if ( ! empty( $this->data['product'] ) )
		{
			$this->data['product_id'] = array(
				'type'		=> 'hidden',
				'id'		=> 'product_id',
				'name'		=> 'product_id',
				'value'		=> $this->data['product']['product_id'],
			);

			$this->data['name'] = array(
				'type'		=> 'text',
				'id'		=> 'name',
				'name'		=> 'name',
				'class'		=> 'form-control form-control-lg',
				'value'		=> $this->data['product']['name'],
			);

			$this->data['category_id'] = array(
				'id'		=> 'category_id',
				'name'		=> 'category_id',
				'class'		=> 'form-control',
				'selected'	=> $this->data['product']['category_id'],
				'options'	=> $this->categories->build_dropdown(),
			);

			$this->data['description'] = array(
				'id'			=> 'description',
				'name'			=> 'description',
				'class'			=> 'form-control',
				'rows'			=> 3,
				'placeholder'	=> __( "Use this field to store any other information about the product..." ),
				'value'			=> $this->data['product']['description'],
			);

			$this->data['currency'] = array(
				'id'		=> 'currency',
				'name'		=> 'currency',
				'class'		=> 'form-control',
				'selected'	=> $this->data['product']['currency'],
				'options'	=> $this->assets->conf['currencies_ext'],
			);

			$this->data['price'] = array(
				'type'		=> 'text',
				'id'		=> 'price',
				'name'		=> 'price',
				'class'		=> 'form-control input-currency-foreign',
				'value'		=> my_number_format( $this->data['product']['price'] ),
			);

			$this->data['unit_measure'] = array(
				'id'		=> 'unit_measure',
				'name'		=> 'unit_measure',
				'class'		=> 'form-control',
				'selected'	=> $this->data['product']['unit_measure'],
				'options'	=> $this->assets->conf['units'],
			);

			$this->data['unit_quantity'] = array(
				'type'		=> 'text',
				'id'		=> 'unit_quantity',
				'name'		=> 'unit_quantity',
				'class'		=> 'form-control input-quantity',
				'value'		=> my_number_format( $this->data['product']['unit_quantity'], $this->assets->conf['qty_decimals'] ),
			);

			$this->data['tax'] = array(
				'id'		=> 'tax',
				'name'		=> 'tax',
				'class'		=> 'form-control',
				'selected'	=> $this->data['product']['tax'],
				'options'	=> array_merge( array( '' => '' ), $this->assets->conf['bill_columns'] ),
			);

			$this->data['min_stock_warning'] = array(
				'type'		=> 'text',
				'id'		=> 'min_stock_warning',
				'name'		=> 'min_stock_warning',
				'class'		=> 'form-control input-quantity',
				'value'		=> my_number_format( $this->data['product']['min_stock_warning'], $this->assets->conf['qty_decimals'] ),
			);

			$this->data['min_stock_danger'] = array(
				'type'		=> 'text',
				'id'		=> 'min_stock_danger',
				'name'		=> 'min_stock_danger',
				'class'		=> 'form-control input-quantity',
				'value'		=> my_number_format( $this->data['product']['min_stock_danger'], $this->assets->conf['qty_decimals'] ),
			);

			$this->data['free'] = array(
				'type'		=> 'checkbox',
				'id'		=> 'free',
				'name'		=> 'free',
				'class' 	=> 'form-check-input',
				'value'		=> '1',
			);

			if ( $this->data['product']['free'] == 1 ) $this->data['free']['checked'] = "checked";

			// jQuery UI sortable - this way so we don't mess the BS tooltip
			$this->assets->load_script( base_url( 'assets/bower_components/jquery-ui/ui/data.js' ) );
			$this->assets->load_script( base_url( 'assets/bower_components/jquery-ui/ui/scroll-parent.js' ) );
			$this->assets->load_script( base_url( 'assets/bower_components/jquery-ui/ui/disable-selection.js' ) );
			$this->assets->load_script( base_url( 'assets/bower_components/jquery-ui/ui/widget.js' ) );
			$this->assets->load_script( base_url( 'assets/bower_components/jquery-ui/ui/widgets/mouse.js' ) );
			$this->assets->load_script( base_url( 'assets/bower_components/jquery-ui/ui/widgets/sortable.js' ) );

			// Inputmask
			$this->assets->load_script( base_url( 'assets/bower_components/inputmask/dist/jquery.inputmask.min.js' ) );

			// jQuery File Uploader assets
			$this->assets->load_style( site_url( 'assets/bower_components/jQuery-File-Upload/css/jquery.fileupload.css' ) );
			$this->assets->load_script( site_url( 'assets/bower_components/jQuery-File-Upload/js/vendor/jquery.ui.widget.js' ) );
			$this->assets->load_script( site_url( 'assets/bower_components/jQuery-File-Upload/js/jquery.iframe-transport.js' ) );
			$this->assets->load_script( site_url( 'assets/bower_components/jQuery-File-Upload/js/jquery.fileupload.js' ) );

			// Select2
			$this->assets->load_style( site_url( 'assets/bower_components/select2/dist/css/select2.min.css' ) );
			$this->assets->load_script( site_url( 'assets/bower_components/select2/dist/js/select2.full.min.js' ) );
			$this->assets->load_script( site_url( 'assets/bower_components/select2/dist/js/i18n/' . substr( $this->assets->conf['language'], 0, 2 ) . '.js' ) );

			$this->assets->load_script( 'forms.min.js' );
			$this->assets->load_script( 'forms-products.min.js' );

			$this->data['tags']['page_title'] = __( "Edit Product" );
			$this->templates->view( 'products/form', $this->data );
		}
		else
		{
			$this->alerts->persist( TRUE )->alert( __( "The product is not stored into the database." ), __( "Product does not exist" ), NULL, 'danger' );
			redirect( 'products/lists' );
		}
	}

	/**
	 * Item brief page
	 * 
	 * @param  int $product_id
	 * @return output
	 */
	public function brief( $product_id )
	{
		$this->data['product'] = $this->products->get( $product_id );

		if ( ! empty( $this->data['product'] ) )
		{
			$this->data['category'] = ( ! empty( $this->data['product']['category_id'] ) ) ? $this->categories->get( $this->data['product']['category_id'] ) : NULL;
			$this->data['category'] = $this->assets->fill_empty_vars( $this->data['category'], 'categories' );
			$this->data['product'] = $this->assets->fill_empty_vars( $this->data['product'] );
			$this->data['tags']['page_title'] = __( "Category Brief" );
			$this->templates->view( 'products/brief', $this->data );
		}
		else
		{
			$this->alerts->persist( TRUE )->alert( __( "The product is not stored into the database." ), __( "Product does not exist" ), NULL, 'danger' );
			redirect( 'products/lists' );			
		}
	}

}