<?php defined( 'BASEPATH' ) OR exit( 'No direct script access allowed' );

class Stock extends CI_Controller
{
	/**
	 * Class constructor
	 */
	public function __construct()
	{
		parent::__construct();

		if ( ! $this->ion_auth->logged_in() ) redirect( 'login', 'refresh' );

		$this->load->model( "Products_model", "products" );
		$this->load->model( "Purchases_model", "purchases" );
		$this->load->model( "Stock_model", "stock" );

		$this->data['tags']['controller_name'] = __( "Stock" );

		/**
		 * -----------------------------------------------------------------------------------------------
		 * BULK ACTIONS FOR SELECT IN LISTS
		 * -----------------------------------------------------------------------------------------------
		 */

		$bulk_options['-1'] = __( "Bulk actions" );
		if ( $this->uri->segment( 2 ) != 'trash' AND $this->permissions->can_trash() ) $bulk_options['trash'] = __( "Move to Trash" );
		if ( $this->uri->segment( 2 ) == 'trash' AND $this->permissions->can_restore() ) $bulk_options['restore'] = __( "Restore" );
		if ( $this->uri->segment( 2 ) == 'trash' AND $this->permissions->can_delete() ) $bulk_options['delete'] = __( "Delete" );
		
		$this->data['bulk_actions'] = array(
			'id'		=> 'bulk_actions',
			'name'		=> 'bulk_actions',
			'class'		=> 'form-control form-control-sm mb-1',
			'options'	=> $bulk_options,
		);

		$this->data['submit_bulk_actions'] = array(
			'id'			=> 'submit_bulk_actions',
			'name'			=> 'submit_bulk_actions',
			'class'			=> 'btn btn-secondary btn-sm btn-bulk mb-1',
			'value'			=> __( "Apply" ),
			'data-confirm'	=> __( "Are you sure you want to perform that action?\n\nIt may be definitive and unable to undo in the future." ),
		);

		/**
		 * -----------------------------------------------------------------------------------------------
		 * LIST TOOLS EXTENSION
		 * -----------------------------------------------------------------------------------------------
		 */
		
		$this->data['list_buttons_extension'] = array(
			'brief' => anchor(
				'stock/brief/%d',
				'<i class="fa fa-search"></i>',
				array(
					'title' => __( "Brief" ),
					'class' => 'brief btn btn-info',
				)
			),
		);
	}

	/**
	 * Index redirecting to lists
	 * 
	 * @return void
	 */
	public function index()
	{
		redirect( 'stock/lists' );
	}

	/**
	 * Items list
	 * 
	 * @return output
	 */
	public function lists()
	{
		if ( $_POST )
		{
			if ( ! empty( $_POST['cb_ids'] ) )
			{
				switch ( $_POST['bulk_actions'] )
				{
					default: break;
					case 'trash': 
						if ( $this->permissions->can_trash() )
						{
							foreach ( $_POST['cb_ids'] AS $id )
							{
								if ( $this->stock->save( array( 'stock_id' => (int)$id, 'trash' => 1 ) ) )
								{
									$this->logs->set_data( array( 'bulk_actions' => $_POST['bulk_actions'], 'cb_id' => $id ), "POST" )->set_result( TRUE )->register();
									$this->alerts->persist( TRUE )->alert( __( "Items were successfully trashed." ), __( "Items trashed" ), NULL, 'success' );
								}
								else
								{
									$this->logs->set_data( array( 'bulk_actions' => $_POST['bulk_actions'], 'cb_id' => $id ), "POST" )->set_result( FALSE )->register();
									$this->alerts->persist( TRUE )->alert( __( "There were some errors while trying to perform the task. Please try again." ), __( "Error" ), NULL, 'danger' );
								}
							}

							redirect( uri_string() );
						}
						else
						{
							$this->alerts->alert( $this->assets->conf['no_permission']['message'], $this->assets->conf['no_permission']['title'], NULL, 'warning' );
						}
					break;
				} 
			}
		}

		if ( isset( $_GET['action'] ) AND isset( $_GET['stock_id'] ) )
		{
			if ( $_GET['action'] == "trash" AND $this->permissions->can_trash() )
			{
				if ( $this->stock->save( array( 'stock_id' => (int)$_GET['stock_id'], 'trash' => 1 ) ) )
				{
					$this->logs->set_data( $_GET, "GET" )->set_result( TRUE )->register();
					$this->alerts->alert( __( "Item was successfully trashed." ), __( "Item trashed" ), NULL, 'success' );
				}
				else
				{
					$this->logs->set_data( $_GET, "GET" )->set_result( FALSE )->register();
					$this->alerts->alert( __( "There were some errors while trying to perform the task. Please try again." ), __( "Error" ), NULL, 'danger' );
				}
			}
			else
			{
				$this->alerts->alert( $this->assets->conf['no_permission']['message'], $this->assets->conf['no_permission']['title'], NULL, 'warning' );
			}
		}

		// Order
		if ( $this->input->get( 'order_by' ) AND $this->input->get( 'direction' ) )
		{
			$query_args['order_by'][$this->input->get( 'order_by' )] = $this->input->get( 'direction' );
		}
		else
		{
			$query_args['order_by']['stock_id'] = 'DESC';
		}

		$query_args['where']['trash'] = 0;		
		$query_args['limit'] = $this->assets->conf['items_per_page'];
		$query_args['offset'] = $this->uri->segment( 3 );
		$this->data['stock'] = $this->stock->get( $query_args );
		$this->data['pagination'] = $this->templates->create_pagination( $this->assets->conf['tables']['stock'], $query_args );
		
		$this->templates->reverse_pagination( $this->data['stock'] );

		$this->assets->load_script( 'lists.min.js' );
		
		$this->data['tags']['page_title'] = __( "Inventory" );
		$this->templates->view( 'stock/lists', $this->data );
	}

	/**
	 * Items trash
	 * 
	 * @return output
	 */
	public function trash()
	{
		$this->permissions->block_not_admin();
		
		if ( $_POST )
		{
			if ( ! empty( $_POST['cb_ids'] ) )
			{
				switch ( $_POST['bulk_actions'] )
				{
					default: break;
					case 'restore': 
						if ( $this->permissions->can_restore() )
						{
							foreach ( $_POST['cb_ids'] AS $id )
							{
								if ( $this->stock->save( array( 'stock_id' => (int)$id, 'trash' => 0 ) ) )
								{
									$this->logs->set_data( array( 'bulk_actions' => $_POST['bulk_actions'], 'cb_id' => $id ), "POST" )->set_result( TRUE )->register();
									$this->alerts->persist( TRUE )->alert( __( "Items were successfully restored." ), __( "Items restored" ), NULL, 'success' );
								}
								else
								{
									$this->logs->set_data( array( 'bulk_actions' => $_POST['bulk_actions'], 'cb_id' => $id ), "POST" )->set_result( FALSE )->register();
									$this->alerts->persist( TRUE )->alert( __( "There were some errors while trying to perform the task. Please try again." ), __( "Error" ), NULL, 'danger' );
								}
							}

							redirect( uri_string() );
						}
						else
						{
							$this->alerts->alert( $this->assets->conf['no_permission']['message'], $this->assets->conf['no_permission']['title'], NULL, 'warning' );
						}
					break;
					case 'delete':
						if ( $this->permissions->can_delete() )
						{
							foreach ( $_POST['cb_ids'] AS $id )
							{
								if ( $this->stock->erase( (int)$id ) )
								{
									$this->logs->set_data( array( 'bulk_actions' => $_POST['bulk_actions'], 'cb_id' => $id ), "POST" )->set_result( TRUE )->register();
									$this->alerts->persist( TRUE )->alert( __( "Items were successfully deleted." ), __( "Items deleted" ), NULL, 'success' );
								}
								else
								{
									$this->logs->set_data( array( 'bulk_actions' => $_POST['bulk_actions'], 'cb_id' => $id ), "POST" )->set_result( FALSE )->register();
									$this->alerts->persist( TRUE )->alert( __( "There were some errors while trying to perform the task. Please try again." ), __( "Error" ), NULL, 'danger' );
								}
							}

							redirect( uri_string() );
						}
						else
						{
							$this->alerts->alert( $this->assets->conf['no_permission']['message'], $this->assets->conf['no_permission']['title'], NULL, 'warning' );
						}
					break;
				} 
			}
		}

		if ( isset( $_GET['action'] ) AND isset( $_GET['stock_id'] ) )
		{
			if ( $_GET['action'] == "restore" AND $this->permissions->can_restore() )
			{
				if ( $this->stock->save( array( 'stock_id' => (int)$_GET['stock_id'], 'trash' => 0 ) ) )
				{
					$this->logs->set_data( $_GET, "GET" )->set_result( TRUE )->register();
					$this->alerts->alert( __( "Item successfully restored." ), __( "Item restored" ), NULL, 'success' );
				}
				else
				{
					$this->logs->set_data( $_GET, "GET" )->set_result( FALSE )->register();
					$this->alerts->alert( __( "There were some errors while trying to perform the task. Please try again." ), __( "Error" ), NULL, 'danger' );				
				}
			}
			else if ( $_GET['action'] == "delete" AND $this->permissions->can_delete() )
			{
				if ( $this->stock->erase( (int)$_GET['stock_id'] ) )
				{
					$this->logs->set_data( $_GET, "GET" )->set_result( TRUE )->register();
					$this->alerts->alert( __( "Item successfully deleted." ), __( "Item deleted" ), NULL, 'success' );
				}
				else
				{
					$this->logs->set_data( $_GET, "GET" )->set_result( FALSE )->register();
					$this->alerts->alert( __( "There were some errors while trying to perform the task. Please try again." ), __( "Error" ), NULL, 'danger' );				
				}
			}
			else
			{
				$this->alerts->alert( $this->assets->conf['no_permission']['message'], $this->assets->conf['no_permission']['title'], NULL, 'warning' );
			}
		}

		// Order
		if ( $this->input->get( 'order_by' ) AND $this->input->get( 'direction' ) )
		{
			$query_args['order_by'][$this->input->get( 'order_by' )] = $this->input->get( 'direction' );
		}
		else
		{
			$query_args['order_by']['stock_id'] = 'DESC';
		}

		$query_args['where']['trash'] = 1;
		$query_args['limit'] = $this->assets->conf['items_per_page'];
		$query_args['offset'] = $this->uri->segment( 3 );
		$this->data['stock'] = $this->stock->get( $query_args );
		$this->data['pagination'] = $this->templates->create_pagination( $this->assets->conf['tables']['stock'], $query_args );
		
		$this->templates->reverse_pagination( $this->data['stock'] );

		$this->assets->load_script( 'lists.min.js' );
		
		$this->data['tags']['page_title'] = __( "Stock Trash" );
		$this->templates->view( 'stock/lists', $this->data );
	}

	/**
	 * Items add page (self production)
	 * 
	 * @return output
	 */
	public function self()
	{
		if ( $this->input->post() )
		{
			if ( ! $this->permissions->can_add() )
			{
				$this->alerts->persist( TRUE )->alert( $this->assets->conf['no_permission']['message'], $this->assets->conf['no_permission']['title'], NULL, 'warning' );
				redirect( uri_string() );
			}

			$this->form_validation->set_rules( 'product_id', __( "Product ID" ), 'trim|required' );
			$this->form_validation->set_rules( 'sku', __( "SKU" ), 'trim|required|is_unique[' . $this->assets->conf['tables']['stock'] . '.sku]' );
			$this->form_validation->set_rules( 'unit_price', __( "Sale price" ), 'trim|required|greater_than[0]|' . ( ( $this->assets->conf['decimals'] > 0 ) ? 'decimal' : 'integer' ) );
			$this->form_validation->set_rules( 'quantity', __( "Quantity" ), 'trim|required|greater_than[0]|' . ( ( $this->assets->conf['decimals'] > 0 ) ? 'decimal' : 'integer' ) );

			if ( $this->form_validation->run() )
			{
				if ( ! $this->input->post( 'purchase_quantity' ) ) $_POST['purchase_quantity'] = $this->input->post( 'quantity' );

				if ( $stock_id = $this->stock->save( $this->input->post( NULL, TRUE ) ) )
				{
					$_POST['stock_id'] = $stock_id;
					$this->logs->set_data( $this->input->post(), "POST" )->set_result( TRUE )->register();
					$this->alerts->persist( TRUE )->alert( __( "Item successfully added to the database." ), __( "Product added to stock" ), NULL, 'success' );

					redirect( "stock/edit/" . $stock_id );
				}
				else
				{
					$this->logs->set_data( $this->input->post(), "POST" )->set_result( FALSE )->register();
					$this->alerts->alert( __( "There were some errors while trying to perform the task. Please try again." ), __( "Product discarded" ), NULL, 'danger' );
				}
			}
			else
			{
				$this->alerts->alert( __( "Errors appear below every field." ), __( "Item could not be stored into the database" ), NULL, 'danger' );
			}
		}

		$this->data['product_id'] = array(
			'id'		=> 'product_id',
			'name'		=> 'product_id',
			'class'		=> 'form-control',
			'style'		=> 'width: 100%',
			'selected'	=> set_value( 'product_id' ),
			'options'	=> $this->products->build_dropdown(),
		);

		$this->data['sku'] = array(
			'type'			=> 'text',
			'id'			=> 'sku',
			'name'			=> 'sku',
			'class'			=> 'form-control form-control-lg input-sku',
			'value'			=> set_value( 'sku' ),
			'autocomplete'	=> 'off',
		);

		$this->data['expiration_date'] = array(
			'type'		=> 'text',
			'id'		=> 'expiration_date',
			'name'		=> 'expiration_date',
			'class'		=> 'form-control input-date',
			'value'		=> set_value( 'expiration_date' ),
		);

		$this->data['init_quantity'] = array(
			'type'		=> 'text',
			'id'		=> 'init_quantity',
			'name'		=> 'init_quantity',
			'class'		=> 'form-control input-quantity',
			'value'		=> set_value( 'init_quantity' ),
		);

		$this->data['quantity'] = array(
			'type'		=> 'text',
			'id'		=> 'quantity',
			'name'		=> 'quantity',
			'class'		=> 'form-control input-quantity',
			'value'		=> set_value( 'quantity' ),
		);

		$this->data['unit_price'] = array(
			'type'		=> 'text',
			'id'		=> 'unit_price',
			'name'		=> 'unit_price',
			'class'		=> 'form-control input-currency',
			'value'		=> set_value( 'unit_price' ),
		);

		// MODAL - ADD NEW PRODUCT ------------------------------------------

		$this->data['modal_new_product_name'] = array(
			'type'			=> 'text',
			'id'			=> 'modal_new_product_name',
			'name'			=> 'modal_new_product_name',
			'class'			=> 'form-control form-control-lg',
			'autocomplete'	=> 'off',
		);

		$this->data['modal_new_product_category_id'] = array(
			'id'		=> 'modal_new_product_category_id',
			'name'		=> 'modal_new_product_category_id',
			'class'		=> 'form-control',
			'style'		=> 'width: 100%',
			'options'	=> $this->categories->build_dropdown(),
		);

		$this->data['modal_new_product_unit_price'] = array(
			'type'			=> 'text',
			'id'			=> 'modal_new_product_unit_price',
			'name'			=> 'modal_new_product_unit_price',
			'class'			=> 'form-control input-currency',
			'autocomplete'	=> 'off',
		);

		$this->data['modal_new_product_unit_measure'] = array(
			'id'		=> 'modal_new_product_unit_measure',
			'name'		=> 'modal_new_product_unit_measure',
			'class'		=> 'form-control',
			'style'		=> 'width: 100%',
			'options'	=> array_merge( array( '' => '&nbsp;' ), $this->assets->conf['units'] ),
		);

		$this->data['modal_new_product_currency'] = array(
			'type'		=> 'hidden',
			'id'		=> 'modal_new_product_currency',
			'name'		=> 'modal_new_product_currency',
			'value'		=> $this->assets->conf['currency'],
		);

		$this->data['modal_new_product_tax'] = array(
			'id'		=> 'modal_new_product_tax',
			'name'		=> 'modal_new_product_tax',
			'class'		=> 'form-control',
			'style'		=> 'width: 100%',
			'options'	=> array_merge( array( '' => '&nbsp;' ), $this->assets->conf['bill_columns'] ),
		);

		// Pickadate
		$this->assets->load_style( base_url( 'assets/bower_components/pickadate/lib/compressed/themes/default.css' ) );
		$this->assets->load_style( base_url( 'assets/bower_components/pickadate/lib/compressed/themes/default.date.css' ) );
		$this->assets->load_script( base_url( 'assets/bower_components/pickadate/lib/compressed/picker.js' ) );
		$this->assets->load_script( base_url( 'assets/bower_components/pickadate/lib/compressed/picker.date.js' ) );	
		if ( $this->assets->conf['language'] !== "en_US" ) $this->assets->load_script( base_url( 'assets/bower_components/pickadate/lib/translations/' . $this->assets->conf['language'] . '.js' ) );

		// Inputmask
		$this->assets->load_script( base_url( 'assets/bower_components/inputmask/dist/jquery.inputmask.min.js' ) );

		// Select2
		$this->assets->load_style( site_url( 'assets/bower_components/select2/dist/css/select2.min.css' ) );
		$this->assets->load_script( site_url( 'assets/bower_components/select2/dist/js/select2.full.min.js' ) );
		$this->assets->load_script( site_url( 'assets/bower_components/select2/dist/js/i18n/' . substr( $this->assets->conf['language'], 0, 2 ) . '.js' ) );

		$this->assets->load_script( 'forms.min.js' );
		$this->assets->load_script( 'forms-stock.min.js' );

		$this->data['tags']['page_title'] = __( "Add Self-Production To Stock" );
		$this->templates->view( 'stock/form-self', $this->data );
	}

	/**
	 * Items add page (from purchase)
	 * 
	 * @param int $purchase_id The purchase ID
	 * @return output
	 */
	public function add( $purchase_id )
	{
		if ( ! $this->permissions->can_add() )
		{
			$this->alerts->persist( TRUE )->alert( $this->assets->conf['no_permission']['message'], $this->assets->conf['no_permission']['title'], NULL, 'warning' );
			redirect( 'purchases/lists' );
		}

		$this->data['purchase'] = $this->assets->fill_empty_vars( $this->purchases->get( $purchase_id ), 'purchases' );

		if ( ! empty( $this->data['purchase'] ) )
		{
			$this->data['meta'] = $this->purchases->meta_get( $this->data['purchase']['purchase_id'], array( 'where' => array( 'stocked' => '0' ) ) );

			$this->data['purchase_id'] = array(
				'type'		=> 'hidden',
				'id'		=> 'purchase_id',
				'name'		=> 'purchase_id',
				'value'		=> $this->data['purchase']['purchase_id'],
			);

			$this->data['modal_price'] = array(
				'type'		=> 'text',
				'id'		=> 'modal_price',
				'name'		=> 'modal_price',
				'class'		=> 'form-control input-currency',
			);

			$this->data['modal_quantity'] = array(
				'type'		=> 'text',
				'id'		=> 'modal_quantity',
				'name'		=> 'modal_quantity',
				'class'		=> 'form-control input-quantity',
			);

			$this->data['modal_expiration_date'] = array(
				'type'		=> 'text',
				'id'		=> 'modal_expiration_date',
				'name'		=> 'modal_expiration_date',
				'class'		=> 'form-control input-date',
			);

			// Pickadate
			$this->assets->load_style( base_url( 'assets/bower_components/pickadate/lib/compressed/themes/default.css' ) );
			$this->assets->load_style( base_url( 'assets/bower_components/pickadate/lib/compressed/themes/default.date.css' ) );
			$this->assets->load_script( base_url( 'assets/bower_components/pickadate/lib/compressed/picker.js' ) );
			$this->assets->load_script( base_url( 'assets/bower_components/pickadate/lib/compressed/picker.date.js' ) );	
			if ( $this->assets->conf['language'] !== "en_US" ) $this->assets->load_script( base_url( 'assets/bower_components/pickadate/lib/translations/' . $this->assets->conf['language'] . '.js' ) );

			// Inputmask
			$this->assets->load_script( base_url( 'assets/bower_components/inputmask/dist/jquery.inputmask.min.js' ) );

			$this->assets->load_script( 'forms.min.js' );
			$this->assets->load_script( 'forms-stock.min.js' );

			$this->data['tags']['page_title'] = __( "Add To Stock" );
			$this->templates->view( 'stock/form-add', $this->data );
		}
		else
		{
			$this->alerts->persist( TRUE )->alert( __( "The purchase is not stored into the database." ), __( "Purchase does not exist" ), NULL, 'danger' );
			redirect( 'purchases/lists' );
		}
	}

	/**
	 * Items edit page
	 * 
	 * @param int $stock_id The stock ID
	 * @return output
	 */
	public function edit( $stock_id )
	{
		if ( $this->input->post() )
		{
			if ( ! $this->permissions->can_edit() )
			{
				$this->alerts->persist( TRUE )->alert( $this->assets->conf['no_permission']['message'], $this->assets->conf['no_permission']['title'], NULL, 'warning' );
				redirect( uri_string() );
			}

			$this->form_validation->set_rules( 'stock_id', __( "Stock ID" ), 'integer|required' );

			if ( $this->form_validation->run() )
			{
				if ( $this->stock->save( $this->input->post( NULL, TRUE ) ) )
				{
					$this->logs->set_data( $this->input->post(), "POST" )->set_result( TRUE )->register();
					$this->alerts->persist( TRUE )->alert( __( "Item was successfully updated into the stock." ), __( "Product updated" ), NULL, 'success' );
					redirect( "stock/edit/" . $stock_id );
				}
				else
				{
					$this->logs->set_data( $this->input->post(), "POST" )->set_result( FALSE )->register();
					$this->alerts->alert( __( "There were some errors while trying to perform the task. Please try again." ), __( "Product not updated" ), NULL, 'danger' );
				}
			}
			else
			{
				$this->alerts->alert( __( "Errors appear below every field." ), __( "Item could not be stored into the stock." ), NULL, 'danger' );
			}
		}

		$this->data['stock'] = $this->stock->get( $stock_id, 'stock_id' );

		if ( ! empty( $this->data['stock'] ) )
		{
			$this->data['stock_id'] = array(
				'type'		=> 'hidden',
				'id'		=> 'stock_id',
				'name'		=> 'stock_id',
				'value'		=> $this->data['stock']['stock_id'],
			);

			$this->data['sku'] = array(
				'type'			=> 'text',
				'id'			=> 'sku',
				'name'			=> 'sku',
				'class'			=> 'form-control input-sku',
				'value'			=> $this->data['stock']['sku'],
				'autocomplete'	=> 'off',
			);

			$this->data['expiration_date'] = array(
				'type'		=> 'text',
				'id'		=> 'expiration_date',
				'name'		=> 'expiration_date',
				'class'		=> 'form-control input-date',
				'value'		=> ( ! empty( $this->data['stock']['expiration_date'] ) ) ? date( $this->assets->conf['date_format'], strtotime( $this->data['stock']['expiration_date'] ) ) : NULL,
			);

			$this->data['init_quantity'] = array(
				'type'		=> 'text',
				'id'		=> 'init_quantity',
				'name'		=> 'init_quantity',
				'class'		=> 'form-control input-quantity',
				'value'		=> my_number_format( $this->data['stock']['init_quantity'], $this->assets->conf['qty_decimals'] ),
			);

			$this->data['quantity'] = array(
				'type'		=> 'text',
				'id'		=> 'quantity',
				'name'		=> 'quantity',
				'class'		=> 'form-control input-quantity',
				'value'		=> my_number_format( $this->data['stock']['quantity'], $this->assets->conf['qty_decimals'] ),
			);

			$this->data['unit_price'] = array(
				'type'		=> 'text',
				'id'		=> 'unit_price',
				'name'		=> 'unit_price',
				'class'		=> 'form-control input-currency',
				'value'		=> my_number_format( $this->data['stock']['unit_price'] ),
			);

			$this->data['stock'] = $this->assets->fill_empty_vars( $this->data['stock'], 'stock' );
			$this->data['purchase'] = $this->assets->fill_empty_vars( $this->purchases->get( $this->data['stock']['purchase_id'] ), 'purchases' );
			$this->data['product'] = $this->assets->fill_empty_vars( $this->products->get( $this->data['stock']['product_id'] ), 'products' );

			// Pickadate
			$this->assets->load_style( base_url( 'assets/bower_components/pickadate/lib/compressed/themes/default.css' ) );
			$this->assets->load_style( base_url( 'assets/bower_components/pickadate/lib/compressed/themes/default.date.css' ) );
			$this->assets->load_script( base_url( 'assets/bower_components/pickadate/lib/compressed/picker.js' ) );
			$this->assets->load_script( base_url( 'assets/bower_components/pickadate/lib/compressed/picker.date.js' ) );	
			if ( $this->assets->conf['language'] !== "en_US" ) $this->assets->load_script( base_url( 'assets/bower_components/pickadate/lib/translations/' . $this->assets->conf['language'] . '.js' ) );

			// Inputmask
			$this->assets->load_script( base_url( 'assets/bower_components/inputmask/dist/jquery.inputmask.min.js' ) );

			$this->assets->load_script( 'forms.min.js' );
			$this->assets->load_script( 'forms-stock.min.js' );

			$this->data['tags']['page_title'] = __( "Edit Stocked Product" );
			$this->templates->view( 'stock/form-edit', $this->data );

		}
		else
		{
			$this->alerts->persist( TRUE )->alert( __( "The product is not stored into the stock." ), __( "Product does not exist" ), NULL, 'danger' );
			redirect( 'stock/lists' );
		}
	}

	/**
	 * Item brief page
	 * 
	 * @param  int $stock_id The stock ID
	 * @return output
	 */
	public function brief( $stock_id )
	{
		$this->load->helper( 'date' );
		$this->data['stock'] = $this->stock->get( $stock_id, 'stock_id' );

		if ( ! empty( $this->data['stock'] ) )
		{
			$this->data['stock'] 	= $this->assets->fill_empty_vars( $this->data['stock'], 'stock' );
			$this->data['product'] 	= $this->assets->fill_empty_vars( $this->products->get( $this->data['stock']['product_id'] ), 'products' );
			$this->data['purchase'] = $this->assets->fill_empty_vars( $this->purchases->get( $this->data['stock']['purchase_id'] ), 'purchases' );

			// Estimates
			$this->data['init_quantity']		= (float)$this->data['stock']['init_quantity'];
			$this->data['purchased_quantity'] 	= (float)( (float)$this->data['stock']['purchase_quantity'] );
			$this->data['investment'] 			= (float)( $this->data['purchased_quantity'] * (float)$this->data['stock']['purchase_price'] );
			$this->data['sales'] 				= (float)( $this->data['init_quantity'] * (float)$this->data['stock']['unit_price'] );
			$this->data['profit'] 				= (float)( $this->data['sales'] - $this->data['investment'] );
			$this->data['percentage'] 			= (float)( ( $this->data['profit'] * 100 ) / $this->data['sales'] );

			$this->data['tags']['page_title'] = __( "Product Brief" );
			$this->templates->view( 'stock/brief', $this->data );
		}
		else
		{
			$this->alerts->persist( TRUE )->alert( __( "The product is not stored into the stock." ), __( "Product does not exist" ), NULL, 'danger' );
			redirect( 'stock/lists' );
		}

	}
}