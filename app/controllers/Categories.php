<?php defined( 'BASEPATH' ) OR exit( 'No direct script access allowed' );

class Categories extends CI_Controller
{
	/**
	 * Class constructor
	 */
	public function __construct()
	{
		parent::__construct();

		if ( ! $this->ion_auth->logged_in() ) redirect( 'login', 'refresh' );

		$this->load->model( "Categories_model", "categories" );

		$this->data['tags']['controller_name'] = __( "Categories" );

		/**
		 * -----------------------------------------------------------------------------------------------
		 * BULK ACTIONS FOR SELECT IN LISTS
		 * -----------------------------------------------------------------------------------------------
		 */

		$bulk_options['-1'] = __( "Bulk actions" );
		if ( $this->uri->segment( 2 ) != 'trash' AND $this->permissions->can_trash() ) $bulk_options['trash'] = __( "Move to Trash" );
		if ( $this->uri->segment( 2 ) == 'trash' AND $this->permissions->can_restore() ) $bulk_options['restore'] = __( "Restore" );
		if ( $this->uri->segment( 2 ) == 'trash' AND $this->permissions->can_delete() ) $bulk_options['delete'] = __( "Delete" );
		
		$this->data['bulk_actions'] = array(
			'id'		=> 'bulk_actions',
			'name'		=> 'bulk_actions',
			'class'		=> 'form-control form-control-sm mb-1',
			'options'	=> $bulk_options,
		);

		$this->data['submit_bulk_actions'] = array(
			'id'			=> 'submit_bulk_actions',
			'name'			=> 'submit_bulk_actions',
			'class'			=> 'btn btn-secondary btn-sm btn-bulk mb-1',
			'value'			=> __( "Apply" ),
			'data-confirm'	=> __( "Are you sure you want to perform that action?\n\nIt may be definitive and unable to undo in the future." ),
		);

		/**
		 * -----------------------------------------------------------------------------------------------
		 * LIST TOOLS EXTENSION
		 * -----------------------------------------------------------------------------------------------
		 */
		
		$this->data['list_buttons_extension'] = array(
			'brief' => anchor(
				'categories/brief/%d',
				'<i class="fa fa-search"></i>',
				array(
					'title' 				=> __( "Brief" ),
					'class' 				=> 'brief btn btn-info',
					// 'data-enlarge-modal' 	=> 1,
				)
			),
			'see_all' => anchor(
				'products/lists?category_id=%d',
				'<i class="fa fa-search"></i>',
				array(
					'title' => __( "See all products" ),
					'class' => 'btn btn-info',
				)
			),
		);
	}

	/**
	 * Index redirecting to lists
	 * 
	 * @return void
	 */
	public function index()
	{
		redirect( 'categories/lists' );
	}

	/**
	 * Items list
	 * 
	 * @return output
	 */
	public function lists()
	{
		if ( $_POST )
		{
			if ( ! empty( $_POST['cb_ids'] ) )
			{
				switch ( $_POST['bulk_actions'] )
				{
					default: break;
					case 'trash': 
						if ( $this->permissions->can_trash() )
						{
							foreach ( $_POST['cb_ids'] AS $id )
							{
								if ( $this->categories->save( array( 'category_id' => (int)$id, 'trash' => 1 ) ) )
								{
									$this->logs->set_data( array( 'bulk_actions' => $_POST['bulk_actions'], 'cb_id' => $id ), "POST" )->set_result( TRUE )->register();
									$this->alerts->persist( TRUE )->alert( __( "Items were successfully trashed." ), __( "Items trashed" ), NULL, 'success' );
								}
								else
								{
									$this->logs->set_data( array( 'bulk_actions' => $_POST['bulk_actions'], 'cb_id' => $id ), "POST" )->set_result( FALSE )->register();
									$this->alerts->persist( TRUE )->alert( __( "There were some errors while trying to perform the task. Please try again." ), __( "Error" ), NULL, 'danger' );
								}
							}

							redirect( uri_string() );
						}
						else
						{
							$this->alerts->alert( $this->assets->conf['no_permission']['message'], $this->assets->conf['no_permission']['title'], NULL, 'warning' );
						}
					break;
				} 
			}
		}

		if ( isset( $_GET['action'] ) AND isset( $_GET['category_id'] ) )
		{
			if ( $_GET['action'] == "trash" AND $this->permissions->can_trash() )
			{
				if ( $this->categories->save( array( 'category_id' => (int)$_GET['category_id'], 'trash' => 1 ) ) )
				{
					$this->logs->set_data( $_GET, "GET" )->set_result( TRUE )->register();
					$this->alerts->alert( __( "Item was successfully trashed." ), __( "Item trashed" ), NULL, 'success' );
				}
				else
				{
					$this->logs->set_data( $_GET, "GET" )->set_result( FALSE )->register();
					$this->alerts->alert( __( "There were some errors while trying to perform the task. Please try again." ), __( "Error" ), NULL, 'danger' );
				}
			}
			else
			{
				$this->alerts->alert( $this->assets->conf['no_permission']['message'], $this->assets->conf['no_permission']['title'], NULL, 'warning' );
			}
		}

		// Order
		if ( $this->input->get( 'order_by' ) AND $this->input->get( 'direction' ) )
		{
			$query_args['order_by'][$this->input->get( 'order_by' )] = $this->input->get( 'direction' );
		}
		else
		{
			$query_args['order_by']['category_id'] = 'DESC';
		}

		$query_args['where']['trash'] = 0;
		$query_args['limit'] = $this->assets->conf['items_per_page'];
		$query_args['offset'] = $this->uri->segment( 3 );
		$this->data['categories'] = $this->categories->get( $query_args );
		$this->data['pagination'] = $this->templates->create_pagination( $this->assets->conf['tables']['categories'], $query_args );

		$this->templates->reverse_pagination( $this->data['categories'] );
		
		$this->assets->load_script( 'lists.min.js' );
		
		$this->data['tags']['page_title'] = __( "Categories List" );
		$this->templates->view( 'categories/lists', $this->data );
	}

	/**
	 * Items trash
	 * 
	 * @return output
	 */
	public function trash()
	{
		$this->permissions->block_not_admin();
		
		if ( $_POST )
		{
			if ( ! empty( $_POST['cb_ids'] ) )
			{
				switch ( $_POST['bulk_actions'] )
				{
					default: break;
					case 'restore': 
						if ( $this->permissions->can_restore() )
						{
							foreach ( $_POST['cb_ids'] AS $id )
							{
								if ( $this->categories->save( array( 'category_id' => $id, 'trash' => 0 ) ) )
								{
									$this->logs->set_data( array( 'bulk_actions' => $_POST['bulk_actions'], 'cb_id' => $id ), "POST" )->set_result( TRUE )->register();
									$this->alerts->persist( TRUE )->alert( __( "Items were successfully restored." ), __( "Items restored" ), NULL, 'success' );
								}
								else
								{
									$this->logs->set_data( array( 'bulk_actions' => $_POST['bulk_actions'], 'cb_id' => $id ), "POST" )->set_result( FALSE )->register();
									$this->alerts->persist( TRUE )->alert( __( "There were some errors while trying to perform the task. Please try again." ), __( "Error" ), NULL, 'danger' );
								}
							}

							redirect( uri_string() );
						}
						else
						{
							$this->alerts->alert( $this->assets->conf['no_permission']['message'], $this->assets->conf['no_permission']['title'], NULL, 'warning' );
						}
					break;
					case 'delete':
						if ( $this->permissions->can_delete() )
						{
							foreach ( $_POST['cb_ids'] AS $id )
							{
								if ( $this->categories->erase( array( 'category_id' => $id ) ) )
								{
									$this->logs->set_data( array( 'bulk_actions' => $_POST['bulk_actions'], 'cb_id' => $id ), "POST" )->set_result( TRUE )->register();
									$this->alerts->persist( TRUE )->alert( __( "Items were successfully deleted." ), __( "Items deleted" ), NULL, 'success' );
								}
								else
								{
									$this->logs->set_data( array( 'bulk_actions' => $_POST['bulk_actions'], 'cb_id' => $id ), "POST" )->set_result( FALSE )->register();
									$this->alerts->persist( TRUE )->alert( __( "There were some errors while trying to perform the task. Please try again." ), __( "Error" ), NULL, 'danger' );
								}
							}

							redirect( uri_string() );
						}
						else
						{
							$this->alerts->alert( $this->assets->conf['no_permission']['message'], $this->assets->conf['no_permission']['title'], NULL, 'warning' );
						}
					break;
				} 
			}
		}

		if ( isset( $_GET['action'] ) AND isset( $_GET['category_id'] ) )
		{
			if ( $_GET['action'] == "restore" AND $this->permissions->can_restore() )
			{
				if ( $this->categories->save( array( 'category_id' => (int)$_GET['category_id'], 'trash' => 0 ) ) )
				{
					$this->logs->set_data( $_GET, "GET" )->set_result( TRUE )->register();
					$this->alerts->alert( __( "Item successfully restored." ), __( "Item restored" ), NULL, 'success' );
				}
				else
				{
					$this->logs->set_data( $_GET, "GET" )->set_result( FALSE )->register();
					$this->alerts->alert( __( "There were some errors while trying to perform the task. Please try again." ), __( "Error" ), NULL, 'danger' );				
				}
			}
			else if ( $_GET['action'] == "delete" AND $this->permissions->can_delete() )
			{
				if ( $this->categories->erase( (int)$_GET['category_id'] ) )
				{
					$this->logs->set_data( $_GET, "GET" )->set_result( TRUE )->register();
					$this->alerts->alert( __( "Item successfully deleted." ), __( "Item deleted" ), NULL, 'success' );
				}
				else
				{
					$this->logs->set_data( $_GET, "GET" )->set_result( FALSE )->register();
					$this->alerts->alert( __( "There were some errors while trying to perform the task. Please try again." ), __( "Error" ), NULL, 'danger' );				
				}
			}
			else
			{
				$this->alerts->alert( $this->assets->conf['no_permission']['message'], $this->assets->conf['no_permission']['title'], NULL, 'warning' );
			}
		}

		// Order
		if ( $this->input->get( 'order_by' ) AND $this->input->get( 'direction' ) )
		{
			$query_args['order_by'][$this->input->get( 'order_by' )] = $this->input->get( 'direction' );
		}
		else
		{
			$query_args['order_by']['category_id'] = 'DESC';
		}

		$query_args['where']['trash'] = 1;
		$query_args['limit'] = $this->assets->conf['items_per_page'];
		$query_args['offset'] = $this->uri->segment( 3 );
		$this->data['categories'] = $this->categories->get( $query_args );
		$this->data['pagination'] = $this->templates->create_pagination( $this->assets->conf['tables']['categories'], $query_args );

		$this->templates->reverse_pagination( $this->data['categories'] );
		
		$this->assets->load_script( 'lists.min.js' );
		
		$this->data['tags']['page_title'] = __( "Categories Trash" );
		$this->templates->view( 'categories/lists', $this->data );
	}

	/**
	 * Item add page
	 * 
	 * @return output
	 */
	public function add()
	{
		if ( $this->input->post() )
		{
			if ( ! $this->permissions->can_add() )
			{
				$this->alerts->persist( TRUE )->alert( $this->assets->conf['no_permission']['message'], $this->assets->conf['no_permission']['title'], NULL, 'warning' );
				redirect( uri_string() );
			}

			$this->form_validation->set_rules( 'name', __( "Name" ), 'trim|required' );

			if ( $this->form_validation->run() )
			{
				if ( $category_id = $this->categories->save( $this->input->post( NULL, TRUE ) ) )
				{
					// Associate in case there were any uploaded files
					if ( $this->input->post( 'file_id' ) )
					{
						$this->load->model( "Files_model", "files" );
						$this->files->save( array(
							'file_id' => $this->input->post( 'file_id' ),
							'registry_id' => $category_id,
						) );
					}

					$_POST['category_id'] = $category_id;
					$this->logs->set_data( $this->input->post(), "POST" )->set_result( TRUE )->register();
					$this->alerts->persist( TRUE )->alert( __( "Item successfully added to the database." ), __( "Category created" ), NULL, 'success' );

					redirect( "categories/edit/" . $category_id );
				}
				else
				{
					$this->logs->set_data( $this->input->post(), "POST" )->set_result( FALSE )->register();
					$this->alerts->alert( __( "There were some errors while trying to perform the task. Please try again." ), __( "Category discarded" ), NULL, 'danger' );
				}
			}
			else
			{
				$this->alerts->alert( __( "Errors appear below every field." ), __( "Item could not be stored into the database" ), NULL, 'danger' );
			}
		}

		$this->data['name'] = array(
			'type'		=> 'text',
			'id'		=> 'name',
			'name'		=> 'name',
			'class'		=> 'form-control form-control-lg',
			'value'		=> set_value( 'name' ),
		);

		$this->data['description'] = array(
			'id'			=> 'description',
			'name'			=> 'description',
			'class'			=> 'form-control',
			'rows'			=> 3,
			'placeholder'	=> __( "Use this field to store any other information about the category..." ),
			'value'			=> set_value( 'description' ),
		);

		$this->data['unit_quantity'] = array(
			'type'		=> 'text',
			'id'		=> 'unit_quantity',
			'name'		=> 'unit_quantity',
			'class'		=> 'form-control input-quantity',
			'value'		=> set_value( 'unit_quantity' ),
		);

		$this->data['unit_measure'] = array(
			'id'		=> 'unit_measure',
			'name'		=> 'unit_measure',
			'class'		=> 'form-control',
			'selected'	=> set_value( 'unit_measure' ),
			'options'	=> $this->assets->conf['units'],
		);

		$this->data['tax'] = array(
			'id'		=> 'tax',
			'name'		=> 'tax',
			'class'		=> 'form-control',
			'selected'	=> set_value( 'tax' ),
			'options'	=> array_merge( array( '' => '' ), $this->assets->conf['bill_columns'] ),
		);

		$this->data['currency'] = array(
			'id'		=> 'currency',
			'name'		=> 'currency',
			'class'		=> 'form-control',
			'selected'	=> set_value( 'currency' ),
			'options'	=> $this->assets->conf['currencies_ext'],
		);

		// Inputmask
		$this->assets->load_script( base_url( 'assets/bower_components/inputmask/dist/jquery.inputmask.min.js' ) );

		// jQuery File Uploader assets
		$this->assets->load_style( site_url( 'assets/bower_components/jQuery-File-Upload/css/jquery.fileupload.css' ) );
		$this->assets->load_script( site_url( 'assets/bower_components/jQuery-File-Upload/js/vendor/jquery.ui.widget.js' ) );
		$this->assets->load_script( site_url( 'assets/bower_components/jQuery-File-Upload/js/jquery.iframe-transport.js' ) );
		$this->assets->load_script( site_url( 'assets/bower_components/jQuery-File-Upload/js/jquery.fileupload.js' ) );

		$this->assets->load_script( 'forms.min.js' );
		$this->assets->load_script( 'forms-categories.min.js' );

		$this->data['tags']['page_title'] = __( "Add Category" );
		$this->templates->view( 'categories/form', $this->data );
	}

	/**
	 * Item edit page
	 * 
	 * @param  int $category_id
	 * @return output
	 */
	public function edit( $category_id )
	{
		if ( $this->input->post() )
		{
			if ( ! $this->permissions->can_edit() )
			{
				$this->alerts->persist( TRUE )->alert( $this->assets->conf['no_permission']['message'], $this->assets->conf['no_permission']['title'], NULL, 'warning' );
				redirect( uri_string() );
			}

			$this->form_validation->set_rules( 'category_id', __( "Category ID" ), 'integer|required' );
			$this->form_validation->set_rules( 'name', __( "Name" ), 'trim|required' );

			if ( $this->form_validation->run() )
			{
				if ( $this->categories->save( $this->input->post( NULL, TRUE ) ) )
				{
					$this->logs->set_data( $this->input->post(), "POST" )->set_result( TRUE )->register();
					$this->alerts->persist( TRUE )->alert( __( "Item was successfully updated into the database." ), __( "Category updated" ), NULL, 'success' );
					redirect( "categories/edit/" . $category_id );
				}
				else
				{
					$this->logs->set_data( $this->input->post(), "POST" )->set_result( FALSE )->register();
					$this->alerts->alert( __( "There were some errors while trying to perform the task. Please try again." ), __( "Category not updated" ), NULL, 'danger' );
				}
			}
			else
			{
				$this->alerts->alert( __( "Errors appear below every field." ), __( "Item could not be stored into the database" ), NULL, 'danger' );
			}
		}

		$this->data['category'] = $this->categories->get( $category_id );

		if ( ! empty( $this->data['category'] ) )
		{
			$this->data['category_id'] = array(
				'type'		=> 'hidden',
				'id'		=> 'category_id',
				'name'		=> 'category_id',
				'value'		=> $this->data['category']['category_id'],
			);

			$this->data['name'] = array(
				'type'		=> 'text',
				'id'		=> 'name',
				'name'		=> 'name',
				'class'		=> 'form-control form-control-lg',
				'value'		=> $this->data['category']['name'],
			);

			$this->data['description'] = array(
				'id'			=> 'description',
				'name'			=> 'description',
				'class'			=> 'form-control',
				'rows'			=> 3,
				'placeholder'	=> __( "Use this field to store any other information about the category..." ),
				'value'			=> $this->data['category']['description'],
			);

			$this->data['unit_quantity'] = array(
				'type'		=> 'text',
				'id'		=> 'unit_quantity',
				'name'		=> 'unit_quantity',
				'class'		=> 'form-control input-quantity',
				'value'		=> my_number_format( $this->data['category']['unit_quantity'], $this->assets->conf['qty_decimals'] ),
			);

			$this->data['unit_measure'] = array(
				'id'		=> 'unit_measure',
				'name'		=> 'unit_measure',
				'class'		=> 'form-control',
				'selected'	=> $this->data['category']['unit_measure'],
				'options'	=> $this->assets->conf['units'],
			);

			$this->data['tax'] = array(
				'id'		=> 'tax',
				'name'		=> 'tax',
				'class'		=> 'form-control',
				'selected'	=> $this->data['category']['tax'],
				'options'	=> array_merge( array( '' => '' ), $this->assets->conf['bill_columns'] ),
			);

			$this->data['currency'] = array(
				'id'		=> 'currency',
				'name'		=> 'currency',
				'class'		=> 'form-control',
				'selected'	=> $this->data['category']['currency'],
				'options'	=> $this->assets->conf['currencies_ext'],
			);

			// Inputmask
			$this->assets->load_script( base_url( 'assets/bower_components/inputmask/dist/jquery.inputmask.min.js' ) );

			// jQuery File Uploader assets
			$this->assets->load_style( site_url( 'assets/bower_components/jQuery-File-Upload/css/jquery.fileupload.css' ) );
			$this->assets->load_script( site_url( 'assets/bower_components/jQuery-File-Upload/js/vendor/jquery.ui.widget.js' ) );
			$this->assets->load_script( site_url( 'assets/bower_components/jQuery-File-Upload/js/jquery.iframe-transport.js' ) );
			$this->assets->load_script( site_url( 'assets/bower_components/jQuery-File-Upload/js/jquery.fileupload.js' ) );

			$this->assets->load_script( 'forms.min.js' );
			$this->assets->load_script( 'forms-categories.min.js' );

			$this->data['tags']['page_title'] = __( "Edit Category" );
			$this->templates->view( 'categories/form', $this->data );
		}
		else
		{
			$this->alerts->persist( TRUE )->alert( __( "The category is not stored into the database." ), __( "Category does not exist" ), NULL, 'danger' );
			redirect( 'categories/lists' );
		}
	}

	/**
	 * Item brief page
	 * 
	 * @param  int $category_id
	 * @return output
	 */
	public function brief( $category_id )
	{
		$this->data['category'] = $this->categories->get( $category_id );

		if ( ! empty( $this->data['category'] ) )
		{
			$this->data['category'] = $this->assets->fill_empty_vars( $this->data['category'] );
			$this->data['tags']['page_title'] = __( "Category Brief" );
			$this->templates->view( 'categories/brief', $this->data );
		}
		else
		{
			$this->alerts->persist( TRUE )->alert( __( "The category is not stored into the database." ), __( "Category does not exist" ), NULL, 'danger' );
			redirect( 'categories/lists' );			
		}
	}

}