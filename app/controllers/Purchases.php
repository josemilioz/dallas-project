<?php defined( 'BASEPATH' ) OR exit( 'No direct script access allowed' );

class Purchases extends CI_Controller 
{
	/**
	 * Class constructor
	 */
	public function __construct()
	{
		parent::__construct();

		if ( ! $this->ion_auth->logged_in() ) redirect( 'login', 'refresh' );

		$this->data['tags']['controller_name'] = __( "Purchases" );

		$this->load->model( "Files_model", "files" );
		$this->load->model( "Purchases_model", "purchases" );
		$this->load->model( "Products_model", "products" );
		$this->load->model( "Movements_model", "movements" );

		/**
		 * -----------------------------------------------------------------------------------------------
		 * BULK ACTIONS FOR SELECT IN LISTS
		 * -----------------------------------------------------------------------------------------------
		 */

		$bulk_options['-1'] = __( "Bulk actions" );
		if ( $this->uri->segment( 2 ) != 'trash' AND $this->permissions->can_trash() ) $bulk_options['trash'] = __( "Move to Trash" );
		if ( $this->uri->segment( 2 ) != 'trash' AND $this->permissions->can_edit() ) $bulk_options['mark_stocked'] = __( "Mark Stocked" );
		if ( $this->uri->segment( 2 ) != 'trash' AND $this->permissions->can_edit() ) $bulk_options['mark_unstocked'] = __( "Mark Unstocked" );
		if ( $this->uri->segment( 2 ) != 'trash' AND $this->permissions->can_edit() ) $bulk_options['mark_paid'] = __( "Mark Paid" );
		if ( $this->uri->segment( 2 ) != 'trash' AND $this->permissions->can_delete() ) $bulk_options['mark_unpaid'] = __( "Mark Unpaid" );

		if ( $this->uri->segment( 2 ) == 'trash' AND $this->permissions->can_restore() ) $bulk_options['restore'] = __( "Restore" );
		if ( $this->uri->segment( 2 ) == 'trash' AND $this->permissions->can_delete() ) $bulk_options['delete'] = __( "Delete" );
		
		$this->data['bulk_actions'] = array(
			'id'		=> 'bulk_actions',
			'name'		=> 'bulk_actions',
			'class'		=> 'form-control form-control-sm mb-1',
			'options'	=> $bulk_options,
		);

		$this->data['submit_bulk_actions'] = array(
			'id'			=> 'submit_bulk_actions',
			'name'			=> 'submit_bulk_actions',
			'class'			=> 'btn btn-secondary btn-sm btn-bulk mb-1',
			'value'			=> __( "Apply" ),
			'data-confirm'	=> __( "Are you sure you want to perform that action?\n\nIt may be definitive and unable to undo in the future." ),
		);

		/**
		 * -----------------------------------------------------------------------------------------------
		 * LIST TOOLS EXTENSION
		 * -----------------------------------------------------------------------------------------------
		 */
		
		$this->data['list_buttons_extension'] = array(
			'brief' => anchor(
				'purchases/brief/%d',
				'<i class="fa fa-search"></i>',
				array(
					'title' => __( "Brief" ),
					'class' => 'brief btn btn-info',
				)
			),
			'mark_paid' => anchor(
				uri_string() . '?action=mark_paid&purchase_id=%d&' . $this->elements->clear_query_string( array( 'action', 'purchase_id' ), $this->input->server( 'QUERY_STRING' ) ),
				__( "Mark Paid" ),
				array( 
					'class' => 'dropdown-item' 
				)
			),
			'mark_unpaid' => anchor(
				uri_string() . '?action=mark_unpaid&purchase_id=%d&' . $this->elements->clear_query_string( array( 'action', 'purchase_id' ), $this->input->server( 'QUERY_STRING' ) ),
				__( "Mark Unpaid" ),
				array( 
					'class' => 'dropdown-item btn-confirm',
					'data-confirm' => __( "Are you sure you want to perform this action? The associated movement will be deleted permanently." ),
				)
			),
			'mark_stocked' => anchor(
				'stock/add/%d',
				__( "To Stock" ),
				array( 
					'class' => 'dropdown-item' 
				)
			),
			'mark_unstocked' => anchor(
				uri_string() . '?action=mark_unstocked&purchase_id=%d&' . $this->elements->clear_query_string( array( 'action', 'purchase_id' ), $this->input->server( 'QUERY_STRING' ) ),
				__( "Mark Unstocked" ),
				array( 
					'class' => 'dropdown-item btn-confirm',
					'data-confirm' => __( "Are you sure you want to perform this action? The items from this purchase that have already been stocked have to be deleted manually." ),
				)
			),
		);
	}

	/**
	 * Index redirecting to lists
	 * 
	 * @return void
	 */
	public function index()
	{
		redirect( 'purchases/lists' );
	}

	/**
	 * Items list
	 * 
	 * @return output
	 */
	public function lists()
	{
		if ( $_POST )
		{
			if ( ! empty( $_POST['cb_ids'] ) )
			{
				switch ( $_POST['bulk_actions'] )
				{
					default: break;
					case 'trash': 
						if ( $this->permissions->can_trash() )
						{
							foreach ( $_POST['cb_ids'] AS $id )
							{
								if ( $this->purchases->save( array( 'purchase_id' => (int)$id, 'trash' => 1 ) ) )
								{
									// Send movements to trash
									if ( $this->purchases->is_paid( (int)$id ) )
									{
										$movement = $this->movements->get( array(
											'where' => array(
												'transaction_type' => 'purchases',
												'transaction_id' => (int)$id,
											)
										) );

										if ( ! empty( $movement[0] ) ) $this->movements->save( array( 'movement_id' => $movement[0]['movement_id'], 'trash' => 1 ) );
									}

									$this->logs->set_data( array( 'bulk_actions' => $_POST['bulk_actions'], 'cb_id' => $id ), "POST" )->set_result( TRUE )->register();
									$this->alerts->persist( TRUE )->alert( __( "Items were successfully trashed." ), __( "Items trashed" ), NULL, 'success' );
								}
								else
								{
									$this->logs->set_data( array( 'bulk_actions' => $_POST['bulk_actions'], 'cb_id' => $id ), "POST" )->set_result( FALSE )->register();
									$this->alerts->persist( TRUE )->alert( __( "There were some errors while trying to perform the task. Please try again." ), __( "Error" ), NULL, 'danger' );
								}
							}

							redirect( uri_string() );
						}
						else
						{
							$this->alerts->alert( $this->assets->conf['no_permission']['message'], $this->assets->conf['no_permission']['title'], NULL, 'warning' );
						}
					break;
					case 'mark_paid': 
						if ( $this->permissions->can_edit() )
						{
							foreach ( $_POST['cb_ids'] AS $id )
							{
								if ( ! $this->purchases->is_paid( (int)$id ) )
								{
									$purchase = $this->assets->fill_empty_vars( $this->purchases->get( (int)$id ), 'purchases' );

									// Create new movement
									if ( ! empty( $purchase ) )
									{
										$new_movement = array(
											'date' 				=> date( "Y-m-d" ),
											'type' 				=> 'outcome',
											'label'				=> __( "Purchase of products" ),
											'book'				=> __( "Purchases" ),
											'comments'			=> sprintf( __( "Provider: %s\nBill number: %s" ), $purchase['provider'], $purchase['bill_number'] ),
											'amount'			=> $purchase['total_amount'],
											'transaction_type'	=> 'purchases',
											'transaction_id'	=> $purchase['purchase_id'],
										);

										if ( $new_movement_id = $this->movements->save( $new_movement ) )
										{
											$new_movement = $this->movements->get( $new_movement_id );
											$this->logs->set_data( array( 'bulk_actions' => $_POST['bulk_actions'], 'cb_id' => $id, 'movement' => $new_movement ), "POST" )->set_result( TRUE )->register();
											$this->alerts->persist( TRUE )->alert( __( "Items were successfully marked paid." ), __( "Items edited" ), NULL, 'success' );
										}
										else
										{
											$this->logs->set_data( array( 'bulk_actions' => $_POST['bulk_actions'], 'cb_id' => $id ), "POST" )->set_result( FALSE )->register();
											$this->alerts->persist( TRUE )->alert( __( "There were some errors while trying to perform the task. Please try again." ), __( "Error" ), NULL, 'danger' );
										}
									}
								}
							}

							redirect( uri_string() );
						}
						else
						{
							$this->alerts->alert( $this->assets->conf['no_permission']['message'], $this->assets->conf['no_permission']['title'], NULL, 'warning' );
						}
					break;
					case 'mark_unpaid': 
						if ( $this->permissions->can_delete() )
						{
							foreach ( $_POST['cb_ids'] AS $id )
							{
								if ( $this->purchases->is_paid( (int)$id ) )
								{
									// Delete the movement
									$movement = $this->movements->get( array(
										'where' => array(
											'transaction_type' => 'purchases',
											'transaction_id' => (int)$id,
										)
									) );

									if ( ! empty( $movement[0] ) )
									{
										if ( $this->movements->erase( $movement[0]['movement_id'] ) )
										{
											$this->logs->set_data( array( 'bulk_actions' => $_POST['bulk_actions'], 'cb_id' => $id ), "POST" )->set_result( TRUE )->register();
											$this->alerts->persist( TRUE )->alert( __( "Items were successfully marked unpaid." ), __( "Items deleted" ), NULL, 'success' );
										}
										else
										{
											$this->logs->set_data( array( 'bulk_actions' => $_POST['bulk_actions'], 'cb_id' => $id ), "POST" )->set_result( FALSE )->register();
											$this->alerts->persist( TRUE )->alert( __( "There were some errors while trying to perform the task. Please try again." ), __( "Error" ), NULL, 'danger' );
										}
									}
								}
							}

							redirect( uri_string() );
						}
						else
						{
							$this->alerts->alert( $this->assets->conf['no_permission']['message'], $this->assets->conf['no_permission']['title'], NULL, 'warning' );
						}
					break;
					case 'mark_unstocked': 
						if ( $this->permissions->can_edit() )
						{
							foreach ( $_POST['cb_ids'] AS $id )
							{
								if ( $this->purchases->save( array( 'purchase_id' => (int)$id, 'stocked' => 0 ) ) )
								{
									// Mark metas as unstocked too
									$metas = $this->purchases->meta_get( (int)$id );
									if ( ! empty( $metas ) ) foreach ( $metas AS $row ) $this->purchases->meta_edit( array( 'purchase_meta_id' => $row['purchase_meta_id'], 'stocked' => 0 ) );

									$this->logs->set_data( array( 'bulk_actions' => $_POST['bulk_actions'], 'cb_id' => $id ), "POST" )->set_result( TRUE )->register();
									$this->alerts->persist( TRUE )->alert( __( "Items were successfully marked unstocked." ), __( "Items edited" ), NULL, 'success' );
								}
								else
								{
									$this->logs->set_data( array( 'bulk_actions' => $_POST['bulk_actions'], 'cb_id' => $id ), "POST" )->set_result( FALSE )->register();
									$this->alerts->persist( TRUE )->alert( __( "There were some errors while trying to perform the task. Please try again." ), __( "Error" ), NULL, 'danger' );
								}
							}

							redirect( uri_string() );
						}
						else
						{
							$this->alerts->alert( $this->assets->conf['no_permission']['message'], $this->assets->conf['no_permission']['title'], NULL, 'warning' );
						}
					break;
				} 
			}
		}

		if ( isset( $_GET['action'] ) AND isset( $_GET['purchase_id'] ) )
		{
			if ( $_GET['action'] == "trash" AND $this->permissions->can_trash() )
			{
				if ( $this->purchases->save( array( 'purchase_id' => (int)$_GET['purchase_id'], 'trash' => 1 ) ) )
				{
					// Send movements to trash
					if ( $this->purchases->is_paid( (int)$_GET['purchase_id'] ) )
					{
						$movement = $this->movements->get( array(
							'where' => array(
								'transaction_type' => 'purchases',
								'transaction_id' => (int)$_GET['purchase_id'],
							)
						) );

						if ( ! empty( $movement[0] ) ) $this->movements->save( array( 'movement_id' => $movement[0]['movement_id'], 'trash' => 1 ) );
					}

					$this->logs->set_data( $_GET, "GET" )->set_result( TRUE )->register();
					$this->alerts->alert( __( "Item was successfully trashed." ), __( "Item trashed" ), NULL, 'success' );
				}
				else
				{
					$this->logs->set_data( $_GET, "GET" )->set_result( FALSE )->register();
					$this->alerts->alert( __( "There were some errors while trying to perform the task. Please try again." ), __( "Error" ), NULL, 'danger' );
				}
			}
			else if ( $_GET['action'] == "mark_paid" AND $this->permissions->can_edit() )
			{
				if ( ! $this->purchases->is_paid( (int)$_GET['purchase_id'] ) )
				{
					$purchase = $this->assets->fill_empty_vars( $this->purchases->get( (int)$_GET['purchase_id'] ), 'purchases' );

					if ( ! empty( $purchase ) )
					{
						// Create new movement
						$new_movement = array(
							'date' 				=> date( "Y-m-d" ),
							'type' 				=> 'outcome',
							'label'				=> __( "Purchase of products" ),
							'book'				=> __( "Purchases" ),
							'comments'			=> sprintf( __( "Provider: %s\nBill number: %s" ), $purchase['provider'], $purchase['bill_number'] ),
							'amount'			=> $purchase['total_amount'],
							'transaction_type'	=> 'purchases',
							'transaction_id'	=> $purchase['purchase_id'],
						);

						if ( $new_movement_id = $this->movements->save( $new_movement ) )
						{
							$new_movement = $this->movements->get( $new_movement_id );
							$this->logs->set_data( array_merge( $_GET, array( 'movement' => $new_movement ) ), "GET" )->set_result( TRUE )->register();
							$this->alerts->alert( __( "Item was successfully marked paid." ), __( "Item edited" ), NULL, 'success' );
						}
						else
						{
							$this->logs->set_data( array_merge( $_GET, array( 'movement' => $new_movement ) ), "GET" )->set_result( FALSE )->register();
							$this->alerts->alert( __( "There were some errors while trying to perform the task. Please try again." ), __( "Error" ), NULL, 'danger' );
						}
					}
				}
			}
			else if ( $_GET['action'] == "mark_unpaid" AND $this->permissions->can_delete() )
			{
				if ( $this->purchases->is_paid( (int)$_GET['purchase_id'] ) )
				{
					// Delete the movement
					$movement = $this->movements->get( array(
						'where' => array(
							'transaction_type' => 'purchases',
							'transaction_id' => (int)$_GET['purchase_id'],
						)
					) );

					if ( ! empty( $movement[0] ) )
					{
						if ( $this->movements->erase( $movement[0]['movement_id'] ) )
						{
							$this->logs->set_data( array_merge( $_GET, array( 'movement' => $movement ) ), "GET" )->set_result( TRUE )->register();
							$this->alerts->alert( __( "Item was successfully marked unpaid." ), __( "Item deleted" ), NULL, 'success' );
						}
						else
						{
							$this->logs->set_data( array_merge( $_GET, array( 'movement' => $movement ) ), "GET" )->set_result( FALSE )->register();
							$this->alerts->alert( __( "There were some errors while trying to perform the task. Please try again." ), __( "Error" ), NULL, 'danger' );
						}
					}
				}
			}
			else if ( $_GET['action'] == "mark_unstocked" AND $this->permissions->can_edit() )
			{
				if ( $this->purchases->save( array( 'purchase_id' => (int)$_GET['purchase_id'], 'stocked' => 0 ) ) )
				{
					// Mark metas as unstocked too
					$metas = $this->purchases->meta_get( (int)$_GET['purchase_id'] );
					if ( ! empty( $metas ) ) foreach ( $metas AS $row ) $this->purchases->meta_edit( array( 'purchase_meta_id' => $row['purchase_meta_id'], 'stocked' => 0 ) );

					$this->logs->set_data( $_GET, "GET" )->set_result( TRUE )->register();
					$this->alerts->alert( __( "Item was successfully marked unstocked." ), __( "Item edited" ), NULL, 'success' );
				}
				else
				{
					$this->logs->set_data( $_GET, "GET" )->set_result( FALSE )->register();
					$this->alerts->alert( __( "There were some errors while trying to perform the task. Please try again." ), __( "Error" ), NULL, 'danger' );
				}
			}
			else
			{
				$this->alerts->alert( $this->assets->conf['no_permission']['message'], $this->assets->conf['no_permission']['title'], NULL, 'warning' );
			}
		}

		// Order
		if ( $this->input->get( 'order_by' ) AND $this->input->get( 'direction' ) )
		{
			$query_args['order_by'][$this->input->get( 'order_by' )] = $this->input->get( 'direction' );
		}
		else
		{
			$query_args['order_by']['purchase_id'] = 'DESC';
		}

		$query_args['where']['trash'] = 0;		
		$query_args['limit'] = $this->assets->conf['items_per_page'];
		$query_args['offset'] = $this->uri->segment( 3 );
		$this->data['purchases'] = $this->purchases->get( $query_args );
		$this->data['pagination'] = $this->templates->create_pagination( $this->assets->conf['tables']['purchases'], $query_args );

		$this->templates->reverse_pagination( $this->data['purchases'] );
		
		$this->assets->load_script( 'lists.min.js' );
		
		$this->data['tags']['page_title'] = __( "Purchases List" );
		$this->templates->view( 'purchases/lists', $this->data );
	}

	/**
	 * Items trash
	 * 
	 * @return output
	 */
	public function trash()
	{
		$this->permissions->block_not_admin();
		
		if ( $_POST )
		{
			if ( ! empty( $_POST['cb_ids'] ) )
			{
				switch ( $_POST['bulk_actions'] )
				{
					default: break;
					case 'restore': 
						if ( $this->permissions->can_restore() )
						{
							foreach ( $_POST['cb_ids'] AS $id )
							{
								if ( $this->purchases->save( array( 'purchase_id' => (int)$id, 'trash' => 0 ) ) )
								{
									// Restore the movement
									$movement = $this->movements->get( array(
										'where' => array(
											'transaction_type' => 'purchases',
											'transaction_id' => (int)$id,
										)
									) );

									if ( ! empty( $movement[0] ) ) $this->movements->save( array( 'movement_id' => $movement[0]['movement_id'], 'trash' => 0 ) );

									$this->logs->set_data( array( 'bulk_actions' => $_POST['bulk_actions'], 'cb_id' => $id ), "POST" )->set_result( TRUE )->register();
									$this->alerts->persist( TRUE )->alert( __( "Items were successfully restored." ), __( "Items restored" ), NULL, 'success' );
								}
								else
								{
									$this->logs->set_data( array( 'bulk_actions' => $_POST['bulk_actions'], 'cb_id' => $id ), "POST" )->set_result( FALSE )->register();
									$this->alerts->persist( TRUE )->alert( __( "There were some errors while trying to perform the task. Please try again." ), __( "Error" ), NULL, 'danger' );
								}
							}

							redirect( uri_string() );
						}
						else
						{
							$this->alerts->alert( $this->assets->conf['no_permission']['message'], $this->assets->conf['no_permission']['title'], NULL, 'warning' );
						}
					break;
					case 'delete':
						if ( $this->permissions->can_delete() )
						{
							foreach ( $_POST['cb_ids'] AS $id )
							{
								if ( $this->purchases->erase( (int)$id ) )
								{
									// Delete the movement
									$movement = $this->movements->get( array(
										'where' => array(
											'transaction_type' => 'purchases',
											'transaction_id' => (int)$id,
										)
									) );

									if ( ! empty( $movement[0] ) ) $this->movements->erase( $movement[0]['movement_id'] );

									$this->logs->set_data( array( 'bulk_actions' => $_POST['bulk_actions'], 'cb_id' => $id ), "POST" )->set_result( TRUE )->register();
									$this->alerts->persist( TRUE )->alert( __( "Items were successfully deleted." ), __( "Items deleted" ), NULL, 'success' );
								}
								else
								{
									$this->logs->set_data( array( 'bulk_actions' => $_POST['bulk_actions'], 'cb_id' => $id ), "POST" )->set_result( FALSE )->register();
									$this->alerts->persist( TRUE )->alert( __( "There were some errors while trying to perform the task. Please try again." ), __( "Error" ), NULL, 'danger' );
								}
							}

							redirect( uri_string() );
						}
						else
						{
							$this->alerts->alert( $this->assets->conf['no_permission']['message'], $this->assets->conf['no_permission']['title'], NULL, 'warning' );
						}
					break;
				} 
			}
		}

		if ( isset( $_GET['action'] ) AND isset( $_GET['purchase_id'] ) )
		{
			if ( $_GET['action'] == "restore" AND $this->permissions->can_restore() )
			{
				if ( $this->purchases->save( array( 'purchase_id' => (int)$_GET['purchase_id'], 'trash' => 0 ) ) )
				{
					// Restore the movement
					$movement = $this->movements->get( array(
						'where' => array(
							'transaction_type' => 'purchases',
							'transaction_id' => (int)$_GET['purchase_id'],
						)
					) );

					if ( ! empty( $movement[0] ) ) $this->movements->save( array( 'movement_id' => $movement[0]['movement_id'], 'trash' => '0' ) );

					$this->logs->set_data( array_merge( $_GET, array( 'movement_id' => $movement[0]['movement_id'] ) ), "GET" )->set_result( TRUE )->register();
					$this->alerts->alert( __( "Item successfully restored." ), __( "Item restored" ), NULL, 'success' );
				}
				else
				{
					$this->logs->set_data( $_GET, "GET" )->set_result( FALSE )->register();
					$this->alerts->alert( __( "There were some errors while trying to perform the task. Please try again." ), __( "Error" ), NULL, 'danger' );				
				}
			}
			else if ( $_GET['action'] == "delete" AND $this->permissions->can_delete() )
			{
				if ( $this->purchases->erase( (int)$_GET['purchase_id'] ) )
				{
					// Erase the movement
					$movement = $this->movements->get( array(
						'where' => array(
							'transaction_type' => 'purchases',
							'transaction_id' => (int)$_GET['purchase_id'],
						)
					) );

					if ( ! empty( $movement[0] ) ) $this->movements->erase( $movement[0]['movement_id'] );

					$this->logs->set_data( array_merge( $_GET, array( 'movement_id' => $movement[0]['movement_id'] ) ), "GET" )->set_result( TRUE )->register();
					$this->alerts->alert( __( "Item successfully deleted." ), __( "Item deleted" ), NULL, 'success' );
				}
				else
				{
					$this->logs->set_data( $_GET, "GET" )->set_result( FALSE )->register();
					$this->alerts->alert( __( "There were some errors while trying to perform the task. Please try again." ), __( "Error" ), NULL, 'danger' );				
				}
			}
			else
			{
				$this->alerts->alert( $this->assets->conf['no_permission']['message'], $this->assets->conf['no_permission']['title'], NULL, 'warning' );
			}
		}

		// Order
		if ( $this->input->get( 'order_by' ) AND $this->input->get( 'direction' ) )
		{
			$query_args['order_by'][$this->input->get( 'order_by' )] = $this->input->get( 'direction' );
		}
		else
		{
			$query_args['order_by']['purchase_id'] = 'DESC';
		}

		$query_args['where']['trash'] = 1;
		$query_args['limit'] = $this->assets->conf['items_per_page'];
		$query_args['offset'] = $this->uri->segment( 3 );
		$this->data['purchases'] = $this->purchases->get( $query_args );
		$this->data['pagination'] = $this->templates->create_pagination( $this->assets->conf['tables']['purchases'], $query_args );
		
		$this->templates->reverse_pagination( $this->data['purchases'] );

		$this->assets->load_script( 'lists.min.js' );
		
		$this->data['tags']['page_title'] = __( "Purchases Trash" );
		$this->templates->view( 'purchases/lists', $this->data );
	}

	/**
	 * Item add page
	 * 
	 * @return output
	 */
	public function add()
	{
		if ( $this->input->post() )
		{
			if ( ! $this->permissions->can_add() )
			{
				$this->alerts->persist( TRUE )->alert( $this->assets->conf['no_permission']['message'], $this->assets->conf['no_permission']['title'], NULL, 'warning' );
				redirect( uri_string() );
			}

			$this->form_validation->set_rules( 'purchase_date', __( "Purchase date" ), 'trim|required' );
			$this->form_validation->set_rules( 'provider', __( "Provider" ), 'trim|required' );
			$this->form_validation->set_rules( 'total_amount', __( "Total amount" ), ( ( $this->assets->conf['decimals'] > 0 ) ? 'decimal' : 'integer' ) . '|required|greater_than[0]' );

			if ( $this->form_validation->run() )
			{
				if ( $purchase_id = $this->purchases->save( $this->input->post( NULL, TRUE ) ) )
				{
					// Associate in case there were any uploaded files
					if ( $this->input->post( 'file_id' ) )
					{
						$this->load->model( "Files_model", "files" );

						foreach ( $this->input->post( 'file_id' ) AS $key => $file_id )
						{
							$this->files->save( array(
								'file_id' => $file_id,
								'registry_id' => $purchase_id,
							) );
						}
					}

					$new_purchase = $this->assets->fill_empty_vars( $this->purchases->get( $purchase_id, 'purchases' ) );

					// Associate movement in case it is paid already
					$new_movement_id = $this->movements->save( array(
						'date' 				=> ( ! empty( $new_purchase['original']['expiration_date'] ) ) ? $new_purchase['expiration_date'] : $new_purchase['purchase_date'],
						'type' 				=> 'outcome',
						'label'				=> __( "Purchase of products" ),
						'book'				=> __( "Purchases" ),
						'comments'			=> sprintf( __( "Provider: %s\nBill number:%s" ), $new_purchase['provider'], $new_purchase['bill_number'] ),
						'amount'			=> $new_purchase['total_amount'],
						'transaction_type'	=> 'purchases',
						'transaction_id'	=> $purchase_id,
					) );

					$_POST['purchase_id'] = $purchase_id;
					$_POST['movement'] = $this->movements->get( $new_movement_id );
					$this->logs->set_data( $this->input->post(), "POST" )->set_result( TRUE )->register();
					$this->alerts->persist( TRUE )->alert( __( "Item successfully added to the database." ), __( "Purchase created" ), NULL, 'success' );

					redirect( "purchases/edit/" . $purchase_id );
				}
				else
				{
					$this->logs->set_data( $this->input->post(), "POST" )->set_result( FALSE )->register();
					$this->alerts->alert( __( "There were some errors while trying to perform the task. Please try again." ), __( "Purchase discarded" ), NULL, 'danger' );
				}
			}
			else
			{
				$this->alerts->alert( __( "Errors appear below every field." ), __( "Item could not be stored into the database" ), NULL, 'danger' );
			}
		}

		$this->data['purchase_date'] = array(
			'type'		=> 'text',
			'id'		=> 'purchase_date',
			'name'		=> 'purchase_date',
			'class'		=> 'form-control input-date',
			'value'		=> set_value( 'purchase_date', date( $this->assets->conf['date_format'] ) ),
		);

		$this->data['delivery_date'] = array(
			'type'		=> 'text',
			'id'		=> 'delivery_date',
			'name'		=> 'delivery_date',
			'class'		=> 'form-control input-date',
			'value'		=> set_value( 'delivery_date' ),
		);

		$this->data['expiration_date'] = array(
			'type'		=> 'text',
			'id'		=> 'expiration_date',
			'name'		=> 'expiration_date',
			'class'		=> 'form-control input-date',
			'value'		=> set_value( 'expiration_date' ),
		);

		$this->data['bill_number'] = array(
			'type'		=> 'text',
			'id'		=> 'bill_number',
			'name'		=> 'bill_number',
			'class'		=> 'form-control input-bill',
			'value'		=> set_value( 'bill_number' ),
		);

		$this->data['provider'] = array(
			'id'		=> 'provider',
			'name'		=> 'provider',
			'class'		=> 'form-control',
			'style'		=> 'width: 100%',
			'selected'	=> set_value( 'provider' ),
			'options'	=> $this->purchases->build_dropdown_providers(),
		);

		$this->data['observations'] = array(
			'name'			=> 'observations',
			'id'			=> 'observations',
			'class'			=> 'form-control',
			'rows'			=> 3,
			'value'			=> set_value( 'observations' ),
			'placeholder'	=> __( "Use this field to store any other information about the purchase..." ),
		);

		$this->data['total_amount'] = array(
			'type'		=> 'text',
			'id'		=> 'total_amount',
			'name'		=> 'total_amount',
			'readonly'	=> 'readonly',
			'class'		=> 'form-control form-control-lg input-currency',
			'value'		=> set_value( 'total_amount' ),
		);

		// MODAL ------------------------------------------------------------

		$this->data['modal_product_id'] = array(
			'id'		=> 'modal_product_id',
			'name'		=> 'modal_product_id',
			'class'		=> 'form-control',
			'style'		=> 'width: 100%',
			'options'	=> $this->products->build_dropdown(),
		);

		$this->data['modal_unit_price'] = array(
			'type'		=> 'text',
			'id'		=> 'modal_unit_price',
			'name'		=> 'modal_unit_price',
			'class'		=> 'form-control input-currency',
		);

		$this->data['modal_quantity'] = array(
			'type'		=> 'text',
			'id'		=> 'modal_quantity',
			'name'		=> 'modal_quantity',
			'class'		=> 'form-control input-quantity',
		);

		// MODAL - ADD NEW PRODUCT ------------------------------------------

		$this->data['modal_new_product_name'] = array(
			'type'			=> 'text',
			'id'			=> 'modal_new_product_name',
			'name'			=> 'modal_new_product_name',
			'class'			=> 'form-control form-control-lg',
			'autocomplete'	=> 'off',
		);

		$this->data['modal_new_product_category_id'] = array(
			'id'		=> 'modal_new_product_category_id',
			'name'		=> 'modal_new_product_category_id',
			'class'		=> 'form-control',
			'style'		=> 'width: 100%',
			'options'	=> $this->categories->build_dropdown(),
		);

		$this->data['modal_new_product_unit_price'] = array(
			'type'			=> 'text',
			'id'			=> 'modal_new_product_unit_price',
			'name'			=> 'modal_new_product_unit_price',
			'class'			=> 'form-control input-currency',
			'autocomplete'	=> 'off',
		);

		$this->data['modal_new_product_unit_measure'] = array(
			'id'		=> 'modal_new_product_unit_measure',
			'name'		=> 'modal_new_product_unit_measure',
			'class'		=> 'form-control',
			'style'		=> 'width: 100%',
			'options'	=> array_merge( array( '' => '&nbsp;' ), $this->assets->conf['units'] ),
		);

		$this->data['modal_new_product_currency'] = array(
			'type'		=> 'hidden',
			'id'		=> 'modal_new_product_currency',
			'name'		=> 'modal_new_product_currency',
			'value'		=> $this->assets->conf['currency'],
		);

		$this->data['modal_new_product_tax'] = array(
			'id'		=> 'modal_new_product_tax',
			'name'		=> 'modal_new_product_tax',
			'class'		=> 'form-control',
			'style'		=> 'width: 100%',
			'options'	=> array_merge( array( '' => '&nbsp;' ), $this->assets->conf['bill_columns'] ),
		);

		// Pickadate
		$this->assets->load_style( base_url( 'assets/bower_components/pickadate/lib/compressed/themes/default.css' ) );
		$this->assets->load_style( base_url( 'assets/bower_components/pickadate/lib/compressed/themes/default.date.css' ) );
		$this->assets->load_script( base_url( 'assets/bower_components/pickadate/lib/compressed/picker.js' ) );
		$this->assets->load_script( base_url( 'assets/bower_components/pickadate/lib/compressed/picker.date.js' ) );	
		if ( $this->assets->conf['language'] !== "en_US" ) $this->assets->load_script( base_url( 'assets/bower_components/pickadate/lib/translations/' . $this->assets->conf['language'] . '.js' ) );

		// Inputmask
		$this->assets->load_script( base_url( 'assets/bower_components/inputmask/dist/jquery.inputmask.min.js' ) );

		// jQuery File Uploader assets
		$this->assets->load_style( site_url( 'assets/bower_components/jQuery-File-Upload/css/jquery.fileupload.css' ) );
		$this->assets->load_script( site_url( 'assets/bower_components/jQuery-File-Upload/js/vendor/jquery.ui.widget.js' ) );
		$this->assets->load_script( site_url( 'assets/bower_components/jQuery-File-Upload/js/jquery.iframe-transport.js' ) );
		$this->assets->load_script( site_url( 'assets/bower_components/jQuery-File-Upload/js/jquery.fileupload.js' ) );

		// Select2
		$this->assets->load_style( site_url( 'assets/bower_components/select2/dist/css/select2.min.css' ) );
		$this->assets->load_script( site_url( 'assets/bower_components/select2/dist/js/select2.full.min.js' ) );
		$this->assets->load_script( site_url( 'assets/bower_components/select2/dist/js/i18n/' . substr( $this->assets->conf['language'], 0, 2 ) . '.js' ) );

		$this->assets->load_script( 'forms.min.js' );
		$this->assets->load_script( 'forms-purchases.min.js' );

		$this->data['tags']['page_title'] = __( "Add Purchase" );
		$this->templates->view( 'purchases/form', $this->data );
	}

	/**
	 * Item edit page
	 * 
	 * @param  int $purchase_id Purchase ID
	 * @return output
	 */
	public function edit( $purchase_id )
	{
		if ( $this->input->post() )
		{
			if ( ! $this->permissions->can_edit() )
			{
				$this->alerts->persist( TRUE )->alert( $this->assets->conf['no_permission']['message'], $this->assets->conf['no_permission']['title'], NULL, 'warning' );
				redirect( uri_string() );
			}

			$this->form_validation->set_rules( 'purchase_id', __( "Purchase ID" ), 'integer|required' );
			$this->form_validation->set_rules( 'purchase_date', __( "Purchase date" ), 'trim|required' );
			$this->form_validation->set_rules( 'provider', __( "Provider" ), 'trim|required' );
			$this->form_validation->set_rules( 'total_amount', __( "Total amount" ), ( ( $this->assets->conf['decimals'] > 0 ) ? 'decimal' : 'integer' ) . '|required|greater_than[0]' );

			if ( $this->form_validation->run() )
			{
				if ( $this->purchases->save( $this->input->post( NULL, TRUE ) ) )
				{
					// Edit associated movement in case we need to ---------------------------
					$movement = $this->movements->get( array(
						'where' => array(
							'transaction_type' => 'purchases',
							'transaction_id' => $purchase_id,
						)
					) );

					if ( ! empty( $movement ) )
					{
						$movement = $movement[0];
						$purchase = $this->assets->fill_empty_vars( $this->purchases->get( $purchase_id ), 'purchases' );

						// Associate movement in case it is paid already
						$this->movements->save( array(
							'movement_id'		=> $movement['movement_id'],
							'date' 				=> ( ! empty( $purchase['original']['expiration_date'] ) ) ? $purchase['expiration_date'] : $purchase['purchase_date'],
							'comments'			=> sprintf( __( "Provider: %s\nBill number:%s" ), $purchase['provider'], $purchase['bill_number'] ),
							'amount'			=> $purchase['total_amount'],
						) );
					}

					$this->logs->set_data( $this->input->post(), "POST" )->set_result( TRUE )->register();
					$this->alerts->persist( TRUE )->alert( __( "Item was successfully updated into the database." ), __( "Purchase updated" ), NULL, 'success' );
					redirect( "purchases/edit/" . $purchase_id );
				}
				else
				{
					$this->logs->set_data( $this->input->post(), "POST" )->set_result( FALSE )->register();
					$this->alerts->alert( __( "There were some errors while trying to perform the task. Please try again." ), __( "Purchase not updated" ), NULL, 'danger' );
				}
			}
			else
			{
				$this->alerts->alert( __( "Errors appear below every field." ), __( "Item could not be stored into the database" ), NULL, 'danger' );
			}
		}

		$this->data['purchase'] = $this->purchases->get( $purchase_id );

		if ( ! empty( $this->data['purchase'] ) )
		{
			$this->data['purchase_id'] = array(
				'type'		=> 'hidden',
				'id'		=> 'purchase_id',
				'name'		=> 'purchase_id',
				'value'		=> $this->data['purchase']['purchase_id'],
			);

			$this->data['purchase_date'] = array(
				'type'		=> 'text',
				'id'		=> 'purchase_date',
				'name'		=> 'purchase_date',
				'class'		=> 'form-control input-date',
				'value'		=> ( ! empty( $this->data['purchase']['purchase_date'] ) ) ? date( $this->assets->conf['date_format'], strtotime( $this->data['purchase']['purchase_date'] ) ) : NULL,
			);

			$this->data['delivery_date'] = array(
				'type'		=> 'text',
				'id'		=> 'delivery_date',
				'name'		=> 'delivery_date',
				'class'		=> 'form-control input-date',
				'value'		=> ( ! empty( $this->data['purchase']['delivery_date'] ) ) ? date( $this->assets->conf['date_format'], strtotime( $this->data['purchase']['delivery_date'] ) ) : NULL,
			);

			$this->data['expiration_date'] = array(
				'type'		=> 'text',
				'id'		=> 'expiration_date',
				'name'		=> 'expiration_date',
				'class'		=> 'form-control input-date',
				'value'		=> ( ! empty( $this->data['purchase']['expiration_date'] ) ) ? date( $this->assets->conf['date_format'], strtotime( $this->data['purchase']['expiration_date'] ) ) : NULL,
			);

			$this->data['bill_number'] = array(
				'type'		=> 'text',
				'id'		=> 'bill_number',
				'name'		=> 'bill_number',
				'class'		=> 'form-control input-bill',
				'value'		=> $this->data['purchase']['bill_number'],
			);

			$this->data['provider'] = array(
				'id'		=> 'provider',
				'name'		=> 'provider',
				'class'		=> 'form-control',
				'style'		=> 'width: 100%',
				'selected'	=> $this->data['purchase']['provider'],
				'options'	=> $this->purchases->build_dropdown_providers(),
			);

			$this->data['observations'] = array(
				'name'			=> 'observations',
				'id'			=> 'observations',
				'class'			=> 'form-control',
				'rows'			=> 3,
				'value'			=> $this->data['purchase']['observations'],
				'placeholder'	=> __( "Use this field to store any other information about the purchase..." ),
			);

			$this->data['total_amount'] = array(
				'type'		=> 'text',
				'id'		=> 'total_amount',
				'name'		=> 'total_amount',
				'readonly'	=> 'readonly',
				'class'		=> 'form-control form-control-lg input-currency',
				'value'		=> my_number_format( $this->data['purchase']['total_amount'] ),
			);

			// MODAL ------------------------------------------------------------

			$this->data['modal_product_id'] = array(
				'id'		=> 'modal_product_id',
				'name'		=> 'modal_product_id',
				'class'		=> 'form-control',
				'style'		=> 'width: 100%',
				'options'	=> $this->products->build_dropdown(),
			);

			$this->data['modal_unit_price'] = array(
				'type'		=> 'text',
				'id'		=> 'modal_unit_price',
				'name'		=> 'modal_unit_price',
				'class'		=> 'form-control input-currency',
			);

			$this->data['modal_quantity'] = array(
				'type'		=> 'text',
				'id'		=> 'modal_quantity',
				'name'		=> 'modal_quantity',
				'class'		=> 'form-control input-quantity',
			);

			// MODAL - ADD NEW PRODUCT ------------------------------------------

			$this->data['modal_new_product_name'] = array(
				'type'			=> 'text',
				'id'			=> 'modal_new_product_name',
				'name'			=> 'modal_new_product_name',
				'class'			=> 'form-control form-control-lg',
				'autocomplete'	=> 'off',
			);

			$this->data['modal_new_product_category_id'] = array(
				'id'		=> 'modal_new_product_category_id',
				'name'		=> 'modal_new_product_category_id',
				'class'		=> 'form-control',
				'style'		=> 'width: 100%',
				'options'	=> $this->categories->build_dropdown(),
			);

			$this->data['modal_new_product_unit_price'] = array(
				'type'			=> 'text',
				'id'			=> 'modal_new_product_unit_price',
				'name'			=> 'modal_new_product_unit_price',
				'class'			=> 'form-control input-currency',
				'autocomplete'	=> 'off',
			);

			$this->data['modal_new_product_unit_measure'] = array(
				'id'		=> 'modal_new_product_unit_measure',
				'name'		=> 'modal_new_product_unit_measure',
				'class'		=> 'form-control',
				'style'		=> 'width: 100%',
				'options'	=> array_merge( array( '' => '&nbsp;' ), $this->assets->conf['units'] ),
			);

			$this->data['modal_new_product_currency'] = array(
				'type'		=> 'hidden',
				'id'		=> 'modal_new_product_currency',
				'name'		=> 'modal_new_product_currency',
				'value'		=> $this->assets->conf['currency'],
			);

			$this->data['modal_new_product_tax'] = array(
				'id'		=> 'modal_new_product_tax',
				'name'		=> 'modal_new_product_tax',
				'class'		=> 'form-control',
				'style'		=> 'width: 100%',
				'options'	=> array_merge( array( '' => '&nbsp;' ), $this->assets->conf['bill_columns'] ),
			);

			// Pickadate
			$this->assets->load_style( base_url( 'assets/bower_components/pickadate/lib/compressed/themes/default.css' ) );
			$this->assets->load_style( base_url( 'assets/bower_components/pickadate/lib/compressed/themes/default.date.css' ) );
			$this->assets->load_script( base_url( 'assets/bower_components/pickadate/lib/compressed/picker.js' ) );
			$this->assets->load_script( base_url( 'assets/bower_components/pickadate/lib/compressed/picker.date.js' ) );	
			if ( $this->assets->conf['language'] !== "en_US" ) $this->assets->load_script( base_url( 'assets/bower_components/pickadate/lib/translations/' . $this->assets->conf['language'] . '.js' ) );

			// Inputmask
			$this->assets->load_script( base_url( 'assets/bower_components/inputmask/dist/jquery.inputmask.min.js' ) );

			// jQuery File Uploader assets
			$this->assets->load_style( site_url( 'assets/bower_components/jQuery-File-Upload/css/jquery.fileupload.css' ) );
			$this->assets->load_script( site_url( 'assets/bower_components/jQuery-File-Upload/js/vendor/jquery.ui.widget.js' ) );
			$this->assets->load_script( site_url( 'assets/bower_components/jQuery-File-Upload/js/jquery.iframe-transport.js' ) );
			$this->assets->load_script( site_url( 'assets/bower_components/jQuery-File-Upload/js/jquery.fileupload.js' ) );

			// Select2
			$this->assets->load_style( site_url( 'assets/bower_components/select2/dist/css/select2.min.css' ) );
			$this->assets->load_script( site_url( 'assets/bower_components/select2/dist/js/select2.full.min.js' ) );
			$this->assets->load_script( site_url( 'assets/bower_components/select2/dist/js/i18n/' . substr( $this->assets->conf['language'], 0, 2 ) . '.js' ) );

			$this->assets->load_script( 'forms.min.js' );
			$this->assets->load_script( 'forms-purchases.min.js' );

			$this->data['tags']['page_title'] = __( "Edit Purchase" );
			$this->templates->view( 'purchases/form', $this->data );
		}
		else
		{
			$this->alerts->persist( TRUE )->alert( __( "The purchase is not stored into the database." ), __( "Purchase does not exist" ), NULL, 'danger' );
			redirect( 'purchases/lists' );
		}
	}

	/**
	 * Item brief page
	 * 
	 * @param  int $purchase_id The purchase id
	 * @return output
	 */
	public function brief( $purchase_id )
	{
		$this->load->model( "Files_model", "files" );
		$this->data['purchase'] = $this->purchases->get( $purchase_id );

		if ( ! empty( $this->data['purchase'] ) )
		{
			$this->data['files'] 				= $this->files->get_files_src( $this->data['purchase']['purchase_id'], 'purchases', FALSE );
			$this->data['meta'] 				= $this->purchases->meta_get( $this->data['purchase']['purchase_id'] );
			$this->data['purchase'] 			= $this->assets->fill_empty_vars( $this->data['purchase'] );
			$this->data['stocked'] 				= $this->purchases->is_stocked( $this->data['purchase'] );
			$this->data['paid'] 				= $this->purchases->is_paid( $this->data['purchase'] );
			$this->data['labels']['yes'] 		= '<span class="badge badge-success">' . __( "Yes" ) . '</span>';
			$this->data['labels']['no'] 		= '<span class="badge badge-danger">' . __( "No" ) . '</span>';
			$this->data['tags']['page_title'] 	= __( "Purchase Brief" );
			$this->templates->view( 'purchases/brief', $this->data );
		}
		else
		{
			$this->alerts->persist( TRUE )->alert( __( "The purchase is not stored into the database." ), __( "Purchase does not exist" ), NULL, 'danger' );
			redirect( 'purchases/lists' );
		}
	}

}