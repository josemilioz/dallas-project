<?php defined( 'BASEPATH' ) OR exit( 'No direct script access allowed' );

class Users extends CI_Controller 
{
	/**
	 * Class constructor
	 */
	public function __construct()
	{
		parent::__construct();

		if ( ! $this->ion_auth->logged_in() ) redirect( 'login', 'refresh' );

		$this->permissions->block_not_admin();

		$this->load->model( "Files_model", "files" );

		$this->data['tags']['controller_name'] = __( "Users" );

		/**
		 * -----------------------------------------------------------------------------------------------
		 * BULK ACTIONS FOR SELECT IN LISTS
		 * -----------------------------------------------------------------------------------------------
		 */

		$bulk_options['-1'] = __( "Bulk actions" );
		if ( $this->uri->segment( 2 ) != 'trash' AND $this->permissions->can_trash() ) $bulk_options['trash'] = __( "Move to Trash" );
		if ( $this->uri->segment( 2 ) == 'trash' AND $this->permissions->can_restore() ) $bulk_options['restore'] = __( "Restore" );
		if ( $this->uri->segment( 2 ) == 'trash' AND $this->permissions->can_delete() ) $bulk_options['delete'] = __( "Delete" );
		
		$this->data['bulk_actions'] = array(
			'id'		=> 'bulk_actions',
			'name'		=> 'bulk_actions',
			'class'		=> 'form-control form-control-sm mb-1',
			'options'	=> $bulk_options,
		);

		$this->data['submit_bulk_actions'] = array(
			'id'			=> 'submit_bulk_actions',
			'name'			=> 'submit_bulk_actions',
			'class'			=> 'btn btn-secondary btn-sm btn-bulk mb-1',
			'value'			=> __( "Apply" ),
			'data-confirm'	=> __( "Are you sure you want to perform that action?\n\nIt may be definitive and unable to undo in the future." ),
		);

		/**
		 * -----------------------------------------------------------------------------------------------
		 * LIST TOOLS EXTENSION
		 * -----------------------------------------------------------------------------------------------
		 */
		
		$this->data['list_buttons_extension'] = array(
			'brief' => anchor(
				'users/brief/%d',
				'<i class="fa fa-search"></i>',
				array(
					'title' 				=> __( "Brief" ),
					'class' 				=> 'brief btn btn-info',
					// 'data-enlarge-modal' 	=> 1,
				)
			),
		);
	}

	/**
	 * Index redirecting to lists
	 * 
	 * @return void
	 */
	public function index()
	{
		redirect( 'users/lists' );
	}

	/**
	 * Items list
	 * 
	 * @return output
	 */
	public function lists()
	{
		if ( $_POST )
		{
			if ( ! empty( $_POST['cb_ids'] ) )
			{
				switch ( $_POST['bulk_actions'] )
				{
					default: break;
					case 'trash': 
						if ( $this->permissions->can_trash() )
						{
							foreach ( $_POST['cb_ids'] AS $id )
							{
								if ( $id !== $this->ion_auth->user()->row()->id AND ! $this->ion_auth->is_superior( $id ) )
								{
									if ( $this->ion_auth->update( (int)$id, array( 'active' => 0 ) ) )
									{
										$this->logs->set_data( array( 'bulk_actions' => $_POST['bulk_actions'], 'cb_id' => $id ), "POST" )->set_result( TRUE )->register();
										$this->alerts->persist( TRUE )->alert( __( "Items were successfully trashed." ), __( "Items trashed" ), NULL, 'success' );
									}
									else
									{
										$this->logs->set_data( array( 'bulk_actions' => $_POST['bulk_actions'], 'cb_id' => $id ), "POST" )->set_result( FALSE )->register();
										$this->alerts->persist( TRUE )->alert( __( "There were some errors while trying to perform the task. Please try again." ), __( "Error" ), NULL, 'danger' );
									}									
								}
								else
								{
									$this->logs->set_data( array( 'bulk_actions' => $_POST['bulk_actions'], 'cb_id' => $id ), "POST" )->set_result( FALSE )->register();

									if ( $this->ion_auth->is_superior( $id ) )
									{
										$this->alerts->persist( TRUE )->alert( __( "You can't edit an user with a superior role." ), $this->assets->conf['no_permission']['title'], NULL, 'warning' );
									}
									else
									{
										$this->alerts->persist( TRUE )->alert( __( "You can't make these kind of changes to your own account." ), $this->assets->conf['no_permission']['title'], NULL, 'warning' );
									}
								}
							}

							redirect( uri_string() );
						}
						else
						{
							$this->alerts->alert( $this->assets->conf['no_permission']['message'], $this->assets->conf['no_permission']['title'], NULL, 'warning' );
						}
					break;
				} 
			}
		}

		if ( isset( $_GET['action'] ) AND isset( $_GET['user_id'] ) )
		{
			if ( $_GET['user_id'] !== $this->ion_auth->user()->row()->id AND ! $this->ion_auth->is_superior( $_GET['user_id'] ) )
			{
				if ( $_GET['action'] == "trash" AND $this->permissions->can_trash() )
				{
					if ( $this->ion_auth->update( (int)$_GET['user_id'], array( 'active' => 0 ) ) )
					{
						$this->logs->set_data( $_GET, "GET" )->set_result( TRUE )->register();
						$this->alerts->alert( __( "Item was successfully trashed." ), __( "Item trashed" ), NULL, 'success' );
					}
					else
					{
						$this->logs->set_data( $_GET, "GET" )->set_result( FALSE )->register();
						$this->alerts->alert( __( "There were some errors while trying to perform the task. Please try again." ), __( "Error" ), NULL, 'danger' );
					}
				}
				else
				{
					$this->alerts->alert( $this->assets->conf['no_permission']['message'], $this->assets->conf['no_permission']['title'], NULL, 'warning' );					
					$this->logs->set_data( $_GET, "GET" )->set_result( FALSE )->register();
				}
			}
			else
			{
				$this->logs->set_data( $_GET, "GET" )->set_result( FALSE )->register();

				if ( $this->ion_auth->is_superior( $_GET['user_id'] ) )
				{
					$this->alerts->alert( __( "You can't edit an user with a superior role." ), $this->assets->conf['no_permission']['title'], NULL, 'warning' );
				}
				else
				{
					$this->alerts->alert( __( "You can't make these kind of changes to your own account." ), $this->assets->conf['no_permission']['title'], NULL, 'warning' );						
				}
			}
		}

		// Order
		if ( $this->input->get( 'order_by' ) AND $this->input->get( 'direction' ) )
		{
			$query_args['order_by'][$this->input->get( 'order_by' )] = $this->input->get( 'direction' );
			$this->db->order_by( $this->input->get( 'order_by' ), $this->input->get( 'direction' ) );
		}
		else
		{
			$query_args['order_by']['id'] = 'DESC';
			$this->db->order_by( 'id', 'DESC' );
		}

		$query_args['where']['active'] = 1;
		$query_args['limit'] = $this->assets->conf['items_per_page'];
		$query_args['offset'] = $this->uri->segment( 3 );

		$this->db->where( 'active', 1 );
		$this->db->limit( $this->assets->conf['items_per_page'], $this->uri->segment( 3 ) );
		
		$this->data['users'] = $this->ion_auth->users()->result_array();
		$this->data['pagination'] = $this->templates->create_pagination( $this->assets->conf['tables']['users'], $query_args );

		$this->templates->reverse_pagination( $this->data['users'] );
		
		$this->assets->load_script( 'lists.min.js' );
		
		$this->data['tags']['page_title'] = __( "Users List" );
		$this->templates->view( 'users/lists', $this->data );
	}

	/**
	 * Items trash
	 * 
	 * @return output
	 */
	public function trash()
	{
		if ( $_POST )
		{
			if ( ! empty( $_POST['cb_ids'] ) )
			{
				switch ( $_POST['bulk_actions'] )
				{
					default: break;
					case 'restore': 
						if ( $this->permissions->can_trash() )
						{
							foreach ( $_POST['cb_ids'] AS $id )
							{
								if ( $id !== $this->ion_auth->user()->row()->id AND ! $this->ion_auth->is_superior( $id ) )
								{
									if ( $this->ion_auth->update( (int)$id, array( 'active' => 1 ) ) )
									{
										$this->logs->set_data( array( 'bulk_actions' => $_POST['bulk_actions'], 'cb_id' => $id ), "POST" )->set_result( TRUE )->register();
										$this->alerts->persist( TRUE )->alert( __( "Items were successfully restored." ), __( "Items restored" ), NULL, 'success' );
									}
									else
									{
										$this->logs->set_data( array( 'bulk_actions' => $_POST['bulk_actions'], 'cb_id' => $id ), "POST" )->set_result( FALSE )->register();
										$this->alerts->persist( TRUE )->alert( __( "There were some errors while trying to perform the task. Please try again." ), __( "Error" ), NULL, 'danger' );
									}
								}
								else
								{
									$this->logs->set_data( array( 'bulk_actions' => $_POST['bulk_actions'], 'cb_id' => $id ), "POST" )->set_result( FALSE )->register();

									if ( $this->ion_auth->is_superior( $id ) )
									{
										$this->alerts->persist( TRUE )->alert( __( "You can't edit an user with a superior role." ), $this->assets->conf['no_permission']['title'], NULL, 'warning' );
									}
									else
									{
										$this->alerts->persist( TRUE )->alert( __( "You can't make these kind of changes to your own account." ), $this->assets->conf['no_permission']['title'], NULL, 'warning' );										
									}
								}
							}

							redirect( uri_string() );
						}
						else
						{
							$this->alerts->alert( $this->assets->conf['no_permission']['message'], $this->assets->conf['no_permission']['title'], NULL, 'warning' );
						}
					break;
					case 'delete':
						if ( $this->permissions->can_delete() )
						{
							foreach ( $_POST['cb_ids'] AS $id )
							{
								if ( $id !== $this->ion_auth->user()->row()->id AND ! $this->ion_auth->is_superior( $id ) )
								{
									if ( $this->ion_auth->delete_user( (int)$id ) )
									{
										$this->files->erase_file_by_registry( (int)$id, 'users' );
										$this->logs->set_data( array( 'bulk_actions' => $_POST['bulk_actions'], 'cb_id' => $id ), "POST" )->set_result( TRUE )->register();
										$this->alerts->persist( TRUE )->alert( __( "Items were successfully deleted." ), __( "Items deleted" ), NULL, 'success' );
									}
									else
									{
										$this->logs->set_data( array( 'bulk_actions' => $_POST['bulk_actions'], 'cb_id' => $id ), "POST" )->set_result( FALSE )->register();
										$this->alerts->persist( TRUE )->alert( __( "There were some errors while trying to perform the task. Please try again." ), __( "Error" ), NULL, 'danger' );
									}
								}
								else
								{
									$this->logs->set_data( array( 'bulk_actions' => $_POST['bulk_actions'], 'cb_id' => $id ), "POST" )->set_result( FALSE )->register();

									if ( $this->ion_auth->is_superior( $id ) )
									{
										$this->alerts->persist( TRUE )->alert( __( "You can't edit an user with a superior role." ), $this->assets->conf['no_permission']['title'], NULL, 'warning' );
									}
									else
									{
										$this->alerts->persist( TRUE )->alert( __( "You can't make these kind of changes to your own account." ), $this->assets->conf['no_permission']['title'], NULL, 'warning' );
									}
								}
							}

							redirect( uri_string() );
						}
						else
						{
							$this->alerts->alert( $this->assets->conf['no_permission']['message'], $this->assets->conf['no_permission']['title'], NULL, 'warning' );
						}
					break;
				} 
			}
		}

		if ( isset( $_GET['action'] ) AND isset( $_GET['user_id'] ) )
		{
			if ( $_GET['user_id'] !== $this->ion_auth->user()->row()->id AND ! $this->ion_auth->is_superior( $_GET['user_id'] ) )
			{
				if ( $_GET['action'] == "restore" AND $this->permissions->can_trash() )
				{
					if ( $this->ion_auth->update( (int)$_GET['user_id'], array( 'active' => 1 ) ) )
					{
						$this->logs->set_data( $_GET, "GET" )->set_result( TRUE )->register();
						$this->alerts->alert( __( "Item successfully restored." ), __( "Item restored" ), NULL, 'success' );
					}
					else
					{
						$this->logs->set_data( $_GET, "GET" )->set_result( FALSE )->register();
						$this->alerts->alert( __( "There were some errors while trying to perform the task. Please try again." ), __( "Error" ), NULL, 'danger' );
					}
				}
				else if ( $_GET['action'] == "delete" AND $this->permissions->can_delete() )
				{
					if ( $this->ion_auth->delete_user( (int)$_GET['user_id'] ) )
					{
						$this->files->erase_file_by_registry( (int)$_GET['user_id'], 'users' );
						$this->logs->set_data( $_GET, "GET" )->set_result( TRUE )->register();
						$this->alerts->alert( __( "Item successfully deleted." ), __( "Item deleted" ), NULL, 'success' );
					}
					else
					{
						$this->logs->set_data( $_GET, "GET" )->set_result( FALSE )->register();
						$this->alerts->alert( __( "There were some errors while trying to perform the task. Please try again." ), __( "Error" ), NULL, 'danger' );				
					}
				}
				else
				{
					$this->alerts->alert( $this->assets->conf['no_permission']['message'], $this->assets->conf['no_permission']['title'], NULL, 'warning' );
				}
			}
			else
			{
				$this->logs->set_data( $_GET, "GET" )->set_result( FALSE )->register();

				if ( $this->ion_auth->is_superior( $_GET['user_id'] ) )
				{
					$this->alerts->alert( __( "You can't edit an user with a superior role." ), $this->assets->conf['no_permission']['title'], NULL, 'warning' );
				}
				else
				{
					$this->alerts->alert( __( "You can't make these kind of changes to your own account." ), $this->assets->conf['no_permission']['title'], NULL, 'warning' );						
				}
			}
		}

		// Order
		if ( $this->input->get( 'order_by' ) AND $this->input->get( 'direction' ) )
		{
			$query_args['order_by'][$this->input->get( 'order_by' )] = $this->input->get( 'direction' );
			$this->db->order_by( $this->input->get( 'order_by' ), $this->input->get( 'direction' ) );
		}
		else
		{
			$query_args['order_by']['id'] = 'DESC';
			$this->db->order_by( 'id', 'DESC' );
		}

		$query_args['where']['active'] = 0;
		$query_args['limit'] = $this->assets->conf['items_per_page'];
		$query_args['offset'] = $this->uri->segment( 3 );

		$this->db->where( 'active', 0 );
		$this->db->limit( $this->assets->conf['items_per_page'], $this->uri->segment( 3 ) );

		$this->data['users'] = $this->ion_auth->users()->result_array();
		$this->data['pagination'] = $this->templates->create_pagination( $this->assets->conf['tables']['users'], $query_args );
		
		$this->templates->reverse_pagination( $this->data['users'] );

		$this->assets->load_script( 'lists.min.js' );
		
		$this->data['tags']['page_title'] = __( "Users Trash" );
		$this->templates->view( 'users/lists', $this->data );
	}

	/**
	 * Item add page
	 *
	 * @return  output
	 */
	public function add()
	{
		if ( $this->input->post() )
		{
			if ( ! $this->permissions->can_add() )
			{
				$this->alerts->persist( TRUE )->alert( $this->assets->conf['no_permission']['message'], $this->assets->conf['no_permission']['title'], NULL, 'warning' );
				redirect( uri_string() );
			}

			$this->form_validation->set_rules( 'first_name', 		__( "First Name" ),			'trim|required' );
			$this->form_validation->set_rules( 'last_name', 		__( "Last Name" ),			'trim|required' );
			$this->form_validation->set_rules( 'language',			__( "Language" ),			'required' );
			$this->form_validation->set_rules( 'email',				__( "Email" ),				'trim|required|valid_email' );

			$this->form_validation->set_rules( 'username',			__( "Username" ),			'required|is_unique[' . $this->assets->conf['tables']['users'] . '.username]|min_length[' . $this->config->item( 'min_password_length', 'ion_auth' ) . ']|max_length[' . $this->config->item( 'max_password_length', 'ion_auth' ) . ']|alpha_numeric' );
			$this->form_validation->set_rules( 'password',			__( "Password" ),			'required|min_length[' . $this->config->item( 'min_password_length', 'ion_auth' ) . ']|max_length[' . $this->config->item( 'max_password_length', 'ion_auth' ) . ']' );
			$this->form_validation->set_rules( 'password_repeat',	__( "Password repeat" ),	'required|matches[password]' );

			if ( $this->form_validation->run() )
			{
				// Vars preparation
				$identity 			= $this->input->post( 'username' );
				$password 			= $this->input->post( 'password' );
				$email 				= $this->input->post( 'email' );
				$groups 			= $this->input->post( 'groups' );
				$additional_data	= array( 
					'first_name' 		=> mb_convert_case( $this->input->post( 'first_name' ), MB_CASE_TITLE, "UTF-8" ),
					'last_name' 		=> mb_convert_case( $this->input->post( 'last_name' ), MB_CASE_TITLE, "UTF-8" ),
					'language' 			=> $this->input->post( 'language' ),
					'phone' 			=> $this->input->post( 'phone' ),
					'created'			=> unix_to_human( now(), TRUE, 'eu' ),
					'creator'			=> $this->ion_auth->user()->row()->id,
					'active'			=> 1,
				);

				if ( $user_id = $this->ion_auth->register( $identity, $password, $email, $additional_data, $groups ) )
				{
					// Send credentials if enabled
					if ( $this->input->post( 'send' ) ) $this->ion_auth->send_credentials( $user_id, $this->input->post() );

					// Associate in case there were any uploaded files
					if ( $this->input->post( 'file_id' ) )
					{
						$this->files->save( array(
							'file_id' 		=> $this->input->post( 'file_id' ),
							'registry_id'	=> $user_id,
						) );
					}

					$_POST['user_id'] = $user_id;
					$this->logs->set_data( $this->input->post(), "POST" )->set_result( TRUE )->register();
					$this->alerts->persist( TRUE )->alert( __( "Item successfully added to the database." ), __( "User created" ), NULL, 'success' );
					redirect( "users/edit/" . $user_id );
				}
				else
				{
					$this->logs->set_data( $this->input->post(), "POST" )->set_result( FALSE )->register();
					$this->alerts->alert( __( "There were some errors while trying to perform the task. Please try again." ), __( "User discarded" ), NULL, 'danger' );
				}
			}
			else
			{
				$this->alerts->alert( __( "Errors appear below every field." ), __( "Item could not be stored into the database" ), NULL, 'danger' );
			}
		}

		$this->data['first_name'] = array(
			'type'		=> 'text',
			'id'		=> 'first_name',
			'name'		=> 'first_name',
			'class'		=> 'form-control form-control-lg',
			'value'		=> set_value( 'first_name' ),
		);

		$this->data['last_name'] = array(
			'type'		=> 'text',
			'id'		=> 'last_name',
			'name'		=> 'last_name',
			'class'		=> 'form-control form-control-lg',
			'value'		=> set_value( 'last_name' ),
		);

		$this->data['language'] = array(
			'id'		=> 'language',
			'name'		=> 'language',
			'class'		=> 'form-control',
			'selected'	=> set_value( 'language' ),
			'options'	=> $this->assets->conf['languages'],
		);

		$this->data['phone'] = array(
			'type'		=> 'text',
			'id'		=> 'phone',
			'name'		=> 'phone',
			'class'		=> 'form-control',
			'value'		=> set_value( 'phone' ),
		);

		$this->data['email'] = array(
			'type'		=> 'text',
			'id'		=> 'email',
			'name'		=> 'email',
			'class'		=> 'form-control',
			'value'		=> set_value( 'email' ),
		);

		$this->data['username'] = array(
			'type'		=> 'text',
			'id'		=> 'username',
			'name'		=> 'username',
			'class'		=> 'form-control input-username',
			'maxlength'	=> $this->config->item( 'max_password_length', 'ion_auth' ),
			'value'		=> set_value( 'username' ),
		);

		$this->data['password'] = array(
			'type'		=> 'password',
			'id'		=> 'password',
			'name'		=> 'password',
			'class'		=> 'form-control',
			'maxlength'	=> $this->config->item( 'max_password_length', 'ion_auth' ),
			'value'		=> '',
		);

		$this->data['password_repeat'] = array(
			'type'		=> 'password',
			'id'		=> 'password_repeat',
			'name'		=> 'password_repeat',
			'class'		=> 'form-control',
			'maxlength'	=> $this->config->item( 'max_password_length', 'ion_auth' ),
			'value'		=> '',
		);

		$this->data['send'] = array(
			'type'		=> 'checkbox',
			'id'		=> 'send',
			'name'		=> 'send',
			'class'		=> 'form-check-input',
			'value'		=> 1,
		);

		$this->data['groups'] = $this->ion_auth->groups()->result_array();

		// jQuery File Uploader assets
		$this->assets->load_style( site_url( 'assets/bower_components/jQuery-File-Upload/css/jquery.fileupload.css' ) );
		$this->assets->load_script( site_url( 'assets/bower_components/jQuery-File-Upload/js/vendor/jquery.ui.widget.js' ) );
		$this->assets->load_script( site_url( 'assets/bower_components/jQuery-File-Upload/js/jquery.iframe-transport.js' ) );
		$this->assets->load_script( site_url( 'assets/bower_components/jQuery-File-Upload/js/jquery.fileupload.js' ) );

		// Inputmask
		$this->assets->load_script( base_url( 'assets/bower_components/inputmask/dist/jquery.inputmask.min.js' ) );

		$this->assets->load_script( 'forms.min.js' );
		$this->assets->load_script( 'forms-users.min.js' );

		$this->data['tags']['page_title'] = __( "Add User" );
		$this->templates->view( 'users/form', $this->data );
	}

	/**
	 * Item edit page
	 * 
	 * @param  int $user_id User ID
	 * @return output
	 */
	public function edit( $user_id )
	{
		if ( $this->ion_auth->is_superior( $user_id ) )
		{
			$this->alerts->persist( TRUE )->alert( __( "You can't edit an user with a superior role." ), $this->assets->conf['no_permission']['title'], NULL, 'warning' );
			redirect( 'users/lists' );
		}

		if ( $this->input->post() )
		{
			if ( ! $this->permissions->can_edit() )
			{
				$this->alerts->persist( TRUE )->alert( $this->assets->conf['no_permission']['message'], $this->assets->conf['no_permission']['title'], NULL, 'warning' );
				redirect( uri_string() );
			}

			// Check username only if we're changing it
			$user_validator = $this->ion_auth->user( $user_id )->row();

			if ( $user_validator AND $this->input->post( 'username' ) !== $user_validator->username )
			{
				$this->form_validation->set_rules( 'username', __( "Username" ), 'trim|required|is_unique[' . $this->db->dbprefix( $this->assets->conf['tables']['users'] ) . '.username]|alpha_numeric' );
			}

			$this->form_validation->set_rules( 'user_id',			__( "User ID" ),				'integer|required' );
			$this->form_validation->set_rules( 'first_name',		__( "First Name" ),				'trim|required' );
			$this->form_validation->set_rules( 'last_name',			__( "Last Name" ),				'trim|required' );
			$this->form_validation->set_rules( 'email',				__( "Email" ),					'trim|required|valid_email' );
			$this->form_validation->set_rules( 'groups[]',			__( "User level" ),				'required' );
			$this->form_validation->set_rules( 'password',			__( "New password" ),			'trim|min_length[' . $this->config->item( 'min_password_length', 'ion_auth' ) . ']|max_length[' . $this->config->item( 'max_password_length', 'ion_auth' ) . ']' );
			$this->form_validation->set_rules( 'password_repeat',	__( "Repeat new password" ),	'trim|matches[password]' );

			if ( $this->form_validation->run() )
			{
				$mail_data = $this->input->post();
				$groups	= $this->input->post( 'groups' );

				$additional_data = array(
					'first_name'	=> mb_convert_case( $this->input->post( 'first_name' ), MB_CASE_TITLE, "UTF-8" ),
					'last_name'		=> mb_convert_case( $this->input->post( 'last_name' ), MB_CASE_TITLE, "UTF-8" ),
					'email'			=> $this->input->post( 'email' ),
					'phone'			=> $this->input->post( 'phone' ),
					'language'		=> $this->input->post( 'language' ),
					'modified'		=> unix_to_human( now(), TRUE, 'eu' ),
					'modifier'		=> $this->ion_auth->user()->row()->id,
				);

				if ( $this->input->post( 'password' ) )
				{
					$additional_data['password'] = $this->input->post( 'password' );
				}

				unset( $_POST['new_password'], $_POST['repeat_new_password'] );

				if ( $this->ion_auth->in_group( array( 'superadmin', 'admin' ) ) )
				{
					//Update the groups user belongs to
					$new_groups = $this->input->post( 'groups' );

					if ( ! empty( $new_groups ) )
					{
						$this->ion_auth->remove_from_group( '', $user_id );

						foreach ( $new_groups AS $group )
						{
							$this->ion_auth->add_to_group( $group, $user_id );
						}
					}
				}

				if ( $this->ion_auth->update( $user_id, $additional_data ) )
				{
					// Send credentials if enabled
					if ( $this->input->post( 'send' ) ) $this->ion_auth->send_credentials( $user_id, $mail_data );

					$this->logs->set_data( $this->input->post(), "POST" )->set_result( TRUE )->register();
					$this->alerts->persist( TRUE )->alert( __( "Item was successfully updated into the database." ), __( "User updated" ), NULL, 'success' );
					redirect( "users/edit/" . $user_id );
				}
				else
				{
					$this->logs->set_data( $this->input->post(), "POST" )->set_result( FALSE )->register();
					$this->alerts->alert( __( "There were some errors while trying to perform the task. Please try again." ), __( "User not updated" ), NULL, 'danger' );
				}
			}
			else
			{
				$this->alerts->alert( __( "Errors appear below every field." ), __( "Item could not be stored into the database" ), NULL, 'danger' );
			}
		}

		$this->data['user'] = $this->ion_auth->user( $user_id )->row_array();

		if ( ! empty( $this->data['user'] ) )
		{
			$this->data['picture_is_set'] = ( ! empty( $this->data['user']['images'] ) ) ? TRUE : FALSE;

			$this->data['user_id'] = array(
				'type'		=> 'hidden',
				'id'		=> 'user_id',
				'name'		=> 'user_id',
				'value'		=> $this->data['user']['id'],
			);

			$this->data['first_name'] = array(
				'type'		=> 'text',
				'id'		=> 'first_name',
				'name'		=> 'first_name',
				'class'		=> 'form-control form-control-lg',
				'value'		=> $this->data['user']['first_name'],
			);

			$this->data['last_name'] = array(
				'type'		=> 'text',
				'id'		=> 'last_name',
				'name'		=> 'last_name',
				'class'		=> 'form-control form-control-lg',
				'value'		=> $this->data['user']['last_name'],
			);

			$this->data['language'] = array(
				'id'		=> 'language',
				'name'		=> 'language',
				'class'		=> 'form-control',
				'selected'	=> $this->data['user']['language'],
				'options'	=> $this->assets->conf['languages'],
			);

			$this->data['phone'] = array(
				'type'		=> 'text',
				'id'		=> 'phone',
				'name'		=> 'phone',
				'class'		=> 'form-control',
				'value'		=> $this->data['user']['phone'],
			);

			$this->data['email'] = array(
				'type'		=> 'text',
				'id'		=> 'email',
				'name'		=> 'email',
				'class'		=> 'form-control',
				'value'		=> $this->data['user']['email'],
			);

			$this->data['username'] = array(
				'type'		=> 'text',
				'id'		=> 'username',
				'name'		=> 'username',
				'class'		=> 'form-control input-username',
				'maxlength'	=> $this->config->item( 'max_password_length', 'ion_auth' ),
				'value'		=> $this->data['user']['username'],
			);

			$this->data['password'] = array(
				'type'		=> 'password',
				'id'		=> 'password',
				'name'		=> 'password',
				'class'		=> 'form-control',
				'maxlength'	=> $this->config->item( 'max_password_length', 'ion_auth' ),
				'value'		=> '',
			);

			$this->data['password_repeat'] = array(
				'type'		=> 'password',
				'id'		=> 'password_repeat',
				'name'		=> 'password_repeat',
				'class'		=> 'form-control',
				'maxlength'	=> $this->config->item( 'max_password_length', 'ion_auth' ),
				'value'		=> '',
			);

			$this->data['send'] = array(
				'type'		=> 'checkbox',
				'id'		=> 'send',
				'name'		=> 'send',
				'class'		=> 'form-check-input',
				'value'		=> 1,
			);

			$this->data['groups']			= $this->ion_auth->groups()->result_array();
			$this->data['selected_groups']	= $this->ion_auth->get_users_groups( $user_id )->result();

			// jQuery File Uploader assets
			$this->assets->load_style( site_url( 'assets/bower_components/jQuery-File-Upload/css/jquery.fileupload.css' ) );
			$this->assets->load_script( site_url( 'assets/bower_components/jQuery-File-Upload/js/vendor/jquery.ui.widget.js' ) );
			$this->assets->load_script( site_url( 'assets/bower_components/jQuery-File-Upload/js/jquery.iframe-transport.js' ) );
			$this->assets->load_script( site_url( 'assets/bower_components/jQuery-File-Upload/js/jquery.fileupload.js' ) );

			// Inputmask
			$this->assets->load_script( base_url( 'assets/bower_components/inputmask/dist/jquery.inputmask.min.js' ) );

			$this->assets->load_script( 'forms.min.js' );
			$this->assets->load_script( 'forms-users.min.js' );

			$this->data['tags']['page_title'] = __( "Edit User" );
			$this->templates->view( 'users/form', $this->data );
		}
		else
		{
			$this->alerts->persist( TRUE )->alert( __( "The user is not stored into the database." ), __( "User does not exist" ), NULL, 'danger' );
			redirect( 'users/lists' );
		}
	}

	/**
	 * The brief page for users
	 * 
	 * @param  int $user_id The User ID
	 * @return output
	 */
	public function brief( $user_id )
	{
		$this->data['user'] = $this->ion_auth->user( $user_id )->row_array();

		if ( ! empty( $this->data['user'] ) )
		{
			if ( ! empty( $this->data['user']['birthday'] ) ) $this->data['user']['age'] = timespan( strtotime( $this->data['user']['birthday'] ), time(), 1 );
			if ( ! empty( $this->data['user']['birthday'] ) ) $this->data['user']['birthday'] = date( $this->assets->conf['date_format'], strtotime( $this->data['user']['birthday'] ) );
			
			$this->data['user'] = $this->assets->fill_empty_vars( $this->data['user'] );
			$this->data['tags']['page_title'] = __( "User Brief" );
			$this->templates->view( 'users/brief', $this->data );
		}
		else
		{
			$this->alerts->persist( TRUE )->alert( __( "The user is not stored into the database." ), __( "User does not exist" ), NULL, 'danger' );
			redirect( 'users/lists' );			
		}
	}
}