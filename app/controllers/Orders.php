<?php defined( 'BASEPATH' ) OR exit( 'No direct script access allowed' );

class Orders extends CI_Controller 
{
	/**
	 * Class constructor
	 */
	public function __construct()
	{
		parent::__construct();

		if ( ! $this->ion_auth->logged_in() ) redirect( 'login', 'refresh' );

		$this->data['tags']['controller_name'] = __( "Orders" );

		$this->load->model( "Orders_model", "orders" );
		$this->load->model( "Products_model", "products" );
		$this->load->model( "Clients_model", "clients" );

		/**
		 * -----------------------------------------------------------------------------------------------
		 * BULK ACTIONS FOR SELECT IN LISTS
		 * -----------------------------------------------------------------------------------------------
		 */

		$bulk_options['-1'] = __( "Bulk actions" );
		if ( $this->uri->segment( 2 ) != 'trash' AND $this->permissions->can_trash() ) $bulk_options['trash'] = __( "Move to Trash" );
		if ( $this->uri->segment( 2 ) != 'trash' AND $this->permissions->can_edit() ) $bulk_options['mark_stocked'] = __( "Mark Stocked" );
		if ( $this->uri->segment( 2 ) != 'trash' AND $this->permissions->can_edit() ) $bulk_options['mark_unstocked'] = __( "Mark Unstocked" );
		if ( $this->uri->segment( 2 ) != 'trash' AND $this->permissions->can_edit() ) $bulk_options['mark_paid'] = __( "Mark Paid" );
		if ( $this->uri->segment( 2 ) != 'trash' AND $this->permissions->can_delete() ) $bulk_options['mark_unpaid'] = __( "Mark Unpaid" );

		if ( $this->uri->segment( 2 ) == 'trash' AND $this->permissions->can_restore() ) $bulk_options['restore'] = __( "Restore" );
		if ( $this->uri->segment( 2 ) == 'trash' AND $this->permissions->can_delete() ) $bulk_options['delete'] = __( "Delete" );
		
		$this->data['bulk_actions'] = array(
			'id'		=> 'bulk_actions',
			'name'		=> 'bulk_actions',
			'class'		=> 'form-control form-control-sm mb-1',
			'options'	=> $bulk_options,
		);

		$this->data['submit_bulk_actions'] = array(
			'id'			=> 'submit_bulk_actions',
			'name'			=> 'submit_bulk_actions',
			'class'			=> 'btn btn-secondary btn-sm btn-bulk mb-1',
			'value'			=> __( "Apply" ),
			'data-confirm'	=> __( "Are you sure you want to perform that action?\n\nIt may be definitive and unable to undo in the future." ),
		);

		/**
		 * -----------------------------------------------------------------------------------------------
		 * LIST TOOLS EXTENSION
		 * -----------------------------------------------------------------------------------------------
		 */
		
		$this->data['list_buttons_extension'] = array(
			'brief' => anchor(
				'orders/brief/%d',
				'<i class="fa fa-search"></i>',
				array(
					'title' => __( "Brief" ),
					'class' => 'brief btn btn-info',
				)
			),
			'sale' => anchor(
				'sales/add/%d',
				__( "Make Sale" ),
				array( 
					'class' => 'dropdown-item' 
				)
			),
			'mark_finished' => anchor(
				uri_string() . '?action=mark_finished&order_id=%d&' . $this->elements->clear_query_string( array( 'action', 'order_id' ), $this->input->server( 'QUERY_STRING' ) ),
				__( "Mark Finished" ),
				array( 
					'class' => 'dropdown-item' 
				)
			),
			'mark_unfinished' => anchor(
				uri_string() . '?action=mark_unfinished&order_id=%d&' . $this->elements->clear_query_string( array( 'action', 'order_id' ), $this->input->server( 'QUERY_STRING' ) ),
				__( "Mark Unfinished" ),
				array( 
					'class' => 'dropdown-item' 
				)
			),
		);
	}

	/**
	 * Index redirecting to lists
	 * 
	 * @return void
	 */
	public function index()
	{
		redirect( 'orders/lists' );
	}

	/**
	 * Items list
	 * 
	 * @return output
	 */
	public function lists()
	{
		if ( $_POST )
		{
			if ( ! empty( $_POST['cb_ids'] ) )
			{
				switch ( $_POST['bulk_actions'] )
				{
					default: break;
					case 'trash': 
						if ( $this->permissions->can_trash() )
						{
							foreach ( $_POST['cb_ids'] AS $id )
							{
								if ( $this->orders->save( array( 'order_id' => (int)$id, 'trash' => 1 ) ) )
								{
									$this->logs->set_data( array( 'bulk_actions' => $_POST['bulk_actions'], 'cb_id' => $id ), "POST" )->set_result( TRUE )->register();
									$this->alerts->persist( TRUE )->alert( __( "Items were successfully trashed." ), __( "Items trashed" ), NULL, 'success' );
								}
								else
								{
									$this->logs->set_data( array( 'bulk_actions' => $_POST['bulk_actions'], 'cb_id' => $id ), "POST" )->set_result( FALSE )->register();
									$this->alerts->persist( TRUE )->alert( __( "There were some errors while trying to perform the task. Please try again." ), __( "Error" ), NULL, 'danger' );
								}
							}

							redirect( uri_string() );
						}
						else
						{
							$this->alerts->alert( $this->assets->conf['no_permission']['message'], $this->assets->conf['no_permission']['title'], NULL, 'warning' );
						}
					break;
					case 'mark_finished':
						if ( $this->permissions->can_edit() )
						{
							foreach ( $_POST['cb_ids'] AS $id )
							{
								if ( $this->orders->save( array( 'order_id' => (int)$id, 'sold' => 1 ) ) )
								{
									$this->logs->set_data( array( 'bulk_actions' => $_POST['bulk_actions'], 'cb_id' => $id ), "POST" )->set_result( TRUE )->register();
									$this->alerts->persist( TRUE )->alert( __( "Items were successfully edited." ), __( "Items marked as finished" ), NULL, 'success' );
								}
								else
								{
									$this->logs->set_data( array( 'bulk_actions' => $_POST['bulk_actions'], 'cb_id' => $id ), "POST" )->set_result( FALSE )->register();
									$this->alerts->persist( TRUE )->alert( __( "There were some errors while trying to perform the task. Please try again." ), __( "Error" ), NULL, 'danger' );
								}
							}

							redirect( uri_string() );
						}
						else
						{
							$this->alerts->alert( $this->assets->conf['no_permission']['message'], $this->assets->conf['no_permission']['title'], NULL, 'warning' );
						}					
					break;
					case 'mark_unfinished':
						if ( $this->permissions->can_edit() )
						{
							foreach ( $_POST['cb_ids'] AS $id )
							{
								if ( $this->orders->save( array( 'order_id' => (int)$id, 'sold' => 0 ) ) )
								{
									$this->logs->set_data( array( 'bulk_actions' => $_POST['bulk_actions'], 'cb_id' => $id ), "POST" )->set_result( TRUE )->register();
									$this->alerts->persist( TRUE )->alert( __( "Items were successfully edited." ), __( "Items marked as unfinished" ), NULL, 'success' );
								}
								else
								{
									$this->logs->set_data( array( 'bulk_actions' => $_POST['bulk_actions'], 'cb_id' => $id ), "POST" )->set_result( FALSE )->register();
									$this->alerts->persist( TRUE )->alert( __( "There were some errors while trying to perform the task. Please try again." ), __( "Error" ), NULL, 'danger' );
								}
							}

							redirect( uri_string() );
						}
						else
						{
							$this->alerts->alert( $this->assets->conf['no_permission']['message'], $this->assets->conf['no_permission']['title'], NULL, 'warning' );
						}
					break;
				} 
			}
		}

		if ( isset( $_GET['action'] ) AND isset( $_GET['order_id'] ) )
		{
			if ( $_GET['action'] == "trash" AND $this->permissions->can_trash() )
			{
				if ( $this->orders->save( array( 'order_id' => (int)$_GET['order_id'], 'trash' => 1 ) ) )
				{
					$this->logs->set_data( $_GET, "GET" )->set_result( TRUE )->register();
					$this->alerts->alert( __( "Item was successfully trashed." ), __( "Item trashed" ), NULL, 'success' );
				}
				else
				{
					$this->logs->set_data( $_GET, "GET" )->set_result( FALSE )->register();
					$this->alerts->alert( __( "There were some errors while trying to perform the task. Please try again." ), __( "Error" ), NULL, 'danger' );
				}
			}
			else if ( $_GET['action'] == "mark_finished" AND $this->permissions->can_edit() )
			{
				if ( $this->orders->save( array( 'order_id' => (int)$_GET['order_id'], 'sold' => 1 ) ) )
				{
					$this->logs->set_data( $_GET, "GET" )->set_result( TRUE )->register();
					$this->alerts->persist( TRUE )->alert( __( "Item was successfully edited." ), __( "Item marked as finished" ), NULL, 'success' );
				}
				else
				{
					$this->logs->set_data( $_GET, "GET" )->set_result( FALSE )->register();
					$this->alerts->alert( __( "There were some errors while trying to perform the task. Please try again." ), __( "Error" ), NULL, 'danger' );
				}
			}
			else if ( $_GET['action'] == "mark_unfinished" AND $this->permissions->can_edit() )
			{
				if ( $this->orders->save( array( 'order_id' => (int)$_GET['order_id'], 'sold' => 0 ) ) )
				{
					$this->logs->set_data( $_GET, "GET" )->set_result( TRUE )->register();
					$this->alerts->persist( TRUE )->alert( __( "Item was successfully edited." ), __( "Item marked as unfinished" ), NULL, 'success' );
				}
				else
				{
					$this->logs->set_data( $_GET, "GET" )->set_result( FALSE )->register();
					$this->alerts->alert( __( "There were some errors while trying to perform the task. Please try again." ), __( "Error" ), NULL, 'danger' );
				}
			}
			else
			{
				$this->alerts->alert( $this->assets->conf['no_permission']['message'], $this->assets->conf['no_permission']['title'], NULL, 'warning' );
			}
		}

		// Order
		if ( $this->input->get( 'order_by' ) AND $this->input->get( 'direction' ) )
		{
			$query_args['order_by'][$this->input->get( 'order_by' )] = $this->input->get( 'direction' );
		}
		else
		{
			$query_args['order_by']['order_id'] = 'DESC';
		}

		$query_args['where']['trash'] = 0;		
		$query_args['limit'] = $this->assets->conf['items_per_page'];
		$query_args['offset'] = $this->uri->segment( 3 );
		$this->data['orders'] = $this->orders->get( $query_args );
		$this->data['pagination'] = $this->templates->create_pagination( $this->assets->conf['tables']['orders'], $query_args );

		$this->templates->reverse_pagination( $this->data['orders'] );
		
		$this->assets->load_script( 'lists.min.js' );
		
		$this->data['tags']['page_title'] = __( "Orders List" );
		$this->templates->view( 'orders/lists', $this->data );
	}

	/**
	 * Items trash
	 * 
	 * @return output
	 */
	public function trash()
	{
		$this->permissions->block_not_admin();
		
		if ( $_POST )
		{
			if ( ! empty( $_POST['cb_ids'] ) )
			{
				switch ( $_POST['bulk_actions'] )
				{
					default: break;
					case 'restore': 
						if ( $this->permissions->can_restore() )
						{
							foreach ( $_POST['cb_ids'] AS $id )
							{
								if ( $this->orders->save( array( 'order_id' => (int)$id, 'trash' => 0 ) ) )
								{
									$this->logs->set_data( array( 'bulk_actions' => $_POST['bulk_actions'], 'cb_id' => $id ), "POST" )->set_result( TRUE )->register();
									$this->alerts->persist( TRUE )->alert( __( "Items were successfully restored." ), __( "Items restored" ), NULL, 'success' );
								}
								else
								{
									$this->logs->set_data( array( 'bulk_actions' => $_POST['bulk_actions'], 'cb_id' => $id ), "POST" )->set_result( FALSE )->register();
									$this->alerts->persist( TRUE )->alert( __( "There were some errors while trying to perform the task. Please try again." ), __( "Error" ), NULL, 'danger' );
								}
							}

							redirect( uri_string() );
						}
						else
						{
							$this->alerts->alert( $this->assets->conf['no_permission']['message'], $this->assets->conf['no_permission']['title'], NULL, 'warning' );
						}
					break;
					case 'delete':
						if ( $this->permissions->can_delete() )
						{
							foreach ( $_POST['cb_ids'] AS $id )
							{
								if ( $this->orders->erase( (int)$id ) )
								{
									$this->logs->set_data( array( 'bulk_actions' => $_POST['bulk_actions'], 'cb_id' => $id ), "POST" )->set_result( TRUE )->register();
									$this->alerts->persist( TRUE )->alert( __( "Items were successfully deleted." ), __( "Items deleted" ), NULL, 'success' );
								}
								else
								{
									$this->logs->set_data( array( 'bulk_actions' => $_POST['bulk_actions'], 'cb_id' => $id ), "POST" )->set_result( FALSE )->register();
									$this->alerts->persist( TRUE )->alert( __( "There were some errors while trying to perform the task. Please try again." ), __( "Error" ), NULL, 'danger' );
								}
							}

							redirect( uri_string() );
						}
						else
						{
							$this->alerts->alert( $this->assets->conf['no_permission']['message'], $this->assets->conf['no_permission']['title'], NULL, 'warning' );
						}
					break;
				} 
			}
		}

		if ( isset( $_GET['action'] ) AND isset( $_GET['order_id'] ) )
		{
			if ( $_GET['action'] == "restore" AND $this->permissions->can_restore() )
			{
				if ( $this->orders->save( array( 'order_id' => (int)$_GET['order_id'], 'trash' => 0 ) ) )
				{
					$this->logs->set_data( $_GET, "GET" )->set_result( TRUE )->register();
					$this->alerts->alert( __( "Item successfully restored." ), __( "Item restored" ), NULL, 'success' );
				}
				else
				{
					$this->logs->set_data( $_GET, "GET" )->set_result( FALSE )->register();
					$this->alerts->alert( __( "There were some errors while trying to perform the task. Please try again." ), __( "Error" ), NULL, 'danger' );				
				}
			}
			else if ( $_GET['action'] == "delete" AND $this->permissions->can_delete() )
			{
				if ( $this->orders->erase( (int)$_GET['order_id'] ) )
				{
					$this->logs->set_data( $_GET, "GET" )->set_result( TRUE )->register();
					$this->alerts->alert( __( "Item successfully deleted." ), __( "Item deleted" ), NULL, 'success' );
				}
				else
				{
					$this->logs->set_data( $_GET, "GET" )->set_result( FALSE )->register();
					$this->alerts->alert( __( "There were some errors while trying to perform the task. Please try again." ), __( "Error" ), NULL, 'danger' );				
				}
			}
			else
			{
				$this->alerts->alert( $this->assets->conf['no_permission']['message'], $this->assets->conf['no_permission']['title'], NULL, 'warning' );
			}
		}

		// Order
		if ( $this->input->get( 'order_by' ) AND $this->input->get( 'direction' ) )
		{
			$query_args['order_by'][$this->input->get( 'order_by' )] = $this->input->get( 'direction' );
		}
		else
		{
			$query_args['order_by']['order_id'] = 'DESC';
		}

		$query_args['where']['trash'] = 1;
		$query_args['limit'] = $this->assets->conf['items_per_page'];
		$query_args['offset'] = $this->uri->segment( 3 );
		$this->data['orders'] = $this->orders->get( $query_args );
		$this->data['pagination'] = $this->templates->create_pagination( $this->assets->conf['tables']['orders'], $query_args );

		$this->templates->reverse_pagination( $this->data['orders'] );
		
		$this->assets->load_script( 'lists.min.js' );
		
		$this->data['tags']['page_title'] = __( "Orders Trash" );
		$this->templates->view( 'orders/lists', $this->data );
	}

	/**
	 * Item add page
	 * 
	 * @return output
	 */
	public function add()
	{
		if ( $this->input->post() )
		{
			if ( ! $this->permissions->can_add() )
			{
				$this->alerts->persist( TRUE )->alert( $this->assets->conf['no_permission']['message'], $this->assets->conf['no_permission']['title'], NULL, 'warning' );
				redirect( uri_string() );
			}

			$this->form_validation->set_rules( 'order_date', __( "Order date" ), 'trim|required' );
			$this->form_validation->set_rules( 'client_id', __( "Client" ), 'trim|required' );
			$this->form_validation->set_rules( 'total_amount', __( "Total amount" ), ( ( $this->assets->conf['decimals'] > 0 ) ? 'decimal' : 'integer' ) . '|required|greater_than[0]' );

			if ( $this->form_validation->run() )
			{
				if ( $order_id = $this->orders->save( $this->input->post( NULL, TRUE ) ) )
				{
					// Associate in case there were any uploaded files
					if ( $this->input->post( 'file_id' ) )
					{
						$this->load->model( "Files_model", "files" );

						foreach ( $this->input->post( 'file_id' ) AS $key => $file_id )
						{
							$this->files->save( array(
								'file_id' => $file_id,
								'registry_id' => $order_id,
							) );
						}
					}

					$_POST['order_id'] = $order_id;
					$this->logs->set_data( $this->input->post(), "POST" )->set_result( TRUE )->register();
					$this->alerts->persist( TRUE )->alert( __( "Item successfully added to the database." ), __( "Order created" ), NULL, 'success' );

					redirect( "orders/edit/" . $order_id );
				}
				else
				{
					$this->logs->set_data( $this->input->post(), "POST" )->set_result( FALSE )->register();
					$this->alerts->alert( __( "There were some errors while trying to perform the task. Please try again." ), __( "Order discarded" ), NULL, 'danger' );
				}
			}
			else
			{
				$this->alerts->alert( __( "Errors appear below every field." ), __( "Item could not be stored into the database" ), NULL, 'danger' );
			}
		}

		$this->data['client_id'] = array(
			'id'		=> 'client_id',
			'name'		=> 'client_id',
			'class'		=> 'form-control',
			'style'		=> 'width: 100%',
			'options'	=> $this->clients->build_dropdown(),
		);

		$this->data['order_date'] = array(
			'type'		=> 'text',
			'id'		=> 'order_date',
			'name'		=> 'order_date',
			'class'		=> 'form-control input-date',
			'value'		=> set_value( 'order_date', date( $this->assets->conf['date_format'] ) ),
		);

		$this->data['delivery_date'] = array(
			'type'		=> 'text',
			'id'		=> 'delivery_date',
			'name'		=> 'delivery_date',
			'class'		=> 'form-control input-date',
			'value'		=> set_value( 'delivery_date' ),
		);

		$this->data['expiration_date'] = array(
			'type'		=> 'text',
			'id'		=> 'expiration_date',
			'name'		=> 'expiration_date',
			'class'		=> 'form-control input-date',
			'value'		=> set_value( 'expiration_date' ),
		);

		$this->data['seller_id'] = array(
			'id'				=> 'seller_id',
			'name'				=> 'seller_id',
			'class'				=> 'form-control',
			'style'				=> 'width: 100%',
			'selected'			=> set_value( 'seller_id', $this->ion_auth->user()->row()->id ),
			'options'			=> $this->ion_auth->build_dropdown(),
			'data-init-value'	=> $this->ion_auth->user()->row()->id,
		);

		$this->data['observations'] = array(
			'name'			=> 'observations',
			'id'			=> 'observations',
			'class'			=> 'form-control',
			'rows'			=> 3,
			'value'			=> set_value( 'observations' ),
			'placeholder'	=> __( "Use this field to store any other information about the order..." ),
		);

		$this->data['total_amount'] = array(
			'type'		=> 'text',
			'id'		=> 'total_amount',
			'name'		=> 'total_amount',
			'readonly'	=> 'readonly',
			'class'		=> 'form-control form-control-lg input-currency',
			'value'		=> set_value( 'total_amount' ),
		);

		// MODAL ------------------------------------------------------------

		$this->data['modal_product_id'] = array(
			'id'		=> 'modal_product_id',
			'name'		=> 'modal_product_id',
			'class'		=> 'form-control',
			'style'		=> 'width: 100%',
			'options'	=> $this->products->build_dropdown(),
		);

		$this->data['modal_unit_price'] = array(
			'type'		=> 'text',
			'id'		=> 'modal_unit_price',
			'name'		=> 'modal_unit_price',
			'class'		=> 'form-control input-currency',
		);

		$this->data['modal_quantity'] = array(
			'type'		=> 'text',
			'id'		=> 'modal_quantity',
			'name'		=> 'modal_quantity',
			'class'		=> 'form-control input-quantity',
		);

		// MODAL - ADD NEW PRODUCT ------------------------------------------

		$this->data['modal_new_product_name'] = array(
			'type'			=> 'text',
			'id'			=> 'modal_new_product_name',
			'name'			=> 'modal_new_product_name',
			'class'			=> 'form-control form-control-lg',
			'autocomplete'	=> 'off',
		);

		$this->data['modal_new_product_category_id'] = array(
			'id'		=> 'modal_new_product_category_id',
			'name'		=> 'modal_new_product_category_id',
			'class'		=> 'form-control',
			'style'		=> 'width: 100%',
			'options'	=> $this->categories->build_dropdown(),
		);

		$this->data['modal_new_product_unit_price'] = array(
			'type'			=> 'text',
			'id'			=> 'modal_new_product_unit_price',
			'name'			=> 'modal_new_product_unit_price',
			'class'			=> 'form-control input-currency',
			'autocomplete'	=> 'off',
		);

		$this->data['modal_new_product_unit_measure'] = array(
			'id'		=> 'modal_new_product_unit_measure',
			'name'		=> 'modal_new_product_unit_measure',
			'class'		=> 'form-control',
			'style'		=> 'width: 100%',
			'options'	=> array_merge( array( '' => '&nbsp;' ), $this->assets->conf['units'] ),
		);

		$this->data['modal_new_product_currency'] = array(
			'type'		=> 'hidden',
			'id'		=> 'modal_new_product_currency',
			'name'		=> 'modal_new_product_currency',
			'value'		=> $this->assets->conf['currency'],
		);

		$this->data['modal_new_product_tax'] = array(
			'id'		=> 'modal_new_product_tax',
			'name'		=> 'modal_new_product_tax',
			'class'		=> 'form-control',
			'style'		=> 'width: 100%',
			'options'	=> array_merge( array( '' => '&nbsp;' ), $this->assets->conf['bill_columns'] ),
		);

		// Pickadate
		$this->assets->load_style( base_url( 'assets/bower_components/pickadate/lib/compressed/themes/default.css' ) );
		$this->assets->load_style( base_url( 'assets/bower_components/pickadate/lib/compressed/themes/default.date.css' ) );
		$this->assets->load_script( base_url( 'assets/bower_components/pickadate/lib/compressed/picker.js' ) );
		$this->assets->load_script( base_url( 'assets/bower_components/pickadate/lib/compressed/picker.date.js' ) );	
		if ( $this->assets->conf['language'] !== "en_US" ) $this->assets->load_script( base_url( 'assets/bower_components/pickadate/lib/translations/' . $this->assets->conf['language'] . '.js' ) );

		// Inputmask
		$this->assets->load_script( base_url( 'assets/bower_components/inputmask/dist/jquery.inputmask.min.js' ) );

		// jQuery File Uploader assets
		$this->assets->load_style( site_url( 'assets/bower_components/jQuery-File-Upload/css/jquery.fileupload.css' ) );
		$this->assets->load_script( site_url( 'assets/bower_components/jQuery-File-Upload/js/vendor/jquery.ui.widget.js' ) );
		$this->assets->load_script( site_url( 'assets/bower_components/jQuery-File-Upload/js/jquery.iframe-transport.js' ) );
		$this->assets->load_script( site_url( 'assets/bower_components/jQuery-File-Upload/js/jquery.fileupload.js' ) );

		// Select2
		$this->assets->load_style( site_url( 'assets/bower_components/select2/dist/css/select2.min.css' ) );
		$this->assets->load_script( site_url( 'assets/bower_components/select2/dist/js/select2.full.min.js' ) );
		$this->assets->load_script( site_url( 'assets/bower_components/select2/dist/js/i18n/' . substr( $this->assets->conf['language'], 0, 2 ) . '.js' ) );

		$this->assets->load_script( 'forms.min.js' );
		$this->assets->load_script( 'forms-orders.min.js' );

		$this->data['tags']['page_title'] = __( "Add Order" );
		$this->templates->view( 'orders/form', $this->data );
	}

	/**
	 * Item edit page
	 * 
	 * @param  int $order_id Order ID
	 * @return output
	 */
	public function edit( $order_id )
	{
		if ( $this->input->post() )
		{
			if ( ! $this->permissions->can_edit() )
			{
				$this->alerts->persist( TRUE )->alert( $this->assets->conf['no_permission']['message'], $this->assets->conf['no_permission']['title'], NULL, 'warning' );
				redirect( uri_string() );
			}

			$this->form_validation->set_rules( 'order_id', __( "Order ID" ), 'integer|required' );
			$this->form_validation->set_rules( 'order_date', __( "Order date" ), 'trim|required' );
			$this->form_validation->set_rules( 'client_id', __( "Client" ), 'trim|required' );
			$this->form_validation->set_rules( 'total_amount', __( "Total amount" ), ( ( $this->assets->conf['decimals'] > 0 ) ? 'decimal' : 'integer' ) . '|required|greater_than[0]' );

			if ( $this->form_validation->run() )
			{
				if ( $this->orders->save( $this->input->post( NULL, TRUE ) ) )
				{
					$this->logs->set_data( $this->input->post(), "POST" )->set_result( TRUE )->register();
					$this->alerts->persist( TRUE )->alert( __( "Item was successfully updated into the database." ), __( "Order updated" ), NULL, 'success' );
					redirect( "orders/edit/" . $order_id );
				}
				else
				{
					$this->logs->set_data( $this->input->post(), "POST" )->set_result( FALSE )->register();
					$this->alerts->alert( __( "There were some errors while trying to perform the task. Please try again." ), __( "Order not updated" ), NULL, 'danger' );
				}
			}
			else
			{
				$this->alerts->alert( __( "Errors appear below every field." ), __( "Item could not be stored into the database" ), NULL, 'danger' );
			}
		}

		$this->data['order'] = $this->orders->get( $order_id );

		if ( ! empty( $this->data['order'] ) )
		{
			$this->data['order_id'] = array(
				'type'		=> 'hidden',
				'id'		=> 'order_id',
				'name'		=> 'order_id',
				'value'		=> $this->data['order']['order_id'],
			);

			$this->data['client_id'] = array(
				'id'		=> 'client_id',
				'name'		=> 'client_id',
				'class'		=> 'form-control',
				'style'		=> 'width: 100%',
				'selected'	=> $this->data['order']['client_id'],
				'options'	=> $this->clients->build_dropdown(),
			);

			$this->data['order_date'] = array(
				'type'		=> 'text',
				'id'		=> 'order_date',
				'name'		=> 'order_date',
				'class'		=> 'form-control input-date',
				'value'		=> ( ! empty( $this->data['order']['order_date'] ) ) ? date( $this->assets->conf['date_format'], strtotime( $this->data['order']['order_date'] ) ) : NULL,
			);

			$this->data['delivery_date'] = array(
				'type'		=> 'text',
				'id'		=> 'delivery_date',
				'name'		=> 'delivery_date',
				'class'		=> 'form-control input-date',
				'value'		=> ( ! empty( $this->data['order']['delivery_date'] ) ) ? date( $this->assets->conf['date_format'], strtotime( $this->data['order']['delivery_date'] ) ) : NULL,
			);

			$this->data['expiration_date'] = array(
				'type'		=> 'text',
				'id'		=> 'expiration_date',
				'name'		=> 'expiration_date',
				'class'		=> 'form-control input-date',
				'value'		=> ( ! empty( $this->data['order']['expiration_date'] ) ) ? date( $this->assets->conf['date_format'], strtotime( $this->data['order']['expiration_date'] ) ) : NULL,
			);

			$this->data['seller_id'] = array(
				'id'				=> 'seller_id',
				'name'				=> 'seller_id',
				'class'				=> 'form-control',
				'style'				=> 'width: 100%',
				'selected'			=> $this->data['order']['seller_id'],
				'options'			=> $this->ion_auth->build_dropdown(),
				'data-init-value'	=> $this->data['order']['seller_id'],
			);

			$this->data['observations'] = array(
				'name'			=> 'observations',
				'id'			=> 'observations',
				'class'			=> 'form-control',
				'rows'			=> 3,
				'value'			=> $this->data['order']['observations'],
				'placeholder'	=> __( "Use this field to store any other information about the order..." ),
			);

			$this->data['total_amount'] = array(
				'type'		=> 'text',
				'id'		=> 'total_amount',
				'name'		=> 'total_amount',
				'readonly'	=> 'readonly',
				'class'		=> 'form-control form-control-lg input-currency',
				'value'		=> my_number_format( $this->data['order']['total_amount'] ),
			);

			// MODAL ------------------------------------------------------------

			$this->data['modal_product_id'] = array(
				'id'		=> 'modal_product_id',
				'name'		=> 'modal_product_id',
				'class'		=> 'form-control',
				'style'		=> 'width: 100%',
				'options'	=> $this->products->build_dropdown(),
			);

			$this->data['modal_unit_price'] = array(
				'type'		=> 'text',
				'id'		=> 'modal_unit_price',
				'name'		=> 'modal_unit_price',
				'class'		=> 'form-control input-currency',
			);

			$this->data['modal_quantity'] = array(
				'type'		=> 'text',
				'id'		=> 'modal_quantity',
				'name'		=> 'modal_quantity',
				'class'		=> 'form-control input-quantity',
			);

			// MODAL - ADD NEW PRODUCT ------------------------------------------

			$this->data['modal_new_product_name'] = array(
				'type'			=> 'text',
				'id'			=> 'modal_new_product_name',
				'name'			=> 'modal_new_product_name',
				'class'			=> 'form-control form-control-lg',
				'autocomplete'	=> 'off',
			);

			$this->data['modal_new_product_category_id'] = array(
				'id'		=> 'modal_new_product_category_id',
				'name'		=> 'modal_new_product_category_id',
				'class'		=> 'form-control',
				'style'		=> 'width: 100%',
				'options'	=> $this->categories->build_dropdown(),
			);

			$this->data['modal_new_product_unit_price'] = array(
				'type'			=> 'text',
				'id'			=> 'modal_new_product_unit_price',
				'name'			=> 'modal_new_product_unit_price',
				'class'			=> 'form-control input-currency',
				'autocomplete'	=> 'off',
			);

			$this->data['modal_new_product_unit_measure'] = array(
				'id'		=> 'modal_new_product_unit_measure',
				'name'		=> 'modal_new_product_unit_measure',
				'class'		=> 'form-control',
				'style'		=> 'width: 100%',
				'options'	=> array_merge( array( '' => '&nbsp;' ), $this->assets->conf['units'] ),
			);

			$this->data['modal_new_product_currency'] = array(
				'type'		=> 'hidden',
				'id'		=> 'modal_new_product_currency',
				'name'		=> 'modal_new_product_currency',
				'value'		=> $this->assets->conf['currency'],
			);

			$this->data['modal_new_product_tax'] = array(
				'id'		=> 'modal_new_product_tax',
				'name'		=> 'modal_new_product_tax',
				'class'		=> 'form-control',
				'style'		=> 'width: 100%',
				'options'	=> array_merge( array( '' => '&nbsp;' ), $this->assets->conf['bill_columns'] ),
			);

			// Pickadate
			$this->assets->load_style( base_url( 'assets/bower_components/pickadate/lib/compressed/themes/default.css' ) );
			$this->assets->load_style( base_url( 'assets/bower_components/pickadate/lib/compressed/themes/default.date.css' ) );
			$this->assets->load_script( base_url( 'assets/bower_components/pickadate/lib/compressed/picker.js' ) );
			$this->assets->load_script( base_url( 'assets/bower_components/pickadate/lib/compressed/picker.date.js' ) );	
			if ( $this->assets->conf['language'] !== "en_US" ) $this->assets->load_script( base_url( 'assets/bower_components/pickadate/lib/translations/' . $this->assets->conf['language'] . '.js' ) );

			// Inputmask
			$this->assets->load_script( base_url( 'assets/bower_components/inputmask/dist/jquery.inputmask.min.js' ) );

			// jQuery File Uploader assets
			$this->assets->load_style( site_url( 'assets/bower_components/jQuery-File-Upload/css/jquery.fileupload.css' ) );
			$this->assets->load_script( site_url( 'assets/bower_components/jQuery-File-Upload/js/vendor/jquery.ui.widget.js' ) );
			$this->assets->load_script( site_url( 'assets/bower_components/jQuery-File-Upload/js/jquery.iframe-transport.js' ) );
			$this->assets->load_script( site_url( 'assets/bower_components/jQuery-File-Upload/js/jquery.fileupload.js' ) );

			// Select2
			$this->assets->load_style( site_url( 'assets/bower_components/select2/dist/css/select2.min.css' ) );
			$this->assets->load_script( site_url( 'assets/bower_components/select2/dist/js/select2.full.min.js' ) );
			$this->assets->load_script( site_url( 'assets/bower_components/select2/dist/js/i18n/' . substr( $this->assets->conf['language'], 0, 2 ) . '.js' ) );

			$this->assets->load_script( 'forms.min.js' );
			$this->assets->load_script( 'forms-orders.min.js' );

			$this->data['tags']['page_title'] = __( "Edit Order" );
			$this->templates->view( 'orders/form', $this->data );
		}
		else
		{
			$this->alerts->persist( TRUE )->alert( __( "The order is not stored into the database." ), __( "Order does not exist" ), NULL, 'danger' );
			redirect( 'orders/lists' );
		}
	}

	/**
	 * Item brief page
	 * 
	 * @param  int $order_id The order id
	 * @return output
	 */
	public function brief( $order_id )
	{
		$this->load->model( "Files_model", "files" );
		$this->data['order'] = $this->orders->get( $order_id );

		if ( ! empty( $this->data['order'] ) )
		{
			$this->data['files'] = $this->files->get_files_src( $this->data['order']['order_id'], 'orders', FALSE );
			$this->data['meta'] = $this->orders->meta_get( $this->data['order']['order_id'] );
			$this->data['order'] = $this->assets->fill_empty_vars( $this->data['order'] );
			$this->data['client'] = $this->assets->fill_empty_vars( $this->clients->get( $this->data['order']['client_id'] ) );
			$this->data['tags']['page_title'] = __( "Order Brief" );
			$this->templates->view( 'orders/brief', $this->data );
		}
		else
		{
			$this->alerts->persist( TRUE )->alert( __( "The order is not stored into the database." ), __( "Order does not exist" ), NULL, 'danger' );
			redirect( 'orders/lists' );
		}
	}

}