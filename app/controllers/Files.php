<?php defined( 'BASEPATH' ) OR exit( 'No direct script access allowed' );

class Files extends CI_Controller
{
	/**
	 * Class constructor
	 */
	public function __construct()
	{
		parent::__construct();

		if ( ! $this->ion_auth->logged_in() ) redirect( 'login', 'refresh' );

		$this->load->library( 'image_lib' );
		$this->load->library( 'upload' );
		$this->load->model( "Files_model", "files" );
	}

	/**
	 * Uploads any file and store it into the given location, thru the POST key called location
	 * 
	 * @return output
	 */
	public function upload()
	{
		// Make sure file is not cached (as it happens for example on iOS devices)
		header( "Expires: Mon, 26 Jul 1997 05:00:00 GMT" );
		header( "Last-Modified: " . gmdate( "D, d M Y H:i:s" ) . " GMT" );
		header( "Cache-Control: no-store, no-cache, must-revalidate" );
		header( "Cache-Control: post-check=0, pre-check=0", false );
		header( "Pragma: no-cache" );

		$to_log['post'] = $_POST;
		$to_log['files'] = $_FILES;

		if ( ! empty( $_FILES ) AND ! empty( $_POST ) )
		{
			$upload_config['upload_path']		= FCPATH . $this->input->post( 'location' );
			$upload_config['file_name']			= time();
			$upload_config['allowed_types']		= ( $this->input->post( 'file_types' ) ) ? $this->input->post( 'file_types' ) : '*';
			$upload_config['file_ext_tolower']	= TRUE;
/*
			$upload_config['max_size']			= ( 5 * 1024 );
			$upload_config['max_width']			= 3000;
			$upload_config['max_height']		= 3000;
			$upload_config['min_width']			= 300;
			$upload_config['min_height']		= 300;
*/
			$this->upload->initialize( $upload_config );

			if ( $this->upload->do_upload( 'file' ) )
			{
				$return = $this->upload->data();

				// Add extra information to the returned data
				$return['controller']	= $this->input->post( 'controller' );
				$return['view_name'] 	= $this->input->post( 'view_name' );

				$to_store = array(
					'relative_location' => $this->input->post( 'location' ),
					'name' 				=> $return['raw_name'],
					'name_ext'			=> $return['file_name'],
					'original_name' 	=> $return['client_name'],
					'size' 				=> (float)$return['file_size'],
					'mimetype' 			=> $return['file_type'],
					'extension' 		=> $return['file_ext'],
					'controller'		=> ( $this->input->post( 'controller' ) ) ? $this->input->post( 'controller' ) : NULL,
					'registry_id'		=> ( $this->input->post( 'registry_id' ) ) ? $this->input->post( 'registry_id' ) : NULL,
				);

				// Store in the DB and add the generated ID to the output
				if ( $file_id = $this->files->save( $to_store ) )
				{
					http_response_code( 200 );
					$return['file_id'] = $file_id;
					$this->logs->set_data( $return, 'POST' )->set_result( TRUE )->register();
					die( json_encode( array( 'message' => __( 'Files successfully uploaded.' ), 'upload' => $return ) ) );
				}
				else
				{
					http_response_code( 500 );
					$this->logs->set_data( $return, 'POST' )->set_result( FALSE )->register();
					die( json_encode( array( 'error' => __( 'There were errors during the upload. Please try again.' ), 'upload' => $return ) ) );
				}

				/*
					data: {
						client_name: 		"nombre.jpg" => old name
						file_ext: 			".jpg"
						file_name: 			"1543951947.jpg"
						file_path: 			"/Volumes/Docs/Webapps/dallas/uploads/avatars/"
						file_size: 			276.36
						file_type: 			"image/jpeg"
						full_path: 			"/Volumes/Docs/Webapps/dallas/uploads/avatars/1543951947.jpg"
						image_height: 		1200
						image_size_str: 	"width="1920" height="1200""
						image_type: 		"jpeg"
						image_width: 		1920
						is_image: 			true
						orig_name: 			"1543951947.jpg"
						raw_name: 			"1543951947"
					}
				*/
			}
			else
			{
				http_response_code( 500 );
				$this->logs->set_data( $to_log, 'POST' )->set_result( FALSE )->register();
				die( json_encode( array( 'error' => sprintf( __( 'File upload failed because: %s Please try again.' ), strip_tags( $this->upload->display_errors() ) ) ) ) );
			}
		}
		else
		{
			http_response_code( 500 );
			$this->logs->set_data( $to_log, 'POST' )->set_result( FALSE )->register();
			die( json_encode( array( 'error' => __( 'There were no files to upload or the files got rejected by the server. Please try again.' ) ) ) );
		}
	}

	/**
	 * Deletes the files associated to the given ID
	 * 
	 * @return output
	 */
	public function delete()
	{
		if ( $this->input->post( 'file_id' ) )
		{
			// Retrieve the remaining ids from the view to get the files again and populate the multiple uploading thingy
			$files_array = NULL;

			if ( $this->input->post( 'remaining_ids' ) )
			{
				foreach ( $this->input->post( 'remaining_ids' ) AS $key => $val )
				{
					$file_data = $this->files->get( $val );

					if ( ! empty( $file_data ) )
					{
						$files_array['files'][] = array_merge( array(
							'src'			=> site_url( $file_data['relative_location'] . $file_data['name'] . "_thumb" . $file_data['extension'] ),
							'src_normal'	=> site_url( $file_data['relative_location'] . $file_data['name_ext'] ),
						), $file_data );
					}
				}
			}

			if ( $this->files->erase( $this->input->post( 'file_id' ) ) )
			{
				http_response_code( 200 );
				$this->logs->set_data( $this->input->post(), 'POST' )->set_result( TRUE )->register();
				die( json_encode( array( 
					'message' => __( "Files successfully deleted." ),
					'html_form' => ( $this->input->post( 'view_name' ) ) ? $this->load->view( $this->input->post( 'view_name' ), $files_array, TRUE ) : NULL,
				) ) );
			}
			else
			{
				http_response_code( 500 );
				$this->logs->set_data( $this->input->post(), 'POST' )->set_result( FALSE )->register();
				die( json_encode( array( 'error' => __( 'There were errors during the delete process. Please try again.' ) ) ) );
			}
		}
		else
		{
			http_response_code( 500 );
			die( json_encode( array( 'error' => __( "You didn't specify a file ID." ) ) ) );
		}
	}

	/**
	 * Process the uploaded file in case it is an image
	 * 
	 * @return output
	 */
	public function picture_processing()
	{
		if ( $this->input->post() )
		{
			$uploaded_image = $this->input->post();

			if ( $this->input->post( 'new_upload' ) ) $uploaded_image = $this->input->post( 'new_upload' ); // In case it's coming from a multiple image scenario

			if ( strpos( $uploaded_image['file_type'], 'image' ) !== FALSE )
			{
				$new_size = ( $uploaded_image['image_width'] > $uploaded_image['image_height'] ) ? $uploaded_image['image_height'] : $uploaded_image['image_width'];

				// Make thumbnail first by cropping it
				$thumb_config['maintain_ratio']	= FALSE;
				$thumb_config['source_image']	= $uploaded_image['full_path'];
				$thumb_config['y_axis'] 		= ( $uploaded_image['image_height'] - $new_size ) / 2;
				$thumb_config['x_axis'] 		= ( $uploaded_image['image_width'] - $new_size ) / 2;
				$thumb_config['width']			= $new_size;
				$thumb_config['height']			= $new_size;
				$thumb_config['create_thumb']	= TRUE;

				$this->image_lib->initialize( $thumb_config );
				$this->image_lib->crop();
				$this->image_lib->clear();

				// Now resize the thumbnail
				$thumb_config['source_image']	= $uploaded_image['file_path'] . $uploaded_image['raw_name'] . "_thumb" . $uploaded_image['file_ext'];
				$thumb_config['width']			= 300;
				$thumb_config['height']			= 300;
				$thumb_config['maintain_ratio']	= TRUE;
				$thumb_config['create_thumb']	= FALSE;
				$this->image_lib->initialize( $thumb_config );
				$this->image_lib->resize();
				$this->image_lib->clear();

				// Resize main image
				$img_config['source_image']		= $uploaded_image['full_path'];
				$img_config['maintain_ratio']	= TRUE;
				$img_config['width']			= ( $uploaded_image['image_width'] >= 1024 ) ? 1024 : $uploaded_image['image_width'] > 1024;
				$img_config['height']			= ( $uploaded_image['image_height'] >= 1024 ) ? 1024 : $uploaded_image['image_height'] > 1024;

				$this->image_lib->initialize( $img_config );
				$this->image_lib->resize();
				$this->image_lib->clear();

				$file_data = $this->files->get( $this->input->post( 'file_id' ) );

				http_response_code( 200 );
				$this->logs->set_data( $this->input->post(), 'POST' )->set_result( TRUE )->register();
				
				/**
				 * Prepare the html to return that will replace the whole operation...
				 * If we're using 'new_upload' it means we needed the alter the responseJSON.upload
				 * to add the existant ids, hence, it is a multiple file mode.
				 */
				if ( ! $this->input->post( 'new_upload' ) )
				{
					$view_data['file'] = array_merge( array(
						'src' 			=> site_url( $file_data['relative_location'] . $file_data['name'] . "_thumb" . $file_data['extension'] ),
						'src_normal' 	=> site_url( $file_data['relative_location'] . $file_data['name_ext'] ),
					), $file_data );

					$html_form = $this->load->view( 'files/model-single-picture', $view_data, TRUE );
				}
				else
				{
					$files_retrieving = array();

					if ( $this->input->post( 'existant_ids' ) )
					{
						foreach ( $this->input->post( 'existant_ids' ) AS $file_id )
						{
							$file_data = $this->files->get( $file_id );

							if ( ! empty( $file_data ) )
							{
								$files_retrieving[] = array_merge( array(
									'src' 			=> site_url( $file_data['relative_location'] . $file_data['name'] . "_thumb" . $file_data['extension'] ),
									'src_normal' 	=> site_url( $file_data['relative_location'] . $file_data['name_ext'] ),
								), $file_data );
							}
						}
					}

					// Get the latest upload last
					$latest_upload = $this->files->get( $uploaded_image['file_id'] );
					$files_retrieving[] = array_merge( array(
						'src' 			=> site_url( $latest_upload['relative_location'] . $latest_upload['name'] . "_thumb" . $latest_upload['extension'] ),
						'src_normal' 	=> site_url( $latest_upload['relative_location'] . $latest_upload['name_ext'] ),
					), $latest_upload );
					
					$html_form = $this->load->view( $uploaded_image['view_name'], array(
						'files' => $files_retrieving
					), TRUE );
				}
				
				die( json_encode( array( 
					'message' 	=> __( 'Image successfully processed.' ),
					'html_form' => $html_form,
				) ) );
			}
			else
			{
				http_response_code( 500 );
				$this->logs->set_data( $this->input->post(), 'POST' )->set_result( FALSE )->register();
				die( json_encode( array( 'error' => __( 'The file is not an image.' ) ) ) );
			}
		}
	}

	/**
	 * Outputs the given post upload for use on the javascript side of 'multiple' uploading
	 * 
	 * @return output
	 */
	public function multiple_files_retrieving()
	{
		if ( $this->input->post() )
		{
			$files_array = array();

			if ( $this->input->post( 'new_upload' ) ) $files_array['files'][] = $this->files->get( $this->input->post( 'new_upload' )['file_id'] );

			// Get the files that already were uploaded in the screen
			if ( $this->input->post( 'existant_ids' ) )
			{
				foreach ( $this->input->post( 'existant_ids' ) AS $key => $val )
				{
					$files_array['files'][] = $this->files->get( $val );
				}
			}

			// Add src
			foreach ( $files_array['files'] AS $key => $file )
			{
				$files_array['files'][$key]['src'] = site_url( $file['relative_location'] . $file['name'] . "_thumb" . $file['extension'] );
				if ( strpos( $file['mimetype'], 'image' ) !== FALSE ) $files_array['files'][$key]['src_normal'] = site_url( $file['relative_location'] . $file['name_ext'] );
			}

			http_response_code( 200 );
			die( json_encode( array( 
				'message' 	=> __( 'Files successfully stored.' ),
				'html_form' => $this->load->view( 'files/model-multiple-files', $files_array, TRUE ),
			) ) );
		}
		else
		{
			http_response_code( 500 );
			$this->logs->set_data( $this->input->post(), 'POST' )->set_result( FALSE )->register();
			// $this->alerts->persist( TRUE )->alert( __( 'The are no files to get.' ), NULL, NULL, 'danger' );
			die( json_encode( array( 'error' => __( 'The are no files to get.' ) ) ) );
		}
	}

	/**
	 * Deletes the files associated to the given ID
	 * 
	 * @return output
	 */
	public function delete_on_multiple()
	{
		if ( $this->input->post( 'file_id' ) )
		{
			$files_array = array();

			if ( $this->input->post( 'remaining_ids' ) )
			{
				foreach ( $this->input->post( 'remaining_ids' ) AS $id )
				{
					$files_array['files'][] = $this->files->get( $id );
				}
			}

			if ( $this->files->erase( $this->input->post( 'file_id' ) ) )
			{
				http_response_code( 200 );
				$this->logs->set_data( $this->input->post(), 'POST' )->set_result( TRUE )->register();
				die( json_encode( array( 
					'message' => __( "Files successfully deleted." ),
					'html_form' => ( $this->input->post( 'view_name' ) ) ? $this->load->view( $this->input->post( 'view_name' ), $files_array, TRUE ) : NULL,
				) ) );
			}
			else
			{
				http_response_code( 500 );
				$this->logs->set_data( $this->input->post(), 'POST' )->set_result( FALSE )->register();
				die( json_encode( array( 'error' => __( 'There were errors during the delete process. Please try again.' ) ) ) );
			}
		}
		else
		{
			http_response_code( 500 );
			die( json_encode( array( 'error' => __( "You didn't specify a file ID." ) ) ) );
		}
	}

	/**
	 * Forces the download of a file
	 * 
	 * @return output
	 */
	public function download( $file_id )
	{
		$file = $this->files->get( $file_id );

		if ( ! empty( $file ) )
		{
			$absolute_uri = FCPATH . ltrim( $file['relative_location'], "/" ) . $file['name_ext'];

			if ( is_file( $absolute_uri ) )
			{
				$this->load->helper( 'download' );
				force_download( $file['original_name'], file_get_contents( $absolute_uri ), $file['mimetype'] );
				die( -1 );
			}
			else
			{
				// http_response_code( 500 );
				// die( json_encode( array( 'error' => __( "The file does not exist." ) ) ) );
				$this->alerts->persist( TRUE )->alert( __( 'The file does not exist.' ), NULL, NULL, 'warning' );
				if ( ! $this->agent->is_referral() ) redirect( $this->agent->referrer(), 'refresh' );
			}
		}

		// http_response_code( 500 );
		// die( json_encode( array( 'error' => __( "The file does not exist." ) ) ) );
		$this->alerts->persist( TRUE )->alert( __( 'The file does not exist.' ), NULL, NULL, 'warning' );
		if ( ! $this->agent->is_referral() ) redirect( $this->agent->referrer(), 'refresh' );
	}

	/**
	 * Reorders the list of sortables in a multiple picture upload environment
	 * 
	 * @return output
	 */
	public function reorder_files()
	{
		if ( $this->input->post( 'existant_ids' ) )
		{
			$this->files->reorder_filelist( $this->input->post( 'existant_ids' ) );
		}
	}
}