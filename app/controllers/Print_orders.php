<?php defined( 'BASEPATH' ) OR exit( 'No direct script access allowed' );

class Print_orders extends CI_Controller
{
	/**
	 * Class constructor
	 */
	public function __construct()
	{
		parent::__construct();

		if ( ! $this->ion_auth->logged_in() ) redirect( 'login', 'refresh' );

		$this->load->model( "Print_orders_model", "print_orders" );
		$this->load->model( "Pos_model", "pos" );

		$this->data['tags']['controller_name'] = __( "Print Orders" );

		/**
		 * -----------------------------------------------------------------------------------------------
		 * BULK ACTIONS FOR SELECT IN LISTS
		 * -----------------------------------------------------------------------------------------------
		 */

		$bulk_options['-1'] = __( "Bulk actions" );
		if ( $this->uri->segment( 2 ) != 'trash' AND $this->permissions->can_trash() ) $bulk_options['trash'] = __( "Move to Trash" );
		if ( $this->uri->segment( 2 ) == 'trash' AND $this->permissions->can_restore() ) $bulk_options['restore'] = __( "Restore" );
		if ( $this->uri->segment( 2 ) == 'trash' AND $this->permissions->can_delete() ) $bulk_options['delete'] = __( "Delete" );
		
		$this->data['bulk_actions'] = array(
			'id'		=> 'bulk_actions',
			'name'		=> 'bulk_actions',
			'class'		=> 'form-control form-control-sm mb-1',
			'options'	=> $bulk_options,
		);

		$this->data['submit_bulk_actions'] = array(
			'id'			=> 'submit_bulk_actions',
			'name'			=> 'submit_bulk_actions',
			'class'			=> 'btn btn-secondary btn-sm btn-bulk mb-1',
			'value'			=> __( "Apply" ),
			'data-confirm'	=> __( "Are you sure you want to perform that action?\n\nIt may be definitive and unable to undo in the future." ),
		);

		/**
		 * -----------------------------------------------------------------------------------------------
		 * LIST TOOLS EXTENSION
		 * -----------------------------------------------------------------------------------------------
		 */
		
		$this->data['list_buttons_extension'] = array(
			'brief' => anchor(
				'print_orders/brief/%d',
				'<i class="fa fa-search"></i>',
				array(
					'title' => __( "Brief" ),
					'class' => 'brief btn btn-info',
				)
			),
		);
	}

	/**
	 * Index redirecting to lists
	 * 
	 * @return void
	 */
	public function index()
	{
		redirect( 'print_orders/lists' );
	}

	/**
	 * Items list
	 * 
	 * @return output
	 */
	public function lists()
	{
		if ( $_POST )
		{
			if ( ! empty( $_POST['cb_ids'] ) )
			{
				switch ( $_POST['bulk_actions'] )
				{
					default: break;
					case 'trash': 
						if ( $this->permissions->can_trash() )
						{
							foreach ( $_POST['cb_ids'] AS $id )
							{
								if ( $this->print_orders->save( array( 'print_order_id' => (int)$id, 'trash' => 1 ) ) )
								{
									$this->logs->set_data( array( 'bulk_actions' => $_POST['bulk_actions'], 'cb_id' => $id ), "POST" )->set_result( TRUE )->register();
									$this->alerts->persist( TRUE )->alert( __( "Items were successfully trashed." ), __( "Items trashed" ), NULL, 'success' );
								}
								else
								{
									$this->logs->set_data( array( 'bulk_actions' => $_POST['bulk_actions'], 'cb_id' => $id ), "POST" )->set_result( FALSE )->register();
									$this->alerts->persist( TRUE )->alert( __( "There were some errors while trying to perform the task. Please try again." ), __( "Error" ), NULL, 'danger' );
								}
							}

							redirect( uri_string() );
						}
						else
						{
							$this->alerts->alert( $this->assets->conf['no_permission']['message'], $this->assets->conf['no_permission']['title'], NULL, 'warning' );
						}
					break;
				} 
			}
		}

		if ( isset( $_GET['action'] ) AND isset( $_GET['print_order_id'] ) )
		{
			if ( $_GET['action'] == "trash" AND $this->permissions->can_trash() )
			{
				if ( $this->print_orders->save( array( 'print_order_id' => (int)$_GET['print_order_id'], 'trash' => 1 ) ) )
				{
					$this->logs->set_data( $_GET, "GET" )->set_result( TRUE )->register();
					$this->alerts->alert( __( "Item was successfully trashed." ), __( "Item trashed" ), NULL, 'success' );
				}
				else
				{
					$this->logs->set_data( $_GET, "GET" )->set_result( FALSE )->register();
					$this->alerts->alert( __( "There were some errors while trying to perform the task. Please try again." ), __( "Error" ), NULL, 'danger' );
				}
			}
			else
			{
				$this->alerts->alert( $this->assets->conf['no_permission']['message'], $this->assets->conf['no_permission']['title'], NULL, 'warning' );
			}
		}

		// Order
		if ( $this->input->get( 'order_by' ) AND $this->input->get( 'direction' ) )
		{
			$query_args['order_by'][$this->input->get( 'order_by' )] = $this->input->get( 'direction' );
		}
		else
		{
			$query_args['order_by']['print_order_id'] = 'DESC';
		}

		$query_args['where']['trash'] = 0;
		$query_args['limit'] = $this->assets->conf['items_per_page'];
		$query_args['offset'] = $this->uri->segment( 3 );
		$this->data['print_orders'] = $this->print_orders->get( $query_args );
		$this->data['pagination'] = $this->templates->create_pagination( $this->assets->conf['tables']['print_orders'], $query_args );

		$this->templates->reverse_pagination( $this->data['print_orders'] );
		
		$this->assets->load_script( 'lists.min.js' );
		
		$this->data['tags']['page_title'] = __( "Print Orders List" );
		$this->data['tags']['extra'] = anchor( 'print_orders/add', '<i class="fa fa-plus-circle"></i> ' . __( "Add Print Order" ), array( 'class' => 'btn btn-success btn-sm ml-2' ) );
		$this->templates->view( 'print_orders/lists', $this->data );
	}

	/**
	 * Items trash
	 * 
	 * @return output
	 */
	public function trash()
	{
		$this->permissions->block_not_admin();
		
		if ( $_POST )
		{
			if ( ! empty( $_POST['cb_ids'] ) )
			{
				switch ( $_POST['bulk_actions'] )
				{
					default: break;
					case 'restore': 
						if ( $this->permissions->can_restore() )
						{
							foreach ( $_POST['cb_ids'] AS $id )
							{
								if ( $this->print_orders->save( array( 'print_order_id' => $id, 'trash' => 0 ) ) )
								{
									$this->logs->set_data( array( 'bulk_actions' => $_POST['bulk_actions'], 'cb_id' => $id ), "POST" )->set_result( TRUE )->register();
									$this->alerts->persist( TRUE )->alert( __( "Items were successfully restored." ), __( "Items restored" ), NULL, 'success' );
								}
								else
								{
									$this->logs->set_data( array( 'bulk_actions' => $_POST['bulk_actions'], 'cb_id' => $id ), "POST" )->set_result( FALSE )->register();
									$this->alerts->persist( TRUE )->alert( __( "There were some errors while trying to perform the task. Please try again." ), __( "Error" ), NULL, 'danger' );
								}
							}

							redirect( uri_string() );
						}
						else
						{
							$this->alerts->alert( $this->assets->conf['no_permission']['message'], $this->assets->conf['no_permission']['title'], NULL, 'warning' );
						}
					break;
					case 'delete':
						if ( $this->permissions->can_delete() )
						{
							foreach ( $_POST['cb_ids'] AS $id )
							{
								if ( $this->print_orders->erase( array( 'print_order_id' => $id ) ) )
								{
									$this->logs->set_data( array( 'bulk_actions' => $_POST['bulk_actions'], 'cb_id' => $id ), "POST" )->set_result( TRUE )->register();
									$this->alerts->persist( TRUE )->alert( __( "Items were successfully deleted." ), __( "Items deleted" ), NULL, 'success' );
								}
								else
								{
									$this->logs->set_data( array( 'bulk_actions' => $_POST['bulk_actions'], 'cb_id' => $id ), "POST" )->set_result( FALSE )->register();
									$this->alerts->persist( TRUE )->alert( __( "There were some errors while trying to perform the task. Please try again." ), __( "Error" ), NULL, 'danger' );
								}
							}

							redirect( uri_string() );
						}
						else
						{
							$this->alerts->alert( $this->assets->conf['no_permission']['message'], $this->assets->conf['no_permission']['title'], NULL, 'warning' );
						}
					break;
				} 
			}
		}

		if ( isset( $_GET['action'] ) AND isset( $_GET['print_order_id'] ) )
		{
			if ( $_GET['action'] == "restore" AND $this->permissions->can_restore() )
			{
				if ( $this->print_orders->save( array( 'print_order_id' => (int)$_GET['print_order_id'], 'trash' => 0 ) ) )
				{
					$this->logs->set_data( $_GET, "GET" )->set_result( TRUE )->register();
					$this->alerts->alert( __( "Item successfully restored." ), __( "Item restored" ), NULL, 'success' );
				}
				else
				{
					$this->logs->set_data( $_GET, "GET" )->set_result( FALSE )->register();
					$this->alerts->alert( __( "There were some errors while trying to perform the task. Please try again." ), __( "Error" ), NULL, 'danger' );				
				}
			}
			else if ( $_GET['action'] == "delete" AND $this->permissions->can_delete() )
			{
				if ( $this->print_orders->erase( (int)$_GET['print_order_id'] ) )
				{
					$this->logs->set_data( $_GET, "GET" )->set_result( TRUE )->register();
					$this->alerts->alert( __( "Item successfully deleted." ), __( "Item deleted" ), NULL, 'success' );
				}
				else
				{
					$this->logs->set_data( $_GET, "GET" )->set_result( FALSE )->register();
					$this->alerts->alert( __( "There were some errors while trying to perform the task. Please try again." ), __( "Error" ), NULL, 'danger' );				
				}
			}
			else
			{
				$this->alerts->alert( $this->assets->conf['no_permission']['message'], $this->assets->conf['no_permission']['title'], NULL, 'warning' );
			}
		}

		// Order
		if ( $this->input->get( 'order_by' ) AND $this->input->get( 'direction' ) )
		{
			$query_args['order_by'][$this->input->get( 'order_by' )] = $this->input->get( 'direction' );
		}
		else
		{
			$query_args['order_by']['print_order_id'] = 'DESC';
		}

		$query_args['where']['trash'] = 1;
		$query_args['limit'] = $this->assets->conf['items_per_page'];
		$query_args['offset'] = $this->uri->segment( 3 );
		$this->data['print_orders'] = $this->print_orders->get( $query_args );
		$this->data['pagination'] = $this->templates->create_pagination( $this->assets->conf['tables']['print_orders'], $query_args );

		$this->templates->reverse_pagination( $this->data['print_orders'] );
		
		$this->assets->load_script( 'lists.min.js' );
		
		$this->data['tags']['page_title'] = __( "Print Orders Trash" );
		$this->templates->view( 'print_orders/lists', $this->data );
	}

	/**
	 * Item add page
	 * 
	 * @return output
	 */
	public function add()
	{
		if ( $this->input->post() )
		{
			if ( ! $this->permissions->can_add() )
			{
				$this->alerts->persist( TRUE )->alert( $this->assets->conf['no_permission']['message'], $this->assets->conf['no_permission']['title'], NULL, 'warning' );
				redirect( uri_string() );
			}

			$this->form_validation->set_rules( 'print_order', __( "Print order" ), 'trim|required|is_unique[' . $this->assets->conf['tables']['print_orders'] . '.print_order]|exact_length[8]', array( 'is_unique' => __( "The print order was used already. This value has to be unique." ) ) );
			$this->form_validation->set_rules( 'pos_id', __( "POS" ), 'trim|required' );
			$this->form_validation->set_rules( 'date_start', __( "Date start" ), 'trim|required' );
			$this->form_validation->set_rules( 'date_end', __( "Date end" ), 'trim|required|date_greater_than[' . $this->input->post( 'date_start' ) . ']' );
			$this->form_validation->set_rules( 'location', __( "Location" ), 'trim|required|integer' );
			$this->form_validation->set_rules( 'expedition', __( "Expedition" ), 'trim|required|integer' );
			$this->form_validation->set_rules( 'number_start', __( "Number start" ), 'trim|required|integer' );
			$this->form_validation->set_rules( 'number_end', __( "Number end" ), 'trim|required|integer|greater_than[' . $this->input->post( 'number_start' ) . ']' );

			if ( $this->form_validation->run() )
			{
				if ( $print_order_id = $this->print_orders->save( $this->input->post( NULL, TRUE ) ) )
				{
					$_POST['print_order_id'] = $print_order_id;
					$this->logs->set_data( $this->input->post(), "POST" )->set_result( TRUE )->register();
					$this->alerts->persist( TRUE )->alert( __( "Item successfully added to the database." ), __( "Print Order created" ), NULL, 'success' );

					redirect( "print_orders/edit/" . $print_order_id );
				}
				else
				{
					$this->logs->set_data( $this->input->post(), "POST" )->set_result( FALSE )->register();
					$this->alerts->alert( __( "There were some errors while trying to perform the task. Please try again." ), __( "Print Order discarded" ), NULL, 'danger' );
				}
			}
			else
			{
				$this->alerts->alert( __( "Errors appear below every field." ), __( "Item could not be stored into the database" ), NULL, 'danger' );
			}
		}

		$this->data['print_order'] = array(
			'type'		=> 'text',
			'id'		=> 'print_order',
			'name'		=> 'print_order',
			'class'		=> 'form-control form-control-lg input-raw-integer',
			'maxlength'	=> 8,
			'value'		=> set_value( 'print_order' ),
		);

		$this->data['pos_id'] = array(
			'id'		=> 'pos_id',
			'name'		=> 'pos_id',
			'class'		=> 'form-control',
			'style'		=> 'width: 100%',
			'selected'	=> set_value( 'pos_id' ),
			'options'	=> $this->pos->build_dropdown(),
		);

		$this->data['date_start'] = array(
			'type'		=> 'text',
			'id'		=> 'date_start',
			'name'		=> 'date_start',
			'class'		=> 'form-control input-date',
			'value'		=> set_value( 'date_start' ),
		);

		$this->data['date_end'] = array(
			'type'		=> 'text',
			'id'		=> 'date_end',
			'name'		=> 'date_end',
			'class'		=> 'form-control input-date',
			'value'		=> set_value( 'date_end' ),
		);

		$this->data['location'] = array(
			'type'		=> 'text',
			'id'		=> 'location',
			'name'		=> 'location',
			'class'		=> 'form-control input-raw-integer',
			'maxlength'	=> 3,
			'value'		=> set_value( 'location' ),
		);

		$this->data['expedition'] = array(
			'type'		=> 'text',
			'id'		=> 'expedition',
			'name'		=> 'expedition',
			'class'		=> 'form-control input-raw-integer',
			'maxlength'	=> 3,
			'value'		=> set_value( 'expedition' ),
		);

		$this->data['number_start'] = array(
			'type'		=> 'text',
			'id'		=> 'number_start',
			'name'		=> 'number_start',
			'class'		=> 'form-control input-integer',
			'value'		=> set_value( 'number_start' ),
		);

		$this->data['number_end'] = array(
			'type'		=> 'text',
			'id'		=> 'number_end',
			'name'		=> 'number_end',
			'class'		=> 'form-control input-integer',
			'value'		=> set_value( 'number_end' ),
		);

		// Pickadate
		$this->assets->load_style( base_url( 'assets/bower_components/pickadate/lib/compressed/themes/default.css' ) );
		$this->assets->load_style( base_url( 'assets/bower_components/pickadate/lib/compressed/themes/default.date.css' ) );
		$this->assets->load_script( base_url( 'assets/bower_components/pickadate/lib/compressed/picker.js' ) );
		$this->assets->load_script( base_url( 'assets/bower_components/pickadate/lib/compressed/picker.date.js' ) );	
		if ( $this->assets->conf['language'] !== "en_US" ) $this->assets->load_script( base_url( 'assets/bower_components/pickadate/lib/translations/' . $this->assets->conf['language'] . '.js' ) );

		// Inputmask
		$this->assets->load_script( base_url( 'assets/bower_components/inputmask/dist/jquery.inputmask.min.js' ) );

		// Select2
		$this->assets->load_style( site_url( 'assets/bower_components/select2/dist/css/select2.min.css' ) );
		$this->assets->load_script( site_url( 'assets/bower_components/select2/dist/js/select2.full.min.js' ) );
		$this->assets->load_script( site_url( 'assets/bower_components/select2/dist/js/i18n/' . substr( $this->assets->conf['language'], 0, 2 ) . '.js' ) );

		$this->assets->load_script( 'forms.min.js' );
		$this->assets->load_script( 'forms-print-orders.min.js' );

		$this->data['tags']['page_title'] = __( "Add Print Order" );
		$this->templates->view( 'print_orders/form', $this->data );
	}

	/**
	 * Item edit page
	 * 
	 * @param  int $print_order_id
	 * @return output
	 */
	public function edit( $print_order_id )
	{
		if ( $this->input->post() )
		{
			if ( ! $this->permissions->can_edit() )
			{
				$this->alerts->persist( TRUE )->alert( $this->assets->conf['no_permission']['message'], $this->assets->conf['no_permission']['title'], NULL, 'warning' );
				redirect( uri_string() );
			}

			$print_order_rules = array(
				'trim',
				'required',
				'exact_length[8]',
			);

			$pre_data = $this->print_orders->get( $print_order_id );

			if ( $pre_data['print_order'] !== $this->input->post( 'print_order' ) )
			{
				$print_order_rules[] = 'is_unique[' . $this->assets->conf['tables']['print_orders'] . '.print_order]';
			}

			$this->form_validation->set_rules( 'print_order_id', __( "Print Order ID" ), 'integer|required' );
			$this->form_validation->set_rules( 'print_order', __( "Print order" ), $print_order_rules, array( 'is_unique' => __( "The print order was used already. This value has to be unique." ) ) );
			$this->form_validation->set_rules( 'pos_id', __( "POS" ), 'trim|required' );
			$this->form_validation->set_rules( 'date_start', __( "Date start" ), 'trim|required' );
			$this->form_validation->set_rules( 'date_end', __( "Date end" ), 'trim|required|date_greater_than[' . $this->input->post( 'date_start' ) . ']' );
			$this->form_validation->set_rules( 'location', __( "Location" ), 'trim|required|integer' );
			$this->form_validation->set_rules( 'expedition', __( "Expedition" ), 'trim|required|integer' );
			$this->form_validation->set_rules( 'number_start', __( "Number start" ), 'trim|required|integer' );
			$this->form_validation->set_rules( 'number_end', __( "Number end" ), 'trim|required|integer|greater_than[' . $this->input->post( 'number_start' ) . ']' );

			if ( $this->form_validation->run() )
			{
				if ( $this->print_orders->save( $this->input->post( NULL, TRUE ) ) )
				{
					$this->logs->set_data( $this->input->post(), "POST" )->set_result( TRUE )->register();
					$this->alerts->persist( TRUE )->alert( __( "Item was successfully updated into the database." ), __( "Print Order updated" ), NULL, 'success' );
					redirect( "print_orders/edit/" . $print_order_id );
				}
				else
				{
					$this->logs->set_data( $this->input->post(), "POST" )->set_result( FALSE )->register();
					$this->alerts->alert( __( "There were some errors while trying to perform the task. Please try again." ), __( "Print Order not updated" ), NULL, 'danger' );
				}
			}
			else
			{
				$this->alerts->alert( __( "Errors appear below every field." ), __( "Item could not be stored into the database" ), NULL, 'danger' );
			}
		}

		$this->data['print_order_item'] = $this->print_orders->get( $print_order_id );

		if ( ! empty( $this->data['print_order_item'] ) )
		{
			$this->data['print_order_id'] = array(
				'type'		=> 'hidden',
				'id'		=> 'print_order_id',
				'name'		=> 'print_order_id',
				'value'		=> $this->data['print_order_item']['print_order_id'],
			);

			$this->data['print_order'] = array(
				'type'		=> 'text',
				'id'		=> 'print_order',
				'name'		=> 'print_order',
				'class'		=> 'form-control form-control-lg input-raw-integer',
				'maxlength'	=> 8,
				'value'		=> $this->data['print_order_item']['print_order'],
			);

			$this->data['pos_id'] = array(
				'id'		=> 'pos_id',
				'name'		=> 'pos_id',
				'class'		=> 'form-control',
				'style'		=> 'width: 100%',
				'selected'	=> $this->data['print_order_item']['pos_id'],
				'options'	=> $this->pos->build_dropdown(),
			);

			$this->data['date_start'] = array(
				'type'		=> 'text',
				'id'		=> 'date_start',
				'name'		=> 'date_start',
				'class'		=> 'form-control input-date',
				'value'		=> ( ! empty( $this->data['print_order_item']['date_start'] ) ) ? date( $this->assets->conf['date_format'], strtotime( $this->data['print_order_item']['date_start'] ) ) : NULL,
			);

			$this->data['date_end'] = array(
				'type'		=> 'text',
				'id'		=> 'date_end',
				'name'		=> 'date_end',
				'class'		=> 'form-control input-date',
				'value'		=> ( ! empty( $this->data['print_order_item']['date_end'] ) ) ? date( $this->assets->conf['date_format'], strtotime( $this->data['print_order_item']['date_end'] ) ) : NULL,
			);

			$this->data['location'] = array(
				'type'		=> 'text',
				'id'		=> 'location',
				'name'		=> 'location',
				'class'		=> 'form-control input-raw-integer',
				'maxlength'	=> 3,
				'value'		=> $this->data['print_order_item']['location'],
			);

			$this->data['expedition'] = array(
				'type'		=> 'text',
				'id'		=> 'expedition',
				'name'		=> 'expedition',
				'class'		=> 'form-control input-raw-integer',
				'maxlength'	=> 3,
				'value'		=> $this->data['print_order_item']['expedition'],
			);

			$this->data['number_start'] = array(
				'type'		=> 'text',
				'id'		=> 'number_start',
				'name'		=> 'number_start',
				'class'		=> 'form-control input-integer',
				'value'		=> $this->data['print_order_item']['number_start'],
			);

			$this->data['number_end'] = array(
				'type'		=> 'text',
				'id'		=> 'number_end',
				'name'		=> 'number_end',
				'class'		=> 'form-control input-integer',
				'value'		=> $this->data['print_order_item']['number_end'],
			);

			// Pickadate
			$this->assets->load_style( base_url( 'assets/bower_components/pickadate/lib/compressed/themes/default.css' ) );
			$this->assets->load_style( base_url( 'assets/bower_components/pickadate/lib/compressed/themes/default.date.css' ) );
			$this->assets->load_script( base_url( 'assets/bower_components/pickadate/lib/compressed/picker.js' ) );
			$this->assets->load_script( base_url( 'assets/bower_components/pickadate/lib/compressed/picker.date.js' ) );	
			if ( $this->assets->conf['language'] !== "en_US" ) $this->assets->load_script( base_url( 'assets/bower_components/pickadate/lib/translations/' . $this->assets->conf['language'] . '.js' ) );

			// Inputmask
			$this->assets->load_script( base_url( 'assets/bower_components/inputmask/dist/jquery.inputmask.min.js' ) );

			// Select2
			$this->assets->load_style( site_url( 'assets/bower_components/select2/dist/css/select2.min.css' ) );
			$this->assets->load_script( site_url( 'assets/bower_components/select2/dist/js/select2.full.min.js' ) );
			$this->assets->load_script( site_url( 'assets/bower_components/select2/dist/js/i18n/' . substr( $this->assets->conf['language'], 0, 2 ) . '.js' ) );

			$this->assets->load_script( 'forms.min.js' );
			$this->assets->load_script( 'forms-print-orders.min.js' );

			$this->data['tags']['page_title'] = __( "Edit Print Order" );
			$this->templates->view( 'print_orders/form', $this->data );
		}
		else
		{
			$this->alerts->persist( TRUE )->alert( __( "The Print Order is not stored into the database." ), __( "Print Order does not exist" ), NULL, 'danger' );
			redirect( 'print_orders/lists' );
		}
	}

	/**
	 * Item brief page
	 * 
	 * @param  int $print_order_id
	 * @return output
	 */
	public function brief( $print_order_id )
	{
		$this->data['item'] = $this->print_orders->get( $print_order_id );

		if ( ! empty( $this->data['item'] ) )
		{
			$this->data['item'] = $this->assets->fill_empty_vars( $this->data['item'], 'print_orders' );
			$this->data['pos'] = $this->assets->fill_empty_vars( $this->pos->get( $this->data['item']['pos_id'] ), 'pos' );
			$this->data['tags']['page_title'] = __( "Print Order Brief" );
			$this->templates->view( 'print_orders/brief', $this->data );
		}
		else
		{
			$this->alerts->persist( TRUE )->alert( __( "The Print Order is not stored into the database." ), __( "Print Order does not exist" ), NULL, 'danger' );
			redirect( 'print_orders/lists' );			
		}
	}

}