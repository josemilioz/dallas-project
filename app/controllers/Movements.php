<?php defined( 'BASEPATH' ) OR exit( 'No direct script access allowed' );

class Movements extends CI_Controller 
{
	/**
	 * Class constructor
	 */
	public function __construct()
	{
		parent::__construct();

		if ( ! $this->ion_auth->logged_in() ) redirect( 'login', 'refresh' );

		$this->data['tags']['controller_name'] = __( "Movements" );

		$this->load->model( "Movements_model", "movements" );

		/**
		 * -----------------------------------------------------------------------------------------------
		 * BULK ACTIONS FOR SELECT IN LISTS
		 * -----------------------------------------------------------------------------------------------
		 */

		$bulk_options['-1'] = __( "Bulk actions" );
		if ( $this->uri->segment( 2 ) != 'trash' AND $this->permissions->can_trash() ) $bulk_options['trash'] = __( "Move to Trash" );
		if ( $this->uri->segment( 2 ) == 'trash' AND $this->permissions->can_restore() ) $bulk_options['restore'] = __( "Restore" );
		if ( $this->uri->segment( 2 ) == 'trash' AND $this->permissions->can_delete() ) $bulk_options['delete'] = __( "Delete" );
		
		$this->data['bulk_actions'] = array(
			'id'		=> 'bulk_actions',
			'name'		=> 'bulk_actions',
			'class'		=> 'form-control form-control-sm mb-1',
			'options'	=> $bulk_options,
		);

		$this->data['submit_bulk_actions'] = array(
			'id'			=> 'submit_bulk_actions',
			'name'			=> 'submit_bulk_actions',
			'class'			=> 'btn btn-secondary btn-sm btn-bulk mb-1',
			'value'			=> __( "Apply" ),
			'data-confirm'	=> __( "Are you sure you want to perform that action?\n\nIt may be definitive and unable to undo in the future." ),
		);

		/**
		 * -----------------------------------------------------------------------------------------------
		 * LIST TOOLS EXTENSION
		 * -----------------------------------------------------------------------------------------------
		 */
		
		$this->data['list_buttons_extension'] = array(
			'brief' => anchor(
				'movements/brief/%d',
				'<i class="fa fa-search"></i>',
				array(
					'title' => __( "Brief" ),
					'class' => 'brief btn btn-info',
				)
			),
		);
	}

	/**
	 * Index redirecting to lists
	 * 
	 * @return void
	 */
	public function index()
	{
		redirect( 'movements/lists' );
	}

	/**
	 * Items list
	 * 
	 * @return output
	 */
	public function lists()
	{
		if ( $_POST )
		{
			if ( ! empty( $_POST['cb_ids'] ) )
			{
				switch ( $_POST['bulk_actions'] )
				{
					default: break;
					case 'trash': 
						if ( $this->permissions->can_trash() )
						{
							foreach ( $_POST['cb_ids'] AS $id )
							{
								if ( $this->movements->save( array( 'movement_id' => (int)$id, 'trash' => 1 ) ) )
								{
									$this->logs->set_data( array( 'bulk_actions' => $_POST['bulk_actions'], 'cb_id' => $id ), "POST" )->set_result( TRUE )->register();
									$this->alerts->persist( TRUE )->alert( __( "Items were successfully trashed." ), __( "Items trashed" ), NULL, 'success' );
								}
								else
								{
									$this->logs->set_data( array( 'bulk_actions' => $_POST['bulk_actions'], 'cb_id' => $id ), "POST" )->set_result( FALSE )->register();
									$this->alerts->persist( TRUE )->alert( __( "There were some errors while trying to perform the task. Please try again." ), __( "Error" ), NULL, 'danger' );
								}
							}

							redirect( uri_string() );
						}
						else
						{
							$this->alerts->alert( $this->assets->conf['no_permission']['message'], $this->assets->conf['no_permission']['title'], NULL, 'warning' );
						}
					break;
				} 
			}
		}

		if ( isset( $_GET['action'] ) AND isset( $_GET['movement_id'] ) )
		{
			if ( $_GET['action'] == "trash" AND $this->permissions->can_trash() )
			{
				if ( $this->movements->save( array( 'movement_id' => (int)$_GET['movement_id'], 'trash' => 1 ) ) )
				{
					$this->logs->set_data( $_GET, "GET" )->set_result( TRUE )->register();
					$this->alerts->alert( __( "Item was successfully trashed." ), __( "Item trashed" ), NULL, 'success' );
				}
				else
				{
					$this->logs->set_data( $_GET, "GET" )->set_result( FALSE )->register();
					$this->alerts->alert( __( "There were some errors while trying to perform the task. Please try again." ), __( "Error" ), NULL, 'danger' );
				}
			}
			else
			{
				$this->alerts->alert( $this->assets->conf['no_permission']['message'], $this->assets->conf['no_permission']['title'], NULL, 'warning' );
			}
		}

		// Order
		if ( $this->input->get( 'order_by' ) AND $this->input->get( 'direction' ) )
		{
			$query_args['order_by'][$this->input->get( 'order_by' )] = $this->input->get( 'direction' );
		}
		else
		{
			$query_args['order_by']['movement_id'] = 'DESC';
		}

		$query_args['where']['trash'] = 0;		
		$query_args['limit'] = $this->assets->conf['items_per_page'];
		$query_args['offset'] = $this->uri->segment( 3 );
		$this->data['movements'] = $this->movements->get( $query_args );
		$this->data['pagination'] = $this->templates->create_pagination( $this->assets->conf['tables']['movements'], $query_args );

		$this->templates->reverse_pagination( $this->data['movements'] );
		
		$this->assets->load_script( 'lists.min.js' );
		
		$this->data['tags']['page_title'] = __( "Movements List" );
		$this->templates->view( 'movements/lists', $this->data );
	}

	/**
	 * Items trash
	 * 
	 * @return output
	 */
	public function trash()
	{
		$this->permissions->block_not_admin();
		
		if ( $_POST )
		{
			if ( ! empty( $_POST['cb_ids'] ) )
			{
				switch ( $_POST['bulk_actions'] )
				{
					default: break;
					case 'restore': 
						if ( $this->permissions->can_restore() )
						{
							foreach ( $_POST['cb_ids'] AS $id )
							{
								if ( $this->movements->save( array( 'movement_id' => $id, 'trash' => 0 ) ) )
								{
									$this->logs->set_data( array( 'bulk_actions' => $_POST['bulk_actions'], 'cb_id' => $id ), "POST" )->set_result( TRUE )->register();
									$this->alerts->persist( TRUE )->alert( __( "Items were successfully restored." ), __( "Items restored" ), NULL, 'success' );
								}
								else
								{
									$this->logs->set_data( array( 'bulk_actions' => $_POST['bulk_actions'], 'cb_id' => $id ), "POST" )->set_result( FALSE )->register();
									$this->alerts->persist( TRUE )->alert( __( "There were some errors while trying to perform the task. Please try again." ), __( "Error" ), NULL, 'danger' );
								}
							}

							redirect( uri_string() );
						}
						else
						{
							$this->alerts->alert( $this->assets->conf['no_permission']['message'], $this->assets->conf['no_permission']['title'], NULL, 'warning' );
						}
					break;
					case 'delete':
						if ( $this->permissions->can_delete() )
						{
							foreach ( $_POST['cb_ids'] AS $id )
							{
								if ( $this->movements->erase( array( 'movement_id' => $id ) ) )
								{
									$this->logs->set_data( array( 'bulk_actions' => $_POST['bulk_actions'], 'cb_id' => $id ), "POST" )->set_result( TRUE )->register();
									$this->alerts->persist( TRUE )->alert( __( "Items were successfully deleted." ), __( "Items deleted" ), NULL, 'success' );
								}
								else
								{
									$this->logs->set_data( array( 'bulk_actions' => $_POST['bulk_actions'], 'cb_id' => $id ), "POST" )->set_result( FALSE )->register();
									$this->alerts->persist( TRUE )->alert( __( "There were some errors while trying to perform the task. Please try again." ), __( "Error" ), NULL, 'danger' );
								}
							}

							redirect( uri_string() );
						}
						else
						{
							$this->alerts->alert( $this->assets->conf['no_permission']['message'], $this->assets->conf['no_permission']['title'], NULL, 'warning' );
						}
					break;
				} 
			}
		}

		if ( isset( $_GET['action'] ) AND isset( $_GET['movement_id'] ) )
		{
			if ( $_GET['action'] == "restore" AND $this->permissions->can_restore() )
			{
				if ( $this->movements->save( array( 'movement_id' => (int)$_GET['movement_id'], 'trash' => 0 ) ) )
				{
					$this->logs->set_data( $_GET, "GET" )->set_result( TRUE )->register();
					$this->alerts->alert( __( "Item successfully restored." ), __( "Item restored" ), NULL, 'success' );
				}
				else
				{
					$this->logs->set_data( $_GET, "GET" )->set_result( FALSE )->register();
					$this->alerts->alert( __( "There were some errors while trying to perform the task. Please try again." ), __( "Error" ), NULL, 'danger' );				
				}
			}
			else if ( $_GET['action'] == "delete" AND $this->permissions->can_delete() )
			{
				if ( $this->movements->erase( (int)$_GET['movement_id'] ) )
				{
					$this->logs->set_data( $_GET, "GET" )->set_result( TRUE )->register();
					$this->alerts->alert( __( "Item successfully deleted." ), __( "Item deleted" ), NULL, 'success' );
				}
				else
				{
					$this->logs->set_data( $_GET, "GET" )->set_result( FALSE )->register();
					$this->alerts->alert( __( "There were some errors while trying to perform the task. Please try again." ), __( "Error" ), NULL, 'danger' );				
				}
			}
			else
			{
				$this->alerts->alert( $this->assets->conf['no_permission']['message'], $this->assets->conf['no_permission']['title'], NULL, 'warning' );
			}
		}

		// Order
		if ( $this->input->get( 'order_by' ) AND $this->input->get( 'direction' ) )
		{
			$query_args['order_by'][$this->input->get( 'order_by' )] = $this->input->get( 'direction' );
		}
		else
		{
			$query_args['order_by']['movement_id'] = 'DESC';
		}

		$query_args['where']['trash'] = 1;
		$query_args['limit'] = $this->assets->conf['items_per_page'];
		$query_args['offset'] = $this->uri->segment( 3 );
		$this->data['movements'] = $this->movements->get( $query_args );
		$this->data['pagination'] = $this->templates->create_pagination( $this->assets->conf['tables']['movements'], $query_args );

		$this->templates->reverse_pagination( $this->data['movements'] );
		
		$this->assets->load_script( 'lists.min.js' );
		
		$this->data['tags']['page_title'] = __( "Movements Trash" );
		$this->templates->view( 'movements/lists', $this->data );
	}

	/**
	 * Item add page
	 * 
	 * @return output
	 */
	public function add()
	{
		if ( $this->input->post() )
		{
			if ( ! $this->permissions->can_add() )
			{
				$this->alerts->persist( TRUE )->alert( $this->assets->conf['no_permission']['message'], $this->assets->conf['no_permission']['title'], NULL, 'warning' );
				redirect( uri_string() );
			}

			$this->form_validation->set_rules( 'date', __( "Date" ), 'trim|required' );
			$this->form_validation->set_rules( 'label', __( "Label" ), 'trim|required' );
			$this->form_validation->set_rules( 'type', __( "Type" ), 'trim|required' );
			$this->form_validation->set_rules( 'amount', __( "Amount" ), ( ( $this->assets->conf['decimals'] > 0 ) ? 'decimal' : 'integer' ) . '|required|greater_than[0]' );

			if ( $this->form_validation->run() )
			{
				if ( $movement_id = $this->movements->save( $this->input->post( NULL, TRUE ) ) )
				{
					// Associate in case there were any uploaded files
					if ( $this->input->post( 'file_id' ) )
					{
						$this->load->model( "Files_model", "files" );

						foreach ( $this->input->post( 'file_id' ) AS $key => $file_id )
						{
							$this->files->save( array(
								'file_id' => $file_id,
								'registry_id' => $movement_id,
							) );
						}
					}

					$_POST['movement_id'] = $movement_id;
					$this->logs->set_data( $this->input->post(), "POST" )->set_result( TRUE )->register();
					$this->alerts->persist( TRUE )->alert( __( "Item successfully added to the database." ), __( "Movement created" ), NULL, 'success' );

					redirect( "movements/edit/" . $movement_id );
				}
				else
				{
					$this->logs->set_data( $this->input->post(), "POST" )->set_result( FALSE )->register();
					$this->alerts->alert( __( "There were some errors while trying to perform the task. Please try again." ), __( "Movement discarded" ), NULL, 'danger' );
				}
			}
			else
			{
				$this->alerts->alert( __( "Errors appear below every field." ), __( "Item could not be stored into the database" ), NULL, 'danger' );
			}
		}

		$this->data['date'] = array(
			'type'		=> 'text',
			'id'		=> 'date',
			'name'		=> 'date',
			'class'		=> 'form-control input-date',
			'value'		=> set_value( 'date', date( $this->assets->conf['date_format'] ) ),
		);

		$this->data['type'] = array(
			'name'			=> 'type',
			'id'			=> 'type',
			'class'			=> 'form-control form-control-lg',
			'selected'		=> set_value( 'type' ),
			'options'		=> $this->assets->conf['movements'],
		);

		$this->data['label'] = array(
			'name'			=> 'label',
			'id'			=> 'label',
			'class'			=> 'form-control',
			'style'			=> 'width: 100%',
			'selected'		=> set_value( 'label' ),
			'options'		=> $this->movements->build_labels_dropdown(),
		);

		$this->data['book'] = array(
			'name'			=> 'book',
			'id'			=> 'book',
			'class'			=> 'form-control',
			'style'			=> 'width: 100%',
			'selected'		=> set_value( 'book' ),
			'options'		=> $this->movements->build_labels_dropdown( 'book' ),
		);

		$this->data['amount'] = array(
			'name'			=> 'amount',
			'id'			=> 'amount',
			'class'			=> 'form-control form-control-lg input-currency',
			'value'			=> set_value( 'amount' ),
			'data-currency'	=> $this->assets->conf['currency'],
		);

		$this->data['comments'] = array(
			'name'			=> 'comments',
			'id'			=> 'comments',
			'class'			=> 'form-control',
			'rows'			=> 3,
			'value'			=> set_value( 'comments' ),
			'placeholder'	=> __( "Use this field to store any other information about the transaction..." ),
		);

		$this->data['assignee'] = array(
			'name'			=> 'assignee',
			'id'			=> 'assignee',
			'class'			=> 'form-control',
			'style'			=> 'width: 100%',
			'selected'		=> set_value( 'assignee' ),
			'options'		=> $this->ion_auth->build_dropdown(),
		);

		// Pickadate
		$this->assets->load_style( base_url( 'assets/bower_components/pickadate/lib/compressed/themes/default.css' ) );
		$this->assets->load_style( base_url( 'assets/bower_components/pickadate/lib/compressed/themes/default.date.css' ) );
		$this->assets->load_script( base_url( 'assets/bower_components/pickadate/lib/compressed/picker.js' ) );
		$this->assets->load_script( base_url( 'assets/bower_components/pickadate/lib/compressed/picker.date.js' ) );	
		if ( $this->assets->conf['language'] !== "en_US" ) $this->assets->load_script( base_url( 'assets/bower_components/pickadate/lib/translations/' . $this->assets->conf['language'] . '.js' ) );

		// Inputmask
		$this->assets->load_script( base_url( 'assets/bower_components/inputmask/dist/jquery.inputmask.min.js' ) );

		// jQuery File Uploader assets
		$this->assets->load_style( site_url( 'assets/bower_components/jQuery-File-Upload/css/jquery.fileupload.css' ) );
		$this->assets->load_script( site_url( 'assets/bower_components/jQuery-File-Upload/js/vendor/jquery.ui.widget.js' ) );
		$this->assets->load_script( site_url( 'assets/bower_components/jQuery-File-Upload/js/jquery.iframe-transport.js' ) );
		$this->assets->load_script( site_url( 'assets/bower_components/jQuery-File-Upload/js/jquery.fileupload.js' ) );

		// Select2
		$this->assets->load_style( site_url( 'assets/bower_components/select2/dist/css/select2.min.css' ) );
		$this->assets->load_script( site_url( 'assets/bower_components/select2/dist/js/select2.full.min.js' ) );
		$this->assets->load_script( site_url( 'assets/bower_components/select2/dist/js/i18n/' . substr( $this->assets->conf['language'], 0, 2 ) . '.js' ) );

		$this->assets->load_script( 'forms.min.js' );
		$this->assets->load_script( 'forms-movements.min.js' );

		$this->data['tags']['page_title'] = __( "Add Movement" );
		$this->templates->view( 'movements/form', $this->data );
	}

	/**
	 * Item edit page
	 * 
	 * @param  int $movement_id Movement ID
	 * @return output
	 */
	public function edit( $movement_id )
	{
		if ( $this->input->post() )
		{
			if ( ! $this->permissions->can_edit() )
			{
				$this->alerts->persist( TRUE )->alert( $this->assets->conf['no_permission']['message'], $this->assets->conf['no_permission']['title'], NULL, 'warning' );
				redirect( uri_string() );
			}

			$this->form_validation->set_rules( 'movement_id', __( "Movement ID" ), 'integer|required' );
			$this->form_validation->set_rules( 'date', __( "Date" ), 'trim|required' );
			$this->form_validation->set_rules( 'label', __( "Label" ), 'trim|required' );
			$this->form_validation->set_rules( 'type', __( "Type" ), 'trim|required' );
			$this->form_validation->set_rules( 'amount', __( "Amount" ), 'required|greater_than[0]|' . ( ( $this->assets->conf['decimals'] > 0 ) ? 'decimal' : 'integer' ) );

			if ( $this->form_validation->run() )
			{
				if ( $this->movements->save( $this->input->post( NULL, TRUE ) ) )
				{
					$this->logs->set_data( $this->input->post(), "POST" )->set_result( TRUE )->register();
					$this->alerts->persist( TRUE )->alert( __( "Item was successfully updated into the database." ), __( "Movement updated" ), NULL, 'success' );
					redirect( "movements/edit/" . $movement_id );
				}
				else
				{
					$this->logs->set_data( $this->input->post(), "POST" )->set_result( FALSE )->register();
					$this->alerts->alert( __( "There were some errors while trying to perform the task. Please try again." ), __( "Movement not updated" ), NULL, 'danger' );
				}
			}
			else
			{
				$this->alerts->alert( __( "Errors appear below every field." ), __( "Item could not be stored into the database" ), NULL, 'danger' );
			}
		}

		$this->data['movement'] = $this->movements->get( $movement_id );

		if ( ! empty( $this->data['movement'] ) )
		{
			$this->data['movement_id'] = array(
				'type'		=> 'hidden',
				'id'		=> 'movement_id',
				'name'		=> 'movement_id',
				'value'		=> $this->data['movement']['movement_id'],
			);

			$this->data['date'] = array(
				'type'		=> 'text',
				'id'		=> 'date',
				'name'		=> 'date',
				'class'		=> 'form-control input-date',
				'value'		=> ( ! empty( $this->data['movement']['date'] ) ) ? date( $this->assets->conf['date_format'], strtotime( $this->data['movement']['date'] ) ) : NULL,
			);

			$this->data['type'] = array(
				'name'			=> 'type',
				'id'			=> 'type',
				'class'			=> 'form-control form-control-lg',
				'selected'		=> $this->data['movement']['type'],
				'options'		=> $this->assets->conf['movements'],
			);

			$this->data['label'] = array(
				'name'			=> 'label',
				'id'			=> 'label',
				'class'			=> 'form-control',
				'style'			=> 'width: 100%',
				'selected'		=> $this->data['movement']['label'],
				'options'		=> $this->movements->build_labels_dropdown(),
			);

			$this->data['book'] = array(
				'name'			=> 'book',
				'id'			=> 'book',
				'class'			=> 'form-control',
				'style'			=> 'width: 100%',
				'selected'		=> $this->data['movement']['book'],
				'options'		=> $this->movements->build_labels_dropdown( 'book' ),
			);

			$this->data['amount'] = array(
				'name'			=> 'amount',
				'id'			=> 'amount',
				'class'			=> 'form-control form-control-lg input-currency',
				'value'			=> my_number_format( $this->data['movement']['amount'] ),
				'data-currency'	=> $this->assets->conf['currency'],
			);

			$this->data['comments'] = array(
				'name'			=> 'comments',
				'id'			=> 'comments',
				'class'			=> 'form-control',
				'rows'			=> 3,
				'value'			=> $this->data['movement']['comments'],
				'placeholder'	=> __( "Use this field to store any other information about the transaction..." ),
			);

			$this->data['assignee'] = array(
				'name'			=> 'assignee',
				'id'			=> 'assignee',
				'class'			=> 'form-control',
				'style'			=> 'width: 100%',
				'selected'		=> $this->data['movement']['assignee'],
				'options'		=> $this->ion_auth->build_dropdown(),
			);

			// Pickadate
			$this->assets->load_style( base_url( 'assets/bower_components/pickadate/lib/compressed/themes/default.css' ) );
			$this->assets->load_style( base_url( 'assets/bower_components/pickadate/lib/compressed/themes/default.date.css' ) );
			$this->assets->load_script( base_url( 'assets/bower_components/pickadate/lib/compressed/picker.js' ) );
			$this->assets->load_script( base_url( 'assets/bower_components/pickadate/lib/compressed/picker.date.js' ) );	
			if ( $this->assets->conf['language'] !== "en_US" ) $this->assets->load_script( base_url( 'assets/bower_components/pickadate/lib/translations/' . $this->assets->conf['language'] . '.js' ) );

			// Inputmask
			$this->assets->load_script( base_url( 'assets/bower_components/inputmask/dist/jquery.inputmask.min.js' ) );

			// jQuery File Uploader assets
			$this->assets->load_style( site_url( 'assets/bower_components/jQuery-File-Upload/css/jquery.fileupload.css' ) );
			$this->assets->load_script( site_url( 'assets/bower_components/jQuery-File-Upload/js/vendor/jquery.ui.widget.js' ) );
			$this->assets->load_script( site_url( 'assets/bower_components/jQuery-File-Upload/js/jquery.iframe-transport.js' ) );
			$this->assets->load_script( site_url( 'assets/bower_components/jQuery-File-Upload/js/jquery.fileupload.js' ) );

			// Select2
			$this->assets->load_style( site_url( 'assets/bower_components/select2/dist/css/select2.min.css' ) );
			$this->assets->load_script( site_url( 'assets/bower_components/select2/dist/js/select2.full.min.js' ) );
			$this->assets->load_script( site_url( 'assets/bower_components/select2/dist/js/i18n/' . substr( $this->assets->conf['language'], 0, 2 ) . '.js' ) );

			$this->assets->load_script( 'forms.min.js' );
			$this->assets->load_script( 'forms-movements.min.js' );

			$this->data['tags']['page_title'] = __( "Edit Movement" );
			$this->templates->view( 'movements/form', $this->data );
		}
		else
		{
			$this->alerts->persist( TRUE )->alert( __( "The movement is not stored into the database." ), __( "Movement does not exist" ), NULL, 'danger' );
			redirect( 'movements/lists' );
		}
	}

	/**
	 * Item brief page
	 * 
	 * @param  int $movement_id The movement id
	 * @return output
	 */
	public function brief( $movement_id )
	{
		$this->data['movement'] = $this->movements->get( $movement_id );

		if ( ! empty( $this->data['movement'] ) )
		{
			$this->load->model( "Files_model", "files" );
			$this->data['files'] = $this->files->get_files_src( $this->data['movement']['movement_id'], 'movements', FALSE );

			$this->data['movement'] = $this->assets->fill_empty_vars( $this->data['movement'] );
			$this->data['tags']['page_title'] = __( "Movement Brief" );
			$this->templates->view( 'movements/brief', $this->data );
		}
		else
		{
			$this->alerts->persist( TRUE )->alert( __( "The movement is not stored into the database." ), __( "Movement does not exist" ), NULL, 'danger' );
			redirect( 'movements/lists' );
		}
	}

}