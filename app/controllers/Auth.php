<?php defined( 'BASEPATH' ) OR exit( 'No direct script access allowed' );

class Auth extends CI_Controller 
{

	public function __construct()
	{
		parent::__construct();
		$this->load->database();
		$this->load->library( array( 'ion_auth', 'form_validation' ) );
	}

	/**
	 * Login view
	 * 
	 * @return none
	 */
	public function login()
	{
		// If logged in, then redirect to main controller and page
		if ( $this->ion_auth->logged_in() ) redirect( "/", 'refresh' );

		$this->data['tags']['page_title'] = __( "Login" );

		//validate form input
		$this->form_validation->set_rules( 'identity', __( "Username" ), 'required' );
		$this->form_validation->set_rules( 'password', __( "Password" ), 'required' );

		if ( $this->form_validation->run() === true )
		{
			// check to see if the user is logging in
			// check for "remember me"
			$remember = ( bool ) $this->input->post( 'remember' );

			if ( $this->ion_auth->login( $this->input->post( 'identity' ), $this->input->post( 'password' ), $remember ) )
			{
				// if the login is successful redirect them back to the home page
				$this->logs->set_data( NULL, "POST" )->set_result( TRUE )->register();
				// $this->alerts->persist( TRUE )->alert( NULL, NULL, $this->ion_auth->messages(), 'success' );
				redirect( '/?welcome=true', 'refresh' );
			}
			else
			{
				// if the login was un-successful redirect them back to the login page
				$this->logs->set_data( $_POST, "POST" )->set_result( FALSE )->register();
				$this->alerts->persist( TRUE )->alert( NULL, __( "Error" ), $this->ion_auth->errors(), 'danger' );
				redirect( 'login', 'refresh' ); // use redirects instead of loading views for compatibility with MY_Controller libraries
			}
		}
		else
		{
			// the user is not logging in so display the login page set the flash data error message if there is one			
			if ( validation_errors() )
			{
				$this->logs->set_data( $_POST, "POST" )->set_result( FALSE )->register();
				$this->alerts->alert( NULL, __( "Error" ), validation_errors(), 'danger' );
			}
			else if ( $this->ion_auth->messages() != "" )
			{
				$this->logs->set_data( $_POST, "POST" )->set_result( FALSE )->register();
				$this->alerts->alert( NULL, __( "Error" ), $this->ion_auth->messages(), 'danger' );
			}

			if ( $this->input->get( 'changed_password' ) ) $this->alerts->alert( NULL, NULL, __( "Password successfully changed." ), 'success' );
			if ( $this->input->get( 'loggedout' ) ) $this->alerts->alert( NULL, NULL, __( "You logged out successfully." ), 'success' );

			$this->data['identity'] = array( 
				'name'			=> 'identity',
				'id'			=> 'identity',
				'type'			=> 'text',
				'class'			=> 'form-control form-control-lg',
				'value'			=> $this->form_validation->set_value( 'identity' ),
				'autocomplete'	=> 'off',
			);

			$this->data['password'] = array( 
				'name'			=> 'password',
				'id'			=> 'password',
				'class'			=> 'form-control form-control-lg',
				'type'			=> 'password',
				'autocomplete'	=> 'off',
			);

			$this->data['submit'] = array( 
				'name'		=> 'submit',
				'id'		=> 'submit',
				'type'		=> 'submit',
				'class'		=> 'btn btn-block btn-outline-light',
				'value'		=> __( "Login" ),
			);

			$this->assets->load_script( 'login.min.js' );
			$this->_render_auth_page( 'auth/login', $this->data );
		}
	}

	/**
	 * Logout view
	 * 
	 * @return none
	 */
	public function logout()
	{
		$outing = array(
			'id' 			=> $this->ion_auth->user()->row()->id,
			'username' 		=> $this->ion_auth->user()->row()->username,
			'first_name' 	=> $this->ion_auth->user()->row()->first_name,
			'last_name' 	=> $this->ion_auth->user()->row()->last_name,
		);
		$this->logs->set_data( $outing, "GET" )->set_result( TRUE )->register();

		// log the user out
		$logout = $this->ion_auth->logout();

		$this->alerts->persist( TRUE )->alert( NULL, NULL, $this->ion_auth->messages(), 'success' );

		// redirect them to the login page
		redirect( 'login?loggedout=true', 'refresh' );
	}

	/**
	 * Change password view
	 * 
	 * @return none
	 */
	public function change_password()
	{
		if ( ! $this->ion_auth->logged_in() ) redirect( 'login', 'refresh' );

		$this->data['tags']['page_title'] = __( "Change Password" );

		$this->form_validation->set_rules( 'old', __( "Old Password" ), 'required' );
		$this->form_validation->set_rules( 'new', __( "New Password" ), 'required|min_length[' . $this->config->item( 'min_password_length', 'ion_auth' ) . ']|max_length[' . $this->config->item( 'max_password_length', 'ion_auth' ) . ']|matches[new_confirm]' );
		$this->form_validation->set_rules( 'new_confirm', __( "Confirm New Password" ), 'required' );

		$user = $this->ion_auth->user()->row();

		if ( $this->form_validation->run() == false )
		{
			// display the form
			// set the flash data error message if there is one
			// $this->data['message'] = ( validation_errors() ) ? validation_errors() : $this->session->flashdata( 'message' );
			if ( validation_errors() )
			{
				$this->logs->set_data( NULL, "POST" )->set_result( FALSE )->register();
				$this->alerts->alert( NULL, __( "Error" ), validation_errors(), 'danger' );
			}
			else if ( $this->ion_auth->messages() != "" )
			{
				$this->logs->set_data( $_POST, "POST" )->set_result( FALSE )->register();
				$this->alerts->alert( NULL, __( "Error" ), $this->ion_auth->messages(), 'danger' );
			}

			$this->data['min_password_length'] = $this->config->item( 'min_password_length', 'ion_auth' );

			$this->data['old_password'] = array( 
				'name'		=> 'old',
				'id'		=> 'old',
				'type'		=> 'password',
				'class'		=> 'form-control form-control-lg',
			);
			
			$this->data['new_password'] = array( 
				'name'		=> 'new',
				'id'		=> 'new',
				'type'		=> 'password',
				'class'		=> 'form-control form-control-lg',
				'pattern'	=> '^.{' . $this->data['min_password_length'] . '}.*$',
			);
			
			$this->data['new_password_confirm'] = array( 
				'name'		=> 'new_confirm',
				'id'		=> 'new_confirm',
				'type'		=> 'password',
				'class'		=> 'form-control form-control-lg',
				'pattern'	=> '^.{' . $this->data['min_password_length'] . '}.*$',
			);

			$this->data['submit'] = array( 
				'name'		=> 'submit',
				'id'		=> 'submit',
				'class'		=> 'btn btn-block btn-outline-light w-50 mx-auto',
				'value'		=> __( "Change" ),
			);
			
			$this->data['user_id'] = array( 
				'name'		=> 'user_id',
				'id'		=> 'user_id',
				'type'		=> 'hidden',
				'value'		=> $user->id,
			);

			// render
			$this->assets->load_script( 'login.min.js' );
			$this->_render_auth_page( 'auth/change_password', $this->data );
		}
		else
		{
			$identity = $this->session->userdata( 'identity' );

			$change = $this->ion_auth->change_password( $identity, $this->input->post( 'old' ), $this->input->post( 'new' ) );

			if ( $change )
			{
				//if the password was successfully changed
				// $this->session->set_flashdata( 'message', $this->ion_auth->messages() );
				// $this->logout();
				$this->logs->set_data( NULL, "POST" )->set_result( TRUE )->register();
				$logout = $this->ion_auth->logout();
				redirect( 'login?changed_password=true', 'refresh' );
			}
			else
			{
				// $this->session->set_flashdata( 'message', $this->ion_auth->errors() );
				$this->logs->set_data( NULL, "POST" )->set_result( FALSE )->register();
				$this->alerts->persist( TRUE )->alert( NULL, NULL, $this->ion_auth->errors(), 'danger' );
				redirect( 'change_password', 'refresh' );
			}
		}
	}

	/**
	 * Forgotten password view tool
	 * 
	 * @return none
	 */
	public function forgot_password()
	{
		$this->data['tags']['page_title'] = __( "Forgotten Password" );

		// setting validation rules by checking whether identity is username or email
		if ( $this->config->item( 'identity', 'ion_auth' ) != 'email'  )
		{
		   $this->form_validation->set_rules( 'identity', __( "Username" ), 'required' );
		}
		else
		{
		   $this->form_validation->set_rules( 'identity', __( "E-mail Address" ), 'required|valid_email' );
		}

		if ( $this->form_validation->run() == false )
		{
			$this->data['type'] = $this->config->item( 'identity','ion_auth' );

			// setup the input

			$this->data['identity'] = array( 
				'name'			=> 'identity',
				'id'			=> 'identity',
				'class'			=> 'form-control form-control-lg',
			);

			$this->data['submit'] = array( 
				'name'		=> 'submit',
				'id'		=> 'submit',
				'class'		=> 'btn btn-block btn-outline-light',
				'value'		=> __( "Send" ),
			);

			if ( $this->config->item( 'identity', 'ion_auth' ) != 'email' )
			{
				$this->data['identity_label'] = __( "Username" );
			}
			else
			{
				$this->data['identity_label'] = __( "Password" );
			}

			// set any errors and display the form
			if ( validation_errors() )
			{
				$this->logs->set_data( $_POST, "POST" )->set_result( FALSE )->register();
				$this->alerts->alert( NULL, __( "Error" ), validation_errors(), 'danger' );
			}
			else if ( $this->ion_auth->messages() != "" )
			{
				$this->logs->set_data( $_POST, "POST" )->set_result( FALSE )->register();
				$this->alerts->alert( NULL, __( "Error" ), $this->ion_auth->messages(), 'danger' );
			}

			$this->assets->load_script( 'login.min.js' );
			$this->_render_auth_page( 'auth/forgot_password', $this->data );
		}
		else
		{
			$identity_column = $this->config->item( 'identity','ion_auth' );
			$identity = $this->ion_auth->where( $identity_column, $this->input->post( 'identity' ) )->users()->row();

			if ( empty( $identity ) )
			{
				if ( $this->config->item( 'identity', 'ion_auth' ) != 'email' )
				{
					$this->ion_auth->set_error( __( "That username does not appear in our records." ) );
				}
				else
				{
					$this->ion_auth->set_error( __( "That e-mail address does not appear in our records." ) );
				}

				$this->logs->set_data( $_POST, "POST" )->set_result( FALSE )->register();
				$this->alerts->persist( TRUE )->alert( NULL, __( "Error" ), $this->ion_auth->errors(), 'danger' );
				redirect( "forgot_password", 'refresh' );
			}

			// run the forgotten password method to email an activation code to the user
			$forgotten = $this->ion_auth->forgotten_password( $identity->{$this->config->item( 'identity', 'ion_auth' )} );

			if ( $forgotten )
			{
				// if there were no errors
				$this->logs->set_data( $_POST, "POST" )->set_result( TRUE )->register();
				$this->alerts->persist( TRUE )->alert( NULL, NULL, $this->ion_auth->messages(), 'success' );
				redirect( "login", 'refresh' ); //we should display a confirmation page here instead of the login page
			}
			else
			{
				$this->logs->set_data( $_POST, "POST" )->set_result( FALSE )->register();
				$this->alerts->persist( TRUE )->alert( NULL, __( "Error" ), $this->ion_auth->errors(), 'danger' );
				redirect( "forgot_password", 'refresh' );
			}
		}
	}

	/**
	 * Password reset after forgotten
	 * 
	 * @param  string $code
	 * @return none
	 */
	public function reset_password()
	{
		$this->data['tags']['page_title'] = __( "Reset Password" );

		$code = ( $this->uri->segment( 3 ) ) ? $this->uri->segment( 3 ) : $this->uri->segment( 2 );

		if ( ! $code )
		{
			show_404();
		}

		$user = $this->ion_auth->forgotten_password_check( $code );

		if ( $user )
		{
			// if the code is valid then display the password reset form

			$this->form_validation->set_rules( 'new', __( "New Password" ), 'required|min_length[' . $this->config->item( 'min_password_length', 'ion_auth' ) . ']|max_length[' . $this->config->item( 'max_password_length', 'ion_auth' ) . ']|matches[new_confirm]' );
			$this->form_validation->set_rules( 'new_confirm', __( "Confirm New Password" ), 'required' );

			if ( $this->form_validation->run() === FALSE )
			{
				// If validation errors, let's show them. Otherwise, the one from the model.
				if ( validation_errors() )
				{
					$this->logs->set_data( $_POST, "POST" )->set_result( FALSE )->register();
					$this->alerts->alert( NULL, __( "Error" ), validation_errors(), 'danger' );
				}
				else if ( $this->ion_auth->messages() != "" )
				{
					$this->logs->set_data( $_POST, "POST" )->set_result( FALSE )->register();
					$this->alerts->alert( NULL, __( "Error" ), $this->ion_auth->messages(), 'danger' );
				}

				$this->data['min_password_length'] = $this->config->item( 'min_password_length', 'ion_auth' );
				
				$this->data['new_password'] = array( 
					'name'		=> 'new',
					'id'		=> 'new',
					'type'		=> 'password',
					'class'		=> 'form-control form-control-lg',
					'pattern'	=> '^.{' . $this->data['min_password_length'] . '}.*$',
				);
				
				$this->data['new_password_confirm'] = array( 
					'name'		=> 'new_confirm',
					'id'		=> 'new_confirm',
					'type'		=> 'password',
					'class'		=> 'form-control form-control-lg',
					'pattern'	=> '^.{' . $this->data['min_password_length'] . '}.*$',
				);

				$this->data['submit'] = array( 
					'name'		=> 'submit',
					'id'		=> 'submit',
					'class'		=> 'btn btn-outline-light w-50',
					'value'		=> __( "Save" ),
				);
				
				$this->data['user_id'] = array( 
					'name'		=> 'user_id',
					'id'		=> 'user_id',
					'type'		=> 'hidden',
					'value'		=> $user->id,
				);
				
				$this->data['csrf'] = $this->_get_csrf_nonce();
				$this->data['code'] = $code;

				$this->assets->load_script( 'login.min.js' );

				// render
				$this->_render_auth_page( 'auth/reset_password', $this->data );
			}
			else
			{
				// do we have a valid request?
				if ( $this->_valid_csrf_nonce() === FALSE OR $user->id != $this->input->post( 'user_id' ) )
				{
					// something fishy might be up
					$this->ion_auth->clear_forgotten_password_code( $code );
					show_error( __( "This form post did not pass our security check." ), 500, __( "Security Check" ) );
				}
				else
				{
					// finally change the password
					$identity = $user->{$this->config->item( 'identity', 'ion_auth' )};
					$change = $this->ion_auth->reset_password( $identity, $this->input->post( 'new' ) );

					if ( $change )
					{
						// if the password was successfully changed
						$this->ion_auth->forgotten_password_complete_comunicate( $user->email, $identity, $this->input->post( 'new' ) );
						$this->logs->set_data( NULL, "POST" )->set_result( TRUE )->register();
						$this->alerts->persist( TRUE )->alert( NULL, NULL, $this->ion_auth->messages(), 'success' );
						redirect( "login", 'refresh' );
					}
					else
					{
						$this->logs->set_data( $_POST, "POST" )->set_result( FALSE )->register();
						$this->alerts->persist( TRUE )->alert( NULL, __( "Error" ), $this->ion_auth->errors(), 'danger' );
						redirect( 'reset_password/' . $code, 'refresh' );
					}
				}
			}
		}
		else
		{
			// if the code is invalid then send them back to the forgot password page
			$this->alerts->persist( TRUE )->alert( NULL, __( "Error" ), $this->ion_auth->errors(), 'danger' );
			redirect( "forgot_password", 'refresh' );
		}
	}

	/**
	 * Activate user view
	 * 
	 * @param  int  $id
	 * @param  boolean/string $code
	 * @return none
	 */

	public function activate( $id, $code = false )
	{
		if ( $code !== false )
		{
			$activation = $this->ion_auth->activate( $id, $code );
		}
		elseif ( $this->ion_auth->is_admin() )
		{
			$activation = $this->ion_auth->activate( $id );
		}

		if ( $activation )
		{
			// redirect them to the auth page
			// $this->session->set_flashdata( 'message', $this->ion_auth->messages() );
			$this->logs->set_data( NULL, "GET" )->set_result( TRUE )->register();
			$this->alerts->persist( TRUE )->alert( NULL, NULL, $this->ion_auth->messages(), 'success' );
			redirect( "login", 'refresh' );
		}
		else
		{
			// redirect them to the forgot password page
			// $this->session->set_flashdata( 'message', $this->ion_auth->errors() );
			$this->logs->set_data( NULL, "GET" )->set_result( FALSE )->register();
			$this->alerts->persist( TRUE )->alert( NULL, __( "Error" ), $this->ion_auth->errors(), 'danger' );
			redirect( "forgot_password", 'refresh' );
		}
	}

	/**
	 * Deactivate the user
	 * 
	 * @param  int id
	 * @return none
	 */
/*
	public function deactivate( $id = NULL )
	{
		$this->data['tags']['page_title'] = __( "Desactivar Usuario" );

		if ( ! $this->ion_auth->logged_in() OR ! $this->ion_auth->is_admin() )
		{
			// redirect them to the home page because they must be an administrator to view this
			return show_error( 'You must be an administrator to view this page.' );
		}

		$id = ( int ) $id;

		if ( $this->ion_auth->user()->row()->id == $id )
		{
			// $this->session->set_flashdata( 'message', __( "No puedes desactivar tu propia cuenta. Deberás pedírselo a otro administrador." ) );
			$this->logs->set_data( $_POST, "POST" )->set_result( FALSE )->register();
			$this->alerts->persist( TRUE )->alert( __( "No puedes desactivar tu propia cuenta. Deberás pedírselo a otro administrador." ), NULL, NULL, 'danger' );
			redirect( "auth", 'refresh' );
		}

		$this->load->library( 'form_validation' );
		$this->form_validation->set_rules( 'confirm', __( "Confirmation" ), 'required' );
		$this->form_validation->set_rules( 'id', __( "User ID" ), 'required|alpha_numeric' );

		if ( $this->form_validation->run() == FALSE )
		{
			$this->logs->set_data( $_POST, "POST" )->set_result( FALSE )->register();

			// insert csrf check
			$this->data['csrf'] = $this->_get_csrf_nonce();
			$this->data['user'] = $this->ion_auth->user( $id )->row();

			$this->_render_page( 'auth/deactivate_user', $this->data );
		}
		else
		{
			// do we really want to deactivate?
			if ( $this->input->post( 'confirm' ) == 'yes' )
			{
				// do we have a valid request?
				if ( $this->_valid_csrf_nonce() === FALSE OR $id != $this->input->post( 'id' ) )
				{
					$this->logs->set_data( $_POST, "POST" )->set_result( FALSE )->register();
					show_error( __( "This form post did not pass our security check." ) );
				}

				// do we have the right userlevel?
				if ( $this->ion_auth->logged_in() && $this->ion_auth->is_admin() )
				{
					$this->ion_auth->deactivate( $id );
					$this->logs->set_data( $_POST, "POST" )->set_result( TRUE )->register();
					$this->alerts->persist( TRUE )->alert( __( "Usuario desactivado" ), NULL, NULL, 'success' );
				}
			}

			// redirect them back to the auth page
			redirect( 'auth', 'refresh' );
		}
	}
*/

	public function profile()
	{
		if ( ! $this->ion_auth->logged_in() ) redirect( 'login', 'refresh' );

		if ( $this->input->post() )
		{
			$this->form_validation->set_rules( 'first_name', __( "First Name" ), 'trim|required' );
			$this->form_validation->set_rules( 'last_name', __( "Last Name" ), 'trim|required' );
			$this->form_validation->set_rules( 'email', __( "Email" ), 'trim|required|valid_email' );
			$this->form_validation->set_rules( 'language', __( "Language" ), 'trim|required' );

			if ( $this->form_validation->run() )
			{
				$new_data = array(
					'first_name' 	=> mb_convert_case( $this->input->post( 'first_name' ), MB_CASE_TITLE, "UTF-8" ),
					'last_name' 	=> mb_convert_case( $this->input->post( 'last_name' ), MB_CASE_TITLE, "UTF-8" ),
					'language' 		=> $this->input->post( 'language' ),
					'phone' 		=> $this->input->post( 'phone' ),
					'email'			=> $this->input->post( 'email' ),
					'modified'		=> unix_to_human( now(), TRUE, 'eu' ),
					'modifier'		=> $this->ion_auth->user()->row()->id,
				);

				if ( $this->ion_auth->update( $this->ion_auth->user()->row()->id, $new_data ) )
				{
					$new_data['user_id'] = $this->ion_auth->user()->row()->id;
					$this->logs->set_data( $new_data, "POST" )->set_result( TRUE )->register();
					$this->alerts->persist( TRUE )->alert( __( "New settings stored successfully." ), __( "Profile updated" ), NULL, 'success' );
					redirect( "profile" );
				}
				else
				{
					$new_data['user_id'] = $this->ion_auth->user()->row()->id;
					$this->logs->set_data( $new_data, "POST" )->set_result( FALSE )->register();
					$this->alerts->alert( __( "There were some errors while trying to perform the task. Please try again." ), __( "Profile not updated" ), NULL, 'danger' );
				}
			}
			else
			{
				$this->alerts->alert( __( "Errors appear below every field." ), __( "Profile could not be updated" ), NULL, 'danger' );
			}
		}

		$user = $this->ion_auth->user()->row_array();

		$this->data['user_id'] = array(
			'type'		=> 'hidden',
			'id'		=> 'user_id',
			'name'		=> 'user_id',
			'value'		=> $user['id'],
		);

		$this->data['first_name'] = array(
			'type'		=> 'text',
			'id'		=> 'first_name',
			'name'		=> 'first_name',
			'class'		=> 'form-control form-control-lg',
			'value'		=> set_value( 'first_name', $user['first_name'] ),
		);

		$this->data['last_name'] = array(
			'type'		=> 'text',
			'id'		=> 'last_name',
			'name'		=> 'last_name',
			'class'		=> 'form-control form-control-lg',
			'value'		=> set_value( 'last_name', $user['last_name'] ),
		);

		$this->data['language'] = array(
			'id'		=> 'language',
			'name'		=> 'language',
			'class'		=> 'form-control',
			'selected'	=> set_value( 'language', $user['language'] ),
			'options'	=> $this->assets->conf['languages'],
		);

		$this->data['phone'] = array(
			'type'		=> 'text',
			'id'		=> 'phone',
			'name'		=> 'phone',
			'class'		=> 'form-control',
			'value'		=> set_value( 'phone', $user['phone'] ),
		);

		$this->data['email'] = array(
			'type'		=> 'text',
			'id'		=> 'email',
			'name'		=> 'email',
			'class'		=> 'form-control',
			'value'		=> set_value( 'email', $user['email'] ),
		);

		// jQuery File Uploader assets
		$this->assets->load_style( site_url( 'assets/bower_components/jQuery-File-Upload/css/jquery.fileupload.css' ) );
		$this->assets->load_script( site_url( 'assets/bower_components/jQuery-File-Upload/js/vendor/jquery.ui.widget.js' ) );
		$this->assets->load_script( site_url( 'assets/bower_components/jQuery-File-Upload/js/jquery.iframe-transport.js' ) );
		$this->assets->load_script( site_url( 'assets/bower_components/jQuery-File-Upload/js/jquery.fileupload.js' ) );

		$this->assets->load_script( 'forms.min.js' );
		$this->assets->load_script( 'profile.min.js' );

		$this->data['tags']['page_title'] = __( "My Profile" );
		$this->templates->view( 'auth/profile', $this->data );

	}

	/**
	 * Cross site forgery nonce retrievement
	 * 
	 * @return array
	 */
	public function _get_csrf_nonce()
	{
		$this->load->helper( 'string' );
		$key	= random_string( 'alnum', 8 );
		$value	= random_string( 'alnum', 20 );
		$this->session->set_flashdata( 'csrfkey', $key );
		$this->session->set_flashdata( 'csrfvalue', $value );

		return array( $key => $value );
	}

	/**
	 * Checks if the cross site forgery nonce is valid or not
	 * 
	 * @return boolean
	 */
	public function _valid_csrf_nonce()
	{
		// return true;
		$csrfkey = $this->input->post( $this->session->flashdata( 'csrfkey' ) );

		if ( $csrfkey AND $csrfkey == $this->session->flashdata( 'csrfvalue' ) )
		{
			return TRUE;
		}
		else
		{
			return FALSE;
		}
	}

	/**
	 * Displays the called views
	 * 
	 * @param  string
	 * @param  array/null
	 * @param  boolean
	 * @return string
	 */
	public function _render_page( $view, $data = null, $returnhtml = false ) //I think this makes more sense
	{
		$this->viewdata = ( empty( $data ) ) ? $this->data : $data;
		$view_html = $this->templates->view( $view, $this->viewdata, $returnhtml );
		if ( $returnhtml ) return $view_html; //This will return html on 3rd argument being true
	}

	/**
	 * Displays the called views, with header and footer from the auth section
	 * 
	 * @param  string
	 * @param  array/null
	 * @param  boolean
	 * @return string
	 */
	public function _render_auth_page( $view, $data = null )
	{
		$this->assets->load_style( 'login' );
		$this->viewdata = ( empty( $data ) ) ? $this->data : $data;
		$this->load->view( 'auth/header', $this->viewdata );
		$this->load->view( $view, $this->viewdata );
		$this->load->view( 'auth/footer', $this->viewdata );
	}
}
