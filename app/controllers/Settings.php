<?php defined( 'BASEPATH' ) OR exit( 'No direct script access allowed' );

class Settings extends CI_Controller
{
	/**
	 * Class constructor
	 */
	public function __construct()
	{
		parent::__construct();

		if ( ! $this->ion_auth->logged_in() ) redirect( 'login', 'refresh' );

		$this->permissions->block_not_admin();

		$this->data['tags']['controller_name'] = __( "Settings" );

		$this->load->model( "Settings_model", "settings" );
	}

	/**
	 * Index redirecting to main
	 * 
	 * @return void
	 */
	public function index()
	{
		redirect( 'settings/main' );
	}

	/**
	 * Main settings page
	 * 
	 * @return output
	 */
	public function main()
	{
		if ( $this->input->post() )
		{
			$stored = FALSE;

			$this->db->truncate( $this->assets->conf['tables']['settings'] );

			foreach ( $_POST AS $key => $value )
			{
				if ( is_array( $value ) ) $value = json_encode( $value );

				if ( $key == "decimals" OR $key == "qty_decimals" )
				{
					if ( $value == 0 OR empty( $value ) ) $value = (float)0;
				}

				if ( $this->settings->save( array( 'key' => $key, 'value' => $value ) ) )
				{
					$stored = TRUE;
				}
				else
				{
					$stored = FALSE;
				}
			}

			if ( $stored )
			{
				$this->logs->set_data( $_POST, "POST" )->set_result( TRUE )->register();
				$this->alerts->persist( TRUE )->alert( __( "New settings have been stored successfully." ), __( "Settings updated" ), NULL, 'success' );
			}
			else
			{
				$this->logs->set_data( $_POST, "POST" )->set_result( FALSE )->register();
				$this->alerts->persist( TRUE )->alert( __( "There were some errors while trying to perform the task. Please try again." ), __( "Settings discarded" ), NULL, 'danger' );
			}

			redirect( 'settings/main' );
		}

		$this->data['items_per_page'] = array(
			'type' 		=> 'text',
			'name'		=> 'items_per_page',
			'id'		=> 'items_per_page',
			'class' 	=> 'form-control input-integer',
			'value'		=> my_number_format( $this->assets->conf['items_per_page'] ),
		);

		$this->data['pages_to_show'] = array(
			'type' 		=> 'text',
			'name'		=> 'pages_to_show',
			'id'		=> 'pages_to_show',
			'class' 	=> 'form-control input-integer',
			'value'		=> my_number_format( $this->assets->conf['pages_to_show'] ),
		);

		$this->data['language'] = array(
			'name'		=> 'language',
			'id'		=> 'language',
			'class' 	=> 'form-control',
			'selected'	=> $this->assets->conf['language'],
			'options'	=> $this->assets->conf['languages'],
		);

		$this->data['company_name'] = array(
			'type' 		=> 'text',
			'name'		=> 'company_name',
			'id'		=> 'company_name',
			'class' 	=> 'form-control',
			'value'		=> $this->assets->conf['company_name'],
		);

		$this->data['cache_assets'] = array(
			'type' 		=> 'checkbox',
			'name'		=> 'cache_assets',
			'id'		=> 'cache_assets',
			'class' 	=> 'form-check-input',
			'value'		=> '1',
		);

		if ( $this->assets->conf['cache_assets'] == TRUE ) $this->data['cache_assets']['checked'] = "checked";

		$this->data['min_warning'] = array(
			'type' 		=> 'text',
			'name'		=> 'stock[min_warning]',
			'id'		=> 'stock_min_warning',
			'class' 	=> 'form-control input-quantity',
			'value'		=> my_number_format( $this->assets->conf['stock']['min_warning'], $this->assets->conf['qty_decimals'] ),
		);

		$this->data['min_danger'] = array(
			'type' 		=> 'text',
			'name'		=> 'stock[min_danger]',
			'id'		=> 'stock_min_danger',
			'class' 	=> 'form-control input-quantity',
			'value'		=> my_number_format( $this->assets->conf['stock']['min_danger'], $this->assets->conf['qty_decimals'] ),
		);

		$this->data['currency'] = array(
			'name'		=> 'currency',
			'id'		=> 'currency',
			'class' 	=> 'form-control',
			'selected'	=> $this->assets->conf['currency'],
			'options'	=> $this->assets->conf['currencies_ext'],
		);

		$this->data['auto_update_exchange'] = array(
			'type' 			=> 'checkbox',
			'name'			=> 'auto_update_exchange',
			'id'			=> 'auto_update_exchange',
			'class' 		=> 'form-check-input',
			'value'			=> '1',
			'data-toggle'	=> 'collapse',
			'data-target'	=> '#auto-update-hours-group',
		);

		if ( $this->assets->conf['auto_update_exchange'] == TRUE ) $this->data['auto_update_exchange']['checked'] = "checked";

		$this->data['auto_update_hours'] = array(
			'type' 		=> 'text',
			'name'		=> 'auto_update_hours',
			'id'		=> 'auto_update_hours',
			'maxlength'	=> 2,
			'class' 	=> 'form-control input-raw-integer',
			'value'		=> $this->assets->conf['auto_update_hours'],
		);

		$this->data['auto_update_source'] = array(
			'type' 		=> 'text',
			'name'		=> 'auto_update_source',
			'id'		=> 'auto_update_source',
			'class' 	=> 'form-control',
			'value'		=> $this->assets->conf['auto_update_source'],
		);

		$this->data['decimals'] = array(
			'name'		=> 'decimals',
			'id'		=> 'decimals',
			'class' 	=> 'form-control',
			'selected'	=> $this->assets->conf['decimals'],
			'options'	=> array( '0', '1', '2' ),
		);

		$this->data['qty_decimals'] = array(
			'name'		=> 'qty_decimals',
			'id'		=> 'qty_decimals',
			'class' 	=> 'form-control',
			'selected'	=> $this->assets->conf['qty_decimals'],
			'options'	=> array( '0', '1', '2' ),
		);

		// Inputmask
		$this->assets->load_script( base_url( 'assets/bower_components/inputmask/dist/jquery.inputmask.min.js' ) );

		// Select2
		$this->assets->load_style( site_url( 'assets/bower_components/select2/dist/css/select2.min.css' ) );
		$this->assets->load_script( site_url( 'assets/bower_components/select2/dist/js/select2.full.min.js' ) );
		$this->assets->load_script( site_url( 'assets/bower_components/select2/dist/js/i18n/' . substr( $this->assets->conf['language'], 0, 2 ) . '.js' ) );

		$this->assets->load_script( 'forms.min.js' );

		$this->data['tags']['page_title'] = __( "Main Settings" );
		$this->templates->view( 'singles/settings-main', $this->data );
	}

	/**
	 * Exchange page
	 * 
	 * @return output
	 */
	public function exchange()
	{
		if ( $this->input->post() )
		{
			$this->assets->conf['stop_exchange_auto_update'] = TRUE;

			foreach ( $this->assets->conf['currencies'] AS $key => $symbol )
			{
				$this->form_validation->set_rules( "purchase[" . $key . "]", $this->assets->conf['currencies_ext'][$key], 'trim|required|greater_than[0]|' . ( ( $this->assets->conf['decimals'] > 0 ) ? 'decimal' : 'integer' ) );
				$this->form_validation->set_rules( "sale[" . $key . "]", $this->assets->conf['currencies_ext'][$key], 'trim|required|greater_than[0]|' . ( ( $this->assets->conf['decimals'] > 0 ) ? 'decimal' : 'integer' ) );
			}

			if ( $this->form_validation->run() )
			{
				$to_store = array();

				foreach ( $_POST['sale'] AS $key => $value )
				{
					$to_store[$key] = array( 
						'sale' 		=> (float)$value, 
						'purchase' 	=> (float)$_POST['purchase'][$key],
					);
				}

				if ( ! empty( $to_store ) ) 
				{
					if ( file_put_contents( FCPATH . 'uploads/exchange.json', json_encode( $to_store ) ) )
					{
						$this->logs->set_data( $_POST, "POST" )->set_result( TRUE )->register();
						$this->alerts->alert( __( "New exchange information have been stored successfully." ), __( "Exchange updated" ), NULL, 'success' );						
					}
					else
					{
						$this->logs->set_data( $_POST, "POST" )->set_result( FALSE )->register();
						$this->alerts->alert( __( "There were some errors while trying to perform the task. Please try again." ), __( "Exchange discarded" ), NULL, 'danger' );
					}
				}
			}
			else
			{
				$this->alerts->alert( __( "Errors appear below every field." ), __( "Settings could not be stored into the database" ), NULL, 'danger' );
			}
		}

		$exchange_file = ( is_file( FCPATH . "uploads/exchange.json" ) ) ? file_get_contents( FCPATH . "uploads/exchange.json" ) : NULL;


		if ( ! empty( $exchange_file ) )
		{
			$this->data['exchange'] = json_decode( $exchange_file, TRUE );
			$this->data['last_edition'] = filemtime( FCPATH . "uploads/exchange.json" );
		}
		else
		{
			foreach ( $this->assets->conf['currencies'] AS $key => $symbol )
			{
				$this->data['exchange'][$key] = array( 'sale' => 0, 'purchase' => 0 );

				if ( $key == $this->assets->conf['currency'] ) $this->data['exchange'][$key] = array( 'sale' => 1, 'purchase' => 1 );
			}
		}

		// Inputmask
		$this->assets->load_script( base_url( 'assets/bower_components/inputmask/dist/jquery.inputmask.min.js' ) );

		$this->assets->load_script( 'forms.min.js' );
		$this->assets->load_script( 'forms-exchange.min.js' );

		$this->data['tags']['page_title'] = __( "Currency Exchange" );
		$this->templates->view( 'singles/settings-exchange', $this->data );
	}

	/**
	 * Log page
	 * 
	 * @return output
	 */
	public function log()
	{

	}

	/**
	 * Action that blocks the entire system for users other than superadmins.
	 * It creates a .blocked empty file that acts like a lock. Redirects to a given 'referer'
	 * 
	 * @return void
	 */
	public function system_lock()
	{
		if ( $this->ion_auth->is_superadmin() )
		{
			if ( file_put_contents( $this->assets->conf['locked_file_path'], '' ) !== FALSE )
			{
				$this->logs->set_data( $_GET, "GET" )->set_result( TRUE )->register();
				$this->alerts->persist( TRUE )->alert( __( "The system has been locked down for any user who's not a super admin." ), __( "System Locked" ), NULL, 'success' );
			}
			else
			{
				$this->logs->set_data( $_GET, "GET" )->set_result( FALSE )->register();
				$this->alerts->persist( TRUE )->alert( __( "There were some errors while trying to perform the task. Please try again." ), __( "Settings discarded" ), NULL, 'danger' );					
			}
		}
		else
		{
			$_GET['not_super_admin'] = TRUE;
			$this->logs->set_data( $_GET, "GET" )->set_result( FALSE )->register();
			$this->alerts->persist( TRUE )->alert( $this->assets->conf['no_permission']['message'], $this->assets->conf['no_permission']['title'], NULL, 'warning' );
		}

		redirect( ( isset( $_GET['referer'] ) AND ! empty( $_GET['referer'] ) ) ? $_GET['referer'] : '/' );
	}

	/**
	 * Action that unblocks the entire system for users.
	 * It removes the .blocked empty file that acts like a lock. Redirects to a given 'referer'
	 * 
	 * @return void
	 */
	public function system_unlock()
	{
		if ( $this->ion_auth->is_superadmin() )
		{
			if ( unlink( $this->assets->conf['locked_file_path'] ) )
			{
				$this->logs->set_data( $_GET, "GET" )->set_result( TRUE )->register();
				$this->alerts->persist( TRUE )->alert( __( "The system has been unlocked for any user." ), __( "System Unlocked" ), NULL, 'success' );
			}
			else
			{
				$this->logs->set_data( $_GET, "GET" )->set_result( FALSE )->register();
				$this->alerts->persist( TRUE )->alert( __( "There were some errors while trying to perform the task. Please try again." ), __( "Settings discarded" ), NULL, 'danger' );					
			}
		}
		else
		{
			$_GET['not_super_admin'] = TRUE;
			$this->logs->set_data( $_GET, "GET" )->set_result( FALSE )->register();
			$this->alerts->persist( TRUE )->alert( $this->assets->conf['no_permission']['message'], $this->assets->conf['no_permission']['title'], NULL, 'warning' );
		}

		redirect( ( isset( $_GET['referer'] ) AND ! empty( $_GET['referer'] ) ) ? $_GET['referer'] : '/' );
	}

	/**
	 * Action that performs a database backup and store it in our sql dir.
	 * This needs to be polished for extra security. Redirects to a given 'referer'
	 * 
	 * @return void
	 */
	public function backup_database()
	{
		$this->load->library( 'backup' );

		if ( $this->ion_auth->is_superadmin() )
		{
			if ( $this->backup->do_database_backup( $this->assets->conf['database_backup_path'] ) )
			{
				$this->logs->set_data( $_GET, "GET" )->set_result( TRUE )->register();
				$this->alerts->persist( TRUE )->alert( __( "The database backup was made correctly and stored on the hard drive." ), __( "Database Backup done" ), NULL, 'success' );
			}
			else
			{
				$this->logs->set_data( $_GET, "GET" )->set_result( FALSE )->register();
				$this->alerts->persist( TRUE )->alert( __( "There were some errors while trying to perform the task. Please try again." ), __( "Database Backup discarded" ), NULL, 'danger' );
			}
		}

		redirect( ( isset( $_GET['referer'] ) AND ! empty( $_GET['referer'] ) ) ? $_GET['referer'] : '/' );
	}
}
