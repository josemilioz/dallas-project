<?php defined( 'BASEPATH' ) OR exit( 'No direct script access allowed' );

class Ajax extends CI_Controller
{
	private $result;
	private $json_opts;
	private $output_type;
	private $encoding;

	/**
	 * Class constructor
	 */
	public function __construct()
	{
		parent::__construct();

		if ( ! $this->ion_auth->logged_in() ) redirect( 'login', 'refresh' );

		$this->json_opts = JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES;
		$this->output_type = 'application/json';
		$this->encoding = 'UTF-8';
	}

	/**
	 * Output the ajax result
	 * 
	 * @return output
	 */
	private function output_result()
	{
		$this->output->set_status_header( 200 )
					 ->set_content_type( $this->output_type, $this->encoding )
					 ->set_output( json_encode( $this->result, $this->json_opts ) )
					 ->_display();
		die( -1 );
	}

	/**
	 * Gets the current hash data to send with POST ajax
	 * 
	 * @return output
	 */
	public function get_csrf_hash()
	{
		$this->result = array(
			'csrf_name' => $this->security->get_csrf_token_name(),
			'csrf_hash' => $this->security->get_csrf_hash(),
		);
	
		$this->output_result();	
	}

	/**
	 * Just checking connectivity printing a number from the server
	 * 
	 * @return output
	 */
	public function check_connectivity()
	{
		die( '1' );
	}

	/**
	 * Checks whether the session still valid or not
	 * 
	 * @return output
	 */
	public function check_session()
	{
		if ( $this->ion_auth->logged_in() )
		{
			die( 'loggedin' );
		}
		else
		{
			die( 'loggedout' );
		}
	}

	/**
	 * Checks whether an user can do certain actions given its user level. Then returns the array of enabled actions for the user.
	 * See the switch definition to know which actions you can associate to the function, getting propper clearance.
	 * 
	 * @return output
	 */
	public function get_current_user_actions_enabled()
	{
		$user = $this->ion_auth->user()->row();
		$query = $this->ion_auth_model->get_users_groups( $user->id )->result();
		$users_groups = array();
		$actions_enabled = array();

		foreach ( $query AS $group ) $users_groups[] = $group->id;

		if ( ! empty( $_REQUEST['controller'] ) )
		{
			if ( isset( $this->assets->conf['group_permissions'][$_REQUEST['controller']] ) )
			{
				foreach ( $this->assets->conf['group_permissions'][$_REQUEST['controller']] AS $k => $v )
				{
					if ( ! empty( array_intersect( $users_groups, $v ) ) ) $this->result[] = $k;
				}
			}
		}

		$this->output_result();
	}

	/**
	 * Retrieves the category thru the category ID
	 * 
	 * @param  int $category_id The category ID
	 * @return output
	 */
	public function category_retrieve( $category_id )
	{
		$this->load->model( "Categories_model", "categories" );
		$category = $this->categories->get( $category_id );
		if ( ! empty( $category ) ) $this->result = $category;
		$this->output_result();
	}

	/**
	 * Retrieves the product thru the product ID (with thumbnail)
	 * 
	 * @param  int $product_id The product ID
	 * @return output
	 */
	public function product_retrieve( $product_id )
	{
		$this->load->model( "Products_model", "products" );
		$product = $this->products->get( $product_id );
		if ( ! empty( $product ) ) 
		{
			$this->result = $product;
			$this->result['picture'] = $this->products->picture( $product['product_id'] );
		}

		$this->output_result();
	}

	/**
	 * Retrieves a full list of available products, for dropdowns
	 * 
	 * @return output
	 */
	public function product_dropdown_retrieve()
	{
		$this->load->model( "Products_model", "products" );

		$product_list = $this->products->build_dropdown();
		
		if ( ! empty( $product_list ) ) 
		{
			$this->result = $product_list;
		}
		else
		{
			$this->result['error'] = __( "There are no products to populate the product list." );
		}

		$this->output_result();
	}

	/**
	 * Retrieves the stock and the product associated thru the SKU
	 * 
	 * @param  string $sku The SKU
	 * @return output
	 */
	public function stock_retrieve( $sku )
	{
		$this->load->model( "Stock_model", "stock" );
		$this->load->model( "Products_model", "products" );

		if ( isset( $_GET['type'] ) )
		{
			if ( $_GET['type'] == "stock" )
			{
				$stock = $this->stock->get( array(
					'where' => array(
						'sku' => $sku,
						'trash' => 0,
					)
				) );
				
				if ( ! empty( $stock[0] ) )
				{ 
					$this->result['stock'] = $stock[0];
					if ( ! empty( $stock[0]['expiration_date'] ) ) $this->result['stock']['expiration_date_formatted'] = date( $this->assets->conf['date_format'], strtotime( $stock[0]['expiration_date'] ) );
					$this->result['product'] = $this->products->get( $stock[0]['product_id'] );
					$this->result['product']['picture'] = $this->products->picture( $this->result['product']['product_id'] );
				}
			}
			elseif ( $_GET['type'] == "product" )
			{
				$product = $this->products->get( $sku ); // which is product_id in this case
				
				if ( ! empty( $product ) )
				{ 
					$this->result['product'] = $product;
					$this->result['product']['picture'] = $this->products->picture( $product['product_id'] );
				}
			}
		}

		$this->output_result();
	}

	/**
	 * Gets the purchase meta by its ID
	 * 
	 * @param  int $purchase_meta_id The purchase meta ID
	 * @return output
	 */
	public function purchase_meta_retrieve( $purchase_meta_id )
	{
		$this->load->model( "Purchases_model", "purchases" );
		$this->load->model( "Products_model", "products" );

		$this->db->where( 'purchase_meta_id', $purchase_meta_id );
		if ( $query = $this->db->get( $this->assets->conf['tables']['purchases_products'] ) AND $query->num_rows() > 0 )
		{
			$this->result['purchase'] = $query->row_array();
			$this->result['product'] = $this->products->get( $this->result['purchase']['product_id'] );
		}

		$this->output_result();
	}

	/**
	 * Stores or updates a stock row into de DB
	 * 
	 * @return output
	 */
	public function store_stock_row()
	{
		$this->load->model( "Stock_model", "stock" );
		$this->load->model( "Purchases_model", "purchases" );

		if ( $this->input->post() )
		{
			$this->form_validation->set_rules( 'purchase_meta_id', 	__( "Purchase Meta ID" ),	'trim|required' );
			$this->form_validation->set_rules( 'product_id', 		__( "Product ID" ),			'trim|required' );
			$this->form_validation->set_rules( 'purchase_id', 		__( "Purchase ID" ),		'trim|required' );
			$this->form_validation->set_rules( 'sku', 				__( "SKU" ),				'trim|required' );
			$this->form_validation->set_rules( 'quantity', 			__( "Quantity" ),			'trim|required' );
			$this->form_validation->set_rules( 'purchase_price', 	__( "Purchase price" ),		'trim|required' );
			$this->form_validation->set_rules( 'unit_price', 		__( "Sale price" ),			'trim|required' );

			if ( $this->form_validation->run() )
			{
				$store = array(
					'purchase_id'		=> $this->input->post( 'purchase_id' ),
					'product_id'		=> $this->input->post( 'product_id' ),
					'sku'				=> $this->input->post( 'sku' ),
					'expiration_date'	=> $this->input->post( 'expiration_date' ),
					'purchase_price'	=> (float)$this->input->post( 'purchase_price' ),
					'unit_price'		=> (float)$this->input->post( 'unit_price' ),
					'init_quantity'		=> (float)( ( ! empty( $this->input->post( 'init_quantity' ) ) ) ? $this->input->post( 'init_quantity' ) : $this->input->post( 'quantity' ) ),
					'quantity'			=> (float)( (float)$this->input->post( 'quantity' ) + (float)$this->input->post( 'init_quantity' ) ),
				);

				$stocked = $this->stock->get( array(
					'where' => array(
						'sku' => $this->input->post( 'sku' ),
					)
				) );

				if ( ! empty( $stocked[0] ) )
				{
					$store['stock_id'] = $stocked[0]['stock_id'];
					$store['edit'] = 1;
				}

				if ( $this->input->post( 'last_row' ) )
				{
					$this->purchases->save( array( 'purchase_id' => $this->input->post( 'purchase_id' ), 'stocked' => 1 ) );
					$this->alerts->persist( TRUE )->alert( __( "The products from the purchase have been successfully stocked into the database." ), __( "Purchase Stocked" ), NULL, 'success' );
					$this->result['redirect'] = site_url( 'stock/lists' );
				}

				if ( $stock_id = $this->stock->save( $store ) )
				{
					// Update meta stocked to 1;
					$this->purchases->meta_edit( array( 'purchase_meta_id' => $this->input->post( 'purchase_meta_id' ), 'stocked' => '1' ) );

					if ( ! isset( $store['stock_id'] ) ) $store['stock_id'] = $stock_id;
					$store['purchase_meta_id'] = $this->input->post( 'purchase_meta_id' );

					$this->logs->set_data( $store, "POST" )->set_result( TRUE )->register();
					$this->result['item_row'] = $this->stock->get( $this->input->post( 'sku' ) );
					$this->result['message'] = __( "Product stocked successfully." );
				}
				else
				{
					$this->logs->set_data( $store, "POST" )->set_result( FALSE )->register();
					$this->result['error'] = __( "There were some errors during the task. Please, try again." );
				}
			}
			else
			{
				$this->result['error'] = sprintf( __( "There were some errors during the task: %s" ), strip_tags( validation_errors() ) );
			}
		}

		$this->output_result();
	}

	/**
	 * Stores or updates a product row into the DB
	 * 
	 * @return output
	 */
	public function store_product()
	{
		$this->load->model( "Products_model", "products" );

		if ( $this->input->post() )
		{
			$this->form_validation->set_rules( 'name', 			__( "Name" ),			'trim|required' );
			$this->form_validation->set_rules( 'unit_measure', 	__( "Unit measure" ),	'trim|required' );
			$this->form_validation->set_rules( 'category_id', 	__( "Category" ),		'trim|required' );
			$this->form_validation->set_rules( 'price', 		__( "Price" ),			'trim|required' );
			$this->form_validation->set_rules( 'currency', 		__( "Currency" ),		'trim|required' );
			$this->form_validation->set_rules( 'tax', 			__( "Tax" ),			'trim|required' );

			if ( $this->form_validation->run() )
			{
				$store = array(
					'name' 				=> $this->input->post( 'name' ),
					'unit_measure'		=> $this->input->post( 'unit_measure' ),
					'category_id' 		=> $this->input->post( 'category_id' ),
					'price' 			=> $this->input->post( 'price' ),
					'currency'			=> $this->input->post( 'currency' ),
					'tax'				=> $this->input->post( 'tax' ),
					// Defaults
					'min_stock_warning'	=> $this->assets->conf['stock']['min_warning'],
					'min_stock_danger'	=> $this->assets->conf['stock']['min_danger'],
					'unit_quantity'		=> 1,
					'free'				=> 0,
				);

				if ( $product_id = $this->products->save( $store ) )
				{
					if ( ! isset( $store['product_id'] ) ) $store['product_id'] = $product_id;

					$this->logs->set_data( $store, "POST" )->set_result( TRUE )->register();
					$this->result['item_row'] = $this->products->get( $product_id );
					$this->result['message'] = __( "Product stored successfully." );
				}
				else
				{
					$this->logs->set_data( $store, "POST" )->set_result( FALSE )->register();
					$this->result['error'] = __( "There were some errors during the task. Please, try again." );
				}
			}
			else
			{
				$this->result['error'] = sprintf( __( "There were some errors during the task: %s" ), strip_tags( validation_errors() ) );
			}
		}

		$this->output_result();
	}

	/**
	 * --------------------------------------------------------------------------------------------------
	 * SALES ACTIONS
	 * --------------------------------------------------------------------------------------------------
	 */

	/**
	 * Retrieve the next bill number for the sale
	 * 
	 * @return output
	 */
	public function generate_bill_number()
	{
		$this->load->model( "Sales_model", "sales" );
		$this->load->model( "Print_orders_model", "print_orders" );
		$this->load->model( "Pos_model", "pos" );

		// If we're editing, and we don't touch bill number or pos or print order, then we can override the verification.
		if ( $this->input->post( 'sale_id' ) )
		{
			$sale = $this->sales->get( $this->input->post( 'sale_id' ) );

			if ( ! empty( $sale ) )
			{
				if ( 
					$sale['bill_number'] == $this->input->post( 'bill_number' ) AND
					$sale['print_order'] == $this->input->post( 'print_order' ) AND
					$sale['pos_id'] == $this->input->post( 'pos_id' )
				)
				{
					unset( $_POST['pos_id'] );
					$this->result['print_order'] = $sale['print_order'];
					$this->result['bill_number'] = $sale['bill_number'];
				}
			}
		}

		if ( $this->input->post( 'pos_id' ) )
		{
			$goon = true;

			$where = array(
				'trash' => 0,
				'pos_id' => $this->input->post( 'pos_id' ),
			);

			if ( $this->input->post( 'print_order' ) ) $where['print_order'] = $this->input->post( 'print_order' );

			$print_order = $this->print_orders->get( array(
				'where' => $where,
				'limit' => 1,
				'order_by' => array(
					'print_order_id' => 'DESC',
				),
			) );

			$print_order = ( isset( $print_order[0] ) ) ? $print_order[0] : NULL;

			if ( ! empty( $print_order ) )
			{
				if ( ! $this->print_orders->is_still_valid( $print_order ) )
				{
					$goon = false;
					$this->result['error'] = __( "The print order for this POS is not valid anymore. You will have to add a new print order for the POS." );
				}

				if ( $this->print_orders->count_remaining_bills( $print_order ) <= 0 )
				{
					$goon = false;
					$this->result['error'] = __( "The print order for this POS runned out of available numbers. You will have to add a new print order for the POS." );
				}

				if ( $goon AND ! $this->input->post( 'move' ) )
				{
					$this->result['print_order'] = $print_order['print_order'];
					$this->result['bill_number'] = $this->pos->generate_bill_number( $this->input->post( 'pos_id' ) );

					if ( $this->input->post( 'bill_number' ) AND ! $this->pos->bill_number_exists( $this->input->post( 'bill_number' ) ) )
					{
						unset( $this->result['bill_number'], $this->result['print_order'] );
					}
					else if ( $this->pos->bill_number_exists( $this->input->post( 'bill_number' ) ) )
					{
						$this->result['bill_number_exists_message'] = sprintf( 
							__( "The bill number %s will be changed to %s because it has just been used in other sale." ),
							'<strong>' . $this->input->post( 'bill_number' ) . '</strong>',
							'<strong>' . $this->result['bill_number'] . '</strong>'
						);
					}
				}
				else if ( $this->input->post( 'move' ) AND $this->input->post( 'bill_number' ) )
				{
					$found = false;
					$bill_number = explode( "-", $this->input->post( 'bill_number' ) );
					$bill_number = (int)$bill_number[2];

					while ( ! $found )
					{
						switch ( $this->input->post( 'move' ) )
						{
							case 'up':
								$bill_number++;
								
								if ( $bill_number > (int)$print_order['number_end'] )
								{
									$found = true;
									$this->result['error'] = __( "You've reached the end limit for this print order." );
								}
								break;

							case 'down':
								$bill_number--;

								if ( $bill_number < (int)$print_order['number_start'] )
								{
									$found = true;
									$this->result['error'] = __( "You've reached the start limit for this print order." );
								}
								break;
						}

						$bill_number_result = $this->pos->build_bill_number( $print_order, $bill_number );
						
						if ( ! $this->pos->bill_number_exists( $bill_number_result ) )
						{
							$found = true;

							$this->result['print_order'] = $print_order['print_order'];
							$this->result['bill_number'] = $bill_number_result;
						}
					}
				}
			}
		}

		$this->output_result();
	}

	/**
	 * Searchs into the clients table for a match
	 * 
	 * @return output
	 */
	public function search_client()
	{
		$this->load->model( "Clients_model", "clients" );

		$this->result['results'] = array();

		if ( ! empty( $_GET['term'] ) )
		{
			$this->db->like( 'first_name', $_GET['term'] );
			$this->db->or_like( 'last_name', $_GET['term'] );
			$this->db->or_like( 'ruc', $_GET['term'] );

			$clients_result = $this->clients->get();

			if ( ! empty( $clients_result ) )
			{
				foreach ( $clients_result AS $client )
				{
					$this->result['results'][] = array(
						'id' 	=> $client['client_id'],
						'text' 	=> $client['first_name'] . " " . $client['last_name'] . " [" . $client['ruc'] . "]",
					);
				}
			}
		}

		$this->output_result();
	}

	/**
	 * Gets a client according to the given ID
	 *
	 * @param  int $client_id The client ID
	 * @return output
	 */
	public function get_client( $client_id )
	{
		$this->load->model( "Clients_model", "clients" );

		$client = $this->clients->get( $client_id );

		if ( ! empty( $client ) )
		{
			$this->result = $client;
		}
		else
		{
			$this->result['error'] = __( "The client is not stored into the database." );
		}

		$this->output_result();
	}

	/**
	 * Stores a new client into the DB
	 * 
	 * @return output
	 */
	public function store_client()
	{
		$this->load->model( "Clients_model", "clients" );

		$this->form_validation->set_rules( 'first_name', __( "First Name" ), 'trim|required' );

		if ( $this->form_validation->run() )
		{
			if ( $client_id = $this->clients->save( $this->input->post() ) )
			{
				$this->result = $this->clients->get( $client_id );
			}
			else
			{
				$this->result['error'] = __( "There were some errors while trying to perform the task. Please try again." );
			}
		}
		else
		{
			$this->result['error'] = sprintf( __( "There were some errors during the task: %s" ), strip_tags( validation_errors() ) );
		}

		$this->output_result();
	}

	/**
	 * Searchs for the stocked products available for sell
	 * 
	 * @return output
	 */
	public function search_stock()
	{
		$this->load->model( "Stock_model", "stock" );
		$this->load->model( "Products_model", "products" );
		$this->load->model( "Pos_model", "pos" );

		$this->result['results'] = array();
		$forbidden_products = array();

		// Fetch the POS categories that are available
		if ( $this->input->get( 'pos_id' ) ) 
		{
			$active_categories = $this->pos->get_categories( $_GET['pos_id'] );
			$this->db->reset_query();
		}

		if ( ! empty( $_GET['term'] ) )
		{
			// First, get stocked items
			$tb_products = $this->assets->conf['tables']['products'];
			$tb_stock = $this->assets->conf['tables']['stock'];

			$this->db->group_start();
			$this->db->where( "$tb_stock.trash", 0 );
			$this->db->where( "$tb_stock.quantity >", 0 );
			if ( isset( $_GET['product'] ) ) // To pick directly for orders
			{
				$this->db->where( "$tb_stock.product_id", $_GET['term'] );
			}
			$this->db->group_end();

			if ( ! isset( $_GET['product'] ) ) // To override the direct pick for orders
			{
				$this->db->group_start();
				$this->db->where( "$tb_stock.sku", $_GET['term'] );
				$this->db->or_like( "$tb_products.name", $_GET['term'] );
				$this->db->group_end();
			}

			// Check against active categories
			if ( isset( $active_categories ) AND is_array( $active_categories ) )
			{
				$this->db->group_start();
				foreach ( $active_categories AS $k => $v )
				{
					$this->db->or_where( "$tb_products.category_id", $k );
				}
				$this->db->group_end();
			}

			$this->db->join( $tb_products, "$tb_products.product_id = $tb_stock.product_id", "left" );

			$this->db->select( array(
				"$tb_stock.stock_id",
				"$tb_stock.product_id",
				"$tb_stock.purchase_id",
				"$tb_stock.code",
				"$tb_stock.sku",
				"$tb_stock.quantity",
				"$tb_stock.init_quantity",
				"$tb_stock.purchase_quantity",
				"$tb_stock.purchase_price",
				"$tb_stock.unit_price",
				"$tb_stock.expiration_date",
				"$tb_stock.created",
				"$tb_stock.creator",
				"$tb_stock.modified",
				"$tb_stock.modifier",
				"$tb_stock.trash",
				"$tb_products.category_id",
				"$tb_products.name",
				"$tb_products.description",
				"$tb_products.unit_measure",
				"$tb_products.unit_quantity",
				"$tb_products.price",
				"$tb_products.currency",
				"$tb_products.tax",
				"$tb_products.min_stock_warning",
				"$tb_products.min_stock_danger",
				"$tb_products.free",
			) );

			$stock = $this->stock->get();

			if ( ! empty( $stock ) )
			{
				foreach ( $stock AS $item )
				{
					$this->result['results'][] = array(
						'id' 		=> $item['sku'],
						'text' 		=> $item['name'] . " [" . $item['sku'] . "]",
						'stock'		=> $item,
						'picture'	=> $this->products->picture( $item['product_id'] ),
						'labels'	=> array(
							'quantity'	=> __( "Quantity: " ),
							'price'		=> __( "Price: " ),
						)
					);

					$forbidden_products[] = $item['product_id'];
				}
			}

			$this->db->reset_query();
			
			// Then let's get the sale-free products
			$this->db->group_start();
			$this->db->where( 'trash', 0 );
			$this->db->where( 'free', 1 );
			$this->db->like( 'name', $_GET['term'] );
			$this->db->group_end();

			// Check against active categories
			if ( isset( $active_categories ) AND is_array( $active_categories ) )
			{
				$this->db->group_start();
				foreach ( $active_categories AS $k => $v )
				{
					$this->db->or_where( "category_id", $k );
				}
				$this->db->group_end();
			}

			// Let's prefer stocked items instead of free sale items
			// On 08-03-2020 I commented this because you should be able to pick in case the product is from stock or free
			// The best way to do this actually is fractionate the free part from the stock part in the JS of the form
			// So in case the user surpasses the available quantity, the rest is assigned to the free product in another row.
			// Look at the order in the way it adds products, and try to replicate it.
			// 
			// if ( ! empty( $forbidden_products ) ) $this->db->where_not_in( 'product_id', $forbidden_products );

			if ( isset( $_GET['product'] ) ) // To pick directly for orders
			{
				$this->db->reset_query();
				$this->db->where( "product_id", $_GET['term'] );
				$this->db->where( "free", 1 );
			}

			$free_products = $this->products->get();

			if ( ! empty( $free_products ) )
			{
				foreach ( $free_products AS $item )
				{
					$this->result['results'][] = array(
						'id' 		=> $item['product_id'],
						'text' 		=> $item['name'],
						'product'	=> $item,
						'picture'	=> $this->products->picture( $item['product_id'] ),
						'labels'	=> array(
							'quantity'	=> __( "Quantity: " ),
							'price'		=> __( "Price: " ),
						)
					);
				}
			}
		}

		$this->output_result();
	}

	/**
	 * Retrieves the full list of available stock for the POS
	 * 
	 * @return output
	 */
	public function retrieve_full_stock()
	{
		$this->load->model( "Stock_model", "stock" );
		$this->load->model( "Products_model", "products" );

		$tb_products = $this->assets->conf['tables']['products'];
		$tb_stock = $this->assets->conf['tables']['stock'];

		$this->db->select( "$tb_stock.*" );
		$this->db->group_start();
		$this->db->where( "$tb_stock.trash", 0 );
		$this->db->where( "$tb_stock.quantity >", 0 );
		$this->db->group_end();

		if ( $this->input->get( 'pos_id' ) ) 
		{
			// Retrieve the available categories for the POS here
			// and then use that array for the where in in which
			// we will determine which products to show to the POS
			// 
			// $this->db->where_in();
		}

		$this->db->join( $tb_products, "$tb_stock.product_id = $tb_products.product_id" );

		if ( $query = $this->db->get( $tb_stock ) AND $query->num_rows() > 0 )
		{
			foreach ( $query->result_array() AS $i => $item )
			{
				$product = $this->products->get( $item['product_id'] );

				foreach ( $item AS $key => $val ) $this->result[$i][$key] = ( is_numeric( $val ) ) ? (float)$val : $val;

				$this->result[$i]['product'] = $product;
				$this->result[$i]['picture'] = $this->products->picture( $item['product_id'] );
				$this->result[$i]['labels'] = array(
					'quantity'	=> __( "Quantity: " ),
					'price'		=> __( "Price: " ),
				);
			}
		}

		$this->output_result();
	}

}
