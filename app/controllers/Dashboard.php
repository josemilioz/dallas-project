<?php defined( 'BASEPATH' ) OR exit( 'No direct script access allowed' );

class Dashboard extends CI_Controller
{
	/**
	 * Class constructor
	 */
	public function __construct()
	{
		parent::__construct();
		$this->data['tags']['controller_name'] = __( "Dashboard" );

		$this->assets->load_style( 'main' );
	}

	/**
	 * Index Page for Dashboard
	 * 
	 * @return none
	 */
	public function index()
	{
		if ( ! $this->ion_auth->logged_in() ) redirect( 'login', 'refresh' );
		$this->load->model( 'Dashboard_model', 'dashboard' );
		$this->data['tags']['welcome_title'] = sprintf( __( "¡Hola %s!" ), $this->ion_auth->user()->row()->first_name );
		$this->templates->view( 'index', $this->data );
	}

	/**
	 * Error 404 display page
	 * 
	 * @return echo
	 */
	public function page404()
	{
		$this->assets->load_style( 'login.css' );

		$this->load->view( 'auth/header' );
		$this->load->view( 'singles/404' );
		$this->load->view( 'auth/footer' );
	}

	/**
	 * Outputs the ajax login form for ajax re-login
	 * 
	 * @return echo
	 */
	public function async_login()
	{
		$this->load->library( array( 'ion_auth', 'form_validation' ) );
		$this->load->helper( array( 'url', 'language' ) );
		$this->form_validation->set_error_delimiters( $this->config->item( 'error_start_delimiter', 'ion_auth' ), $this->config->item( 'error_end_delimiter', 'ion_auth' ) );
		$this->lang->load( 'auth' );

		if ( $_POST )
		{
			$this->form_validation->set_rules( 'identity', str_replace( ':', '', $this->lang->line( 'login_identity_label' ) ), 'required' );
			$this->form_validation->set_rules( 'password', str_replace( ':', '', $this->lang->line( 'login_password_label' ) ), 'required' );

			if ( $this->form_validation->run() )
			{
				$remember = ( bool ) $this->input->post( 'remember' );

				if ( $this->ion_auth->login( $this->input->post( 'identity' ), $this->input->post( 'password' ), $remember ) )
				{
					// success
					$this->logs->set_data( NULL, "POST" )->set_result( TRUE )->register();
					$this->alerts->alert( NULL, NULL, $this->ion_auth->messages(), 'success', FALSE );
				}
				else
				{
					// un-successful
					set_status_header( 403 );
					$this->logs->set_data( $_POST, "POST" )->set_result( FALSE )->register();
					$this->alerts->alert( NULL, NULL, $this->ion_auth->errors(), 'danger', FALSE );
				}
			}
			else
			{
				set_status_header( 403 );
				$this->logs->set_data( $_POST, "POST" )->set_result( FALSE )->register();
				$this->alerts->alert( NULL, NULL, validation_errors(), 'danger', FALSE );
			}

			ob_start();
			$this->alerts->output_alert();
			$alert = ob_get_clean();

			die( json_encode( array(
				'alert' => $alert,
			) ) );
			exit;
		}

		// if ( ! $this->ion_auth->logged_in() ) 
		$this->load->view( 'singles/ajax-login' );
	}
}
