<?php defined( 'BASEPATH' ) OR exit( 'No direct script access allowed' );

/**
 * Translator function
 * 
 * @param  string $text
 * @return string
 */
function translate( $text )
{
	return $text;
}

/**
 * String returner
 * 
 * @param  string $text
 * @return string
 */
function __( $text )
{
	return translate( $text );
}

/**
 * String printer
 * 
 * @param  string $text
 * @return string
 */
function _e( $text )
{
	echo translate( $text );
}

function _n( $single, $plural, $number )
{
	if ( $number == 1 )
	{
		$text = $single;
	}
	else
	{
		$text = $plural;
	}

	return translate( $text );
}