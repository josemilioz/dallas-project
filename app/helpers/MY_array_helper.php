<?php defined( 'BASEPATH' ) OR exit( 'No direct script access allowed' );

/**
 * Walk through a multidimensional array and in case there is an 
 * empty string or element, it places a message as the value
 * 
 * @param  array  $array   The array to be walked
 * @param  string $message The message to replace the empty value (optional)
 * @return array
 */
function apply_empty_message_array( $array, $message = "" )
{
	foreach ( $array AS $key => $val )
	{
		if ( ! is_array( $val ) )
		{
			if ( empty( $val ) AND 
				 $key !== "modified" AND 
				 $key !== "created" AND 
				 $key !== "modifier" AND 
				 $key !== "creator" AND
				 $key !== "trash" AND 
				 $key !== "billed" AND
				 $key !== "sold" AND
				 $key !== "stocked"
			) $array[$key] = trim( $message );

			// return $val;
		}
		else if ( $key !== "original" )
		{
			if ( ! empty( $val ) ) $array[$key] = apply_empty_message_array( $val, trim( $message ) );
		}
	}

	return $array;
}
