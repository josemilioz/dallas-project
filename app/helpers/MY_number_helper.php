<?php defined( 'BASEPATH' ) OR exit( 'No direct script access allowed' );

/**
 * Override of the native number_format PHP's function in order to globalize
 * the use of the last special chars that determine the output of the number
 * given.
 * 
 * @param  float   $number              The number to format
 * @param  integer $decimals            The quantity of decimals available
 * @param  string  $decimal_point       The decimal point character
 * @param  string  $thousands_separator The thousands separator character
 * @return string
 */
function my_number_format( $number, $decimals = NULL, $decimal_point = NULL, $thousands_separator = NULL )
{
	$CI =& get_instance();
	$decimals 				= ( empty( $decimals ) ) 			? $CI->assets->conf['decimals'] 			: $decimals;
	$decimal_point 			= ( empty( $decimal_point ) ) 		? $CI->assets->conf['decimal_point'] 		: $decimal_point;
	$thousands_separator 	= ( empty( $thousands_separator ) ) ? $CI->assets->conf['thousands_separator']	: $thousands_separator;

	return number_format( (float)$number, $decimals, $decimal_point, $thousands_separator );
}