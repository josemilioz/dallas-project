<?php
/**
 * CodeIgniter
 *
 * An open source application development framework for PHP
 *
 * This content is released under the MIT License (MIT)
 *
 * Copyright (c) 2014 - 2018, British Columbia Institute of Technology
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 * @package	CodeIgniter
 * @author	EllisLab Dev Team
 * @copyright	Copyright (c) 2008 - 2014, EllisLab, Inc. (https://ellislab.com/)
 * @copyright	Copyright (c) 2014 - 2018, British Columbia Institute of Technology (http://bcit.ca/)
 * @license	http://opensource.org/licenses/MIT	MIT License
 * @link	https://codeigniter.com
 * @since	Version 1.0.0
 * @filesource
 */
defined('BASEPATH') OR exit('No direct script access allowed');

$lang['cal_su'] 			= __( 'Su' );
$lang['cal_mo'] 			= __( 'Mo' );
$lang['cal_tu'] 			= __( 'Tu' );
$lang['cal_we'] 			= __( 'We' );
$lang['cal_th'] 			= __( 'Th' );
$lang['cal_fr'] 			= __( 'Fr' );
$lang['cal_sa'] 			= __( 'Sa' );
$lang['cal_sun'] 			= __( 'Sun' );
$lang['cal_mon'] 			= __( 'Mon' );
$lang['cal_tue'] 			= __( 'Tue' );
$lang['cal_wed'] 			= __( 'Wed' );
$lang['cal_thu'] 			= __( 'Thu' );
$lang['cal_fri'] 			= __( 'Fri' );
$lang['cal_sat'] 			= __( 'Sat' );
$lang['cal_sunday'] 		= __( 'Sunday' );
$lang['cal_monday'] 		= __( 'Monday' );
$lang['cal_tuesday'] 		= __( 'Tuesday' );
$lang['cal_wednesday'] 		= __( 'Wednesday' );
$lang['cal_thursday'] 		= __( 'Thursday' );
$lang['cal_friday'] 		= __( 'Friday' );
$lang['cal_saturday'] 		= __( 'Saturday' );
$lang['cal_jan'] 			= __( 'Jan' );
$lang['cal_feb'] 			= __( 'Feb' );
$lang['cal_mar'] 			= __( 'Mar' );
$lang['cal_apr'] 			= __( 'Apr' );
$lang['cal_may'] 			= __( 'May' );
$lang['cal_jun'] 			= __( 'Jun' );
$lang['cal_jul'] 			= __( 'Jul' );
$lang['cal_aug'] 			= __( 'Aug' );
$lang['cal_sep'] 			= __( 'Sep' );
$lang['cal_oct'] 			= __( 'Oct' );
$lang['cal_nov'] 			= __( 'Nov' );
$lang['cal_dec'] 			= __( 'Dec' );
$lang['cal_january'] 		= __( 'January' );
$lang['cal_february'] 		= __( 'February' );
$lang['cal_march'] 			= __( 'March' );
$lang['cal_april'] 			= __( 'April' );
$lang['cal_mayl'] 			= __( 'May' );
$lang['cal_june'] 			= __( 'June' );
$lang['cal_july'] 			= __( 'July' );
$lang['cal_august'] 		= __( 'August' );
$lang['cal_september'] 		= __( 'September' );
$lang['cal_october'] 		= __( 'October' );
$lang['cal_november'] 		= __( 'November' );
$lang['cal_december'] 		= __( 'December' );
