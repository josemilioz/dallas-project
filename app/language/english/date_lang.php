<?php
/**
 * CodeIgniter
 *
 * An open source application development framework for PHP
 *
 * This content is released under the MIT License (MIT)
 *
 * Copyright (c) 2014 - 2018, British Columbia Institute of Technology
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 * @package	CodeIgniter
 * @author	EllisLab Dev Team
 * @copyright	Copyright (c) 2008 - 2014, EllisLab, Inc. (https://ellislab.com/)
 * @copyright	Copyright (c) 2014 - 2018, British Columbia Institute of Technology (http://bcit.ca/)
 * @license	http://opensource.org/licenses/MIT	MIT License
 * @link	https://codeigniter.com
 * @since	Version 1.0.0
 * @filesource
 */
defined('BASEPATH') OR exit('No direct script access allowed');

$lang['date_year'] 			= __( 'Year' );
$lang['date_years'] 		= __( 'Years' );
$lang['date_month'] 		= __( 'Month' );
$lang['date_months'] 		= __( 'Months' );
$lang['date_week'] 			= __( 'Week' );
$lang['date_weeks'] 		= __( 'Weeks' );
$lang['date_day'] 			= __( 'Day' );
$lang['date_days'] 			= __( 'Days' );
$lang['date_hour'] 			= __( 'Hour' );
$lang['date_hours'] 		= __( 'Hours' );
$lang['date_minute'] 		= __( 'Minute' );
$lang['date_minutes'] 		= __( 'Minutes' );
$lang['date_second'] 		= __( 'Second' );
$lang['date_seconds'] 		= __( 'Seconds' );

$lang['UM12']		= __( '(UTC -12:00) Baker/Howland Island' );
$lang['UM11']		= __( '(UTC -11:00) Niue' );
$lang['UM10']		= __( '(UTC -10:00) Hawaii-Aleutian Standard Time, Cook Islands, Tahiti' );
$lang['UM95']		= __( '(UTC -9:30) Marquesas Islands' );
$lang['UM9']		= __( '(UTC -9:00) Alaska Standard Time, Gambier Islands' );
$lang['UM8']		= __( '(UTC -8:00) Pacific Standard Time, Clipperton Island' );
$lang['UM7']		= __( '(UTC -7:00) Mountain Standard Time' );
$lang['UM6']		= __( '(UTC -6:00) Central Standard Time' );
$lang['UM5']		= __( '(UTC -5:00) Eastern Standard Time, Western Caribbean Standard Time' );
$lang['UM45']		= __( '(UTC -4:30) Venezuelan Standard Time' );
$lang['UM4']		= __( '(UTC -4:00) Atlantic Standard Time, Eastern Caribbean Standard Time' );
$lang['UM35']		= __( '(UTC -3:30) Newfoundland Standard Time' );
$lang['UM3']		= __( '(UTC -3:00) Argentina, Brazil, French Guiana, Uruguay' );
$lang['UM2']		= __( '(UTC -2:00) South Georgia/South Sandwich Islands' );
$lang['UM1']		= __( '(UTC -1:00) Azores, Cape Verde Islands' );
$lang['UTC']		= __( '(UTC) Greenwich Mean Time, Western European Time' );
$lang['UP1']		= __( '(UTC +1:00) Central European Time, West Africa Time' );
$lang['UP2']		= __( '(UTC +2:00) Central Africa Time, Eastern European Time, Kaliningrad Time' );
$lang['UP3']		= __( '(UTC +3:00) Moscow Time, East Africa Time, Arabia Standard Time' );
$lang['UP35']		= __( '(UTC +3:30) Iran Standard Time' );
$lang['UP4']		= __( '(UTC +4:00) Azerbaijan Standard Time, Samara Time' );
$lang['UP45']		= __( '(UTC +4:30) Afghanistan' );
$lang['UP5']		= __( '(UTC +5:00) Pakistan Standard Time, Yekaterinburg Time' );
$lang['UP55']		= __( '(UTC +5:30) Indian Standard Time, Sri Lanka Time' );
$lang['UP575']		= __( '(UTC +5:45) Nepal Time' );
$lang['UP6']		= __( '(UTC +6:00) Bangladesh Standard Time, Bhutan Time, Omsk Time' );
$lang['UP65']		= __( '(UTC +6:30) Cocos Islands, Myanmar' );
$lang['UP7']		= __( '(UTC +7:00) Krasnoyarsk Time, Cambodia, Laos, Thailand, Vietnam' );
$lang['UP8']		= __( '(UTC +8:00) Australian Western Standard Time, Beijing Time, Irkutsk Time' );
$lang['UP875']		= __( '(UTC +8:45) Australian Central Western Standard Time' );
$lang['UP9']		= __( '(UTC +9:00) Japan Standard Time, Korea Standard Time, Yakutsk Time' );
$lang['UP95']		= __( '(UTC +9:30) Australian Central Standard Time' );
$lang['UP10']		= __( '(UTC +10:00) Australian Eastern Standard Time, Vladivostok Time' );
$lang['UP105']		= __( '(UTC +10:30) Lord Howe Island' );
$lang['UP11']		= __( '(UTC +11:00) Srednekolymsk Time, Solomon Islands, Vanuatu' );
$lang['UP115']		= __( '(UTC +11:30) Norfolk Island' );
$lang['UP12']		= __( '(UTC +12:00) Fiji, Gilbert Islands, Kamchatka Time, New Zealand Standard Time' );
$lang['UP1275']		= __( '(UTC +12:45) Chatham Islands Standard Time' );
$lang['UP13']		= __( '(UTC +13:00) Samoa Time Zone, Phoenix Islands Time, Tonga' );
$lang['UP14']		= __( '(UTC +14:00) Line Islands' );
