<?php
/**
 * CodeIgniter
 *
 * An open source application development framework for PHP
 *
 * This content is released under the MIT License (MIT)
 *
 * Copyright (c) 2014 - 2018, British Columbia Institute of Technology
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 * @package	CodeIgniter
 * @author	EllisLab Dev Team
 * @copyright	Copyright (c) 2008 - 2014, EllisLab, Inc. (https://ellislab.com/)
 * @copyright	Copyright (c) 2014 - 2018, British Columbia Institute of Technology (http://bcit.ca/)
 * @license	http://opensource.org/licenses/MIT	MIT License
 * @link	https://codeigniter.com
 * @since	Version 1.0.0
 * @filesource
 */
defined('BASEPATH') OR exit('No direct script access allowed');

$lang['ut_test_name'] 		= __( 'Test Name' );
$lang['ut_test_datatype'] 	= __( 'Test Datatype' );
$lang['ut_res_datatype'] 	= __( 'Expected Datatype' );
$lang['ut_result'] 			= __( 'Result' );
$lang['ut_undefined'] 		= __( 'Undefined Test Name' );
$lang['ut_file'] 			= __( 'File Name' );
$lang['ut_line'] 			= __( 'Line Number' );
$lang['ut_passed'] 			= __( 'Passed' );
$lang['ut_failed'] 			= __( 'Failed' );
$lang['ut_boolean'] 		= __( 'Boolean' );
$lang['ut_integer'] 		= __( 'Integer' );
$lang['ut_float'] 			= __( 'Float' );
$lang['ut_double'] 			= __( 'Float' ); // can be the same as float
$lang['ut_string'] 			= __( 'String' );
$lang['ut_array'] 			= __( 'Array' );
$lang['ut_object'] 			= __( 'Object' );
$lang['ut_resource'] 		= __( 'Resource' );
$lang['ut_null'] 			= __( 'Null' );
$lang['ut_notes'] 			= __( 'Notes' );
