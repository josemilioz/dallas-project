
<div class="card">
	<div class="card-body">
		<div id="brief-wrapper">

			<div class="row">
				<div class="col-md-8 mb-3">
					<div class="element-group row">
						<div class="col-3"><span class="item-label"><?php _e( "Client" ); ?></span></div>
						<div class="col-9"><span class="full-name"><?php echo $this->clients->name( $client, "full" ); ?></span></div>
					</div>
					<div class="element-group row">
						<div class="col-3"><span class="item-label"><?php _e( "Observations" ); ?></span></div>
						<div class="col-9"><span><?php echo nl2br( $order['observations'] ); ?></span></div>
					</div>
					<?php if ( ! empty( $files ) ) : ?>
					<div class="element-group row">
						<div class="col-3"><span class="item-label"><?php _e( "Attachments" ); ?></span></div>
						<div class="col-9"><span><?php 
							$attachs = array(); 
							foreach ( $files['files'] AS $file ) $attachs[] = anchor( 'files/download/' . $file['file_id'], $file['original_name'] ) . ' <small class="text-muted">(' . byte_format( $file['size'], 2 ) . ')</small>';
							echo implode( ", ", $attachs );
						?></span></div>
					</div>
					<?php endif; ?>
				</div>

				<div class="col-md-4 mb-3">
					<div class="element-group row">
						<div class="col-5"><span class="item-label"><?php _e( "Order date" ); ?></span></div>
						<div class="col-7"><span><?php echo ( ! empty( $order['original']['order_date'] ) ) ? date( $this->assets->conf['date_format'], strtotime( $order['order_date'] ) ) : $order['order_date']; ?></span></div>
					</div>
					<div class="element-group row">
						<div class="col-5"><span class="item-label"><?php _e( "Delivery date" ); ?></span></div>
						<div class="col-7"><span><?php echo ( ! empty( $order['original']['delivery_date'] ) ) ? date( $this->assets->conf['date_format'], strtotime( $order['delivery_date'] ) ) : $order['delivery_date']; ?></span></div>
					</div>
					<div class="element-group row">
						<div class="col-5"><span class="item-label"><?php _e( "Due date" ); ?></span></div>
						<div class="col-7"><span><?php echo ( ! empty( $order['original']['expiration_date'] ) ) ? date( $this->assets->conf['date_format'], strtotime( $order['expiration_date'] ) ) : $order['expiration_date']; ?></span></div>
					</div>
				</div>
			</div>

			<div class="table-responsive">
				<table class="table table-striped table-hover product-list">
					<col width="auto">
					<col width="160">
					<col width="160">
					<col width="160">
					<col width="160">
					<thead>
						<tr>
							<th><?php _e( "Product" ); ?></th>
							<th class="text-right"><abbr title="<?php _e( "Price per unit" ); ?>"><?php _e( "Price" ); ?></abbr> <small class="text-muted">(<?php echo $this->assets->conf['currencies'][$this->assets->conf['currency']]; ?>)</small></th>
							<th class="text-right"><?php _e( "Quantity" ); ?></th>
							<th class="text-right"><?php _e( "Remaining" ); ?></th>
							<th class="text-right"><?php _e( "Total" ); ?> <small class="text-muted">(<?php echo $this->assets->conf['currencies'][$this->assets->conf['currency']]; ?>)</small></th>
						</tr>
					</thead>
					<tfoot>
						<tr>
							<th><?php _e( "Product" ); ?></th>
							<th class="text-right"><abbr title="<?php _e( "Price per unit" ); ?>"><?php _e( "Price" ); ?></abbr> <small class="text-muted">(<?php echo $this->assets->conf['currencies'][$this->assets->conf['currency']]; ?>)</small></th>
							<th class="text-right"><?php _e( "Quantity" ); ?></th>
							<th class="text-right"><?php _e( "Remaining" ); ?></th>
							<th class="text-right"><?php _e( "Total" ); ?> <small class="text-muted">(<?php echo $this->assets->conf['currencies'][$this->assets->conf['currency']]; ?>)</small></th>
						</tr>
					</tfoot>
					<tbody>
						<?php if ( ! empty( $meta ) ) : foreach ( $meta AS $row ) : $product = $this->products->get( $row['product_id'] ); ?>
						<tr>
							<td><?php echo ( ! empty( $row['description'] ) ) ? $row['description'] : $product['name']; ?></td>
							<td class="text-right"><?php echo my_number_format( $row['unit_price'], $this->assets->conf['decimals'] ); ?></td>
							<td class="text-right"><?php printf( "%s %s", my_number_format( $row['quantity'], $this->assets->conf['qty_decimals'] ), '<small class="text-muted">' . $this->assets->conf['units'][$product['unit_measure']] . '</small>' ); ?></td>
							<td class="text-right"><?php printf( "%s %s", my_number_format( $row['quantity_remaining'], $this->assets->conf['qty_decimals'] ), '<small class="text-muted">' . $this->assets->conf['units'][$product['unit_measure']] . '</small>' ); ?></td>
							<td class="text-right"><?php echo my_number_format( $row['row_total'], $this->assets->conf['decimals'] ); ?></td>
						</tr>
						<?php endforeach; endif; ?>
						<tr class="table-info">
							<th colspan="4" class="text-right"><?php _e( "Total amount" ); ?></th>
							<th class="text-right"><h6 class="mb-0"><?php echo my_number_format( $order['total_amount'], $this->assets->conf['decimals'] ); ?></h6></th>
						</tr>
					</tbody>
				</table>
			</div>

			<?php if ( ! empty( $order['created'] ) OR ! empty( $order['modified'] ) ) : ?>
			<div class="row last-activity">
				<div class="col">
					<div class="created">
						<?php if ( ! empty( $order['created'] ) ) : ?>
						<p class="label"><?php _e( "Created by" ); ?></p>
						<p class="value"><?php echo $this->ion_auth->user_name( "fullname", $order['creator'] ) ?> <small><?php echo date( $this->assets->conf['datetime_format'], strtotime( $order['created'] ) ); ?></small></p>
						<?php endif; ?>
					</div>
				</div>
				<div class="col">
					<div class="modified">
						<?php if ( ! empty( $order['modified'] ) ) : ?>
						<p class="label"><?php _e( "Last modified by" ); ?></p>
						<p class="value"><?php echo $this->ion_auth->user_name( "fullname", $order['modifier'] ) ?> <small><?php echo date( $this->assets->conf['datetime_format'], strtotime( $order['modified'] ) ); ?></small></p>
						<?php endif; ?>
					</div>
				</div>
			</div>
			<?php endif; ?>
			
		</div>
	</div>
</div>