<!-- ##################################### ADD PRODUCT MODAL ###################################### -->

<div class="modal fade d-print-none modal-default" id="modal-add-product" tabindex="-1" role="dialog">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title"><?php _e( "Add Product" ); ?></h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="<?php _e( "Close" ); ?>"><span aria-hidden="true">&times;</span></button>
			</div>
			<div class="modal-body">
				<div class="form-row mb-3">
					<div class="form-group col" id="modal_product_id_box">
						<?php 
							echo form_label( __( "Product" ), 'modal_product_id', array( 'class' => 'control-label' ) );
							echo $this->assets->conf['mandatory'];
							echo form_dropdown( $modal_product_id );
							echo form_error( 'modal_product_id' );
						?>
						<div class="text-right">
							<button type="button" id="open-new-product-modal" class="btn btn-link"><?php _e( "Add new product" ); ?></button>
						</div>
					</div>
				</div>
				<div class="form-row">
					<div class="form-group col-md-6" id="modal_unit_price_box">
						<?php 
							echo form_label( __( "Unit price" ), 'modal_unit_price', array( 'class' => 'control-label' ) );
							echo $this->assets->conf['mandatory'];
						?>
						<div class="input-group">
							<?php echo form_input( $modal_unit_price ); ?>
							<div class="input-group-append">
								<span class="input-group-text"></span>
							</div>
						</div>
						<?php echo form_error( 'modal_unit_price' ); ?>
						<input type="hidden" name="modal_currency" id="modal_currency" />
					</div>
					<div class="form-group col-md-6" id="modal_quantity_box">
						<?php 
							echo form_label( __( "Quantity" ), 'modal_quantity', array( 'class' => 'control-label' ) );
							echo $this->assets->conf['mandatory'];
						?>
						<div class="input-group">
							<?php echo form_input( $modal_quantity ); ?>
							<div class="input-group-append">
								<span class="input-group-text"></span>
							</div>
						</div>
						<?php echo form_error( 'modal_quantity' ); ?>
					</div>
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-purple btn-sm" id="confirm-add-product"><?php _e( "Add" ); ?></button>
				<button type="button" class="btn btn-secondary btn-sm" data-dismiss="modal"><?php _e( "Cancel" ); ?></button>
			</div>
		</div>
	</div>
</div>