	
<?php if ( ! empty( $orders ) ) : echo form_open( current_url() ); ?>

<div class="lists-table">
	<div class="table-responsive">

		<table class="table table-striped table-hover">
			<col width="10" class="d-print-none">
			<col width="90">
			<col width="90">
			<col width="90">
			<col width="90">
			<col width="auto">
			<col width="auto">
			<col width="140">
			<thead>
				<tr>
					<th class="d-print-none"><input type="checkbox" name="select_all_cb" class="select_all_cb" title="<?php _e( "Select All" ); ?>" /></th>
					<th><abbr title="<?php _e( "Purchase date" ); ?>"><?php _e( "Date" ); ?></abbr></th>
					<th><abbr title="<?php _e( "Delivery date" ); ?>"><?php _e( "Delivery" ); ?></abbr></th>
					<th><abbr title="<?php _e( "Due date" ); ?>"><?php _e( "Due" ); ?></abbr></th>
					<th><?php _e( "Sold" ); ?></th>
					<th><?php _e( "Client" ); ?></th>
					<th class="text-right"><?php _e( "Total amount" ); ?> <small class="text-muted">(<?php echo $this->assets->conf['currencies'][$this->assets->conf['currency']]; ?>)</small></th>
					<th class="d-print-none"></th>
				</tr>
			</thead>
			<tfoot>
				<tr>
					<th class="d-print-none"><input type="checkbox" name="select_all_cb" class="select_all_cb" title="<?php _e( "Select All" ); ?>" /></th>
					<th><abbr title="<?php _e( "Purchase date" ); ?>"><?php _e( "Date" ); ?></abbr></th>
					<th><abbr title="<?php _e( "Delivery date" ); ?>"><?php _e( "Delivery" ); ?></abbr></th>
					<th><abbr title="<?php _e( "Due date" ); ?>"><?php _e( "Due" ); ?></abbr></th>
					<th><?php _e( "Sold" ); ?></th>
					<th><?php _e( "Client" ); ?></th>
					<th class="text-right"><?php _e( "Total amount" ); ?> <small class="text-muted">(<?php echo $this->assets->conf['currencies'][$this->assets->conf['currency']]; ?>)</small></th>
					<th class="d-print-none"></th>
				</tr>
			</tfoot>
			<tbody>
				<?php 
				foreach ( $orders AS $order ) : 
					$order = $this->assets->fill_empty_vars( $order );
					$client = $this->assets->fill_empty_vars( $this->clients->get( $order['client_id'] ) );
					$buttons = $this->elements->lists_action_buttons( 'orders', 'order_id', $order['order_id'], $list_buttons_extension );
					$sold = $this->orders->is_sold( $order );
				?>
				<tr>
					<td class="d-print-none"><input type="checkbox" name="cb_ids[]" value="<?php echo $order['order_id']; ?>" /></td>
					<td><?php echo ( $order['original']['order_date'] ) ? date( $this->assets->conf['date_format'], strtotime( $order['order_date'] ) ) : $order['order_date']; ?></td>
					<td><?php echo ( $order['original']['delivery_date'] ) ? date( $this->assets->conf['date_format'], strtotime( $order['delivery_date'] ) ) : $order['delivery_date']; ?></td>
					<td><?php echo ( $order['original']['expiration_date'] ) ? date( $this->assets->conf['date_format'], strtotime( $order['expiration_date'] ) ) : $order['expiration_date']; ?></td>
					<td><span class="badge badge-<?php echo ( $sold ) ? 'success' : 'danger'; ?>"><?php echo ( $sold ) ? __( "Yes" ) : __( "No" ); ?></span></td>
					<td class="text-wrap"><?php echo anchor( 'orders/brief/' . $order['order_id'], '<strong>' . $this->clients->name( $client, 'full_backwards' ) . '</strong>', array( 'class' => 'brief', 'title' => __( "Open Brief" ), 'data-enlarge-modal' => 1 ) ); ?></td>
					<td class="text-right"><?php echo my_number_format( $order['total_amount'], $this->assets->conf['decimals'] ); ?></td>
					<td class="text-right d-print-none action-buttons">
					<?php 
					foreach ( $buttons AS $n => $b ) $buttons[$n] = ' ' . $b;
					if ( $this->uri->segment( 2 ) !== "trash" )
					{
						if ( $this->permissions->can_edit() ) echo $buttons['edit']; ?>
						<button class="btn btn-primary dropdown-toggle" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
							<i class="fas fa-cog"></i>
						</button>
						<div class="dropdown-menu dropdown-menu-right">
							<?php if ( $this->permissions->can_add() AND ! $sold ) echo $buttons['sale']; ?>
							<?php if ( $this->permissions->can_edit() AND ! $sold ) echo $buttons['mark_finished']; ?>
							<?php if ( $this->permissions->can_edit() AND $sold ) echo $buttons['mark_unfinished']; ?>
						</div>
						<?php
						if ( $this->permissions->can_trash() ) echo $buttons['trash'];
					}
					else
					{
						if ( $this->permissions->can_restore() ) echo $buttons['restore']; 
						if ( $this->permissions->can_delete() ) echo $buttons['delete'];
					}
					?>						
					</td>
				</tr>
				<?php endforeach; ?>
			</tbody>
		</table>

	</div>
</div>

<div class="row d-print-none lists-tools">

	<?php if ( count( $bulk_actions['options'] ) > 1 ) : ?>

	<div class="col form-inline">
		<div class="bulk-box">
			<?php echo form_dropdown( $bulk_actions ); ?>
			<?php echo form_submit( $submit_bulk_actions ); ?>
		</div>
	</div>

	<?php endif; if ( isset( $pagination ) ) : ?><div class="col page-navigation"><nav><?php echo $pagination; ?></nav></div><?php endif; ?>

</div>

<?php echo form_close(); else : $this->load->view( 'singles/list-empty' ); endif; ?>
