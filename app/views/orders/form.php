
<?php echo form_open( current_url(), array( 'autocomplete' => 'off', 'id' => 'form-orders' ) ); ?>

<div class="row form-elements">

	<div class="col-lg-6">
		<div class="card" id="data-card">
			<div class="card-body">
				<div class="form-row">
					<div class="form-group col">
						<?php 
							echo form_label( __( "Client" ), 'client_id', array( 'class' => 'control-label' ) );
							echo $this->assets->conf['mandatory'];
							echo form_dropdown( $client_id ); 
							echo form_error( 'client_id' );
						?>
					</div>
				</div>
				<div class="form-row">
					<div class="form-group col">
						<?php 
							echo form_label( __( "Observations" ), 'observations', array( 'class' => 'control-label' ) );
							echo form_textarea( $observations );
							echo form_error( 'observations' );
						?>
					</div>
				</div>
			</div>
		</div>
	</div>

	<div class="col-lg-6">
		<div class="card" id="dates-card">
			<div class="card-body">
				<div class="row">
					<div class="col-md-6">
						<div class="form-row">
							<div class="form-group col">
								<?php 
									echo form_label( __( "Order date" ), 'order_date', array( 'class' => 'control-label' ) );
									echo $this->assets->conf['mandatory'];
									echo form_input( $order_date );
									echo form_error( 'order_date' );
								?>
							</div>
						</div>
						<div class="form-row">
							<div class="form-group col">
								<?php 
									echo form_label( __( "Delivery date" ), 'delivery_date', array( 'class' => 'control-label' ) );
									echo form_input( $delivery_date );
									echo form_error( 'delivery_date' );
								?>
							</div>
						</div>
					</div>
					<div class="col-md-6">
						<div class="form-row">
							<div class="form-group col">
								<?php 
									echo form_label( __( "Due date" ), 'expiration_date', array( 'class' => 'control-label' ) );
									echo form_input( $expiration_date );
									echo form_error( 'expiration_date' );
								?>
							</div>
						</div>
						<div class="form-row">
							<div class="form-group col">
								<?php 
									echo form_label( __( "Seller" ), 'seller_id', array( 'class' => 'control-label' ) );
									echo form_dropdown( $seller_id );
									echo form_error( 'seller_id' );
								?>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

	<div class="col-12">
		<div class="card" id="products-card">
			<div class="card-body">
				<button type="button" class="btn btn-warning btn-sm float-right" id="add-product-button"><?php _e( "Add Product" ); ?></button>
				<h4 class="mb-0"><?php _e( "Product list" ); ?></h4>
			</div>
			<div class="table-responsive">
				<table class="table table-striped table-hover product-list">
					<col width="auto">
					<col width="160">
					<col width="160">
					<col width="160">
					<col width="160">
					<col width="60">
					<thead>
						<tr>
							<th><?php _e( "Product" ); ?></th>
							<th class="text-right"><abbr title="<?php _e( "Price per unit" ); ?>"><?php _e( "Price" ); ?></abbr> <small class="text-muted">(<?php echo $this->assets->conf['currencies'][$this->assets->conf['currency']]; ?>)</small></th>
							<th class="text-right"><?php _e( "Quantity" ); ?></th>
							<th class="text-right"><?php _e( "Remaining" ); ?></th>
							<th class="text-right"><?php _e( "Total" ); ?> <small class="text-muted">(<?php echo $this->assets->conf['currencies'][$this->assets->conf['currency']]; ?>)</small></th>
							<th></th>
						</tr>
					</thead>
					<tfoot>
						<tr>
							<th><?php _e( "Product" ); ?></th>
							<th class="text-right"><abbr title="<?php _e( "Price per unit" ); ?>"><?php _e( "Price" ); ?></abbr> <small class="text-muted">(<?php echo $this->assets->conf['currencies'][$this->assets->conf['currency']]; ?>)</small></th>
							<th class="text-right"><?php _e( "Quantity" ); ?></th>
							<th class="text-right"><?php _e( "Remaining" ); ?></th>
							<th class="text-right"><?php _e( "Total" ); ?> <small class="text-muted">(<?php echo $this->assets->conf['currencies'][$this->assets->conf['currency']]; ?>)</small></th>
							<th></th>
						</tr>
					</tfoot>
					<tbody>
						<tr>
							<td class="text-wrap"></td>
							<td class="text-right cell-price"></td>
							<td class="text-right cell-quantity"></td>
							<td class="text-right cell-remaining"></td>
							<td class="text-right cell-row-total"></td>
							<td class="text-right action-buttons">
								<button class="btn btn-danger btn-remove-product" title="<?php _e( "Remove" ); ?>">
									<i class="fa fa-times"></i>
								</button>
								<input type="hidden" name="list_product_id[]" 			class="input-product-id" />
								<input type="hidden" name="list_description[]" 			class="input-description" />
								<input type="hidden" name="list_unit_price[]" 			class="input-unit-price" />
								<input type="hidden" name="list_quantity[]" 			class="input-quantity" />
								<input type="hidden" name="list_quantity_remaining[]" 	class="input-quantity-remaining" />
								<input type="hidden" name="list_row_total[]" 			class="input-row-total" />
							</td>
						</tr>
						<?php 
						if ( $this->input->post( 'list_product_id' ) OR isset( $order_id ) ) : 
							$meta = $this->orders->prepare_meta( ( $this->input->post( 'list_product_id' ) ) ? $this->input->post() : $this->orders->meta_get( $order_id['value'] ) );
							if ( ! empty( $meta ) ) : foreach ( $meta AS $row ) : $product = $this->products->get( $row['product_id'] );
						?>
						<tr>
							<td class="text-wrap"><?php echo $row['description']; ?></td>
							<td class="text-right cell-price"><?php echo my_number_format( $row['unit_price'], $this->assets->conf['decimals'] ); ?></td>
							<td class="text-right cell-quantity"><?php printf( '%s <small class="text-muted">%s</small>', my_number_format( $row['quantity'], $this->assets->conf['qty_decimals'] ), $this->assets->conf['units'][$product['unit_measure']] ); ?></td>
							<td class="text-right cell-remaining"><?php printf( '%s <small class="text-muted">%s</small>', my_number_format( $row['quantity_remaining'], $this->assets->conf['qty_decimals'] ), $this->assets->conf['units'][$product['unit_measure']] ); ?></td>
							<td class="text-right cell-row-total"><?php echo my_number_format( $row['row_total'], $this->assets->conf['decimals'] ); ?></td>
							<td class="text-right action-buttons">
								<button class="btn btn-danger btn-remove-product" title="<?php _e( "Remove" ); ?>">
									<i class="fa fa-times"></i>
								</button>
								<input type="hidden" name="list_product_id[]" 			value="<?php echo $row['product_id']; ?>" 			class="input-product-id" />
								<input type="hidden" name="list_description[]" 			value="<?php echo $row['description']; ?>" 			class="input-description" />
								<input type="hidden" name="list_unit_price[]" 			value="<?php echo $row['unit_price']; ?>" 			class="input-unit-price" />
								<input type="hidden" name="list_quantity[]" 			value="<?php echo $row['quantity']; ?>" 			class="input-quantity" />
								<input type="hidden" name="list_quantity_remaining[]" 	value="<?php echo $row['quantity_remaining']; ?>" 	class="input-quantity-remaining" />
								<input type="hidden" name="list_row_total[]" 			value="<?php echo $row['row_total']; ?>" 			class="input-row-total" />
							</td>
						</tr>
						<?php endforeach ; endif; endif; ?>

						<tr class="table-warning total-amount-row">
							<td colspan="5" class="text-right">
								<div class="d-flex justify-content-end align-items-center">
									<div class="mr-3"><strong><?php _e( "Total amount" ); ?></strong> <span class="mandatory">*</span></div>
									<div>
										<div class="input-group input-group-lg">
											<?php echo form_input( $total_amount ); ?>
											<div class="input-group-append">
												<span class="input-group-text"><?php echo $this->assets->conf['currencies'][$this->assets->conf['currency']]; ?></span>
											</div>
										</div>
									</div>
								</div>
								<?php echo form_error( 'total_amount' ); ?>
							</td>
							<td></td>
						</tr>
					</tbody>
				</table>
			</div>
		</div>
	</div>

	<div class="col-12">
		<div class="card" id="files-card">
			<div class="card-body">
				<?php $this->load->view( 'files/model-multiple-files', ( isset( $order_id ) ) ? $this->files->get_files_src( $order_id['value'], 'orders', FALSE ) : NULL ); ?>
			</div>
		</div>
	</div>

</div>

<div class="buttons-set">
	<button type="submit" class="btn btn-primary">
		<i class="fa fa-save"></i>
		<?php _e( "Save" ); ?>
	</button>
	<?php if ( $this->uri->segment( 2 ) == "edit" ) echo anchor( 'orders/add', '<i class="fa fa-plus-circle"></i> ' . __( "New" ), array( 'class' => 'btn btn-secondary', 'title' => __( "Add New Order" ) ) ); ?>
</div>

<?php if ( ! empty( $order_id ) ) echo form_input( $order_id ); ?>

<?php echo form_close(); ?>
<?php $this->load->view( 'orders/modal-add-product' ); ?>
<?php $this->load->view( 'singles/modal-edit-magnitude' ); ?>
<?php $this->load->view( 'singles/modal-edit-remaining' ); ?>
<?php $this->load->view( 'singles/modal-add-new-product' ); ?>