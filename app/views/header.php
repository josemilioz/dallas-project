<?php defined( 'BASEPATH' ) OR exit( 'No direct script access allowed' ); ?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="<?php echo $this->config->item( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no">
	<title><?php $this->templates->page_title( $this->data ); ?></title>
	<meta name="apple-mobile-web-app-title" content="<?php echo $this->assets->conf['company_name']; ?>" />
	<meta name="application-name" content="<?php echo $this->assets->conf['company_name']; ?>" />

	<?php $this->assets->print_styles(); ?>

</head>
<body class="<?php echo $this->elements->body_class(); ?>">

	<nav id="topbar" class="navbar fixed-top navbar-expand-lg navbar-dark bg-dark">
		<a class="navbar-brand" href="<?php echo site_url(); ?>"><?php echo $this->assets->conf['company_name']; ?></a>

		<button class="navbar-toggler ml-auto collapsed" type="button" data-toggle="collapse" data-target="#full-menu" aria-controls="full-menu" aria-expanded="false" aria-label="<?php _e( "Toggle navigation" ); ?>">
			<i class="fas fa-bars"></i>
			<i class="fas fa-times"></i>
		</button>

		<div class="collapse navbar-collapse" id="full-menu">

			<ul class="navbar-nav flex-md-column" id="controller-menu">
				<li class="nav-item<?php $this->assets->active_menu_link( 'dashboard' ); ?>">
					<a class="nav-link" href="<?php echo site_url( '/' ); ?>">
						<i class="fas fa-tachometer-alt"></i>
						<span class="menu-text-label"><?php _e( "Dashboard" ); ?></span>
					</a>
				</li>
				<li class="nav-item dropdown<?php $this->assets->active_menu_link( 'clients' ); ?>">
					<a class="nav-link dropdown-toggle" href="#" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
						<i class="fas fa-user-tie"></i>
						<span class="menu-text-label"><?php _e( "Clients" ); ?></span>
					</a>
					<div class="dropdown-menu">
						<h4><?php _e( "Clients" ); ?></h4>
						<?php echo anchor( 'clients/add', __( "Add" ), array( 'class' => 'dropdown-item' ) ); ?>
						<?php echo anchor( 'clients/lists', __( "List" ), array( 'class' => 'dropdown-item' ) ); ?>
						<div class="dropdown-divider"></div>
						<?php echo anchor( 'clients/trash', __( "Trash" ), array( 'class' => 'dropdown-item' ) ); ?>
					</div>
				</li>
				<li class="nav-item dropdown<?php $this->assets->active_menu_link( array( 'products', 'categories' ) ); ?>">
					<a class="nav-link dropdown-toggle" href="#" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
						<i class="fas fa-box-open"></i>
						<span class="menu-text-label"><?php _e( "Products" ); ?></span>
					</a>
					<div class="dropdown-menu">
						<h4><?php _e( "Products" ); ?></h4>
						<?php echo anchor( 'products/add', __( "Add" ), array( 'class' => 'dropdown-item' ) ); ?>
						<?php echo anchor( 'products/lists', __( "List" ), array( 'class' => 'dropdown-item' ) ); ?>
						<div class="dropdown-divider"></div>
						<?php echo anchor( 'products/trash', __( "Trash" ), array( 'class' => 'dropdown-item' ) ); ?>
						<h5><?php _e( "Categories" ); ?></h5>
						<?php echo anchor( 'categories/add', __( "Add" ), array( 'class' => 'dropdown-item' ) ); ?>
						<?php echo anchor( 'categories/lists', __( "List" ), array( 'class' => 'dropdown-item' ) ); ?>
						<div class="dropdown-divider"></div>
						<?php echo anchor( 'categories/trash', __( "Trash" ), array( 'class' => 'dropdown-item' ) ); ?>
					</div>
				</li>
				<li class="nav-item dropdown<?php $this->assets->active_menu_link( 'purchases' ); ?>">
					<a class="nav-link dropdown-toggle" href="#" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
						<i class="fas fa-shopping-cart"></i>
						<span class="menu-text-label"><?php _e( "Purchases" ); ?></span>
					</a>
					<div class="dropdown-menu">
						<h4><?php _e( "Purchases" ); ?></h4>
						<?php echo anchor( 'purchases/add', __( "Add" ), array( 'class' => 'dropdown-item' ) ); ?>
						<?php echo anchor( 'purchases/lists', __( "List" ), array( 'class' => 'dropdown-item' ) ); ?>
						<div class="dropdown-divider"></div>
						<?php echo anchor( 'purchases/trash', __( "Trash" ), array( 'class' => 'dropdown-item' ) ); ?>
					</div>
				</li>
				<li class="nav-item dropdown<?php $this->assets->active_menu_link( 'stock' ); ?>">
					<a class="nav-link dropdown-toggle" href="#" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
						<i class="fas fa-truck"></i>
						<span class="menu-text-label"><?php _e( "Stock" ); ?></span>
					</a>
					<div class="dropdown-menu">
						<h4><?php _e( "Stock" ); ?></h4>
						<?php echo anchor( 'stock/self', __( "Add Self-Production" ), array( 'class' => 'dropdown-item' ) ); ?>
						<?php echo anchor( 'stock/lists', __( "Inventory" ), array( 'class' => 'dropdown-item' ) ); ?>
						<div class="dropdown-divider"></div>
						<?php echo anchor( 'stock/trash', __( "Trash" ), array( 'class' => 'dropdown-item' ) ); ?>
					</div>
				</li>
				<li class="nav-item dropdown<?php $this->assets->active_menu_link( 'orders' ); ?>">
					<a class="nav-link dropdown-toggle" href="#" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
						<i class="fas fa-folder-open"></i>
						<span class="menu-text-label"><?php _e( "Orders" ); ?></span>
					</a>
					<div class="dropdown-menu">
						<h4><?php _e( "Orders" ); ?></h4>
						<?php echo anchor( 'orders/add', __( "Add" ), array( 'class' => 'dropdown-item' ) ); ?>
						<?php echo anchor( 'orders/lists', __( "List" ), array( 'class' => 'dropdown-item' ) ); ?>
						<div class="dropdown-divider"></div>
						<?php echo anchor( 'orders/trash', __( "Trash" ), array( 'class' => 'dropdown-item' ) ); ?>
					</div>
				</li>
				<li class="nav-item dropdown<?php $this->assets->active_menu_link( 'movements' ); ?>">
					<a class="nav-link dropdown-toggle" href="#" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
						<i class="fas fa-tasks"></i>
						<span class="menu-text-label"><?php _e( "Movements" ); ?></span>
					</a>
					<div class="dropdown-menu">
						<h4><?php _e( "Movements" ); ?></h4>
						<?php echo anchor( 'movements/add', __( "Add" ), array( 'class' => 'dropdown-item' ) ); ?>
						<?php echo anchor( 'movements/lists', __( "List" ), array( 'class' => 'dropdown-item' ) ); ?>
						<div class="dropdown-divider"></div>
						<?php echo anchor( 'movements/trash', __( "Trash" ), array( 'class' => 'dropdown-item' ) ); ?>
					</div>
				</li>
				<li class="nav-item dropdown<?php $this->assets->active_menu_link( 'sales' ); ?>">
					<a class="nav-link dropdown-toggle" href="#" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
						<i class="fas fa-dollar-sign"></i>
						<span class="menu-text-label"><?php _e( "Sales" ); ?></span>
					</a>
					<div class="dropdown-menu">
						<h4><?php _e( "Sales" ); ?></h4>
						<?php echo anchor( 'sales/add', __( "Add" ), array( 'class' => 'dropdown-item' ) ); ?>
						<div class="dropdown-divider"></div>
						<?php echo anchor( 'sales/lists?status=billed', __( "Billed" ), array( 'class' => 'dropdown-item' ) ); ?>
						<?php echo anchor( 'sales/lists?status=unbilled', __( "Unbilled" ), array( 'class' => 'dropdown-item' ) ); ?>
						<?php echo anchor( 'sales/lists?status=unpaid', __( "Billed Unpaid" ), array( 'class' => 'dropdown-item' ) ); ?>
						<?php echo anchor( 'sales/combine', __( "Combine Unbilled" ), array( 'class' => 'dropdown-item' ) ); ?>
						<div class="dropdown-divider"></div>
						<?php echo anchor( 'sales/trash', __( "Trash" ), array( 'class' => 'dropdown-item' ) ); ?>
					</div>
				</li>
				<li class="nav-item dropdown<?php $this->assets->active_menu_link( 'reports' ); ?>">
					<a class="nav-link" href="<?php echo site_url( 'reports' ); ?>">
						<i class="fas fa-chart-pie"></i>
						<span class="menu-text-label"><?php _e( "Reports" ); ?></span>
					</a>
				</li>
				<li class="nav-item dropdown<?php $this->assets->active_menu_link( 'search' ); ?>">
					<a class="nav-link" href="<?php echo site_url( 'search' ); ?>">
						<i class="fas fa-search"></i>
						<span class="menu-text-label"><?php _e( "Search" ); ?></span>
					</a>
				</li>
			</ul>
			<ul class="navbar-nav ml-auto" id="user-menu">
				<?php if ( $this->ion_auth->is_admin() ) : ?>
				<li class="nav-item dropdown<?php $this->assets->active_menu_link( 'users' ); ?>">
					<a class="nav-link dropdown-toggle" href="#" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
						<i class="fas fa-users"></i>
						<span class="menu-text-label"><?php _e( "Users" ); ?></span>
					</a>
					<div class="dropdown-menu">
						<?php echo anchor( 'users/add', __( "Add" ), array( 'class' => 'dropdown-item' ) ); ?>
						<?php echo anchor( 'users/lists', __( "List" ), array( 'class' => 'dropdown-item' ) ); ?>
						<div class="dropdown-divider"></div>
						<?php echo anchor( 'users/trash', __( "Trash" ), array( 'class' => 'dropdown-item' ) ); ?>
						<div class="dropdown-divider"></div>
						<?php echo anchor( 'users/log', __( "Activity Log" ), array( 'class' => 'dropdown-item' ) ); ?>
					</div>
				</li>				
				<li class="nav-item dropdown<?php $this->assets->active_menu_link( array( 'settings', 'pos', 'print_orders' ) ); ?>">
					<a class="nav-link dropdown-toggle" href="#" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
						<i class="fas fa-cog"></i>
						<span class="menu-text-label"><?php _e( "Settings" ); ?></span>
					</a>
					<div class="dropdown-menu">
						<?php echo anchor( 'settings/main', __( "Main Settings" ), array( 'class' => 'dropdown-item' ) ); ?>
						<?php echo anchor( 'settings/exchange', __( "Currency Exchange" ), array( 'class' => 'dropdown-item' ) ); ?>
						<div class="dropdown-divider"></div>
						<?php echo anchor( 'pos', __( "Points of Sale" ), array( 'class' => 'dropdown-item' ) ); ?>
						<?php echo anchor( 'print_orders', __( "Print Orders" ), array( 'class' => 'dropdown-item' ) ); ?>
					</div>
				</li>
				<?php endif; ?>
				<li class="nav-item dropdown<?php $this->assets->active_menu_link( 'profile' ); ?>">
					<a class="nav-link dropdown-toggle" href="#" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
						<i class="fas fa-user"></i>
						<span class="menu-text-label"><?php echo $this->ion_auth->user_name( 'firstname_initial' ); ?></span>
					</a>
					<div class="dropdown-menu dropdown-menu-right" id="dropdown-profile">
						<div class="d-none d-lg-block">
							<div class="username">
								<div class="col-4">
									<img class="rounded-avatar" src="<?php echo $this->ion_auth->avatar(); ?>" alt="<?php echo $this->ion_auth->user_name( 'fullname' ); ?>">
								</div>
								<div class="col-8">
									<span class="user-fullname"><?php echo $this->ion_auth->user_name( 'fullname' ); ?></span>
									<span class="user-role"><?php echo $this->ion_auth->current_user_role(); ?></span>
								</div>
							</div>
							<div class="dropdown-divider"></div>
						</div>
						<?php echo anchor( 'profile', __( "My Profile" ), array( 'class' => 'dropdown-item' ) ); ?>
						<?php echo anchor( 'change_password', __( "Change Password" ), array( 'class' => 'dropdown-item' ) ); ?>
						
						<?php
						if ( $this->ion_auth->is_superadmin() ) :
							$referer = ( ! empty( uri_string() ) ) ? '?referer=' . uri_string() : NULL;
						?>
						<div class="dropdown-divider"></div>
						<?php echo ( $this->permissions->is_system_locked() ) ? anchor( "settings/system_unlock$referer", __( "Unlock System" ), array( 'class' => 'dropdown-item' ) ) : anchor( "settings/system_lock$referer", __( "Lock System" ), array( 'class' => 'dropdown-item' ) ); ?>
						<?php echo anchor( "settings/backup_database$referer", __( "Backup Database" ), array( 'class' => 'dropdown-item' ) ); ?>
						<?php endif; ?>
					</div>
				</li>
				<li class="nav-item<?php $this->assets->active_menu_link( 'logout' ); ?>" id="logout">
					<a class="nav-link" href="<?php echo site_url( 'logout' ); ?>">
						<i class="fas fa-ban"></i>
						<span class="menu-text-label"><?php _e( "Logout" ); ?></span>
					</a>
				</li>
			</ul>
			
		</div>
	</nav>

	<div id="main-content-wrapper">
		<div class="container">

			<div class="row">
				<div class="col" id="main-screen-titles">
					<?php if ( isset( $this->data['tags']['controller_name'] ) ) : ?><h4 class="main-controller-name"><?php echo $this->data['tags']['controller_name']; ?></h4><?php endif; ?>
					<?php if ( isset( $this->data['tags']['page_title'] ) ) : ?><h1 class="main-page-title"><?php echo $this->data['tags']['page_title']; ?></h1><?php endif; ?>
					<?php if ( isset( $this->data['tags']['extra'] ) ) : ?><div class="title-tools"><?php echo $this->data['tags']['extra']; ?></div><?php endif; ?>
					<?php if ( isset( $this->data['tags']['welcome_title'] ) AND isset( $_GET['welcome'] ) ) : ?><h1 class="welcome-title"><span><?php echo $this->data['tags']['welcome_title']; ?></span></h1><?php endif; ?>
				</div>
			</div>
		


<!-- #################################### PAGE OUTPUT BEGINS #################################### -->
		