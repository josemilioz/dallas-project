
<?php echo form_open( current_url(), array( 'autocomplete' => 'off', 'id' => 'form-products' ) ); ?>

<div class="row form-elements">

	<div class="col-lg-8">
		<div class="card" id="brand-card">
			<div class="card-body">
				<div class="form-row">
					<div class="form-group col">
						<?php 
							echo form_label( __( "Name" ), 'name', array( 'class' => 'control-label' ) );
							echo $this->assets->conf['mandatory'];
							echo form_input( $name );
							echo form_error( 'name' );
						?>
					</div>
				</div>
				<div class="form-row">
					<div class="form-group col-md-5 col-lg-4">
						<?php 
							echo form_label( __( "Category" ), 'category_id', array( 'class' => 'control-label' ) );
							echo $this->assets->conf['mandatory'];
							echo form_dropdown( $category_id );
							echo form_error( 'category_id' );
						?>
					</div>
					<div class="form-group col-md-7 col-lg-8">
						<?php 
							echo form_label( __( "Description" ), 'description', array( 'class' => 'control-label' ) );
							echo form_textarea( $description );
							echo form_error( 'description' );
						?>
					</div>
				</div>
			</div>
		</div>

		<div class="row">
			<div class="col-lg-6">
				<div class="card" id="payment-card">
					<div class="card-body">
						<h4><?php _e( "Payment" ); ?></h4>
						<div class="form-row">
							<div class="form-group col-md">
								<?php 
									echo form_label( __( "Currency" ), 'currency', array( 'class' => 'control-label' ) );
									echo $this->assets->conf['mandatory'];
									echo form_dropdown( $currency );
									echo form_error( 'currency' );
								?>
							</div>
						</div>
						<div class="form-row">
							<div class="form-group col-md-6">
								<?php 
									echo form_label( __( "Price" ), 'price', array( 'class' => 'control-label' ) );
									echo $this->assets->conf['mandatory'];
									echo form_input( $price );
									echo form_error( 'price' );
								?>
							</div>
							<div class="form-group col-md-6">
								<?php 
									echo form_label( __( "Tax" ), 'tax', array( 'class' => 'control-label' ) );
									echo $this->assets->conf['mandatory'];
									echo form_dropdown( $tax );
									echo form_error( 'tax' );
								?>
							</div>
						</div>
					</div>
				</div>				
			</div>
			<div class="col-lg-6">
				<div class="card" id="measurement-card">
					<div class="card-body">
						<h4><?php _e( "Measurement" ); ?></h4>
						<div class="form-row">
							<div class="form-group col-md-6">
								<?php 
									echo form_label( __( "Unit measure" ), 'unit_measure', array( 'class' => 'control-label' ) );
									echo $this->assets->conf['mandatory'];
									echo form_dropdown( $unit_measure );
									echo form_error( 'unit_measure' );
								?>
							</div>
							<div class="form-group col-md-6">
								<?php 
									echo form_label( __( "Unit quantity" ), 'unit_quantity', array( 'class' => 'control-label' ) );
									echo $this->assets->conf['mandatory'];
									echo form_input( $unit_quantity );
									echo form_error( 'unit_quantity' );
								?>
							</div>
						</div>
						<div class="form-row">
							<div class="form-group col-md-6">
								<?php echo form_label( __( "Stock warning" ), 'min_stock_warning', array( 'class' => 'control-label', 'title' => __( "Minimum stock warning" ) ) ); ?>
								<div class="input-group">
									<?php echo form_input( $min_stock_warning ); ?>
									<div class="input-group-append">
										<span class="input-group-text"><?php if ( isset( $product_id ) ) echo $this->assets->conf['units'][$unit_measure['selected']]; ?></span>
									</div>
								</div>
								<?php echo form_error( 'min_stock_warning' ); ?>
							</div>
							<div class="form-group col-md-6">
								<?php echo form_label( __( "Stock danger" ), 'min_stock_danger', array( 'class' => 'control-label', 'title' => __( "Minimum stock danger" ) ) ); ?>
								<div class="input-group">
									<?php echo form_input( $min_stock_danger ); ?>
									<div class="input-group-append">
										<span class="input-group-text"><?php if ( isset( $product_id ) ) echo $this->assets->conf['units'][$unit_measure['selected']]; ?></span>
									</div>
								</div>
								<?php echo form_error( 'min_stock_danger' ); ?>
							</div>
						</div>
					</div>
				</div>				
			</div>
		</div>
	</div>

	<div class="col-lg-4">
		<div class="card" id="picture-card">
			<div class="card-body">
				<h4><?php _e( "Pictures" ); ?></h4>
				<?php $this->load->view( 'files/model-multiple-picture', ( isset( $product_id ) ) ? $this->files->get_files_src( $product_id['value'], 'products', FALSE ) : NULL ); ?>
			</div>
		</div>

		<div class="card">
			<div class="card-body">
				<h4><?php _e( "Free sale" ); ?></h4>
				<div class="form-row">
					<div class="form-group col">
						<div class="form-check">
							<?php echo form_input( $free ); ?>
							<?php echo form_label( __( "Enable this product on POS without being stocked" ), 'free', array( 'class' => 'form-check-label' ) ); ?>
						</div>
						<?php echo form_error( 'free' ); ?>
					</div>
				</div>
			</div>
		</div>
	</div>

</div>

<div class="buttons-set">
	<button type="submit" class="btn btn-primary">
		<i class="fa fa-save"></i>
		<?php _e( "Save" ); ?>
	</button>
	<?php if ( $this->uri->segment( 2 ) == "edit" ) echo anchor( 'clients/add', '<i class="fa fa-plus-circle"></i> ' . __( "New" ), array( 'class' => 'btn btn-secondary', 'title' => __( "Add New Product" ) ) ); ?>
</div>

<?php if ( ! empty( $product_id ) ) echo form_input( $product_id ); ?>
<?php echo form_close(); ?>