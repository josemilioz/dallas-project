
<div class="card">
	<div class="card-body">
		<div id="brief-wrapper">

			<div class="row">
				<div class="col-lg-5 mb-3">
					<div class="picture-big-container w-100 text-center">
						<img class="picture-big w-75" src="<?php echo $this->products->picture( $product['product_id'] ); ?>" />
					</div>
				</div>
				<div class="col-lg-7 mb-3">
					<div class="element-group row">
						<div class="col-4"><span class="item-label"><?php _e( "Name" ); ?></span></div>
						<div class="col-8"><span class="full-name"><?php echo $product['name']; ?></span></div>
					</div>
					<div class="element-group row">
						<div class="col-4"><span class="item-label"><?php _e( "Category" ); ?></span></div>
						<div class="col-8"><span class="text-info"><?php echo $category['name']; ?></span></div>
					</div>
					<div class="element-group row">
						<div class="col-4"><span class="item-label"><?php _e( "Description" ); ?></span></div>
						<div class="col-8"><span><?php echo nl2br( $product['description'] ); ?></span></div>
					</div>
					<div class="element-group row">
						<div class="col-4"><span class="item-label"><?php _e( "Currency" ); ?></span></div>
						<div class="col-8"><span><?php echo ( ! empty( $product['original']['currency'] ) ) ? $this->assets->conf['currencies_ext'][$product['currency']] : $product['currency']; ?></span></div>
					</div>
					<div class="element-group row">
						<div class="col-4"><span class="item-label"><?php _e( "Price" ); ?></span></div>
						<div class="col-8"><span><?php echo ( ! empty( $product['original']['price'] ) ) ? my_number_format( $product['price'], $this->assets->conf['decimals'] ) : $product['price']; ?></span></div>
					</div>
					<div class="element-group row">
						<div class="col-4"><span class="item-label"><?php _e( "Tax" ); ?></span></div>
						<div class="col-8"><span><?php echo ( ! empty( $product['original']['tax'] ) ) ? $this->assets->conf['bill_columns'][$product['tax']] : $product['tax']; ?></span></div>
					</div>
					<div class="element-group row">
						<div class="col-4"><span class="item-label"><?php _e( "Unit measure" ); ?></span></div>
						<div class="col-8"><span><?php echo ( ! empty( $product['original']['unit_measure'] ) ) ? $this->assets->conf['units'][$product['unit_measure']] : $product['unit_measure']; ?></span></div>
					</div>
					<div class="element-group row">
						<div class="col-4"><span class="item-label"><?php _e( "Unit quantity" ); ?></span></div>
						<div class="col-8"><span><?php echo ( ! empty( $product['original']['unit_quantity'] ) ) ? my_number_format( $product['unit_quantity'], $this->assets->conf['qty_decimals'] ) : $product['unit_quantity']; ?></span></div>
					</div>
					<div class="element-group row">
						<div class="col-4"><span class="item-label"><?php _e( "Free sale without stock" ); ?></span></div>
						<div class="col-8"><span><?php echo ( ! empty( $product['original']['free'] ) ) ? __( "Yes" ) : __( "No" ); ?></span></div>
					</div>
				</div>
			</div>

			<?php if ( ! empty( $product['created'] ) OR ! empty( $product['modified'] ) ) : ?>
			<div class="row last-activity">
				<div class="col">
					<div class="created">
						<?php if ( ! empty( $product['created'] ) ) : ?>
						<p class="label"><?php _e( "Created by" ); ?></p>
						<p class="value"><?php echo $this->ion_auth->user_name( "fullname", $product['creator'] ) ?> <small><?php echo date( $this->assets->conf['datetime_format'], strtotime( $product['created'] ) ); ?></small></p>
						<?php endif; ?>
					</div>
				</div>
				<div class="col">
					<div class="modified">
						<?php if ( ! empty( $product['modified'] ) ) : ?>
						<p class="label"><?php _e( "Last modified by" ); ?></p>
						<p class="value"><?php echo $this->ion_auth->user_name( "fullname", $product['modifier'] ) ?> <small><?php echo date( $this->assets->conf['datetime_format'], strtotime( $product['modified'] ) ); ?></small></p>
						<?php endif; ?>
					</div>
				</div>
			</div>
			<?php endif; ?>
			
		</div>
	</div>
</div>