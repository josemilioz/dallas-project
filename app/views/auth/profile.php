
<?php echo form_open_multipart( current_url(), array( 'autocomplete' => 'off', 'id' => 'form-profile' ) ); ?>

<div class="row form-elements">

	<div class="col-lg-4">
		<div class="card" id="picture-card">
			<div class="card-body">
				<h4><?php _e( "Picture" ); ?></h4>
				<?php $this->load->view( 'files/model-single-picture', $this->files->get_files_src( $this->ion_auth->user()->row()->id, 'users' ) ); ?>
			</div>
		</div>
	</div>

	<div class="col-lg-8">
		<div class="card" id="personal-card">
			<div class="card-body">
				<h4><?php _e( "Personal Information" ); ?></h4>
				<div class="form-row">
					<div class="form-group col-md">
						<?php 
							echo form_label( __( "First Name" ), 'first_name', array( 'class' => 'control-label' ) );
							echo $this->assets->conf['mandatory'];
							echo form_input( $first_name );
							echo form_error( 'first_name' );
						?>
					</div>
					<div class="form-group col-md">
						<?php
							echo form_label( __( "Last Name" ), 'last_name', array( 'class' => 'control-label' ) );
							echo $this->assets->conf['mandatory'];
							echo form_input( $last_name );
							echo form_error( 'last_name' );
						?>
					</div>
				</div>
				<div class="form-row">
					<div class="form-group col-md-6">
						<?php
							echo form_label( __( "Language" ), 'language', array( 'class' => 'control-label' ) );
							echo $this->assets->conf['mandatory'];
							echo form_dropdown( $language );
							echo form_error( 'language' );
						?>
					</div>
				</div>
			</div>
		</div>
		<div class="card" id="contact-card">
			<div class="card-body">
				<h4><?php _e( "Contact Information" ); ?></h4>
				<div class="form-row">
					<div class="form-group col-md">
						<?php
							echo form_label( __( "Phone" ), 'phone', array( 'class' => 'control-label' ) );
							echo form_input( $phone );
							echo form_error( 'phone' );
						?>
					</div>
					<div class="form-group col-md">
						<?php
							echo form_label( __( "Email" ), 'email', array( 'class' => 'control-label' ) );
							echo $this->assets->conf['mandatory'];
							echo form_input( $email );
							echo form_error( 'email' );
						?>
					</div>
				</div>				
			</div>
		</div>
	</div>

</div>

<div class="buttons-set">
	<button type="submit" class="btn btn-primary">
		<i class="fa fa-save"></i>
		<?php _e( "Save" ); ?>
	</button>
</div>

<?php echo form_input( $user_id ); echo form_close(); ?>