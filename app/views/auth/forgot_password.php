
<h2><?php _e( "Forgot my password" ); ?></h2>

<p class="mt-3"><?php printf( __( "Please, complete your %s so we can send you an e-mail to restore your password." ), $identity_label ); ?></p>

<?php echo form_open( current_url(), array( 'autocomplete' => 'off', 'id' => 'form-forgot-password' ) ); ?>

<p>
	<label for="identity" class="control-label"><?php echo $identity_label; ?></label>
	<?php echo form_input( $identity ); ?>
</p>

<div class="d-flex justify-content-between align-items-center">
	<div class="column flex-fill">
		<a href="<?php echo base_url( 'login' ); ?>"><?php _e( "Return to Login screen" ); ?></a>		
	</div>
	<div class="column flex-fill">
		<?php echo form_submit( $submit ); ?>
	</div>
</div>

<?php echo form_close(); ?>
