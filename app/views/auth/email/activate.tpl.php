<!DOCTYPE html>
<html>
<head>
	<title><?php _e( "Activate User" ); ?></title>
	<style type="text/css">
	
	body {
		background: #CCC;
		font-family: "Helvetica Neue", Helvetica, Arial, sans-serif;
	}

	.company-name {
		text-align: center;
		font-size: 14pt;
		font-weight: bold;
		color: #777777;
		margin-top: 40px;
	}

	.mail-container {
		margin-top: 20px;
		margin-bottom: 20px;
		box-shadow: 0 2px 2px #999;
	}

	.mail-container, .meta-data {
		width: 100%;
		max-width: 600px;		
		margin-left: auto;
		margin-right: auto;
		border: 0;
	}

	.mail-container p {
		line-height: 1.5;
	}

	a {
		color: #7730DB;
	}

	a:hover {
		color: #2A3DBB;
	}

	code {
		font-size: 13pt;
		font-weight: bold;
		display: inline-block;
		background: #DDD;
		padding-left: 5px;
		padding-right: 5px;
	}

	.first-head {
		background: #7730DB;
		color: #FFFFFF;
		padding-top: 20px;
		padding-bottom: 20px;
		padding-right: 20px;
		padding-left: 20px;
		text-align: center;
	}

	.second-head {
		background: #BF4EDB;
		color: #FFFFFF;
		padding-top: 20px;
		padding-bottom: 20px;
		padding-right: 20px;
		padding-left: 20px;
		text-align: center;
	}

	h3 {
		font-size: 17pt;
	}

	h4 {
		font-size: 15pt;
	}

	.first-head h3, .second-head h4 {
		margin: 0;
		font-weight: 200;
	}

	.second-head strong {
		color: #FFFFAA;
	}

	.message {
		background: #FFFFFF;
		padding-top: 20px;
		padding-bottom: 20px;
		padding-right: 20px;
		padding-left: 20px;
		font-size: 12pt;
	}

	.meta-data td {
		text-align: center;
		font-size: 10pt;
	}

	</style>
</head>
<body>

	<p class="company-name"><?php echo $this->assets->conf['company_name']; ?></p>

	<table class="mail-container" cellspacing="0" cellpadding="0">
		<tr>
			<td class="first-head"><h3><?php _e( "User Activation" ); ?></h3></td>
		</tr>
		<tr>
			<td class="second-head"><h4><?php printf( __( "Activate account for %s" ), '<strong>' . $identity . '</strong>' ); ?></h4></td>
		</tr>
		<tr>
			<td class="message">
				<p><?php printf( __( "Please, click the following link to %s. You'll be redirected to a webpage and get your account activated." ), anchor( 'activate/'. $id .'/'. $activation, __( "activate your account" ) ) ); ?></p>
			</td>
		</tr>
	</table>

	<table class="meta-data">
		<tr>
			<td>
				<p><?php printf( __( "Sent automatically by %s" ), "<em>" . $this->assets->conf['product_name'] . "</em>" ); ?></p>
				<p><?php echo anchor( site_url() ); ?></p>
			</td>
		</tr>
	</table>

</body>
</html>