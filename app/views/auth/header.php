<?php defined( 'BASEPATH' ) OR exit( 'No direct script access allowed' ); ?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="<?php echo $this->config->item( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no">
	<title><?php $this->templates->page_title( $this->data ); ?></title>
	<meta name="apple-mobile-web-app-title" content="<?php echo $this->assets->conf['company_name']; ?>" />
	<meta name="application-name" content="<?php echo $this->assets->conf['company_name']; ?>" />

	<?php $this->assets->print_styles(); ?>

</head>
<body class="<?php echo $this->elements->body_class( 'login-screens bg-' . rand( 1, 3 ) ); ?>">
	
	<div class="container-fluid d-flex align-items-center">
		<div class="row-fluid flex-fill">
			<h2 class="login-logo"><?php echo $this->assets->conf['company_name']; ?></h2>
			<div class="login-box">
