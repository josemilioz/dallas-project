
<h2><?php _e( "Reset Password" ); ?></h2>

<?php echo form_open( 'auth/reset_password/' . $code, array( 'autocomplete' => 'off', 'id' => 'form-reset-password' ) ); ?>

<p class="text-left">
	<label for="new" class="control-label"><?php printf( __( "New Password <small>(at least %s chars)</small>" ), $min_password_length ); ?></label>
	<?php echo form_input( $new_password ); ?>
</p>

<p class="text-left">
	<label for="new_confirm" class="control-label"><?php _e( "Confirm New Password" ); ?></label>
	<?php echo form_input( $new_password_confirm ); ?>
</p>

<p class="mb-0 mt-4 text-center"><?php echo form_submit( $submit ); ?></p>

<?php echo form_input( $user_id ); ?>
<?php echo form_hidden( $csrf );  ?>
<?php echo form_close(); ?>
