
<h2><?php printf( __( "Change password for %s" ), '<strong>' . $this->ion_auth->user_name( 'username' ) . '</strong>' ); ?></h2>

<?php echo form_open( current_url(), array( 'autocomplete' => 'off', 'id' => 'form-change-password' ) ); ?>

<p class="text-left">
	<label for="old" class="control-label"><?php _e( "Old Password" ); ?></label>
	<?php echo form_input( $old_password ); ?>
</p>

<p class="text-left">
	<label for="new" class="control-label"><?php printf( __( "New Password <small>(at least %s chars)</small>" ), $min_password_length ); ?></label>
	<?php echo form_input( $new_password ); ?>
</p>

<p class="text-left">
	<label for="new_confirm" class="control-label"><?php _e( "Confirm New Password" ); ?></label>
	<?php echo form_input( $new_password_confirm ); ?>
</p>

<p class="text-center"><?php echo form_submit( $submit ); ?></p>

<?php echo form_input( $user_id ); ?>
<?php echo form_close(); ?>

<p class="mb-0 text-center"><a href="<?php echo base_url(); ?>"><?php _e( "Cancel" ); ?></a></p>
