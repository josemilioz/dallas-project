<?php defined( 'BASEPATH' ) OR exit( 'No direct script access allowed' ); ?>

			</div>
			<p class="login-copyright"><?php printf( __( '&copy; %1$s %2$s' ), date( "Y" ), anchor( "http://ombu.co/", "Ombú Internet" ) ); ?></p>
		</div>
	</div>

	<?php $this->alerts->output_alert(); ?>

	<div class="bg"></div>

	<script type="text/javascript">
		/*<![CDATA[*/

		// Global System Vars
		var conf = {
			site_url: '<?php echo site_url(); ?>',
			decimals: <?php echo $this->assets->conf['decimals']; ?>,
			date_format: '<?php echo $this->assets->conf['date_format_js']; ?>',
			isAndroid: <?php echo ( $this->mobile_detect->isAndroidOS() ) ? 'true' : 'false'; ?>,
			isiOS: <?php echo ( $this->mobile_detect->isIOS() ) ? 'true' : 'false'; ?>,
			isTablet: <?php echo ( $this->mobile_detect->isTablet() ) ? 'true' : 'false'; ?>,
			isMobile: <?php echo ( $this->mobile_detect->isMobile() ) ? 'true' : 'false'; ?>,
		};

		/*]]>*/
	</script>
		
	<?php $this->assets->print_scripts(); ?>
</body>
</html>