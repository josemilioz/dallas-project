
<?php echo form_open( current_url(), array( 'autocomplete' => 'off', 'id' => 'form-login' ) ); ?>

<p>
	<label for="identity" class="control-label"><?php _e( "Username" ); ?></label>
	<?php echo form_input( $identity ); ?>
</p>

<p class="mb-4">
	<label for="password" class="control-label"><?php echo _e( "Password" ); ?></label>
	<?php echo form_input( $password ); ?>
</p>

<div class="form-group d-flex justify-content-between align-items-center">
	<div class="flex-fill">
		<label for="remember">
			<?php echo form_checkbox( 'remember', '1', FALSE, array( 'id' => 'remember' ) ); ?>
			<?php _e( "Remember me" ); ?>
		</label>
	</div>

	<div class="flex-fill text-right">
		<?php echo form_submit( $submit ); ?>
	</div>
</div>

<?php echo form_close(); ?>

<p class="text-center mb-0"><a href="<?php echo base_url( 'forgot_password' ); ?>"><?php _e( "I forgot my password" ); ?></a></p>
