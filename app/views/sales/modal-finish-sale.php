<!-- ##################################### FINISH SALE MODAL ###################################### -->

<div class="modal fade d-print-none modal-default" id="modal-finish-sale" tabindex="-1" role="dialog">
	<div class="modal-dialog modal-lg" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title"><?php _e( "Finish Sale" ); ?></h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="<?php _e( "Close" ); ?>"><span aria-hidden="true">&times;</span></button>
			</div>
			<div class="modal-body">
				<div class="row">
					<div class="col-lg-4">
						<div class="form-row mb-3" id="modal_finish_sale_total">
							<div class="form-group col">
								<?php echo form_label( __( "Sale's total" ), 'meta_sale_total', array( 'class' => 'control-label' ) ); ?>
								<div class="input-group input-group-lg">
									<?php echo form_input( $meta_sale_total ); ?>
									<div class="input-group-append">
										<span class="input-group-text"><?php echo $this->assets->conf['currencies'][$this->assets->conf['currency']]; ?></span>
									</div>
								</div>
								<?php echo form_error( 'meta_sale_total' ); ?>
							</div>
						</div>

						<div class="form-row mb-3" id="modal_finish_amount_given">
							<div class="form-group col">
								<?php echo form_label( __( "Amount given" ), 'meta_vars[amount_given]', array( 'class' => 'control-label' ) ); ?>
								<div class="input-group input-group-lg">
									<?php echo form_input( $meta_vars_amount_given ); ?>
									<div class="input-group-append">
										<span class="input-group-text"><?php echo $this->assets->conf['currencies'][$this->assets->conf['currency']]; ?></span>
									</div>
								</div>
								<?php echo form_error( 'meta_vars[amount_given]' ); ?>
							</div>
						</div>

						<div class="form-row" id="modal_finish_change">
							<div class="form-group col">
								<?php echo form_label( __( "Change" ), 'meta_vars[change]', array( 'class' => 'control-label' ) ); ?>
								<div class="input-group input-group-lg">
									<?php echo form_input( $meta_vars_change ); ?>
									<div class="input-group-append">
										<span class="input-group-text"><?php echo $this->assets->conf['currencies'][$this->assets->conf['currency']]; ?></span>
									</div>
								</div>
								<?php echo form_error( 'meta_vars[change]' ); ?>
							</div>
						</div>
					</div>
					<div class="col-lg-8">
						<?php if ( ! empty( $this->assets->conf['payment_method'] ) ) : ?>
						<div class="form-row mb-3" id="modal_finish_payment_method">
							<div class="form-group col">
								<?php 
									echo form_label( __( "Payment method" ), 'meta_vars[payment_method]', array( 'class' => 'control-label' ) ); 
									echo $this->assets->conf['mandatory'];
								?>
								<div class="checkbox-group btn-group-toggle text-center" data-toggle="buttons">
									<?php foreach ( $this->assets->conf['payment_method'] AS $key => $value ) : ?>
									<label class="btn btn-outline-purple my-1">
										<input type="radio" name="meta_vars[payment_method]" id="meta_vars_payment_method_<?php echo $key; ?>" value="<?php echo $key; ?>" <?php echo set_radio( 'meta_vars[payment_method]', $key, ( isset( $sale['meta_vars']['payment_method'] ) AND $sale['meta_vars']['payment_method'] == $key ? TRUE : FALSE ) ); ?>>
										<?php echo $value; ?>
									</label>
									<?php endforeach; ?>
								</div>
								<?php echo form_error( 'meta_vars[payment_method]' ); ?>
							</div>
						</div>
						<?php endif; ?>

						<?php if ( ! empty( $this->assets->conf['cc_company'] ) ) : ?>
						<div class="form-row mb-3 collapse" id="modal_finish_cc_company">
							<div class="form-group col"> 
								<?php 
									echo form_label( __( "Credit card" ), 'meta_vars[credit_card_company]', array( 'class' => 'control-label' ) ); 
									echo $this->assets->conf['mandatory'];
								?>
								<div class="checkbox-group btn-group-toggle text-center" data-toggle="buttons">
									<?php foreach ( $this->assets->conf['cc_company'] AS $key => $value ) : ?>
									<label class="btn btn-outline-secondary my-1">
										<input type="radio" name="meta_vars[credit_card_company]" id="meta_vars_credit_card_company_<?php echo $key; ?>" value="<?php echo $key; ?>" <?php echo set_radio( 'meta_vars[credit_card_company]', $key, ( isset( $sale['meta_vars']['credit_card_company'] ) AND $sale['meta_vars']['credit_card_company'] == $key ? TRUE : FALSE ) ); ?>>
										<?php echo $value; ?>
									</label>
									<?php endforeach; ?>
								</div>
								<?php echo form_error( 'meta_vars[credit_card_company]' ); ?>
							</div>
						</div>
						<?php endif; ?>

						<?php if ( ! empty( $this->assets->conf['dc_company'] ) ) : ?>
						<div class="form-row collapse" id="modal_finish_dc_company">
							<div class="form-group col">
								<?php 
									echo form_label( __( "Debit card" ), 'meta_vars[debit_card_company]', array( 'class' => 'control-label' ) );
									echo $this->assets->conf['mandatory'];
								?>
								<div class="checkbox-group btn-group-toggle text-center" data-toggle="buttons">
									<?php foreach ( $this->assets->conf['dc_company'] AS $key => $value ) : ?>
									<label class="btn btn-outline-info my-1">
										<input type="radio" name="meta_vars[debit_card_company]" id="meta_vars_debit_card_company_<?php echo $key; ?>" value="<?php echo $key; ?>" <?php echo set_radio( 'meta_vars[debit_card_company]', $key, ( isset( $sale['meta_vars']['debit_card_company'] ) AND $sale['meta_vars']['debit_card_company'] == $key ? TRUE : FALSE ) ); ?>>
										<?php echo $value; ?>
									</label>
									<?php endforeach; ?>
								</div>
								<?php echo form_error( 'meta_vars[debit_card_company]' ); ?>
							</div>
						</div>
						<?php endif; ?>
					</div>
				</div>
				<hr>
				<div class="row" id="modal_finish_foreign">
					<div class="col-lg-4">
						<div class="form-row" id="modal_finish_amount_given_foreign">
							<div class="form-group col">
								<?php echo form_label( __( "Foreign currency" ), 'meta_amount_given_foreign', array( 'class' => 'control-label' ) ); ?>
								<div class="input-group">
									<div class="input-group-prepend">
										<button class="btn btn-purple btn-sm dropdown-toggle" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"></button>
										<div class="dropdown-menu">
											<?php foreach ( $this->assets->conf['currencies_ext'] AS $key => $name ) : if ( ! empty( $key ) AND $key != $this->assets->conf['currency'] ) : ?>
											<a class="dropdown-item foreign-currency-item" href="#" data-currency="<?php echo $key; ?>"><?php echo $name; ?></a>
											<?php endif; endforeach; ?>
										</div>
									</div>
									<?php echo form_input( $meta_amount_given_foreign ); ?>
								</div>
								<?php echo form_error( 'meta_amount_given_foreign' ); ?>
							</div>
						</div>
					</div>

					<div class="col-lg-4">
						<div class="form-row" id="modal_finish_sale_total_foreign">
							<div class="form-group col">
								<?php echo form_label( __( "Sale total" ), 'meta_sale_total_foreign', array( 'class' => 'control-label' ) ); ?>
								<div class="input-group">
									<?php echo form_input( $meta_sale_total_foreign ); ?>
									<div class="input-group-append">
										<span class="input-group-text"></span>
									</div>
								</div>
								<?php echo form_error( 'meta_sale_total_foreign' ); ?>
							</div>
						</div>
					</div>

					<div class="col-lg-4">
						<div class="form-row" id="modal_finish_change_foreign">
							<div class="form-group col">
								<?php echo form_label( __( "Change" ), 'meta_change_foreign', array( 'class' => 'control-label' ) ); ?>
								<div class="input-group">
									<?php echo form_input( $meta_change_foreign ); ?>
									<div class="input-group-append">
										<span class="input-group-text"></span>
									</div>
								</div>
								<?php echo form_error( 'meta_change_foreign' ); ?>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="modal-footer">
				<button type="submit" class="btn btn-purple btn-sm" id="save-sale"><?php _e( "Save" ); ?></button>
				<button type="button" class="btn btn-secondary btn-sm" data-dismiss="modal"><?php _e( "Cancel" ); ?></button>
			</div>
		</div>
	</div>
</div>