
<?php 
	echo form_open( current_url(), array( 'autocomplete' => 'off', 'id' => 'form-sales' ) ); 

	// Hiddens
	if ( ! empty( $sale_id ) ) echo form_input( $sale_id );

	echo form_input( $order_id );
	echo form_input( $bill_number );
	echo form_input( $print_order );
	echo form_input( $name );
	echo form_input( $ruc );
	echo form_input( $client_id );
	echo form_input( $address );
	echo form_input( $telephone );

	foreach ( $this->assets->conf['bill_columns'] AS $key => $val )
	{
		echo form_input( ${ "taxes_totals_" . $key } );
		echo form_input( ${ "taxes_liquidation_" . $key } );
	}

	echo form_input( $taxes_liquidation_total );
?>

<div class="row form-elements">

	<div class="col-lg-3">
		<div class="card" id="pos-card">
			<div class="card-body">
				<div class="form-row">
					<div class="form-group col mb-0">
						<?php 
							echo form_label( __( "POS" ), 'pos_id', array( 'class' => 'control-label' ) );
							echo $this->assets->conf['mandatory'];
							echo form_dropdown( $pos_id );
							echo form_error( 'pos_id' );
						?>
					</div>
				</div>
				<div class="form-row bill-number-row">
					<div class="form-group col text-center mt-3">
						<span class="badge badge-primary bill-number-tag"><?php _e( "Bill #" ); ?></span>
						<span class="bill-number-output"></span>
						<?php echo form_error( 'bill_number' ); ?>
					</div>
				</div>
				<div class="form-row bill-number-tools">
					<div class="form-group col text-center">
						<button type="button" id="bill-number-up" class="btn btn-success bill-number-move" title="<?php _e( "Bill Number Up" ); ?>"><i class="fa fa-plus"></i></button>
						<button type="button" id="bill-number-down" class="btn btn-danger bill-number-move" title="<?php _e( "Bill Number Down" ); ?>"><i class="fa fa-minus"></i></button>
					</div>
				</div>
			</div>
		</div>

		<div class="card" id="date-card">
			<div class="card-body">
				<div class="form-row">
					<div class="form-group col">
						<?php 
							echo form_label( __( "Sale date" ), 'sale_date', array( 'class' => 'control-label' ) );
							echo $this->assets->conf['mandatory'];
							echo form_input( $sale_date );
							echo form_error( 'sale_date' );
						?>
					</div>
				</div>
				<div class="form-row">
					<div class="form-group col mb-0">
						<?php 
							echo form_label( __( "Type" ), 'type', array( 'class' => 'control-label' ) );
							echo $this->assets->conf['mandatory'];
						?>
						<div class="checkbox-group p-2">
							<div class="btn-group btn-group-toggle d-flex justify-content-center" data-toggle="buttons">
								<?php foreach ( $this->assets->conf['bill_types'] AS $key => $label ) : ?>
								<label class="btn btn-outline-<?php echo ( $key == "full" ) ? 'success' : 'warning'; ?> d-flex justify-content-center w-50 <?php if ( ! empty( $set_radio ) ) echo "active"; ?>" for="<?php echo $key; ?>">
									<input type="radio" name="type" id="<?php echo $key; ?>" value="<?php echo $key; ?>" <?php echo set_radio( 'type', $key, ( isset( $sale['type'] ) AND $sale['type'] == $key ? TRUE : FALSE ) ); ?>>
									<?php echo $label; ?>
								</label>
								<?php endforeach; ?>
							</div>
						</div>
						<?php echo form_error( 'type' ); ?>
					</div>
				</div>
				<div class="form-row">
					<div class="form-group col due-date-row mt-3">
						<?php 
							echo form_label( __( "Due date" ), 'expiration_date', array( 'class' => 'control-label' ) );
							echo $this->assets->conf['mandatory'];
							echo form_input( $expiration_date );
							echo form_error( 'expiration_date' );
						?>
					</div>
				</div>
			</div>
		</div>

		<?php if ( $this->permissions->can_edit() ) : ?>
		<div class="card" id="user-card">
			<div class="card-body">
				<div class="form-row">
					<div class="form-group col">
						<?php 
							echo form_label( __( "Seller" ), 'seller_id', array( 'class' => 'control-label' ) );
							echo $this->assets->conf['mandatory'];
							echo form_dropdown( $seller_id );
							echo form_error( 'seller_id' );
						?>
					</div>
				</div>
			</div>
		</div>
		<?php endif; ?>
	</div>

	<div class="col-lg-9">
		<?php 
		
		// Errors for hiddens
		
		if ( 
			form_error( 'order_id' ) !== "" OR
			form_error( 'print_order' ) !== "" OR
			form_error( 'taxes' ) !== "" OR
			form_error( 'name' ) !== "" OR
			form_error( 'ruc' ) !== "" OR
			form_error( 'client_id' ) !== "" OR
			form_error( 'address' ) !== "" OR
			form_error( 'telephone' ) !== "" OR
			form_error( 'bill_number' ) !== ""
		) : ?>
		<div class="card" id="errors-card">
			<div class="card-body p-2">
				<div class="form-row">
					<div class="form-group col">
						<?php 
							echo form_error( 'order_id' );
							echo form_error( 'print_order' );
							echo form_error( 'taxes' );
							echo form_error( 'name' );
							echo form_error( 'ruc' );
							echo form_error( 'client_id' );
							echo form_error( 'address' );
							echo form_error( 'telephone' );
							echo form_error( 'bill_number' );
						?>
					</div>
				</div>
			</div>
		</div>
		<?php endif; ?>

		<div class="card" id="client-card">
			<div class="row mx-0 px-0">
				<div class="col-10 mx-0 px-0">
					<table class="table mb-0">
						<col width="100">
						<col width="auto">
						<tbody>
							<tr>
								<th class="py-2"><?php _e( "Name" ); ?></th>
								<td class="py-2"></td>
							</tr>
							<tr>
								<th class="py-2"><?php _e( "R.U.C." ); ?></th>
								<td class="py-2"></td>
							</tr>
						</tbody>
					</table>
				</div>
				<div class="col-2 mx-0 px-0 d-flex align-items-center justify-content-center">
					<button type="button" class="btn btn-secondary text-center w-75" id="search-client-button" title="<?php _e( "Search client" ); ?>">
						<i class="fa fa-search"></i>
					</button>
				</div>
			</div>
		</div>

		<?php if ( isset( $order_meta ) AND ! empty( $order_meta ) ) : ?>
		<div class="card" id="order-card">
			<div class="card-body">
 				<div class="row">
 					<div class="col">
 						<h4 class="mb-0"><?php _e( "Order list" ); ?></h4>
 					</div>
 				</div>				
			</div>
			<div class="table-responsive">
				<table class="table table-striped table-hover order-list">
					<col width="auto">
					<col width="130">
					<col width="130">
					<col width="130">
					<col width="60">
					<thead>
						<tr>
							<th><?php _e( "Product" ); ?></th>
							<th class="text-right"><abbr title="<?php _e( "Price per unit" ); ?>"><?php _e( "Price" ); ?></abbr> <small class="text-muted">(<?php echo $this->assets->conf['currencies'][$this->assets->conf['currency']]; ?>)</small></th>
							<th class="text-right"><?php _e( "Quantity" ); ?></th>
							<th class="text-right"><?php _e( "Remaining" ); ?></th>
							<th></th>
						</tr>
					</thead>
					<tfoot>
						<tr>
							<th><?php _e( "Product" ); ?></th>
							<th class="text-right"><abbr title="<?php _e( "Price per unit" ); ?>"><?php _e( "Price" ); ?></abbr> <small class="text-muted">(<?php echo $this->assets->conf['currencies'][$this->assets->conf['currency']]; ?>)</small></th>
							<th class="text-right"><?php _e( "Quantity" ); ?></th>
							<th class="text-right"><?php _e( "Remaining" ); ?></th>
							<th></th>
						</tr>
					</tfoot>
					<tbody>
						<?php foreach ( $order_meta AS $row ) : $product = $this->products->get( $row['product_id'] ); ?>
						<tr id="order-product-id-<?php echo $row['product_id']; ?>">
							<td class="text-wrap"><?php echo $row['description']; ?></td>
							<td class="text-right order-cell-price"><?php echo my_number_format( $row['unit_price'], $this->assets->conf['decimals'] ); ?></td>
							<td class="text-right order-cell-quantity"><?php printf( '%s <small class="text-muted">%s</small>', my_number_format( $row['quantity'], $this->assets->conf['qty_decimals'] ), $this->assets->conf['units'][$product['unit_measure']] ); ?></td>
							<td class="text-right order-cell-remaining"><?php printf( '%s <small class="text-muted">%s</small>', my_number_format( $row['quantity_remaining'], $this->assets->conf['qty_decimals'] ), $this->assets->conf['units'][$product['unit_measure']] ); ?></td>
							<td class="text-right action-buttons">
								<button class="btn btn-success btn-add-to-sales" title="<?php _e( "Add to Sale" ); ?>">
									<i class="fa fa-plus"></i>
								</button>
								<input type="hidden" name="order_product_id[]" 			class="input-order-product-id" 			value="<?php echo $row['product_id']; ?>" />
								<input type="hidden" name="order_description[]" 		class="input-order-description" 		value="<?php echo $row['description']; ?>" />
								<input type="hidden" name="order_unit_price[]" 			class="input-order-unit-price" 			value="<?php echo $row['unit_price']; ?>" />
								<input type="hidden" name="order_quantity[]" 			class="input-order-quantity" 			value="<?php echo $row['quantity']; ?>" />
								<input type="hidden" name="order_quantity_remaining[]" 	class="input-order-quantity-remaining" 	value="<?php echo $row['quantity_remaining']; ?>" data-original-quantity="<?php echo $row['quantity_remaining']; ?>" />
								<input type="hidden" name="order_unit_measure[]" 		class="input-order-unit-measure" 		value="<?php echo $product['unit_measure']; ?>" />
								<input type="hidden" name="order_product_free[]" 		class="input-order-product-free" 		value="<?php echo $product['free']; ?>" />
							</td>
						</tr>
						<?php endforeach; ?>
					</tbody>
				</table>
			</div>
		</div>
		<?php endif; ?>

		<div class="card" id="products-card">
			<div class="card-body">
 				<div class="row">
 					<div class="col-lg-4 col-md-5 mb-2">
 						<h4 class="mb-0"><?php _e( "Product list" ); ?></h4>
 					</div>
 					<div class="col-lg-8 col-md-7">
 						<?php echo form_dropdown( $product_selection ); ?>
 					</div>
 				</div>
			</div>
			<div class="table-responsive">
				<table class="table table-striped table-hover product-list">
					<col width="auto">
					<col width="130">
					<col width="130">
					<col width="130">
					<col width="60">
					<thead>
						<tr>
							<th><?php _e( "Product" ); ?></th>
							<th class="text-right"><abbr title="<?php _e( "Price per unit" ); ?>"><?php _e( "Price" ); ?></abbr> <small class="text-muted">(<?php echo $this->assets->conf['currencies'][$this->assets->conf['currency']]; ?>)</small></th>
							<th class="text-right"><?php _e( "Quantity" ); ?></th>
							<th class="text-right"><?php _e( "Total" ); ?> <small class="text-muted">(<?php echo $this->assets->conf['currencies'][$this->assets->conf['currency']]; ?>)</small></th>
							<th></th>
						</tr>
					</thead>
					<tfoot>
						<tr>
							<th><?php _e( "Product" ); ?></th>
							<th class="text-right"><abbr title="<?php _e( "Price per unit" ); ?>"><?php _e( "Price" ); ?></abbr> <small class="text-muted">(<?php echo $this->assets->conf['currencies'][$this->assets->conf['currency']]; ?>)</small></th>
							<th class="text-right"><?php _e( "Quantity" ); ?></th>
							<th class="text-right"><?php _e( "Total" ); ?> <small class="text-muted">(<?php echo $this->assets->conf['currencies'][$this->assets->conf['currency']]; ?>)</small></th>
							<th></th>
						</tr>
					</tfoot>
					<tbody>
						<tr>
							<td class="text-wrap cell-description"></td>
							<td class="text-right cell-price"></td>
							<td class="text-right cell-quantity"></td>
							<td class="text-right cell-row-total"></td>
							<td class="text-right action-buttons">
								<button class="btn btn-danger btn-remove-product" title="<?php _e( "Remove" ); ?>">
									<i class="fa fa-times"></i>
								</button>
								<input type="hidden" name="list_sku[]" 			class="input-sku" />
								<input type="hidden" name="list_stock_id[]" 	class="input-stock-id" />
								<input type="hidden" name="list_product_id[]" 	class="input-product-id" />
								<input type="hidden" name="list_description[]" 	class="input-description" />
								<input type="hidden" name="list_unit_price[]" 	class="input-unit-price" />
								<input type="hidden" name="list_quantity[]" 	class="input-quantity" />
								<input type="hidden" name="list_row_total[]" 	class="input-row-total" />
								<input type="hidden" name="list_tax[]" 			class="input-tax" />
							</td>
						</tr>
						<?php
						if ( $this->input->post( 'list_product_id' ) OR isset( $sale_id ) ) : 
							$meta = $this->sales->prepare_meta( ( $this->input->post( 'list_product_id' ) ) ? $this->input->post() : $this->sales->meta_get( $sale_id['value'] ) );
							if ( ! empty( $meta ) ) : foreach ( $meta AS $row ) : $product = $this->products->get( $row['product_id'] );
								$description_title = NULL;
								if ( ! empty( $row['tax'] ) ) $description_title .= $this->assets->conf['bill_columns'][$row['tax']];
								if ( ! empty( $row['sku'] ) ) $description_title .= " | " . __( "SKU" ) . ": " . $row['sku'];
						?>
						<tr>
							<td class="text-wrap cell-description"><span <?php if ( ! empty( $description_title ) ) echo 'title="' . $description_title . '"'; ?>><?php echo $row['description']; ?></span></td>
							<td class="text-right cell-price"><?php echo my_number_format( $row['unit_price'], $this->assets->conf['decimals'] ); ?></td>
							<td class="text-right cell-quantity"><?php printf( '%s <small class="text-muted">%s</small>', my_number_format( $row['quantity'], $this->assets->conf['qty_decimals'] ), $this->assets->conf['units'][$product['unit_measure']] ); ?></td>
							<td class="text-right cell-row-total"><?php echo my_number_format( $row['row_total'], $this->assets->conf['decimals'] ); ?></td>
							<td class="text-right action-buttons">
								<button class="btn btn-danger btn-remove-product" title="<?php _e( "Remove" ); ?>">
									<i class="fa fa-times"></i>
								</button>
								<input type="hidden" name="list_sku[]" 			value="<?php echo $row['sku']; ?>" 			class="input-sku" />
								<input type="hidden" name="list_stock_id[]" 	value="<?php echo $row['stock_id']; ?>" 	class="input-stock-id" />
								<input type="hidden" name="list_product_id[]" 	value="<?php echo $row['product_id']; ?>" 	class="input-product-id" />
								<input type="hidden" name="list_description[]" 	value="<?php echo $row['description']; ?>" 	class="input-description" />
								<input type="hidden" name="list_unit_price[]" 	value="<?php echo $row['unit_price']; ?>" 	class="input-unit-price" />
								<input type="hidden" name="list_quantity[]" 	value="<?php echo $row['quantity']; ?>" 	class="input-quantity" />
								<input type="hidden" name="list_row_total[]" 	value="<?php echo $row['row_total']; ?>" 	class="input-row-total" />
								<input type="hidden" name="list_tax[]" 			value="<?php echo $row['tax']; ?>" 			class="input-tax" />
							</td>
						</tr>
						<?php endforeach ; endif; endif; ?>

						<tr class="table-warning total-amount-row">
							<td colspan="4" class="text-right">
								<div class="d-flex justify-content-end align-items-center">
									<div class="mr-3"><strong><?php _e( "Total amount" ); ?></strong> <span class="mandatory">*</span></div>
									<div>
										<div class="input-group input-group-lg">
											<?php echo form_input( $total_amount ); ?>
											<div class="input-group-append">
												<span class="input-group-text"><?php echo $this->assets->conf['currencies'][$this->assets->conf['currency']]; ?></span>
											</div>
										</div>
									</div>
								</div>
								<?php echo form_error( 'total_amount' ); ?>
							</td>
							<td></td>
						</tr>
					</tbody>
				</table>
			</div>
		</div>

		<div class="card" id="taxes-card">
			<div class="card-body">
 				<div class="row">
 					<?php foreach ( $this->assets->conf['bill_columns'] AS $key => $val ) : ?>
 					<div class="col">
 						<span style="width: 90px" class="badge badge-info tag-taxes-totals-<?php echo $key; ?>"><?php printf( __( "Total %s" ), $val ); ?></span>
 						<small class="text-muted"><?php echo $this->assets->conf['currencies'][$this->assets->conf['currency']]; ?></small>
 						<span class="value-taxes-totals-<?php echo $key; ?>">&mdash;</span>
 					</div>
					<?php endforeach; ?>
 				</div>
 				<div class="row">
 					<?php foreach ( $this->assets->conf['bill_columns'] AS $key => $val ) : ?>
 					<div class="col">
 						<span style="width: 90px" class="badge badge-primary tag-taxes-liquidation-<?php echo $key; ?>" title="<?php printf( __( "Liquidation %s" ), $val ); ?>"><?php printf( __( "Liq. %s" ), $val ); ?></span>
 						<small class="text-muted"><?php echo $this->assets->conf['currencies'][$this->assets->conf['currency']]; ?></small>
 						<span class="value-taxes-liquidation-<?php echo $key; ?>">&mdash;</span>
 					</div>
 					<?php endforeach; ?>
 				</div>
 				<div class="row">
 					<div class="col">
 						<span style="width: 90px" class="badge badge-warning tag-total_taxes"><?php _e( "Total Taxes" ); ?></span>
 						<small class="text-muted"><?php echo $this->assets->conf['currencies'][$this->assets->conf['currency']]; ?></small>
 						<span class="value-total_taxes">&mdash;</span> 						
 					</div>
 				</div>
			</div>			
		</div>
	</div>

</div>

<div class="buttons-set">
	<button type="button" id="finish-sale" class="btn btn-primary">
		<i class="fa fa-save"></i>
		<?php _e( "Save" ); ?>
	</button>
	<?php if ( $this->uri->segment( 2 ) == "edit" ) : ?>
		<?php echo anchor( 'sales/bill/' . $sale['sale_id'], '<i class="fa fa-print"></i> ' . __( "Bill" ), array( 'class' => 'btn btn-purple', 'title' => __( "Print the bill" ) ) ); ?>
		<?php echo anchor( 'sales/add', '<i class="fa fa-plus-circle"></i> ' . __( "New" ), array( 'class' => 'btn btn-secondary', 'title' => __( "Add New Sale" ) ) ); ?>
	<?php endif; ?>
</div>

<?php $this->load->view( 'sales/modal-finish-sale' ); ?>

<?php echo form_close(); ?>
<?php $this->load->view( 'sales/modal-search-client' ); ?>
<?php $this->load->view( 'sales/modal-set-quantity' ); ?>
<?php $this->load->view( 'singles/modal-edit-magnitude' ); ?>

