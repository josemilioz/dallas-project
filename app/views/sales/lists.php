	
<?php if ( ! empty( $sales ) ) : echo form_open( current_url() . '?' . $this->elements->clear_query_string( array( 'action', 'sale_id' ), $this->input->server( 'QUERY_STRING' ) ) ); ?>

<div class="lists-table">
	<div class="table-responsive">

		<table class="table table-striped table-hover">
			<col width="10" class="d-print-none">
			<col width="110">
			<col width="auto">
			<col width="100">
			<col width="100">
			<col width="140">
			<col width="100">
			<thead>
				<tr>
					<th class="d-print-none"><input type="checkbox" name="select_all_cb" class="select_all_cb" title="<?php _e( "Select All" ); ?>" /></th>
					<th><?php _e( "Date" ); ?></th>
					<th><?php _e( "Client" ); ?></th>
					<th class="text-center"><?php _e( "Condition" ); ?></th>
					<th class="text-center"><?php _e( "Paid" ); ?></th>
					<th class="text-right"><?php _e( "Total" ); ?> <small class="text-muted">(<?php echo $this->assets->conf['currencies'][$this->assets->conf['currency']]; ?>)</small></th>
					<th class="d-print-none"></th>
				</tr>
			</thead>
			<tfoot>
				<tr>
					<th class="d-print-none"><input type="checkbox" name="select_all_cb" class="select_all_cb" title="<?php _e( "Select All" ); ?>" /></th>
					<th><?php _e( "Date" ); ?></th>
					<th><?php _e( "Client" ); ?></th>
					<th class="text-center"><?php _e( "Condition" ); ?></th>
					<th class="text-center"><?php _e( "Paid" ); ?></th>
					<th class="text-right"><?php _e( "Total" ); ?> <small class="text-muted">(<?php echo $this->assets->conf['currencies'][$this->assets->conf['currency']]; ?>)</small></th>
					<th class="d-print-none"></th>
				</tr>
			</tfoot>
			<tbody>
				<?php 
				foreach ( $sales AS $sale ) : 
					$sale = $this->assets->fill_empty_vars( $sale );
					$buttons = $this->elements->lists_action_buttons( 'sales', 'sale_id', $sale['sale_id'], $list_buttons_extension );
					$paid = $this->sales->is_paid( $sale );
				?>
				<tr>
					<td class="d-print-none"><input type="checkbox" name="cb_ids[]" value="<?php echo $sale['sale_id']; ?>" /></td>
					<td><?php echo date( $this->assets->conf['date_format'], strtotime( $sale['sale_date'] ) ); ?></td>
					<td class="text-wrap"><strong><?php echo anchor( 'sales/brief/' . $sale['sale_id'], $sale['name'], array( 'class' => 'brief', 'title' => __( "Open Brief" ), 'data-enlarge-modal' => 1 ) ); ?></strong></td>
					<td class="text-center"><?php echo $this->assets->conf['bill_types'][$sale['type']]; ?></td>
					<td class="text-center"><?php echo '<span class="badge badge-'; echo ( $paid ) ? 'success">' . __( "Yes" ) . '</span>' : 'danger">' . __( "No" ) . '</span>'; ?></td>
					<td class="text-right"><?php echo my_number_format( $sale['total_amount'], $this->assets->conf['decimals'] ); ?></td>
					<td class="text-right d-print-none action-buttons">
					<?php 
					foreach ( $buttons AS $n => $b ) $buttons[$n] = ' ' . $b;
					if ( $this->uri->segment( 2 ) !== "trash" )
					{
						if ( $this->permissions->can_edit() ) echo $buttons['edit']; ?>
						<button class="btn btn-primary dropdown-toggle" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
							<i class="fas fa-cog"></i>
						</button>
						<div class="dropdown-menu dropdown-menu-right">
							<?php
							if ( $this->permissions->can_edit() AND ! $paid ) echo $buttons['mark_paid'];
							if ( $this->permissions->can_delete() AND $paid ) echo $buttons['mark_unpaid'];
							if ( $this->permissions->can_edit() )
							{
								switch ( $_GET['status'] )
								{
									case 'unbilled': echo $buttons['mark_billed']; echo $buttons['print']; break;
									case 'billed': echo $buttons['mark_unbilled']; break;
								}
							}
							?>
						</div>						
						<?php if ( $this->permissions->can_trash() ) echo $buttons['trash'];
					}
					else
					{
						if ( $this->permissions->can_restore() ) echo $buttons['restore']; 
						if ( $this->permissions->can_delete() ) echo $buttons['delete'];
					}
					?>						
					</td>
				</tr>
				<?php endforeach; ?>
			</tbody>
		</table>

	</div>
</div>

<div class="row d-print-none lists-tools">

	<?php if ( count( $bulk_actions['options'] ) > 1 ) : ?>

	<div class="col form-inline">
		<div class="bulk-box">
			<?php echo form_dropdown( $bulk_actions ); ?>
			<?php echo form_submit( $submit_bulk_actions ); ?>
		</div>
	</div>

	<?php endif; if ( isset( $pagination ) ) : ?><div class="col page-navigation"><nav><?php echo $pagination; ?></nav></div><?php endif; ?>

</div>

<?php echo form_close(); else : $this->load->view( 'singles/list-empty' ); endif; ?>
