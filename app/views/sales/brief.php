
<div class="card">
	<div class="card-body">
		<div id="brief-wrapper">

			<div class="row">
				<div class="col-md-8 mb-3">
					<div class="element-group row">
						<div class="col-4"><span class="item-label"><?php _e( "Client" ); ?></span></div>
						<div class="col-8"><span class="full-name"><?php echo $sale['name']; ?></span></div>
					</div>
					<div class="element-group row">
						<div class="col-4"><span class="item-label"><?php _e( "R.U.C." ); ?></span></div>
						<div class="col-8"><span><?php echo $sale['ruc']; ?></span></div>
					</div>
					<div class="element-group row">
						<div class="col-4"><span class="item-label"><?php _e( "Address" ); ?></span></div>
						<div class="col-8"><span><?php echo $sale['address']; ?></span></div>
					</div>
					<div class="element-group row">
						<div class="col-4"><span class="item-label"><abbr title="<?php _e( "Point of Sale" ); ?>"><?php _e( "POS" ); ?></abbr></span></div>
						<div class="col-8"><span><?php echo $pos['name']; ?></span></div>
					</div>
					<div class="element-group row">
						<div class="col-4"><span class="item-label"><?php _e( "Status" ); ?></span></div>
						<div class="col-8">
							<span><strong><?php _e( "Paid" ); ?></strong></span>
							<span><?php echo ( $this->sales->is_paid( $sale ) ) ? '<span class="badge badge-success">' . __( "Yes" ) : '<span class="badge badge-danger">' . __( "No" ); ?></span></span>
							<span class="ml-3"><strong><?php _e( "Billed" ); ?></strong></span>
							<span><?php echo ( $sale['billed'] == 1 ) ? '<span class="badge badge-success">' . __( "Yes" ) : '<span class="badge badge-danger">' . __( "No" ); ?></span></span>
						</div>
					</div>
					<?php if ( ! empty( $sale['original']['meta_vars'] ) ) : ?>
					<div class="element-group row">
						<div class="col-4"><span class="item-label"><?php _e( "Payment Method" ); ?></span></div>
						<div class="col-8"><span><?php 
							echo ( ! empty( $sale['original']['meta_vars']['payment_method'] ) ) ? $this->assets->conf['payment_method'][$sale['original']['meta_vars']['payment_method']] : $sale['meta_vars']['payment_method'];

							if ( $sale['original']['meta_vars']['payment_method'] == "debit" AND ! empty( $sale['original']['meta_vars']['credit_card_company'] ) )
							{
								echo ' (' . $this->assets->conf['cc_company'][$sale['meta_vars']['credit_card_company']] . ')';
							}
							else if ( $sale['original']['meta_vars']['payment_method'] == "debit" AND ! empty( $sale['original']['meta_vars']['debit_card_company'] ) )
							{
								echo ' (' . $this->assets->conf['dc_company'][$sale['meta_vars']['debit_card_company']] . ')';								
							}
						?></span></div>
					</div>
					<?php endif; ?>
				</div>

				<div class="col-md-4 mb-3">
					<div class="element-group row">
						<div class="col-5"><span class="item-label"><?php _e( "Bill Number" ); ?></span></div>
						<div class="col-7"><span><code style="font-size: 1em"><?php echo $sale['bill_number']; ?></code></span></div>
					</div>
					<div class="element-group row">
						<div class="col-5"><span class="item-label"><?php _e( "Print Order" ); ?></span></div>
						<div class="col-7"><span><?php echo $sale['print_order']; ?></span></div>
					</div>
					<div class="element-group row">
						<div class="col-5"><span class="item-label"><?php _e( "Sale Date" ); ?></span></div>
						<div class="col-7"><span><?php echo $sale['sale_date']; ?></span></div>
					</div>
					<div class="element-group row">
						<div class="col-5"><span class="item-label"><?php _e( "Sale Condition" ); ?></span></div>
						<div class="col-7"><span><?php echo $this->assets->conf['bill_types'][$sale['type']]; ?></span></div>
					</div>
					<div class="element-group row">
						<div class="col-5"><span class="item-label"><?php _e( "Due Date" ); ?></span></div>
						<div class="col-7"><span><?php echo $sale['expiration_date']; ?></span></div>
					</div>
				</div>
			</div>

			<div class="table-responsive">
				<table class="table table-striped table-hover product-list">
					<col width="auto">
					<col width="160">
					<col width="160">
					<col width="160">
					<thead>
						<tr>
							<th><?php _e( "Product" ); ?></th>
							<th class="text-right"><abbr title="<?php _e( "Price per unit" ); ?>"><?php _e( "Price" ); ?></abbr> <small class="text-muted">(<?php echo $this->assets->conf['currencies'][$this->assets->conf['currency']]; ?>)</small></th>
							<th class="text-right"><?php _e( "Quantity" ); ?></th>
							<th class="text-right"><?php _e( "Total" ); ?> <small class="text-muted">(<?php echo $this->assets->conf['currencies'][$this->assets->conf['currency']]; ?>)</small></th>
						</tr>
					</thead>
					<tfoot>
						<tr>
							<th><?php _e( "Product" ); ?></th>
							<th class="text-right"><abbr title="<?php _e( "Price per unit" ); ?>"><?php _e( "Price" ); ?></abbr> <small class="text-muted">(<?php echo $this->assets->conf['currencies'][$this->assets->conf['currency']]; ?>)</small></th>
							<th class="text-right"><?php _e( "Quantity" ); ?></th>
							<th class="text-right"><?php _e( "Total" ); ?> <small class="text-muted">(<?php echo $this->assets->conf['currencies'][$this->assets->conf['currency']]; ?>)</small></th>
						</tr>
					</tfoot>
					<tbody>
						<?php if ( ! empty( $meta ) ) : foreach ( $meta AS $row ) : $product = $this->products->get( $row['product_id'] ); ?>
						<tr>
							<td><?php echo ( ! empty( $row['description'] ) ) ? $row['description'] : $product['name']; ?> <small class="text-muted">[<?php echo $this->assets->conf['bill_columns'][$product['tax']]; ?>]</small></td>
							<td class="text-right"><?php echo my_number_format( $row['unit_price'], $this->assets->conf['decimals'] ); ?></td>
							<td class="text-right"><?php printf( "%s %s", my_number_format( $row['quantity'], $this->assets->conf['qty_decimals'] ), '<small class="text-muted">' . $this->assets->conf['units'][$product['unit_measure']] . '</small>' ); ?></td>
							<td class="text-right"><?php echo my_number_format( $row['row_total'], $this->assets->conf['decimals'] ); ?></td>
						</tr>
						<?php endforeach; endif; ?>
						<tr class="table-info">
							<th colspan="3" class="text-right"><?php _e( "Total amount" ); ?></th>
							<th class="text-right"><h6 class="mb-0"><?php echo my_number_format( $sale['total_amount'], $this->assets->conf['decimals'] ); ?></h6></th>
						</tr>
					</tbody>
				</table>
			</div>

			<?php if ( ! empty( $sale['original']['meta_vars'] ) ) : ?>
			<div class="row text-center pb-3 pl-3 pr-3" style="font-size: 1.15em;">
				<div class="col-md table-success">
					<div class="p-2">
						<span class="badge badge-dark"><?php _e( "Amount Given" ); ?></span>
						<small><?php echo $this->assets->conf['currencies'][$this->assets->conf['currency']]; ?></small>
						<strong><?php echo ( ! empty( $sale['original']['meta_vars']['amount_given'] ) ) ? my_number_format( $sale['meta_vars']['amount_given'], $this->assets->conf['decimals'] ) : $sale['meta_vars']['amount_given']; ?></strong>
					</div>
				</div>
				<div class="col-md table-warning">
					<div class="p-2">
						<span class="badge badge-dark"><?php _e( "Change" ); ?></span>
						<small><?php echo $this->assets->conf['currencies'][$this->assets->conf['currency']]; ?></small>
						<strong><?php echo ( ! empty( $sale['original']['meta_vars']['change'] ) ) ? my_number_format( $sale['meta_vars']['change'], $this->assets->conf['decimals'] ) : $sale['meta_vars']['change']; ?></strong>
					</div>
				</div>
			</div>
			<?php endif; ?>

			<div class="row">
				<div class="col bg-light py-2">
					<em class="d-block d-md-inline-block mr-3"><?php _e( "Tax totals" ); ?></em>
					<?php foreach ( $this->assets->conf['bill_columns'] AS $key => $name ) : ?>
					<div class="d-block d-md-inline-block mr-3">
						<span class="badge badge-secondary"><?php echo $name; ?></span>
						<small class="text-muted"><?php echo $this->assets->conf['currencies'][$this->assets->conf['currency']]; ?></small>
						<span><?php echo ( ! empty( $sale['original']['taxes']['totals'][$key] ) ) ? my_number_format( $sale['taxes']['totals'][$key], $this->assets->conf['decimals'] ) : '&mdash;'; ?></span>
					</div>
					<?php endforeach; ?>
				</div>
			</div>

			<div class="row">
				<div class="col bg-light py-2">
					<em class="d-block d-md-inline-block mr-3"><?php _e( "Tax liquidation" ); ?></em>

					<?php foreach ( $this->assets->conf['bill_columns'] AS $key => $name ) : ?>
					<div class="d-block d-md-inline-block mr-3">
						<span class="badge badge-secondary"><?php echo $name; ?></span>
						<small class="text-muted"><?php echo $this->assets->conf['currencies'][$this->assets->conf['currency']]; ?></small>
						<span><?php echo ( ! empty( $sale['original']['taxes']['liquidation'][$key] ) ) ? my_number_format( $sale['taxes']['liquidation'][$key], $this->assets->conf['decimals'] ) : '&mdash;'; ?></span>
					</div>
					<?php endforeach; ?>
					<div class="d-block d-md-inline-block">
						<span class="badge badge-purple"><?php _e( "Total Taxes" ); ?></span>
						<span><?php echo ( ! empty( $sale['original']['taxes']['liquidation']['total'] ) ) ? my_number_format( $sale['taxes']['liquidation']['total'], $this->assets->conf['decimals'] ) : '&mdash;'; ?></span>
					</div>
				</div>
			</div>

			<?php if ( ! empty( $sale['created'] ) OR ! empty( $sale['modified'] ) ) : ?>
			<div class="row last-activity">
				<div class="col">
					<div class="created">
						<?php if ( ! empty( $sale['created'] ) ) : ?>
						<p class="label"><?php _e( "Created by" ); ?></p>
						<p class="value"><?php echo $this->ion_auth->user_name( "fullname", $sale['creator'] ) ?> <small><?php echo date( $this->assets->conf['datetime_format'], strtotime( $sale['created'] ) ); ?></small></p>
						<?php endif; ?>
					</div>
				</div>
				<div class="col">
					<div class="modified">
						<?php if ( ! empty( $sale['modified'] ) ) : ?>
						<p class="label"><?php _e( "Last modified by" ); ?></p>
						<p class="value"><?php echo $this->ion_auth->user_name( "fullname", $sale['modifier'] ) ?> <small><?php echo date( $this->assets->conf['datetime_format'], strtotime( $sale['modified'] ) ); ?></small></p>
						<?php endif; ?>
					</div>
				</div>
			</div>
			<?php endif; ?>
			
		</div>
	</div>
</div>