<!-- ##################################### SEARCH CLIENT MODAL ###################################### -->

<div class="modal fade d-print-none modal-default" id="modal-search-client" tabindex="-1" role="dialog">
	<div class="modal-dialog modal-lg" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title"><?php _e( "Search Client" ); ?></h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="<?php _e( "Close" ); ?>"><span aria-hidden="true">&times;</span></button>
			</div>
			<div class="modal-body">
				<div class="form-row mb-3">
					<div class="form-group col" id="modal_client_id_box">
						<?php 
							echo form_label( __( "Client" ), 'modal_client_id', array( 'class' => 'control-label' ) );
							echo $this->assets->conf['mandatory'];
							echo form_dropdown( $modal_client_id );
							echo form_error( 'modal_client_id' );
						?>
					</div>
				</div>

				<div id="user-brief"></div>
				
				<div id="flash-client-add">
					<hr>
					<p class="text-info"><em><?php _e( "... or ADD a new client quickly" ); ?></em></p>
					<div class="form-row mb-2">
						<div class="form-group col">
							<?php
								echo form_label( __( "First name" ), 'modal_client_first_name', array( 'class' => 'control-label' ) );
								echo $this->assets->conf['mandatory'];
								echo form_input( $modal_client_first_name );
								echo form_error( 'modal_client_first_name' );
							?>
						</div>
						<div class="form-group col">
							<?php
								echo form_label( __( "Last name" ), 'modal_client_last_name', array( 'class' => 'control-label' ) );
								echo form_input( $modal_client_last_name );
								echo form_error( 'modal_client_last_name' );
							?>
						</div>
					</div>
					<div class="form-row">
						<div class="form-group col">
							<?php
								echo form_label( __( "RUC or ID" ), 'modal_client_ruc', array( 'class' => 'control-label' ) );
								echo form_input( $modal_client_ruc );
								echo form_error( 'modal_client_ruc' );
							?>
						</div>
						<div class="form-group col">
							<?php
								echo form_label( __( "Cellphone" ), 'modal_client_cellphone', array( 'class' => 'control-label' ) );
								echo form_input( $modal_client_cellphone );
								echo form_error( 'modal_client_cellphone' );
							?>
						</div>
						<div class="form-group col">
							<?php
								echo form_label( __( "Email" ), 'modal_client_email', array( 'class' => 'control-label' ) );
								echo form_input( $modal_client_email );
								echo form_error( 'modal_client_email' );
							?>
						</div>
					</div>
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-purple btn-sm" id="confirm-client-button"><?php _e( "Assign" ); ?></button>
				<button type="button" class="btn btn-warning btn-sm" id="casual-client-button"><?php _e( "Casual Client" ); ?></button>
				<button type="button" class="btn btn-secondary btn-sm" data-dismiss="modal"><?php _e( "Cancel" ); ?></button>
			</div>
		</div>
	</div>
</div>