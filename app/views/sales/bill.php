<link rel="stylesheet" type="text/css" href="<?php echo site_url( 'assets/css/printables.css' ); ?>" />

<page class="bill">
	<div class="header">
		<div class="blocks logo">
			<h2><?php echo $this->assets->conf['company_name']; ?></h2>
		</div>
		<div class="blocks company-info">
			<h4><?php echo $this->assets->conf['bill_header']['oficial_company_name']; ?></h4>
			<h5><?php echo $this->assets->conf['bill_header']['activity']; ?></h5>
			<?php if ( ! empty( $this->assets->conf['bill_header']['address'] ) ) : ?><p><?php printf( '<u>%1$s:</u> %2$s', __( "Address" ), $this->assets->conf['bill_header']['address'] ); ?></p><?php endif; ?>
			<?php if ( ! empty( $this->assets->conf['bill_header']['phone'] ) ) : ?><p><?php printf( '<u>%1$s:</u> %2$s', __( "Phone" ), $this->assets->conf['bill_header']['phone'] ); ?></p><?php endif; ?>
			<?php if ( ! empty( $this->assets->conf['bill_header']['email'] ) ) : ?><p><?php printf( '<u>%1$s:</u> %2$s', __( "E-mail" ), $this->assets->conf['bill_header']['email'] ); ?></p><?php endif; ?>
			<?php if ( ! empty( $this->assets->conf['bill_header']['website'] ) ) : ?><p><?php printf( '<u>%1$s:</u> %2$s', __( "Website" ), $this->assets->conf['bill_header']['website'] ); ?></p><?php endif; ?>
		</div>
		<div class="blocks print-order">
			<table class="print-order-data" cellpadding="0" cellspacing="0">
				<tr>
					<td><?php _e( "Print Order Number" ); ?></td>
					<td>: <?php echo $print_order['print_order']; ?></td>
				</tr>
				<tr>
					<td><?php _e( "Print Order Start Date" ); ?></td>
					<td>: <?php echo date( $this->assets->conf['date_format'], strtotime( $print_order['date_start'] ) ); ?></td>
				</tr>
				<tr>
					<td><?php _e( "Print Order End Date" ); ?></td>
					<td>: <?php echo date( $this->assets->conf['date_format'], strtotime( $print_order['date_end'] ) ); ?></td>
				</tr>
			</table>
			<div class="labels">
				<div class="document-type"><?php _e( "FACTURA" ); ?></div>
				<div class="company-ruc"><?php printf( __( "RUC: %s" ), $this->assets->conf['bill_header']['company_ruc'] ); ?></div>
			</div>
			<div class="bill-number">
				<span class="label"><?php _e( "#" ); ?></span>
				<span class="places"><?php echo $print_order['location']; ?>-<?php echo $print_order['expedition']; ?>-</span>
				<span class="number"><?php $number = explode( "-", $sale['bill_number'] ); echo $number[2]; ?></span>
			</div>
		</div>
	</div>

	<div class="customer">
		<table cellpadding="0" cellspacing="4" class="first">
			<tr>
				<td class="col-1 tag"><?php _e( "Name or social reason" ); ?></td>
				<td class="value" colspan="3"><?php echo ( ! empty( $sale['name'] ) ) ? $sale['name'] : $this->assets->conf['no_data']; ?></td>
			</tr>
			<tr>
				<td class="col-1 tag"><?php _e( "Address" ); ?></td>
				<td class="col-2 value"><?php echo ( ! empty( $sale['address'] ) ) ? $sale['address'] : $this->assets->conf['no_data']; ?></td>
				<td class="col-3 tag"><?php _e( "Phone" ); ?></td>
				<td class="col-4 value"><?php echo ( ! empty( $sale['telephone'] ) ) ? $sale['telephone'] : $this->assets->conf['no_data']; ?></td>
			</tr>
		</table>
		<table cellpadding="0" cellspacing="4" class="second">
			<tr>
				<td class="col-1 tag"><?php _e( "Date" ); ?></td>
				<td class="col-2 value"><?php echo ( ! empty( $sale['sale_date'] ) ) ? date( $this->assets->conf['date_format'], strtotime( $sale['sale_date'] ) ) : $this->assets->conf['no_data']; ?></td>
				<td class="col-3 tag"><?php _e( "R.U.C." ); ?></td>
				<td class="col-4 value"><?php echo ( ! empty( $sale['ruc'] ) ) ? $sale['ruc'] : $this->assets->conf['no_data']; ?></td>
				<td class="col-5 tag"><?php _e( "Condition" ); ?></td>
				<td class="col-6 value"><?php echo ( ! empty( $sale['type'] ) ) ? $this->assets->conf['bill_types'][$sale['type']] : $this->assets->conf['no_data']; ?></td>
				<td class="col-7 tag"><?php _e( "Due" ); ?></td>
				<td class="col-8 value"><?php echo ( ! empty( $sale['expiration_date'] ) ) ? date( $this->assets->conf['date_format'], strtotime( $sale['expiration_date'] ) ) : $this->assets->conf['no_data']; ?></td>
			</tr>
		</table>
	</div>

	<div class="products">
		<table class="products-list" cellpadding="0" cellspacing="0">
			<thead>
				<tr>
					<th rowspan="2" class="col col-1 tags"><?php _e( "Quantity" ); ?></th>
					<th rowspan="2" class="col col-2 tags"><?php _e( "Description" ); ?></th>
					<th rowspan="2" class="col col-3 tags"><?php _e( "Unit Price" ); ?></th>
					<th colspan="3" class="values-title"><?php printf( __( "Values in %s" ), $this->assets->conf['currencies_ext'][$this->assets->conf['currency']] ); ?></th>
				</tr>
				<tr>
					<?php $count = 4; foreach ( $this->assets->conf['bill_columns'] AS $key => $val ) : ?>
					<th class="col col-<?php echo $count; ?> tags"><?php echo $val; ?></th>
					<?php $count++; endforeach; ?>
				</tr>
			</thead>
			<tbody>
				<?php foreach ( $metas AS $meta ) : ?>
				<tr>
					<td class="col col-1 values"><?php echo my_number_format( $meta['quantity'], $this->assets->conf['qty_decimals'] ); ?></td>
					<td class="col col-2 values"><?php echo $meta['description']; ?></td>
					<td class="col col-3 values"><?php echo my_number_format( $meta['unit_price'], $this->assets->conf['decimals'] ); ?></td>
					<?php $count = 4; foreach ( $this->assets->conf['bill_columns'] AS $key => $val ) : ?>
					<td class="col col-<?php echo $count; ?> values"><?php echo ( $key == $meta['tax'] ) ? my_number_format( $meta['row_total'], $this->assets->conf['decimals'] ) : '&mdash;'; ?></td>
					<?php $count++; endforeach; ?>
				</tr>
				<?php endforeach; ?>
			</tbody>
			<tfoot>
				<tr>
					<th colspan="3" class="subtotals-title"><?php _e( "Subtotals" ); ?></th>
					<?php $count = 4; foreach ( $this->assets->conf['bill_columns'] AS $key => $val ) : ?>
					<th class="col-4 values"><?php echo ( ! empty( $sale['taxes']['totals'][$key] ) ) ? my_number_format( $sale['taxes']['totals'][$key], $this->assets->conf['decimals'] ) : '&mdash;'; ?></th>
					<?php $count++; endforeach; ?>
				</tr>
			</tfoot>
		</table>
	</div>

	<div class="totals">
		<table cellpadding="0" cellspacing="0">
			<tr>
				<td class="tags"><?php _e( "Sale total" ); ?></td>
				<td class="total-letters">UN MILLON CUATROCIENTOS OCHENTA MIL SETECIENTOS SETENTA MIL NOVENTA Y QUINIENTOS MIL GUARANIES</td>
				<td rowspan="2" class="total-number">
					<div class="tag">
						<?php _e( "Total" ); ?>
					</div>
					<div class="value">
						<span class="currency"><?php echo $this->assets->conf['currencies'][$this->assets->conf['currency']]; ?></span>
						<?php echo my_number_format( $sale['total_amount'], $this->assets->conf['decimals'] ); ?>
					</div>
				</td>
			</tr>
			<tr>
				<td class="tags"><?php _e( "VAT Liquidation" ); ?></td>
				<td><?php
					foreach ( $sale['taxes']['liquidation'] AS $key => $val )
					{
						printf( '<span class="tax-labels">%1$s:</span> <span class="tax-values">%2$s</span>', ( isset( $this->assets->conf['bill_columns'][$key] ) ? $this->assets->conf['bill_columns'][$key] : __( "Total" ) ), ( ! empty( $val ) ? my_number_format( $val, $this->assets->conf['decimals'] ) . '.&mdash;' : '&mdash;' ) );
						echo '&nbsp;&nbsp;&nbsp;';
					}
				?></td>
			</tr>
			<tr>
				<td colspan="3"><?php 
					printf( '<u>%1$s:</u> %2$s', __( "Seller" ), 'Asflkrt sdflkr' );
					echo '&nbsp;&nbsp;&nbsp;';
					printf( '<u>%1$s:</u> %2$s', __( "Bill Creator" ), 'Akjdfkj wekrjadf' );
					echo '&nbsp;&nbsp;&nbsp;';
					printf( '<u>%1$s:</u> %2$s', __( "POS ID" ), ( ! empty( $pos ) ? $pos['name'] : $this->assets->conf['no_data'] ) );
				?></td>
			</tr>
		</table>
	</div>
</page>