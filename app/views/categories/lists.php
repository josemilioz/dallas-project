	
<?php if ( ! empty( $categories ) ) : echo form_open( current_url() ); ?>

<div class="lists-table">
	<div class="table-responsive">

		<table class="table table-striped table-hover">
			<col width="10" class="d-print-none">
			<col width="40">
			<col width="auto">
			<col width="auto">
			<col width="auto">
			<col width="100">
			<thead>
				<tr>
					<th class="d-print-none"><input type="checkbox" name="select_all_cb" class="select_all_cb" title="<?php _e( "Select All" ); ?>" /></th>
					<th colspan="2"><?php _e( "Category" ); ?></th>
					<th class="d-none d-lg-table-cell"><?php _e( "Description" ); ?></th>
					<th class="d-none d-md-table-cell text-right"><abbr title="<?php _e( "Number of products" ); ?>"><?php _e( "# Products" ); ?></abbr></th>
					<th class="d-print-none"></th>
				</tr>
			</thead>
			<tfoot>
				<tr>
					<th class="d-print-none"><input type="checkbox" name="select_all_cb" class="select_all_cb" title="<?php _e( "Select All" ); ?>" /></th>
					<th colspan="2"><?php _e( "Category" ); ?></th>
					<th class="d-none d-lg-table-cell"><?php _e( "Description" ); ?></th>
					<th class="d-none d-md-table-cell text-right"><abbr title="<?php _e( "Number of products" ); ?>"><?php _e( "# Products" ); ?></abbr></th>
					<th class="d-print-none"></th>
				</tr>
			</tfoot>
			<tbody>
				<?php 
				foreach ( $categories AS $category ) : 
					// Description addapt
					$category['description'] = ( strlen( $category['description'] ) > 80 ) ? substr( $category['description'], 0, 80 ) . "..." : $category['description'];
					$category = $this->assets->fill_empty_vars( $category );
					$buttons = $this->elements->lists_action_buttons( 'categories', 'category_id', $category['category_id'], $list_buttons_extension );
				?>
				<tr>
					<td class="d-print-none"><input type="checkbox" name="cb_ids[]" value="<?php echo $category['category_id']; ?>" /></td>
					<td class="user-picture"><img src="<?php echo $this->categories->picture( $category['category_id'] ); ?>" /></td>
					<td><strong><?php echo anchor( 'categories/brief/' . $category['category_id'], $category['name'], array( 'class' => 'brief', 'title' => __( "Open Brief" ) ) ); ?></strong></td>
					<td class="d-none d-lg-table-cell"><?php echo $category['description']; ?></td>
					<td class="d-none d-md-table-cell text-right"><?php echo my_number_format( $this->categories->count_products( $category['category_id'] ) ); ?></td>
					<td class="text-right d-print-none action-buttons">
					<?php 
					foreach ( $buttons AS $n => $b ) $buttons[$n] = ' ' . $b;
					if ( $this->uri->segment( 2 ) !== "trash" )
					{
						echo $buttons['see_all'];
						if ( $this->permissions->can_edit() ) echo $buttons['edit'];
						if ( $this->permissions->can_trash() ) echo $buttons['trash'];
					}
					else
					{
						if ( $this->permissions->can_restore() ) echo $buttons['restore']; 
						if ( $this->permissions->can_delete() ) echo $buttons['delete'];
					}
					?>						
					</td>
				</tr>
				<?php endforeach; ?>
			</tbody>
		</table>

	</div>
</div>

<div class="row d-print-none lists-tools">

	<?php if ( count( $bulk_actions['options'] ) > 1 ) : ?>

	<div class="col form-inline">
		<div class="bulk-box">
			<?php echo form_dropdown( $bulk_actions ); ?>
			<?php echo form_submit( $submit_bulk_actions ); ?>
		</div>
	</div>

	<?php endif; if ( isset( $pagination ) ) : ?><div class="col page-navigation"><nav><?php echo $pagination; ?></nav></div><?php endif; ?>

</div>

<?php echo form_close(); else : $this->load->view( 'singles/list-empty' ); endif; ?>
