
<div class="card">
	<div class="card-body">
		<div id="brief-wrapper">

			<div class="row">
				<div class="col-lg-5 mb-3">
					<div class="picture-big-container w-100 text-center">
						<img class="picture-big w-75" src="<?php echo $this->categories->picture( $category['category_id'] ); ?>" />
					</div>
				</div>
				<div class="col-lg-7 mb-3">
					<div>
						<div class="element-group row">
							<div class="col-4"><span class="item-label"><?php _e( "Name" ); ?></span></div>
							<div class="col-8"><span class="full-name"><?php echo $category['name']; ?></span></div>
						</div>
						<div class="element-group row">
							<div class="col-4"><span class="item-label"><?php _e( "Description" ); ?></span></div>
							<div class="col-8"><?php echo nl2br( $category['description'] ); ?></div>
						</div>
					</div>
					<h4 class="mt-3">Defaults</h4>
					<div>
						<div class="element-group row">
							<div class="col-4"><span class="item-label"><?php _e( "Unit measure" ); ?></span></div>
							<div class="col-8"><span><?php echo ( ! empty( $category['original']['unit_measure'] ) ) ? $this->assets->conf['units'][$category['unit_measure']] : $category['unit_measure']; ?></span></div>
						</div>
						<div class="element-group row">
							<div class="col-4"><span class="item-label"><?php _e( "Unit quantity" ); ?></span></div>
							<div class="col-8"><span><?php echo ( ! empty( $category['original']['unit_quantity'] ) ) ? my_number_format( $category['unit_quantity'], $this->assets->conf['decimals'] ) : $category['unit_quantity']; ?></span></div>
						</div>
						<div class="element-group row">
							<div class="col-4"><span class="item-label"><?php _e( "Currency" ); ?></span></div>
							<div class="col-8"><span><?php echo ( ! empty( $category['original']['currency'] ) ) ? $this->assets->conf['currencies_ext'][$category['currency']] : $category['currency']; ?></span></div>
						</div>
						<div class="element-group row">
							<div class="col-4"><span class="item-label"><?php _e( "Tax" ); ?></span></div>
							<div class="col-8"><span><?php echo ( ! empty( $category['original']['tax'] ) ) ? $this->assets->conf['bill_columns'][$category['tax']] : $category['tax']; ?></span></div>
						</div>
					</div>
				</div>
			</div>

			<?php if ( ! empty( $category['created'] ) OR ! empty( $category['modified'] ) ) : ?>
			<div class="row last-activity">
				<div class="col">
					<div class="created">
						<?php if ( ! empty( $category['created'] ) ) : ?>
						<p class="label"><?php _e( "Created by" ); ?></p>
						<p class="value"><?php echo $this->ion_auth->user_name( "fullname", $category['creator'] ) ?> <small><?php echo date( $this->assets->conf['datetime_format'], strtotime( $category['created'] ) ); ?></small></p>
						<?php endif; ?>
					</div>
				</div>
				<div class="col">
					<div class="modified">
						<?php if ( ! empty( $category['modified'] ) ) : ?>
						<p class="label"><?php _e( "Last modified by" ); ?></p>
						<p class="value"><?php echo $this->ion_auth->user_name( "fullname", $category['modifier'] ) ?> <small><?php echo date( $this->assets->conf['datetime_format'], strtotime( $category['modified'] ) ); ?></small></p>
						<?php endif; ?>
					</div>
				</div>
			</div>
			<?php endif; ?>
			
		</div>
	</div>
</div>