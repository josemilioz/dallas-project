
<?php echo form_open( current_url(), array( 'autocomplete' => 'off', 'id' => 'form-categories' ) ); ?>

<div class="row form-elements">

	<div class="col-lg-8">
		<div class="card" id="brand-card">
			<div class="card-body">
				<div class="form-row">
					<div class="form-group col">
						<?php 
							echo form_label( __( "Name" ), 'name', array( 'class' => 'control-label' ) );
							echo $this->assets->conf['mandatory'];
							echo form_input( $name );
							echo form_error( 'name' );
						?>
					</div>
				</div>
				<div class="form-row">
					<div class="form-group col">
						<?php
							echo form_label( __( "Description" ), 'description', array( 'class' => 'control-label' ) );
							echo form_textarea( $description );
							echo form_error( 'description' );
						?>
					</div>
				</div>
			</div>
		</div>

		<div class="card" id="measurement-card">
			<div class="card-body">
				<h4><?php _e( "Default Measurement" ); ?></h4>
				<div class="form-row">
					<div class="form-group col-md-6">
						<?php 
							echo form_label( __( "Unit measure" ), 'unit_measure', array( 'class' => 'control-label' ) );
							echo form_dropdown( $unit_measure );
							echo form_error( 'unit_measure' );
						?>
					</div>
					<div class="form-group col-md-6">
						<?php 
							echo form_label( __( "Unit quantity" ), 'unit_quantity', array( 'class' => 'control-label' ) );
							echo form_input( $unit_quantity );
							echo form_error( 'unit_quantity' );
						?>
					</div>
				</div>
				<div class="form-row">
					<div class="form-group col-md-6">
						<?php
							echo form_label( __( "Currency" ), 'currency', array( 'class' => 'control-label' ) );
							echo form_dropdown( $currency );
							echo form_error( 'currency' );
						?>
					</div>
					<div class="form-group col-md-6">
						<?php 
							echo form_label( __( "Tax" ), 'tax', array( 'class' => 'control-label' ) );
							echo form_dropdown( $tax );
							echo form_error( 'tax' );
						?>
					</div>
				</div>
			</div>
		</div>
	</div>

	<div class="col-lg-4">
		<div class="card" id="picture-card">
			<div class="card-body">
				<h4><?php _e( "Picture" ); ?></h4>
				<?php $this->load->view( 'files/model-single-picture', ( isset( $category_id ) ) ? $this->files->get_files_src( $category_id['value'], 'categories' ) : NULL ); ?>
			</div>
		</div>
	</div>

</div>

<div class="buttons-set">
	<button type="submit" class="btn btn-primary">
		<i class="fa fa-save"></i>
		<?php _e( "Save" ); ?>
	</button>
	<?php if ( $this->uri->segment( 2 ) == "edit" ) echo anchor( 'categories/add', '<i class="fa fa-plus-circle"></i> ' . __( "New" ), array( 'class' => 'btn btn-secondary', 'title' => __( "Add New Category" ) ) ); ?>
</div>

<?php if ( ! empty( $category_id ) ) echo form_input( $category_id ); ?>
<?php echo form_close(); ?>