
<?php echo form_open( current_url(), array( 'autocomplete' => 'off', 'id' => 'form-print-orders' ) ); ?>

<div class="row form-elements">
	<div class="col-lg-5">
		<div class="card" id="data-card">
			<div class="card-body">
				<div class="form-row">
					<div class="form-group col">
						<?php 
							echo form_label( __( "Print order" ), 'print_order', array( 'class' => 'control-label' ) );
							echo $this->assets->conf['mandatory'];
							echo form_input( $print_order );
							echo form_error( 'print_order' );
						?>
					</div>
				</div>
				<div class="form-row">
					<div class="form-group col">
						<?php 
							echo form_label( __( "POS" ), 'pos_id', array( 'class' => 'control-label' ) );
							echo $this->assets->conf['mandatory'];
							echo form_dropdown( $pos_id );
							echo form_error( 'pos_id' );
						?>
					</div>
				</div>
			</div>
		</div>
	</div>

	<div class="col-lg-7">
		<div class="card" id="users-card">
			<div class="card-body">
				<div class="form-row">
					<div class="form-group col-md-6">
						<?php
							echo form_label( __( "Date start" ), 'date_start', array( 'class' => 'control-label' ) );
							echo $this->assets->conf['mandatory'];
							echo form_input( $date_start );
							echo form_error( 'date_start' );
						?>
					</div>
					<div class="form-group col-md-6">
						<?php 
							echo form_label( __( "Date end" ), 'date_end', array( 'class' => 'control-label' ) );
							echo $this->assets->conf['mandatory'];
							echo form_input( $date_end );
							echo form_error( 'date_end' );
						?>
					</div>
				</div>
				<div class="form-row">
					<div class="form-group col-md-6">
						<?php 
							echo form_label( __( "Location" ), 'location', array( 'class' => 'control-label' ) );
							echo $this->assets->conf['mandatory'];
							echo form_input( $location );
							echo form_error( 'location' );
						?>
					</div>
					<div class="form-group col-md-6">
						<?php 
							echo form_label( __( "Expedition" ), 'expedition', array( 'class' => 'control-label' ) );
							echo $this->assets->conf['mandatory'];
							echo form_input( $expedition );
							echo form_error( 'expedition' );
						?>
					</div>
				</div>
				<div class="form-row">
					<div class="form-group col-md-6">
						<?php 
							echo form_label( __( "Number start" ), 'number_start', array( 'class' => 'control-label' ) );
							echo $this->assets->conf['mandatory'];
							echo form_input( $number_start );
							echo form_error( 'number_start' );
						?>
					</div>
					<div class="form-group col-md-6">
						<?php 
							echo form_label( __( "Number end" ), 'number_end', array( 'class' => 'control-label' ) );
							echo $this->assets->conf['mandatory'];
							echo form_input( $number_end );
							echo form_error( 'number_end' );
						?>
					</div>
				</div>
			</div>
		</div>
	</div>

</div>

<div class="buttons-set">
	<button type="submit" class="btn btn-primary">
		<i class="fa fa-save"></i>
		<?php _e( "Save" ); ?>
	</button>
	<?php if ( $this->uri->segment( 2 ) == "edit" ) echo anchor( 'print_orders/add', '<i class="fa fa-plus-circle"></i> ' . __( "New" ), array( 'class' => 'btn btn-secondary', 'title' => __( "Add New Print Order" ) ) ); ?>
</div>

<?php if ( ! empty( $print_order_id ) ) echo form_input( $print_order_id ); ?>
<?php echo form_close(); ?>