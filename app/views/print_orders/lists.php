
<?php if ( ! empty( $print_orders ) ) : echo form_open( current_url() ); ?>

<div class="lists-table">
	<div class="table-responsive">

		<table class="table table-striped table-hover">
			<col width="10" class="d-print-none">
			<col width="auto">
			<col width="auto">
			<col width="auto">
			<col width="auto">
			<col width="auto">
			<col width="100">
			<thead>
				<tr>
					<th class="d-print-none"><input type="checkbox" name="select_all_cb" class="select_all_cb" title="<?php _e( "Select All" ); ?>" /></th>
					<th><?php _e( "Print Order" ); ?></th>
					<th><abbr title="<?php _e( "Point of Sale" ); ?>"><?php _e( "POS" ); ?></abbr></th>
					<th class="text-right"><?php _e( "Number Start" ); ?></th>
					<th class="text-right"><?php _e( "Number End" ); ?></th>
					<th class="text-right"><abbr title="<?php _e( "Remaining bill numbers" ); ?>"><?php _e( "Remaining" ); ?></abbr></th>
					<th class="d-print-none"></th>
				</tr>
			</thead>
			<tfoot>
				<tr>
					<th class="d-print-none"><input type="checkbox" name="select_all_cb" class="select_all_cb" title="<?php _e( "Select All" ); ?>" /></th>
					<th><?php _e( "Print Order" ); ?></th>
					<th><abbr title="<?php _e( "Point of Sale" ); ?>"><?php _e( "POS" ); ?></abbr></th>
					<th class="text-right"><?php _e( "Number Start" ); ?></th>
					<th class="text-right"><?php _e( "Number End" ); ?></th>
					<th class="text-right"><abbr title="<?php _e( "Remaining bill numbers" ); ?>"><?php _e( "Remaining" ); ?></abbr></th>
					<th class="d-print-none"></th>
				</tr>
			</tfoot>
			<tbody>
				<?php 
				foreach ( $print_orders AS $print_order ) : 
					$pos = $this->assets->fill_empty_vars( $this->pos->get( $print_order['pos_id'] ), 'pos' );
					$print_order = $this->assets->fill_empty_vars( $print_order, 'print_orders' );
					$buttons = $this->elements->lists_action_buttons( 'print_orders', 'print_order_id', $print_order['print_order_id'], $list_buttons_extension );
				?>
				<tr>
					<td class="d-print-none"><input type="checkbox" name="cb_ids[]" value="<?php echo $print_order['print_order_id']; ?>" /></td>
					<td><?php echo anchor( 'print_orders/brief/' . $print_order['print_order_id'], $print_order['print_order'], array( 'class' => 'brief', 'title' => __( "Open Brief" ) ) ); ?></td>
					<td><?php echo $pos['name']; ?></td>
					<td class="text-right"><?php echo ( ! empty( $print_order['original']['number_start'] ) ) ? my_number_format( $print_order['number_start'] ) : $print_order['number_start']; ?></td>
					<td class="text-right"><?php echo ( ! empty( $print_order['original']['number_end'] ) ) ? my_number_format( $print_order['number_end'] ) : $print_order['number_end']; ?></td>
					<td class="text-right"><?php echo my_number_format( $this->print_orders->count_remaining_bills( $print_order ) ); ?></td>
					<td class="text-right d-print-none action-buttons">
					<?php 
					foreach ( $buttons AS $n => $b ) $buttons[$n] = ' ' . $b;
					if ( $this->uri->segment( 2 ) !== "trash" )
					{
						if ( $this->permissions->can_edit() ) echo $buttons['edit'];
						if ( $this->permissions->can_trash() ) echo $buttons['trash'];
					}
					else
					{
						if ( $this->permissions->can_restore() ) echo $buttons['restore']; 
						if ( $this->permissions->can_delete() ) echo $buttons['delete'];
					}
					?>						
					</td>
				</tr>
				<?php endforeach; ?>
			</tbody>
		</table>

	</div>
</div>

<div class="row d-print-none lists-tools">

	<?php if ( count( $bulk_actions['options'] ) > 1 ) : ?>

	<div class="col form-inline">
		<div class="bulk-box">
			<?php echo form_dropdown( $bulk_actions ); ?>
			<?php echo form_submit( $submit_bulk_actions ); ?>
		</div>
	</div>

	<?php endif; if ( isset( $pagination ) ) : ?><div class="col page-navigation"><nav><?php echo $pagination; ?></nav></div><?php endif; ?>

</div>

<?php echo form_close(); else : $this->load->view( 'singles/list-empty' ); endif; ?>
