
<div class="card">
	<div class="card-body">
		<div id="brief-wrapper">

			<div class="row">

				<div class="col-md-6 mb-3">
					<div class="element-group row">
						<div class="col-4"><span class="item-label"><?php _e( "Print Order" ); ?></span></div>
						<div class="col-8"><span class="full-name"><?php echo $item['print_order']; ?></div>
					</div>
					<div class="element-group row">
						<div class="col-4"><span class="item-label" title="<?php _e( "Point of Sale" ); ?>"><?php _e( "POS" ); ?></span></div>
						<div class="col-8"><span><?php echo $pos['name']; ?></div>
					</div>
					<div class="element-group row">
						<div class="col-4"><span class="item-label"><?php _e( "Location" ); ?></span></div>
						<div class="col-8"><span><?php echo $item['location']; ?></div>
					</div>
					<div class="element-group row">
						<div class="col-4"><span class="item-label"><?php _e( "Expedition" ); ?></span></div>
						<div class="col-8"><span><?php echo $item['expedition']; ?></div>
					</div>
				</div>

				<div class="col-md-6 mb-3">
					<div class="element-group row">
						<div class="col-4"><span class="item-label"><?php _e( "Date start" ); ?></span></div>
						<div class="col-8"><span><?php echo ( ! empty( $item['original']['date_start'] ) ) ? date( $this->assets->conf['date_format'], strtotime( $item['date_start'] ) ) : $item['date_start']; ?></div>
					</div>
					<div class="element-group row">
						<div class="col-4"><span class="item-label"><?php _e( "Date end" ); ?></span></div>
						<div class="col-8"><span><?php echo ( ! empty( $item['original']['date_end'] ) ) ? date( $this->assets->conf['date_format'], strtotime( $item['date_end'] ) ) : $item['date_end']; ?></div>
					</div>
					<div class="element-group row">
						<div class="col-4"><span class="item-label"><?php _e( "Due" ); ?></span></div>
						<div class="col-8"><span><?php echo ( ! empty( $item['original']['date_end'] ) ) ? timespan( time(), strtotime( $item['date_end'] ), 2 ) : $item['date_end']; ?></div>
					</div>
					<div class="element-group row">
						<div class="col-4"><span class="item-label"><?php _e( "Number start" ); ?></span></div>
						<div class="col-8"><span><?php echo ( ! empty( $item['original']['number_start'] ) ) ? my_number_format( $item['number_start'] ) : $item['number_start']; ?></div>
					</div>
					<div class="element-group row">
						<div class="col-4"><span class="item-label"><?php _e( "Number end" ); ?></span></div>
						<div class="col-8"><span><?php echo ( ! empty( $item['original']['number_end'] ) ) ? my_number_format( $item['number_end'] ) : $item['number_end']; ?></div>
					</div>
					<div class="element-group row">
						<div class="col-4"><span class="item-label"><?php _e( "Remaining" ); ?></span></div>
						<div class="col-8"><span><?php echo my_number_format( $this->print_orders->count_remaining_bills( $item ) ); ?></div>
					</div>
				</div>
			</div>

			<?php if ( ! empty( $item['created'] ) OR ! empty( $item['modified'] ) ) : ?>
			<div class="row last-activity">
				<div class="col">
					<div class="created">
						<?php if ( ! empty( $item['created'] ) ) : ?>
						<p class="label"><?php _e( "Created by" ); ?></p>
						<p class="value"><?php echo $this->ion_auth->user_name( "fullname", $item['creator'] ) ?> <small><?php echo date( $this->assets->conf['datetime_format'], strtotime( $item['created'] ) ); ?></small></p>
						<?php endif; ?>
					</div>
				</div>
				<div class="col">
					<div class="modified">
						<?php if ( ! empty( $item['modified'] ) ) : ?>
						<p class="label"><?php _e( "Last modified by" ); ?></p>
						<p class="value"><?php echo $this->ion_auth->user_name( "fullname", $item['modifier'] ) ?> <small><?php echo date( $this->assets->conf['datetime_format'], strtotime( $item['modified'] ) ); ?></small></p>
						<?php endif; ?>
					</div>
				</div>
			</div>
			<?php endif; ?>
			
		</div>
	</div>
</div>