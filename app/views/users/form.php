
<?php echo form_open( current_url(), array( 'autocomplete' => 'off', 'id' => 'form-users' ) ); ?>

<div class="row form-elements">

	<div class="col-lg-8">
		<div class="card" id="personal-card">
			<div class="card-body">
				<h4><?php _e( "Personal Information" ); ?></h4>
				<div class="form-row">
					<div class="form-group col-md-6">
						<?php echo form_label( __( "First name" ), 'first_name', array( 'class' => 'control-label' ) ); ?>
						<span class="mandatory">*</span>
						<?php echo form_input( $first_name ); echo form_error( 'first_name' ); ?>
					</div>
					<div class="form-group col-md-6">
						<?php echo form_label( __( "Last name" ), 'last_name', array( 'class' => 'control-label' ) ); ?>
						<span class="mandatory">*</span>
						<?php echo form_input( $last_name ); echo form_error( 'last_name' ); ?>
					</div>
				</div>
				<div class="form-row">
					<div class="form-group col-md-6">
						<?php echo form_label( __( "Language" ), 'language', array( 'class' => 'control-label' ) ); ?>
						<span class="mandatory">*</span>
						<?php echo form_dropdown( $language ); echo form_error( 'language' ); ?>
					</div>
				</div>
			</div>
		</div>

		<div class="card" id="system-card">
			<div class="card-body">
				<h4><?php _e( "System Information" ); ?></h4>
				<?php if ( $this->uri->segment( 2 ) == "edit" ) : ?>
				<div class="alert alert-warning">
					<h5 class="mb-0"><?php _e( "Warning" ); ?></h5>
					<p class="mb-0"><?php _e( "Complete the passwords fields ONLY if you want to change the user's old password. Otherwise, leave it blank." ); ?></p>
				</div>
				<?php endif; ?>
				<div class="form-row">
					<div class="form-group col-md-6">
						<?php echo form_label( __( "Username" ), 'username', array( 'class' => 'control-label' ) ); ?>
						<span class="mandatory">*</span>
						<?php echo form_input( $username ); echo form_error( 'username' ); ?>
					</div>
					<div class="form-group col-md-6 d-flex align-items-center justify-content-center mh-100">
						<div class="alert alert-info py-2 text-center mb-0">
							<small>
								<p class="mb-0"><?php printf( __( "Username min chars: %d" ), $this->config->item( 'min_password_length', 'ion_auth' ) ); ?> | <?php printf( __( "Username max chars: %d" ), $this->config->item( 'max_password_length', 'ion_auth' ) ); ?></p>
								<p class="mb-0"><?php printf( __( "Password min chars: %d" ), $this->config->item( 'min_password_length', 'ion_auth' ) ); ?> | <?php printf( __( "Password max chars: %d" ), $this->config->item( 'max_password_length', 'ion_auth' ) ); ?></p>
							</small>
						</div>
					</div>
				</div>
				<div class="form-row">
					<div class="form-group col-md-6">
						<?php if ( $this->uri->segment( 2 ) == "add" ) : echo form_label( __( "Password" ), 'password', array( 'class' => 'control-label' ) ); ?> <span class="mandatory">*</span><?php else : echo form_label( __( "New password" ), 'password', array( 'class' => 'control-label' ) ); endif; ?>
						<?php echo form_input( $password ); echo form_error( 'password' ); ?>
					</div>
					<div class="form-group col-md-6">
						<?php if ( $this->uri->segment( 2 ) == "add" ) : echo form_label( __( "Repeat password" ), 'password_repeat', array( 'class' => 'control-label' ) ); ?> <span class="mandatory">*</span><?php else : echo form_label( __( "Repeat new password" ), 'password_repeat', array( 'class' => 'control-label' ) ); endif; ?>
						<?php echo form_input( $password_repeat ); echo form_error( 'password_repeat' ); ?>
					</div>
				</div>
				<div class="form-row">
					<div class="form-group col">
						<?php echo form_label( __( "User level" ), 'user_level', array( 'class' => 'control-label' ) ); ?>
						<?php if ( $this->uri->segment( 2 ) == "edit" ) : ?><span class="mandatory">*</span><?php endif; ?>
						<div class="checkbox-group">
							<?php foreach ( $groups AS $group ) : if ( ! $this->ion_auth->in_group( array( 'superadmin' ) ) AND $group['name'] == 'superadmin' ) continue; ?>
							<div class="form-check">
								<label class="form-check-label" for="group_<?php echo $group['name']; ?>">
									<?php $checked = ""; if ( isset( $selected_groups ) ) : foreach ( $selected_groups AS $sg ) : if ( $group['id'] == $sg->id ) $checked = 'checked="checked" '; endforeach; endif; ?>
									<input type="checkbox" name="groups[]" id="group_<?php echo $group['name']; ?>" class="form-check-input" value="<?php echo $group['id']; ?>" <?php echo $checked; ?> />
									<?php echo $group['description']; ?>
									<small class="text-info">(<?php echo $group['name']; ?>)</small>
								</label>
							</div>
							<?php endforeach; ?>
						</div>
						<?php echo form_error( 'groups[]' ); ?>
					</div>
				</div>
			</div>
		</div>
	</div>

	<div class="col-lg-4">
		<div class="card" id="picture-card">
			<div class="card-body">
				<h4><?php _e( "Picture" ); ?></h4>
				<?php $this->load->view( 'files/model-single-picture', ( isset( $user_id ) ) ? $this->files->get_files_src( $user_id['value'], 'users' ) : NULL ); ?>
			</div>
		</div>

		<div class="card" id="contact-card">
			<div class="card-body">
				<h4><?php _e( "Contact Information" ); ?></h4>
				<div class="form-row">
					<div class="form-group col">
						<?php echo form_label( __( "Email" ), 'email', array( 'class' => 'control-label' ) ); ?>
						<span class="mandatory">*</span>
						<?php echo form_input( $email ); echo form_error( 'email' ); ?>
					</div>
				</div>
				<div class="form-row">
					<div class="form-group col">
						<div class="form-check">
							<label for="send" class="form-check-label">
								<?php echo form_input( $send ); ?>
								<?php _e( "Send login credentials to user's email" ); ?>
							</label>
						</div>
						<?php echo form_error( 'send' ); ?>						
					</div>
				</div>
				<div class="form-row">
					<div class="form-group col">
						<?php echo form_label( __( "Phone" ), 'phone', array( 'class' => 'control-label' ) ); ?>
						<?php echo form_input( $phone ); echo form_error( 'phone' ); ?>
					</div>
				</div>
			</div>
		</div>
	</div>

</div>

<div class="buttons-set">
	<button type="submit" class="btn btn-primary">
		<i class="fa fa-save"></i>
		<?php _e( "Save" ); ?>
	</button>
	<?php if ( $this->uri->segment( 2 ) == "edit" ) echo anchor( 'users/add', '<i class="fa fa-plus-circle"></i> ' . __( "New" ), array( 'class' => 'btn btn-secondary', 'title' => __( "Add New User" ) ) ); ?>
</div>

<?php if ( ! empty( $user_id ) ) echo form_input( $user_id ); ?>
<?php echo form_close(); ?>