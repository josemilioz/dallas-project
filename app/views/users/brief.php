
<div class="card">
	<div class="card-body">
		<div id="brief-wrapper">

			<div class="row">
				<div class="col-lg-5 mb-3">
					<div class="picture-big-container w-100 text-center">
						<img class="picture-big w-75" src="<?php echo $this->ion_auth->avatar( "thumb", $user['id'] ); ?>" />
					</div>
				</div>
				<div class="col-lg-7 mb-3">
					<h4><?php _e( "Personal Information" ); ?></h4>
					<div class="element-group row">
						<div class="col-4"><span class="item-label"><?php _e( "Full name" ); ?></span></div>
						<div class="col-8"><span class="full-name"><?php echo $this->ion_auth->user_name( "fullname", $user ); ?></span></div>
					</div>
					<div class="element-group row">
						<div class="col-4"><span class="item-label"><?php _e( "Username" ); ?></span></div>
						<div class="col-8"><span><code><?php echo $user['username']; ?></code></span></div>
					</div>

					<h4 class="mt-3"><?php _e( "Contact Information" ); ?></h4>
					<div class="element-group row">
						<div class="col-4"><span class="item-label"><?php _e( "Telephone" ); ?></span></div>
						<div class="col-8"><span><?php echo $user['phone']; ?></span></div>
					</div>
					<div class="element-group row">
						<div class="col-4"><span class="item-label"><?php _e( "Email" ); ?></span></div>
						<div class="col-8"><span><?php echo $user['email']; ?></span></div>
					</div>

					<h4 class="mt-3"><?php _e( "System Information" ); ?></h4>
					<div class="element-group row">
						<div class="col-4"><span class="item-label"><?php _e( "Language" ); ?></span></div>
						<div class="col-8"><span><?php echo ( empty( $user['original']['language'] ) ) ? $this->assets->conf['languages'][$user['language']] : $this->assets->conf['languages'][$user['language']]; ?></span></div>
					</div>
					<div class="element-group row">
						<div class="col-4"><span class="item-label"><?php _e( "Last login" ); ?></span></div>
						<div class="col-8">
							<?php if ( ! empty( $user['original']['last_login'] ) ) : ?>
							<span><?php echo date( $this->assets->conf['datetime_format'], $user['last_login'] ); ?></span>
							<small class="text-muted">(<?php printf( __( "%s ago" ), timespan( $user['last_login'] ) ); ?>)</small>
							<?php else : echo $user['last_login']; endif; ?>
						</div>
					</div>
				</div>
			</div>

			<?php if ( ! empty( $user['created'] ) OR ! empty( $user['modified'] ) ) : ?>
			<div class="row last-activity">
				<div class="col">
					<div class="created">
						<?php if ( ! empty( $user['created'] ) ) : ?>
						<p class="label"><?php _e( "Created by" ); ?></p>
						<p class="value"><?php echo $this->ion_auth->user_name( "fullname", $user['creator'] ) ?> <small><?php echo date( $this->assets->conf['datetime_format'], strtotime( $user['created'] ) ); ?></small></p>
						<?php endif; ?>
					</div>
				</div>
				<div class="col">
					<div class="modified">
						<?php if ( ! empty( $user['modified'] ) ) : ?>
						<p class="label"><?php _e( "Last modified by" ); ?></p>
						<p class="value"><?php echo $this->ion_auth->user_name( "fullname", $user['modifier'] ) ?> <small><?php echo date( $this->assets->conf['datetime_format'], strtotime( $user['modified'] ) ); ?></small></p>
						<?php endif; ?>
					</div>
				</div>
			</div>
			<?php endif; ?>
			
		</div>
	</div>
</div>