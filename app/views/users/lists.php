	
<?php if ( ! empty( $users ) ) : echo form_open( current_url() ); ?>

<div class="lists-table">
	<div class="table-responsive">

		<table class="table table-striped table-hover">
			<col width="10" class="d-print-none">
			<col width="40">
			<col width="auto">
			<col width="auto">
			<col width="200">
			<col width="130">
			<col width="100">
			<thead>
				<tr>
					<th class="d-print-none"><input type="checkbox" name="select_all_cb" class="select_all_cb" title="<?php _e( "Select All" ); ?>" /></th>
					<th colspan="2"><?php _e( "Full name" ); ?></th>
					<th><?php _e( "Groups" ); ?></th>
					<th><?php _e( "Last login" ); ?></th>
					<th><?php _e( "Status" ); ?></th>
					<th class="d-print-none"></th>
				</tr>
			</thead>
			<tfoot>
				<tr>
					<th class="d-print-none"><input type="checkbox" name="select_all_cb" class="select_all_cb" title="<?php _e( "Select All" ); ?>" /></th>
					<th colspan="2"><?php _e( "Full name" ); ?></th>
					<th><?php _e( "Groups" ); ?></th>
					<th><?php _e( "Last login" ); ?></th>
					<th><?php _e( "Status" ); ?></th>
					<th class="d-print-none"></th>
				</tr>
			</tfoot>
			<tbody>
				<?php 
				foreach ( $users AS $user ) : 
					$user = $this->assets->fill_empty_vars( $user );
					$groups = $this->ion_auth->get_users_groups( $user['id'] )->result_array();
					$buttons = $this->elements->lists_action_buttons( 'users', 'user_id', $user['id'], $list_buttons_extension );
				?>
				<tr>
					<td class="d-print-none"><input type="checkbox" name="cb_ids[]" value="<?php echo $user['id']; ?>" /></td>
					<td class="user-picture"><img src="<?php echo $this->ion_auth->avatar( 'thumb', $user['id'] ); ?>"></td>
					<td class="text-wrap"><strong><?php echo anchor( 'users/brief/' . $user['id'], $this->ion_auth->user_name( "fullname", $user ), array( 'class' => 'brief', 'title' => __( "Open Brief" ) ) ); ?></strong></td>
					<td><?php echo implode( ", ", array_map( function( $string ){ return $string['description']; }, $groups ) ); ?></td>
					<td><?php echo ( ! empty( $user['original']['last_login'] ) ) ? date( $this->assets->conf['datetime_format'], $user['last_login'] ) : $user['last_login']; ?></td>
					<td><?php echo ( $user['active'] == 1 ) ? '<span class="badge badge-success">' . __( "Active" ) . '</span>' : '<span class="badge badge-danger">' . __( "Inactive" ) . '</span>'; ?></td>
					<td class="text-right d-print-none">
					<?php 
					foreach ( $buttons AS $n => $b ) $buttons[$n] = ' ' . $b;
					if ( $this->uri->segment( 2 ) !== "trash" )
					{
						if ( $this->permissions->can_edit() ) echo $buttons['edit'];
						if ( $this->permissions->can_trash() ) echo $buttons['trash'];
					}
					else
					{
						if ( $this->permissions->can_restore() ) echo $buttons['restore']; 
						if ( $this->permissions->can_delete() ) echo $buttons['delete'];
					}
					?>						
					</td>
				</tr>
				<?php endforeach; ?>
			</tbody>
		</table>

	</div>
</div>

<div class="row d-print-none lists-tools">

	<?php if ( count( $bulk_actions['options'] ) > 1 ) : ?>

	<div class="col form-inline">
		<div class="bulk-box">
			<?php echo form_dropdown( $bulk_actions ); ?>
			<?php echo form_submit( $submit_bulk_actions ); ?>
		</div>
	</div>

	<?php endif; if ( isset( $pagination ) ) : ?><div class="col page-navigation"><nav><?php echo $pagination; ?></nav></div><?php endif; ?>

</div>

<?php echo form_close(); else : $this->load->view( 'singles/list-empty' ); endif; ?>
