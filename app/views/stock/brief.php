
<div class="card">
	<div class="card-body">
		<div id="brief-wrapper">

			<div class="row">
				<div class="col-lg-5 mb-3">
					<div class="picture-big-container w-100 text-center">
						<img class="picture-big w-75" src="<?php echo $this->products->picture( $product['product_id'] ); ?>" />
					</div>
				</div>
				<div class="col-lg-7 mb-3">
					<div class="element-group row">
						<div class="col-4"><span class="item-label"><?php _e( "Product" ); ?></span></div>
						<div class="col-8"><span class="full-name"><?php echo $product['name']; ?></span></div>
					</div>
					<div class="element-group row">
						<div class="col-4"><span class="item-label"><?php _e( "Purchase" ); ?></span></div>
						<div class="col-8"><span><?php echo '#' . $purchase['purchase_id']; ?></span></div>
					</div>
					<div class="element-group row">
						<div class="col-4"><span class="item-label"><?php _e( "Purchase date" ); ?></span></div>
						<div class="col-8"><span><?php echo ( ! empty( $purchase['original']['purchase_date'] ) ) ? date( $this->assets->conf['date_format'], strtotime( $purchase['purchase_date'] ) ) : $purchase['purchase_date']; ?></span></div>
					</div>
					<div class="element-group row">
						<div class="col-4"><span class="item-label"><?php _e( "Purchase delivery" ); ?></span></div>
						<div class="col-8"><span><?php echo ( ! empty( $purchase['original']['delivery_date'] ) ) ? date( $this->assets->conf['date_format'], strtotime( $purchase['delivery_date'] ) ) : $purchase['delivery_date']; ?></span></div>
					</div>
					<div class="element-group row">
						<div class="col-4"><span class="item-label"><?php _e( "Initial quantity" ); ?></span></div>
						<div class="col-8"><span><?php echo ( ! empty( $stock['original']['init_quantity'] ) ) ? sprintf( "%s %s", '<small class="text-muted">' . $this->assets->conf['units'][$product['unit_measure']] . '</small>', my_number_format( $stock['init_quantity'], $this->assets->conf['qty_decimals'] ) ) : $stock['init_quantity']; ?></span></div>
					</div>
					<div class="element-group row">
						<div class="col-4"><span class="item-label"><?php _e( "Quantity" ); ?></span></div>
						<div class="col-8"><span><?php echo ( ! empty( $stock['original']['quantity'] ) ) ? sprintf( "%s %s", '<small class="text-muted">' . $this->assets->conf['units'][$product['unit_measure']] . '</small>', my_number_format( $stock['quantity'], $this->assets->conf['qty_decimals'] ) ) : $stock['quantity']; ?></span></div>
					</div>
					<div class="element-group row">
						<div class="col-4"><span class="item-label"><?php _e( "Purchase price" ); ?></span></div>
						<div class="col-8"><span><?php echo ( ! empty( $stock['original']['purchase_price'] ) ) ? sprintf( "%s %s", '<small class="text-muted">' . $this->assets->conf['currencies'][$this->assets->conf['currency']] . '</small>', my_number_format( $stock['purchase_price'], $this->assets->conf['decimals'] ) ) : $stock['purchase_price']; ?></span></div>
					</div>
					<div class="element-group row">
						<div class="col-4"><span class="item-label"><?php _e( "Sale price" ); ?></span></div>
						<div class="col-8"><span><?php echo ( ! empty( $stock['original']['unit_price'] ) ) ? sprintf( "%s %s", '<small class="text-muted">' . $this->assets->conf['currencies'][$this->assets->conf['currency']] . '</small>', my_number_format( $stock['unit_price'], $this->assets->conf['decimals'] ) ) : $stock['unit_price']; ?></span></div>
					</div>
					<div class="element-group row">
						<div class="col-4"><span class="item-label"><?php _e( "Expiration date" ); ?></span></div>
						<div class="col-8">
							<span><?php echo ( ! empty( $stock['original']['expiration_date'] ) ) ? date( $this->assets->conf['date_format'], strtotime( $stock['expiration_date'] ) ) : $stock['expiration_date']; ?></span>
							<?php if ( ! empty( $stock['original']['expiration_date'] ) ) : ?><small class="text-muted">(<?php echo timespan( time(), strtotime( $stock['expiration_date'] ), 2 ); ?>)</small><?php endif; ?>
						</div>
					</div>
				</div>
			</div>

			<div class="row">
				<div class="col"><h4><?php _e( "Estimates" ); ?></h4></div>
			</div>

			<div class="row">
				<div class="col-lg-6 mb-3">
					<div class="element-group row">
						<div class="col-6"><span class="item-label"><?php _e( "Purchased quantity" ); ?></span></div>
						<div class="col-6"><span><?php printf( "%s %s", '<small class="text-muted">' . $this->assets->conf['units'][$product['unit_measure']] . '</small>', my_number_format( $purchased_quantity, $this->assets->conf['qty_decimals'] ) ); ?></span></div>
					</div>
					<div class="element-group row">
						<div class="col-6"><span class="item-label"><?php _e( "Investment" ); ?></span></div>
						<div class="col-6"><span><?php printf( "%s %s", '<small class="text-muted">' . $this->assets->conf['currencies'][$this->assets->conf['currency']] . '</small>', my_number_format( $investment, $this->assets->conf['decimals'] ) ); ?></span></div>
					</div>
				</div>
				<div class="col-lg-6 mb-3">
					<div class="element-group row">
						<div class="col-6"><span class="item-label"><?php _e( "Total sales" ); ?></span></div>
						<div class="col-6"><span><?php printf( "%s %s", '<small class="text-muted">' . $this->assets->conf['currencies'][$this->assets->conf['currency']] . '</small>', my_number_format( $sales, $this->assets->conf['decimals'] ) ); ?></span></div>
					</div>
					<div class="element-group row">
						<div class="col-6"><span class="item-label"><?php _e( "Profit" ); ?></span></div>
						<div class="col-6"><span><?php printf( "%s %s", '<small class="text-muted">' . $this->assets->conf['currencies'][$this->assets->conf['currency']] . '</small>', my_number_format( $profit, $this->assets->conf['decimals'] ) ); ?></span></div>
					</div>
					<div class="element-group row">
						<div class="col-6"><span class="item-label"><?php _e( "Profit over sales" ); ?></span></div>
						<div class="col-6"><span><?php echo my_number_format( $percentage, $this->assets->conf['qty_decimals'] ); ?>%</span></div>
					</div>
				</div>
			</div>

			<?php if ( ! empty( $stock['created'] ) OR ! empty( $stock['modified'] ) ) : ?>
			<div class="row last-activity">
				<div class="col">
					<div class="created">
						<?php if ( ! empty( $stock['created'] ) ) : ?>
						<p class="label"><?php _e( "Created by" ); ?></p>
						<p class="value"><?php echo $this->ion_auth->user_name( "fullname", $stock['creator'] ) ?> <small><?php echo date( $this->assets->conf['datetime_format'], strtotime( $stock['created'] ) ); ?></small></p>
						<?php endif; ?>
					</div>
				</div>
				<div class="col">
					<div class="modified">
						<?php if ( ! empty( $stock['modified'] ) ) : ?>
						<p class="label"><?php _e( "Last modified by" ); ?></p>
						<p class="value"><?php echo $this->ion_auth->user_name( "fullname", $stock['modifier'] ) ?> <small><?php echo date( $this->assets->conf['datetime_format'], strtotime( $stock['modified'] ) ); ?></small></p>
						<?php endif; ?>
					</div>
				</div>
			</div>
			<?php endif; ?>
			
		</div>
	</div>
</div>