<!-- ##################################### EDIT PRODUCT MODAL ###################################### -->

<div class="modal fade d-print-none modal-default" id="modal-edit-product" tabindex="-1" role="dialog">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title"><?php _e( "Edit Product" ); ?></h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="<?php _e( "Close" ); ?>"><span aria-hidden="true">&times;</span></button>
			</div>
			<table class="table table-striped">
				<col width="180">
				<col width="auto">
				<tbody>
					<tr>
						<td><?php _e( "Product" ); ?></td>
						<td class="fullname"></td>
					</tr>
					<tr>
						<td><?php _e( "SKU" ); ?></td>
						<td></td>
					</tr>
					<tr>
						<td><?php _e( "Initial quantity" ); ?></td>
						<td></td>
					</tr>
					<tr>
						<td><?php _e( "Purchase price" ); ?></td>
						<td></td>
					</tr>
				</tbody>
			</table>
			<div class="modal-body">
				<div class="form-row">
					<div class="form-group col-md-6" id="modal_quantity_box">
						<?php echo form_label( __( "Quantity" ), 'modal_quantity', array( 'class' => 'control-label' ) ); ?>
						<span class="mandatory">*</span>
						<div class="input-group">
							<?php echo form_input( $modal_quantity ); ?>
							<div class="input-group-append">
								<span class="input-group-text"></span>
							</div>
						</div>
						<?php echo form_error( 'modal_quantity' ); ?>
					</div>
					<div class="form-group col-md-6" id="modal_price_box">
						<?php echo form_label( __( "Unit sale price" ), 'modal_price', array( 'class' => 'control-label' ) ); ?>
						<span class="mandatory">*</span>
						<div class="input-group">
							<?php echo form_input( $modal_price ); ?>
							<div class="input-group-append">
								<span class="input-group-text"></span>
							</div>
						</div>
						<?php echo form_error( 'modal_price' ); ?>
					</div>
				</div>
				<div class="form-row">
					<div class="form-group col-md-6" id="modal_expiration_date_box">
						<?php echo form_label( __( "Expiration date" ), 'modal_expiration_date', array( 'class' => 'control-label' ) ); ?>
						<?php echo form_input( $modal_expiration_date ); echo form_error( 'modal_expiration_date' ); ?>
					</div>
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-purple btn-sm" id="confirm-edit-product"><?php _e( "Edit" ); ?></button>
				<button type="button" class="btn btn-secondary btn-sm" data-dismiss="modal"><?php _e( "Cancel" ); ?></button>
			</div>
		</div>
	</div>
</div>