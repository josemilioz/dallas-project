
<div class="row form-elements">

	<div class="col-lg-6">
		<div class="card" id="data-card">
			<table class="table table-striped mb-0">
				<col width="160">
				<col width="auto">
				<tbody>
					<tr>
						<td><?php _e( "Provider" ); ?></td>
						<td><?php echo $purchase['provider']; ?></td>
					</tr>
					<tr>
						<td><?php _e( "Observations" ); ?></td>
						<td><?php echo nl2br( $purchase['observations'] ); ?></td>
					</tr>
					<tr>
						<td><?php _e( "Purchase date" ); ?></td>
						<td><?php echo ( ! empty( $purchase['original']['purchase_date'] ) ) ? date( $this->assets->conf['date_format'], strtotime( $purchase['purchase_date'] ) ) : $purchase['purchase_date']; ?></td>
					</tr>
					<tr>
						<td><?php _e( "Delivery date" ); ?></td>
						<td><?php echo ( ! empty( $purchase['original']['delivery_date'] ) ) ? date( $this->assets->conf['date_format'], strtotime( $purchase['delivery_date'] ) ) : $purchase['delivery_date']; ?></td>
					</tr>
					<tr>
						<td><?php _e( "Due date" ); ?></td>
						<td><?php echo ( ! empty( $purchase['original']['expiration_date'] ) ) ? date( $this->assets->conf['date_format'], strtotime( $purchase['expiration_date'] ) ) : $purchase['expiration_date']; ?></td>
					</tr>
					<tr>
						<td><?php _e( "Bill number" ); ?></td>
						<td><?php echo $purchase['bill_number']; ?></td>
					</tr>
				</tbody>
			</table>
		</div>
	</div>

	<div class="col-lg-6">
		<div class="card" id="steps-card">
			<div class="card-body">
				<h4 class="mb-0"><?php _e( "Steps" ); ?></h4>
			</div>
			<ol class="list-group list-group-flush">
				<li class="list-group-item"><?php _e( "Insert the SKU for the desired product, according to the order of the list." ); ?></li>
				<li class="list-group-item"><?php _e( "Wait until the system checks if the product has a remaining stock." ); ?></li>
				<li class="list-group-item"><?php _e( "Add the sale price and expiration date by pressing the 'Edit' button." ); ?></li>
				<li class="list-group-item"><?php _e( "If you need to add another SKU for the same product, press 'Duplicate'." ); ?></li>
				<li class="list-group-item"><?php _e( "If you're good to go, press 'Store' to stock the product." ); ?></li>
			</ol>
		</div>
	</div>

	<?php if ( ! empty( $meta ) ) : ?>

	<div class="col-12">
		<div class="card" id="products-card">
			<div class="card-body">
				<h4 class="mb-0"><?php _e( "Product list" ); ?></h4>
			</div>
			<div class="table-responsive">
				<table class="table table-striped table-hover product-list">
					<col width="auto">
					<col width="180">
					<col width="120">
					<col width="120">
					<col width="120">
					<col width="120">
					<col width="100">
					<col width="110">
					<thead>
						<tr>
							<th><?php _e( "Product" ); ?></th>
							<th><abbr title="<?php _e( "Storage keeping unit" ); ?>"><?php _e( "SKU" ); ?></abbr> <span class="mandatory">*</span></th>
							<th><abbr title="<?php _e( "Initial quantity" ); ?>"><?php _e( "Init. Qty." ); ?></abbr></th>
							<th><abbr title="<?php _e( "Purchase price per unit" ); ?>"><?php _e( "P. Price" ); ?></abbr> <span class="mandatory">*</span></th>
							<th><abbr title="<?php _e( "Quantity" ); ?>"><?php _e( "Quantity" ); ?></abbr> <span class="mandatory">*</span></th>
							<th><abbr title="<?php _e( "Sale price per unit" ); ?>"><?php _e( "S. Price" ); ?></abbr> <span class="mandatory">*</span></th>
							<th><abbr title="<?php _e( "Expiration date" ); ?>"><?php _e( "Exp. Date" ); ?></abbr></th>
							<th class="text-right"></th>
						</tr>
					</thead>
					<tfoot>
						<tr>
							<th><?php _e( "Product" ); ?></th>
							<th><abbr title="<?php _e( "Storage keeping unit" ); ?>"><?php _e( "SKU" ); ?></abbr> <span class="mandatory">*</span></th>
							<th><abbr title="<?php _e( "Initial quantity" ); ?>"><?php _e( "Init. Qty." ); ?></abbr></th>
							<th><abbr title="<?php _e( "Purchase price per unit" ); ?>"><?php _e( "P. Price" ); ?></abbr> <span class="mandatory">*</span></th>
							<th><abbr title="<?php _e( "Quantity" ); ?>"><?php _e( "Quantity" ); ?></abbr> <span class="mandatory">*</span></th>
							<th><abbr title="<?php _e( "Sale price per unit" ); ?>"><?php _e( "S. Price" ); ?></abbr> <span class="mandatory">*</span></th>
							<th><abbr title="<?php _e( "Expiration date" ); ?>"><?php _e( "Exp. Date" ); ?></abbr></th>
							<th class="text-right"></th>
						</tr>
					</tfoot>
					<tbody>
						<?php 
						foreach ( $meta AS $index => $row ) : 
							$index++;
							$row = $this->assets->fill_empty_vars( $row, 'purchases_products' ); 
							$product = $this->assets->fill_empty_vars( $this->products->get( $row['product_id'] ), 'products' ); 
						?>
						<tr id="row-<?php echo $index; ?>" data-row-original="true">
							<td class="align-middle text-wrap"><?php echo ( ! empty( $row['original']['description'] ) ) ? $row['description'] : $product['name']; ?></td>
							<td>
								<input type="text" name="sku[]" class="form-control input-sku" autocomplete="off" />
								<?php echo form_error( 'sku' ); ?>
							</td>
							<td class="align-middle text-wrap"></td>
							<td class="align-middle text-wrap">
								<small class="text-muted"><?php echo $this->assets->conf['currencies'][$this->assets->conf['currency']]; ?></small>
								<?php echo my_number_format( $row['unit_price'], $this->assets->conf['decimals'] ); ?>
							</td>
							<td class="align-middle text-wrap">
								<small class="text-muted"><?php echo $this->assets->conf['units'][$product['unit_measure']]; ?></small>
								<?php echo my_number_format( $row['quantity'], $this->assets->conf['qty_decimals'] ); ?>
							</td>
							<td class="align-middle text-wrap">
								<small class="text-muted"><?php echo $this->assets->conf['currencies'][$this->assets->conf['currency']]; ?></small>
								<?php echo my_number_format( $product['price'], $this->assets->conf['decimals'] ); ?>								
							</td>
							<td class="align-middle"></td>
							<td class="text-right action-buttons align-middle">
							    <button class="btn btn-secondary dropdown-toggle" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" disabled="disabled" title="<?php _e( "Options" ); ?>">
							    	<i class="fa fa-cog"></i>
							    </button>
								<div class="dropdown-menu dropdown-menu-right">
									<button type="button" class="dropdown-item btn-edit" disabled="disabled">
										<i class="fa fa-pencil-alt mr-2" style="width: 1em"></i>
										<?php _e( "Edit" ); ?>
									</button>
									<button type="button" class="dropdown-item btn-duplicate" disabled="disabled">
										<i class="fa fa-copy mr-2 text-warning" style="width: 1em"></i>
										<?php _e( "Duplicate" ); ?>
									</button>
									<button type="button" class="dropdown-item btn-rebuild" disabled="disabled">
										<i class="fa fa-redo-alt mr-2 text-info" style="width: 1em"></i>
										<?php _e( "Restore" ); ?>
									</button>
									<button type="button" class="dropdown-item btn-erase" disabled="disabled">
										<i class="fa fa-times mr-2 text-danger" style="width: 1em"></i>
										<?php _e( "Delete" ); ?>
									</button>
								</div>
								<button type="button" class="btn btn-purple btn-store" title="<?php _e( "Store" ); ?>" disabled="disabled">
									<i class="fa fa-save"></i>
								</button>

								<!-- TO SET WITH SKU VIA AJAX REQUEST -->
								<input type="hidden" name="init_quantity[]" 	class="input-init-quantity" />

								<!-- TO SET WITH MODAL -->
								<input type="hidden" name="expiration_date[]" 	class="input-expiration-date" />

								<!-- ALREADY SET -->
								<input type="hidden" name="purchase_meta_id[]" 		value="<?php echo $row['purchase_meta_id']; ?>"				class="input-purchase-meta-id" />
								<input type="hidden" name="product_id[]" 			value="<?php echo $row['product_id']; ?>"					class="input-product-id" />
								<input type="hidden" name="purchase_quantity[]" 	value="<?php echo $row['quantity']; ?>"						class="input-purchase-quantity" />
								<input type="hidden" name="purchase_price[]" 		value="<?php echo $row['unit_price']; ?>"					class="input-purchase-price" />
								<input type="hidden" name="quantity[]" 				value="<?php echo $row['quantity']; ?>"						class="input-quantity" />
								<input type="hidden" name="unit_price[]" 			value="<?php echo $product['price']; ?>" 					class="input-unit-price" />
								<input type="hidden" name="product_currency[]" 		value="<?php echo $this->assets->conf['currency']; ?>" 		class="input-product-currency" />
								<input type="hidden" name="product_unit_measure[]" 	value="<?php echo $product['unit_measure']; ?>"				class="input-product-unit-measure" />
							</td>
						</tr>
						<?php endforeach; ?>
					</tbody>
				</table>
			</div>
		</div>
	</div>

	<?php else : $this->load->view( 'singles/list-empty' ); endif; ?>

</div>

<?php if ( ! empty( $purchase_id ) ) echo form_input( $purchase_id ); ?>

<?php $this->load->view( 'stock/modal-edit-product' ); ?>
