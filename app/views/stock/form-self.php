
<?php echo form_open( current_url(), array( 'autocomplete' => 'off', 'id' => 'form-stock' ) ); ?>

<div class="row form-elements">

	<div class="col-lg-5">
		<div class="card" id="data-card">
			<div class="card-body">
				<div class="form-row">
					<div class="form-group col">
						<?php echo form_label( __( "Product" ), 'product_id', array( 'class' => 'control-label' ) ); ?>
						<span class="mandatory">*</span>
						<?php echo form_dropdown( $product_id ); echo form_error( 'product_id' ); ?>
						<div class="text-right">
							<button type="button" id="open-new-product-modal" class="btn btn-link"><?php _e( "Add new product" ); ?></button>
						</div>
					</div>
				</div>
				<div class="form-row">
					<div class="form-group col">
						<?php echo form_label( __( "SKU" ), 'sku', array( 'class' => 'control-label', 'title' => __( "Storage keeping unit" ) ) ); ?>
						<span class="mandatory">*</span>
						<?php echo form_input( $sku ); echo form_error( 'sku' ); ?>
					</div>					
				</div>
			</div>
		</div>
	</div>

	<div class="col-lg-7">
		<div class="card" id="editable-card">
			<div class="card-body">
				<div class="form-row">
					<div class="form-group col-md-6">
						<?php echo form_label( __( "Sale price" ), 'unit_price', array( 'class' => 'control-label' ) ); ?>
						<span class="mandatory">*</span>
						<div class="input-group">
							<?php echo form_input( $unit_price ); ?>
							<div class="input-group-append">
								<span class="input-group-text"><?php echo $this->assets->conf['currencies'][$this->assets->conf['currency']]; ?></span>
							</div>
						</div>
						<?php echo form_error( 'unit_price' ); ?>
					</div>
					<div class="form-group col-md-6">
						<?php echo form_label( __( "Quantity" ), 'quantity', array( 'class' => 'control-label' ) ); ?>
						<span class="mandatory">*</span>
						<div class="input-group">
							<?php echo form_input( $quantity ); ?>
							<div class="input-group-append">
								<span class="input-group-text"></span>
							</div>
						</div>
						<?php echo form_error( 'quantity' ); ?>
					</div>					
				</div>
				<div class="form-row">
					<div class="form-group col-md-6">
						<?php echo form_label( __( "Initial quantity" ), 'init_quantity', array( 'class' => 'control-label' ) ); ?>
						<div class="input-group">
							<?php echo form_input( $init_quantity ); ?>
							<div class="input-group-append">
								<span class="input-group-text"></span>
							</div>
						</div>
						<?php echo form_error( 'init_quantity' ); ?>
					</div>
					<div class="form-group col-md-6">
						<?php echo form_label( __( "Expiration date" ), 'expiration_date', array( 'class' => 'control-label' ) ); ?>
						<?php echo form_input( $expiration_date ); echo form_error( 'expiration_date' ); ?>
					</div>
				</div>
			</div>
		</div>
	</div>

</div>

<div class="buttons-set">
	<button type="submit" class="btn btn-primary">
		<i class="fa fa-save"></i>
		<?php _e( "Save" ); ?>
	</button>
</div>

<?php if ( ! empty( $stock_id ) ) echo form_input( $stock_id ); ?>
<?php echo form_close(); ?>
<?php $this->load->view( 'singles/modal-add-new-product' ); ?>