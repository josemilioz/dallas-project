	
<?php if ( ! empty( $stock ) ) : echo form_open( current_url() ); ?>

<div class="lists-table">
	<div class="table-responsive">

		<table class="table table-striped table-hover">
			<col width="10" class="d-print-none">
			<col width="auto">
			<col width="auto">
			<col width="auto">
			<col width="auto">
			<col width="auto">
			<col width="auto">
			<col width="auto">
			<col width="auto">
			<col width="110">
			<thead>
				<tr>
					<th class="d-print-none"><input type="checkbox" name="select_all_cb" class="select_all_cb" title="<?php _e( "Select All" ); ?>" /></th>
					<th><abbr title="<?php _e( "Storage keeping unit" ); ?>"><?php _e( "SKU" ); ?></abbr></th>
					<th class="d-none d-lg-table-cell"><?php _e( "Purchase" ); ?></th>
					<th><?php _e( "Product" ); ?></th>
					<th><?php _e( "Quantity" ); ?></th>
					<th class="d-none d-lg-table-cell"><abbr title="<?php _e( "Initial quantity" ); ?>"><?php _e( "Init. Qty." ); ?></abbr></th>
					<th class="d-none d-lg-table-cell"><abbr title="<?php _e( "Purchase price" ); ?>"><?php _e( "P. Price" ); ?></abbr></th>
					<th class="d-none d-md-table-cell"><abbr title="<?php _e( "Sale price" ); ?>"><?php _e( "S. Price" ); ?></abbr></th>
					<th class="d-none d-lg-table-cell"><abbr title="<?php _e( "Expiration date" ); ?>"><?php _e( "Expiration" ); ?></abbr></th>
					<th class="d-print-none"></th>
				</tr>
			</thead>
			<tfoot>
				<tr>
					<th class="d-print-none"><input type="checkbox" name="select_all_cb" class="select_all_cb" title="<?php _e( "Select All" ); ?>" /></th>
					<th><abbr title="<?php _e( "Storage keeping unit" ); ?>"><?php _e( "SKU" ); ?></abbr></th>
					<th class="d-none d-lg-table-cell"><?php _e( "Purchase" ); ?></th>
					<th><?php _e( "Product" ); ?></th>
					<th><?php _e( "Quantity" ); ?></th>
					<th class="d-none d-lg-table-cell"><abbr title="<?php _e( "Initial quantity" ); ?>"><?php _e( "Init. Qty." ); ?></abbr></th>
					<th class="d-none d-lg-table-cell"><abbr title="<?php _e( "Purchase price" ); ?>"><?php _e( "P. Price" ); ?></abbr></th>
					<th class="d-none d-md-table-cell"><abbr title="<?php _e( "Sale price" ); ?>"><?php _e( "S. Price" ); ?></abbr></th>
					<th class="d-none d-lg-table-cell"><abbr title="<?php _e( "Expiration date" ); ?>"><?php _e( "Expiration" ); ?></abbr></th>
					<th class="d-print-none"></th>
				</tr>
			</tfoot>
			<tbody>
				<?php 
				foreach ( $stock AS $row ) : 
					$row = $this->assets->fill_empty_vars( $row, 'stock' );
					$product = $this->assets->fill_empty_vars( $this->products->get( $row['product_id'] ), 'products' );
					$buttons = $this->elements->lists_action_buttons( 'stock', 'stock_id', $row['stock_id'], $list_buttons_extension );
					$row_class = "";
					if ( $product['min_stock_warning'] >= $row['original']['quantity'] ) $row_class = ' class="table-warning" title="' . __( "Attention! Ending" ) . '"';
					if ( $product['min_stock_danger'] >= $row['original']['quantity'] ) $row_class = ' class="table-danger" title="' . __( "WARNING! ENDING" ) . '"';
				?>
				<tr<?php echo $row_class; ?>>
					<td class="d-print-none"><input type="checkbox" name="cb_ids[]" value="<?php echo $row['stock_id']; ?>" /></td>
					<td><?php echo anchor( 'stock/brief/' . $row['stock_id'], $row['sku'], array( 'class' => 'brief', 'title' => __( "Open Brief" ) ) ); ?></td>
					<td class="d-none d-lg-table-cell"><?php echo ( $row['original']['purchase_id'] ) ? anchor( 'purchases/brief/' . $row['purchase_id'], '#' . $row['purchase_id'], array( 'class' => 'brief', 'data-enlarge-modal' => 1, 'title' => __( "Open Brief" ) ) ) : $row['purchase_id']; ?></td>
					<td><?php echo anchor( 'products/brief/' . $row['product_id'], $product['name'], array( 'class' => 'brief', 'title' => __( "Open Brief" ) ) ); ?></td>
					<td><?php echo ( ! empty( $row['original']['quantity'] ) ) ? sprintf( "%s %s", '<small class="text-muted">' . $this->assets->conf['units'][$product['unit_measure']] . '</small>', my_number_format( $row['quantity'], $this->assets->conf['qty_decimals'] ) ) : $row['quantity']; ?></td>
					<td class="d-none d-lg-table-cell"><?php echo ( ! empty( $row['original']['init_quantity'] ) ) ? sprintf( "%s %s", '<small class="text-muted">' . $this->assets->conf['units'][$product['unit_measure']] . '</small>', my_number_format( $row['init_quantity'], $this->assets->conf['qty_decimals'] ) ) : $row['init_quantity']; ?></td>
					<td class="d-none d-lg-table-cell"><?php echo ( ! empty( $row['original']['purchase_price'] ) ) ? sprintf( "%s %s", '<small class="text-muted">' . $this->assets->conf['currencies'][$this->assets->conf['currency']] . '</small>', my_number_format( $row['purchase_price'] ) ) : $row['purchase_price']; ?></td>
					<td class="d-none d-md-table-cell"><?php echo ( ! empty( $row['original']['unit_price'] ) ) ? sprintf( "%s %s", '<small class="text-muted">' . $this->assets->conf['currencies'][$product['currency']] . '</small>', my_number_format( $row['unit_price'] ) ) : $row['unit_price']; ?></td>
					<td class="d-none d-lg-table-cell"><?php echo ( ! empty( $row['original']['expiration_date'] ) ) ? date( $this->assets->conf['date_format'], strtotime( $row['expiration_date'] ) ) : $row['expiration_date']; ?></td>
					<td class="text-right d-print-none action-buttons">
					<?php 
					foreach ( $buttons AS $n => $b ) $buttons[$n] = ' ' . $b;
					if ( $this->uri->segment( 2 ) !== "trash" )
					{
						if ( $this->permissions->can_edit() ) echo $buttons['edit'];
						if ( $this->permissions->can_trash() ) echo $buttons['trash'];
					}
					else
					{
						if ( $this->permissions->can_restore() ) echo $buttons['restore']; 
						if ( $this->permissions->can_delete() ) echo $buttons['delete'];
					}
					?>						
					</td>
				</tr>
				<?php endforeach; ?>
			</tbody>
		</table>

	</div>
</div>

<div class="row d-print-none lists-tools">

	<?php if ( count( $bulk_actions['options'] ) > 1 ) : ?>

	<div class="col form-inline">
		<div class="bulk-box">
			<?php echo form_dropdown( $bulk_actions ); ?>
			<?php echo form_submit( $submit_bulk_actions ); ?>
		</div>
	</div>

	<?php endif; if ( isset( $pagination ) ) : ?><div class="col page-navigation"><nav><?php echo $pagination; ?></nav></div><?php endif; ?>

</div>

<?php echo form_close(); else : $this->load->view( 'singles/list-empty' ); endif; ?>
