<?php defined( 'BASEPATH' ) OR exit( 'No direct script access allowed' ); ?>

		</div>
	</div>

	<!-- ###################################### MODAL FOR BRIEFS ###################################### -->

	<div class="modal fade d-print-none" id="modal-brief" tabindex="-1" role="dialog">
		<div class="modal-dialog modal-lg" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<h5 class="modal-title"><?php _e( "Brief" ); ?></h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="<?php _e( "Close" ); ?>"><span aria-hidden="true">&times;</span></button>
				</div>
				<div class="modal-body">
					<p class="text-center" style="line-height: 200px;"><img src="<?php echo base_url( 'assets/img/loading.gif' ); ?>"></p>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-secondary btn-sm" data-dismiss="modal"><?php _e( "Close" ); ?></button>
				</div>
			</div>
		</div>
	</div>

	<!-- ###################################### MODAL FOR ALERTS ###################################### -->

	<div class="modal d-print-none" id="modal-alert" tabindex="-1" role="dialog">
		<div class="modal-dialog modal-dialog-centered" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<h5 class="modal-title"><i class="fa fa-exclamation-triangle"></i><?php _e( "Alert!" ); ?></h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="<?php _e( "Close" ); ?>"><span aria-hidden="true">&times;</span></button>
				</div>
				<div class="modal-body">
					<p class="text-center" style="line-height: 200px;"><img src="<?php echo base_url( 'assets/img/loading.gif' ); ?>"></p>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-secondary btn-sm" data-dismiss="modal"><?php _e( "Close" ); ?></button>
				</div>
			</div>
		</div>
	</div>

	<!-- ##################################### MODAL FOR CONFIRMS ###################################### -->

	<div class="modal d-print-none" id="modal-confirm" tabindex="-1" role="dialog">
		<div class="modal-dialog modal-dialog-centered" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<h5 class="modal-title"><i class="fa fa-info-circle"></i><?php _e( "Confirm" ); ?></h5>
				</div>
				<div class="modal-body">
					<p class="text-center" style="line-height: 200px;"><img src="<?php echo base_url( 'assets/img/loading.gif' ); ?>"></p>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-primary btn-sm"><?php _e( "OK" ); ?></button>
					<button type="button" class="btn btn-secondary btn-sm" data-dismiss="modal"><?php _e( "Cancel" ); ?></button>
				</div>
			</div>
		</div>
	</div>

	<!-- ######################################## LOADING SCREEN ######################################## -->
	
	<div id="loading-screen">
		<div class="background"></div>
		<div class="content d-flex align-items-center">
			<div class="content-elements">

				<div class="spinner">
					<div class="lds-css ng-scope">
						<div class="lds-spinner">
							<div></div>
							<div></div>
							<div></div>
							<div></div>
							<div></div>
							<div></div>
							<div></div>
							<div></div>
							<div></div>
							<div></div>
							<div></div>
							<div></div>
						</div>
					</div>
				</div>

				<p><?php _e( "Loading... Please wait." ); ?></p>

			</div>
		</div>
	</div>

	<?php $this->alerts->output_alert( 'main-top-alert' ); ?>

	<script type="text/javascript">
		/*<![CDATA[*/

		// Global System Vars
		var conf = {
			site_url: '<?php echo site_url(); ?>',
			decimals: <?php echo $this->assets->conf['decimals']; ?>,
			decimal_point: '<?php echo $this->assets->conf['decimal_point']; ?>',
			thousands_separator: '<?php echo $this->assets->conf['thousands_separator']; ?>',
			qty_decimals: <?php echo $this->assets->conf['qty_decimals']; ?>,
			date_format: '<?php echo $this->assets->conf['date_format_js']; ?>',
			isAndroid: <?php echo ( $this->mobile_detect->isAndroidOS() ) ? 'true' : 'false'; ?>,
			isiOS: <?php echo ( $this->mobile_detect->isIOS() ) ? 'true' : 'false'; ?>,
			isTablet: <?php echo ( $this->mobile_detect->isTablet() ) ? 'true' : 'false'; ?>,
			isMobile: <?php echo ( $this->mobile_detect->isMobile() ) ? 'true' : 'false'; ?>,
			language: '<?php echo $this->assets->conf['language']; ?>',
			language_short: '<?php echo substr( $this->assets->conf['language'], 0, 2 ); ?>',

			units: <?php echo json_encode( $this->assets->conf['units'] ); ?>,
			currencies: <?php echo json_encode( $this->assets->conf['currencies'] ); ?>,
			currencies_ext: <?php echo json_encode( $this->assets->conf['currencies_ext'] ); ?>,
			currency: '<?php echo $this->assets->conf['currency']; ?>',
			taxes: <?php echo json_encode( $this->assets->conf['bill_columns'] ); ?>,
			tax_multipliers: <?php echo json_encode( $this->assets->conf['tax_multipliers'] ); ?>,

			casual_client: <?php echo json_encode( $this->assets->conf['casual_client'] ); ?>,
			current_controller: '<?php echo $this->uri->segment( 1 ); ?>',
		};

		var messages = {
			tag_new: ' <em>(<?php _e( "New" ); ?>)</em>',
			no_data: '<?php echo $this->assets->conf['no_data']; ?>',

			complete_fields_to_list: '<?php _e( "You have to complete every field to add the item to the list, otherwise the information will be discarded." ); ?>',
			complete_fields_to_edit: '<?php _e( "You have to complete every field to edit the item into the list, otherwise the information will be discarded." ); ?>',
			complete_fields_to_add_stock: '<?php _e( "You have to complete every field marked with a star (*) to add the item to the stock." ); ?>',
			sure_store_stock: '<?php _e( "Are you sure you want to stock this item?" ); ?>',
			original_row: '<?php _e( "You cannot delete an original row originated in the original purchase." ); ?>',

			sku_duplicated: '<?php _e( "The SKU you entered is in use already. Enter a new value." ); ?>',
			sku_duplicated_alert: '<?php _e( 'The SKU you entered is in use already by the following product:\n\nProduct: <strong>{0}</strong>\nInitial quantity: <strong>{1}</strong>\nCurrent quantity: <strong>{2}</strong>\n\nThe new quantity will be added to the existant lot. Are you sure you want to perform that action?' ); ?>',
			sku_label: '<?php _e( 'SKU' ); ?>',
			exchange_file_not_loaded: '<?php _e( 'The currency exchange file does not exist. Complete the exchange rate manually on the Currency Exchange screen, under Settings.' ); ?>',
			select_pos_first: '<?php _e( 'First, select the POS in order to search the stock.' ); ?>',
			quantity_invalid: '<?php _e( 'The quantity is invalid. It has to be numeric and greater than zero.' ); ?>',
			quantity_invalid_zero: '<?php _e( 'The quantity is invalid. It has to be numeric and greater than or equal to zero.' ); ?>',
			quantity_surpased: '<?php _e( 'The available quantity was surpased. There are <strong>{0} {1}</strong> available.' ); ?>',
			price_invalid: '<?php _e( 'The price is invalid. It has to be numeric and greater than zero.' ); ?>',
			print_order_number: '<?php _e( 'Print Order: {0}' ); ?>',

			product_id_already_present: '<?php _e( 'Products already added on the list cannot be duplicated.' ); ?>',
			product_not_enough_for_sale: '<?php _e( 'There are not enough {0} of {1} to aggregate to the sale.' ); ?>',

			one_product_to_finish: '<?php _e( 'You have to add at least one product to the list to finish the sale.' ); ?>',
			client_to_finish: '<?php _e( 'You have to pick a client (or assign a casual client) to finish the sale.' ); ?>',

			user_level_forbidden_action: '<?php _e( 'Your user level is not cleared to perform that action. Get in touch with an administrator.' ); ?>',
		};

		/*]]>*/
	</script>

	<?php $this->assets->print_scripts(); ?>
</body>
</html>