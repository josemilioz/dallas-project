
<div class="row">

<?php if ( ! empty( $results ) ) : ?>

	<div class="col-xs-12" id="search-results">
		<?php foreach ( $results AS $name => $count ) : ?>
		<div class="search-block text-center search-<?php echo $name; ?>">
			<?php $module = $result = "";
			switch ( $name ) : 
				case 'products' : $module = _n( 'Producto', 'Productos', $count ); break;
				case 'clients' : $module = _n( 'Cliente', 'Clientes', $count ); break;
				case 'orders' : $module = _n( 'Pedido', 'Pedidos', $count ); break;
				case 'purchases' : $module = _n( 'Compra', 'Compras', $count ); break;
				case 'sales' : $module = _n( 'Venta', 'Ventas', $count ); break;
				case 'movements' : $module = _n( 'Movimiento', 'Movimientos', $count ); break;
			endswitch;

			$result = '<p><span class="tag">' . _n( "Se encontró", "Se encontraron", $count ) . '</span>';
			$result.= '<span class="number">' . $count . '</span>';
			$result.= '<span class="module">' . $module . '</span></p>';

			echo $result;

			if ( $count > 0 )
			{
				echo anchor( "$name/search?s=$url_keyword", __( "Mostrar" ), array( 'class' => 'btn btn-primary' ) );
			}
			?>
		</div>
		<?php endforeach; ?>
	</div>
<?php else : ?>
	<p class="text-center text-danger no-items"><?php _e( "No se encontraron resultados de búsqueda. Inténtelo nuevamente con otras palabras clave." ); ?></p>
<?php endif; ?>

</div>
