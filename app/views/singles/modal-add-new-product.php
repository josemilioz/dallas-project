<!-- ##################################### ADD PRODUCT MODAL ###################################### -->

<div class="modal fade d-print-none modal-default" id="modal-add-new-product" tabindex="-1" role="dialog">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title"><?php _e( "Add New Product" ); ?></h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="<?php _e( "Close" ); ?>"><span aria-hidden="true">&times;</span></button>
			</div>
			<div class="modal-body">
				<div class="form-row">
					<div class="form-group col mb-3" id="modal_new_product_name_box">
						<?php 
							echo form_label( __( "Name" ), 'modal_new_product_name', array( 'class' => 'control-label' ) );
							echo $this->assets->conf['mandatory'];
							echo form_input( $modal_new_product_name );
							echo form_error( 'modal_new_product_name' );
						?>
					</div>
				</div>

				<div class="form-row">
					<div class="form-group col-6 mb-3" id="modal_new_product_unit_price_box">
						<?php 
							echo form_label( __( "Unit price" ), 'modal_new_product_unit_price', array( 'class' => 'control-label' ) );
							echo $this->assets->conf['mandatory'];
						?>
						<div class="input-group">
							<?php echo form_input( $modal_new_product_unit_price ); ?>
							<div class="input-group-append">
								<button class="btn btn-purple btn-sm dropdown-toggle" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><?php echo $this->assets->conf['currencies'][$this->assets->conf['currency']]; ?></button>
								<div class="dropdown-menu dropdown-menu-right">
									<?php foreach ( $this->assets->conf['currencies_ext'] AS $key => $name ) : if ( ! empty( $key ) ) : ?>
									<a class="dropdown-item units-item" href="#" data-currency="<?php echo $key; ?>"><?php echo $name; ?></a>
									<?php endif; endforeach; ?>
								</div>
							</div>
						</div>
						<?php
							echo form_input( $modal_new_product_currency );
							echo form_error( 'modal_new_product_unit_price' );
							echo form_error( 'modal_new_product_currency' );
						?>
					</div>
					<div class="form-group col-6" id="modal_new_product_unit_measure_box">
						<?php 
							echo form_label( __( "Unit measure" ), 'modal_new_product_unit_measure', array( 'class' => 'control-label' ) );
							echo $this->assets->conf['mandatory'];
							echo form_dropdown( $modal_new_product_unit_measure );
							echo form_error( 'modal_new_product_unit_measure' );
						?>
					</div>
				</div>

				<div class="form-row">
					<div class="form-group col-6 mb-3" id="modal_new_product_category_id_box">
						<?php 
							echo form_label( __( "Category" ), 'modal_new_product_category_id', array( 'class' => 'control-label' ) );
							echo $this->assets->conf['mandatory'];
							echo form_dropdown( $modal_new_product_category_id );
							echo form_error( 'modal_new_product_category_id' );
						?>
					</div>
					<div class="form-group col-6" id="modal_new_product_tax_box">
						<?php 
							echo form_label( __( "Tax" ), 'modal_new_product_tax', array( 'class' => 'control-label' ) );
							echo $this->assets->conf['mandatory'];
							echo form_dropdown( $modal_new_product_tax );
							echo form_error( 'modal_new_product_tax' );
						?>
					</div>
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-purple btn-sm" id="confirm-add-new-product"><?php _e( "Add" ); ?></button>
				<button type="button" class="btn btn-secondary btn-sm" data-dismiss="modal"><?php _e( "Cancel" ); ?></button>
			</div>
		</div>
	</div>
</div>