
<?php echo form_open( current_url(), array( 'autocomplete' => 'off' ) ); ?>

<div class="row form-elements">

	<div class="col-lg-5">
		<div class="card">
			<div class="card-body">
				<p class="text-muted mb-1"><?php _e( "The current system currency is" ); ?></p>
				<h3 class="text-success mb-1"><?php echo $this->assets->conf['currencies_ext'][$this->assets->conf['currency']]; ?></h3>
				<?php if ( ! empty( $last_edition ) ) : ?><p class="text-secondary"><small><?php printf( __( "Last edition: %s" ), date( $this->assets->conf['datetime_format'], $last_edition ) ); ?></small></p><?php endif; ?>
				<hr>
				<p class="text-muted"><?php _e( "For reference, system will only use <em>On sale</em> value when doing conversions." ); ?></p>
				<?php if ( $this->assets->conf['auto_update_exchange'] ) : ?>
				<hr>
				<div class="alert alert-warning mb-0">
					<div class="row d-flex align-items-center">
						<div class="col-2"><i class="fa fa-info-circle" style="font-size: 2.7rem"></i></div>
						<div class="col-10"><?php printf( __( "You have exchange auto-update turned on <strong>every %d hours</strong>. The values you enter now might be altered later without you noticing. You can turn it off in the %s screen." ), $this->assets->conf['auto_update_hours'], anchor( 'settings/main', __( "Main Settings" ) ) ); ?></div>
					</div>
				</div>
				<?php endif; ?>
			</div>
		</div>
	</div>

	<div class="col-lg-7">
		<div class="card">
			<div class="table-responsive">
				<table class="table table-striped table-hover mb-0">
					<col width="auto" />
					<col width="150" />
					<col width="150" />
					<thead>
						<tr>
							<th><?php _e( "Currency" ); ?></th>
							<th><?php _e( "On purchase" ); ?></th>
							<th><?php _e( "On sale" ); ?></th>
						</tr>
					</thead>
					<tbody>
						<?php foreach ( $this->assets->conf['currencies'] AS $key => $symbol ) : ?>
						<tr>
							<td class="align-middle"><?php 
								echo $this->assets->conf['currencies_ext'][$key]; 
								if ( $key == $this->assets->conf['currency'] )
								{
									echo ' <small class="text-success"><strong>[' . __( "1 to 1" ) . ']</strong></small>';
								}
							?></td>
							<td class="align-middle">
								<input type="text" name="purchase[<?php echo $key; ?>]" id="purchase-<?php echo $key; ?>" class="form-control input-currency-ext" value="<?php echo my_number_format( $exchange[$key]['purchase'], 5 ); ?>" />
								<?php echo form_error( "purchase[$key]" ); ?>
							</td>
							<td class="align-middle">
								<input type="text" name="sale[<?php echo $key; ?>]" id="sale-<?php echo $key; ?>" class="form-control input-currency-ext" value="<?php echo my_number_format( $exchange[$key]['sale'], 5 ); ?>" />
								<?php echo form_error( "sale[$key]" ); ?>
							</td>
						</tr>
						<?php endforeach; ?>
					</tbody>
				</table>
			</div>
		</div>		
	</div>

</div>


<div class="buttons-set">
	<button type="submit" class="btn btn-primary">
		<i class="fa fa-save"></i>
		<?php _e( "Save" ); ?>
	</button>
</div>

<?php echo form_close(); ?>
