
	<div class="alert-place"></div>

	<input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>">

	<p>
		<label for="identity" class="control-label"><?php _e( "Username" ); ?></label>
		<input type="text" name="identity" value="" id="identity" class="form-control input-lg">
	</p>

	<p>
		<label for="password" class="control-label"><?php _e( "Password" ); ?></label>
		<input type="password" name="password" value="" id="password" class="form-control input-lg">
	</p>

	<div class="checkbox text-center">
		<label for="remember">
			<input type="checkbox" name="remember" value="1" id="remember">
			<?php _e( "Remember me" ); ?>
		</label>
	</div>

	<p class="text-center"><input type="submit" name="submit" id="login-button" value="<?php _e( "Login" ); ?>" class="btn btn-primary btn-lg"></p>

	<p class="text-center"><?php echo anchor( 'forgot_password', __( "¿Has olvidado tu contraseña?" ), array( 'target' => '_blank' ) ); ?></p>

	<input type="hidden" id="token" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>">
