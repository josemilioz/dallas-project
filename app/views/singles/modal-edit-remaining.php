<?php
$modal_edit_magnitude_remaining = array(
	'type'		=> 'tel',
	'id'		=> 'modal_edit_magnitude_remaining',
	'name'		=> 'modal_edit_magnitude_remaining',
	'maxlength'	=> 9,
	'class'		=> 'form-control form-control-lg input-quantity',
);
?>

<!-- ##################################### EDIT REMAINING MODAL ###################################### -->

<div class="modal fade d-print-none modal-default" id="modal-edit-magnitude-remaining" tabindex="-1" role="dialog">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title"><?php _e( "Set Remaining Quantity" ); ?></h5>
			</div>
			<div class="modal-body">
				<div class="row">
					<div class="col-md-4 product-picture d-none d-md-block"></div>
					<div class="col-md-8">
						<h5 class="mb-3"></h5>
						<div class="form-row">
							<div class="form-group col" id="modal_edit_magnitude_remaining_box">
								<?php 
									echo form_label( __( "Remaining" ), 'modal_edit_magnitude_remaining', array( 'class' => 'control-label' ) ); 
									echo $this->assets->conf['mandatory'];
								?>
								<div class="input-group input-group-lg">
									<?php echo form_input( $modal_edit_magnitude_remaining ); ?>
									<div class="input-group-append">
										<span class="input-group-text"></span>
									</div>
								</div>
								<?php echo form_error( 'modal_edit_magnitude_remaining' ); ?>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-purple btn-sm" id="confirm-edit-magnitude-remaining"><?php _e( "Set Remaining" ); ?></button>
			</div>
		</div>
	</div>
</div>