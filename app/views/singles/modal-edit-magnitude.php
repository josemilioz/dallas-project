<?php
$modal_edit_magnitude_quantity = array(
	'type'		=> 'tel',
	'id'		=> 'modal_edit_magnitude_quantity',
	'name'		=> 'modal_edit_magnitude_quantity',
	'maxlength'	=> 9,
	'class'		=> 'form-control form-control-lg input-quantity',
);

$modal_edit_magnitude_price = array(
	'type'		=> 'tel',
	'id'		=> 'modal_edit_magnitude_price',
	'name'		=> 'modal_edit_magnitude_price',
	'maxlength'	=> 9,
	'class'		=> 'form-control form-control-lg input-currency',
);
?>

<!-- ##################################### EDIT QUANTITY MODAL ###################################### -->

<div class="modal fade d-print-none modal-default" id="modal-edit-magnitude-quantity" tabindex="-1" role="dialog">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title"><?php _e( "Set Quantity" ); ?></h5>
			</div>
			<div class="modal-body">
				<div class="row">
					<div class="col-md-4 product-picture d-none d-md-block"></div>
					<div class="col-md-8">
						<h5 class="mb-3"></h5>
						<div class="form-row">
							<div class="form-group col" id="modal_edit_magnitude_quantity_box">
								<?php 
									echo form_label( __( "Quantity" ), 'modal_edit_magnitude_quantity', array( 'class' => 'control-label' ) ); 
									echo $this->assets->conf['mandatory'];
								?>
								<div class="input-group input-group-lg">
									<?php echo form_input( $modal_edit_magnitude_quantity ); ?>
									<div class="input-group-append">
										<span class="input-group-text"></span>
									</div>
								</div>
								<?php echo form_error( 'modal_edit_magnitude_quantity' ); ?>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-purple btn-sm" id="confirm-edit-magnitude-quantity"><?php _e( "Set Quantity" ); ?></button>
			</div>
		</div>
	</div>
</div>

<!-- ##################################### EDIT PRICE MODAL ###################################### -->

<div class="modal fade d-print-none modal-default" id="modal-edit-magnitude-price" tabindex="-1" role="dialog">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title"><?php _e( "Set Unitary Price" ); ?></h5>
			</div>
			<div class="modal-body">
				<div class="row">
					<div class="col-md-4 product-picture d-none d-md-block"></div>
					<div class="col-md-8">
						<h5 class="mb-3"></h5>
						<div class="form-row">
							<div class="form-group col" id="modal_edit_magnitude_price_box">
								<?php 
									echo form_label( __( "Price per Unit" ), 'modal_edit_magnitude_price', array( 'class' => 'control-label' ) ); 
									echo $this->assets->conf['mandatory'];
								?>
								<div class="input-group input-group-lg">
									<?php echo form_input( $modal_edit_magnitude_price ); ?>
									<div class="input-group-append">
										<span class="input-group-text"></span>
									</div>
								</div>
								<?php echo form_error( 'modal_edit_magnitude_price' ); ?>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-purple btn-sm" id="confirm-edit-magnitude-price"><?php _e( "Set Unitary Price" ); ?></button>
			</div>
		</div>
	</div>
</div>