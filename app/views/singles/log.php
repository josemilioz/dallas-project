
<?php if ( ! empty( $logs ) ) : ?>

<div class="table-responsive" id="logs">
	<table class="table table-striped table-hover">
		<col width="160">
		<col width="220">
		<col width="auto">
		<thead>
			<tr>
				<th><?php _e( "Date & Time" ); ?></th>
				<th><?php _e( "User" ); ?></th>
				<th><?php _e( "Action" ); ?></th>
			</tr>
		</thead>
		<tfoot>
			<tr>
				<th><?php _e( "Date & Time" ); ?></th>
				<th><?php _e( "User" ); ?></th>
				<th><?php _e( "Action" ); ?></th>
			</tr>
		</tfoot>
		<tbody>
			<?php foreach ( $logs AS $l ) : ?>
			<tr>
				<td><?php echo date( $this->assets->conf['log_date_format'], strtotime( $l['date'] ) ); ?></td>
				<td><?php printf( '%1$s %2$s', $this->ion_auth->user( $l['user_id'] )->row()->first_name, $this->ion_auth->user( $l['user_id'] )->row()->last_name ); ?></td>
				<td>
					<a href="#comments-<?php echo $l['log_id']; ?>" class="print-hide expand-data btn btn-xs btn-default pull-right" data-toggle="collapse" title="<?php _e( "Expandir" ); ?>">
						<i class="glyphicon glyphicon-chevron-down"></i>
					</a>
					<?php echo ( ! empty( $l['uri'] ) ) ? $l['uri'] : "-"; ?>
					<div class="collapse" id="comments-<?php echo $l['log_id']; ?>">
						<div class="table-responsive">
							<table class="table">
								<col width="145">
								<tbody>
									<tr>
										<td><?php _e( "URI" ); ?></td>
										<td><?php echo ( ! empty( $l['uri'] ) ) ? $l['uri'] : "-"; ?></td>
									</tr>
									<tr>
										<td><?php _e( "IP" ); ?></td>
										<td><?php echo ( ! empty( $l['ip'] ) ) ? $l['ip'] : "-"; ?></td>
									</tr>
									<tr>
										<td><?php _e( "Navigator" ); ?></td>
										<td><?php echo ( ! empty( $l['browser'] ) ) ? $l['browser'] : "-"; ?></td>
									</tr>
									<tr>
										<td><?php _e( "Version" ); ?></td>
										<td><?php echo ( ! empty( $l['version'] ) ) ? $l['version'] : "-"; ?></td>
									</tr>
									<tr>
										<td><?php _e( "Operative system" ); ?></td>
										<td><?php echo ( ! empty( $l['os'] ) ) ? $l['os'] : "-"; ?></td>
									</tr>
									<tr>
										<td><?php _e( "Result" ); ?></td>
										<td><?php 
										if ( ! empty( $l['result'] ) )
										{
											switch ( $l['result'] )
											{
												case 'SUCCESS': echo '<span class="label label-success">' . __( "Success" ) . '</span>'; break;
												case 'FAILURE': echo '<span class="label label-danger">' . __( "Failure" ) . '</span>'; break;
											}
										}
										?></td>
									</tr>
									<tr>
										<td><?php _e( "Type" ); ?></td>
										<td><?php echo ( ! empty( $l['type'] ) ) ? $l['type'] : "-"; ?></td>
									</tr>
									<tr>
										<td><?php _e( "Sent data" ); ?></td>
										<td>
											<?php if ( $l['data'] ) : ?>
											<pre><?php print_r( json_decode( $l['data'] ) ); ?></pre>
											<?php else : ?>
											<span class="text-danger"><?php _e( "The procedure did not store data into the database." ); ?></span>
											<?php endif; ?>
										</td>
									</tr>
								</tbody>
							</table>
						</div>
					</div>
				</td>
			</tr>
			<?php endforeach; ?>
		</tbody>
	</table>
</div>

<div class="row print-hide">
	<div class="col-sm-6 bulk-actions">
		<a href="#" class="btn btn-primary print-button"><?php _e( "Print" ); ?></a>
	</div>
	<?php if ( isset( $pagination ) ) : ?><div class="col-sm-6 pagination-links"><?php echo $pagination; ?></div><?php endif; ?>
</div>

<?php else : $this->load->view( 'singles/list-empty' ); endif; ?>
