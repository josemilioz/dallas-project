
<?php echo form_open( current_url(), array( 'autocomplete' => 'off' ) ); ?>

<div class="row form-elements">

	<div class="col-lg-4">
		<div class="card">
			<div class="card-body">
				<h4><?php _e( "Preferences" ); ?></h4>
				<div class="form-row">
					<div class="form-group col">
						<?php 
							echo form_label( __( "Company name" ), 'company_name', array( 'class' => 'control-label' ) );
							echo form_input( $company_name );
							echo form_error( 'company_name' );
						?>
					</div>
				</div>
				<div class="form-row">
					<div class="form-group col">
						<?php 
							echo form_label( __( "Default language" ), 'language', array( 'class' => 'control-label' ) );
							echo form_dropdown( $language );
							echo form_error( 'language' );
						?>
					</div>
				</div>
			</div>
		</div>

		<div class="card">
			<div class="card-body">
				<h4><?php _e( "Pagination" ); ?></h4>
				<div class="form-row">
					<div class="form-group col">
						<?php 
							echo form_label( __( "Items per page" ), 'items_per_page', array( 'class' => 'control-label' ) );
							echo form_input( $items_per_page );
							echo form_error( 'items_per_page' );
						?>
					</div>
				</div>
				<div class="form-row">
					<div class="form-group col">
						<?php 
							echo form_label( __( "Pages to show" ), 'pages_to_show', array( 'class' => 'control-label' ) );
							echo form_input( $pages_to_show );
							echo form_error( 'pages_to_show' );
						?>
					</div>
				</div>
			</div>
		</div>

	</div>

	<div class="col-lg-4">
		<div class="card">
			<div class="card-body">
				<h4><?php _e( "Stock" ); ?></h4>
				<div class="form-row">
					<div class="form-group col">
						<?php 
							echo form_label( __( "Stock minimum warning" ), 'min_warning', array( 'class' => 'control-label' ) );
							echo form_input( $min_warning );
							echo form_error( 'min_warning' );
						?>
					</div>
				</div>
				<div class="form-row">
					<div class="form-group col">
						<?php 
							echo form_label( __( "Stock minimum danger" ), 'min_danger', array( 'class' => 'control-label' ) );
							echo form_input( $min_danger );
							echo form_error( 'min_danger' );
						?>
					</div>
				</div>
				<div class="form-row">
					<div class="form-group col">
						<?php 
							echo form_label( __( "Quantities decimals" ), 'qty_decimals', array( 'class' => 'control-label' ) );
							echo form_dropdown( $qty_decimals );
							echo form_error( 'qty_decimals' );
						?>
					</div>
				</div>
			</div>
		</div>

		<div class="card">
			<div class="card-body">
				<h4><?php _e( "Performance" ); ?></h4>
				<div class="form-row">
					<div class="form-group col">
						<div class="form-check">
							<?php echo form_input( $cache_assets ); ?>
							<?php echo form_label( __( "Cache assets" ), 'cache_assets', array( 'class' => 'form-check-label' ) ); ?>
						</div>
						<?php echo form_error( 'cache_assets' ); ?>
						<small class="text-muted"><?php _e( "Deprecated" ); ?></small>
					</div>
				</div>
			</div>
		</div>
	</div>

	<div class="col-lg-4">
		<div class="card">
			<div class="card-body">
				<h4><?php _e( "Currency" ); ?></h4>
				<div class="form-row">
					<div class="form-group col">
						<?php 
							echo form_label( __( "System currency" ), 'currency', array( 'class' => 'control-label' ) );
							echo form_dropdown( $currency );
							echo form_error( 'currency' );
						?>
					</div>
				</div>
				<div class="form-row">
					<div class="form-group col">
						<?php 
							echo form_label( __( "Decimals" ), 'decimals', array( 'class' => 'control-label' ) );
							echo form_dropdown( $decimals );
							echo form_error( 'decimals' );
						?>
					</div>
				</div>
				<div class="form-row">
					<div class="form-group col">
						<div class="form-check">
							<?php echo form_input( $auto_update_exchange ); ?>
							<?php echo form_label( __( "Auto update the exchange rate" ), 'auto_update_exchange', array( 'class' => 'form-check-label' ) ); ?>
						</div>
						<?php echo form_error( 'auto_update_exchange' ); ?>
					</div>
				</div>
				<div class="collapse <?php echo ( ! $this->assets->conf['auto_update_exchange'] ) ? "hide" : "show"; ?>" id="auto-update-hours-group">
					<div class="form-row">
						<div class="form-group col">
							<div class="input-group">
								<div class="input-group-prepend">
									<span class="input-group-text"><?php _e( "Every" ); ?></span>
								</div>
								<?php echo form_input( $auto_update_hours ); ?>
								<div class="input-group-append">
									<span class="input-group-text"><?php _e( "hours" ); ?></span>
								</div>
							</div>
							<?php echo form_error( 'auto_update_hours' ); ?>
						</div>
					</div>
					<div class="form-row">
						<div class="form-group col">
							<?php 
								echo form_label( __( "Auto-update source" ), 'auto_update_source', array( 'class' => 'control-label' ) );
								echo form_input( $auto_update_source );
								echo form_error( 'auto_update_source' );
							?>
							<small class="form-text text-muted"><?php _e( "Consider the data format of the source. Contact your Dallas developer for more information." ); ?></small>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

</div>

<div class="buttons-set">
	<button type="submit" class="btn btn-primary">
		<i class="fa fa-save"></i>
		<?php _e( "Save" ); ?>
	</button>
</div>

<?php echo form_close(); ?>
