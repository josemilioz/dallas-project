<ul class="list-group">
	<?php
	if ( ! empty( $messages ) ) :

		foreach ( $messages AS $m ) : 
			$user = $this->ion_auth->user( $m['creator'] );
			
			$username = $user->row()->first_name;
			
			if ( ! empty( $user->row()->last_name ) )
			{
				$username .= " " . substr( $user->row()->last_name, 0, 1 ) . ".";
			}
	?>
	<li class="list-group-item">
		<?php if ( $this->ion_auth->in_group( array( "admin" ) ) ) : ?>
		<a href="#" class="btn btn-xs btn-warning pull-right chat-trash" 
			title="<?php _e( "Descartar" ); ?>" 
			data-chat-id="<?php echo $m['chat_id']; ?>" 
			data-confirm="<?php _e( "¿Estás seguro que quieres enviar a papelera este item?" ); ?>">
			<i class="glyphicon glyphicon-trash"></i>
		</a>
		<?php endif; ?>
		<span class="user" title="<?php echo date( $this->assets->conf['datetime_format'], strtotime( $m['created'] ) ); ?>">
			<span><?php echo ucwords( $username ); ?></span>
			<i class="glyphicon glyphicon-chevron-right"></i>
		</span>
		<span class="message"><?php echo $m['message']; ?></span>
	</li>
	<?php endforeach; else : ?>
	<li class="list-group-item">
		<p class="text-center text-danger"><?php _e( "No se encontraron mensajes." ); ?></p>
	</li>
	<?php endif; ?>

</ul>