
<h2><?php _e( "System Locked" ); ?></h2>

<p class="text-center"><?php _e( "The system is locked. Please, get in touch with a superadmin for more information about this situation." ); ?></p>
<p class="text-center mb-0"><?php echo anchor( "logout", __( "Logout" ), array( 'class' => 'btn btn-outline-light' ) ); ?></p>
