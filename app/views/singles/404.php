
	<div class="text-center">
		<h2 class="mb-3"><?php _e( "404 Error" ); ?></h2>

		<p><?php _e( "The screen you're looking for does not exist or has been moved." ); ?></p>
		<p class="mb-0"><?php printf( __( 'Please, return to the home screen %1$s.' ), anchor( '/', __( "clicking here" ) ) ); ?></p>
	</div>
