
	<?php 
	$avatar_instead_places = array( 'users/', 'profile', 'clients/' );
	$default_image = site_url( 'assets/img/default-no-image.png' );

	foreach ( $avatar_instead_places AS $s )
	{
		if ( strpos( uri_string(), $s ) !== FALSE ) $default_image = site_url( 'assets/img/default-avatar.png' );
	}
	?>
	<div class="form-row">
		<div class="picture-big-container w-100 text-center">
			<img class="picture-big w-75" src="<?php echo ( isset( $file['src'] ) ) ? $file['src'] : $default_image; ?>" />
		</div>
		<?php if ( ! isset( $file ) ) : ?>
		<div id="progress" class="progress w-100 mt-3">
			<div class="progress-bar"></div>
		</div>
		<?php endif; ?>
		<div class="upload-buttons mt-3">
			<?php if ( ! isset( $file ) ) : ?>
			<span class="btn btn-secondary btn-sm fileinput-button">
				<span><?php _e( "Upload Picture" ); ?></span>
				<input id="fileupload" type="file" name="file" />
			</span>
			<?php else : ?>
			<button type="button" class="btn btn-secondary btn-sm fileinput-delete">
				<?php _e( "Delete Picture" ); ?>
			</button>
			<input type="hidden" name="file_id" id="file_id" value="<?php echo $file['file_id']; ?>" />
			<?php endif; ?>
		</div>
	</div>
