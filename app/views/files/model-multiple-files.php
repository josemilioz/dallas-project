

	<div class="d-flex flex-row">
		<h4 <?php if ( ! isset( $files ) ) echo 'class="mb-0"'; ?>><?php _e( "Attachments" ); ?></h4>
		<div class="mx-3">
			<span class="btn btn-secondary btn-sm fileinput-button text-nowrap">
				<span><?php _e( "Upload" ); ?></span>
				<span class="d-none d-sm-inline"><?php _e( "File" ); ?></span>
				<input id="fileupload" type="file" name="file" />
			</span>			
		</div>
		<div class="w-100">
			<div id="progress" class="progress w-100 mt-2">		
				<div class="progress-bar"></div>
			</div>			
		</div>
	</div>

	<?php if ( isset( $files ) AND ! empty( $files ) ) : ?>
	<div class="table-responsive">
		<table class="table table-striped table-hover">
			<col width="auto">
			<col width="auto">
			<col width="auto">
			<col width="auto">
			<col width="100">
			<thead>
				<tr>
					<th><?php _e( "Filename" ); ?></th>
					<th><?php _e( "Size" ); ?></th>
					<th><?php _e( "Mime/type" ); ?></th>
					<th><?php _e( "Uploaded by" );?></th>
					<th></th>
				</tr>
			</thead>
			<tfoot>
				<tr>
					<th><?php _e( "Filename" ); ?></th>
					<th><?php _e( "Size" ); ?></th>
					<th><?php _e( "Mime/type" ); ?></th>
					<th><?php _e( "Uploaded by" );?></th>
					<th></th>
				</tr>
			</tfoot>
			<tbody>
				<?php foreach ( $files AS $key => $file ) : ?>
				<tr>
					<td><strong><?php echo $file['original_name']; ?></strong></td>
					<td><?php echo byte_format( $file['size'], 2 ); ?></td>
					<td><?php echo $file['mimetype']; ?></td>
					<td><?php
					if ( empty( $file['modified'] ) )
					{
						printf( 
							__( "%s on %s" ),
							$this->ion_auth->user_name( 'lastname_start', $file['creator'] ),
							'<em class="text-muted">' . date( $this->assets->conf['datetime_format'], strtotime( $file['created'] ) ) . '</em>'
						);
					}
					else
					{
						printf( 
							__( "%s on %s" ),
							$this->ion_auth->user_name( 'lastname_start', $file['modifier'] ),
							'<em class="text-muted">' . date( $this->assets->conf['datetime_format'], strtotime( $file['modified'] ) ) . '</em>'
						);
					}
					?></td>
					<td class="text-right action-buttons">
						<?php echo anchor( "files/download/" . $file['file_id'], '<i class="fa fa-download"></i>', array( 'class' => 'btn btn-purple btn-download-file', 'title' => __( 'Download' ) ) ); ?>
						<?php echo anchor( "#", '<i class="fa fa-times"></i>', array( 'class' => 'btn btn-danger btn-delete-file', 'title' => __( 'Delete' ) ) ); ?>
						<input type="hidden" name="file_id[]" id="file_id_<?php echo $key; ?>" value="<?php echo $file['file_id']; ?>" />
					</td>
				</tr>
				<?php endforeach; ?>
			</tbody>
		</table>
	</div>
	<?php endif; ?>
