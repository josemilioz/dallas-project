
	<div class="form-row" id="file-upload">
		<div class="col-5 col-md-4 col-lg-6">
			<div class="upload-buttons">
				<span class="btn btn-secondary btn-sm fileinput-button">
					<span><?php _e( "Upload Picture" ); ?></span>
					<input id="fileupload" type="file" name="file" />
				</span>
			</div>
		</div>
		<div class="col-7 col-md-8 col-lg-6">
			<div id="progress" class="progress w-100 mt-2">
				<div class="progress-bar"></div>
			</div>
		</div>
	</div>
	
	<?php if ( isset( $files ) ) : ?>
	<div class="form-row" id="file-list">
		<ul class="list-group w-100 mt-3" id="sortable-list">
			<?php foreach ( $files AS $key => $file ) : ?>
			<li class="list-group-item arrangeable-item">
				<button type="button" class="btn btn-sm btn-danger fileinput-delete float-right" data-file-id="<?php echo $file['file_id']; ?>" title="<?php _e( "Delete" ); ?>"><i class="fa fa-times"></i></button>
				<div class="media">
					<img class="thumb align-self-start mr-3" src="<?php echo $file['src']; ?>" alt="<?php _e( "Image" ); ?>" />
					<div class="media-body">
						<p><strong><?php echo ellipsize( $file['original_name'], 22, .5 ); ?></strong></p>
						<p class="text-small text-info"><?php echo $file['mimetype']; ?></p>
						<p class="text-small text-muted"><em><?php echo byte_format( $file['size'], 2 ); ?></em></p>
					</div>
				</div>
				<input type="hidden" name="file_id[]" id="file_id_<?php echo $key; ?>" value="<?php echo $file['file_id']; ?>" />
			</li>
			<?php endforeach; ?>
		</ul>
	</div>
	<?php endif; ?>
