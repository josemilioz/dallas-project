
<?php echo form_open( current_url(), array( 'autocomplete' => 'off', 'id' => 'form-pos' ) ); ?>

<div class="row form-elements">

	<div class="col-lg-6">
		<div class="card" id="data-card">
			<div class="card-body">
				<div class="form-row">
					<div class="form-group col">
						<?php 
							echo form_label( __( "Name" ), 'name', array( 'class' => 'control-label' ) );
							echo $this->assets->conf['mandatory'];
							echo form_input( $name );
							echo form_error( 'name' );
						?>
					</div>
				</div>
				<div class="form-row">
					<div class="form-group col">
						<?php 
							echo form_label( __( "Printer name" ), 'printer', array( 'class' => 'control-label' ) );
							echo form_input( $printer );
							echo form_error( 'printer' );
						?>
					</div>
				</div>
				<div class="form-row">
					<div class="form-group col">
						<?php 
							echo form_label( __( "Assigned IP" ), 'assigned_ip', array( 'class' => 'control-label' ) );
							echo form_input( $assigned_ip );
							echo form_error( 'assigned_ip' );
						?>
					</div>
				</div>
			</div>
		</div>
	</div>

	<div class="col-lg-6">
		<div class="card" id="users-card">
			<div class="card-body">
				<h4><?php _e( "Enabled Users" ); ?></h4>
				<div class="form-row">
					<div class="form-group col">
						<?php echo form_label( __( "Enable user" ), 'user_id', array( 'class' => 'control-label' ) ); ?>
						<div class="d-flex">
							<div class="w-75">
								<?php echo form_dropdown( $user_id ); ?>
							</div>
							<div class="w-25 text-right">
								<button class="btn btn btn-purple btn-add-user">
									<i class="fa fa-plus-circle d-none d-md-inline"></i>
									<?php _e( "Add" ); ?>
									<span class="d-none d-md-inline"><?php _e( "User" ); ?></span>
								</button>
							</div>
						</div>
					</div>
				</div>
			</div>
			<table class="table table-striped table-hover">
				<tbody>
					<tr>
						<td></td>
						<td class="action-buttons text-right">
							<button class="btn btn-sm btn-danger btn-erase" title="<?php _e( "Delete" ); ?>"><i class="fa fa-times"></i></button>
							<input type="hidden" name="user_list[]" />
						</td>
					</tr>
					<?php
					if ( $this->input->post( 'user_list' ) OR isset( $pos_id ) ) : 
						$users = $this->pos->prepare_users( ( $this->input->post( 'user_list' ) ) ? $this->input->post() : $this->pos->users_get( (int)$pos_id['value'] ) );
						if ( ! empty( $users ) ) : foreach ( $users AS $user ) :
							if ( ! isset( $user['fullname'] ) ) $user['fullname'] = $this->ion_auth->user_name( 'lastname_start', $user['user_id'] );
					?>
					<tr>
						<td><?php echo $user['fullname']; ?></td>
						<td class="action-buttons text-right">
							<button class="btn btn-sm btn-danger btn-erase" title="<?php _e( "Delete" ); ?>"><i class="fa fa-times"></i></button>
							<input type="hidden" name="user_list[]" value="<?php echo $user['user_id']; ?>" />
						</td>
					</tr>
					<?php endforeach; endif; endif; ?>
				</tbody>
			</table>
		</div>
	</div>

	<div class="col-lg-12">
		<div class="card" id="categories-card">
			<div class="card-body">
				<h4><?php _e( "Enabled Categories" ); ?></h4>
				<div class="columns-overflow">
					<div class="row">
						<div class="form-group col-5">
							<?php 
								echo form_label( __( "List" ), 'categories_list', array( 'class' => 'control-label' ) );
								echo form_dropdown( $categories_list );
								echo form_error( 'categories_list' );
							?>
						</div>
						<div class="col-2 d-flex align-items-center">
							<div class="flex-fill">
								<button type="button" class="btn btn-secondary btn-block put-right">&gt;</button>
								<button type="button" class="btn btn-secondary btn-block put-all-right">&gt;&gt;</button>
								<button type="button" class="btn btn-secondary btn-block put-left">&lt;</button>
								<button type="button" class="btn btn-secondary btn-block put-all-left">&lt;&lt;</button>								
							</div>
						</div>
						<div class="form-group col-5">
							<?php 
								echo form_label( __( "Enabled" ), 'categories_enabled', array( 'class' => 'control-label' ) );
								echo form_dropdown( $categories_enabled );
								echo form_error( 'categories_enabled' );
							?>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

</div>

<div class="buttons-set">
	<button type="submit" class="btn btn-primary">
		<i class="fa fa-save"></i>
		<?php _e( "Save" ); ?>
	</button>
	<?php if ( $this->uri->segment( 2 ) == "edit" ) echo anchor( 'pos/add', '<i class="fa fa-plus-circle"></i> ' . __( "New" ), array( 'class' => 'btn btn-secondary', 'title' => __( "Add New POS" ) ) ); ?>
</div>

<?php if ( ! empty( $pos_id ) ) echo form_input( $pos_id ); ?>
<?php echo form_close(); ?>