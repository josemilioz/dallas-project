
<div class="card">
	<div class="card-body">
		<div id="brief-wrapper">

			<div class="row">

				<div class="col-md-6 mb-3">
					<h4><?php _e( "POS information" ); ?></h4>
					<div class="element-group row">
						<div class="col-4"><span class="item-label"><?php _e( "Name" ); ?></span></div>
						<div class="col-8"><span class="full-name"><?php echo $pos['name']; ?></div>
					</div>
					<div class="element-group row">
						<div class="col-4"><span class="item-label"><?php _e( "Type" ); ?></span></div>
						<div class="col-8"><span><?php echo $pos['type']; ?></div>
					</div>
					<div class="element-group row">
						<div class="col-4"><span class="item-label"><?php _e( "Printer name" ); ?></span></div>
						<div class="col-8"><span><?php echo $pos['printer']; ?></div>
					</div>
					<div class="element-group row">
						<div class="col-4"><span class="item-label"><?php _e( "Assigned IP" ); ?></span></div>
						<div class="col-8"><span><?php echo $pos['assigned_ip']; ?></div>
					</div>
				</div>

				<div class="col-md-6 mb-3">
					<h4><?php _e( "Users enabled" ); ?></h4>

					<?php if ( ! empty( $users ) ) : foreach ( $users AS $user ) : ?>
					<div class="element-group row">
						<div class="col"><?php echo $this->ion_auth->user_name( 'lastname_start', $user['user_id'] ); ?></div>
					</div>
					<?php endforeach; endif; ?>

				</div>
			</div>

			<?php if ( ! empty( $pos['created'] ) OR ! empty( $pos['modified'] ) ) : ?>
			<div class="row last-activity">
				<div class="col">
					<div class="created">
						<?php if ( ! empty( $pos['created'] ) ) : ?>
						<p class="label"><?php _e( "Created by" ); ?></p>
						<p class="value"><?php echo $this->ion_auth->user_name( "fullname", $pos['creator'] ) ?> <small><?php echo date( $this->assets->conf['datetime_format'], strtotime( $pos['created'] ) ); ?></small></p>
						<?php endif; ?>
					</div>
				</div>
				<div class="col">
					<div class="modified">
						<?php if ( ! empty( $pos['modified'] ) ) : ?>
						<p class="label"><?php _e( "Last modified by" ); ?></p>
						<p class="value"><?php echo $this->ion_auth->user_name( "fullname", $pos['modifier'] ) ?> <small><?php echo date( $this->assets->conf['datetime_format'], strtotime( $pos['modified'] ) ); ?></small></p>
						<?php endif; ?>
					</div>
				</div>
			</div>
			<?php endif; ?>
			
		</div>
	</div>
</div>