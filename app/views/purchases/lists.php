	
<?php if ( ! empty( $purchases ) ) : echo form_open( current_url() ); ?>

<div class="lists-table">
	<div class="table-responsive">

		<table class="table table-striped table-hover">
			<col width="10" class="d-print-none">
			<col width="90">
			<col width="90">
			<col width="90">
			<col width="90">
			<col width="90">
			<col width="auto">
			<col width="auto">
			<col width="140">
			<thead>
				<tr>
					<th class="d-print-none"><input type="checkbox" name="select_all_cb" class="select_all_cb" title="<?php _e( "Select All" ); ?>" /></th>
					<th><abbr title="<?php _e( "Purchase date" ); ?>"><?php _e( "Date" ); ?></abbr></th>
					<th class="d-none d-lg-table-cell"><abbr title="<?php _e( "Delivery date" ); ?>"><?php _e( "Delivery" ); ?></abbr></th>
					<th class="d-none d-lg-table-cell"><abbr title="<?php _e( "Due date" ); ?>"><?php _e( "Due" ); ?></abbr></th>
					<th class="d-none d-lg-table-cell"><?php _e( "Stocked" ); ?></th>
					<th class="d-none d-lg-table-cell"><?php _e( "Paid" ); ?></th>
					<th><?php _e( "Provider" ); ?></th>
					<th class="d-none d-lg-table-cell text-right"><?php _e( "Total amount" ); ?> <small class="text-muted">(<?php echo $this->assets->conf['currencies'][$this->assets->conf['currency']]; ?>)</small></th>
					<th class="d-print-none"></th>
				</tr>
			</thead>
			<tfoot>
				<tr>
					<th class="d-print-none"><input type="checkbox" name="select_all_cb" class="select_all_cb" title="<?php _e( "Select All" ); ?>" /></th>
					<th><abbr title="<?php _e( "Purchase date" ); ?>"><?php _e( "Date" ); ?></abbr></th>
					<th class="d-none d-lg-table-cell"><abbr title="<?php _e( "Delivery date" ); ?>"><?php _e( "Delivery" ); ?></abbr></th>
					<th class="d-none d-lg-table-cell"><abbr title="<?php _e( "Due date" ); ?>"><?php _e( "Due" ); ?></abbr></th>
					<th class="d-none d-lg-table-cell"><?php _e( "Stocked" ); ?></th>
					<th class="d-none d-lg-table-cell"><?php _e( "Paid" ); ?></th>
					<th><?php _e( "Provider" ); ?></th>
					<th class="d-none d-lg-table-cell text-right"><?php _e( "Total amount" ); ?> <small class="text-muted">(<?php echo $this->assets->conf['currencies'][$this->assets->conf['currency']]; ?>)</small></th>
					<th class="d-print-none"></th>
				</tr>
			</tfoot>
			<tbody>
				<?php 
				foreach ( $purchases AS $purchase ) : 
					$purchase = $this->assets->fill_empty_vars( $purchase );
					$buttons = $this->elements->lists_action_buttons( 'purchases', 'purchase_id', $purchase['purchase_id'], $list_buttons_extension );
					$stocked = $this->purchases->is_stocked( $purchase );
					$paid = $this->purchases->is_paid( $purchase );
				?>
				<tr>
					<td class="d-print-none"><input type="checkbox" name="cb_ids[]" value="<?php echo $purchase['purchase_id']; ?>" /></td>
					<td><?php echo ( $purchase['original']['purchase_date'] ) ? date( $this->assets->conf['date_format'], strtotime( $purchase['purchase_date'] ) ) : $purchase['purchase_date']; ?></td>
					<td class="d-none d-lg-table-cell"><?php echo ( $purchase['original']['delivery_date'] ) ? date( $this->assets->conf['date_format'], strtotime( $purchase['delivery_date'] ) ) : $purchase['delivery_date']; ?></td>
					<td class="d-none d-lg-table-cell"><?php echo ( $purchase['original']['expiration_date'] ) ? date( $this->assets->conf['date_format'], strtotime( $purchase['expiration_date'] ) ) : $purchase['expiration_date']; ?></td>
					<td class="d-none d-lg-table-cell"><span class="badge badge-<?php echo ( $stocked ) ? 'success' : 'danger'; ?>"><?php echo ( $stocked ) ? __( "Yes" ) : __( "No" ); ?></span></td>
					<td class="d-none d-lg-table-cell"><span class="badge badge-<?php echo ( $paid ) ? 'success' : 'danger'; ?>"><?php echo ( $paid ) ? __( "Yes" ) : __( "No" ); ?></span></td>
					<td class="text-wrap"><?php echo anchor( 'purchases/brief/' . $purchase['purchase_id'], '<strong>' . $purchase['provider'] . '</strong>', array( 'class' => 'brief', 'title' => __( "Open Brief" ), 'data-enlarge-modal' => 1 ) ); ?></td>
					<td class="d-none d-lg-table-cell text-right"><?php echo my_number_format( $purchase['total_amount'], $this->assets->conf['decimals'] ); ?></td>
					<td class="text-right d-print-none action-buttons">
					<?php 
					foreach ( $buttons AS $n => $b ) $buttons[$n] = ' ' . $b;
					if ( $this->uri->segment( 2 ) !== "trash" )
					{
						if ( $this->permissions->can_edit() ) echo $buttons['edit']; ?>
						<button class="btn btn-primary dropdown-toggle" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
							<i class="fas fa-cog"></i>
						</button>
						<div class="dropdown-menu dropdown-menu-right">
							<?php
							if ( $this->permissions->can_edit() AND ! $paid ) echo $buttons['mark_paid'];
							if ( $this->permissions->can_delete() AND $paid ) echo $buttons['mark_unpaid'];
							if ( $this->permissions->can_edit() ) echo ( $stocked ) ? $buttons['mark_unstocked'] : $buttons['mark_stocked'];
							?>
						</div>
						<?php if ( $this->permissions->can_trash() ) echo $buttons['trash'];
					}
					else
					{
						if ( $this->permissions->can_restore() ) echo $buttons['restore']; 
						if ( $this->permissions->can_delete() ) echo $buttons['delete'];
					}
					?>						
					</td>
				</tr>
				<?php endforeach; ?>
			</tbody>
		</table>

	</div>
</div>

<div class="row d-print-none lists-tools">

	<?php if ( count( $bulk_actions['options'] ) > 1 ) : ?>

	<div class="col form-inline">
		<div class="bulk-box">
			<?php echo form_dropdown( $bulk_actions ); ?>
			<?php echo form_submit( $submit_bulk_actions ); ?>
		</div>
	</div>

	<?php endif; if ( isset( $pagination ) ) : ?><div class="col page-navigation"><nav><?php echo $pagination; ?></nav></div><?php endif; ?>

</div>

<?php echo form_close(); else : $this->load->view( 'singles/list-empty' ); endif; ?>
