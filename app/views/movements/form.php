
<?php echo form_open( current_url(), array( 'autocomplete' => 'off', 'id' => 'form-movements' ) ); ?>

<div class="row form-elements">

	<div class="col-lg-4">
		<div class="card" id="meta-card">
			<div class="card-body">
				<div class="form-row">
					<div class="form-group col">
						<?php 
							echo form_label( __( "Date" ), 'date', array( 'class' => 'control-label' ) );
							echo $this->assets->conf['mandatory'];
							echo form_input( $date );
							echo form_error( 'date' );
						?>
					</div>
				</div>
				<div class="form-row">
					<div class="form-group col">
						<?php 
							echo form_label( __( "Type" ), 'type', array( 'class' => 'control-label' ) );
							echo $this->assets->conf['mandatory'];
						?>
						<div class="checkbox-group p-2">
							<div class="btn-group btn-group-toggle d-flex justify-content-center" data-toggle="buttons">
								<?php 
								foreach ( $this->assets->conf['movements'] AS $key => $mov ) : 
									if ( isset( $_POST['type'] ) AND $_POST['type'] == $key ) $type['selected'] = $key; 
									$set_radio = set_radio( 'type', $key, ( ! empty( $type['selected'] ) AND $type['selected'] == $key ) ? TRUE : FALSE );
								?>
								<label class="btn btn-lg btn-outline-<?php echo ( $key == "income" ) ? 'success' : 'danger'; ?> d-flex justify-content-center w-50 <?php if ( ! empty( $set_radio ) ) echo "active"; ?>" for="<?php echo $key; ?>">
									<input type="radio" name="type" id="<?php echo $key; ?>" value="<?php echo $key; ?>" <?php echo $set_radio; ?>>
									<?php echo $mov; ?>
								</label>
								<?php endforeach; ?>
							</div>
						</div>
						<?php echo form_error( 'type' ); ?>
					</div>
				</div>

			</div>
		</div>

		<div class="card" id="amount-card">
			<div class="card-body">
				<div class="form-row">
					<div class="form-group col mb-0">
						<?php 
							echo form_label( __( "Amount" ), 'amount', array( 'class' => 'control-label' ) );
							echo $this->assets->conf['mandatory'];
						?>
						<div class="input-group">
							<?php echo form_input( $amount ); ?>
							<div class="input-group-append">
								<span class="input-group-text"><?php echo $this->assets->conf['currencies'][$this->assets->conf['currency']]; ?></span>
							</div>
						</div>
						<?php echo form_error( 'amount' ); ?>
						<p class="text-right mb-0"><button type="button" class="btn btn-sm btn-link mx-0 my-0" data-toggle="collapse" data-target="#exchange-calc"><?php _e( "Exchange calculator" ); ?></button></p>
					</div>
				</div>

				<div class="collapse hide" id="exchange-calc">
					<hr class="mt-0">
					<?php foreach ( $this->assets->conf['currencies'] AS $key => $symbol ) : if ( $key !== $this->assets->conf['currency'] ) : ?>
					<div class="form-row">
						<div class="form-group col">
							<div class="input-group">
								<div class="input-group-prepend">
									<span class="input-group-text"><?php echo $this->assets->conf['currencies'][$key]; ?></span>
								</div>
								<?php echo form_input( array( 
									'class' 		=> 'form-control input-currency-foreign input-exchanges', 
									'id' 			=> 'amount_' . $key,
									'data-currency'	=> $key,
								) ); ?>
							</div>
						</div>
					</div>
					<?php endif; endforeach; ?>
				</div>
			</div>
		</div>
	</div>

	<div class="col-lg-8">
		<div class="card" id="data-card">
			<div class="card-body">
				<div class="form-row">
					<div class="form-group col">
						<?php 
							echo form_label( __( "Label" ), 'label', array( 'class' => 'control-label' ) );
							echo $this->assets->conf['mandatory'];
							echo form_dropdown( $label );
							echo form_error( 'label' );
						?>
					</div>
				</div>
				<div class="form-row">
					<div class="form-group col">
						<?php 
							echo form_label( __( "Book" ), 'book', array( 'class' => 'control-label' ) );
							echo form_dropdown( $book );
							echo form_error( 'book' );
						?>
					</div>
				</div>
				<div class="form-row">
					<div class="form-group col">
						<?php 
							echo form_label( __( "Comments" ), 'comments', array( 'class' => 'control-label' ) );
							echo form_textarea( $comments );
							echo form_error( 'comments' );
						?>
					</div>
				</div>
				<div class="form-row">
					<div class="form-group col">
						<?php 
							echo form_label( __( "Assignee" ), 'assignee', array( 'class' => 'control-label' ) );
							echo form_dropdown( $assignee );
							echo form_error( 'assignee' );
						?>
					</div>
				</div>
			</div>
		</div>
	</div>

	<div class="col-12">
		<div class="card" id="files-card">
			<div class="card-body">
				<?php $this->load->view( 'files/model-multiple-files', ( isset( $movement_id ) ) ? $this->files->get_files_src( $movement_id['value'], 'movements', FALSE ) : NULL ); ?>
			</div>
		</div>
	</div>

</div>

<div class="buttons-set">
	<button type="submit" class="btn btn-primary">
		<i class="fa fa-save"></i>
		<?php _e( "Save" ); ?>
	</button>
	<?php if ( $this->uri->segment( 2 ) == "edit" ) echo anchor( 'movements/add', '<i class="fa fa-plus-circle"></i> ' . __( "New" ), array( 'class' => 'btn btn-secondary', 'title' => __( "Add New Movement" ) ) ); ?>
</div>

<?php if ( ! empty( $movement_id ) ) echo form_input( $movement_id ); ?>
<?php echo form_close(); ?>