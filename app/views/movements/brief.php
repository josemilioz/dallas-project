
<div class="card">
	<div class="card-body">
		<div id="brief-wrapper">

			<div class="row">
				<div class="col mb-3">
					<div class="element-group row">
						<div class="col-4"><span class="item-label"><?php _e( "Date" ); ?></span></div>
						<div class="col-8"><span><?php echo ( ! empty( $movement['original']['date'] ) ) ? date( $this->assets->conf['date_format'], strtotime( $movement['date'] ) ) : $movement['date']; ?></div>
					</div>
					<div class="element-group row">
						<div class="col-4"><span class="item-label"><?php _e( "Label" ); ?></span></div>
						<div class="col-8"><span class="full-name"><?php echo $movement['label']; ?></span></div>
					</div>
					<div class="element-group row">
						<div class="col-4"><span class="item-label"><?php _e( "Book" ); ?></span></div>
						<div class="col-8"><span><?php echo $movement['book']; ?></span></div>
					</div>
					<div class="element-group row">
						<div class="col-4"><span class="item-label"><?php _e( "Comments" ); ?></span></div>
						<div class="col-8"><span><?php echo nl2br( $movement['comments'] ); ?></span></div>
					</div>
					<?php if ( ! empty( $movement['original']['assignee'] ) ) : ?>
					<div class="element-group row">
						<div class="col-4"><span class="item-label"><?php _e( "Assignee" ); ?></span></div>
						<div class="col-8"><span><?php echo $this->ion_auth->user_name( 'fullname', $movement['assignee'] ); ?></span></div>
					</div>
					<?php endif; ?>

					<?php if ( ! empty( $movement['original']['transaction_id'] ) AND ! empty( $movement['original']['transaction_type'] ) ) : ?>
					<div class="element-group row">
						<div class="col-4"><span class="item-label"><?php _e( "Associated transaction" ); ?></span></div>
						<div class="col-8"><span><?php
						switch ( $movement['transaction_type'] )
						{
							case 'purchases': $transaction_name = __( "Purchase" ); break;
							case 'sales': $transaction_name = __( "Sale" ); break;
						}

						echo anchor( 
							$movement['transaction_type']. '/brief/' . $movement['transaction_id'], 
							sprintf( __( "%s #%d" ), $transaction_name, $movement['transaction_id'] ), 
							array( 'class' => 'brief', 'title' => __( "Open Brief" ), 'data-enlarge-modal' => 1 ) 
						);
						?></span></div>
					</div>
					<?php endif; ?>

					<?php if ( ! empty( $files ) ) : ?>
					<div class="element-group row">
						<div class="col-4"><span class="item-label"><?php _e( "Attachments" ); ?></span></div>
						<div class="col-8"><span><?php 
							$attachs = array(); 
							foreach ( $files['files'] AS $file ) $attachs[] = anchor( 'files/download/' . $file['file_id'], $file['original_name'] ) . ' <small class="text-muted">(' . byte_format( $file['size'], 2 ) . ')</small>';
							echo implode( ", ", $attachs );
						?></span></div>
					</div>
					<?php endif; ?>
				</div>
			</div>

			<?php if ( ! empty( $movement['created'] ) OR ! empty( $movement['modified'] ) ) : ?>
			<div class="row last-activity">
				<div class="col">
					<div class="created">
						<?php if ( ! empty( $movement['created'] ) ) : ?>
						<p class="label"><?php _e( "Created by" ); ?></p>
						<p class="value"><?php echo $this->ion_auth->user_name( "fullname", $movement['creator'] ) ?> <small><?php echo date( $this->assets->conf['datetime_format'], strtotime( $movement['created'] ) ); ?></small></p>
						<?php endif; ?>
					</div>
				</div>
				<div class="col">
					<div class="modified">
						<?php if ( ! empty( $movement['modified'] ) ) : ?>
						<p class="label"><?php _e( "Last modified by" ); ?></p>
						<p class="value"><?php echo $this->ion_auth->user_name( "fullname", $movement['modifier'] ) ?> <small><?php echo date( $this->assets->conf['datetime_format'], strtotime( $movement['modified'] ) ); ?></small></p>
						<?php endif; ?>
					</div>
				</div>
			</div>
			<?php endif; ?>
			
		</div>
	</div>
</div>