	
<?php if ( ! empty( $movements ) ) : echo form_open( current_url() ); ?>

<div class="lists-table">
	<div class="table-responsive">

		<table class="table table-striped table-hover">
			<col width="10" class="d-print-none">
			<col width="110">
			<col width="auto">
			<col width="auto">
			<?php foreach ( $this->assets->conf['movements'] AS $k => $v ) : ?>
			<col width="150">
			<?php endforeach; ?>
			<col width="100">
			<thead>
				<tr>
					<th class="d-print-none"><input type="checkbox" name="select_all_cb" class="select_all_cb" title="<?php _e( "Select All" ); ?>" /></th>
					<th><?php _e( "Date" ); ?></th>
					<th><?php _e( "Label" ); ?></th>
					<th><?php _e( "Book" ); ?></th>
					<?php foreach ( $this->assets->conf['movements'] AS $k => $v ) : ?>
					<th class="text-right"><?php echo $v; ?> <small class="text-muted">(<?php echo $this->assets->conf['currencies'][$this->assets->conf['currency']]; ?>)</small></th>
					<?php endforeach; ?>
					<th class="d-print-none"></th>
				</tr>
			</thead>
			<tfoot>
				<tr>
					<th class="d-print-none"><input type="checkbox" name="select_all_cb" class="select_all_cb" title="<?php _e( "Select All" ); ?>" /></th>
					<th><?php _e( "Date" ); ?></th>
					<th><?php _e( "Label" ); ?></th>
					<th><?php _e( "Book" ); ?></th>
					<?php foreach ( $this->assets->conf['movements'] AS $k => $v ) : ?>
					<th class="text-right"><?php echo $v; ?> <small class="text-muted">(<?php echo $this->assets->conf['currencies'][$this->assets->conf['currency']]; ?>)</small></th>
					<?php endforeach; ?>
					<th class="d-print-none"></th>
				</tr>
			</tfoot>
			<tbody>
				<?php 
				foreach ( $movements AS $movement ) : 
					$movement = $this->assets->fill_empty_vars( $movement );
					$buttons = $this->elements->lists_action_buttons( 'movements', 'movement_id', $movement['movement_id'], $list_buttons_extension );
				?>
				<tr>
					<td class="d-print-none"><input type="checkbox" name="cb_ids[]" value="<?php echo $movement['movement_id']; ?>" /></td>
					<td><?php echo date( $this->assets->conf['date_format'], strtotime( $movement['date'] ) ); ?></td>
					<td class="text-wrap"><?php echo anchor( 'movements/brief/' . $movement['movement_id'], $movement['label'], array( 'class' => 'brief', 'title' => __( "Open Brief" ) ) ); ?></td>
					<td><?php echo $movement['book']; ?></td>
					<?php foreach ( $this->assets->conf['movements'] AS $k => $v ) : ?>
					<td class="text-right"><?php echo ( $movement['type'] == $k ) ? my_number_format( $movement['amount'], $this->assets->conf['decimals'] ) : "&mdash;"; ?></td>					
					<?php endforeach; ?>
					<td class="text-right d-print-none action-buttons">
					<?php 
					foreach ( $buttons AS $n => $b ) $buttons[$n] = ' ' . $b;
					if ( $this->uri->segment( 2 ) !== "trash" )
					{
						if ( $this->permissions->can_edit() ) echo $buttons['edit'];
						if ( $this->permissions->can_trash() ) echo $buttons['trash'];
					}
					else
					{
						if ( $this->permissions->can_restore() ) echo $buttons['restore']; 
						if ( $this->permissions->can_delete() ) echo $buttons['delete'];
					}
					?>						
					</td>
				</tr>
				<?php endforeach; ?>
			</tbody>
		</table>

	</div>
</div>

<div class="row d-print-none lists-tools">

	<?php if ( count( $bulk_actions['options'] ) > 1 ) : ?>

	<div class="col form-inline">
		<div class="bulk-box">
			<?php echo form_dropdown( $bulk_actions ); ?>
			<?php echo form_submit( $submit_bulk_actions ); ?>
		</div>
	</div>

	<?php endif; if ( isset( $pagination ) ) : ?><div class="col page-navigation"><nav><?php echo $pagination; ?></nav></div><?php endif; ?>

</div>

<?php echo form_close(); else : $this->load->view( 'singles/list-empty' ); endif; ?>
