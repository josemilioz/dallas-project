
<div class="alert-fixed">
	<div <?php if ( isset( $id ) ) echo $id; ?> class="alert alert-warning fade show d-flex align-items-stretch" role="alert">
		<?php if ( isset( $dismissible ) AND $dismissible == TRUE ) : ?>		
		<button type="button" class="close" data-dismiss="alert" aria-label="<?php _e( "Cerrar" ); ?>">
			<span aria-hidden="true">&times;</span>
		</button>
		<?php endif; ?>

		<div class="alert-icon">
			<i class="fas fa-exclamation"></i>
		</div>

		<div class="alert-message">
			<?php if ( isset( $title ) ) : ?><h5><?php echo $title; ?></h5><?php endif; ?>
			<?php if ( isset( $message ) ) : ?><p><?php echo $message; ?></p><?php endif; ?>
			<?php if ( isset( $errors ) ) echo $errors; ?>
		</div>

	</div>
</div>
