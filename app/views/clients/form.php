
<?php echo form_open( current_url(), array( 'autocomplete' => 'off', 'id' => 'form-clients' ) ); ?>

<div class="row form-elements">

	<div class="col-lg-8">
		<div class="card" id="personal-card">
			<div class="card-body">
				<h4><?php _e( "Personal Information" ); ?></h4>
				<div class="form-row">
					<div class="form-group col-md">
						<?php 
							echo form_label( __( "First Name" ), 'first_name', array( 'class' => 'control-label' ) );
							echo $this->assets->conf['mandatory'];
							echo form_input( $first_name );
							echo form_error( 'first_name' ); 
						?>
					</div>
					<div class="form-group col-md">
						<?php 
							echo form_label( __( "Last Name" ), 'last_name', array( 'class' => 'control-label' ) );
							echo form_input( $last_name );
							echo form_error( 'last_name' );
						?>
					</div>
				</div>
				<div class="form-row">
					<div class="form-group col">
						<?php 
							echo form_label( __( "RUC or ID" ), 'ruc', array( 'class' => 'control-label' ) );
							echo $this->assets->conf['mandatory'];
							echo form_input( $ruc );
							echo form_error( 'ruc' );
						?>
					</div>
					<div class="form-group col">
						<?php 
							echo form_label( __( "Birth Date" ), 'birthday', array( 'class' => 'control-label' ) );
							echo form_input( $birthday );
							echo form_error( 'birthday' );
						?>
					</div>
				</div>
				<div class="form-row">
					<div class="form-group col">
						<?php 
							echo form_label( __( "Observations" ), 'observations', array( 'class' => 'control-label' ) );
							echo form_textarea( $observations );
							echo form_error( 'observations' );
						?>
					</div>
				</div>
			</div>
		</div>

		<div class="card" id="location-card">
			<div class="card-body">
				<h4><?php _e( "Location" ); ?></h4>
				<div class="form-row">
					<div class="form-group col-md">
						<?php 
							echo form_label( __( "Country" ), 'country', array( 'class' => 'control-label' ) );
							echo form_input( $country );
							echo form_error( 'country' );
						?>
					</div>
					<div class="form-group col-md">
						<?php 
							echo form_label( __( "State / Province / Department" ), 'state', array( 'class' => 'control-label' ) );
							echo form_input( $state );
							echo form_error( 'state' );
						?>
					</div>
				</div>
				<div class="form-row">
					<div class="form-group col-md">
						<?php 
							echo form_label( __( "City" ), 'city', array( 'class' => 'control-label' ) );
							echo form_input( $city );
							echo form_error( 'city' );
						?>
					</div>
				</div>
				<div class="form-row">
					<div class="form-group col">
						<?php 
							echo form_label( __( "Address" ), 'address', array( 'class' => 'control-label' ) );
							echo form_input( $address );
							echo form_error( 'address' );
						?>
					</div>
				</div>
			</div>
		</div>
	</div>

	<div class="col-lg-4">
		<div class="card" id="picture-card">
			<div class="card-body">
				<h4><?php _e( "Picture" ); ?></h4>
				<?php $this->load->view( 'files/model-single-picture', ( isset( $client_id ) ) ? $this->files->get_files_src( $client_id['value'], 'clients' ) : NULL ); ?>
			</div>
		</div>

		<div class="card" id="contact-card">
			<div class="card-body">
				<h4><?php _e( "Contact Information" ); ?></h4>
				<div class="form-row">
					<div class="form-group col">
						<?php 
							echo form_label( __( "Phone" ), 'telephone', array( 'class' => 'control-label' ) );
							echo form_input( $telephone );
							echo form_error( 'telephone' );
						?>
					</div>
				</div>
				<div class="form-row">
					<div class="form-group col">
						<?php 
							echo form_label( __( "Cellphone" ), 'cellphone', array( 'class' => 'control-label' ) );
							echo form_input( $cellphone );
							echo form_error( 'cellphone' );
						?>
					</div>
				</div>
				<div class="form-row">
					<div class="form-group col">
						<?php 
							echo form_label( __( "Email" ), 'email', array( 'class' => 'control-label' ) );
							echo form_input( $email );
							echo form_error( 'email' );
						?>
					</div>
				</div>
				<div class="form-row">
					<div class="form-group col">
						<?php 
							echo form_label( __( "Website" ), 'website', array( 'class' => 'control-label' ) );
							echo form_input( $website );
							echo form_error( 'website' );
						?>
					</div>
				</div>
			</div>
		</div>
	</div>

</div>

<div class="buttons-set">
	<button type="submit" class="btn btn-primary">
		<i class="fa fa-save"></i>
		<?php _e( "Save" ); ?>
	</button>
	<?php if ( $this->uri->segment( 2 ) == "edit" ) echo anchor( 'clients/add', '<i class="fa fa-plus-circle"></i> ' . __( "New" ), array( 'class' => 'btn btn-secondary', 'title' => __( "Add New Client" ) ) ); ?>
</div>

<?php if ( ! empty( $client_id ) ) echo form_input( $client_id ); ?>
<?php echo form_close(); ?>