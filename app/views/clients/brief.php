
<div class="card">
	<div class="card-body">
		<div id="brief-wrapper">

			<div class="row">
				<div class="col-lg-5 mb-3">
					<div class="picture-big-container w-100 text-center">
						<img class="picture-big w-75" src="<?php echo $this->clients->picture( $client['client_id'] ); ?>" />
					</div>
				</div>
				<div class="col-lg-7 mb-3">
					<h4><?php _e( "Personal Information" ); ?></h4>
					<div class="element-group row">
						<div class="col-4"><span class="item-label"><?php _e( "Full name" ); ?></span></div>
						<div class="col-8"><span class="full-name"><?php echo $this->clients->name( $client, 'full' ); ?></span></div>
					</div>
					<div class="element-group row">
						<div class="col-4"><span class="item-label"><?php _e( "RUC or ID Number" ); ?></span></div>
						<div class="col-8"><span><code><?php echo $client['ruc']; ?></code></span></div>
					</div>
					<div class="element-group row">
						<div class="col-4"><span class="item-label"><?php _e( "Birth date" ); ?></span></div>
						<div class="col-8"><span><?php echo $client['birthday']; if ( isset( $client['age'] ) ) : ?> <small class="text-muted">(<?php echo $client['age']; ?>)</small><?php endif; ?></span></div>
					</div>
					<div class="element-group row">
						<div class="col-4"><span class="item-label"><?php _e( "Observations" ); ?></span></div>
						<div class="col-8"><span><?php echo nl2br( $client['observations'] ); ?></span></div>
					</div>
				</div>
			</div>

			<div class="row">
				<div class="col-lg-5 mb-3">
					<h4><?php _e( "Contact Information" ); ?></h4>
					<div class="element-group row">
						<div class="col-5"><span class="item-label"><?php _e( "Telephone" ); ?></span></div>
						<div class="col-7"><span><?php echo $client['telephone']; ?></span></div>
					</div>
					<div class="element-group row">
						<div class="col-5"><span class="item-label"><?php _e( "Cellphone" ); ?></span></div>
						<div class="col-7"><span><?php echo $client['cellphone']; ?></span></div>
					</div>
					<div class="element-group row">
						<div class="col-5"><span class="item-label"><?php _e( "Email" ); ?></span></div>
						<div class="col-7"><span><?php echo $client['email']; ?></span></div>
					</div>
					<div class="element-group row">
						<div class="col-5"><span class="item-label"><?php _e( "Website" ); ?></span></div>
						<div class="col-7"><span><?php echo $client['website']; ?></span></div>
					</div>
				</div>
				<div class="col-lg-7">
					<h4><?php _e( "Location" ); ?></h4>
					<div class="element-group row">
						<div class="col-4"><span class="item-label"><?php _e( "Country" ); ?></span></div>
						<div class="col-8"><span><?php echo $client['country']; ?></span></div>
					</div>
					<div class="element-group row">
						<div class="col-4"><span class="item-label"><?php _e( "State / Province / Department" ); ?></span></div>
						<div class="col-8"><span><?php echo $client['state']; ?></span></div>
					</div>
					<div class="element-group row">
						<div class="col-4"><span class="item-label"><?php _e( "City" ); ?></span></div>
						<div class="col-8"><span><?php echo $client['city']; ?></span></div>
					</div>
					<div class="element-group row">
						<div class="col-4"><span class="item-label"><?php _e( "Address" ); ?></span></div>
						<div class="col-8"><span><?php echo $client['address']; ?></span></div>
					</div>
				</div>
			</div>

			<?php if ( ! empty( $client['created'] ) OR ! empty( $client['modified'] ) ) : ?>
			<div class="row last-activity">
				<div class="col">
					<div class="created">
						<?php if ( ! empty( $client['created'] ) ) : ?>
						<p class="label"><?php _e( "Created by" ); ?></p>
						<p class="value"><?php echo $this->ion_auth->user_name( "fullname", $client['creator'] ) ?> <small><?php echo date( $this->assets->conf['datetime_format'], strtotime( $client['created'] ) ); ?></small></p>
						<?php endif; ?>
					</div>
				</div>
				<div class="col">
					<div class="modified">
						<?php if ( ! empty( $client['modified'] ) ) : ?>
						<p class="label"><?php _e( "Last modified by" ); ?></p>
						<p class="value"><?php echo $this->ion_auth->user_name( "fullname", $client['modifier'] ) ?> <small><?php echo date( $this->assets->conf['datetime_format'], strtotime( $client['modified'] ) ); ?></small></p>
						<?php endif; ?>
					</div>
				</div>
			</div>
			<?php endif; ?>
			
		</div>
	</div>
</div>