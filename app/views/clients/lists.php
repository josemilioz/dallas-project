	
<?php if ( ! empty( $clients ) ) : echo form_open( current_url() ); ?>

<div class="lists-table">
	<div class="table-responsive">

		<table class="table table-striped table-hover">
			<col width="10" class="d-print-none">
			<col width="40">
			<col width="auto">
			<col width="auto">
			<col width="auto">
			<col width="auto">
			<col width="100">
			<thead>
				<tr>
					<th class="d-print-none"><input type="checkbox" name="select_all_cb" class="select_all_cb" title="<?php _e( "Select All" ); ?>" /></th>
					<th colspan="2"><?php _e( "Client" ); ?></th>
					<th class="d-none d-lg-table-cell"><?php _e( "RUC or ID" ); ?></th>
					<th class="d-none d-lg-table-cell"><?php _e( "City" ); ?></th>
					<th class="d-none d-md-table-cell"><?php _e( "Cellphone" ); ?></th>
					<th class="d-print-none"></th>
				</tr>
			</thead>
			<tfoot>
				<tr>
					<th class="d-print-none"><input type="checkbox" name="select_all_cb" class="select_all_cb" title="<?php _e( "Select All" ); ?>" /></th>
					<th colspan="2"><?php _e( "Client" ); ?></th>
					<th class="d-none d-lg-table-cell"><?php _e( "RUC or ID" ); ?></th>
					<th class="d-none d-lg-table-cell"><?php _e( "City" ); ?></th>
					<th class="d-none d-md-table-cell"><?php _e( "Cellphone" ); ?></th>
					<th class="d-print-none"></th>
				</tr>
			</tfoot>
			<tbody>
				<?php 
				foreach ( $clients AS $client ) : 
					$client = $this->assets->fill_empty_vars( $client );
					$buttons = $this->elements->lists_action_buttons( 'clients', 'client_id', $client['client_id'], $list_buttons_extension );
				?>
				<tr>
					<td class="d-print-none"><input type="checkbox" name="cb_ids[]" value="<?php echo $client['client_id']; ?>" /></td>
					<td class="user-picture"><img src="<?php echo $this->clients->picture( $client['client_id'] ); ?>" /></td>
					<td class="text-wrap"><strong><?php echo anchor( 'clients/brief/' . $client['client_id'], $this->clients->name( $client, "full" ), array( 'class' => 'brief', 'title' => __( "Open Brief" ) ) ); ?></strong></td>
					<td class="d-none d-lg-table-cell"><?php echo $client['ruc']; ?></td>
					<td class="d-none d-lg-table-cell"><?php echo $client['city']; ?></td>
					<td class="d-none d-md-table-cell"><?php echo $client['cellphone']; ?></td>
					<td class="text-right d-print-none action-buttons">
					<?php 
					foreach ( $buttons AS $n => $b ) $buttons[$n] = ' ' . $b;
					if ( $this->uri->segment( 2 ) !== "trash" )
					{
						if ( $this->permissions->can_edit() ) echo $buttons['edit'];
						if ( $this->permissions->can_trash() ) echo $buttons['trash'];
					}
					else
					{
						if ( $this->permissions->can_restore() ) echo $buttons['restore']; 
						if ( $this->permissions->can_delete() ) echo $buttons['delete'];
					}
					?>						
					</td>
				</tr>
				<?php endforeach; ?>
			</tbody>
		</table>

	</div>
</div>

<div class="row d-print-none lists-tools">

	<?php if ( count( $bulk_actions['options'] ) > 1 ) : ?>

	<div class="col form-inline">
		<div class="bulk-box">
			<?php echo form_dropdown( $bulk_actions ); ?>
			<?php echo form_submit( $submit_bulk_actions ); ?>
		</div>
	</div>

	<?php endif; if ( isset( $pagination ) ) : ?><div class="col page-navigation"><nav><?php echo $pagination; ?></nav></div><?php endif; ?>

</div>

<?php echo form_close(); else : $this->load->view( 'singles/list-empty' ); endif; ?>
