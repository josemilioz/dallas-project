<?php  if ( ! defined( 'BASEPATH' ) ) exit( 'No direct script access allowed' );

class Files_model extends CI_Model
{
	/**
	 * Class constructor
	 */
	public function __construct()
	{
		parent::__construct();
	}

	/**
	 * Gets the item data from the DB. 
	 *
	 *	USAGE:
	 *	======
	 *	$this->model->get( array(
	 *		'where' => array(
	 *			'field1' => $value1,
	 *			'field2' => $value2
	 *		),
	 *		'or_where' => array(
	 *			'field1' => $value1,
	 *			'field2' => $value2
	 *		),
	 *		'like' => array(
	 *			'field1' => $match
	 *		),
	 *		'or_like' => array(
	 *			'field1' => $match
	 *		),
	 *		'order_by' => array(
	 *			'field' => 'ASC',
	 *			'id' => 'DESC'
	 *		),
	 *		'limit' => 20,
	 *		'offset' => 0
	 *	) );
	 *
	 * @param int/array $attr Record id or query array
	 * @return object/boolean Query result 
	 */
	public function get( $attr = NULL )
	{
		if ( ! empty( $attr ) AND is_array( $attr ) )
		{
			if ( isset( $attr['order_by'] ) AND is_array( $attr['order_by'] ) )
			{
				foreach ( $attr['order_by'] AS $field => $direction )
					$this->db->order_by( $field, $direction );
			}

			if ( isset( $attr['where_in'] ) AND is_array( $attr['where_in'] ) )
			{
				foreach ( $attr['where_in'] AS $key => $items )
					$this->db->where_in( $key, $items );
			}

			if ( isset( $attr['join'] ) AND is_array( $attr['join'] ) )
			{
				$this->db->join( $attr['join']['table'], $attr['join']['comparison'], $attr['join']['direction'] );
			}

			$limit = ( ! empty( $attr['limit'] ) ) ? $attr['limit'] : FALSE;
			$offset = ( ! empty( $attr['offset'] ) ) ? $attr['offset'] : 0;
			unset( $attr['limit'], $attr['offset'], $attr['order_by'], $attr['where_in'], $attr['join'] );

			foreach ( $attr AS $key => $val )
			{
				$this->db->{$key}( $val );
			}

			if ( ! empty( $limit ) ) $this->db->limit( $limit, $offset );
		}
		elseif ( ! empty( $attr ) )
		{
			$this->db->where( 'file_id', (int)$attr );
		}
		else
		{
			$this->db->select( "*" );
		}

		if ( $query = $this->db->get( $this->assets->conf['tables']['files'] ) AND $query->num_rows() > 0 )
		{
			if ( is_array( $attr ) OR empty( $attr ) )
			{
				return $query->result_array();
			}

			return $query->row_array();
		}

		return false;
	}
	
	/**
	 * Saves a registry or updates it
	 * 
	 * USAGE:
	 * ======
	 *
	 * When editing a registry, add the identifier id column in the array attribute
	 *
	 * @param array $data Data to be stored
	 * @return boolean/int The insert id
	 */
	public function save( array $data )
	{
		$author = $this->ion_auth->user()->row();

		// Nullify empty elements to store properly
		foreach ( $data AS $key => $val ) if ( empty( $data[$key] ) ) $data[$key] = NULL;

		// Size conversion
		if ( isset( $data['size'] ) AND ! empty( $data['size'] ) ) $data['size'] = (int)( $data['size'] * 1024 );

		if ( empty( $data['file_id'] ) )
		{
			$data['creator'] = $author->id;
			$data['created'] = unix_to_human( now(), TRUE, 'eu' );

			if ( $this->db->insert( $this->assets->conf['tables']['files'], $data ) )
			{
				$insert_id = $this->db->insert_id();
				return $insert_id;
			}
		}
		else
		{
			$data['modifier'] = $author->id;
			$data['modified'] = unix_to_human( now(), TRUE, 'eu' );

			$this->db->where( 'file_id', $data['file_id'] );
			
			if ( $this->db->update( $this->assets->conf['tables']['files'], $data ) )
			{
				return true;
			}
		}
		
		return false;
	}

	/**
	 * Removes registries from DB
	 *
	 * @param int or array $id The item id
	 * @return boolean
	 */
	public function erase( $id )
	{
		if ( is_array( $id ) )
		{
			foreach ( $id AS $v ) $this->erase_file_by_file_id( $v );
			$this->db->where_in( 'file_id', $id );
		}
		else
		{
			$this->erase_file_by_file_id( $id );
			$this->db->where( 'file_id', $id );
		}
		
		if ( $this->db->delete( $this->assets->conf['tables']['files'] ) )
		{
			return true;
		}
		
		return false;
	}

	/**
	 * Deletes associated file to the item given its file ID
	 * 
	 * @param  int $file_id The item id
	 * @return boolean
	 */
	public function erase_file_by_file_id( $file_id )
	{
		if ( $item = $this->get( $file_id ) )
		{
			$absolute_uri = FCPATH . ltrim( $item['relative_location'], "/" ) . $item['name_ext'];

			if ( is_file( $absolute_uri ) ) unlink( $absolute_uri );

			if ( strpos( $item['mimetype'], 'image' ) !== FALSE )
			{
				$absolute_uri_thumb = FCPATH . ltrim( $item['relative_location'], "/" ) . $item['name'] . "_thumb" . $item['extension'];
				if ( is_file( $absolute_uri_thumb ) ) unlink( $absolute_uri_thumb );
			}

			return true;
		}

		return false;
	}

	/**
	 * Deletes the files according to the given registry id and the controller where it came from
	 * 
	 * @param  int $registry_id    The registry id
	 * @param  string $controller  The controller name
	 * @return boolean
	 */
	public function erase_file_by_registry( $registry_id, $controller )
	{
		$files = $this->get( array(
			'where' => array(
				'registry_id' => $registry_id,
				'controller' => $controller,
			)
		) );

		if ( ! empty( $files ) )
		{
			foreach ( $files AS $file )
			{
				$this->erase( $file['file_id'] );
			}

			return true;
		}

		return false;
	}

	/**
	 * Gets the file src and id to display form
	 * 
	 * @param  int     $registry_id  The registry ID
	 * @param  int     $controller   The controller name
	 * @param  boolean $first_result Force to return only the first result
	 * @return array/boolean
	 */
	public function get_files_src( $registry_id, $controller, $first_result = TRUE )
	{
		$files = $this->get( array(
			'where' => array(
				'registry_id' => $registry_id,
				'controller' => $controller,
			),
			'order_by' => array(
				'order' => 'ASC',
				'file_id' => 'DESC',
			)
		) );

		if ( ! empty( $files ) )
		{
			$return = NULL;

			foreach ( $files AS $file )
			{
				if ( strpos( $file['mimetype'], 'image' ) !== FALSE )
				{
					$urls = array(
						'src' => site_url( $file['relative_location'] . $file['name'] . "_thumb" . $file['extension'] ),
						'src_normal' => site_url( $file['relative_location'] . $file['name_ext'] ),
					);
				}
				else
				{
					$urls = array( 
						'src' => site_url( $file['relative_location'] . $file['name_ext'] ),
					);
				}

				$return[] = array_merge( $file, $urls );
			}

			if ( ! $first_result ) return array( 'files' => $return );

			return array( 'file' => $return[0] );
		}

		return false;
	}

	/**
	 * Reorders the multiple files, specially if they're pictures (for galleries and stuff like that)
	 * 
	 * @param  array $entries The list of IDs to arrange
	 * @return boolean
	 */
	public function reorder_filelist( $entries )
	{
		if ( is_array( $entries ) AND ! empty( $entries ) )
		{
			foreach ( $entries AS $key => $file_id )
			{
				$this->files->save( array(
					'file_id' => $file_id,
					'order' => ( $key + 1 )
				) );
			}

			return true;
		}

		return false;
	}

}