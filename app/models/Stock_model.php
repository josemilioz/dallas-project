<?php  if ( ! defined( 'BASEPATH' ) ) exit( 'No direct script access allowed' );

class Stock_model extends CI_Model
{
	/**
	 * Class constructor
	 */
	public function __construct()
	{
		parent::__construct();
	}

	/**
	 * Gets the item data from the DB. 
	 *
	 *	USAGE:
	 *	======
	 *	$this->model->get( array(
	 *		'where' => array(
	 *			'field1' => $value1,
	 *			'field2' => $value2
	 *		),
	 *		'or_where' => array(
	 *			'field1' => $value1,
	 *			'field2' => $value2
	 *		),
	 *		'like' => array(
	 *			'field1' => $match
	 *		),
	 *		'or_like' => array(
	 *			'field1' => $match
	 *		),
	 *		'order_by' => array(
	 *			'field' => 'ASC',
	 *			'id' => 'DESC'
	 *		),
	 *		'limit' => 20,
	 *		'offset' => 0
	 *	) );
	 *
	 * @param int/array $attr      Record id or query array
	 * @param string    $sku_or_id Whether to use the sku column or the id column on single row queries
	 * @return object/boolean Query result 
	 */
	public function get( $attr = NULL, $sku_or_id = 'sku' )
	{
		if ( ! empty( $attr ) AND is_array( $attr ) )
		{
			if ( isset( $attr['order_by'] ) AND is_array( $attr['order_by'] ) )
			{
				foreach ( $attr['order_by'] AS $field => $direction )
					$this->db->order_by( $field, $direction );
			}

			if ( isset( $attr['where_in'] ) AND is_array( $attr['where_in'] ) )
			{
				foreach ( $attr['where_in'] AS $key => $items )
					$this->db->where_in( $key, $items );
			}

			if ( isset( $attr['join'] ) AND is_array( $attr['join'] ) )
			{
				$this->db->join( $attr['join']['table'], $attr['join']['comparison'], $attr['join']['direction'] );
			}

			$limit = ( ! empty( $attr['limit'] ) ) ? $attr['limit'] : FALSE;
			$offset = ( ! empty( $attr['offset'] ) ) ? $attr['offset'] : 0;
			unset( $attr['limit'], $attr['offset'], $attr['order_by'], $attr['where_in'], $attr['join'] );

			foreach ( $attr AS $key => $val )
			{
				$this->db->{$key}( $val );
			}

			if ( ! empty( $limit ) ) $this->db->limit( $limit, $offset );
		}
		elseif ( ! empty( $attr ) )
		{
			$this->db->where( $sku_or_id, $attr );
		}
/*
		Commented so the select can be extended outside of the function
		Anyway, it gets added by default in case no selected is invoked.
		Should be considered to be removed from the other models too.

		else if ( strpos( $this->db->get_compiled_select( $this->assets->conf['tables']['stock'] ), "SELECT *" ) === TRUE )
		{
			$this->db->select( "*" );
		}
*/
		if ( $query = $this->db->get( $this->assets->conf['tables']['stock'] ) AND $query->num_rows() > 0 )
		{
			if ( is_array( $attr ) OR empty( $attr ) )
			{
				return $query->result_array();
			}

			return $query->row_array();
		}

		return false;
	}
	
	/**
	 * Saves a registry or updates it
	 * 
	 * USAGE:
	 * ======
	 *
	 * When editing a registry, add the identifier id column in the array attribute
	 *
	 * @param array $data Data to be stored
	 * @return boolean
	 */
	public function save( array $data )
	{
		$author = $this->ion_auth->user()->row();

		// Nullify empty elements to store properly
		foreach ( $data AS $key => $val ) if ( empty( $data[$key] ) ) $data[$key] = NULL;

		// Expiration date format
		if ( isset( $data['expiration_date_submit'] ) ) $data['expiration_date'] = $data['expiration_date_submit'];

		// Unset unnecessary columns
		unset( $data['expiration_date_submit'], $data['file'], $data['file_id'] );

		if ( empty( $data['stock_id'] ) )
		{
			if ( empty( $data['edit'] ) )
			{
				$data['creator'] = $author->id;
				$data['created'] = unix_to_human( now(), TRUE, 'eu' );

				if ( $this->db->insert( $this->assets->conf['tables']['stock'], $data ) )
				{
					return $this->db->insert_id();
				}
			}
			else
			{
				unset( $data['edit'] );
				$data['modifier'] = $author->id;
				$data['modified'] = unix_to_human( now(), TRUE, 'eu' );

				$this->db->where( 'sku', $data['sku'] );
				
				if ( $this->db->update( $this->assets->conf['tables']['stock'], $data ) )
				{
					return true;
				}
			}			
		}
		else
		{
			unset( $data['edit'] );
			$data['modifier'] = $author->id;
			$data['modified'] = unix_to_human( now(), TRUE, 'eu' );

			$this->db->where( 'stock_id', $data['stock_id'] );
			
			if ( $this->db->update( $this->assets->conf['tables']['stock'], $data ) )
			{
				return true;
			}			
		}
		
		return false;
	}

	/**
	 * Removes registries from DB
	 *
	 * @param int or array $id The item id
	 * @return boolean
	 */
	public function erase( $id )
	{
		if ( is_array( $id ) )
		{
			$this->db->where_in( 'stock_id', $id );
		}
		else
		{
			$this->db->where( 'stock_id', $id );
		}
		
		if ( $this->db->delete( $this->assets->conf['tables']['stock'] ) )
		{
			return true;
		}
		
		return false;
	}

	/**
	 * Counts the total quantity for the current product
	 * 
	 * @param  int $product_id
	 * @return int/boolean
	 */
	public function total_quantity_by_product( $product_id )
	{
		$this->db->select( "SUM( quantity ) AS total_quantity" );
		$this->db->where( 'product_id', $product_id );
		$this->db->where( 'trash', 0 );
		
		if ( $query = $this->db->get( $this->assets->conf['tables']['stock'] ) AND $query->num_rows() > 0 )
		{
			$result = $query->row_array();
			return (int)$result['total_quantity'];
		}

		return false;
	}

	/**
	 * Build array for sales dropdown
	 * 
	 * @return array/boolean
	 */
	public function build_dropdown_array()
	{
		$this->load->model( 'Categories_model', 'categories' );
		$this->load->model( 'Products_model', 'products' );

		$categories = $this->categories->get( array(
			'where' => array(
				'trash' => 0
			)
		) );

		if ( ! empty( $categories ) )
		{
			$return = array( '' => '&nbsp;' );

			foreach ( $categories AS $category )
			{
				$products_ids = $this->products->get_products_ids_by_category( $category['category_id'] );

				if ( ! empty( $products_ids ) )
				{
					foreach ( $products_ids AS $product_id )
					{
						$skus = $this->get( array(
							'where' => array(
								'product_id' => $product_id,
								'trash' => 0,
							)
						) );

						if ( ! empty( $skus ) )
						{
							foreach ( $skus AS $sku )
							{
								$product = $this->products->get( $sku['product_id'] );
								if ( $product['trash'] == "0" ) $return[$category['name']][$sku['sku']] = $product['name'] . " (" . $sku['sku'] . ")" ;
							}
						}
					}
				}
			}

			return $return;
		}

		return false;
	}

	/**
	 * JSON builder for skus in the database
	 * 
	 * @return object/boolean
	 */
	public function build_json_skus()
	{
		$this->load->model( "Products_model", "products" );

		$skus = $this->get( array(
			'where' => array(
				'trash' => 0,
			)
		) );

		if ( ! empty( $skus ) )
		{
			$return = array();

			foreach ( $skus AS $sku )
			{
				// Dispose unnecessary
				unset( $sku['trash'] );

				$product = $this->products->get_by_sku( $sku['sku'] );
				$return[$sku['sku']] = $sku;
				if ( ! empty( $product ) )
				{
					$return[$sku['sku']]['name'] = $product['name'];
					$return[$sku['sku']]['unit_measure'] = $product['unit_measure'];
					$return[$sku['sku']]['unit_quantity'] = $product['unit_quantity'];
					$return[$sku['sku']]['price'] = $product['price'];
					$return[$sku['sku']]['min_price'] = $product['min_price'];
					$return[$sku['sku']]['max_price'] = $product['max_price'];
					$return[$sku['sku']]['currency'] = $product['currency'];
					$return[$sku['sku']]['tags'] = array(
						'currency' => $this->assets->conf['currencies'][$product['currency']],
						'unit_measure' => $this->assets->conf['units'][$product['unit_measure']],
					);
				}
			}

			return json_encode( $return );
		}

		return '{}';
	}

	/**
	 * Restores the stock of a given sale by its ID
	 * 
	 * @param  int $sale_id The sale ID
	 * @return boolean
	 */
	public function restore_stock_from_sale( $sale_id )
	{
		$this->load->model( "Sales_model", "sales" );

		$metas = $this->sales->meta_get( $sale_id );

		if ( ! empty( $metas ) )
		{
			foreach ( $metas AS $meta )
			{
				if ( ! empty( $meta['stock_id'] ) AND ! empty( $meta['sku'] ) )
				{
					$stock = $this->get( $meta['stock_id'], 'stock_id' );

					if ( ! empty( $stock ) )
					{
						$store = array(
							'stock_id'	=> $meta['stock_id'],
							'quantity'	=> (float)( (float)$stock['quantity'] + (float)$meta['quantity'] ),
						);

						if ( $this->save( $store ) )
						{
							$store['sale_id'] = $sale_id;
							$store['action'] = "restore_stock_from_sale";
							$this->logs->set_data( $store, "POST" )->set_result( TRUE )->register();
						}
						else
						{
							$store['sale_id'] = $sale_id;
							$store['action'] = "restore_stock_from_sale";
							$this->logs->set_data( $store, "POST" )->set_result( FALSE )->register();
						}
					}					
				}
			}

			return true;
		}

		return false;
	}

	/**
	 * Subtract the stock of a given sale by its ID
	 * 
	 * @param  int $sale_id The sale ID
	 * @return boolean
	 */
	public function subtract_stock_from_sale( $sale_id )
	{
		$this->load->model( "Sales_model", "sales" );

		$metas = $this->sales->meta_get( $sale_id );

		if ( ! empty( $metas ) )
		{
			foreach ( $metas AS $meta )
			{
				if ( ! empty( $meta['stock_id'] ) AND ! empty( $meta['sku'] ) )
				{
					$stock = $this->get( $meta['stock_id'], 'stock_id' );

					if ( ! empty( $stock ) )
					{
						$subtraction = (float)( (float)$stock['quantity'] - (float)$meta['quantity'] );

						if ( $subtraction < 0 ) $subtraction = 0;

						$store = array(
							'stock_id'	=> $meta['stock_id'],
							'quantity'	=> $subtraction,
						);

						if ( $this->save( $store ) )
						{
							$store['sale_id'] = $sale_id;
							$store['action'] = "restore_stock_from_sale";
							$this->logs->set_data( $store, "POST" )->set_result( TRUE )->register();
						}
						else
						{
							$store['sale_id'] = $sale_id;
							$store['action'] = "restore_stock_from_sale";
							$this->logs->set_data( $store, "POST" )->set_result( FALSE )->register();
						}
					}
				}
			}

			return true;
		}

		return false;
	}

}
