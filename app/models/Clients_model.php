<?php  if ( ! defined( 'BASEPATH' ) ) exit( 'No direct script access allowed' );

class Clients_model extends CI_Model
{
	/**
	 * Class constructor
	 */
	public function __construct()
	{
		parent::__construct();
	}

	/**
	 * Gets the item data from the DB. 
	 *
	 *	USAGE:
	 *	======
	 *	$this->model->get( array(
	 *		'where' => array(
	 *			'field1' => $value1,
	 *			'field2' => $value2
	 *		),
	 *		'or_where' => array(
	 *			'field1' => $value1,
	 *			'field2' => $value2
	 *		),
	 *		'like' => array(
	 *			'field1' => $match
	 *		),
	 *		'or_like' => array(
	 *			'field1' => $match
	 *		),
	 *		'order_by' => array(
	 *			'field' => 'ASC',
	 *			'id' => 'DESC'
	 *		),
	 *		'limit' => 20,
	 *		'offset' => 0
	 *	) );
	 *
	 * @param int/array $attr Record id or query array
	 * @return object/boolean Query result 
	 */
	public function get( $attr = NULL )
	{
		if ( ! empty( $attr ) AND is_array( $attr ) )
		{
			if ( isset( $attr['order_by'] ) AND is_array( $attr['order_by'] ) )
			{
				foreach ( $attr['order_by'] AS $field => $direction )
					$this->db->order_by( $field, $direction );
			}

			if ( isset( $attr['where_in'] ) AND is_array( $attr['where_in'] ) )
			{
				foreach ( $attr['where_in'] AS $key => $items )
					$this->db->where_in( $key, $items );
			}

			if ( isset( $attr['join'] ) AND is_array( $attr['join'] ) )
			{
				$this->db->join( $attr['join']['table'], $attr['join']['comparison'], $attr['join']['direction'] );
			}

			$limit = ( ! empty( $attr['limit'] ) ) ? $attr['limit'] : FALSE;
			$offset = ( ! empty( $attr['offset'] ) ) ? $attr['offset'] : 0;
			unset( $attr['limit'], $attr['offset'], $attr['order_by'], $attr['where_in'], $attr['join'] );

			foreach ( $attr AS $key => $val )
			{
				$this->db->{$key}( $val );
			}

			if ( ! empty( $limit ) ) $this->db->limit( $limit, $offset );
		}
		elseif ( ! empty( $attr ) )
		{
			$this->db->where( 'client_id', (int)$attr );
		}
		else
		{
			$this->db->select( "*" );
		}

		if ( $query = $this->db->get( $this->assets->conf['tables']['clients'] ) AND $query->num_rows() > 0 )
		{
			if ( is_array( $attr ) OR empty( $attr ) )
			{
				return $query->result_array();
			}

			return $query->row_array();
		}

		return false;
	}
	
	/**
	 * Saves a registry or updates it
	 * 
	 * USAGE:
	 * ======
	 *
	 * When editing a registry, add the identifier id column in the array attribute
	 *
	 * @param array $data Data to be stored
	 * @return boolean/int The insert id
	 */
	public function save( array $data )
	{
		$author = $this->ion_auth->user()->row();

		// Nullify empty elements to store properly
		foreach ( $data AS $key => $val ) if ( empty( $data[$key] ) ) $data[$key] = NULL;

		// Addapt dates from picker
		if ( isset( $data['birthday_submit'] ) AND ! empty( $data['birthday_submit'] ) ) $data['birthday'] = $data['birthday_submit'];

		// Addapt text capitalization
		if ( isset( $data['first_name'] ) )		$data['first_name'] 	= mb_convert_case( $data['first_name'], MB_CASE_TITLE, "UTF-8" );
		if ( isset( $data['last_name'] ) ) 		$data['last_name'] 		= mb_convert_case( $data['last_name'], MB_CASE_TITLE, "UTF-8" );
		if ( isset( $data['nationality'] ) ) 	$data['nationality'] 	= mb_convert_case( $data['nationality'], MB_CASE_TITLE, "UTF-8" );
		if ( isset( $data['country'] ) ) 		$data['country'] 		= mb_convert_case( $data['country'], MB_CASE_TITLE, "UTF-8" );
		if ( isset( $data['state'] ) ) 			$data['state'] 			= mb_convert_case( $data['state'], MB_CASE_TITLE, "UTF-8" );
		if ( isset( $data['city'] ) ) 			$data['city'] 			= mb_convert_case( $data['city'], MB_CASE_TITLE, "UTF-8" );
		if ( isset( $data['address'] ) ) 		$data['address'] 		= mb_convert_case( $data['address'], MB_CASE_TITLE, "UTF-8" );
		if ( isset( $data['website'] ) ) 		$data['website'] 		= prep_url( $data['website'] );

		// Remove symbols
		if ( isset( $data['first_name'] ) ) 	$data['first_name'] 	= str_replace( $this->assets->conf['company_names']['to_remove'], '', $data['first_name'] );
		if ( isset( $data['last_name'] ) ) 		$data['last_name'] 		= str_replace( $this->assets->conf['company_names']['to_remove'], '', $data['last_name'] );

		// Appendix replacement for company names
		if ( isset( $data['first_name'] ) ) 	$data['first_name'] 	= str_replace( $this->assets->conf['company_names']['matches'], $this->assets->conf['company_names']['replaces'], $data['first_name'] );
		if ( isset( $data['last_name'] ) ) 		$data['last_name'] 		= str_replace( $this->assets->conf['company_names']['matches'], $this->assets->conf['company_names']['replaces'], $data['last_name'] );

		// Default seller in case no one is specified
		if ( empty( $data['seller_id'] ) ) 		$data['seller_id'] 		= $author->id;

		// Unset unnecessary columns
		unset( $data['birthday_submit'], $data['file'], $data['file_id'] );

		if ( empty( $data['client_id'] ) )
		{
			$data['creator'] = $author->id;
			$data['created'] = unix_to_human( now(), TRUE, 'eu' );

			if ( $this->db->insert( $this->assets->conf['tables']['clients'], $data ) )
			{
				$insert_id = $this->db->insert_id();
				return $insert_id;
			}
		}
		else
		{
			$data['modifier'] = $author->id;
			$data['modified'] = unix_to_human( now(), TRUE, 'eu' );

			$this->db->where( 'client_id', $data['client_id'] );

			if ( $this->db->update( $this->assets->conf['tables']['clients'], $data ) )
			{
				return true;
			}
		}
		
		return false;
	}

	/**
	 * Removes registries from DB
	 *
	 * @param int or array $id The item id
	 * @return boolean
	 */
	public function erase( $id )
	{
		$this->load->model( "Files_model", "files" );

		if ( is_array( $id ) )
		{
			foreach ( $id AS $v ) $this->files->erase_file_by_registry( $v, 'clients' );
			$this->db->where_in( 'client_id', $id );
		}
		else
		{
			$this->files->erase_file_by_registry( $id, 'clients' );
			$this->db->where( 'client_id', $id );
		}
		
		if ( $this->db->delete( $this->assets->conf['tables']['clients'] ) )
		{
			return true;
		}
		
		return false;
	}

	/**
	 * Returns a formatted version of the client name
	 * 
	 * @param  int/array $client The client id or the client array
	 * @param  string    $format The defined format to return
	 * @return string
	 */
	public function name( $client, $format = "short" )
	{
		if ( is_int( $client ) OR ! is_array( $client ) ) $client = $this->get( (int)$client );
		$name = "";

		if ( trim( $client['last_name'] ) == trim( $this->assets->conf['no_data'] ) ) $client['last_name'] = NULL;

		switch ( $format )
		{
			default:
			case 'short': $name = $client['first_name']; break;
			case 'full': $name = $client['first_name'] . " " . $client['last_name']; break;
			case 'last_name_letter': $name = $client['first_name'] . " " . substr( $client['last_name'], 0, 1 ) . "."; break;
			case 'first_name_letter': $name = substr( $client['first_name'], 0, 1 ) . ". " .$client['last_name']; break;
			case 'first': $name = $client['first_name']; break;
			case 'last': $name = $client['last_name']; break;
			case 'full_backwards': $name = mb_strtoupper( $client['last_name'], "UTF-8" ) . ", " . $client['first_name']; break;
		}

		// Remove leading commas if existant
		if ( strpos( $name, ', ' ) === 0 ) $name = substr( $name, 2 );

		return trim( $name );
	}

	/**
	 * Returns a formatted version of the address composed
	 * 
	 * @param  int/array $client Client ID or Array
	 * @param  string    $format Format desired
	 * @return string
	 */
	public function address( $client, $format = "bill" )
	{
		if ( is_int( $client ) OR ! is_array( $client ) ) $client = $this->get( (int)$client );
		$return = "";

		switch ( $format )
		{
			default:
			case 'bill': 
				$return = $client['city']; 
				if ( ! empty( $return ) ) $return .= ", ";
				$return .= $client['country']; 

				if ( $client['city'] == $this->assets->conf['no_data'] ) $return = $client['city'];
				break;
			case 'city': 				
				$return = $client['address']; 
				if ( ! empty( $return ) ) $return .= ", ";
				$return .= $client['city']; 
				if ( ! empty( $return ) ) $return .= ", ";
				$return .= $client['country']; 
				break;
		}

		return trim( $return );
	}

	/**
	 * Returns an url for the client picture. By default, uses default no avatar
	 * 
	 * @param  int/array $client_id Client ID
	 * @param  string $size         The size option (thumb or normal)
	 * @return string
	 */
	public function picture( $client_id, $size = "thumb" )
	{
		$this->load->model( "Files_model", "files" );

		$picture = $this->files->get( array(
			'where' => array(
				'registry_id' => $client_id,
				'controller' => 'clients',
			)
		) );

		$return = $this->assets->conf['no_avatar'];

		if ( ! empty( $picture[0] ) )
		{
			switch ( $size )
			{
				default: case 'thumb':
					$filename = ltrim( $picture[0]['relative_location'], "/" ) . $picture[0]['name'] . "_thumb" . $picture[0]['extension'];
					break;
				case 'normal':
					$filename = ltrim( $picture[0]['relative_location'], "/" ) . $picture[0]['name_ext'];
					break;
			}

			if ( is_file( FCPATH . $filename ) ) $return = site_url( $filename );
		}

		return $return;
	}

	/**
	 * Returns the built array for dropdowns use
	 * 
	 * @return array/boolean
	 */
	public function build_dropdown()
	{
		$clients = $this->get( array(
			'order_by' => array(
				'last_name' => 'ASC',
				'first_name' => 'ASC',
			)
		) );

		if ( ! empty( $clients ) )
		{
			$result = array( '' => '&nbsp;' );

			foreach ( $clients AS $client )
			{
				$result[$client['client_id']] = $this->name( $client, 'full_backwards' );
			}

			return $result;
		}

		return false;
	}
}
