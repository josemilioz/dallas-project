<?php  if ( ! defined( 'BASEPATH' ) ) exit( 'No direct script access allowed' );

class Purchases_model extends CI_Model
{
	/**
	 * Class constructor
	 */
	public function __construct()
	{
		parent::__construct();
	}

	/**
	 * Gets the item data from the DB. 
	 *
	 *	USAGE:
	 *	======
	 *	$this->model->get( array(
	 *		'where' => array(
	 *			'field1' => $value1,
	 *			'field2' => $value2
	 *		),
	 *		'or_where' => array(
	 *			'field1' => $value1,
	 *			'field2' => $value2
	 *		),
	 *		'like' => array(
	 *			'field1' => $match
	 *		),
	 *		'or_like' => array(
	 *			'field1' => $match
	 *		),
	 *		'order_by' => array(
	 *			'field' => 'ASC',
	 *			'id' => 'DESC'
	 *		),
	 *		'limit' => 20,
	 *		'offset' => 0
	 *	) );
	 *
	 * @param int/array $attr Record id or query array
	 * @return object/boolean Query result 
	 */
	public function get( $attr = NULL )
	{
		if ( ! empty( $attr ) AND is_array( $attr ) )
		{
			if ( isset( $attr['order_by'] ) AND is_array( $attr['order_by'] ) )
			{
				foreach ( $attr['order_by'] AS $field => $direction )
					$this->db->order_by( $field, $direction );
			}

			if ( isset( $attr['where_in'] ) AND is_array( $attr['where_in'] ) )
			{
				foreach ( $attr['where_in'] AS $key => $items )
					$this->db->where_in( $key, $items );
			}

			if ( isset( $attr['join'] ) AND is_array( $attr['join'] ) )
			{
				$this->db->join( $attr['join']['table'], $attr['join']['comparison'], $attr['join']['direction'] );
			}

			$limit = ( ! empty( $attr['limit'] ) ) ? $attr['limit'] : FALSE;
			$offset = ( ! empty( $attr['offset'] ) ) ? $attr['offset'] : 0;
			unset( $attr['limit'], $attr['offset'], $attr['order_by'], $attr['where_in'], $attr['join'] );

			foreach ( $attr AS $key => $val )
			{
				$this->db->{$key}( $val );
			}

			if ( ! empty( $limit ) ) $this->db->limit( $limit, $offset );
		}
		elseif ( ! empty( $attr ) )
		{
			$this->db->where( 'purchase_id', (int)$attr );
		}
		else
		{
			$this->db->select( "*" );
		}

		if ( $query = $this->db->get( $this->assets->conf['tables']['purchases'] ) AND $query->num_rows() > 0 )
		{
			if ( is_array( $attr ) OR empty( $attr ) )
			{
				return $query->result_array();
			}

			return $query->row_array();
		}

		return false;
	}
	
	/**
	 * Saves a registry or updates it
	 * 
	 * USAGE:
	 * ======
	 *
	 * When editing a registry, add the identifier id column in the array attribute
	 *
	 * @param array $data Data to be stored
	 * @return boolean/int The insert id
	 */
	public function save( array $data )
	{
		$author = $this->ion_auth->user()->row();

		// Nullify empty elements to store properly
		foreach ( $data AS $key => $val ) if ( empty( $data[$key] ) ) $data[$key] = NULL;

		// Prepare metas
		$product_id 	= ( isset( $data['list_product_id'] ) ) 	? $data['list_product_id'] 	: NULL;
		$description 	= ( isset( $data['list_description'] ) ) 	? $data['list_description'] : NULL;
		$quantity 		= ( isset( $data['list_quantity'] ) ) 		? $data['list_quantity'] 	: NULL;
		$unit_price 	= ( isset( $data['list_unit_price'] ) ) 	? $data['list_unit_price'] 	: NULL;
		$row_total 		= ( isset( $data['list_row_total'] ) ) 		? $data['list_row_total'] 	: NULL;

		unset( $data['list_product_id'], $data['list_description'], $data['list_quantity'], $data['list_unit_price'], $data['list_row_total'] );

		// Addapt dates from picker
		if ( ! empty( $data['purchase_date_submit'] ) ) $data['purchase_date'] = $data['purchase_date_submit'];
		if ( ! empty( $data['delivery_date_submit'] ) ) $data['delivery_date'] = $data['delivery_date_submit'];
		if ( ! empty( $data['expiration_date_submit'] ) ) $data['expiration_date'] = $data['expiration_date_submit'];

		unset( $data['purchase_date_submit'], $data['delivery_date_submit'], $data['expiration_date_submit'] );
		unset( $data['file'], $data['file_id'] );

		// Names
		if ( ! empty( $data['provider'] ) ) $data['provider'] = mb_convert_case( $data['provider'], MB_CASE_UPPER, "UTF-8" );

		if ( empty( $data['purchase_id'] ) )
		{
			$data['creator'] = $author->id;
			$data['created'] = unix_to_human( now(), TRUE, 'eu' );

			if ( $this->db->insert( $this->assets->conf['tables']['purchases'], $data ) )
			{
				$insert_id = $this->db->insert_id();
				// Store metas
				if ( ! empty( $product_id ) AND ! empty( $description ) AND ! empty( $quantity ) AND ! empty( $unit_price ) AND ! empty( $row_total ) )
				{
					$this->meta_save( $insert_id, $product_id, $description, $quantity, $unit_price, $row_total );
				}
				return $insert_id;
			}
		}
		else
		{
			$data['modifier'] = $author->id;
			$data['modified'] = unix_to_human( now(), TRUE, 'eu' );

			$this->db->where( 'purchase_id', $data['purchase_id'] );
			
			if ( $this->db->update( $this->assets->conf['tables']['purchases'], $data ) )
			{
				// Erase previous metas and store again
				if ( ! empty( $product_id ) AND ! empty( $quantity ) AND ! empty( $unit_price ) )
				{
					$this->meta_erase( $data['purchase_id'] );
					$this->meta_save( $data['purchase_id'], $product_id, $description, $quantity, $unit_price, $row_total );
				}

				return true;
			}
		}
		
		return false;
	}

	/**
	 * Removes registries from DB
	 *
	 * @param int or array $id The item id
	 * @return boolean
	 */
	public function erase( $id )
	{
		$this->load->model( "Files_model", "files" );

		if ( is_array( $id ) )
		{
			foreach ( $id AS $v ) $this->files->erase_file_by_registry( $v, 'purchases' );
			$this->db->where_in( 'purchase_id', $id );
		}
		else
		{
			$this->files->erase_file_by_registry( $id, 'purchases' );
			$this->db->where( 'purchase_id', $id );
		}
		
		if ( $this->db->delete( $this->assets->conf['tables']['purchases'] ) )
		{
			return true;
		}
		
		return false;
	}

	/**
	 * Stores the meta we want to associate to the purchase_id
	 * 
	 * @param  int    $purchase_id The purchase ID
	 * @param  array  $product_id
	 * @param  array  $description
	 * @param  array  $quantity
	 * @param  array  $unit_price
	 * @param  array  $row_total
	 * @return void
	 */
	public function meta_save( $purchase_id, array $product_id, array $description, array $quantity, array $unit_price, array $row_total )
	{
		foreach ( $product_id AS $k => $pid )
		{
			$new_row = array(
				'purchase_id' 	=> $purchase_id,
				'product_id' 	=> $pid,
				'description' 	=> $description[$k],
				'quantity' 		=> $quantity[$k],
				'unit_price' 	=> $unit_price[$k],
				'row_total' 	=> $row_total[$k],
			);

			$this->db->insert( $this->assets->conf['tables']['purchases_products'], $new_row );
		}
	}

	/**
	 * Gets all the meta associated to the purchase_id given
	 * 
	 * @param  int        $purchase_id The purchase id
	 * @param  array/null $extra_attr  Extra parameters to refine the query
	 * @return array/boolean
	 */
	public function meta_get( $purchase_id, array $extra_attr = NULL )
	{
		if ( isset( $extra_attr ) )
		{
			foreach ( $extra_attr AS $key => $val )
			{
				$this->db->{$key}( $val );
			}
		}

		$this->db->where( 'purchase_id', $purchase_id );

		if ( $query = $this->db->get( $this->assets->conf['tables']['purchases_products'] ) AND $query->num_rows() > 0 )
		{
			return $query->result_array();
		}

		return false;		
	}

	/**
	 * Erases all the meta associated to the purchase_id given
	 * 
	 * @param  int $purchase_id The purchase id
	 * @return boolean
	 */
	public function meta_erase( $purchase_id )
	{
		$this->db->where( 'purchase_id', $purchase_id );
		if ( $this->db->delete( $this->assets->conf['tables']['purchases_products'] ) )
		{
			return true;
		}
		
		return false;
	}

	/**
	 * Edits the given purchase meta by its ID
	 * 
	 * @param  array  $data The data to store
	 * @return boolean
	 */
	public function meta_edit( array $data )
	{
		if ( ! empty( $data['purchase_meta_id'] ) )
		{
			$this->db->where( 'purchase_meta_id', $data['purchase_meta_id'] );
			
			if ( $this->db->update( $this->assets->conf['tables']['purchases_products'], $data ) )
			{
				return true;
			}
		}

		return false;
	}

	/**
	 * Builds the array of providers and don't repeat
	 * 
	 * @return array
	 */
	public function build_dropdown_providers()
	{
		$this->db->select( 'provider' );
		$this->db->distinct();
		$this->db->order_by( 'provider', 'ASC' );

		$return = array( '' => '&nbsp;' );

		if ( $query = $this->db->get( $this->assets->conf['tables']['purchases'] ) AND $query->num_rows() > 0 )
		{
			foreach ( $query->result_array() AS $r )
			{
				$return[$r['provider']] = $r['provider'];
			}
		}

		return $return;
	}

	/**
	 * Prepares the data to iterate every row, whether it coming from an actual meta or coming from POST
	 * 
	 * @param  array  $data The data
	 * @return array
	 */
	public function prepare_meta( array $data )
	{
		$this->load->model( "Products_model", "products" );

		if ( isset( $data['list_product_id'] ) AND ! empty( $data ) ) // Only if we are not editing
		{
			$rows = array();

			foreach ( $data['list_product_id'] AS $k => $reference )
			{
				$product = $this->products->get( $data['list_product_id'][$k] );

				$rows[$k]['product_id'] 	= $data['list_product_id'][$k];
				$rows[$k]['description'] 	= ( empty( $row['list_description'][$k] ) ) ? $product['name'] : $data['list_description'][$k];
				$rows[$k]['unit_price'] 	= $data['list_unit_price'][$k];
				$rows[$k]['quantity'] 		= $data['list_quantity'][$k];
				$rows[$k]['row_total'] 		= $data['list_row_total'][$k];
			}

			$data = $rows;
		}

		return $data;
	}

	/**
	 * Checks whether a purchase has been stocked already or not
	 * 
	 * @param  int/array $purchase The purchase or its ID
	 * @return boolean
	 */
	public function is_stocked( $purchase )
	{
		$purchase = ( is_array( $purchase ) AND isset( $purchase['purchase_id'] ) ) ? $purchase : $this->get( $purchase );

		if ( ! empty( $purchase ) )
		{
			if ( isset( $purchase['stocked'] ) AND $purchase['stocked'] == 1 ) return true;
		}

		return false;
	}

	/**
	 * Checks whether a purchase has been paid already or not
	 * 
	 * @param  int/array $purchase The purchase or its ID
	 * @return boolean
	 */
	public function is_paid( $purchase )
	{
		$this->load->model( "Movements_model", "movements" );

		$purchase = ( is_array( $purchase ) AND isset( $purchase['purchase_id'] ) ) ? $purchase : $this->get( $purchase );
		
		if ( ! empty( $purchase ) )
		{
			$movement = $this->movements->get( array(
				'where' => array(
					'transaction_type' => 'purchases',
					'transaction_id' => $purchase['purchase_id'],
				)
			) );

			if ( ! empty( $movement ) ) return true;
		}

		return false;
	}

}
