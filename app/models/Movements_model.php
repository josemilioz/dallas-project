<?php  if ( ! defined( 'BASEPATH' ) ) exit( 'No direct script access allowed' );

class Movements_model extends CI_Model
{
	/**
	 * Class constructor
	 */
	public function __construct()
	{
		parent::__construct();
	}

	/**
	 * Gets the item data from the DB. 
	 *
	 *	USAGE:
	 *	======
	 *	$this->model->get( array(
	 *		'where' => array(
	 *			'field1' => $value1,
	 *			'field2' => $value2
	 *		),
	 *		'or_where' => array(
	 *			'field1' => $value1,
	 *			'field2' => $value2
	 *		),
	 *		'like' => array(
	 *			'field1' => $match
	 *		),
	 *		'or_like' => array(
	 *			'field1' => $match
	 *		),
	 *		'order_by' => array(
	 *			'field' => 'ASC',
	 *			'id' => 'DESC'
	 *		),
	 *		'limit' => 20,
	 *		'offset' => 0
	 *	) );
	 *
	 * @param int/array $attr Record id or query array
	 * @return object/boolean Query result 
	 */
	public function get( $attr = NULL )
	{
		if ( ! empty( $attr ) AND is_array( $attr ) )
		{
			if ( isset( $attr['order_by'] ) AND is_array( $attr['order_by'] ) )
			{
				foreach ( $attr['order_by'] AS $field => $direction )
					$this->db->order_by( $field, $direction );
			}

			if ( isset( $attr['where_in'] ) AND is_array( $attr['where_in'] ) )
			{
				foreach ( $attr['where_in'] AS $key => $items )
					$this->db->where_in( $key, $items );
			}

			if ( isset( $attr['join'] ) AND is_array( $attr['join'] ) )
			{
				$this->db->join( $attr['join']['table'], $attr['join']['comparison'], $attr['join']['direction'] );
			}

			$limit = ( ! empty( $attr['limit'] ) ) ? $attr['limit'] : FALSE;
			$offset = ( ! empty( $attr['offset'] ) ) ? $attr['offset'] : 0;
			unset( $attr['limit'], $attr['offset'], $attr['order_by'], $attr['where_in'], $attr['join'] );

			foreach ( $attr AS $key => $val )
			{
				$this->db->{$key}( $val );
			}

			if ( ! empty( $limit ) ) $this->db->limit( $limit, $offset );
		}
		elseif ( ! empty( $attr ) )
		{
			$this->db->where( 'movement_id', (int)$attr );
		}
		else
		{
			$this->db->select( "*" );
		}

		if ( $query = $this->db->get( $this->assets->conf['tables']['movements'] ) AND $query->num_rows() > 0 )
		{
			if ( is_array( $attr ) OR empty( $attr ) )
			{
				return $query->result_array();
			}

			return $query->row_array();
		}

		return false;
	}
	
	/**
	 * Saves a registry or updates it
	 * 
	 * USAGE:
	 * ======
	 *
	 * When editing a registry, add the identifier id column in the array attribute
	 *
	 * @param array $data Data to be stored
	 * @return boolean/int The insert id
	 */
	public function save( array $data )
	{
		$author = $this->ion_auth->user()->row();

		// Nullify empty elements to store properly
		foreach ( $data AS $key => $val ) if ( empty( $data[$key] ) ) $data[$key] = NULL;

		// Addapt dates from picker
		if ( ! empty( $data['date_submit'] ) ) $data['date'] = $data['date_submit'];

		// Convert strings
		if ( ! empty( $data['label'] ) ) $data['label'] = mb_convert_case( $data['label'], MB_CASE_TITLE, "UTF-8" );
		if ( ! empty( $data['book'] ) ) $data['book'] = mb_convert_case( $data['book'], MB_CASE_TITLE, "UTF-8" );

		// Unset unnecessary columns
		unset( $data['date_submit'], $data['file'], $data['file_id'] );

		if ( empty( $data['movement_id'] ) )
		{
			$data['creator'] = $author->id;
			$data['created'] = unix_to_human( now(), TRUE, 'eu' );

			if ( $this->db->insert( $this->assets->conf['tables']['movements'], $data ) )
			{
				$insert_id = $this->db->insert_id();
				return $insert_id;
			}
		}
		else
		{
			$data['modifier'] = $author->id;
			$data['modified'] = unix_to_human( now(), TRUE, 'eu' );

			$this->db->where( 'movement_id', $data['movement_id'] );
			
			if ( $this->db->update( $this->assets->conf['tables']['movements'], $data ) )
			{
				return true;
			}
		}
		
		return false;
	}

	/**
	 * Removes registries from DB
	 *
	 * @param int or array $id The item id
	 * @return boolean
	 */
	public function erase( $id )
	{
		$this->load->model( "Files_model", "files" );

		if ( is_array( $id ) )
		{
			foreach ( $id AS $v ) $this->files->erase_file_by_registry( $v, 'movements' );
			$this->db->where_in( 'movement_id', $id );
		}
		else
		{
			$this->files->erase_file_by_registry( $id, 'movements' );
			$this->db->where( 'movement_id', $id );
		}
		
		if ( $this->db->delete( $this->assets->conf['tables']['movements'] ) )
		{
			return true;
		}
		
		return false;
	}

	/**
	 * Builds the array of labels and don't repeat
	 * 
	 * @param  string $label The column to pick
	 * @return array
	 */
	public function build_labels_dropdown( $label = 'label' )
	{
		$this->db->select( $label );
		$this->db->distinct();
		$this->db->order_by( $label, 'ASC' );

		$return = array( '' => '&nbsp;' );

		if ( $query = $this->db->get( $this->assets->conf['tables']['movements'] ) AND $query->num_rows() > 0 )
		{
			foreach ( $query->result_array() AS $r )
			{
				$return[$r[$label]] = $r[$label];
			}
		}

		return $return;
	}

	/**
	 * Retrieves the ID of the movement associated to a transaction
	 * 
	 * @param  string $transaction_type
	 * @param  int    $transaction_id
	 * @return int/boolean
	 */
	public function get_associated_id( $transaction_type, $transaction_id )
	{
		$movs = $this->get( array(
			'where' => array(
				'transaction_type' => $transaction_type,
				'transaction_id' => $transaction_id,
			)
		) );

		if ( ! empty( $movs ) )
		{
			return $movs[0]['movement_id'];
		}

		return false;
	}
	
}
