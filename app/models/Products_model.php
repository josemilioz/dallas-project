<?php  if ( ! defined( 'BASEPATH' ) ) exit( 'No direct script access allowed' );

class Products_model extends CI_Model
{
	/**
	 * Class constructor
	 */
	public function __construct()
	{
		parent::__construct();
	}

	/**
	 * Gets the item data from the DB. 
	 *
	 *	USAGE:
	 *	======
	 *	$this->model->get( array(
	 *		'where' => array(
	 *			'field1' => $value1,
	 *			'field2' => $value2
	 *		),
	 *		'or_where' => array(
	 *			'field1' => $value1,
	 *			'field2' => $value2
	 *		),
	 *		'like' => array(
	 *			'field1' => $match
	 *		),
	 *		'or_like' => array(
	 *			'field1' => $match
	 *		),
	 *		'order_by' => array(
	 *			'field' => 'ASC',
	 *			'id' => 'DESC'
	 *		),
	 *		'limit' => 20,
	 *		'offset' => 0
	 *	) );
	 *
	 * @param int/array $attr Record id or query array
	 * @return object/boolean Query result 
	 */
	public function get( $attr = NULL )
	{
		if ( ! empty( $attr ) AND is_array( $attr ) )
		{
			if ( isset( $attr['order_by'] ) AND is_array( $attr['order_by'] ) )
			{
				foreach ( $attr['order_by'] AS $field => $direction )
					$this->db->order_by( $field, $direction );
			}

			if ( isset( $attr['where_in'] ) AND is_array( $attr['where_in'] ) )
			{
				foreach ( $attr['where_in'] AS $key => $items )
					$this->db->where_in( $key, $items );
			}

			if ( isset( $attr['join'] ) AND is_array( $attr['join'] ) )
			{
				$this->db->join( $attr['join']['table'], $attr['join']['comparison'], $attr['join']['direction'] );
			}
			
			$limit = ( ! empty( $attr['limit'] ) ) ? $attr['limit'] : FALSE;
			$offset = ( ! empty( $attr['offset'] ) ) ? $attr['offset'] : 0;
			unset( $attr['limit'], $attr['offset'], $attr['order_by'], $attr['where_in'], $attr['join'] );

			foreach ( $attr AS $key => $val )
			{
				$this->db->{$key}( $val );
			}

			if ( ! empty( $limit ) ) $this->db->limit( $limit, $offset );
		}
		elseif ( ! empty( $attr ) )
		{
			$this->db->where( 'product_id', (int)$attr );
		}
		else
		{
			$this->db->select( "*" );
		}

		if ( $query = $this->db->get( $this->assets->conf['tables']['products'] ) AND $query->num_rows() > 0 )
		{
			if ( is_array( $attr ) OR empty( $attr ) )
			{
				return $query->result_array();
			}

			return $query->row_array();
		}

		return false;
	}
	
	/**
	 * Saves a registry or updates it
	 * 
	 * USAGE:
	 * ======
	 *
	 * When editing a registry, add the identifier id column in the array attribute
	 *
	 * @param array $data Data to be stored
	 * @return boolean/int The insert id
	 */
	public function save( array $data )
	{
		$author = $this->ion_auth->user()->row();

		// Nullify empty elements to store properly
		foreach ( $data AS $key => $val ) if ( empty( $data[$key] ) ) $data[$key] = NULL;

		// Free products nullify
		if ( empty( $data['free'] ) OR ! isset( $data['free'] ) ) $data['free'] = 0;

		// Categories nullify
		$categories = ( ! empty( $data['categories'] ) ) ? $data['categories'] : NULL;
		if ( ! empty( $categories ) AND ! is_array( $categories ) ) $categories = array( $categories );

		// Unset unnecessary columns
		unset( $data['file'], $data['file_id'], $data['categories'] );

		if ( empty( $data['product_id'] ) )
		{
			$data['creator'] = $author->id;
			$data['created'] = unix_to_human( now(), TRUE, 'eu' );

			if ( $this->db->insert( $this->assets->conf['tables']['products'], $data ) )
			{
				$insert_id = $this->db->insert_id();

				// Save here the references of categories
				if ( ! empty( $categories ) )
				{
					foreach ( $categories AS $c )
					{
						$link = array();
						$link['category_id'] = $c;
						$link['product_id'] = $insert_id;
						$this->db->insert( $this->assets->conf['tables']['categories_products'], $link );
					}
				}
				
				return $insert_id;
			}
		}
		else
		{
			$data['modifier'] = $author->id;
			$data['modified'] = unix_to_human( now(), TRUE, 'eu' );

			$this->db->where( 'product_id', $data['product_id'] );

			if ( $this->db->update( $this->assets->conf['tables']['products'], $data ) )
			{
				if ( ! empty( $categories ) )
				{
					// Delete all the previous references
					$this->db->where( 'product_id', $data['product_id'] );
					$this->db->delete( $this->assets->conf['tables']['categories_products'] );

					// Save here the references of categories
					foreach ( $categories AS $c )
					{
						$link = array();
						$link['category_id'] = $c;
						$link['product_id'] = $data['product_id'];
						$this->db->insert( $this->assets->conf['tables']['categories_products'], $link );
					}
				}

				return true;
			}
		}
		
		return false;
	}

	/**
	 * Removes registries from DB
	 *
	 * @param int or array $id The item id
	 * @return boolean
	 */
	public function erase( $id )
	{
		$this->load->model( "Files_model", "files" );

		if ( is_array( $id ) )
		{
			foreach ( $id AS $v ) $this->files->erase_file_by_registry( $v, 'products' );
			$this->db->where_in( 'product_id', $id );
		}
		else
		{
			$this->files->erase_file_by_registry( $id, 'products' );
			$this->db->where( 'product_id', $id );
		}
		
		if ( $this->db->delete( $this->assets->conf['tables']['products'] ) )
		{
			return true;
		}
		
		return false;
	}

	/**
	 * Retrieves all the ids associated to a category, by the ID of the last one
	 * 
	 * @param  int $category_id
	 * @return array/boolean
	 */
	public function get_products_ids_by_category( $category_id )
	{		
		$this->db->where( 'category_id', $category_id );
		if ( $query = $this->db->get( $this->assets->conf['tables']['categories_products'] ) AND $query->num_rows() > 0 )
		{
			$product_ids = array();

			foreach ( $query->result_array() AS $p )
			{
				array_push( $product_ids, (int)$p['product_id'] );
			}

			return $product_ids;
		}

		return false;
	}

	/**
	 * Builds the array for products dropdown
	 * 
	 * @return array/boolean
	 */
	public function build_dropdown()
	{
		$this->load->model( 'Categories_model', 'categories' );

		$categories = $this->categories->get( array(
			'where' => array(
				'trash' => 0
			)
		) );

		if ( ! empty( $categories ) )
		{
			$return = array( '' => '&nbsp;' );

			foreach ( $categories AS $category )
			{
				$products = $this->get( array(
					'where' => array(
						'category_id' => $category['category_id'],
					)
				) );

				if ( ! empty( $products ) )
				{
					foreach ( $products AS $product )
					{
						$return[$category['name']][$product['product_id']] = $product['name'];
					}
				}
			}

			return $return;
		}

		return false;
	}

	/**
	 * Builds the json structure for products selection
	 * 
	 * @return object/string
	 */
	public function build_json()
	{
		$this->load->model( 'Categories_model', 'categories' );

		$categories = $this->categories->get( array(
			'where' => array(
				'trash' => 0
			)
		) );

		if ( ! empty( $categories ) )
		{
			foreach ( $categories AS $category )
			{
				$products_ids = $this->get_products_ids_by_category( $category['category_id'] );

				if ( ! empty( $products_ids ) )
				{
					foreach ( $products_ids AS $product_id )
					{
						$product = $this->get( $product_id );

						if ( ! empty( $product ) AND $product['trash'] == "0" )
						{
							$return[$product['product_id']] = $product;

							// Corrections for images
							if ( ! empty( $product['images'] ) )
							{
								$return[$product['product_id']]['images'] = json_decode( $product['images'] );
							}

							// Tags
							$return[$product['product_id']]['tags'] = array(
								'currency' => $this->assets->conf['currencies'][$product['currency']],
								'unit_measure' => $this->assets->conf['units'][$product['unit_measure']],
							);

							// Dispose unnecessary
							unset( $return[$product['product_id']]['trash'] );
						}
					}
				}
			}

			return json_encode( $return );
		}

		return '{}';
	}

	/**
	 * Get product object by sku number
	 * 
	 * @param  int $sku
	 * @return array/boolean
	 */
	public function get_by_sku( $sku )
	{
		$this->load->model( "Stock_model", "stock" );
		$sku = $this->stock->get( $sku );
		if ( ! empty( $sku ) )
		{
			$product = $this->get( $sku['product_id'] );

			if ( ! empty( $product ) ) return $product;
		}

		return false;
	}

	/**
	 * Returns an url for the product picture. By default, uses default no avatar
	 * 
	 * @param  int/array $product_id Product ID
	 * @param  string $size          The size option (thumb or normal)
	 * @return string
	 */
	public function picture( $product_id, $size = "thumb" )
	{
		$this->load->model( "Files_model", "files" );
		$this->load->model( "Categories_model", "categories" );

		$picture = $this->files->get( array(
			'where' => array(
				'registry_id' => $product_id,
				'controller' => 'products',
			),
			'order_by' => array(
				'order' => 'ASC'
			)
		) );

		$return = $this->assets->conf['no_image'];

		if ( ! empty( $picture[0] ) )
		{
			switch ( $size )
			{
				default: case 'thumb':
					$filename = ltrim( $picture[0]['relative_location'], "/" ) . $picture[0]['name'] . "_thumb" . $picture[0]['extension'];
					break;
				case 'normal':
					$filename = ltrim( $picture[0]['relative_location'], "/" ) . $picture[0]['name_ext'];
					break;
			}

			if ( is_file( FCPATH . $filename ) ) $return = site_url( $filename );
		}
		else // In case product does not have a picture, let's see if we can use the one from the category
		{
			$product = $this->get( $product_id );
			$return = $this->categories->picture( $product['category_id'] );
		}

		return $return;
	}
}
