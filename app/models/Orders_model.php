<?php  if ( ! defined( 'BASEPATH' ) ) exit( 'No direct script access allowed' );

class Orders_model extends CI_Model
{
	/**
	 * Class constructor
	 */
	public function __construct()
	{
		parent::__construct();
	}

	/**
	 * Gets the item data from the DB. 
	 *
	 *	USAGE:
	 *	======
	 *	$this->model->get( array(
	 *		'where' => array(
	 *			'field1' => $value1,
	 *			'field2' => $value2
	 *		),
	 *		'or_where' => array(
	 *			'field1' => $value1,
	 *			'field2' => $value2
	 *		),
	 *		'like' => array(
	 *			'field1' => $match
	 *		),
	 *		'or_like' => array(
	 *			'field1' => $match
	 *		),
	 *		'order_by' => array(
	 *			'field' => 'ASC',
	 *			'id' => 'DESC'
	 *		),
	 *		'limit' => 20,
	 *		'offset' => 0
	 *	) );
	 *
	 * @param int/array $attr Record id or query array
	 * @return object/boolean Query result 
	 */
	public function get( $attr = NULL )
	{
		if ( ! empty( $attr ) AND is_array( $attr ) )
		{
			if ( isset( $attr['order_by'] ) AND is_array( $attr['order_by'] ) )
			{
				foreach ( $attr['order_by'] AS $field => $direction )
					$this->db->order_by( $field, $direction );
			}

			if ( isset( $attr['where_in'] ) AND is_array( $attr['where_in'] ) )
			{
				foreach ( $attr['where_in'] AS $key => $items )
					$this->db->where_in( $key, $items );
			}

			if ( isset( $attr['join'] ) AND is_array( $attr['join'] ) )
			{
				$this->db->join( $attr['join']['table'], $attr['join']['comparison'], $attr['join']['direction'] );
			}

			$limit = ( ! empty( $attr['limit'] ) ) ? $attr['limit'] : FALSE;
			$offset = ( ! empty( $attr['offset'] ) ) ? $attr['offset'] : 0;
			unset( $attr['limit'], $attr['offset'], $attr['order_by'], $attr['where_in'], $attr['join'] );

			foreach ( $attr AS $key => $val )
			{
				$this->db->{$key}( $val );
			}

			if ( ! empty( $limit ) ) $this->db->limit( $limit, $offset );
		}
		elseif ( ! empty( $attr ) )
		{
			$this->db->where( 'order_id', (int)$attr );
		}
		else
		{
			$this->db->select( "*" );
		}

		if ( $query = $this->db->get( $this->assets->conf['tables']['orders'] ) AND $query->num_rows() > 0 )
		{
			if ( is_array( $attr ) OR empty( $attr ) )
			{
				return $query->result_array();
			}

			return $query->row_array();
		}

		return false;
	}
	
	/**
	 * Saves a registry or updates it
	 * 
	 * USAGE:
	 * ======
	 *
	 * When editing a registry, add the identifier id column in the array attribute
	 *
	 * @param array $data Data to be stored
	 * @return boolean/int The insert id
	 */
	public function save( array $data )
	{
		$author = $this->ion_auth->user()->row();

		// Nullify empty elements to store properly
		foreach ( $data AS $key => $val ) if ( empty( $data[$key] ) ) $data[$key] = NULL;

		// Prepare metas
		$product_id 		= ( isset( $data['list_product_id'] ) ) 		? $data['list_product_id'] 			: NULL;
		$description 		= ( isset( $data['list_description'] ) ) 		? $data['list_description'] 		: NULL;
		$quantity 			= ( isset( $data['list_quantity'] ) ) 			? $data['list_quantity'] 			: NULL;
		$quantity_remaining = ( isset( $data['list_quantity_remaining'] ) ) ? $data['list_quantity_remaining'] 	: NULL;
		$unit_price 		= ( isset( $data['list_unit_price'] ) ) 		? $data['list_unit_price'] 			: NULL;
		$row_total 			= ( isset( $data['list_row_total'] ) ) 			? $data['list_row_total'] 			: NULL;

		unset( $data['list_product_id'], $data['list_description'], $data['list_quantity'], $data['list_quantity_remaining'], $data['list_unit_price'], $data['list_row_total'] );

		// Addapt dates from picker
		if ( ! empty( $data['order_date_submit'] ) ) $data['order_date'] = $data['order_date_submit'];
		if ( ! empty( $data['delivery_date_submit'] ) ) $data['delivery_date'] = $data['delivery_date_submit'];
		if ( ! empty( $data['expiration_date_submit'] ) ) $data['expiration_date'] = $data['expiration_date_submit'];

		unset( $data['order_date_submit'], $data['delivery_date_submit'], $data['expiration_date_submit'] );
		unset( $data['file'], $data['file_id'] );

		// Names
		if ( ! empty( $data['provider'] ) ) $data['provider'] = mb_convert_case( $data['provider'], MB_CASE_UPPER, "UTF-8" );

		if ( empty( $data['order_id'] ) )
		{
			$data['creator'] = $author->id;
			$data['created'] = unix_to_human( now(), TRUE, 'eu' );

			if ( $this->db->insert( $this->assets->conf['tables']['orders'], $data ) )
			{
				$insert_id = $this->db->insert_id();
				// Store metas
				if ( ! empty( $product_id ) AND ! empty( $description ) AND ! empty( $quantity ) AND ! empty( $unit_price ) AND ! empty( $row_total ) )
				{
					$this->meta_save( $insert_id, $product_id, $description, $quantity, $quantity_remaining, $unit_price, $row_total );
				}
				return $insert_id;
			}
		}
		else
		{
			$data['modifier'] = $author->id;
			$data['modified'] = unix_to_human( now(), TRUE, 'eu' );

			$this->db->where( 'order_id', $data['order_id'] );
			
			if ( $this->db->update( $this->assets->conf['tables']['orders'], $data ) )
			{
				// Erase previous metas and store again
				if ( ! empty( $product_id ) AND ! empty( $quantity ) AND ! empty( $unit_price ) )
				{
					$this->meta_erase( $data['order_id'] );
					$this->meta_save( $data['order_id'], $product_id, $description, $quantity, $quantity_remaining, $unit_price, $row_total );
				}

				return true;
			}
		}
		
		return false;
	}

	/**
	 * Removes registries from DB
	 *
	 * @param int or array $id The item id
	 * @return boolean
	 */
	public function erase( $id )
	{
		$this->load->model( "Files_model", "files" );

		if ( is_array( $id ) )
		{
			foreach ( $id AS $v ) $this->files->erase_file_by_registry( $v, 'orders' );
			$this->db->where_in( 'order_id', $id );
		}
		else
		{
			$this->files->erase_file_by_registry( $id, 'orders' );
			$this->db->where( 'order_id', $id );
		}
		
		if ( $this->db->delete( $this->assets->conf['tables']['orders'] ) )
		{
			return true;
		}
		
		return false;
	}

	/**
	 * Stores the meta we want to associate to the order_id. If there's any quantity remaining, put the order as unfinished, otherwise finish it.
	 * 
	 * @param  int    $order_id The order ID
	 * @param  array  $product_id
	 * @param  array  $description
	 * @param  array  $quantity
	 * @param  array  $remaining
	 * @param  array  $unit_price
	 * @param  array  $row_total
	 * @return void
	 */
	public function meta_save( $order_id, array $product_id, array $description, array $quantity, array $remaining, array $unit_price, array $row_total )
	{
		$full_sum = 0;

		foreach ( $product_id AS $k => $pid )
		{
			$full_sum = $full_sum + (float)$remaining[$k];

			$new_row = array(
				'order_id' 				=> $order_id,
				'product_id' 			=> $pid,
				'description' 			=> $description[$k],
				'quantity' 				=> $quantity[$k],
				'quantity_remaining'	=> $remaining[$k],
				'unit_price' 			=> $unit_price[$k],
				'row_total' 			=> $row_total[$k],
			);

			$this->db->insert( $this->assets->conf['tables']['orders_products'], $new_row );
		}

		if ( $full_sum == 0 ) // turn to sold if the sum of every remaining equals zero
		{
			$this->save( array(
				'order_id' => $order_id,
				'sold' => 1,
			) );
		}
		else
		{
			$this->save( array(
				'order_id' => $order_id,
				'sold' => 0,
			) );			
		}
	}

	/**
	 * Gets all the meta associated to the order_id given
	 * 
	 * @param  int $order_id The order id
	 * @return array/boolean
	 */
	public function meta_get( $order_id )
	{
		$this->db->where( 'order_id', $order_id );
		if ( $query = $this->db->get( $this->assets->conf['tables']['orders_products'] ) AND $query->num_rows() > 0 )
		{
			return $query->result_array();
		}

		return false;		
	}

	/**
	 * Updates the meta remainings only, according the order and the product id. Also performs the checking of the remainings and in case sum gets 0 it marks the order as sold.
	 * Only to be used within the sale editing.
	 * 
	 * @param  int    $order_id The order ID
	 * @param  array  $product_id
	 * @param  array  $remaining
	 * @return void
	 */
	public function meta_update_remainings( $order_id, array $product_id, array $remaining )
	{
		$full_sum = 0;

		foreach ( $product_id AS $k => $pid )
		{
			$full_sum = $full_sum + (float)$remaining[$k];

			$this->db->update( $this->assets->conf['tables']['orders_products'], array(
				'quantity_remaining' => (float)$remaining[$k],
			), array(
				'order_id'		=> $order_id,
				'product_id'	=> $pid,
			) );
		}

		if ( $full_sum == 0 ) // turn to sold if the sum of every remaining equals zero
		{
			$this->save( array(
				'order_id' => $order_id,
				'sold' => 1,
			) );
		}
	}

	/**
	 * Erases all the meta associated to the order_id given
	 * 
	 * @param  int $order_id The order id
	 * @return boolean
	 */
	public function meta_erase( $order_id )
	{
		$this->db->where( 'order_id', $order_id );
		if ( $this->db->delete( $this->assets->conf['tables']['orders_products'] ) )
		{
			return true;
		}
		
		return false;
	}

	/**
	 * Builds the array of providers and don't repeat
	 * 
	 * @return array
	 */
	public function build_dropdown_providers()
	{
		$this->db->select( 'provider' );
		$this->db->distinct();
		$this->db->order_by( 'provider', 'ASC' );

		$return = array( '' => '&nbsp;' );

		if ( $query = $this->db->get( $this->assets->conf['tables']['orders'] ) AND $query->num_rows() > 0 )
		{
			foreach ( $query->result_array() AS $r )
			{
				$return[$r['provider']] = $r['provider'];
			}
		}

		return $return;
	}

	/**
	 * Prepares the data to iterate every row, whether it coming from an actual meta or coming from POST
	 * 
	 * @param  array  $data The data
	 * @return array
	 */
	public function prepare_meta( array $data )
	{
		$this->load->model( "Products_model", "products" );

		if ( isset( $data['list_product_id'] ) AND ! empty( $data ) ) // Only if we are not editing
		{
			$rows = array();

			foreach ( $data['list_product_id'] AS $k => $reference )
			{
				$product = $this->products->get( $data['list_product_id'][$k] );

				$rows[$k]['product_id'] 		= $data['list_product_id'][$k];
				$rows[$k]['description'] 		= ( empty( $row['list_description'][$k] ) ) ? $product['name'] : $data['list_description'][$k];
				$rows[$k]['unit_price'] 		= $data['list_unit_price'][$k];
				$rows[$k]['quantity'] 			= $data['list_quantity'][$k];
				$rows[$k]['quantity_remaining'] = $data['list_quantity_remaining'][$k];
				$rows[$k]['row_total'] 			= $data['list_row_total'][$k];
			}

			$data = $rows;
		}

		return $data;
	}

	/**
	 * Checks whether a order has been sold already or not
	 * 
	 * @param  int/array $order The order or its ID
	 * @return boolean
	 */
	public function is_sold( $order )
	{
		$order = ( is_array( $order ) AND isset( $order['order_id'] ) ) ? $order : $this->get( $order );

		if ( ! empty( $order ) )
		{
			if ( isset( $order['sold'] ) AND $order['sold'] == 1 ) return true;
		}

		return false;
	}

	/**
	 * This function is intended strictly for the sales procedure where we are trying to convert an order into a sale
	 * 
	 * @param  int $order The order ID or array
	 * @return array/boolean
	 */
	public function check_missing_products( $order )
	{
		$order = ( is_array( $order ) AND isset( $order['order_id'] ) ) ? $order : $this->get( $order );

		if ( ! empty( $order ) AND ( isset( $_POST['list_product_id'] ) AND ! empty( $_POST ) ) )
		{
			$order_meta = $this->meta_get( $order['order_id'] );

			if ( ! empty( $order_meta ) )
			{
				$return = array();

				foreach ( $order_meta AS $key => $row )
				{
					$return[$key]['description'] 		= $row['description'];
					$return[$key]['order_quantity'] 	= (float)$row['quantity_remaining'];
					$return[$key]['sale_quantity'] 		= 0;
					$return[$key]['missing_quantity'] 	= 0;

					foreach ( $_POST['list_product_id'] AS $key_2 => $product_id ) if ( $product_id == $row['product_id'] ) $return[$key]['sale_quantity'] += (float)$_POST['list_quantity'][$key_2];

					$return[$key]['missing_quantity'] = (float)( $return[$key]['order_quantity'] - $return[$key]['sale_quantity'] );

					if ( $return[$key]['missing_quantity'] <= 0 ) unset( $return[$key] );
				}

				return ( empty( $return ) ) ? false : $return;
			}
		}
		else if ( ! empty( $order ) AND ( ! isset( $_POST['list_product_id'] ) ) )
		{
			$order_meta = $this->meta_get( $order['order_id'] );

			if ( ! empty( $order_meta ) )
			{
				$return = array();

				foreach ( $order_meta AS $key => $row )
				{
					$return[$key]['description'] 		= $row['description'];
					$return[$key]['order_quantity'] 	= (float)$row['quantity_remaining'];
					$return[$key]['sale_quantity'] 		= 0;
					$return[$key]['missing_quantity'] 	= (float)$row['quantity_remaining'];
				}

				return $return;
			}
		}

		return false;
	}

}
