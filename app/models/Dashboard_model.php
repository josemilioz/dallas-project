<?php  if ( ! defined( 'BASEPATH' ) ) exit( 'No direct script access allowed' );

class Dashboard_model extends CI_Model
{
	/**
	 * Class constructor
	 */
	public function __construct()
	{
		parent::__construct();
	}

	public function totals()
	{
		$current_month = date( "Y-m-" );

		// Clients
		if ( $this->ion_auth->in_group( array( 'vendedor' ) ) )
		{
			$this->db->where( 'creator', $this->ion_auth->user()->row()->id );
		}
		$this->db->where( 'trash', 0 );
		$clients = $this->db->count_all_results( $this->assets->conf['tables']['clients'] );

		// Orders
		if ( $this->ion_auth->in_group( array( 'vendedor' ) ) )
		{
			$this->db->where( 'creator', $this->ion_auth->user()->row()->id );
		}
		$this->db->select_sum( "total_amount" );
		$this->db->where( 'trash', 0 );
		$this->db->like( 'order_date', $current_month );
		$orders = $this->db->get( $this->assets->conf['tables']['orders'] )->row_array();

		// Purchases
		$this->db->select_sum( "total_amount" );
		$this->db->where( 'trash', 0 );
		$this->db->like( 'purchase_date', $current_month );
		$purchases = $this->db->get( $this->assets->conf['tables']['purchases'] )->row_array();
		
		// Sales
		$this->db->select_sum( "total_amount" );
		$this->db->where( 'trash', 0 );
		$this->db->like( 'sale_date', $current_month );
		$sales = $this->db->get( $this->assets->conf['tables']['sales'] )->row_array();

		return array(
			'clients' => $clients,
			'orders' => $orders['total_amount'],
			'purchases' => ( $this->ion_auth->in_group( array( 'admin' ) ) ) ? $purchases['total_amount'] : FALSE,
			'sales' => ( $this->ion_auth->in_group( array( 'admin' ) ) ) ? $sales['total_amount'] : FALSE,
		);
	}

	public function movements_graph_data()
	{
		$current_year = date( "Y-" );

		// Income
		if ( $this->ion_auth->in_group( array( 'vendedor' ) ) )
		{
			$this->db->where( 'assignee', $this->ion_auth->user()->row()->id );
		}
		$this->db->select_sum( "amount" );
		$this->db->where( 'trash', 0 );
		$this->db->where( 'type', 'income' );
		$this->db->like( 'date', $current_year );
		$income = $this->db->get( $this->assets->conf['tables']['movements'] )->row_array();

		// Outcome
		if ( $this->ion_auth->in_group( array( 'vendedor' ) ) )
		{
			$this->db->where( 'assignee', $this->ion_auth->user()->row()->id );
		}
		$this->db->select_sum( "amount" );
		$this->db->where( 'trash', 0 );
		$this->db->where( 'type', 'outcome' );
		$this->db->like( 'date', $current_year );
		$outcome = $this->db->get( $this->assets->conf['tables']['movements'] )->row_array();

		// Difference
		$difference = (float)$income['amount'] - (float)$outcome['amount'];

		return array(
			'income' => $income['amount'],
			'outcome' => $outcome['amount'],
			'wins' => ( $difference > 0 ) ? str_replace( array( "-", "," ), array( "", "." ), (float)$difference ) : 0,
			'loses' => ( $difference < 0 ) ? str_replace( array( "-", "," ), array( "", "." ), (float)$difference ) : 0,
		);
	}

	public function orders_graph_data()
	{
		$months = array( 
			'01' => 'Enero',
			'02' => 'Febrero',
			'03' => 'Marzo',
			'04' => 'Abril',
			'05' => 'Mayo',
			'06' => 'Junio',
			'07' => 'Julio',
			'08' => 'Agosto',
			'09' => 'Setiembre',
			'10' => 'Octubre',
			'11' => 'Noviembre',
			'12' => 'Diciembre',
		);

		$return = array();

		foreach ( $months AS $n => $m )
		{
			$current_month = date( "Y-$n-" );

			if ( $this->ion_auth->in_group( array( 'vendedor' ) ) )
			{
				$this->db->where( 'creator', $this->ion_auth->user()->row()->id );
			}
			$this->db->where( 'trash', 0 );
			$this->db->like( 'order_date', $current_month );
			$orders = $this->db->count_all_results( $this->assets->conf['tables']['orders'] );

			$return[$m] = $orders;
		}

		return $return;
	}

}
