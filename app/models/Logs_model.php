<?php  if ( ! defined( 'BASEPATH' ) ) exit( 'No direct script access allowed' );

class Logs_model extends CI_Model
{
	/**
	 * Class constructor
	 */
	public function __construct()
	{
		parent::__construct();
	}

	/**
 	 * Method which stores the log on the DB
	 * @param mixed $param
	 */
	function add( $param ) 
	{
		$this->db->insert( $this->assets->conf['tables']['logs'], $param );
		return ( $this->db->affected_rows() ) ? TRUE : FALSE;
	}

	/**
	* Get the log data
	* 
	* @param  mixed $attr
	* @return mixed array/boolean
	*/
	public function get( $attr = NULL )
	{
		if ( is_array( $attr ) )
		{
			if ( isset( $attr['order_by'] ) AND is_array( $attr['order_by'] ) )
			{
				foreach ( $attr['order_by'] AS $field => $direction )
					$this->db->order_by( $field, $direction );
			}

			$limit = ( isset( $attr['limit'] ) ) ? $attr['limit'] : FALSE;
			$offset = ( isset( $attr['offset'] ) ) ? $attr['offset'] : 0;
			unset( $attr['limit'], $attr['offset'], $attr['order_by'] );

			foreach ( $attr AS $key => $val )
			{
				$this->db->$key( $val );
			}

			if ( ! empty( $limit ) ) $this->db->limit( $limit, $offset );
		}
		else
		{
			$this->db->where( 'log_id', (int)$attr );
		}

		if ( $query = $this->db->get( $this->assets->conf['tables']['logs'] ) AND $query->num_rows() > 0 )
		{
			return $query->result_array();
		}

		return false;
	}

}
