<?php  if ( ! defined( 'BASEPATH' ) ) exit( 'No direct script access allowed' );

class Pos_model extends CI_Model
{
	/**
	 * Class constructor
	 */
	public function __construct()
	{
		parent::__construct();
	}

	/**
	 * Gets the item data from the DB. 
	 *
	 *	USAGE:
	 *	======
	 *	$this->model->get( array(
	 *		'where' => array(
	 *			'field1' => $value1,
	 *			'field2' => $value2
	 *		),
	 *		'or_where' => array(
	 *			'field1' => $value1,
	 *			'field2' => $value2
	 *		),
	 *		'like' => array(
	 *			'field1' => $match
	 *		),
	 *		'or_like' => array(
	 *			'field1' => $match
	 *		),
	 *		'order_by' => array(
	 *			'field' => 'ASC',
	 *			'id' => 'DESC'
	 *		),
	 *		'limit' => 20,
	 *		'offset' => 0
	 *	) );
	 *
	 * @param int/array $attr Record id or query array
	 * @return object/boolean Query result 
	 */
	public function get( $attr = NULL )
	{
		if ( ! empty( $attr ) AND is_array( $attr ) )
		{
			if ( isset( $attr['order_by'] ) AND is_array( $attr['order_by'] ) )
			{
				foreach ( $attr['order_by'] AS $field => $direction )
					$this->db->order_by( $field, $direction );
			}

			if ( isset( $attr['where_in'] ) AND is_array( $attr['where_in'] ) )
			{
				foreach ( $attr['where_in'] AS $key => $items )
					$this->db->where_in( $key, $items );
			}

			// if ( ! isset( $attr['limit'] ) ) $attr['limit'] = $this->assets->conf['items_per_page'];
			// if ( ! isset( $attr['offset'] ) AND ! empty( $this->uri->segment( 3 ) ) ) $attr['offset'] = $this->uri->segment( 3 );

			if ( isset( $attr['join'] ) AND is_array( $attr['join'] ) )
			{
				$this->db->join( $attr['join']['table'], $attr['join']['comparison'], $attr['join']['direction'] );
			}
			
			$limit = ( ! empty( $attr['limit'] ) ) ? $attr['limit'] : FALSE;
			$offset = ( ! empty( $attr['offset'] ) ) ? $attr['offset'] : 0;
			unset( $attr['limit'], $attr['offset'], $attr['order_by'], $attr['where_in'], $attr['join'] );

			foreach ( $attr AS $key => $val )
			{
				$this->db->{$key}( $val );
			}

			if ( ! empty( $limit ) ) $this->db->limit( $limit, $offset );
		}
		elseif ( ! empty( $attr ) )
		{
			$this->db->where( 'pos_id', (int)$attr );
		}
		else
		{
			$this->db->select( "*" );
		}

		if ( $query = $this->db->get( $this->assets->conf['tables']['pos'] ) AND $query->num_rows() > 0 )
		{
			if ( is_array( $attr ) OR empty( $attr ) )
			{
				return $query->result_array();
			}

			return $query->row_array();
		}

		return false;
	}
	
	/**
	 * Saves a registry or updates it
	 * 
	 * USAGE:
	 * ======
	 *
	 * When editing a registry, add the identifier id column in the array attribute
	 *
	 * @param array $data Data to be stored
	 * @return boolean/int The insert id
	 */
	public function save( array $data )
	{
		$author = $this->ion_auth->user()->row();

		// Nullify empty elements to store properly
		foreach ( $data AS $key => $val ) if ( empty( $data[$key] ) ) $data[$key] = NULL;

		// Prepare users
		$user_ids = ( isset( $data['user_list'] ) AND ! empty( $data['user_list'] ) ) ? $data['user_list'] : NULL;

		// Prepare enabled categories
		$categories = ( isset( $data['categories_enabled'] ) AND ! empty( $data['categories_enabled'] ) ) ? $data['categories_enabled'] : NULL;

		// Unset unnecessary columns
		unset( $data['file'], $data['file_id'], $data['user_list'], $data['user_id'], $data['categories_enabled'], $data['categories_list'] );

		if ( empty( $data['pos_id'] ) )
		{
			$data['creator'] = $author->id;
			$data['created'] = unix_to_human( now(), TRUE, 'eu' );

			if ( $this->db->insert( $this->assets->conf['tables']['pos'], $data ) )
			{
				$insert_id = $this->db->insert_id();

				// Associate users
				if ( ! empty( $user_ids ) )
				{
					$this->users_save( $insert_id, $user_ids );
				}

				// Associate categories enabled
				if ( ! empty( $categories ) )
				{
					$this->categories_save( $insert_id, $categories );
				}

				return $insert_id;
			}
		}
		else
		{
			$data['modifier'] = $author->id;
			$data['modified'] = unix_to_human( now(), TRUE, 'eu' );

			$this->db->where( 'pos_id', $data['pos_id'] );
			
			if ( $this->db->update( $this->assets->conf['tables']['pos'], $data ) )
			{
				// Associate users
				$this->users_erase( $data['pos_id'] );				
				if ( ! empty( $user_ids ) ) $this->users_save( $data['pos_id'], $user_ids );

				// Associate categories enabled
				$this->categories_erase( $data['pos_id'] );				
				if ( ! empty( $categories ) ) $this->categories_save( $data['pos_id'], $categories );

				return true;
			}
		}
		
		return false;
	}

	/**
	 * Removes registries from DB
	 *
	 * @param int or array $id The item id
	 * @return boolean
	 */
	public function erase( $id )
	{
		if ( is_array( $id ) )
		{
			$this->db->where_in( 'pos_id', $id );
		}
		else
		{
			$this->db->where( 'pos_id', $id );
		}
		
		if ( $this->db->delete( $this->assets->conf['tables']['pos'] ) )
		{
			return true;
		}
		
		return false;
	}

	/**
	 * ----------------------------------------------------------------------------------------------------
	 * POS & USERS
	 * ----------------------------------------------------------------------------------------------------
	 */

	/**
	 * Prepares the users to iterate every row, whether it coming from an actual meta or coming from POST
	 * 
	 * @param  array  $data The data
	 * @return array
	 */
	public function prepare_users( $data )
	{
		if ( isset( $data['user_list'] ) AND ! empty( $data ) ) // Only if we are not editing
		{
			$rows = array();

			foreach ( $data['user_list'] AS $k => $reference )
			{
				$rows[$k]['user_id'] 	= $data['user_list'][$k];
				$rows[$k]['fullname'] 	= $this->ion_auth->user_name( 'lastname_start', $reference );
			}

			$data = $rows;
		}

		return $data;
	}

	/**
	 * Builds the array for a dropdown with the active POS
	 * 
	 * @return array/boolean
	 */
	public function build_dropdown()
	{
		$pos = $this->get( array(
			'where' => array(
				'trash' => 0
			)
		) );

		if ( ! empty( $pos ) )
		{
			$return = array( '' => '&nbsp;' );

			foreach ( $pos AS $row )
			{
				$return[$row['pos_id']] = $row['name'];
			}

			return $return;
		}

		return false;
	}

	/**
	 * Retrieves the authorized users of a given POS
	 * 
	 * @param  int $pos_id The POS ID
	 * @return array/boolean
	 */
	public function users_get( $pos_id )
	{
		$this->db->where( 'pos_id', $pos_id );

		if ( $query = $this->db->get( $this->assets->conf['tables']['pos_users'] ) AND $query->num_rows() > 0 )
		{
			return $query->result_array();
		}

		return false;
	}

	/**
	 * Stores the associations with pos and users
	 * 
	 * @param  int    $pos_id The POS ID
	 * @param  array  $users  The array of users ids
	 * @return none
	 */
	public function users_save( $pos_id, array $users )
	{
		foreach ( $users AS $key => $user_id )
		{
			$this->db->insert( $this->assets->conf['tables']['pos_users'], array(
				'pos_id' => $pos_id,
				'user_id' => $user_id,
			) );
		}
	}

	/**
	 * Deletes the associations with users and pos
	 * 
	 * @param  int $pos_id The POS ID
	 * @return boolean
	 */
	public function users_erase( $pos_id )
	{
		$this->db->where( 'pos_id', $pos_id );
		
		if ( $this->db->delete( $this->assets->conf['tables']['pos_users'] ) )
		{
			return true;
		}

		return false;
	}

	/**
	 * Counts the enabled users of a given POS
	 * 
	 * @param  int $pos_id The POS ID
	 * @return int
	 */
	public function count_enabled_users( $pos_id )
	{
		$this->db->where( 'pos_id', $pos_id );
		return $this->db->count_all_results( $this->assets->conf['tables']['pos_users'] );
	}

	/**
	 * ----------------------------------------------------------------------------------------------------
	 * BILL NUMBER AND PREDICTIONS
	 * ----------------------------------------------------------------------------------------------------
	 */

	/**
	 * Function to generate automatically the bill number
	 *
	 * @param  int $pos_id The POS id to check against
	 * @return string/boolean
	 */
	public function generate_bill_number( $pos_id )
	{
		$this->load->model( "Sales_model", "sales" );
		$this->load->model( "Print_orders_model", "print_orders" );

		$print_order = $this->print_orders->get( array(
			'where' => array(
				'trash' => 0,
				'pos_id' => $pos_id,
			),
			'limit' => 1,
			'order_by' => array(
				'print_order_id' => 'DESC',
			),
		) );

		$print_order = ( ! empty( $print_order[0] ) ) ? $print_order[0] : NULL;

		if ( ! empty( $print_order ) AND $this->print_orders->is_still_valid( $print_order ) )
		{
			// Let's start with 0
			$bill = 0;

			$last_sale = $this->sales->get( array(
				'where' => array(
					'trash' => 0,
					'print_order' => $print_order['print_order'],
				),
				'limit' => 1,
				'order_by' => array(
					'bill_number' => 'DESC', // Use bill number instead...
					'sale_id' => 'DESC', // And sale_id just in case
				),
			) );

			$last_sale = ( ! empty( $last_sale[0] ) ) ? $last_sale[0] : NULL;

			if ( ! empty( $last_sale ) )
			{
				// If we have a previous sale, let's add the number
				$bill = explode( '-', $last_sale['bill_number'] );
				$bill = (int)$bill[2];
				$bill++;				
			}

			// If the bill number is below the minimum, let's use the minimum instead
			
			if ( $print_order['number_start'] >= $bill )
			{
				$bill = $print_order['number_start'];
			}

			// Now, let's check if we're below the max. If we are below the max, 
			// we cannot make a sale, then there is no number allowed.
			// If we're cool, then return the number to be used.
			
			if ( $print_order['number_end'] >= $bill )
			{
				return $this->build_bill_number( $print_order, $bill );
			}
		}

		return false;
	}

	/**
	 * Manually build the bill number, given the print order, and the number value
	 * @param  array  $print_order The print order array element
	 * @param  array  $bill_number The bill number
	 * 
	 * @return string/boolean
	 */
	public function build_bill_number( array $print_order, $bill_number )
	{
		if ( ! empty( $print_order['location'] ) AND ! empty( $print_order['expedition'] ) )
		{
			$formatted_bill_number = $print_order['location'] . "-";
			$formatted_bill_number.= $print_order['expedition'] . "-";
			$formatted_bill_number.= str_pad( $bill_number, 7, '0', STR_PAD_LEFT );

			return $formatted_bill_number;
		}

		return false;
	}

	/**
	 * Checks whether a bill number already exists or not
	 * 
	 * @param  string $bill_number The bill number, formatted
	 * @return boolean
	 */
	public function bill_number_exists( $bill_number )
	{
		$this->load->model( "Sales_model", "sales" );

		$query = $this->sales->get( array(
			'where' => array(
				'bill_number' => $bill_number
			)
		) );

		if ( ! empty( $query ) ) return true;

		return false;
	}

	/**
	 * ----------------------------------------------------------------------------------------------------
	 * POS & CATEGORIES
	 * ----------------------------------------------------------------------------------------------------
	 */

	/**
	 * Store the categories with the given POS ID
	 * 
	 * @param  int    $pos_id     The pos ID
	 * @param  array  $categories The categories in array
	 * @return boolean
	 */
	public function categories_save( $pos_id, array $categories )
	{
		if ( ! empty( $categories ) )
		{
			foreach ( $categories AS $key => $val )
			{
				$this->db->insert( $this->assets->conf['tables']['pos_categories'], array(
					'pos_id' => $pos_id,
					'category_id' => $val,
				) );
			}

			return TRUE;
		}

		return FALSE;
	}

	/**
	 * Erases the categories from the given POS ID
	 * 
	 * @param  int $pos_id The POS ID
	 * @return boolean
	 */
	public function categories_erase( $pos_id )
	{
		$this->db->where( 'pos_id', $pos_id );

		if ( $this->db->delete( $this->assets->conf['tables']['pos_categories'] ) )
		{
			return true;
		}
		
		return false;
	}

	/**
	 * Clear the main categories list according the presence of active categories
	 * 
	 * @param  array  $all_cats    All categories array
	 * @param  array  $active_cats Active categories array
	 * @return array
	 */
	public function clear_active_categories( $all_cats, $active_cats )
	{
		foreach ( $all_cats AS $k => $v )
		{
			if ( ! empty( $active_cats ) )
			{
				foreach ( $active_cats AS $k2 => $v2 )
				{
					if ( $k2 == $k ) unset( $all_cats[$k] );
				}
			}
		}

		return $all_cats;
	}

	/**
	 * Get all categories associated to the POS
	 * 
	 * @param  int $pos_id POS ID
	 * @return array/boolean
	 */
	public function get_categories( $pos_id )
	{
		$this->load->model( 'Categories_model', 'categories' );
		$this->db->where( 'pos_id', $pos_id );

		if ( $query = $this->db->get( $this->assets->conf['tables']['pos_categories'] ) AND $query->num_rows() > 0 )
		{
			$result = $query->result_array();
			$return = array();

			foreach ( $result AS $relation )
			{
				$category = $this->categories->get( $relation['category_id'] );
				$return[$category['category_id']] = $category['name'];
			}

			return $return;
		}

		return FALSE;
	}
}
