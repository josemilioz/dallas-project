<?php  if ( ! defined( 'BASEPATH' ) ) exit( 'No direct script access allowed' );

class Print_orders_model extends CI_Model
{
	/**
	 * Class constructor
	 */
	public function __construct()
	{
		parent::__construct();
	}

	/**
	 * Gets the item data from the DB. 
	 *
	 *	USAGE:
	 *	======
	 *	$this->model->get( array(
	 *		'where' => array(
	 *			'field1' => $value1,
	 *			'field2' => $value2
	 *		),
	 *		'or_where' => array(
	 *			'field1' => $value1,
	 *			'field2' => $value2
	 *		),
	 *		'like' => array(
	 *			'field1' => $match
	 *		),
	 *		'or_like' => array(
	 *			'field1' => $match
	 *		),
	 *		'order_by' => array(
	 *			'field' => 'ASC',
	 *			'id' => 'DESC'
	 *		),
	 *		'limit' => 20,
	 *		'offset' => 0
	 *	) );
	 *
	 * @param int/array $attr Record id or query array
	 * @return object/boolean Query result 
	 */
	public function get( $attr = NULL )
	{
		if ( ! empty( $attr ) AND is_array( $attr ) )
		{
			if ( isset( $attr['order_by'] ) AND is_array( $attr['order_by'] ) )
			{
				foreach ( $attr['order_by'] AS $field => $direction )
					$this->db->order_by( $field, $direction );
			}

			if ( isset( $attr['where_in'] ) AND is_array( $attr['where_in'] ) )
			{
				foreach ( $attr['where_in'] AS $key => $items )
					$this->db->where_in( $key, $items );
			}

			// if ( ! isset( $attr['limit'] ) ) $attr['limit'] = $this->assets->conf['items_per_page'];
			// if ( ! isset( $attr['offset'] ) AND ! empty( $this->uri->segment( 3 ) ) ) $attr['offset'] = $this->uri->segment( 3 );

			if ( isset( $attr['join'] ) AND is_array( $attr['join'] ) )
			{
				$this->db->join( $attr['join']['table'], $attr['join']['comparison'], $attr['join']['direction'] );
			}
			
			$limit = ( ! empty( $attr['limit'] ) ) ? $attr['limit'] : FALSE;
			$offset = ( ! empty( $attr['offset'] ) ) ? $attr['offset'] : 0;
			unset( $attr['limit'], $attr['offset'], $attr['order_by'], $attr['where_in'], $attr['join'] );

			foreach ( $attr AS $key => $val )
			{
				$this->db->{$key}( $val );
			}

			if ( ! empty( $limit ) ) $this->db->limit( $limit, $offset );
		}
		elseif ( ! empty( $attr ) )
		{
			$this->db->where( 'print_order_id', (int)$attr );
		}
		else
		{
			$this->db->select( "*" );
		}

		if ( $query = $this->db->get( $this->assets->conf['tables']['print_orders'] ) AND $query->num_rows() > 0 )
		{
			if ( is_array( $attr ) OR empty( $attr ) )
			{
				return $query->result_array();
			}

			return $query->row_array();
		}

		return false;
	}
	
	/**
	 * Saves a registry or updates it
	 * 
	 * USAGE:
	 * ======
	 *
	 * When editing a registry, add the identifier id column in the array attribute
	 *
	 * @param array $data Data to be stored
	 * @return boolean/int The insert id
	 */
	public function save( array $data )
	{
		$author = $this->ion_auth->user()->row();

		// Nullify empty elements to store properly
		foreach ( $data AS $key => $val ) if ( empty( $data[$key] ) ) $data[$key] = NULL;

		if ( isset( $data['date_start_submit'] ) ) $data['date_start'] = $data['date_start_submit'];
		if ( isset( $data['date_end_submit'] ) ) $data['date_end'] = $data['date_end_submit'];

		// Unset unnecessary columns
		unset( $data['file'], $data['file_id'], $data['date_start_submit'], $data['date_end_submit'] );

		if ( empty( $data['print_order_id'] ) )
		{
			$data['creator'] = $author->id;
			$data['created'] = unix_to_human( now(), TRUE, 'eu' );

			if ( $this->db->insert( $this->assets->conf['tables']['print_orders'], $data ) )
			{
				$insert_id = $this->db->insert_id();
				return $insert_id;
			}
		}
		else
		{
			$data['modifier'] = $author->id;
			$data['modified'] = unix_to_human( now(), TRUE, 'eu' );

			$this->db->where( 'print_order_id', $data['print_order_id'] );
			
			if ( $this->db->update( $this->assets->conf['tables']['print_orders'], $data ) )
			{
				return true;
			}
		}
		
		return false;
	}

	/**
	 * Removes registries from DB
	 *
	 * @param int or array $id The item id
	 * @return boolean
	 */
	public function erase( $id )
	{
		if ( is_array( $id ) )
		{
			$this->db->where_in( 'print_order_id', $id );
		}
		else
		{
			$this->db->where( 'print_order_id', $id );
		}
		
		if ( $this->db->delete( $this->assets->conf['tables']['print_orders'] ) )
		{
			return true;
		}
		
		return false;
	}

	/**
	 * Counts the enabled users of a given print order
	 * 
	 * @param  int $print_order The print order ID or array
	 * @return integer
	 */
	public function count_remaining_bills( $print_order )
	{
		$this->load->model( "Sales_model", "sales" );

		if ( is_int( $print_order ) OR ! is_array( $print_order ) ) $print_order = $this->get( (int)$print_order );

		if ( ! empty( $print_order ) )
		{
			$number = 0;

			$sales = $this->sales->get( array(
				'where' => array(
					'print_order' => $print_order['print_order']
				),
				'order_by' => array(
					'sale_id' => 'DESC',
				),
				'limit' => 1
			) );

			if ( ! empty( $sales ) )
			{
				$bill = explode( '-', $sales[0]['bill_number'] );
				$number = (int)$bill[2];
			}
			else
			{
				$number = (int)$print_order['number_start'];
			}

			return ( (int)$print_order['number_end'] - $number );
		}

		return false;
	}

	/**
	 * Checks whether the print order is still valid or not
	 * 
	 * @param  int/array  $print_order The print order id or array
	 * @return boolean
	 */
	public function is_still_valid( $print_order )
	{
		if ( is_int( $print_order ) OR ! is_array( $print_order ) ) $print_order = $this->get( (int)$print_order );

		if ( ! empty( $print_order ) )
		{
			// If we don't have remaining, it is no longer valid
			if ( $this->count_remaining_bills( $print_order ) <= 0 ) return false;

			// If date expired, it is no longer valid
			if ( time() <= strtotime( $print_order['date_start'] ) ) return false;
			if ( time() >= strtotime( $print_order['date_end'] ) ) return false;

			return true;
		}

		return false;
	}

	/**
	 * Retrieves the print order row searching by print order number
	 * 
	 * @param  int $print_order_number The print order number
	 * @return boolean/array
	 */
	public function get_by_number( $print_order_number )
	{
		$print_order_list = $this->get( array(
			'where' => array(
				'print_order' => $print_order_number,
			)
		) );

		if ( ! empty( $print_order_list ) )
		{
			return $print_order_list[0];
		}

		return false;
	}
}
