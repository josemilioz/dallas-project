<?php  if ( ! defined( 'BASEPATH' ) ) exit( 'No direct script access allowed' );

class Categories_model extends CI_Model
{
	/**
	 * Class constructor
	 */
	public function __construct()
	{
		parent::__construct();
	}

	/**
	 * Gets the item data from the DB. 
	 *
	 *	USAGE:
	 *	======
	 *	$this->model->get( array(
	 *		'where' => array(
	 *			'field1' => $value1,
	 *			'field2' => $value2
	 *		),
	 *		'or_where' => array(
	 *			'field1' => $value1,
	 *			'field2' => $value2
	 *		),
	 *		'like' => array(
	 *			'field1' => $match
	 *		),
	 *		'or_like' => array(
	 *			'field1' => $match
	 *		),
	 *		'order_by' => array(
	 *			'field' => 'ASC',
	 *			'id' => 'DESC'
	 *		),
	 *		'limit' => 20,
	 *		'offset' => 0
	 *	) );
	 *
	 * @param int/array $attr Record id or query array
	 * @return object/boolean Query result 
	 */
	public function get( $attr = NULL )
	{
		if ( ! empty( $attr ) AND is_array( $attr ) )
		{
			if ( isset( $attr['order_by'] ) AND is_array( $attr['order_by'] ) )
			{
				foreach ( $attr['order_by'] AS $field => $direction )
					$this->db->order_by( $field, $direction );
			}

			if ( isset( $attr['where_in'] ) AND is_array( $attr['where_in'] ) )
			{
				foreach ( $attr['where_in'] AS $key => $items )
					$this->db->where_in( $key, $items );
			}

			// if ( ! isset( $attr['limit'] ) ) $attr['limit'] = $this->assets->conf['items_per_page'];
			// if ( ! isset( $attr['offset'] ) AND ! empty( $this->uri->segment( 3 ) ) ) $attr['offset'] = $this->uri->segment( 3 );

			if ( isset( $attr['join'] ) AND is_array( $attr['join'] ) )
			{
				$this->db->join( $attr['join']['table'], $attr['join']['comparison'], $attr['join']['direction'] );
			}
			
			$limit = ( ! empty( $attr['limit'] ) ) ? $attr['limit'] : FALSE;
			$offset = ( ! empty( $attr['offset'] ) ) ? $attr['offset'] : 0;
			unset( $attr['limit'], $attr['offset'], $attr['order_by'], $attr['where_in'], $attr['join'] );

			foreach ( $attr AS $key => $val )
			{
				$this->db->{$key}( $val );
			}

			if ( ! empty( $limit ) ) $this->db->limit( $limit, $offset );
		}
		elseif ( ! empty( $attr ) )
		{
			$this->db->where( 'category_id', (int)$attr );
		}
		else
		{
			$this->db->select( "*" );
		}

		if ( $query = $this->db->get( $this->assets->conf['tables']['categories'] ) AND $query->num_rows() > 0 )
		{
			if ( is_array( $attr ) OR empty( $attr ) )
			{
				return $query->result_array();
			}

			return $query->row_array();
		}

		return false;
	}
	
	/**
	 * Saves a registry or updates it
	 * 
	 * USAGE:
	 * ======
	 *
	 * When editing a registry, add the identifier id column in the array attribute
	 *
	 * @param array $data Data to be stored
	 * @return boolean/int The insert id
	 */
	public function save( array $data )
	{
		$author = $this->ion_auth->user()->row();

		// Nullify empty elements to store properly
		foreach ( $data AS $key => $val ) if ( empty( $data[$key] ) ) $data[$key] = NULL;

		// Unset unnecessary columns
		unset( $data['file'], $data['file_id'] );

		if ( empty( $data['category_id'] ) )
		{
			$data['creator'] = $author->id;
			$data['created'] = unix_to_human( now(), TRUE, 'eu' );

			$categories = $data['category']; unset( $data['category'] );

			if ( $this->db->insert( $this->assets->conf['tables']['categories'], $data ) )
			{
				$insert_id = $this->db->insert_id();
				return $insert_id;
			}
		}
		else
		{
			$data['modifier'] = $author->id;
			$data['modified'] = unix_to_human( now(), TRUE, 'eu' );

			$this->db->where( 'category_id', $data['category_id'] );
			
			if ( $this->db->update( $this->assets->conf['tables']['categories'], $data ) )
			{
				return true;
			}
		}
		
		return false;
	}

	/**
	 * Removes registries from DB
	 *
	 * @param int or array $id The item id
	 * @return boolean
	 */
	public function erase( $id )
	{
		$this->load->model( "Files_model", "files" );

		if ( is_array( $id ) )
		{
			foreach ( $id AS $v ) $this->files->erase_file_by_registry( $v, 'categories' );
			$this->db->where_in( 'category_id', $id );
		}
		else
		{
			$this->files->erase_file_by_registry( $id, 'categories' );
			$this->db->where( 'category_id', $id );
		}
		
		if ( $this->db->delete( $this->assets->conf['tables']['categories'] ) )
		{
			return true;
		}
		
		return false;
	}

	/**
	 * Returns an url for the category picture. By default, uses default no avatar
	 * 
	 * @param  int/array $category_id Category ID
	 * @param  string $size           The size option (thumb or normal)
	 * @return string
	 */
	public function picture( $category_id, $size = "thumb" )
	{
		$this->load->model( "Files_model", "files" );

		$picture = $this->files->get( array(
			'where' => array(
				'registry_id' => $category_id,
				'controller' => 'categories',
			)
		) );

		$return = $this->assets->conf['no_image'];

		if ( ! empty( $picture[0] ) )
		{
			switch ( $size )
			{
				default: case 'thumb':
					$filename = ltrim( $picture[0]['relative_location'], "/" ) . $picture[0]['name'] . "_thumb" . $picture[0]['extension'];
					break;
				case 'normal':
					$filename = ltrim( $picture[0]['relative_location'], "/" ) . $picture[0]['name_ext'];
					break;
			}

			if ( is_file( FCPATH . $filename ) ) $return = site_url( $filename );
		}

		return $return;
	}

	/**
	 * Count the number of products associated to the category
	 * 
	 * @param  int $category_id The category ID
	 * @return integer
	 */
	public function count_products( $category_id )
	{
		$this->db->where( 'category_id', $category_id );
		$this->db->where( 'trash', 0 );
		return $this->db->count_all_results( $this->assets->conf['tables']['products'] );
	}

	/**
	 * Builds the array for a dropdown with the active categories
	 * 
	 * @return array/boolean
	 */
	public function build_dropdown()
	{
		$categories = $this->get( array(
			'where' => array(
				'trash' => 0
			)
		) );

		if ( ! empty( $categories ) )
		{
			$return = array( '' => '&nbsp;' );

			foreach ( $categories AS $category )
			{
				$return[$category['category_id']] = $category['name'];
			}

			return $return;
		}

		return false;
	}

}
