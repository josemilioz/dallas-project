<?php  if ( ! defined( 'BASEPATH' ) ) exit( 'No direct script access allowed' );

class Sales_model extends CI_Model
{
	/**
	 * Class constructor
	 */
	public function __construct()
	{
		parent::__construct();
	}

	/**
	 * Gets the item data from the DB. 
	 *
	 *	USAGE:
	 *	======
	 *	$this->model->get( array(
	 *		'where' => array(
	 *			'field1' => $value1,
	 *			'field2' => $value2
	 *		),
	 *		'or_where' => array(
	 *			'field1' => $value1,
	 *			'field2' => $value2
	 *		),
	 *		'like' => array(
	 *			'field1' => $match
	 *		),
	 *		'or_like' => array(
	 *			'field1' => $match
	 *		),
	 *		'order_by' => array(
	 *			'field' => 'ASC',
	 *			'id' => 'DESC'
	 *		),
	 *		'limit' => 20,
	 *		'offset' => 0
	 *	) );
	 *
	 * @param int/array $attr Record id or query array
	 * @return object/boolean Query result 
	 */
	public function get( $attr = NULL )
	{
		if ( ! empty( $attr ) AND is_array( $attr ) )
		{
			if ( isset( $attr['order_by'] ) AND is_array( $attr['order_by'] ) )
			{
				foreach ( $attr['order_by'] AS $field => $direction )
					$this->db->order_by( $field, $direction );
			}

			if ( isset( $attr['where_in'] ) AND is_array( $attr['where_in'] ) )
			{
				foreach ( $attr['where_in'] AS $key => $items )
					$this->db->where_in( $key, $items );
			}

			if ( isset( $attr['join'] ) AND is_array( $attr['join'] ) )
			{
				$this->db->join( $attr['join']['table'], $attr['join']['comparison'], $attr['join']['direction'] );
			}

			$limit = ( ! empty( $attr['limit'] ) ) ? $attr['limit'] : FALSE;
			$offset = ( ! empty( $attr['offset'] ) ) ? $attr['offset'] : 0;
			unset( $attr['limit'], $attr['offset'], $attr['order_by'], $attr['where_in'], $attr['join'] );

			foreach ( $attr AS $key => $val )
			{
				$this->db->{$key}( $val );
			}

			if ( ! empty( $limit ) ) $this->db->limit( $limit, $offset );
		}
		elseif ( ! empty( $attr ) )
		{
			$this->db->where( 'sale_id', (int)$attr );
		}
		else
		{
			$this->db->select( "*" );
		}

		if ( $query = $this->db->get( $this->assets->conf['tables']['sales'] ) AND $query->num_rows() > 0 )
		{
			if ( is_array( $attr ) OR empty( $attr ) )
			{
				$result = $query->result_array();

				// Iterate results to transform meta vars and taxes
				foreach ( $result AS $k => $row )
				{
					if ( ! empty( $result[$k]['meta_vars'] ) ) $result[$k]['meta_vars'] = json_decode( $result[$k]['meta_vars'], TRUE );
					if ( ! empty( $result[$k]['taxes'] ) ) $result[$k]['taxes'] = json_decode( $result[$k]['taxes'], TRUE );
				}
			}
			else
			{
				$result = $query->row_array();

				// Transform meta vars and taxes
				if ( ! empty( $result['meta_vars'] ) ) $result['meta_vars'] = json_decode( $result['meta_vars'], TRUE );
				if ( ! empty( $result['taxes'] ) ) $result['taxes'] = json_decode( $result['taxes'], TRUE );
			}

			return $result;
		}

		return false;
	}
	
	/**
	 * Saves a registry or updates it
	 * 
	 * USAGE:
	 * ======
	 *
	 * When editing a registry, add the identifier id column in the array attribute
	 *
	 * @param array $data Data to be stored
	 * @return boolean/int The insert id
	 */
	public function save( array $data )
	{
		$this->load->model( "Orders_model", "orders" );

		$author = $this->ion_auth->user()->row();

		// Nullify empty elements to store properly
		foreach ( $data AS $key => $val ) if ( empty( $data[$key] ) ) $data[$key] = NULL;

		// Prepare metas - products list
		$sku 			= ( isset( $data['list_sku'] ) ) 			? $data['list_sku'] 		: NULL;
		$product_id 	= ( isset( $data['list_product_id'] ) ) 	? $data['list_product_id'] 	: NULL;
		$quantity 		= ( isset( $data['list_quantity'] ) ) 		? $data['list_quantity'] 	: NULL;
		$unit_price 	= ( isset( $data['list_unit_price'] ) ) 	? $data['list_unit_price'] 	: NULL;
		$tax 			= ( isset( $data['list_tax'] ) ) 			? $data['list_tax'] 		: NULL;
		$stock_id 		= ( isset( $data['list_stock_id'] ) ) 		? $data['list_stock_id'] 	: NULL;
		$description 	= ( isset( $data['list_description'] ) ) 	? $data['list_description'] : NULL;
		$row_total 		= ( isset( $data['list_row_total'] ) ) 		? $data['list_row_total'] 	: NULL;

		// Unset unnecessary columns
		unset( $data['list_sku'], $data['list_product_id'], $data['list_quantity'], $data['list_unit_price'], $data['list_tax'], $data['list_stock_id'], $data['list_description'], $data['list_row_total'], $data['file'], $data['file_id'] );

		// Prepare orders in case it exists
		if ( isset( $data['order_id'] ) AND ! empty( $data['order_id'] ) )
		{
			$order_product_id 			= ( isset( $data['order_product_id'] ) ) 			? $data['order_product_id'] 		: NULL;
			$order_quantity_remaining 	= ( isset( $data['order_quantity_remaining'] ) ) 	? $data['order_quantity_remaining'] : NULL;

			unset( $data['order_product_id'], $data['order_description'], $data['order_unit_price'], $data['order_quantity'], $data['order_quantity_remaining'], $data['order_unit_measure'],$data['order_product_free'] );
		}

		// Addapt dates from picker
		if ( isset( $data['sale_date'] ) ) $data['sale_date'] = NULL;
		if ( isset( $data['expiration_date'] ) ) $data['expiration_date'] = NULL;

		if ( ! empty( $data['sale_date_submit'] ) ) $data['sale_date'] = $data['sale_date_submit'];
		if ( ! empty( $data['expiration_date_submit'] ) ) $data['expiration_date'] = $data['expiration_date_submit'];

		// Prepare taxes
		if ( isset( $data['taxes'] ) AND is_array( $data['taxes'] ) AND ! empty( $data['taxes'] ) ) $data['taxes'] = json_encode( $data['taxes'] );

		// Prepare meta vars
		if ( isset( $data['meta_vars'] ) AND is_array( $data['meta_vars'] ) AND ! empty( $data['meta_vars'] ) ) $data['meta_vars'] = json_encode( $data['meta_vars'] );

		unset( $data['sale_date_submit'], $data['expiration_date_submit'] );

		// Unset exclusively JS inputs
		unset( $data['add_product_id'], $data['add_quantity'], $data['add_unit_price'], $data['add_column'], $data['row_total'] );

		if ( empty( $data['sale_id'] ) )
		{
			$data['creator'] = $author->id;
			$data['created'] = unix_to_human( now(), TRUE, 'eu' );

			if ( $this->db->insert( $this->assets->conf['tables']['sales'], $data ) )
			{
				$insert_id = $this->db->insert_id();
				// Store metas
				if ( ! empty( $sku ) AND ! empty( $product_id ) AND ! empty( $quantity ) AND ! empty( $unit_price ) )
				{
					$this->meta_save( $insert_id, $sku, $product_id, $quantity, $unit_price, $tax, $row_total, $description, $stock_id );
				}

				// Update remainings in case there's an order
				if ( isset( $order_product_id ) AND isset( $data['order_id'] ) )
				{
					$this->orders->meta_update_remainings( $data['order_id'], $order_product_id, $order_quantity_remaining );
				}

				return $insert_id;
			}
		}
		else
		{
			$data['modifier'] = $author->id;
			$data['modified'] = unix_to_human( now(), TRUE, 'eu' );

			$this->db->where( 'sale_id', $data['sale_id'] );
			
			if ( $this->db->update( $this->assets->conf['tables']['sales'], $data ) )
			{
				// Erase previous metas and store again
				if ( ! empty( $sku ) AND ! empty( $product_id ) AND ! empty( $quantity ) AND ! empty( $unit_price ) )
				{
					$this->meta_erase( $data['sale_id'] );
					$this->meta_save( $data['sale_id'], $sku, $product_id, $quantity, $unit_price, $tax, $row_total, $description, $stock_id );
				}

				// Update remainings in case there's an order
				if ( isset( $order_product_id ) AND isset( $data['order_id'] ) )
				{
					$this->orders->meta_update_remainings( $data['order_id'], $order_product_id, $order_quantity_remaining );
				}

				return true;
			}
		}
		
		return false;
	}

	/**
	 * Removes registries from DB
	 *
	 * @param int or array $id The item id
	 * @return boolean
	 */
	public function erase( $id )
	{
		if ( is_array( $id ) )
		{
			$this->db->where_in( 'sale_id', $id );
		}
		else
		{
			$this->db->where( 'sale_id', $id );
		}
		
		if ( $this->db->delete( $this->assets->conf['tables']['sales'] ) )
		{
			return true;
		}
		
		return false;
	}

	/**
	 * Stores the meta we want to associate to the sale_id
	 * @param  int     $sale_id     The Sale ID
	 * @param  array   $sku         The SKU array
	 * @param  array   $product_id  The product IDs array
	 * @param  array   $quantity    The quantity array
	 * @param  array   $unit_price  The unit price array
	 * @param  array   $tax         The tax array
	 * @param  array   $row_total   The row total array
	 * @param  array   $description The description array
	 * @param  array   $stock_id    The stock ids array
	 * @param  boolean $sub_qty     Boolean to say if we want to substract quantity from stock (only use when combining unbilled sales)
	 * @return void
	 */
	public function meta_save( $sale_id, array $sku, array $product_id, array $quantity, array $unit_price, array $tax, array $row_total, array $description, array $stock_id, $sub_qty = TRUE )
	{
		foreach ( $sku AS $k => $s )
		{
			$new_row = array(
				'sale_id' 		=> ( ! empty( $sale_id ) ) 			? $sale_id 			: NULL,
				'sku' 			=> ( ! empty( $s ) ) 				? $s 				: NULL,
				'product_id' 	=> ( ! empty( $product_id[$k] ) ) 	? $product_id[$k] 	: NULL,
				'quantity' 		=> ( ! empty( $quantity[$k] ) ) 	? $quantity[$k] 	: NULL,
				'unit_price' 	=> ( ! empty( $unit_price[$k] ) ) 	? $unit_price[$k] 	: NULL,
				'tax' 			=> ( ! empty( $tax[$k] ) ) 			? $tax[$k] 			: NULL,
				'row_total'		=> ( ! empty( $row_total[$k] ) ) 	? $row_total[$k] 	: NULL,
				'description'	=> ( ! empty( $description[$k] ) ) 	? $description[$k] 	: NULL,
				'stock_id'		=> ( ! empty( $stock_id[$k] ) ) 	? $stock_id[$k] 	: NULL,
			);

			$this->db->insert( $this->assets->conf['tables']['sales_stock'], $new_row );

			// Update stock
			/*
			$tbl 		= $this->db->dbprefix( $this->assets->conf['tables']['stock'] );
			$stock_id 	= $stock_id[$k];
			$qty 		= (float)$quantity[$k];

			$this->db->where( 'sku', $s );
			$this->db->where( 'stock_id', $stock_id );
			$this->db->where( 'quantity >', 0 );
			$count = $this->db->count_all_results( $tbl );

			if ( $count > 0 AND $sub_qty === TRUE ) $this->db->query( "UPDATE $tbl SET $tbl.quantity = $tbl.quantity-$qty WHERE ( $tbl.sku = $s AND $tbl.stock_id = $stock_id )" );
			*/
		}
	}

	/**
	 * Gets all the meta associated to the sale_id given
	 * 
	 * @param  int $sale_id The purchase id
	 * @return array/boolean
	 */
	public function meta_get( $sale_id )
	{
		$this->db->where( 'sale_id', $sale_id );

		if ( $query = $this->db->get( $this->assets->conf['tables']['sales_stock'] ) AND $query->num_rows() > 0 )
		{
			return $query->result_array();
		}

		return false;		
	}

	/**
	 * Erases all the meta associated to the sale_id given
	 * 
	 * @param  int     $sale_id       The purchase id
	 * @param  boolean $restore_stock Whether we restore the stock or not
	 * @return boolean
	 */
	public function meta_erase( $sale_id, $restore_stock = FALSE )
	{
		// Get the metas, and add to the quantity on stock
		if ( $restore_stock == TRUE )
		{
			$this->load->model( "Stock_model", "stock" );
			$metas = $this->meta_get( $sale_id );

			if ( ! empty( $metas ) )
			{
				foreach ( $metas AS $meta )
				{
					if ( ! empty( $meta['stock_id'] ) AND ! empty( $meta['sku'] ) )
					{
						$stock_row = $this->stock->get( $meta['stock_id'] );

						if ( ! empty( $stock_row ) )
						{
							$this->stock->save( array(
								'stock_id' => $stock_row['stock_id'],
								'quantity' => (float)( (float)$meta['quantity'] + (float)$stock_row['quantity'] ),
							) );
						}
					}
				}
			}
		}

		$this->db->where( 'sale_id', $sale_id );

		if ( $this->db->delete( $this->assets->conf['tables']['sales_stock'] ) )
		{
			return true;
		}
		
		return false;
	}

	/**
	 * Prepares the data to iterate every row, whether it coming from an actual meta or coming from POST
	 * 
	 * @param  array  $data The data
	 * @return array
	 */
	public function prepare_meta( array $data )
	{
		$this->load->model( "Products_model", "products" );

		if ( isset( $data['list_product_id'] ) AND ! empty( $data ) ) // Only if we are not editing
		{
			$rows = array();

			foreach ( $data['list_product_id'] AS $k => $reference )
			{
				$product = $this->products->get( $data['list_product_id'][$k] );

				$rows[$k]['sku'] 			= ( ! empty( $data['list_sku'][$k] ) ) 			? $data['list_sku'][$k] 			: NULL;
				$rows[$k]['stock_id'] 		= ( ! empty( $data['list_stock_id'][$k] ) ) 	? $data['list_stock_id'][$k] 		: NULL;
				$rows[$k]['product_id'] 	= ( ! empty( $data['list_product_id'][$k] ) ) 	? $data['list_product_id'][$k] 		: NULL;
				$rows[$k]['description'] 	= ( ! empty( $row['list_description'][$k] ) ) 	? $data['list_description'][$k] 	: $product['name'];
				$rows[$k]['unit_price'] 	= ( ! empty( $data['list_unit_price'][$k] ) ) 	? $data['list_unit_price'][$k]		: $product['price'];
				$rows[$k]['quantity'] 		= ( ! empty( $data['list_quantity'][$k] ) ) 	? $data['list_quantity'][$k] 		: NULL;
				$rows[$k]['row_total'] 		= ( ! empty( $data['list_row_total'][$k] ) ) 	? $data['list_row_total'][$k] 		: NULL;
				$rows[$k]['tax'] 			= ( ! empty( $data['list_tax'][$k] ) ) 			? $data['list_tax'][$k] 			: $product['tax'];
			}

			$data = $rows;
		}

		return $data;
	}

	/**
	 * Checks whether a sale has been paid already or not
	 * 
	 * @param  int/array $sale The sale or its ID
	 * @return boolean
	 */
	public function is_paid( $sale )
	{
		$this->load->model( "Movements_model", "movements" );

		$sale = ( is_array( $sale ) AND isset( $sale['sale_id'] ) ) ? $sale : $this->get( $sale );
		
		if ( ! empty( $sale ) )
		{
			$movement = $this->movements->get( array(
				'where' => array(
					'transaction_type' => 'sales',
					'transaction_id' => $sale['sale_id'],
				)
			) );

			if ( ! empty( $movement ) ) return true;
		}

		return false;
	}

}
