<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| Hooks
| -------------------------------------------------------------------------
| This file lets you define "hooks" to extend CI without hacking the core
| files.  Please see the user guide for info:
|
|	https://codeigniter.com/user_guide/general/hooks.html
|
*/

// Exchange hook
$hook['post_controller'][] = array(
    'class'		=> 'Currency',
    'function'	=> 'retrieve_exchange_file',
    'filename'	=> 'Currency.php',
    'filepath'	=> 'libraries',
    'params'	=> NULL
);

// Lock down verificator
$hook['post_controller'][] = array(
    'class'     => 'Permissions',
    'function'  => 'system_locked_screen',
    'filename'  => 'Permissions.php',
    'filepath'  => 'libraries',
    'params'    => NULL
);

// Backup maker
$hook['post_controller_constructor'] = array(
    'class'     => 'Backup',
    'function'  => 'database_backup',
    'filename'  => 'Backup.php',
    'filepath'  => 'libraries',
    'params'    => NULL
);
