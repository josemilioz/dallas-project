<?php defined( 'BASEPATH' ) OR exit( 'No direct script access allowed' );

$config['error_prefix'] = '<div class="badge badge-danger field-error">';
$config['error_suffix'] = '</div>';