<?php  if ( ! defined( 'BASEPATH' ) ) exit( 'No direct script access allowed' );

/**
 * ------------------------------------------------------------------------------------------------
 * TABLES
 * ------------------------------------------------------------------------------------------------
 */
$config['system']['tables']['logs']					= 'auth_logs';
$config['system']['tables']['users']				= 'auth_users';
$config['system']['tables']['settings']				= 'auth_settings';

$config['system']['tables']['categories']			= 'categories';
$config['system']['tables']['clients']				= 'clients';
$config['system']['tables']['files']				= 'files';
$config['system']['tables']['movements']			= 'movements';
$config['system']['tables']['orders']				= 'orders';
$config['system']['tables']['orders_products']		= 'orders_products';
$config['system']['tables']['pos']					= 'pos';
$config['system']['tables']['pos_users']			= 'pos_users';
$config['system']['tables']['pos_categories']		= 'pos_categories';
$config['system']['tables']['purchases']			= 'purchases';
$config['system']['tables']['purchases_products']	= 'purchases_products';
$config['system']['tables']['print_orders']			= 'print_orders';
$config['system']['tables']['products']				= 'products';
$config['system']['tables']['sales']				= 'sales';
$config['system']['tables']['sales_stock']			= 'sales_stock';
$config['system']['tables']['stock']				= 'stock';

/**
 * ------------------------------------------------------------------------------------------------
 * MISC
 * ------------------------------------------------------------------------------------------------
 */
$config['system']['languages']['es_ES']		= __( "Spanish" );
$config['system']['languages']['en_US']		= __( "English (United States)" );
$config['system']['language']				= 'en_US';

$config['system']['company_name']			= "The Company";
$config['system']['product_name']			= "Dallas Project";
$config['system']['short_product_name']     = "Dallas";
$config['system']['cache_assets']			= FALSE;
$config['system']['activate_log']			= FALSE;
$config['system']['version']                = '1.0.3';
$config['system']['backup_renewal_rate']    = '6'; // in hours
$config['system']['due_alert_before']       = '5'; // in days
$config['system']['due_day']                = '10'; // of every month
$config['system']['due_alert_date_format']  = '%d de %B'; // strftime format
$config['system']['due_alert_cookie_dur']   = '6'; // in hours

$config['system']['mandatory']				= ' <span class="mandatory">*</span>';
$config['system']['no_data']				= ' <em class="text-muted">' . __( "No Data" ) . '</em>';

$config['system']['locked_file_path']		= FCPATH . '.locked';

$config['system']['database_backup_dir']	= FCPATH . 'sql/';
$config['system']['database_backup_file']	= 'backup.sql';
$config['system']['database_backup_path']	= $config['system']['database_backup_dir'] . $config['system']['database_backup_file'];
$config['system']['mysql_dumper']			= ( `which mysqldump` ) ? "/usr/bin/mysqldump" : "/Applications/MAMP/Library/bin/mysqldump";

/**
 * ------------------------------------------------------------------------------------------------
 * DATES & FORMATS
 * ------------------------------------------------------------------------------------------------
 */
$config['system']['date_format']			= 'd-m-Y';
$config['system']['short_date_format']		= 'd-m-y';
$config['system']['time_format']			= 'H:i';
$config['system']['log_date_format']		= 'd-m-Y H:i:s';
$config['system']['datetime_format']		= $config['system']['date_format'] . " " . $config['system']['time_format'];
$config['system']['date_format_js']			= 'dd-mm-yyyy';
$config['system']['decimals']				= 0; // For currency
$config['system']['qty_decimals']			= 2; // For quantities
$config['system']['currency']				= "PYG";
$config['system']['decimal_point']          = ",";
$config['system']['thousands_separator']    = ".";

$config['system']['items_per_page']			= 10;
$config['system']['pages_to_show']			= 3;
$config['system']['no_image']				= site_url( 'assets/img/default-no-image.png' );
$config['system']['no_avatar']				= site_url( 'assets/img/default-avatar.png' );

/**
 * ------------------------------------------------------------------------------------------------
 * UNITS
 * ------------------------------------------------------------------------------------------------
 */
$config['system']['units']['']				= '&nbsp;';
$config['system']['units']['ml']			= __( "ml." );
$config['system']['units']['lts']			= __( "Lts." );
$config['system']['units']['doses']			= __( "Doses" );
$config['system']['units']['kg']			= __( "Kg." );
$config['system']['units']['tn']			= __( "Tn." );
$config['system']['units']['mts']			= __( "Mts." );
$config['system']['units']['unit']			= __( "Units" );

/**
 * ------------------------------------------------------------------------------------------------
 * CURRENCIES
 * ------------------------------------------------------------------------------------------------
 */
$config['system']['currencies']['PYG']		= "G$";//"₲";
$config['system']['currencies']['USD']		= "US$";
$config['system']['currencies']['BRL']		= "R$";
$config['system']['currencies']['ARS']		= "AR$";
$config['system']['currencies']['EUR']		= "€";

$config['system']['currencies_ext']['']		= '&nbsp;';
$config['system']['currencies_ext']['PYG']	= $config['system']['currencies']['PYG'] . " (" . __( "Paraguayan Guarani" ) . ")";
$config['system']['currencies_ext']['USD']	= $config['system']['currencies']['USD'] . " (" . __( "American Dollar" ) . ")";
$config['system']['currencies_ext']['BRL']	= $config['system']['currencies']['BRL'] . " (" . __( "Brazilian Real" ) . ")";
$config['system']['currencies_ext']['ARS']	= $config['system']['currencies']['ARS'] . " (" . __( "Argentinian Peso" ) . ")";
$config['system']['currencies_ext']['EUR']	= $config['system']['currencies']['EUR'] . " (" . __( "Euro" ) . ")";

$config['system']['auto_update_source']		= "http://emaitre.net/_assets/cotizacion.xml";
$config['system']['auto_update_exchange']	= FALSE;
$config['system']['auto_update_hours']		= 24;

/**
 * ------------------------------------------------------------------------------------------------
 * PAYMENTS
 * ------------------------------------------------------------------------------------------------
 */
$config['system']['payment_method']['cash']			= __( "Cash" );
$config['system']['payment_method']['check']		= __( "Check" );
$config['system']['payment_method']['debit']		= __( "Debit Card" );
$config['system']['payment_method']['credit']		= __( "Credit Card" );
$config['system']['payment_method']['deposit']		= __( "Bank Deposit" );
$config['system']['payment_method']['transfer']		= __( "Money Wiring" );

$config['system']['movements']['income']			= __( "Income" );
$config['system']['movements']['outcome']			= __( "Outcome" );

$config['system']['stock']['min_warning']			= 10;
$config['system']['stock']['min_danger']			= 5;

$config['system']['cc_company']['mastercard']		= __( "MasterCard" );
$config['system']['cc_company']['visa']				= __( "Visa" );
$config['system']['cc_company']['amex']				= __( "American Express" );
$config['system']['cc_company']['diners']			= __( "Diners Club" );
$config['system']['cc_company']['cabal']			= __( "Cabal" );
$config['system']['cc_company']['discover']			= __( "Discover" );
$config['system']['cc_company']['credifielco']		= __( "Credifielco Card" );
$config['system']['cc_company']['credicard']		= __( "Credicard" );
$config['system']['cc_company']['bancard']			= __( "Bancard Check" );
$config['system']['cc_company']['other']			= __( "Other" );

$config['system']['dc_company']['cirrus']			= __( "Cirrus" );
$config['system']['dc_company']['maestro']			= __( "Maestro" );
$config['system']['dc_company']['visa_debito']		= __( "Visa Debit" );
$config['system']['dc_company']['infonet']			= __( "Infonet" );
$config['system']['dc_company']['dinelco']			= __( "Dinelco" );
$config['system']['dc_company']['unica']			= __( "Única" );
$config['system']['dc_company']['creditiva']		= __( "Creditiva" );
$config['system']['dc_company']['other']			= __( "Other" );

/**
 * ------------------------------------------------------------------------------------------------
 * BILLING
 * ------------------------------------------------------------------------------------------------
 */
$config['system']['bill_types']['full']						= __( "Full" );
$config['system']['bill_types']['credit']					= __( "Credit" );

$config['system']['bill_columns']['tax_no']					= __( "No Tax" );
$config['system']['bill_columns']['tax_5']					= __( "VAT 5%" );
$config['system']['bill_columns']['tax_10']					= __( "VAT 10%" );

$config['system']['tax_multipliers']['tax_no']				= 0;
$config['system']['tax_multipliers']['tax_5']				= 0.047619;
$config['system']['tax_multipliers']['tax_10']				= 0.090909;

$config['system']['casual_client']['name']					= __( "Casual Client" );
$config['system']['casual_client']['ruc']					= '44444444-7';

$config['system']['company_names']['to_remove']				= array( '.', '-', '_', '~', '!', '#', '@', '$', '%', ':', '/', '+', '=', '<', '>', '?', '¿', '¡', '[', ']', '{', '}', '(', ')', '*', '&', '^', '`', ';', ',' );
$config['system']['company_names']['matches']				= array( ' Sa', ' Srl', ' Saci', ' Saeca', ' Sacei', ' Saic', ' Inc', ' Corp', ' Ltd', ' Sacil', ' Ail', 'Esa', 'Sae' );
$config['system']['company_names']['replaces']				= array( ' SA', ' SRL', ' SACI', ' SAECA', ' SACEI', ' SAIC', ' INC', ' CORP', ' LTD', ' SACIL', ' AIL', 'ESA', 'SAE' );

$config['system']['bill_header']['oficial_company_name']	= "The Company S.A.";
$config['system']['bill_header']['activity']				= "Comercio al por mayor de productos químicos industriales";
$config['system']['bill_header']['address']					= "Ruta 6ta. Dr Juan Leon Mallorquín Km. 44.7 | Ciudad del Este, Itapúa, Paraguay";
$config['system']['bill_header']['phone']					= "(0777) 777 777";
$config['system']['bill_header']['email']					= "info@thecompany.com.py";
$config['system']['bill_header']['website']					= "www.thecompany.com.py";
$config['system']['bill_header']['company_ruc']				= "80004534-5";

/**
 * ------------------------------------------------------------------------------------------------
 * SEARCH
 * ------------------------------------------------------------------------------------------------
 */
$config['system']['search_columms']['products'] 	= array( 'name', 'description', 'price' );
$config['system']['search_columms']['clients'] 		= array( 'first_name', 'last_name', 'observations', 'city', 'state', 'country', 'ruc' );
$config['system']['search_columms']['purchases'] 	= array( 'purchase_number', 'provider', 'observations', 'total_amount', 'bill_number' );
$config['system']['search_columms']['sales'] 		= array( 'type', 'total_amount', 'bill_number', 'name', 'ruc', 'address' );
$config['system']['search_columms']['movements'] 	= array( 'comments', 'label', 'book', 'amount' );

/**
 * ------------------------------------------------------------------------------------------------
 * GROUP PERMISSIONS
 * Check the groups and their IDs to stablish this relation
 * ------------------------------------------------------------------------------------------------
 */

$config['system']['no_permission']['message']							= __( "Your user level does not grant you that action. Get in touch with an administrator for more information." );
$config['system']['no_permission']['title']								= __( "Disabled action" );

$config['system']['group_permissions']['sales']['edit_quantity']		= array( 1, 2 );
$config['system']['group_permissions']['sales']['edit_price']			= array( 1, 2 );
$config['system']['group_permissions']['sales']['edit_bill_number']		= array( 1, 2 );

$config['system']['group_permissions']['purchases']['edit_quantity']	= array( 1, 2 );
$config['system']['group_permissions']['purchases']['edit_price']		= array( 1, 2 );

$config['system']['group_permissions']['orders']['edit_quantity']		= array( 1, 2 );
$config['system']['group_permissions']['orders']['edit_remaining']		= array( 1, 2 );
$config['system']['group_permissions']['orders']['edit_price']			= array( 1, 2 );
