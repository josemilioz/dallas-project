<?php if ( ! defined( 'BASEPATH' ) ) exit( 'No direct script access allowed' );

class Backup
{
	/**
	 * The class constructor
	 *
	 * @access	public
	 * @return	none
	 */
	public function __construct(){}

	/**
	 * Enables the use of CI super-global without having to define an extra variable.
	 *
	 * I can't remember where I first saw this, so thank you if you are the original author. -Militis
	 *
	 * @access	public
	 * @param	$var
	 * @return	mixed
	 */
	public function __get( $var )
	{
		return get_instance()->$var;
	}

	/**
	 * Loads the settings from the database
	 * 
	 * @param  none
	 * @return void
	 */
	public function database_backup()
	{
		$proceed = FALSE;

		if ( ! is_dir( $this->assets->conf['database_backup_dir'] ) )
		{
			mkdir( $this->assets->conf['database_backup_dir'] );
		}

		if ( ! is_file( $this->assets->conf['database_backup_path'] ) )
		{
			$proceed = TRUE;
		}
		else
		{
			$last_modification = filemtime( $this->assets->conf['database_backup_path'] );
			if ( ( $last_modification + ( $this->assets->conf['backup_renewal_rate'] * 3600 ) ) < time() ) $proceed = TRUE;
		}

		if ( $proceed )
		{
			$this->do_database_backup( $this->assets->conf['database_backup_path'] );
		}
	}

	/**
	 * The actual process that stores the file
	 * 
	 * @param  string $filepath The file path to the place it will be stored
	 * @return void
	 */
	public function do_database_backup( $filepath )
	{
		passthru( sprintf( 
			"%s --force --host=%s --user=%s --password=%s --add-drop-table --skip-lock-tables %s > %s", 
			escapeshellcmd( $this->assets->conf['mysql_dumper'] ),
			escapeshellcmd( $this->db->hostname ),
			escapeshellcmd( $this->db->username ),
			escapeshellcmd( $this->db->password ),
			$this->db->database,
			escapeshellcmd( $filepath )
		), $error );
		
		if ( isset( $error ) AND ! empty( $error ) )
		{
			error_log( $error );
			return false;
		}

		return true;
	}
}
