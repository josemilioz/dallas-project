<?php if ( ! defined( 'BASEPATH' ) ) exit( 'No direct script access allowed' );

class Logs
{
	private $result	= "ACCESS";
	private $data	= NULL;
	private $type	= NULL;

	/**
	 * The class constructor
	 *
	 * @access	public
	 * @return	none
	 */
	public function __construct()
	{
		$this->load->model( 'Logs_model', 'logs_model' );
		$this->load->library( 'user_agent' );
	}

	/**
	 * Enables the use of CI super-global without having to define an extra variable.
	 *
	 * I can't remember where I first saw this, so thank you if you are the original author. -Militis
	 *
	 * @access	public
	 * @param	$var
	 * @return	mixed
	 */
	public function __get( $var )
	{
		return get_instance()->$var;
	}

	/**
	 * Gets the result element of the object
	 * 
	 * @return mixed $result
	 */
	public function get_result()
	{
		return $this->result;
	}

	/**
	 * Establishes the result element of the object by a boolean. True means Success and False means Failure.
	 * 
	 * @param mixed $result The result to be set
	 */
	public function set_result( $result )
	{
		if ( $result === TRUE )
			$this->result = 'SUCCESS';
		elseif ( $result === FALSE )
			$this->result = 'FAILURE';

		return $this;
	}

	/**
	 * Get the data element and everything stored within
	 * 
	 * @return mixed $data
	 */
	public function get_data()
	{
		return $this->data;
	}

	/**
	 * Establishes the data for the object
	 * 
	 * @param mixed  $data The whole data to be stored raw (usually an array)
	 * @param string $type The type (protocol) of information sent
	 */
	public function set_data( $data, $type = "POST" )
	{
		$this->data = $data;
		$this->type = $type;
		return $this;
	}

	/**
	 * Stores the so-far fetched data into the elements to the database
	 * 
	 * @return void
	 */
	public function register() 
	{
		if ( $this->assets->conf['activate_log'] )
		{
			$data = array(
				'user_id' 	=> ( ! empty( $this->ion_auth->user()->row()->id ) ) ? $this->ion_auth->user()->row()->id : NULL,
				'uri' 		=> uri_string(),
				'date' 		=> unix_to_human( now(), TRUE, 'eu' ),
				'ip' 		=> $this->input->ip_address(),
				'browser' 	=> $this->agent->browser(),
				'version' 	=> $this->agent->version(),
				'os' 		=> $this->agent->platform(),
				'result' 	=> $this->get_result(),
				'type' 		=> $this->type,
			);
			
			if ( ! empty( $this->data ) ) $data['data'] = json_encode( $this->get_data() );
			
			$this->logs_model->add( $data );
		}
	}

}
