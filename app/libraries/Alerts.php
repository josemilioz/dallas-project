<?php if ( ! defined( 'BASEPATH' ) ) exit( 'No direct script access allowed' );

class Alerts
{
	public $elements = NULL;
	protected $persistance = FALSE;

	/**
	 * The class constructor
	 *
	 * @access	public
	 * @return	none
	 */
	public function __construct()
	{
		if ( $this->session->flashdata( 'alert_elements' ) )
		{
			$this->elements = $this->session->flashdata( 'alert_elements' );
		}
	}

	/**
	 * Enables the use of CI super-global without having to define an extra variable.
	 *
	 * I can't remember where I first saw this, so thank you if you are the original author. -Militis
	 *
	 * @access	public
	 * @param	$var
	 * @return	mixed
	 */
	public function __get( $var )
	{
		return get_instance()->$var;
	}

	/**
	 * Stores the alert in the browser memory, in case we're refreshing
	 * 
	 * @param  boolean $to_persist
	 * @return object
	 */
	public function persist( $to_persist )
	{
		$this->persistance = $to_persist;
		return $this;
	}

	/**
	 * Defines the alert
	 * 
	 * @param  string  $message
	 * @param  string  $title
	 * @param  string  $errors
	 * @param  string  $class
	 * @param  boolean $dismissible
	 * @return object
	 */
	public function alert( $message = NULL, $title = NULL, $errors = NULL, $class = "warning", $dismissible = TRUE )
	{
		$this->elements = array(
			'message' => $message,
			'errors' => $errors,
			'title' => $title,
			'class' => $class,
			'dismissible' => $dismissible,
		);

		if ( $this->persistance )
		{
			$this->session->set_flashdata( 'alert_elements', $this->elements );
		}
		else
		{
			return $this->elements;
		}
	}

	/**
	 * Displays the alert with the information
	 * 
	 * @param  string $id
	 * @return echo
	 */
	public function output_alert( $id = NULL )
	{
		if ( ! empty( $this->elements ) )
		{
			if ( ! empty( $id ) ) $this->elements['id'] = 'id="' . $id . '"';
			if ( ! empty( $this->elements['class'] ) ) $this->elements['message_code'] = $this->elements['class'];
			if ( ! empty( $this->elements['dismissible'] ) ) $this->elements['class'] .= " alert-dismissible";

			if ( ! empty( $this->elements['errors'] ) )
			{
				$new_errors = str_replace( array( '<div', '</div>' ), array( '<p><div', '</div></p>' ), $this->elements['errors'] );
				$new_errors = strip_tags( $new_errors, "<p><ol><ul><li>" );
				$this->elements['errors'] = $new_errors;
			}

			if ( 
				! empty( $this->elements['message_code'] ) OR 
				! empty( $this->elements['class'] ) OR 
				! empty( $this->elements['title'] ) OR 
				! empty( $this->elements['message'] ) OR 
				! empty( $this->elements['errors'] ) 
			) $this->load->view( 'alerts/' . $this->elements['message_code'] . '.php', $this->elements );			
		}
	}
}
