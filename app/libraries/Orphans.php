<?php if ( ! defined( 'BASEPATH' ) ) exit( 'No direct script access allowed' );

class Orphans
{
	private $forbidden_places = array();

	/**
	 * The class constructor
	 *
	 * @access	public
	 * @return	none
	 */
	public function __construct()
	{
		// Overrided controllers, so we don't delete the empty registry yet, if we're working over it.
		$this->forbidden_places = array( 'files/', 'clients/add', 'users/add', 'movements/add', 'purchases/add', 'products/add' );

		if ( $this->ion_auth->logged_in() )$this->remove_orphans();
	}

	/**
	 * Enables the use of CI super-global without having to define an extra variable.
	 *
	 * I can't remember where I first saw this, so thank you if you are the original author. -Militis
	 *
	 * @access	public
	 * @param	$var
	 * @return	mixed
	 */
	public function __get( $var )
	{
		return get_instance()->$var;
	}

	/**
	 * Checks the places where the orphan removal is not applicated
	 * 
	 * @return boolean
	 */
	private function place_enabled()
	{
		if ( ! empty( $this->forbidden_places ) )
		{
			foreach ( $this->forbidden_places AS $place )
			{
				if ( strpos( uri_string(), $place ) !== FALSE ) return FALSE;
			}
		}

		return TRUE;
	}
	
	/**
	 * Clear all the orphans rows that didn't get completed
	 * 
	 * @return boolean
	 */
	public function remove_orphans()
	{
		if ( $this->place_enabled() ) // Check we're not uploading picture
		{
			$this->load->model( "Files_model", "files" );

			$this->db->where( 'creator', $this->ion_auth->user()->row()->id );
			$this->db->where( 'registry_id IS NULL', NULL );

			$orphans = $this->files->get( array() );

			if ( ! empty( $orphans ) )
			{
				$this->logs->set_data( array(  'action' => 'remove_orphans', 'orphans' => $orphans ), "POST" )->set_result( TRUE )->register();

				foreach ( $orphans AS $orphan ) $this->files->erase( $orphan['file_id'] );

				return true;
			}
		}

		return false;
	}
}