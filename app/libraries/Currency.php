<?php if ( ! defined( 'BASEPATH' ) ) exit( 'No direct script access allowed' );

class Currency
{
	public $exchange = array();

	/**
	 * The class constructor.
	 *
	 * @access	public
	 * @return	none
	 */
	public function __construct()
	{
		$this->apply_exchange();
	}

	/**
	 * Enables the use of CI super-global without having to define an extra variable.
	 *
	 * I can't remember where I first saw this, so thank you if you are the original author. -Militis
	 *
	 * @access	public
	 * @param	$var
	 * @return	mixed
	 */
	public function __get( $var )
	{
		return get_instance()->$var;
	}

	/**
	 * Retrieves the exchange file.
	 * 
	 * @return void
	 */
	public function retrieve_exchange_file()
	{
		$this->load->helper( 'text' );

		$source 	= $this->assets->conf['auto_update_source'];
		$filename 	= "exchange.json";
		$filepath 	= FCPATH . 'uploads/' . $filename;
		$proceed 	= FALSE;

		if ( ! is_file( $filepath ) )
		{
			$proceed = TRUE;
		}
		else
		{
			$last_modification = filemtime( $filepath );
			if ( ( $last_modification + ( $this->assets->conf['auto_update_hours'] * 3600 ) ) < time() ) $proceed = TRUE;
		}

		// Stop if we say so
		if ( isset( $this->assets->conf['stop_exchange_auto_update'] ) ) $proceed = FALSE;

		if ( $proceed AND $this->assets->conf['auto_update_exchange'] == TRUE )
		{
			$file 		= @simplexml_load_file( $source );

			if ( ! empty( $file ) )
			{
				$json 		= json_decode( json_encode( $file ), TRUE );
				$store 		= array();
				$columns 	= array(
					'PYG' => 'GUARANÍ',
					'USD' => 'DÓLAR',
					'ARS' => 'PESO AR',
					'BRL' => 'REAL',
					'UYU' => 'PESO UY',
					'EUR' => 'EURO',
					'JPY' => 'YEN',
					'MXN' => 'PESO MX',
					'PEN' => 'SOLES PERUANOS',
					'BOB' => 'PESO BOLIVIANO',
					'COP' => 'PESO COLOMBIANO',
				);

				if ( isset( $json['moneda'] ) AND ! empty( $json['moneda'] ) )
				{
					// Fix for GUARANI only. Remove if retrieving from other source.
					array_push( $json['moneda'], array(
						'nombre' => 'GUARANÍ',
						'compra' => 1,
						'venta' => 1,
					) );

					foreach ( $json['moneda'] AS $row )
					{
						foreach ( $columns AS $key => $old_name )
						{
							if ( $old_name == $row['nombre'] )
							{
								if ( $key !== $this->assets->conf['currency'] )
								{
									$store[$key]['purchase'] 	= $row['compra'];
									$store[$key]['sale'] 		= $row['venta'];
								}
								else
								{
									$store[$key]['purchase'] 	= (string)"1";
									$store[$key]['sale'] 		= (string)"1";
								}
							}
						}
					}

					file_put_contents( $filepath, json_encode( $store ) );
				}				
			}
		}
	}

	/**
	 * Apply the content of the exchange file to a 'global' exchange array.
	 * 
	 * @return void
	 */
	public function apply_exchange()
	{
		$file = file_get_contents( FCPATH . 'uploads/exchange.json' );

		if ( ! empty( $file ) )
		{
			$file = json_decode( utf8_encode( $file ), TRUE );
			if ( is_array( $file ) ) $this->exchange = $file;
		}
	}

	/**
	 * Makes the exchange of a given value on a different currency than the system's one.
	 * 
	 * @param  float  $value    The value
	 * @param  string $currency The currency code
	 * @return float
	 */
	public function exchange_to_main_currency( $value, $currency = NULL )
	{
		if ( empty( $currency ) ) $currency = $this->assets->conf['currency'];

		$value = (float)$value;

		if ( $currency != $this->assets->conf['currency'] AND ! empty( $this->exchange ) )
		{
			if ( ! empty( $this->exchange[$currency]['sale'] ) ) $value = $value * (float)$this->exchange[$currency]['sale'];
		}

		return (float)$value;
	}
}
