<?php if ( ! defined( 'BASEPATH' ) ) exit( 'No direct script access allowed' );

class Templates
{
	/**
	 * The class constructor
	 *
	 * @access	public
	 * @return	none
	 */
	public function __construct(){}

	/**
	 * Enables the use of CI super-global without having to define an extra variable.
	 *
	 * I can't remember where I first saw this, so thank you if you are the original author. -Militis
	 *
	 * @access	public
	 * @param	$var
	 * @return	mixed
	 */
	public function __get( $var )
	{
		return get_instance()->$var;
	}

	/**
	 * View function, adding header and footer
	 * 
	 * @param  string
	 * @param  array
	 * @param  boolean
	 * @return mixed
	 */
	public function view( $view, $args = array(), $return = FALSE )
	{
		// Let's load main style here to allow overwrite of styles
		$this->assets->load_style( 'main' );

		$this->load->view( 'header', array(), $return );
		$this->load->view( $view, $args, $return );
		$this->load->view( 'footer', array(), $return );
	}

	/**
	 * Outputs the title for the current page, considering existent vars
	 * 
	 * @param  array $data
	 * @return output
	 */
	public function page_title( $data )
	{
		echo $this->assets->conf['short_product_name'];
		// if ( isset( $data['tags']['controller_name'] ) ) echo " " . $data['tags']['controller_name'];
		if ( isset( $data['tags']['page_title'] ) )
		{
			echo " - " . strip_tags( $data['tags']['page_title'] );
		}
		else
		{
			echo " - " . $this->assets->conf['company_name'];
		}
	}

	/**
	 * Develops the paginator
	 *
	 * @param	string
	 * @param	array
	 * @param	int
	 * @param	string
	 * @return	echo
	 */
	public function create_pagination( $table_or_results, $args = NULL, $uri_segment = 3, $extra_base = NULL )
	{
		$this->load->library( 'pagination' );

		if ( isset( $args['join'] ) AND is_array( $args['join'] ) )
			$this->db->join( $args['join']['table'], $args['join']['comparison'], $args['join']['direction'] );
		if ( isset( $args['where'] ) AND is_array( $args['where'] ) )
			$this->db->where( $args['where'] );
		if ( isset( $args['or_where'] ) AND is_array( $args['or_where'] ) )
			$this->db->or_where( $args['or_where'] );
		if ( isset( $args['where_in'] ) AND is_array( $args['where_in'] ) )
			foreach( $args['where_in'] AS $k => $v ) $this->db->where_in( $k, $v );
		if ( isset( $args['where_not_in'] ) AND is_array( $args['where_not_in'] ) )
			foreach( $args['where_not_in'] AS $k => $v ) $this->db->where_not_in( $k, $v );
		if ( isset( $args['like'] ) AND is_array( $args['like'] ) )
			$this->db->like( $args['like'] );
		if ( isset( $args['or_like'] ) AND is_array( $args['or_like'] ) )
			$this->db->or_like( $args['or_like'] );
		if ( isset( $args['order_by'] ) AND is_array( $args['order_by'] ) )
			foreach( $args['order_by'] AS $field => $direction )
				$this->db->order_by( $field, $direction );
		/*
		if ( isset( $args['limit'] ) )
			$this->db->limit( $args['limit'] );
		if ( isset( $args['limit'] ) AND isset( $args['offset'] ) )
			$this->db->limit( $args['limit'], $args['offset'] );
		*/
	
		$base = $this->uri->segment( 1 ) . '/';
		switch ( $this->uri->segment( 2 ) )
		{
			default:
			case 'lists': $base .= 'lists/'; break;
			case 'trash': $base .= 'trash/'; break;
			case 'search': $base .= 'search/'; break;
			case 'by_category': $base .= 'by_category/'; break;
			case 'inventory': $base .= 'inventory/'; break;
			// case 'by_client': $base .= 'by_client/'; break;
			// case 'past': $base .= 'past/'; break;
			// case 'billed': $base .= 'billed/'; break;
			// case 'not_billed': $base .= 'not_billed/'; break;
			// case 'lists_group': $base .= 'lists_group/'; break;
		}

		if ( $this->uri->segment( 1 ) == "log" ) $base = $this->uri->segment( 1 ) . "/";

		$this->pagination->initialize( array(
			'base_url'				=> site_url( $base . $extra_base ),
			'total_rows'			=> ( is_string( $table_or_results ) ) ? $this->db->count_all_results( $table_or_results ) : $table_or_results,
			'per_page'				=> $this->assets->conf['items_per_page'],
			'uri_segment'			=> $uri_segment,
			'num_links'				=> $this->assets->conf['pages_to_show'],
			'reuse_query_string'	=> true,

			'num_tag_open'		=> '<li class="page-item">',
			'num_tag_close'		=> '</li>',
			'cur_tag_open'		=> '<li class="page-item active"><span class="page-link">',
			'cur_tag_close'		=> '</span></li>',
			'prev_tag_open'		=> '<li class="page-item">',
			'prev_tag_close'	=> '</li>',
			'next_tag_open'		=> '<li class="page-item">',
			'next_tag_close'	=> '</li>',
			'first_tag_open'	=> '<li class="page-item">',
			'first_tag_close'	=> '</li>',
			'last_tag_open'		=> '<li class="page-item">',
			'last_tag_close'	=> '</li>',
			'full_tag_open'		=> '<ul class="pagination justify-content-end">',
			'full_tag_close'	=> '</ul>',

			'attributes'		=> array( 'class' => "page-link" ),

			// 'first_link'		=> __( 'First' ),
			// 'last_link'		=> __( 'Last' ),

			'next_link'			=> '»',
			'prev_link'			=> '«',
		) );

		return $this->pagination->create_links();
/*
		if ( ! empty( $_SERVER['QUERY_STRING'] ) )
		{
			$pages = $this->pagination->create_links();
			$this->load->library( 'html_dom' );
			$this->html_dom->loadHTML( $pages );
			foreach( $this->html_dom->find( 'a' ) AS $p )
			{
				if ( $p->href != "" )
				{
					$p->href = $p->href . "?" . $_SERVER['QUERY_STRING'];
				}
			}
			$pages = $this->html_dom->save();
			return $pages;
		}
		else
		{
			return $this->pagination->create_links();
		}
*/
	}

	/**
	 * Reverses the current list one page behind when the last element of the given
	 * page have dissappeared because any action. Also makes the alert persistant
	 * in case there's any to show later of the redirection.
	 * 
	 * @param  mixed  $rows  The rows of the query
	 * @return void
	 */
	public function reverse_pagination( $rows )
	{
		$current_offset = (int)$this->uri->segment( 3 );

		if ( empty( $rows ) AND $current_offset > 0 )
		{
			$uri_root = $this->uri->segment( 1 ) . '/' . $this->uri->segment( 2 );
			$new_page = ( $current_offset - (int)$this->assets->conf['items_per_page'] );
			$new_page = ( ! empty( $new_page ) AND $new_page > 0 ) ? '/' . $new_page : '';

			if ( ! empty( $this->alerts->elements['message'] ) )
			{
				$this->alerts->persist( TRUE )->alert(
					$this->alerts->elements['message'],
					$this->alerts->elements['title'],
					$this->alerts->elements['errors'],
					$this->alerts->elements['class'],
					$this->alerts->elements['dismissible']
				);
			}

			redirect( $uri_root . $new_page );
		}
	}

}