<?php  if ( ! defined( 'BASEPATH' ) ) exit( 'No direct script access allowed' );
/**
* Name:  Ion Auth
*
* Author: Ben Edmunds
*		  ben.edmunds@gmail.com
*         @benedmunds
*
* Added Awesomeness: Phil Sturgeon
*
* Location: http://github.com/benedmunds/CodeIgniter-Ion-Auth
*
* Created:  10.01.2009
*
* Description:  Modified auth system based on redux_auth with extensive customization.  This is basically what Redux Auth 2 should be.
* Original Author name has been kept but that does not mean that the method has not been modified.
*
* Requirements: PHP5 or above
*
*/

class Ion_auth
{
	/**
	 * account status ( 'not_activated', etc ... )
	 *
	 * @var string
	 **/
	protected $status;

	/**
	 * extra where
	 *
	 * @var array
	 **/
	public $_extra_where = array();

	/**
	 * extra set
	 *
	 * @var array
	 **/
	public $_extra_set = array();

	/**
	 * caching of users and their groups
	 *
	 * @var array
	 **/
	public $_cache_user_in_group;

	/**
	 * __construct
	 *
	 * @author Ben
	 */
	public function __construct()
	{
		$this->load->config( 'ion_auth', TRUE );
		$this->load->library( array( 'email' ) );
		$this->load->helper( array( 'cookie', /*'language',*/ 'url' ) );

		$this->load->library( 'session' );

		$this->load->model( 'ion_auth_model' );

		$this->_cache_user_in_group =& $this->ion_auth_model->_cache_user_in_group;

		//auto-login the user if they are remembered
		if ( ! $this->logged_in() AND get_cookie( $this->config->item( 'identity_cookie_name', 'ion_auth' ) ) AND get_cookie( $this->config->item( 'remember_cookie_name', 'ion_auth' ) ) )
		{
			$this->ion_auth_model->login_remembered_user();
		}

		$email_config = $this->config->item( 'email_config', 'ion_auth' );

		if ( $this->config->item( 'use_ci_email', 'ion_auth' ) AND isset( $email_config ) AND is_array( $email_config ) )
		{
			$this->email->initialize( $email_config );
		}

		$this->ion_auth_model->trigger_events( 'library_constructor' );
	}

	/**
	 * __call
	 *
	 * Acts as a simple way to call model methods without loads of stupid alias'
	 *
	 * @param $method
	 * @param $arguments
	 * @return mixed
	 * @throws Exception
	 */
	public function __call( $method, $arguments )
	{
		if ( ! method_exists(  $this->ion_auth_model, $method )  )
		{
			throw new Exception( 'Undefined method Ion_auth::' . $method . '() called' );
		}
		if ( $method == 'create_user' )
		{
			return call_user_func_array( array( $this, 'register' ), $arguments );
		}
		if ( $method == 'update_user' )
		{
			return call_user_func_array( array( $this, 'update' ), $arguments );
		}
		return call_user_func_array( array( $this->ion_auth_model, $method ), $arguments );
	}

	/**
	 * __get
	 *
	 * Enables the use of CI super-global without having to define an extra variable.
	 *
	 * I can't remember where I first saw this, so thank you if you are the original author. -Militis
	 *
	 * @access	public
	 * @param	$var
	 * @return	mixed
	 */
	public function __get( $var )
	{
		return get_instance()->$var;
	}


	/**
	 * forgotten password feature
	 *
	 * @param $identity
	 * @return mixed boolean / array
	 * @author Mathew
	 */
	public function forgotten_password( $identity ) //changed $email to $identity
	{
		if ( $this->ion_auth_model->forgotten_password( $identity ) ) //changed
		{
			// Get user information
			$identifier = $this->ion_auth_model->identity_column; // use model identity column, so it can be overridden in a controller
			$user = $this->where( $identifier, $identity )->where( 'active', 1 )->users()->row();  // changed to get_user_by_identity from email

			if ( $user )
			{
				$data = array( 
					'identity'					=> $user->{$this->config->item( 'identity', 'ion_auth' )},
					'forgotten_password_code'	=> $user->forgotten_password_code
				);

				if ( ! $this->config->item( 'use_ci_email', 'ion_auth' ) )
				{
					$this->set_message( __( "E-mail sent. Check it to restore your password." ) );
					return $data;
				}
				else
				{
					$message = $this->load->view( $this->config->item( 'email_templates', 'ion_auth' ) . $this->config->item( 'email_forgot_password', 'ion_auth' ), $data, true );
					$this->email->clear();
					$this->email->from( $this->config->item( 'admin_email', 'ion_auth' ), $this->assets->conf['product_name'] );
					$this->email->to( $user->email );
					$this->email->subject( $this->assets->conf['company_name'] . ' - ' . __( "Forgotten Password Verification" ) );
					$this->email->message( $message );

					if ( $this->email->send() )
					{
						$this->set_message( __( "E-mail sent. Check it to restore your password." ) );
						return TRUE;
					}
					else
					{
						$this->set_error( __( "Unable to reset password. Please, try again." ) );
						return FALSE;
					}
				}
			}
			else
			{
				$this->set_error( __( "Unable to reset password. Please, try again." ) );
				return FALSE;
			}
		}
		else
		{
			$this->set_error( __( "Unable to reset password. Please, try again." ) );
			return FALSE;
		}
	}

	/**
	 * forgotten_password_complete
	 *
	 * @param $code
	 * @author Mathew
	 * @return bool
	 */
	public function forgotten_password_complete( $code )
	{
		$this->ion_auth_model->trigger_events( 'pre_password_change' );

		$profile  = $this->where( 'forgotten_password_code', $code )->users()->row(); //pass the code to profile

		if ( ! $profile )
		{
			$this->ion_auth_model->trigger_events( array( 'post_password_change', 'password_change_unsuccessful' ) );
			$this->set_error( __( "Unable to change the password." ) );
			return FALSE;
		}

		$new_password = $this->ion_auth_model->forgotten_password_complete( $code, $profile->salt );

		if ( $new_password )
		{
			$data = array( 
				'identity'		=> $profile->{$this->config->item( 'identity', 'ion_auth' )},
				'new_password'	=> $new_password,
			);

			if ( ! $this->config->item( 'use_ci_email', 'ion_auth' ) )
			{
				$this->set_message( __( "Password change successful." ) );
			}
			else
			{
				$message = $this->load->view( $this->config->item( 'email_templates', 'ion_auth' ) . $this->config->item( 'email_forgot_password_complete', 'ion_auth' ), $data, true );

				$this->email->clear();
				$this->email->from( $this->config->item( 'admin_email', 'ion_auth' ), $this->assets->conf['product_name'] );
				$this->email->to( $profile->email );
				$this->email->subject( $this->assets->conf['company_name'] . ' - ' . __( "Forgotten Password Verification" ) );
				$this->email->message( $message );

				if ( $this->email->send() )
				{
					$this->set_message( __( "Password change successful. We sent you a confirmation e-mail." ) );
				}
				else
				{
					$this->set_error( __( "Password changed, but we could not send you the confirmation e-mail." ) );
				}
			}

			$this->ion_auth_model->trigger_events( array( 'post_password_change', 'password_change_successful' ) );
			return $data;
		}
		else
		{
			$this->set_error( __( "Unable to change the password." ) );
		}

		$this->ion_auth_model->trigger_events( array( 'post_password_change', 'password_change_unsuccessful' ) );
		return FALSE;
	}

	/**
	 * forgotten_password_complete_comunicate
	 *
	 * @param $email
	 * @param $identity
	 * @param $new_password
	 * @author Emilio
	 * @return bool
	 */
	public function forgotten_password_complete_comunicate( $email, $identity, $new_password )
	{
		$data = array( 
			'identity'		=> $identity,
			'new_password'	=> $new_password,
		);

		if ( $new_password AND $this->config->item( 'use_ci_email', 'ion_auth' ) )
		{
			$message = $this->load->view( $this->config->item( 'email_templates', 'ion_auth' ) . $this->config->item( 'email_forgot_password_complete', 'ion_auth' ), $data, true );

			$this->email->clear();
			$this->email->from( $this->config->item( 'admin_email', 'ion_auth' ), $this->assets->conf['product_name'] );
			$this->email->to( $email );
			$this->email->subject( $this->assets->conf['company_name'] . ' - ' . __( "New Password" ) );
			$this->email->message( $message );

			if ( $this->email->send() ) return TRUE;
		}

		return FALSE;
	}

	/**
	 * forgotten_password_check
	 *
	 * @param $code
	 * @author Michael
	 * @return bool
	 */
	public function forgotten_password_check( $code )
	{
		$profile = $this->where( 'forgotten_password_code', $code )->users()->row(); //pass the code to profile

		if ( ! is_object( $profile ) )
		{
			$this->set_error( __( "Unable to change the password." ) );
			return FALSE;
		}
		else
		{
			if ( $this->config->item( 'forgot_password_expiration', 'ion_auth' ) > 0 )
			{
				//Make sure it isn't expired
				$expiration = $this->config->item( 'forgot_password_expiration', 'ion_auth' );
				if ( time() - $profile->forgotten_password_time > $expiration )
				{
					//it has expired
					$this->clear_forgotten_password_code( $code );
					$this->set_error( __( "Unable to change the password." ) );
					return FALSE;
				}
			}
			return $profile;
		}
	}

	/**
	 * register
	 *
	 * @param $identity
	 * @param $password
	 * @param $email
	 * @param array $additional_data
	 * @param array $group_ids
	 * @author Mathew
	 * @return bool
	 */
	public function register( $identity, $password, $email, $additional_data = array(), $group_ids = array() ) //need to test email activation
	{
		$this->ion_auth_model->trigger_events( 'pre_account_creation' );

		$email_activation = $this->config->item( 'email_activation', 'ion_auth' );

		$id = $this->ion_auth_model->register( $identity, $password, $email, $additional_data, $group_ids );

		if ( ! $email_activation )
		{
			if ( $id !== FALSE )
			{
				$this->set_message( __( "Account successfully created" ) );
				$this->ion_auth_model->trigger_events( array( 'post_account_creation', 'post_account_creation_successful' ) );
				return $id;
			}
			else
			{
				$this->set_error( __( "Unable to create account" ) );
				$this->ion_auth_model->trigger_events( array( 'post_account_creation', 'post_account_creation_unsuccessful' ) );
				return FALSE;
			}
		}
		else
		{
			if ( ! $id )
			{
				$this->set_error( __( "Unable to create account" ) );
				return FALSE;
			}

			// deactivate so the user much follow the activation flow
			$deactivate = $this->ion_auth_model->deactivate( $id );

			// the deactivate method call adds a message, here we need to clear that
			$this->ion_auth_model->clear_messages();

			if ( ! $deactivate )
			{
				$this->set_error( __( "Unable to deactivate account" ) );
				$this->ion_auth_model->trigger_events( array( 'post_account_creation', 'post_account_creation_unsuccessful' ) );
				return FALSE;
			}

			$activation_code	= $this->ion_auth_model->activation_code;
			$identity			= $this->config->item( 'identity', 'ion_auth' );
			$user				= $this->ion_auth_model->user( $id )->row();

			$data = array( 
				'identity'		=> $user->{$identity},
				'id'			=> $user->id,
				'email'			=> $email,
				'activation'	=> $activation_code,
			);

			if ( ! $this->config->item( 'use_ci_email', 'ion_auth' ) )
			{
				$this->ion_auth_model->trigger_events( array( 'post_account_creation', 'post_account_creation_successful', 'activation_email_successful' ) );
				$this->set_message( __( "Activation e-mail not enabled. Please, send user credentials manually." ) );
				return $data;
			}
			else
			{
				$message = $this->load->view( $this->config->item( 'email_templates', 'ion_auth' ) . $this->config->item( 'email_activate', 'ion_auth' ), $data, true );

				$this->email->clear();
				$this->email->from( $this->config->item( 'admin_email', 'ion_auth' ), $this->assets->conf['product_name'] );
				$this->email->to( $email );
				$this->email->subject( $this->assets->conf['company_name'] . ' - ' . __( "Account Activation" ) );
				$this->email->message( $message );

				if ( $this->email->send() == TRUE )
				{
					$this->ion_auth_model->trigger_events( array( 'post_account_creation', 'post_account_creation_successful', 'activation_email_successful' ) );
					$this->set_message( __( "Activation e-mail sent. Please check your inbox or spam." ) );
					return $id;
				}
			}

			$this->ion_auth_model->trigger_events( array( 'post_account_creation', 'post_account_creation_unsuccessful', 'activation_email_unsuccessful' ) );
			$this->set_error( __( "Unable to send activation e-mail" ) );
			return FALSE;
		}
	}

	/**
	 * logout
	 *
	 * @return void
	 * @author Mathew
	 **/
	public function logout()
	{
		$this->ion_auth_model->trigger_events( 'logout' );

		$identity = $this->config->item( 'identity', 'ion_auth' );

		if ( substr( CI_VERSION, 0, 1 ) == '2' )
		{
			$this->session->unset_userdata( array( $identity => '', 'id' => '', 'user_id' => '' )  );
		}
		else
		{
			$this->session->unset_userdata( array( $identity, 'id', 'user_id' )  );
		}

		// delete the remember me cookies if they exist
		if ( get_cookie( $this->config->item( 'identity_cookie_name', 'ion_auth' ) ) )
		{
			delete_cookie( $this->config->item( 'identity_cookie_name', 'ion_auth' ) );
		}
		if ( get_cookie( $this->config->item( 'remember_cookie_name', 'ion_auth' ) ) )
		{
			delete_cookie( $this->config->item( 'remember_cookie_name', 'ion_auth' ) );
		}

		// Destroy the session
		$this->session->sess_destroy();

		//Recreate the session
		if ( substr( CI_VERSION, 0, 1 ) == '2' )
		{
			$this->session->sess_create();
		}
		else
		{
			if ( version_compare( PHP_VERSION, '7.0.0' ) >= 0 )
			{
				session_start();
			}
			$this->session->sess_regenerate( TRUE );
		}

		$this->set_message( __( "Logged out successfully" ) );
		return TRUE;
	}

	/**
	 * logged_in
	 *
	 * @return bool
	 * @author Mathew
	 **/
	public function logged_in()
	{
		$this->ion_auth_model->trigger_events( 'logged_in' );

		return ( bool ) $this->session->userdata( 'identity' );
	}

	/**
	 * logged_in
	 *
	 * @return integer
	 * @author jrmadsen67
	 **/
	public function get_user_id()
	{
		$user_id = $this->session->userdata( 'user_id' );

		if ( ! empty( $user_id ) )
		{
			return $user_id;
		}

		return null;
	}

	/**
	 * is_admin
	 *
	 * @return bool
	 * @author Ben Edmunds
	 **/
	public function is_admin( $id = false )
	{
		$this->ion_auth_model->trigger_events( 'is_admin' );

		$superadmin_group = $this->config->item( 'superadmin_group', 'ion_auth' );
		$admin_group = $this->config->item( 'admin_group', 'ion_auth' );

		return $this->in_group( array( $superadmin_group, $admin_group ), $id );
	}

	/**
	 * in_group
	 *
	 * @param mixed group( s ) to check
	 * @param bool user id
	 * @param bool check if all groups is present, or any of the groups
	 *
	 * @return bool
	 * @author Phil Sturgeon
	 **/
	public function in_group( $check_group, $id = false, $check_all = false )
	{
		$this->ion_auth_model->trigger_events( 'in_group' );

		$id OR $id = $this->session->userdata( 'user_id' );

		if ( ! is_array( $check_group ) )
		{
			$check_group = array( $check_group );
		}

		if ( isset( $this->_cache_user_in_group[$id] ) )
		{
			$groups_array = $this->_cache_user_in_group[$id];
		}
		else
		{
			$users_groups = $this->ion_auth_model->get_users_groups( $id )->result();
			$groups_array = array();

			foreach ( $users_groups AS $group )
			{
				$groups_array[$group->id] = $group->name;
			}
			$this->_cache_user_in_group[$id] = $groups_array;
		}
		
		foreach ( $check_group AS $key => $value )
		{
			$groups = ( is_string( $value ) ) ? $groups_array : array_keys( $groups_array );

			/**
			 * if !all ( default ), in_array
			 * if all, !in_array
			 */
			if ( in_array( $value, $groups ) XOR $check_all )
			{
				/**
				 * if !all ( default ), true
				 * if all, false
				 */
				return ! $check_all;
			}
		}

		/**
		 * if !all ( default ), false
		 * if all, true
		 */
		return $check_all;
	}

	/**
	 * ------------------------------------------------------------------------------------------------------
	 * OWN EXTENSION
	 * ------------------------------------------------------------------------------------------------------
	 */

	/**
	 * Checks if current user is superadmin
	 *
	 * @return bool
	 * @author José Zaracho
	 **/
	public function is_superadmin( $id = false )
	{
		$this->ion_auth_model->trigger_events( 'is_admin' );

		$superadmin_group = $this->config->item( 'superadmin_group', 'ion_auth' );

		return $this->in_group( $superadmin_group, $id );
	}

	
	/**
	 * Returns any formatted user's name depending on the return format, and the user id.
	 * If no user is given, takes current logged in user.
	 * 
	 * @param  string $return  The format
	 * @param  int    $user_e  The User ID or Array
	 * @return string/boolean
	 * @author José Zaracho
	 */
	public function user_name( $return = "fullname", $user_e = NULL )
	{
		if ( empty( $user_e ) )
		{
			$user = $this->ion_auth->user()->row();
		}
		else if ( is_array( $user_e ) )
		{
			$user = json_decode( json_encode( $user_e ) );
		}
		else if ( is_numeric( $user_e ) )
		{
			$user = $this->ion_auth->user( $user_e )->row();
		}

		if ( ! empty( $user ) AND $this->ion_auth->logged_in() )
		{
			// Reset last name, in case there's no any
			// (Actually, just a workaround for the Ombú name on the list)
			if ( $user->last_name == trim( $this->assets->conf['no_data'] ) ) $user->last_name = "";

			switch ( $return )
			{
				default:
				case 'fullname':
					return trim( $user->first_name . ' ' . $user->last_name );
					break;
				case 'firstname':
					return trim( $user->first_name );
					break;
				case 'lastname':
					return trim( ( ! empty( $user->last_name ) ) ? $user->last_name : $user->first_name );
					break;
				case 'lastname_initial':
					if ( ! empty( $user->last_name ) )
					{
						return trim( substr( $user->first_name, 0, 1 ) . ". " . $user->last_name );
					}
					else
					{
						return trim( $user->first_name );
					}
					break;
				case 'firstname_initial':
					$name = $user->first_name;
					if ( ! empty( $user->last_name ) )
					{
						$name .= ' ' . substr( $user->last_name, 0, 1 ) . '.';
					}
					return trim( $name );
					break;
				case 'lastname_start':
					if ( ! empty( $user->last_name ) )
					{
						return trim( mb_strtoupper( $user->last_name, "UTF-8" ) . ", " . $user->first_name );
					}
					else
					{
						return trim( $user->first_name );
					}
					break;
				case 'username':
					return $user->username;
					break;
			}
		}

		return false;
	}

	/**
	 * Returns current user role, or roles
	 * 
	 * @return string/boolean
	 * @author José Zaracho
	 */
	public function current_user_role( $user_id = NULL )
	{
		if ( $this->ion_auth->logged_in() )
		{
			if ( empty( $user_id ) ) $user_id = $this->session->userdata( 'user_id' );
			$users_groups = $this->ion_auth_model->get_users_groups( $user_id )->result();
			$groups_array = array();

			foreach ( $users_groups AS $group )
			{
				$groups_array[$group->id] = $group->description;
			}

			if ( ! empty( $groups_array ) )
			{
				return implode( ", ", $groups_array );
			}
		}

		return false;
	}

	/**
	 * Returns current user image
	 * 
	 * @param  string $size    Size... can be 'normal' and 'thumb'
	 * @param  int    $user_id User ID
	 * @return string
	 * @author José Zaracho
	 */
	public function avatar( $size = 'thumb', $user_id = NULL )
	{
		$this->load->model( "Files_model", "files" );

		if ( $this->ion_auth->logged_in() )
		{
			$files = $this->files->get( array(
				'where' => array(
					'registry_id' => ( ! empty( $user_id ) ) ? $user_id : $this->ion_auth->user()->row()->id,
					'controller' => 'users',
				)
			) );

			if ( ! empty( $files ) )
			{
				$file = $files[0];

				switch ( $size )
				{
					default: case 'thumb': 
						$filename = ltrim( $file['relative_location'], "/" ) . $file['name'] . "_thumb" . $file['extension'];
						break;

					case 'normal': 
						$filename = ltrim( $file['relative_location'], "/" ) . $file['name_ext'];
						break;
				}

				if ( is_file( FCPATH . $filename ) ) return site_url( $filename );
			}
		}

		return site_url( 'assets/img/default-avatar.png' );
	}

	/**
	 * Checks if the current user have a higher role than the one they're 
	 * trying to perform the action within
	 * 
	 * @param  int $comparison_user_id User comparison id
	 * @return boolean
	 * @author José Zaracho
	 */
	public function is_superior( int $comparison_user_id )
	{
		$is_superior = FALSE;
		$current_user_id = (int)$this->session->userdata( 'user_id' );

		if ( $current_user_id == $comparison_user_id ) return $is_superior;

		$current_user_groups = $this->ion_auth_model->get_users_groups( $current_user_id )->result_array();
		$comparison_user_groups = $this->ion_auth_model->get_users_groups( $comparison_user_id )->result_array();

		if ( ! empty( $current_user_groups ) AND ! empty( $comparison_user_groups ) )
		{
			foreach ( $current_user_groups AS $g1 )
			{
				foreach ( $comparison_user_groups AS $g2 )
				{
					if ( $g1['id'] > $g2['id'] )
					{
						$is_superior = TRUE;
					}
				}
			}
		}

		return $is_superior;
	}

	/**
	 * Sends user credentials to the user via email
	 * @param  int   $user_id The user ID
	 * @param  array $data    The additional data, in case we need to send password
	 * @return boolean
	 * @author José Zaracho
	 */
	public function send_credentials( $user_id, array $data = NULL )
	{
		$user = $this->ion_auth_model->user( $user_id )->row();

		if ( ! empty( $user ) )
		{
			$data['username'] = $data['username'];
			$data['password'] = ( ! empty( $data['password'] ) ) ? $data['password'] : __( "The very same you already had" );

			$message = $this->load->view( $this->config->item( 'email_templates', 'ion_auth' ) . $this->config->item( 'email_new_credentials', 'ion_auth' ), $data, true );

			$this->email->clear();
			$this->email->from( $this->config->item( 'admin_email', 'ion_auth' ), $this->assets->conf['product_name'] );
			$this->email->to( $user->email );
			$this->email->subject( $this->assets->conf['company_name'] . ' - ' . __( "Account Credentials" ) );
			$this->email->message( $message );

			if ( $this->email->send() == TRUE )
			{
				return true;
			}
		}

		return false;
	}

	/**
	 * Returns an array to build a dropdown of users
	 * 
	 * @param  string $format The display format for the name
	 * @return array
	 */
	public function build_dropdown( $format = 'lastname_start' )
	{
		$this->db->order_by( 'last_name', 'ASC' );
		$auth_tables = $this->config->item( 'tables', 'ion_auth' );

		$return = array( '' => '&nbsp;' );

		if ( $query = $this->db->get( $auth_tables['users'] ) AND $query->num_rows() > 0 )
		{
			foreach ( $query->result() AS $u )
			{
				$return[$u->id] = $this->user_name( $format, $u->id );
			}
		}

		return $return;
	}
}
