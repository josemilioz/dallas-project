<?php if ( ! defined( 'BASEPATH' ) ) exit( 'No direct script access allowed' );

class Permissions
{
	/**
	 * The class constructor
	 *
	 * @access	public
	 * @return	none
	 */
	public function __construct()
	{
		$this->ion_auth->set_hook( 'logged_in', 'logout_inactive', $this, 'logout_inactive', array( 'identity' => $this->session->userdata( 'identity' ) ) );
	}

	/**
	 * Enables the use of CI super-global without having to define an extra variable.
	 *
	 * I can't remember where I first saw this, so thank you if you are the original author. -Militis
	 *
	 * @access	public
	 * @param	$var
	 * @return	mixed
	 */
	public function __get( $var )
	{
		return get_instance()->$var;
	}

	/**
	 * Blocks the non-admins to certain places in the system
	 * 
	 * @return none
	 */
	public function block_not_admin()
	{
		if ( ! $this->ion_auth->is_admin() )
		{
			$this->alerts->persist( TRUE )->alert( $this->assets->conf['no_permission']['message'], $this->assets->conf['no_permission']['title'], NULL, 'warning' );
			redirect( '/' );
		}
	}

	/**
	 * Blocks the non-superadmins to certain places in the system
	 * 
	 * @return none
	 */
	public function block_not_superadmin()
	{
		if ( ! $this->ion_auth->in_superadmin() )
		{
			$this->alerts->persist( TRUE )->alert( $this->assets->conf['no_permission']['message'], $this->assets->conf['no_permission']['title'], NULL, 'warning' );
			redirect( '/' );
		}
	}

	/**
	 * Boolean to check if can trash or not
	 * 
	 * @return boolean
	 */
	public function can_trash()
	{
		if ( $this->ion_auth->is_admin() ) return true;
		return false;
	}

	/**
	 * Boolean to check if can delete or not
	 * 
	 * @return boolean
	 */
	public function can_delete()
	{
		if ( $this->ion_auth->is_admin() ) return true;
		return false;
	}

	/**
	 * Boolean to check if can restore or not
	 * 
	 * @return boolean
	 */
	public function can_restore()
	{
		if ( $this->ion_auth->is_admin() ) return true;
		return false;
	}

	/**
	 * Boolean to check if can edit or not
	 * 
	 * @return boolean
	 */
	public function can_edit( $args = FALSE )
	{
/*		
		if ( $args )
		{
			switch ( $string = uri_string() )
			{
				// SALESMAN :: CLIENTS
				case ( preg_match( '/(clients\/edit(\/[0-9]*)*)/', $string ) ? true : false ) :
					if ( $this->ion_auth->in_group( array( 'funcionario', 'vendedor' ) ) )
					{
						if ( isset( $args['client_id'] ) )
						{
							$this->load->model( 'Clients_model', 'clients' );
							$client = $this->clients->get( $args['client_id'] );
							if ( $this->ion_auth->user()->row()->id == $client['creator'] ) return true;
						}

						return false;
					}
				break;
			}
		}
*/
		if ( $this->ion_auth->is_admin() ) return true;

		return false;
	}

	/**
	 * Boolean to check if can add or not
	 * 
	 * @return boolean
	 */
	public function can_add()
	{
		if ( $this->ion_auth->in_group( array( 'superadmin', 'admin', 'manager', 'member' ) ) ) return true;
		return false;
	}

	/**
	 * Boolean to check if can see or not
	 * 
	 * @return boolean
	 */
	public function can_see()
	{
/*
		switch ( $string = uri_string() )
		{
			// Salesmen can only see their own stuff
			case ( preg_match( '/(clients\/lists(\/[0-9]*)*)/', $string ) ? true : false ) :
			case ( preg_match( '/(clients\/brief(\/[0-9]*)*)/', $string ) ? true : false ) :
			case ( preg_match( '/(clients\/search(\/[0-9]*)*)/', $string ) ? true : false ) :
			case ( preg_match( '/(stock\/inventory(\/[0-9]*)*)/', $string ) ? true : false ) :
			case ( preg_match( '/(sales\/billed(\/[0-9]*)*)/', $string ) ? true : false ) :
			case ( preg_match( '/(sales\/lists(\/[0-9]*)*)/', $string ) ? true : false ) :
			case ( preg_match( '/(sales\/search(\/[0-9]*)*)/', $string ) ? true : false ) :
			case ( preg_match( '/(movements\/lists(\/[0-9]*)*)/', $string ) ? true : false ) :
			case ( preg_match( '/(movements\/search(\/[0-9]*)*)/', $string ) ? true : false ) :
				if ( $this->ion_auth->in_group( array( 'manager', 'member' ) ) ) return false;
			break;
		}
*/
		if ( $this->ion_auth->in_group( array( 'superadmin', 'admin', 'manager', 'member' ) ) ) return true;

		return false;
	}

	/**
	 * Boolean to check if can add or not
	 * 
	 * @return boolean
	 */
	public function can_print()
	{
		if ( $this->ion_auth->in_group( array( 'superadmin', 'admin', 'manager', 'member' ) ) ) return true;
		return false;
	}

	/**
	 * Logs out inactive users, ocuring as a hook so check when the user is logged in and make it logout
	 * automatically if the blocking occurs in the middle of its session.
	 * 
	 * @param  mixed $args
	 * @return void
	 */
	public function logout_inactive( $args )
	{
		if ( $this->session->userdata( 'user_id' ) )
		{
			if ( $this->ion_auth->user( $this->session->userdata( 'user_id' ) )->row()->active == 0 )
			{
				$this->ion_auth->logout();
			}
		}
	}

	/**
	 * Screen showed to users other than superadmins when the system is locked.
	 * Triggered by a hook.
	 * 
	 * @return void
	 */
	public function system_locked_screen()
	{
		switch ( $string = uri_string() )
		{
			case 'login' :
			case 'logout' :
			case 'change_password' :
			case 'forgot_password' :
			case ( preg_match( '/(reset_password\/.*)/', $string ) ? true : false ) :
				$dont = true;
				break;
		}

		if ( empty( $string ) ) unset( $dont );

		if ( is_file( FCPATH . '.locked' ) AND $this->ion_auth->logged_in() AND ! $this->ion_auth->is_superadmin() AND ! isset( $dont ) )
		{
			$this->assets->load_style( 'login' );

			$viewdata['tags']['page_title'] = __( "System Locked" );

			require_once( VIEWPATH . 'auth/header.php' );
			require_once( VIEWPATH . 'singles/system-locked.php' );
			require_once( VIEWPATH . 'auth/footer.php' );

			exit; die;
		}
	}

	/**
	 * Checks whether system is locked or not
	 * 
	 * @return boolean
	 */
	public function is_system_locked()
	{
		if ( is_file( FCPATH . '.locked' ) )
		{
			return true;
		}

		return false;
	}
}
