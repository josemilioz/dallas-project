<?php if ( ! defined( 'BASEPATH' ) ) exit( 'No direct script access allowed' );

class Assets
{
	protected $css	= array();
	protected $js	= array();
	public $conf	= array();

	/**
	 * The class constructor
	 *
	 * @access	public
	 * @return	none
	 */
	public function __construct()
	{
		$this->load_settings();
		// $this->load_language();
		// $this->translate_settings();

		// Main styles
		// Called in the templates library, under 'view' function

		// Main Scripts
		$this->load_script( base_url( 'assets/bower_components/jquery/dist/jquery.min.js' ) );
		$this->load_script( base_url( 'assets/bower_components/bootstrap/dist/js/bootstrap.bundle.min.js' ) );
		$this->load_script( 'main.min.js' );

		// Font Awesome
		// $this->load_style( base_url( 'assets/bower_components/fontawesome/css/fontawesome.min.css' ) );
		$this->load_style( base_url( 'assets/bower_components/fontawesome/css/all.min.css' ) );
	}

	/**
	 * Enables the use of CI super-global without having to define an extra variable.
	 *
	 * I can't remember where I first saw this, so thank you if you are the original author. -Militis
	 *
	 * @access	public
	 * @param	$var
	 * @return	mixed
	 */
	public function __get( $var )
	{
		return get_instance()->$var;
	}

	/**
	 * Loads the settings from the database
	 * 
	 * @param  none
	 * @return void
	 */
	public function load_settings()
	{
		$this->config->load( 'system' );
		$this->conf = $this->config->item( 'system' );

		// Default system main settings
		$db_settings = $this->db->get( $this->conf['tables']['settings'] )->result_array();

		if ( ! empty( $db_settings ) )
		{
			foreach ( $db_settings AS $s )
			{
				if ( ! empty( $s['value'] ) )
				{
					if ( strpos( ' ' . $s['value'] . ' ', '}' ) == TRUE )
					{
						$this->conf[$s['key']] = json_decode( $s['value'], TRUE );
					}
					else
					{
						$this->conf[$s['key']] = $s['value'];
					}
				}
				else
				{
					$this->conf[$s['key']] = 0;
				}
			}
		}

		// Let's override with custom user settings, in case user is logged in
		if ( $this->ion_auth->logged_in() )
		{
			// Lang override
			$this->conf['language'] = $this->ion_auth->user()->row()->language;
		}

	}

	/**
	 * Re-iterates the settings applying translations when needing it.
	 * Works directly on $this->conf already loaded in the class.
	 * 
	 * @param  none
	 * @return void
	 */
	public function translate_settings()
	{
		// $stack = $this->conf;
		$result = array();

		foreach ( $this->conf AS $key => $value )
		{
			if ( is_array( $value ) )
			{
				foreach ( $value AS $sub_key => $node )
				{
					if ( ! empty( $node ) AND is_string( $node ) ) $result[$key][$sub_key] = __( $node );
				}
			}
			else
			{
				if ( ! empty( $value ) AND is_string( $value ) ) $result[$key] = __( $value );
			}

			// Special wrapped items that don't get translated that easy:
			// No data
			
			if ( $key == 'no_data' )
			{
				$result[$key] = sprintf( $this->conf['no_data_wrapper'], __( $this->conf['no_data_string'] ) );
			}

			// Currencies long name
			if ( $key == 'currencies_ext' AND is_array( $value ) )
			{
				foreach ( $value AS $sub_key => $node )
				{
					if ( ! empty( $sub_key ) AND ! empty( $node ) AND is_string( $node ) ) $result[$key][$sub_key] = sprintf( '%s (%s)', $this->conf['currencies'][$sub_key], __( $this->conf['currencies_names'][$sub_key] ) );
				}
			}
		}

		$this->conf = array_merge( $this->conf, $result );
	}

	/**
	 * Loads the language PO files
	 * 
	 * @return void
	 */
	public function load_language()
	{
		$lang = ( isset( $this->conf['language'] ) ) ? $this->conf['language'] : $this->config->item( 'system' )['language'];

		if ( ! empty( $lang ) )
		{
			$path = FCPATH . "locale";

			putenv( 'LC_MESSAGES=' . $lang );
			setlocale( LC_MESSAGES, $lang );
			putenv( 'LC_TIME=' . $lang );
			setlocale( LC_TIME, $lang );

			bindtextdomain( $lang, $path );
			textdomain( $lang );
			bind_textdomain_codeset( $lang, 'UTF-8' );
		}

		switch ( $lang )
		{
			case 'es_ES': $this->config->set_item( 'language', 'spanish' ); break;
			case 'en_US': $this->config->set_item( 'language', 'english' ); break;
		}
	}

	/**
	 * Stylesheets placement on the main template
	 * 
	 * @param none
	 * @return output
	 */
	public function print_styles()
	{
		if ( ! empty( $this->css ) )
		{
			foreach ( $this->css AS $media => $files )
			{
				switch ( $media )
				{
					default: $media = ""; break;
					case 'screen': $media = 'media="screen" '; break;
					case 'print': $media = 'media="print" '; break;
				}

				foreach ( $files AS $k => $v )
				{
					$uri_file = ( strrpos( $v, "//" ) === FALSE ) ? base_url( "assets/css/" . $v ) : $v;
					?><link rel="stylesheet" href="<?php echo $uri_file; ?>" type="text/css" <?php echo $media; ?>/><?php 
					echo "\n" . ( ( ( $k + 1 ) < count( $files ) ) ? "\t" : "" );
				}
			}
		}
	}
	
	/**
	 * Scripts placement on the main template
	 * 
	 * @param none
	 * @return output
	 */
	public function print_scripts()
	{
		if ( ! empty( $this->js ) )
		{
			foreach ( $this->js AS $k => $v )
			{
				$uri_file = ( strrpos( $v, "//" ) === FALSE ) ? base_url( "assets/js/" . $v ) : $v;
				?><script type="text/javascript" src="<?php echo $uri_file; ?>"></script><?php 
				echo "\n" . ( ( ( $k + 1 ) < count( $this->js ) ) ? "\t" : "" );
			}
		}
	}

	/**
	 * Stylesheets loader
	 * 
	 * @param string $filename The filename
	 * @return output
	 */
	public function load_style( $filename, $media = 'none' )
	{
		$fn = explode( ".", $filename );
		$ext = $fn[(count( $fn )-1)];
		if ( $ext != "css" ) $filename = $filename . ".css";
		// $filename .= ( ! empty( $this->conf['cache_assets'] ) ? "" : "?v=" . time() );
		$filename .= "?v=" . $this->conf['version'];

		if ( empty( $this->css[$media] ) ) $this->css[$media] = array();

		if ( ! in_array( $filename, $this->css[$media] ) )
		{
			$this->css[$media][] = $filename;
		}
		// array_push( $this->css, $filename );
	}

	/**
	 * Scripts loader
	 * 
	 * @param string $filename The filename
	 * @return output
	 */
	public function load_script( $filename )
	{
		$fn = explode( ".", $filename );
		$ext = $fn[(count( $fn )-1)];
		if ( $ext != "js" ) $filename = $filename . ".js";
		// $filename .= ( ! empty( $this->conf['cache_assets'] ) ? "" : "?v=" . time() );
		$filename .= "?v=" . $this->conf['version'];

		if ( ! in_array( $filename, $this->css ) )
		{
			$this->js[] = $filename;
		}
		// array_push( $this->js, $filename );
	}

	/**
	 * Stylesheets unloader
	 * 
	 * @param string $filename The filename
	 * @return output
	 */
	public function unload_style( $filename )
	{
		if ( ! empty( $this->css ) )
		{
			foreach ( $this->css AS $key => $file )
			{
				$fn = explode( ".", $file );
				$ext = $fn[( count( $fn ) - 1 )];
				if ( $ext != "css" ) $file = $file . ".css";

				if ( $filename == $file ) unset( $this->css[$key] );
			}
		}
	}
		
	/**
	 * Scripts unloader
	 * 
	 * @param string $filename The filename
	 * @return output
	 */
	public function unload_script( $filename )
	{
		if ( ! empty( $this->js ) )
		{
			foreach ( $this->js AS $key => $file )
			{
				$fn = explode( ".", $file );
				$ext = $fn[( count( $fn ) - 1 )];
				if ( $ext != "js" ) $file = $file . ".min.js";

				if ( $filename == $file ) unset( $this->js[$key] );
			}
		}
	}

	/**
	 * Prints the active class within menu items
	 * 
	 * @param  mixed $controller
	 * @return output
	 */
	public function active_menu_link( $controller )
	{
		$active = FALSE;

		if ( is_array( $controller ) )
		{
			if ( in_array( $this->uri->segment( 1 ), $controller ) ) $active = TRUE;
		}
		else
		{
			if ( $controller == $this->uri->segment( 1 ) ) $active = TRUE;
		}

		if ( $controller == 'dashboard' AND empty( $this->uri->segment( 1 ) ) ) $active = TRUE;

		if ( $active ) echo ' active';
	}

	/**
	 * Fill the empty vars from an array, for viewing purposes. Don't use for normal logic
	 * If you complete a table_index, without the $set, it will fill the empty cols too, 
	 * with the no data message, obviously
	 * 
	 * @param  array  $set
	 * @param  string $table_index
	 * @return array
	 */
	public function fill_empty_vars( $set = NULL, $table_index = NULL )
	{
		$this->load->helper( 'array' );

		if ( ! empty( $set ) AND is_array( $set ) )
		{
			$set['original'] = $set;
			$set = apply_empty_message_array( $set, $this->conf['no_data'] );
/*			
			OLD STUFF, let's keep it just in case

			foreach ( $set AS $k => $v )
			{
				if ( empty( $v ) AND ( 
					$k !== "modified" AND 
					$k !== "created" AND 
					$k !== "modifier" AND 
					$k !== "creator" ) ) $set[$k] = $this->conf['no_data'];
			}
*/
		}
		else if ( empty( $set ) AND ! empty( $table_index ) )
		{
			// In case we pass the table name, then let's populate the return with the column values
			if ( $table = $this->assets->conf['tables'][$table_index] )
			{
				if ( $this->db->table_exists( $table ) )
				{
					$cols = $this->db->list_fields( $table );

					if ( ! empty( $cols ) )
					{
						foreach ( $cols AS $col ) $set[$col] = $this->conf['no_data'];
					}
				}
			}
		}

		return $set;
	}
}
