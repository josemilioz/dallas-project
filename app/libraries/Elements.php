<?php if ( ! defined( 'BASEPATH' ) ) exit( 'No direct script access allowed' );

class Elements
{
	/**
	 * The class constructor
	 *
	 * @access	public
	 * @return	none
	 */
	public function __construct()
	{}

	/**
	 * Enables the use of CI super-global without having to define an extra variable.
	 *
	 * I can't remember where I first saw this, so thank you if you are the original author. -Militis
	 *
	 * @access	public
	 * @param	$var
	 * @return	mixed
	 */
	public function __get( $var )
	{
		return get_instance()->$var;
	}

	/**
	 * Builds the array of buttons for list's actions
	 * 
	 * @param  string     $controller  The controller name to complete the URL
	 * @param  string     $var_name    The var name to match the value againts
	 * @param  string     $var_value   The value
	 * @param  array|null $extend      An array to extend the list of buttons
	 * @return array
	 */
	public function lists_action_buttons( $controller, $var_name, $var_value, array $extend = NULL )
	{
		$trash_extend = ( $this->uri->segment( 2 ) == "search" ) ? "&s=" . $this->input->get( 's' ) : NULL;
		$query_string = ( ! empty( $this->input->server( 'QUERY_STRING' ) ) ) ? '&' . $this->input->server( 'QUERY_STRING' ) : NULL;

		$class_extend = "";

		$buttons = array(
			'edit' => anchor(
				$controller . '/edit/' . $var_value,
				'<i class="fa fa-pencil-alt"></i>',
				array(
					'title' => __( "Edit" ),
					'class' => "btn btn-edit btn-secondary $class_extend",
				)
			),
			'trash' => anchor(
				$controller . '/' . $this->uri->segment( 2 ) . '?action=trash&' . $var_name . '=' . $var_value . $trash_extend . $query_string,
				'<i class="fa fa-trash"></i>',
				array(
					'title' 		=> __( "Trash" ),
					'data-confirm' 	=> __( "Are you sure you want to trash this item?" ),
					'class' 		=> "btn btn-trash btn-warning $class_extend",
				)
			),
			'restore' => anchor(
				$controller . '/trash?action=restore&' . $var_name . '=' . $var_value . $query_string,
				'<i class="fa fa-redo"></i>',
				array(
					'title' => __( "Restore" ),
					'class' => "btn btn-restore btn-success $class_extend",
				)
			),
			'delete' => anchor(
				$controller . '/' . $this->uri->segment( 2 ) . '?action=delete&' . $var_name . '=' . $var_value . $query_string,
				'<i class="fa fa-times"></i>',
				array(
					'title' 		=> __( "Delete" ),
					'data-confirm' 	=> __( "Are you sure you want to delete this item?\n\nWARNING: This action will be PERMANENT and NOT undoable." ),
					'class' 		=> "btn btn-delete btn-danger $class_extend",
				)
			),
			'brief' => anchor(
				$controller . '/brief/' . $var_value,
				'<i class="fa fa-search"></i> ' . __( "Brief" ),
				array(
					'class' 				=> 'brief dropdown-item',
					'data-enlarge-modal' 	=> 1
				)
			),
		);

		if ( ! empty( $extend ) )
		{
			foreach ( $extend AS $key => $value )
			{
				$extend[$key] = sprintf( $value, $var_value );
			}

			$buttons = array_merge( $buttons, $extend );
		} 

		return $buttons;
	}

	/**
	 * Returns formatted classes for body identities purposes
	 * 
	 * @param  string $extra_classes Extra classes to add to string
	 * @return string/boolean
	 */
	public function body_class( $extra_classes = NULL )
	{
		$segs = array();

		if ( $this->uri->total_segments() > 0 )
		{
			for ( $i = 1; $this->uri->total_segments() >= $i; $i++ )
			{
				if ( ! is_numeric( $this->uri->segment( $i ) ) )
				{
					array_push( $segs, $this->uri->segment( $i ) );
				}
				else
				{
					array_push( $segs, 'element-' . $this->uri->segment( $i ) );
				}
			}			
		}
		else
		{
			array_push( $segs, 'home' );
			array_push( $segs, 'dashboard' );			
		}

		if ( ! empty( $segs ) )
		{
			$segs[] = implode( '-', $segs );
			$segs = array_unique( $segs );

			return trim( implode( ' ', $segs ) . " " . $extra_classes );
		}

		return false;
	}

	/**
	 * Clears the query string from forbidden actions we want to avoid
	 * 
	 * @param  array  $to_clear     The array of forbidden keys
	 * @param  array  $query_string The query string to clear
	 * @return string
	 */
	public function clear_query_string( array $to_clear, $query_string )
	{
		if ( ! empty( $query_string ) )
		{
			parse_str( $query_string, $query_string_array );

			if ( ! empty( $query_string_array ) )
			{
				foreach ( $to_clear AS $keyn => $clearer )
				{
					foreach ( $query_string_array AS $key => $value )
					{
						if ( $clearer == $key ) unset( $query_string_array[$key] );
					}
				}

				$query_string = http_build_query( $query_string_array );
			}			
		}

		return $query_string;
	}
}
