<?php if ( ! defined( 'BASEPATH' ) ) exit( 'No direct script access allowed' );

class MY_Form_validation extends CI_Form_validation
{

	/**
	 * The class constructor
	 *
	 * @access	public
	 * @return	none
	 */
	public function __construct( $config = array() )
	{
		parent::__construct( $config );
	}

	/**
	 * Enables the use of CI super-global without having to define an extra variable.
	 *
	 * I can't remember where I first saw this, so thank you if you are the original author. -Militis
	 *
	 * @access	public
	 * @param	$var
	 * @return	mixed
	 */
	public function __get( $var )
	{
		return get_instance()->$var;
	}

	/**
	 * Checks the ruc is valid
	 * 
	 * @param  string $value
	 * @return boolean
	 */
	public function valid_ruc( $value )
	{
		if ( ! preg_match( '/([0-9]{5,20}-[0-9]{1})|([0-9]{5,20})/', $value ) )
		{
			$this->set_message( 'valid_ruc', __( 'The {field} field must contain a valid {field}.' ) );
			return false;	
		}

		return true;
	}

	/**
	 * Callback to validate a time of 24hs
	 * 
	 * @param  string $value
	 * @return boolean
	 */
	public function valid_time( $value )
	{
		if ( ! preg_match( '/^(0[0-9]|1[0-9]|2[0-3]):[0-5][0-9]+$/', $value ) )
		{
			$this->set_message( 'valid_time', __( 'The {field} field must contain a valid time value.' ) );
			return false;	
		}

		return true;
	}

	/**
	 * Checks the bill number is valid, statically
	 * 
	 * @param  string $value
	 * @return boolean
	 */
	public function valid_bill_number_static( $value )
	{
		if ( strrpos( "/" . $value . "/", "_" ) )
		{
			$this->set_message( 'valid_bill_number_static', __( 'The {field} field may only contain numbers and dashes.' ) );
			return false;
		}

		if ( ! preg_match( '/([0-9]{3})-([0-9]{3})-([0-9]{5,7})/', $value ) )
		{
			$this->set_message( 'valid_bill_number_static', __( 'The {field} field must contain a valid bill number.' ) );
			return false;			
		}

		return true;
	}

	/**
	 * -----------------------------------------------------------------------------------------------------------------------
	 *  							REFORMULAR PARA HACER COINCIDIR CON LA TABLA DE TIMBRADOS
	 * -----------------------------------------------------------------------------------------------------------------------
	 */

	public function date_greater_than( $value, $check )
	{
		$value = strtotime( $value );
		$check = strtotime( $check );

		if ( $value <= $check )
		{
			$this->set_message( 'date_greater_than', __( 'The {field} must be greater than the Date start.' ) );
			return false;
		}

		return true;
	}
}