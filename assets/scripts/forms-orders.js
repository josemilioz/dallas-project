jQuery(function($){

	/**
	 * UPLOAD FILE
	 */

	build_file_upload = function()
	{
		$( document ).find( "#fileupload" ).fileupload({
			url: conf.site_url + 'files/upload',
			dataType: 'json',
			type: 'POST',
			formData: {
				'location' 		: '/uploads/orders/',
				'controller' 	: 'orders',
				'registry_id' 	: ( $( "#order_id" ).val() !== "" ) ? $( "#order_id" ).val() : null,
			},
			start: function(){
				$( '#progress .progress-bar' ).removeClass( 'bg-danger' ).removeClass( 'bg-success' );
			},
			progressall: function ( e, data ){
				var progress = parseInt( data.loaded / data.total * 100, 10 );
				$( '#progress .progress-bar' ).css( 'width', progress + '%' );
			},
			complete: function( object, status ){
				var existant_ids = [];

				if ( $( 'input[name^="file_id"]' ).length > 0 )
				{
					$( 'input[name^="file_id"]' ).each( function( ind, val ){
						existant_ids.push( $( this ).val() );
					});
				}

				if ( status == "success" && object.responseJSON.upload )
				{
					$.ajax({
						url: 		conf.site_url + 'files/multiple_files_retrieving',
						dataType: 	'json',
						type: 		'POST',
						data: {
							existant_ids 	: existant_ids,
							new_upload 		: object.responseJSON.upload,
						},
						success: function( response )
						{
							if ( response.html_form )
							{
								$( document ).find( "#files-card .card-body *" ).remove();
								$( response.html_form ).appendTo( '#files-card .card-body' );
							}
						},
						complete: function()
						{
							rebuild_tooltips();
							build_file_upload();
							build_file_delete();
						}
					});
				}
			},
			error: function( object, status, error ){
				if ( object.responseJSON.error ) alert( object.responseJSON.error );
				$( '#progress .progress-bar' ).prop( 'style', false );
			}
		});
	};

	/**
	 * DELETE FILE
	 */
	
	build_file_delete = function()
	{
		$( document ).on( 'click', '.btn-delete-file', function( ev ){
			ev.preventDefault();

			var file_id = $( this ).parent().find( 'input[name^="file_id"]' ).val();

			if ( file_id !== "" )
			{
				var remaining_ids = [];

				if ( $( 'input[name^="file_id"]' ).length > 0 )
				{
					$( 'input[name^="file_id"]' ).each( function( ind, val ){
						if ( $( this ).val() != file_id ) remaining_ids.push( $( this ).val() );
					});
				}

				$.ajax({
					url: 		conf.site_url + 'files/delete_on_multiple',
					type: 		'POST',
					dataType: 	'json',
					data: { 
						file_id 		: file_id,
						remaining_ids	: remaining_ids,
						view_name 		: 'files/model-multiple-files',
					},
					success: function( response )
					{
						if ( response.html_form )
						{
							$( document ).find( "#files-card .card-body *" ).remove();
							$( response.html_form ).appendTo( '#files-card .card-body' );
						}
					},
					error: function( object, status, error ){
						if ( object.responseJSON.error ) alert( object.responseJSON.error );
					},
					complete: function()
					{
						rebuild_tooltips();
						build_file_upload();
					}
				});
			}
		});
	};

	/**
	 * SELECT2 INIT
	 */
	
	$( "#client_id, #seller_id" ).select2({
		language: conf.language_short,		
	});

	/**
	 * SELLER PICKER WHEN THE CLIENT IS SELECTED - Only when adding, not when editing
	 */

	if ( $( "body.orders-add" ).length > 0 )
	{
		$( "#client_id" ).on( 'change', function(){
			var client_id = $( this ).val();

			$.getJSON( conf.site_url + 'ajax/get_client/' + client_id, function( response, status ){
				if ( status == "success" && ! response.error )
				{
					if ( response.seller_id != "" && response.seller_id != null )
					{
						$( "select#seller_id" ).val( response.seller_id ).trigger( 'change' );
					}
					else
					{
						$( "select#seller_id" ).val( $( "select#seller_id" ).data( 'init-value' ) ).trigger( 'change' );
					}
				}
				else if ( response.error )
				{
					alert( response.error );
				}
			});
		});		
	}

	/**
	 * ADD PRODUCT MODAL PRODUCT ID ON CHANGE
	 */

	var modal_build_product_select = function( data )
	{
		var options = {};
			options.language = conf.language_short;

		if ( data && data.length > 0 ) options.data = data;

		$( "select#modal_product_id" ).select2( options ).on( 'change', function( ev ){
			if ( $( this ).val() !==  "" )
			{
				$.getJSON( conf.site_url + 'ajax/product_retrieve/' + $( this ).val(), function( result, status){
					if ( status == "success" )
					{
						$( "#modal-add-product #modal_unit_price_box" ).find( '.input-group-text' ).text( conf.currencies[result.currency] );
						$( "#modal-add-product #modal_quantity_box" ).find( '.input-group-text' ).text( conf.units[result.unit_measure] );
						$( "#modal-add-product #modal_currency" ).val( result.currency );
					}
				});
			}
		});
	}

	modal_build_product_select();

	/**
	 * RESET TABLE AND STORE FIRST ROW
	 */
	
	var clean_row = $( 'table.product-list tbody tr:first' ).clone();
	$( 'table.product-list tbody tr:first' ).remove();

	/**
	 * ADD PRODUCT BUTTON
	 */
	
	$( "#add-product-button" ).on( 'click', function( ev ){
		ev.preventDefault();

		$( "#modal-add-product" ).modal( 'show' );
	});

	/**
	 * ADD PRODUCT MODAL ON HIDE
	 */
	
	$( "#modal-add-product" ).on( 'hidden.bs.modal', function( ev ){
		$( "#modal-add-product" ).find( 'input' ).val( '' );
		$( "#modal-add-product" ).find( '.input-group-text' ).text( '' );
		$( "#modal-add-product" ).find( 'select' ).val( '' ).trigger( 'change' );
	});

	/**
	 * ADD PRODUCT MODAL PRODUCT ID ON CHANGE
	 */

	$( "select#modal_product_id" ).select2({
		language: conf.language_short,
	}).on( 'change', function( ev ){
		if ( $( this ).val() !==  "" )
		{
			$.getJSON( conf.site_url + 'ajax/product_retrieve/' + $( this ).val(), function( result, status){
				if ( status == "success" )
				{
					$( "#modal-add-product #modal_unit_price_box" ).find( '.input-group-text' ).text( conf.currencies[result.currency] );
					$( "#modal-add-product #modal_quantity_box" ).find( '.input-group-text' ).text( conf.units[result.unit_measure] );
					$( "#modal-add-product input#modal_currency" ).val( result.currency );
					$( "#modal-add-product #modal_unit_price_box" ).find( 'input#modal_unit_price' ).inputmask( 'setvalue', result.price );
				}
			});			
		}
	});

	/**
	 * ADD PRODUCT TO LIST
	 */
	
	$( "#confirm-add-product" ).on( 'click', function( ev ){
		ev.preventDefault();

		var new_row 			= clean_row.clone();
		var product_id			= $( "#modal-add-product" ).find( 'select#modal_product_id' ).val();
		var description 		= $( "#modal-add-product" ).find( 'select#modal_product_id option:selected' ).text();
		var unit_measure		= $( "#modal-add-product" ).find( '#modal_quantity_box .input-group-text' ).text();
		var currency			= $( "#modal-add-product" ).find( 'input#modal_currency' ).val();
		var quantity 			= parseFloat( $( "#modal-add-product" ).find( 'input#modal_quantity' ).inputmask( 'unmaskedvalue' ) );
		var unit_price 			= parseFloat( $( "#modal-add-product" ).find( 'input#modal_unit_price' ).inputmask( 'unmaskedvalue' ) );

		if ( 
			description == '' || 
			product_id == '' || 
			quantity <= 0 || 
			isNaN( quantity ) || 
			quantity <= 0 || 
			isNaN( unit_price ) || 
			unit_price <= 0
		)
		{
			alert( messages.complete_fields_to_list );
			return false;
		}

		// Let's even with the system main currency prior to calculations
		// if ( currency != conf.currency )
		unit_price		= ( ! $.isEmptyObject( exchange_values ) ) ? parseFloat( unit_price * parseFloat( exchange_values[currency].sale ) ) : parseFloat( unit_price );
		var row_total	= parseFloat( quantity * unit_price );
		
		// First, fill up input values
		new_row.find( '.input-product-id' ).val( product_id );
		new_row.find( '.input-description' ).val( description );
		new_row.find( '.input-unit-price' ).val( unit_price );
		new_row.find( '.input-quantity' ).val( quantity );
		new_row.find( '.input-quantity-remaining' ).val( quantity );
		new_row.find( '.input-row-total' ).val( row_total );

		// Then, output html
		new_row.find( 'td:nth-child(1)' ).text( description );
		new_row.find( 'td:nth-child(2)' ).text( unit_price.toCurrency( conf.decimals ) );
		new_row.find( 'td:nth-child(3)' ).html( quantity.toCurrency( conf.qty_decimals ) + ' <small class="text-muted">' + unit_measure + '</small>' );
		new_row.find( 'td:nth-child(4)' ).html( quantity.toCurrency( conf.qty_decimals ) + ' <small class="text-muted">' + unit_measure + '</small>' );
		new_row.find( 'td:nth-child(5)' ).text( row_total.toCurrency( conf.decimals ) );

		new_row.insertBefore( "table.product-list tbody .total-amount-row" );

		$( "#modal-add-product" ).modal( 'hide' );
		rebuild_tooltips();
		calculate_total();
	});

	/**
	 * CALCULATE TOTAL
	 */

	function calculate_total()
	{
		var total = 0;

		if ( $( "table.product-list tbody tr" ).length > 0 )
		{
			$( '.input-row-total' ).each(function(){
				total += parseFloat( $( this ).val() );
			});
		}
		else
		{
			total = 0;
		}

		if ( isNaN( total ) || total == 0 || total.length <= 0 )
		{ 
			total = '';
		}
		else
		{
			total = total.toCurrency( conf.decimals );
		}

		$( "input#total_amount" ).inputmask( 'setvalue', total );
	}

	/**
	 * REMOVE ITEM FROM THE LIST
	 */
	
	$( document ).on( 'click', '.btn-remove-product', function( ev ){
		ev.preventDefault();

		var parent_row = $( this ).parent().parent();
		var total = 0;

		// Now, remove the row
		parent_row.remove();
		rebuild_tooltips();

		// Add everything to total
		calculate_total();
	});

	/**
	 * INITIALIZE ALL FUNCTIONS
	 */
	
	build_file_upload();
	build_file_delete();
	calculate_total();

	/**
	 * ---------------------------------------------------------------------------
	 * EDIT THE PRICE OF EVERY PRODUCT
	 * ---------------------------------------------------------------------------
	 */

	$( document ).on( 'dblclick', "td.cell-price", function( ev ){
		if ( $.inArray( 'edit_price', actions_enabled ) > -1 )
		{
			var row 		= $( this ).parent();
			var product_id 	= row.find( 'input.input-product-id' ).val();

			$.getJSON( conf.site_url + 'ajax/product_retrieve/' + product_id, function( product, status ) {
				if ( status == "success" && product )
				{
					// Set original magnitude on field
					$( "#modal-edit-magnitude-price" ).find( 'input#modal_edit_magnitude_price' ).inputmask( 'setvalue', parseFloat( row.find( 'input.input-unit-price' ).val() ) );

					// Set the product vars on their places
					$( "#modal-edit-magnitude-price" ).find( ".modal-body h5" ).text( product.name );
					$( "#modal-edit-magnitude-price" ).find( ".product-picture" ).html( '<img src="' + product.picture + '" class="picture-big w-100" />' );
					$( "#modal-edit-magnitude-price" ).find( ".input-group-text" ).text( conf.currencies[conf.currency] ); // on editing, we're already using the system currency

					// Hide modal event, and clean
					$( "#modal-edit-magnitude-price" ).on( 'hidden.bs.modal', function( ev ){
						$( "#modal-edit-magnitude-price" ).find( ".modal-body h5" ).text( '' );
						$( "#modal-edit-magnitude-price" ).find( ".product-picture" ).html( '' );
						$( "#modal-edit-magnitude-price" ).find( ".input-group-text" ).text( '' );
						$( "#modal-edit-magnitude-price" ).find( "input#modal_edit_magnitude_price" ).removeData();
						$( "#modal-edit-magnitude-price" ).find( "input#modal_edit_magnitude_price" ).inputmask( 'setvalue', '' );
					});

					// Focus on the field on shown
					$( "#modal-edit-magnitude-price" ).on( 'shown.bs.modal', function( ev ){
						$( "#modal-edit-magnitude-price" ).find( 'input#modal_edit_magnitude_price' ).focus().select();
					});

					// Events to overwrite
					$( "#modal-edit-magnitude-price" ).find( "button#confirm-edit-magnitude-price" ).on( 'click', function( ev ){
						var value 		= $( "#modal-edit-magnitude-price" ).find( 'input#modal_edit_magnitude_price' ).inputmask( 'unmaskedvalue' );
						var quantity 	= parseFloat( row.find( "input.input-quantity" ).val() );

						if ( isNaN( value ) || value <= 0 )
						{
							alert( messages.price_invalid );
						}
						else
						{
							row.find( "input.input-unit-price" ).val( value );
							row.find( "input.input-row-total" ).val( value * quantity );

							row.find( 'td.cell-price' ).text( parseFloat( row.find( "input.input-unit-price" ).val() ).toCurrency( conf.decimals ) );
							row.find( 'td.cell-row-total' ).text( parseFloat( row.find( "input.input-row-total" ).val() ).toCurrency( conf.decimals ) );

							$( "#modal-edit-magnitude-price" ).find( "button#confirm-edit-magnitude-price" ).off( 'click' );
							$( "#modal-edit-magnitude-price" ).modal( 'hide' );
							calculate_total();
						}						
					});
					$( "#modal-edit-magnitude-price" ).find( 'input#modal_edit_magnitude_price' ).on( 'keyup', function( ev ){ if ( ev.keyCode == "13" ) $( "#modal-edit-magnitude-price" ).find( "button#confirm-edit-magnitude-price" ).trigger( 'click' ); });

					// Show the modal
					$( "#modal-edit-magnitude-price" ).modal( 'show' );
				}
			});
		}
		else
		{
			alert( messages.user_level_forbidden_action );
		}
	});

	/**
	 * ---------------------------------------------------------------------------
	 * EDIT THE QUANTITY OF EVERY PRODUCT
	 * ---------------------------------------------------------------------------
	 */
	
	$( document ).on( 'dblclick', "td.cell-quantity", function( ev ){
		if ( $.inArray( 'edit_quantity', actions_enabled ) > -1 )
		{
			var row 		= $( this ).parent();
			var product_id	= row.find( "input.input-product-id" ).val();

			$.getJSON( conf.site_url + 'ajax/product_retrieve/' + product_id, function( product, status ) {
				if ( status == "success" && product )
				{
					// Set original magnitude on field
					$( "#modal-edit-magnitude-quantity" ).find( 'input#modal_edit_magnitude_quantity' ).inputmask( 'setvalue', parseFloat( row.find( 'input.input-quantity' ).val() ) );

					// Set the product vars on their places
					$( "#modal-edit-magnitude-quantity" ).find( ".modal-body h5" ).text( product.name );
					$( "#modal-edit-magnitude-quantity" ).find( ".product-picture" ).html( '<img src="' + product.picture + '" class="picture-big w-100" />' );
					$( "#modal-edit-magnitude-quantity" ).find( ".input-group-text" ).text( conf.units[product.unit_measure] );
					$( "#modal-edit-magnitude-quantity" ).find( 'input#modal_edit_magnitude_quantity' ).data( 'unit-measure', product.unit_measure );

					// Focus on the field on shown
					$( "#modal-edit-magnitude-quantity" ).on( 'shown.bs.modal', function( ev ){
						$( "#modal-edit-magnitude-quantity" ).find( 'input#modal_edit_magnitude_quantity' ).focus().select();
					});

					// Hide modal event, and clean
					$( "#modal-edit-magnitude-quantity" ).on( 'hidden.bs.modal', function( ev ){
						$( "#modal-edit-magnitude-quantity" ).find( ".modal-body h5" ).text( '' );
						$( "#modal-edit-magnitude-quantity" ).find( ".product-picture" ).html( '' );
						$( "#modal-edit-magnitude-quantity" ).find( ".input-group-text" ).text( '' );
						$( "#modal-edit-magnitude-quantity" ).find( "input#modal_edit_magnitude_quantity" ).removeData();
						$( "#modal-edit-magnitude-quantity" ).find( "input#modal_edit_magnitude_quantity" ).inputmask( 'setvalue', '' );
					});

					// Events to overwrite
					$( "#modal-edit-magnitude-quantity" ).find( "button#confirm-edit-magnitude-quantity" ).on( 'click', function( ev ){
						var unit_measure 	= $( "#modal-edit-magnitude-quantity" ).find( 'input#modal_edit_magnitude_quantity' ).data( 'unit-measure' );
						var value			= parseFloat( $( "#modal-edit-magnitude-quantity" ).find( 'input#modal_edit_magnitude_quantity' ).inputmask( 'unmaskedvalue' ) );
						var price 			= parseFloat( row.find( "input.input-unit-price" ).val() );

						if ( isNaN( value ) || value <= 0 )
						{
							alert( messages.quantity_invalid );
						}
						else
						{
							row.find( "input.input-quantity" ).val( value );
							row.find( "input.input-row-total" ).val( value * price );

							row.find( 'td.cell-quantity' ).html( parseFloat( row.find( "input.input-quantity" ).val() ).toCurrency( conf.qty_decimals ) + ' <small class="text-muted">' + conf.units[unit_measure] + '</small>' );
							row.find( 'td.cell-row-total' ).text( parseFloat( row.find( "input.input-row-total" ).val() ).toCurrency( conf.decimals ) );

							calculate_total();

							$( "#modal-edit-magnitude-quantity" ).find( "button#confirm-edit-magnitude-quantity" ).off( 'click' );
							$( "#modal-edit-magnitude-quantity" ).modal( 'hide' );
							ev.stopImmediatePropagation(); // Just do it once
						}
					});

					$( "#modal-edit-magnitude-quantity" ).find( 'input#modal_edit_magnitude_quantity' ).on( 'keyup', function( ev ){ 
						if ( ev.keyCode == "13" ) $( "#modal-edit-magnitude-quantity" ).find( "button#confirm-edit-magnitude-quantity" ).trigger( 'click' ); 
					});

					// Show the modal
					$( "#modal-edit-magnitude-quantity" ).modal( 'show' );
				}
			});
		}
		else
		{
			alert( messages.user_level_forbidden_action );
		}
	});

	/**
	 * ---------------------------------------------------------------------------
	 * EDIT THE REMAINING OF EVERY PRODUCT
	 * ---------------------------------------------------------------------------
	 */
	
	$( document ).on( 'dblclick', "td.cell-remaining", function( ev ){
		if ( $.inArray( 'edit_quantity', actions_enabled ) > -1 )
		{
			var row 		= $( this ).parent();
			var product_id	= row.find( "input.input-product-id" ).val();

			$.getJSON( conf.site_url + 'ajax/product_retrieve/' + product_id, function( product, status ) {
				if ( status == "success" && product )
				{
					var rebuilt_options_input		= input_options.quantity;
						rebuilt_options_input.max	= row.find( 'input.input-quantity' ).val();
						rebuilt_options_input.min	= 0;

					// Set original magnitude on field
					$( "#modal-edit-magnitude-remaining" ).find( 'input#modal_edit_magnitude_remaining' ).inputmask( 'remove' );
					$( "#modal-edit-magnitude-remaining" ).find( 'input#modal_edit_magnitude_remaining' ).inputmask( rebuilt_options_input );
					$( "#modal-edit-magnitude-remaining" ).find( 'input#modal_edit_magnitude_remaining' ).inputmask( 'setvalue', parseFloat( row.find( 'input.input-quantity-remaining' ).val() ) );

					// Set the product vars on their places
					$( "#modal-edit-magnitude-remaining" ).find( ".modal-body h5" ).text( product.name );
					$( "#modal-edit-magnitude-remaining" ).find( ".product-picture" ).html( '<img src="' + product.picture + '" class="picture-big w-100" />' );
					$( "#modal-edit-magnitude-remaining" ).find( ".input-group-text" ).text( conf.units[product.unit_measure] );

					// Focus on the field on shown
					$( "#modal-edit-magnitude-remaining" ).on( 'shown.bs.modal', function( ev ){
						$( "#modal-edit-magnitude-remaining" ).find( 'input#modal_edit_magnitude_remaining' ).focus().select();
					});

					// Hide modal event, and clean
					$( "#modal-edit-magnitude-remaining" ).on( 'hidden.bs.modal', function( ev ){
						$( "#modal-edit-magnitude-remaining" ).find( ".modal-body h5" ).text( '' );
						$( "#modal-edit-magnitude-remaining" ).find( ".product-picture" ).html( '' );
						$( "#modal-edit-magnitude-remaining" ).find( ".input-group-text" ).text( '' );
					});

					// Events to overwrite
					$( "#modal-edit-magnitude-remaining" ).find( "button#confirm-edit-magnitude-remaining" ).on( 'click', function( ev ){
						var value = parseFloat( $( "#modal-edit-magnitude-remaining" ).find( 'input#modal_edit_magnitude_remaining' ).inputmask( 'unmaskedvalue' ) );

						if ( isNaN( value ) || value == "" ) value = 0;

						if ( isNaN( value ) || value < 0 )
						{
							alert( messages.quantity_invalid_zero );
						}
						else
						{
							row.find( "input.input-quantity-remaining" ).val( value );
							row.find( 'td.cell-remaining' ).html( parseFloat( row.find( "input.input-quantity-remaining" ).val() ).toCurrency( conf.qty_decimals ) + ' <small class="text-muted">' + conf.units[product.unit_measure] + '</small>' );

							$( "#modal-edit-magnitude-remaining" ).find( "button#confirm-edit-magnitude-remaining" ).off( 'click' );
							$( "#modal-edit-magnitude-remaining" ).modal( 'hide' );
							ev.stopImmediatePropagation(); // Just do it once
						}
					});

					$( "#modal-edit-magnitude-remaining" ).find( 'input#modal_edit_magnitude_remaining' ).on( 'keyup', function( ev ){ 
						if ( ev.keyCode == "13" ) $( "#modal-edit-magnitude-remaining" ).find( "button#confirm-edit-magnitude-remaining" ).trigger( 'click' ); 
					});

					// Show the modal
					$( "#modal-edit-magnitude-remaining" ).modal( 'show' );
				}
			});
		}
		else
		{
			alert( messages.user_level_forbidden_action );
		}
	});

	/**
	 * ---------------------------------------------------------------------------
	 * ADD NEW PRODUCT MODAL - BUT NEW FOR REAL
	 * ---------------------------------------------------------------------------
	 */
	
	$( "#modal-add-new-product select" ).select2({
		language: conf.language_short,
	});

	/**
	 * ADD NEW PRODUCT - OPEN MODAL
	 */
	
	$( "button#open-new-product-modal" ).on( 'click', function( ev ){
		$( "#modal-add-product" ).modal( 'hide' );
		$( "#modal-add-new-product" ).modal( 'show' );
	});

	/**
	 * INIT MODAL TO ADD NEW PRODUCT WITH ALL ITS BEHAVIORS
	 */

	$( "#modal-add-new-product" ).on( 'show.modal.bs', function(){
		// Pick currency
		$( '#modal_new_product_unit_price_box' ).find( '.dropdown-menu a' ).on( 'click', function( ev ){
			ev.preventDefault();
			var currency = $( this ).data( 'currency' );

			if ( currency != "" )
			{
				$( "input#modal_new_product_currency" ).val( currency );
				$( '#modal_new_product_unit_price_box' ).find( 'button' ).text( conf.currencies[currency] );

				// Change the input class
				if ( currency == conf.currency )
				{
					$( '#modal_new_product_unit_price_box' ).find( "input#modal_new_product_unit_price" ).inputmask( 'remove' );
					$( '#modal_new_product_unit_price_box' ).find( "input#modal_new_product_unit_price" ).inputmask( input_options.currency );
				}
				else
				{
					$( '#modal_new_product_unit_price_box' ).find( "input#modal_new_product_unit_price" ).inputmask( 'remove' );
					$( '#modal_new_product_unit_price_box' ).find( "input#modal_new_product_unit_price" ).inputmask( input_options.currency_foreign );
				}
			}
		});
		
		// Send the new product for storing... or not
		$( "#confirm-add-new-product" ).on( 'click', function( ev ){
			ev.preventDefault();

			var goon = false;

			$( "#modal-add-new-product" ).find( "input, select" ).each( function(){
				if ( $( this ).val() == "" )
				{
					goon = false;
					alert( messages.complete_fields_to_list );
				}
				else
				{
					goon = true;
				}
			});

			if ( goon )
			{
				$.getJSON( conf.site_url + 'ajax/get_csrf_hash', function( response, status ) {
					if ( status == "success" )
					{
						var to_send 					= {};
							to_send.name 				= $( "#modal-add-new-product" ).find( "#modal_new_product_name" ).val();
							to_send.unit_measure		= $( "#modal-add-new-product" ).find( "#modal_new_product_unit_measure" ).val();
							to_send.category_id			= $( "#modal-add-new-product" ).find( "#modal_new_product_category_id" ).val();
							to_send.price				= $( "#modal-add-new-product" ).find( "#modal_new_product_unit_price" ).inputmask( 'unmaskedvalue' );
							to_send.currency			= $( "#modal-add-new-product" ).find( "#modal_new_product_currency" ).val();
							to_send.tax					= $( "#modal-add-new-product" ).find( "#modal_new_product_tax" ).val();
							to_send[response.csrf_name] = response.csrf_hash;

						$.ajax({
							url: conf.site_url + 'ajax/store_product',
							type: 'POST',
							data: to_send,
							success: function( response )
							{
								if ( response != null && response != "" && ! response.error )
								{
									var product_id = response.item_row.product_id;

									$.getJSON( conf.site_url + 'ajax/product_dropdown_retrieve', function( response, status ) {
										if ( status == "success" && response != null && response != "" && ! response.error )
										{
											$( "#modal-add-product" ).find( '#modal_product_id' ).select2( 'destroy' );
											$( "#modal-add-product" ).find( '#modal_product_id option, #modal_product_id optgroup' ).remove();

											var product_list = new Array;
											$.map( response, function( ele, ind ) {
												if ( typeof ele == "string" )
												{
													product_list.push({
														"id": ind,
									  					"text": ( ele != "&nbsp;" ) ? ele : "\n",
													});
												}
												else if ( typeof ele == "object" )
												{
													var with_children = {
									  					"text": ind,
									  					"children": []
													};

													$.map( ele, function( ele_2, ind_2 ) {
														with_children.children.push({
															"id": ind_2,
										  					"text": ele_2,
														});
													});

													product_list.push( with_children );
												}
											});

											modal_build_product_select( product_list );

											$( "#modal-add-product" ).find( '#modal_product_id' ).val( product_id ).trigger( 'change' );
											$( "#modal-add-new-product" ).modal( 'hide' );
											$( "#modal-add-product" ).modal( 'show' );

											// Rebuild inputs on the add new product modal
											$( "#modal-add-new-product" ).find( 'input, select' ).val( '' );
											$( "#modal-add-new-product" ).find( '#modal_new_product_currency' ).val( conf.currency );
											$( "#modal-add-new-product" ).find( '#modal_new_product_unit_price_box button' ).text( conf.currencies[conf.currency] );
										}
										else if ( response.error )
										{
											alert( response.error );
										}
									});
								}
								else if ( response.error )
								{
									alert( response.error );
								}
							},
							error: function( response )
							{
								alert( response.error );
							},
							complete: rebuild_main_tokens,
						});
					}
				});
			}
		});
	});
});