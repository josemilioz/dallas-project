jQuery(function($){

	/**
	 * SORTABLE OPTIONS
	 */
	
	var sortable_opts = {
		stop: function( ev, ui )
		{
			var existant_ids = [];

			if ( $( 'input[name^="file_id"]' ).length > 0 )
			{
				$( 'input[name^="file_id"]' ).each( function( ind, val ){
					existant_ids.push( $( this ).val() );
				});
			}

			if ( existant_ids.length > 0 )
			{
				$.ajax({
					url		: conf.site_url + 'files/reorder_files',
					type	: 'POST',
					data : {
						existant_ids : existant_ids,
					}
				});
			}
		}
	};

	/**
	 * UPLOAD PICTURE
	 */

	build_file_upload = function()
	{
		$( document ).find( "#fileupload" ).fileupload({
			url: conf.site_url + 'files/upload',
			dataType: 'json',
			type: 'POST',
			// fileInput: 'files',
			// paramName: 'files[]',
			formData: {
				'file_types' 	: 'gif|jpg|png|jpeg|JPG|JPEG|PNG|GIF',
				'location' 		: '/uploads/products/',
				'controller' 	: 'products',
				'view_name'		: 'files/model-multiple-picture',
				'registry_id' 	: ( $( "#product_id" ).val() !== "" ) ? $( "#product_id" ).val() : null,
			},
			start: function(){
				$( '#progress .progress-bar' ).removeClass( 'bg-danger' ).removeClass( 'bg-success' );
			},
			progressall: function ( e, data ){
				var progress = parseInt( data.loaded / data.total * 100, 10 );
				$( '#progress .progress-bar' ).css( 'width', progress + '%' );
			},
			complete: function( object, status ){
				if ( status == "success" && object.responseJSON.upload )
				{
					var existant_ids = [];

					if ( $( 'input[name^="file_id"]' ).length > 0 )
					{
						$( 'input[name^="file_id"]' ).each( function( ind, val ){
							existant_ids.push( $( this ).val() );
						});
					}

					$.ajax({
						url: conf.site_url + 'files/picture_processing',
						dataType: 'json',
						type: 'POST',
						data: {
							existant_ids	: existant_ids,
							new_upload		: object.responseJSON.upload,
						},
						success: function( response )
						{							
							if ( response.html_form )
							{
								$( document ).find( "#picture-card .form-row" ).remove();
								$( response.html_form ).insertAfter( '#picture-card h4' );
							}
						},
						complete: function()
						{
							build_file_delete();
							build_file_upload();
							sortable_opts.stop(); //re-order items in list and store it on the DB
							rebuild_tooltips();

							$( "#sortable-list" ).sortable( sortable_opts );
							$( "#sortable-list" ).disableSelection();
						}
					});
				}
			},
			error: function( object, status, error ){
				if ( object.responseJSON.error ) alert( object.responseJSON.error );
				$( '#progress .progress-bar' ).prop( 'style', false );
			}
		});
	};

	/**
	 * DELETE PICTURE
	 */
	
	build_file_delete = function()
	{
		$( document ).on( 'click', '.fileinput-delete', function( ev ){
			 ev.preventDefault();

			 var file_id = $( this ).data( 'file-id' );

			 if ( file_id !== "" )
			 {
				var remaining_ids = [];

				if ( $( 'input[name^="file_id"]' ).length > 0 )
				{
					$( 'input[name^="file_id"]' ).each( function( ind, val ){
						if ( $( this ).val() != file_id ) remaining_ids.push( $( this ).val() );
					});
				}

			 	$.ajax({
			 		url: conf.site_url + 'files/delete',
			 		type: 'POST',
			 		dataType: 'json',
			 		data: { 
			 			file_id			: file_id,
			 			view_name		: 'files/model-multiple-picture',
			 			remaining_ids 	: remaining_ids,
			 		},
			 		success: function( response )
			 		{
			 			if ( response.html_form )
			 			{
							$( document ).find( "#picture-card .form-row" ).remove();
							$( response.html_form ).insertAfter( '#picture-card h4' );
			 			}
			 		},
					error: function( object, status, error ){
						if ( object.responseJSON.error ) alert( object.responseJSON.error );
					},
					complete: function()
					{
						build_file_upload();
						rebuild_tooltips();
						sortable_opts.stop(); //re-order items in list and store it on the DB
					
						$( "#sortable-list" ).sortable( sortable_opts );
						$( "#sortable-list" ).disableSelection();
					}
			 	});
			 }
		});
	};

	/**
	 * INIT
	 */
	build_file_upload();
	build_file_delete();

	// SORTABLE LIST OF UPLOADS
	$( "#sortable-list" ).sortable( sortable_opts );
	$( "#sortable-list" ).disableSelection();


	/**
	 * ATENDER --------------------------------------------------------------------------------------------------------------------------------------------
	 * ORDENAR PARA UBICAR IMAGENES DEL PRODUCTO
	 * O SEA, DESARROLLAR EL ORDEN DE LAS IMAGENES
	 */

	$( "#sortable-list" ).on( 'change', function(){
		alert('si');
	});

	/**
	 * SELECT2 INIT
	 */
	
	$( "select#category_id, select#currency, select#tax, select#unit_measure" ).select2({
		language: conf.language_short,
	});

	/**
	 * CHANGE CATEGORY ID
	 */
	$( "select#category_id" ).on( 'change', function( ev ){
		if ( $( this ).val() != "" )
		{
			$.getJSON( conf.site_url + 'ajax/category_retrieve/' + $( this ).val(), function( json, status ){
				if ( status == "success" && ! $.isEmptyObject( json ) )
				{
					if ( $( "body" ).hasClass( "add" ) )
					{
						$( "#currency" ).val( json.currency ).trigger( 'change' );
						$( "#unit_measure" ).val( json.unit_measure );
						$( "#unit_quantity" ).inputmask( 'setvalue', ( json.unit_quantity ) ? parseFloat( json.unit_quantity ) : '' );
						$( "#tax" ).val( json.tax ).trigger( 'change' );
					}
					else
					{
						if ( $( "#currency" ).val() == "" ) $( "#currency" ).val( json.currency ).trigger( 'change' );
						if ( $( "#unit_measure" ).val() == "" ) $( "#unit_measure" ).val( json.unit_measure );
						if ( $( "#unit_quantity" ).val() == "" ) $( "#unit_quantity" ).inputmask( 'setvalue', ( json.unit_quantity ) ? parseFloat( json.unit_quantity ) : '' );
						if ( $( "#tax" ).val() == "" ) $( "#tax" ).val( json.tax ).trigger( 'change' );
					}
				}
			});
		}
	});

	/**
	 * CHANGE UNIT MEASURE
	 */
	
	$( "select#unit_measure" ).on( 'change', function( ev ){
		var unit = conf.units[$( this ).val()];
		$( "#measurement-card" ).find( '.input-group-text' ).text( unit );
	});

});