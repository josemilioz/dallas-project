jQuery(function($){

	/**
	 * ---------------------------------------------------------------------------
	 * DEFAULT FOR BOTH
	 * ---------------------------------------------------------------------------
	 */
	
	/**
	 * SELECT2 INIT
	 */
	
	$( "select#seller_id, select#pos_id, select#modal_product_id" ).select2({
		language: conf.language_short,
	});

	/**
	 * ADD PRODUCT MODAL BUTTON TRIGGER
	 */

	$( "button#add-product-button" ).on( 'click', function( ev ){
		ev.preventDefault();

		$( "#modal-add-product" ).modal( 'show' );
	});

	/**
	 * SEARCH CLIENT MODAL BUTTON TRIGGER
	 */
	
	$( "button#search-client-button" ).on( 'click', function( ev ){
		ev.preventDefault();

		// When hiding the modal, let's reset its content
		$( "#modal-search-client" ).on( 'hidden.bs.modal', function( ev ){
			$( "select#modal_client_id" ).val( '' ).trigger( 'change' );
			$( "#modal-search-client" ).find( '#brief-wrapper' ).remove();
			$( "#modal-search-client" ).find( '#flash-client-add' ).show();
			$( "#modal-search-client" ).find( 'input' ).val( '' );
		});

		$( "#modal-search-client" ).modal( 'show' );
	});

	/**
	 * SEARCH CLIENT SELECT2 INIT
	 */
	
	$( "select#modal_client_id" ).select2({
		language: 		conf.language_short,
		placeholder: 	$( "select#modal_client_id" ).attr( 'placeholder' ),
		allowClear: 	true,
		dropdownParent: $( "#modal-search-client" ),
		ajax: {
			url: 		conf.site_url + 'ajax/search_client',
			dataType: 	'json',
			delay: 		500,
		},
	});

	/**
	 * SEARCH CLIENT DISPLAY CLIENT BRIEF
	 */
	
	$( "select#modal_client_id" ).on( 'change', function( ev ){
		$( "#modal-search-client" ).find( '#brief-wrapper' ).remove();
		$( "#modal-search-client" ).find( '#flash-client-add' ).show();

		if ( $( this ).val() !== "" && $( this ).val() !== null )
		{
			var client_id = $( this ).val();

			$.ajax({
				url: conf.site_url + 'clients/brief/' + client_id,
				success: function( response )
				{
					var response = $( response, true );
					response.find( "#brief-wrapper" ).appendTo( '#user-brief' );
					$( "#modal-search-client" ).find( '#flash-client-add' ).hide();
					$( "#modal-search-client" ).find( 'input' ).val( '' );
				}
			});
		}
	});

	/**
	 * SEARCH CLIENT DEFINE THE CLIENT ID IN THE MAIN FORM FOR SALE
	 */
	
	$( "button#confirm-client-button" ).on( 'click', function( ev ){
		ev.preventDefault();

		var assign_client = function( client_id )
		{
			var client_id = client_id || $( "select#modal_client_id" ).val();
			$.getJSON( conf.site_url + 'ajax/get_client/' + client_id, function( response, status ){
				if ( status == "success" && ! response.error )
				{
					var fullname = response.first_name;
					if ( response.last_name ) fullname += " " + response.last_name;

					$( "input#client_id" ).val( response.client_id );
					$( "input#name" ).val( fullname.trim() );
					$( "input#ruc" ).val( response.ruc );

					// In case there is no order, it means we can pick the seller from the client registry
					if ( $( "input#order_id" ).val() == "" ) $( "select#seller_id" ).val( response.seller_id ).trigger( 'change' );

					$( "#client-card .table tbody tr:nth-child(1) td" ).text( fullname.trim() );
					$( "#client-card .table tbody tr:nth-child(2) td" ).text( response.ruc );
				}
				else if ( response.error )
				{
					alert( response.error );
				}
			});
		};

		// If the client got selected from the dropdown, let's use that
		if ( $( "select#modal_client_id" ).val() != "" && $( "select#modal_client_id" ).val() != null )
		{
			assign_client();
			$( "#modal-search-client" ).modal( 'hide' );
		}
		else // Otherwise, let's store the new one, in case it exists
		{
			$.getJSON( conf.site_url + 'ajax/get_csrf_hash', function( response, status ) {
				if ( status == "success" )
				{
					var to_send 					= {};
						to_send.first_name 			= $( "#modal-search-client" ).find( "input#modal_client_first_name" ).val();
						to_send.last_name 			= $( "#modal-search-client" ).find( "input#modal_client_last_name" ).val();
						to_send.ruc 				= $( "#modal-search-client" ).find( "input#modal_client_ruc" ).val();
						to_send.cellphone 			= $( "#modal-search-client" ).find( "input#modal_client_cellphone" ).val();
						to_send.email 				= $( "#modal-search-client" ).find( "input#modal_client_email" ).val();
						to_send[response.csrf_name] = response.csrf_hash;

					$.ajax({
						url: conf.site_url + 'ajax/store_client',
						type: 'POST',
						data: to_send,
						success: function( response )
						{
							if ( response != null && response != "" && ! response.error )
							{
								assign_client( response.client_id );
								$( "#modal-search-client" ).modal( 'hide' );
							}
							else if ( response.error )
							{
								alert( response.error );
							}
						},
						error: function( response )
						{
							alert( response.error );
						},
						complete: rebuild_main_tokens,
					});
				}
			});
		}
	});
	
	/**
	 * SEARCH CLIENT USE CASUAL CLIENT
	 */
	
	$( "button#casual-client-button" ).on( 'click', function( ev ){
		ev.preventDefault();

		$( "#modal-search-client" ).modal( 'hide' );

		$( "input#ruc" ).val( conf.casual_client.ruc );
		$( "input#name" ).val( conf.casual_client.name );

		$( "#client-card .table tbody tr:nth-child(1) td" ).text( conf.casual_client.name );
		$( "#client-card .table tbody tr:nth-child(2) td" ).text( conf.casual_client.ruc );
	});

	/**
	 * RESET TABLE AND STORE FIRST ROW
	 */
	
	var clean_row = $( 'table.product-list tbody tr:first' ).clone();
	$( 'table.product-list tbody tr:first' ).remove();

	/**
	 * RESET BILL NUMBER BLOCK IF NO POS IS SELECTED
	 */
	
	if ( $( "select#pos_id" ).val() == "" && $( "input#bill_number" ).val() == "" )
	{
		$( "#pos-card .card-body" ).find( ".bill-number-row, .bill-number-tools" ).hide();
	}

	/**
	 * SET HIDDENS DEFAULTS AGAIN
	 */
	
	if ( $( "input#bill_number" ).val() != "" ) $( "#pos-card .card-body .bill-number-row .bill-number-output" ).text( $( "input#bill_number" ).val() );
	if ( $( "input#name" ).val() != "" ) $( "#client-card .table tbody tr:nth-child(1) td" ).text( $( "input#name" ).val() );
	if ( $( "input#ruc" ).val() != "" ) $( "#client-card .table tbody tr:nth-child(2) td" ).text( $( "input#ruc" ).val() );

	for ( index in conf.taxes )
	{
		if ( $( 'input#taxes_totals_' + index ).val() != "" ) $( "span.value-taxes-totals-" + index ).text( parseFloat( $( 'input#taxes_totals_' + index ).val() ).toCurrency( conf.decimals ) );
		if ( $( 'input#taxes_liquidation_' + index ).val() != "" ) $( "span.value-taxes-liquidation-" + index ).text( parseFloat( $( 'input#taxes_liquidation_' + index ).val() ).toCurrency( conf.decimals ) );
	}

	if ( $( 'input#taxes_liquidation_total' ).val() != "" ) $( "span.value-total_taxes" ).text( parseFloat( $( 'input#taxes_liquidation_total' ).val() ).toCurrency( conf.decimals ) );
	
	/**
	 * RESET DUE DATE BLOCK IF NO TYPE IS SELECTED, OR IF TYPE IS FULL
	 */

	if ( ! $( 'input[name="type"]' ).is( ':checked' ) || $( 'input[name="type"]:checked' ).val() == "full" )
	{
		$( "#date-card .card-body .due-date-row" ).hide();
	}

	/**
	 * LET'S STORE THE FIRST ROW OF THE TABLE IN CASE WE NEED TO CLONE IT
	 */

	$( 'input[name="type"]' ).on( 'change', function( ev ){
		ev.preventDefault();

		var selected = $( this ).val();

		if ( selected == "credit" )
		{
			$( "#date-card .card-body .due-date-row" ).slideDown();
		}
		else
		{
			$( "#date-card .card-body .due-date-row" ).slideUp();
		}
	});

	/**
	 * HANDLING THE ERROR WITH THE BILL NUMBER
	 */

	var handle_bill_error = function()
	{
		$( "span.bill-number-output" ).text( '' );
		$( "span.bill-number-output" ).removeProp( 'title' );
		$( "input#bill_number" ).val( '' );
		$( "#pos-card .card-body" ).find( ".bill-number-row, .bill-number-tools" ).slideUp();

		rebuild_tooltips();
	};

	/**
	 * GENERATE BILL NUMBER ON CHANGE OF POS
	 */
	
	var generate_bill_number = function( ev )
	{
		var pos_id 			= $( "select#pos_id" ).val();
		var print_order		= $( "input#print_order" ).val();
		var bill_number 	= $( "input#bill_number" ).val();
		var to_send 		= {};

		if ( ev && ev.type == "change" ) handle_bill_error();

		if ( pos_id != "" )
		{
			$.getJSON( conf.site_url + 'ajax/get_csrf_hash', function( response, status ) {
				if ( status == "success" )
				{
					to_send.pos_id 				= pos_id;
					to_send[response.csrf_name] = response.csrf_hash;
					
					if ( bill_number != "" ) to_send.bill_number = bill_number;
					if ( print_order != "" ) to_send.print_order = print_order;

					$.ajax({
						url: conf.site_url + 'ajax/generate_bill_number',
						type: 'POST',
						data: to_send,
						success: function( response )
						{
							if ( ! $.isEmptyObject( response ) )
							{
								if ( response.bill_number != "" && ! response.error )
								{
									$( "input#print_order" ).val( response.print_order );
									$( "span.bill-number-output" ).text( response.bill_number );
									$( "span.bill-number-output" ).prop( 'title', messages.print_order_number.format( response.print_order ) );
									$( "input#bill_number" ).val( response.bill_number );
									$( "#pos-card .card-body" ).find( ".bill-number-row" ).slideDown();
									
									// Show tools only if enabled
									if ( $.inArray( 'edit_bill_number', actions_enabled ) > -1 ) $( "#pos-card .card-body" ).find( ".bill-number-tools" ).slideDown();

									rebuild_tooltips();

									if ( response.bill_number_exists_message ) alert( response.bill_number_exists_message );
								}
								else if ( response.error )
								{
									alert( response.error );
									$( "select#pos_id" ).val( '' ).trigger( 'change' );
									handle_bill_error();
								} 
								else
								{
									$( "select#pos_id" ).val( '' ).trigger( 'change' );
									handle_bill_error();
								}								
							}
						},
						error: handle_bill_error,
						complete: rebuild_main_tokens,
					});
				}
			});
		}
	};
	
	$( "select#pos_id" ).on( 'change', generate_bill_number );

	// In case there's another sale already using the same bill number, let's check that and change accordingly
	// BUT---------------
	// Let's see if we can generate the final bill number just before submitting the form
	//setInterval( generate_bill_number, parseInt( 120 * 1000 ) );

	/**
	 * MOVE BILL NUMBER UP OR DOWN
	 */
	
	$( document ).on( 'click', '.bill-number-move', function( ev ){
		ev.preventDefault();

		if ( $.inArray( 'edit_bill_number', actions_enabled ) == -1 )
		{ 
			alert( messages.user_level_forbidden_action );
			return;
		}

		var move 			= null;
		var pos_id 			= $( "select#pos_id" ).val();
		var bill_number 	= $( "input#bill_number" ).val();
		var print_order 	= $( "input#print_order" ).val();
		var to_send 		= {};

		if ( $( this ).is( "#bill-number-up" ) )
		{
			move = 'up';
		}
		else if ( $( this ).is( "#bill-number-down" ) )
		{
			move = 'down';
		}

		if ( move != null && bill_number != "" )
		{
			$.getJSON( conf.site_url + 'ajax/get_csrf_hash', function( response, status ) {
				if ( status == "success" )
				{
					to_send.pos_id 				= pos_id;
					to_send[response.csrf_name] = response.csrf_hash;
					to_send.bill_number 		= bill_number;
					to_send.print_order 		= print_order;
					to_send.move 				= move;

					$.ajax({
						url: conf.site_url + 'ajax/generate_bill_number',
						type: 'POST',
						data: to_send,
						success: function( response )
						{
							if ( ! $.isEmptyObject( response ) )
							{
								if ( response.bill_number != "" && ! response.error )
								{
									$( "input#print_order" ).val( response.print_order );
									$( "span.bill-number-output" ).text( response.bill_number );
									$( "span.bill-number-output" ).prop( 'title', messages.print_order_number.format( response.print_order ) );
									$( "input#bill_number" ).val( response.bill_number );
									$( "#pos-card .card-body" ).find( ".bill-number-row" ).slideDown();

									// Show tools only if enabled
									if ( $.inArray( 'edit_bill_number', actions_enabled ) > -1 ) $( "#pos-card .card-body" ).find( ".bill-number-tools" ).slideDown();

									rebuild_tooltips();
								}
								else if ( response.error )
								{
									alert( response.error );
								} 
								else
								{
									$( "select#pos_id" ).val( '' ).trigger( 'change' );
									handle_bill_error();
								}								
							}
						},
						error: handle_bill_error,
						complete: rebuild_main_tokens,
					});
				}
			});			
		}
	});

	$( "#modal-set-quantity" ).modal({
		backdrop: 'static',
		keyboard: false,
		show: false,
	});

	/**
	 * PRODUCT SELECTION
	 */

	$( "select#product_selection" ).select2({
		language: 		conf.language_short,
		placeholder: 	$( "select#product_selection" ).attr( 'placeholder' ),
		ajax: {
			url: 		conf.site_url + 'ajax/search_stock',
			dataType: 	'json',
			delay: 		500,
			// Pass extra arguments to the query
			data: 		function( params ){
				params.pos_id = $( "select#pos_id" ).val();
				return params;
			},
			// In case POS is not selected, let's clear everything and alert
			beforeSend: function()
			{
				if ( $( "select#pos_id" ).val() == "" )
				{
					$( "select#product_selection" ).val( '' ).trigger( 'change' );
					$( "select#product_selection" ).select2( 'close' );
					alert( messages.select_pos_first );
				}
			},
		},
		templateResult: function( element, container )
		{
			if ( element.stock ) // Stocked product
			{
				var return_html  = '<div class="select2-option-wrapper d-flex">';
					return_html += '<div class="mr-2">';
					return_html += '<img class="select2-thumbnail" src="' + element.picture + '" /> ';
					return_html += '</div>';
					return_html += '<div class="tags">';
					return_html += '<strong>' + element.stock.name + '</strong> ';
					return_html += '<code>[' + element.stock.sku + ']</code>';
					return_html += '<br>';
					return_html += '<small>' + element.labels.quantity + parseFloat( element.stock.quantity ).toCurrency( conf.qty_decimals ) + ' ' + conf.units[element.stock.unit_measure] + '</small> | ';
					return_html += '<small>' + element.labels.price + conf.currencies[element.stock.currency] + ' ' + parseFloat( element.stock.unit_price ).toCurrency( conf.qty_decimals ) + '</small>';
					return_html += '</div>';
					return_html += '</div>';

				return $( return_html );
			}
			else if ( element.product ) // Sale-free product
			{
				var return_html  = '<div class="select2-option-wrapper d-flex">';
					return_html += '<div class="mr-2">';
					return_html += '<img class="select2-thumbnail" src="' + element.picture + '" /> ';
					return_html += '</div>';
					return_html += '<div class="tags">';
					return_html += '<strong>' + element.product.name + '</strong>';
					return_html += '<br>';
					return_html += '<small>' + element.labels.price + conf.currencies[element.product.currency] + ' ' + parseFloat( element.product.price ).toCurrency( conf.qty_decimals ) + '</small>';
					return_html += '</div>';
					return_html += '</div>';

				return $( return_html );
			}

			return element;
		},
		templateSelection: function( element )
		{
			if ( element.stock )
			{
				$( element.element ).attr({
					'data-type': 			'stock',
					'data-max-quantity': 	parseFloat( element.stock.quantity ),
					'data-picture': 		element.picture,
					'data-product-name': 	element.stock.name,
					'data-unit-measure': 	element.stock.unit_measure,
					'max': 					element.stock.quantity,
				});
			}
			else if ( element.product )
			{
				$( element.element ).attr({
					'data-type': 			'product',
					'data-max-quantity': 	0,
					'data-picture': 		element.picture,
					'data-product-name': 	element.product.name,
					'data-unit-measure': 	element.product.unit_measure,
				});
			}

			return element.text;
		}
	});

	$( "select#product_selection" ).on( 'select2:select', function( ev ){
		$( "#modal-set-quantity" ).modal( 'show' );
	});

	/**
	 * QUANTITY MODAL FEATURES
	 */
		
	$( "#modal-set-quantity" ).on( 'shown.bs.modal', function( ev ){
		var max_quantity 	= parseFloat( $( "select#product_selection" ).find( 'option:selected' ).data( 'max-quantity' ) );
		var picture 		= $( "select#product_selection" ).find( 'option:selected' ).data( 'picture' );
		var product_name 	= $( "select#product_selection" ).find( 'option:selected' ).data( 'product-name' );
		var unit_measure 	= $( "select#product_selection" ).find( 'option:selected' ).data( 'unit-measure' );

		$( "#modal-set-quantity" ).find( ".modal-body h5" ).text( product_name );
		$( "#modal-set-quantity" ).find( ".product-picture" ).html( '<img src="' + picture + '" class="picture-big w-100" />' );
		$( "#modal-set-quantity" ).find( ".input-group-text" ).text( conf.units[unit_measure] );

		// Set max quantity to the box
		$( "#modal-set-quantity" ).find( "input#modal_quantity" ).inputmask( "remove" );
		$( "#modal-set-quantity" ).find( "input#modal_quantity" ).inputmask( input_options.quantity );
		$( "#modal-set-quantity" ).find( "input#modal_quantity" ).inputmask( 'setvalue', '1' );
		if ( max_quantity > 0 ) $( "#modal-set-quantity" ).find( "input#modal_quantity" ).inputmask( "option", { max: max_quantity });

		// Events on the field
		$( "#modal-set-quantity" ).find( "button#confirm-set-quantity" ).on( 'click', set_primal_quantity );
		$( "#modal-set-quantity" ).find( "input#modal_quantity" ).on( 'keyup', function( ev ){  if ( ev.keyCode == "13" ) set_primal_quantity( ev ); });

		$( "#modal-set-quantity" ).find( "input#modal_quantity" ).focus().select(); // Focus on the input on shown
	});

	$( "#modal-set-quantity" ).on( 'hidden.bs.modal', function( ev ){
		$( "#modal-set-quantity" ).find( ".modal-body h5" ).text( '' );
		$( "#modal-set-quantity" ).find( ".product-picture" ).html( '' );
		$( "#modal-set-quantity" ).find( ".input-group-text" ).text( '' );
		$( "#modal-set-quantity" ).find( "input#modal_quantity" ).inputmask( 'setvalue', '' );
	});

	/**
	 * PRIMAL QUANTITY FUNCTION
	 */

	set_primal_quantity = function( ev )
	{
		ev.stopImmediatePropagation(); // Just provoke the event once

		var qty 		= parseFloat( $( "#modal-set-quantity" ).find( "input#modal_quantity" ).inputmask( 'unmaskedvalue' ) );
		var sku 		= $( "select#product_selection" ).val();
		var option_type = $( "select#product_selection" ).find( 'option:selected' ).data( 'type' );

		if ( isNaN( qty ) )
		{
			alert( messages.quantity_invalid );
		}
		else if ( qty <= 0 )
		{
			alert( messages.quantity_invalid );
		}
		else
		{
			$.getJSON( conf.site_url + 'ajax/stock_retrieve/' + sku + "?type=" + option_type, function( response, status ) {
				if ( status == "success" && response )
				{
					// First iterate the product list in case we already have a row with the same product, stock or free sell
					var existant_row = null;
					
					$( "table.product-list tbody tr" ).each( function( ind, ele ){
						if ( option_type == "stock" )
						{
							if ( response.stock.stock_id == $( this ).find( 'input.input-stock-id' ).val() )
							{
								existant_row = $( this );
							}
						}
						else if ( option_type == "product" )
						{
							if ( response.product.product_id == $( this ).find( 'input.input-product-id' ).val() )
							{
								existant_row = $( this );
							}
						}
					});

					if ( existant_row !== null ) // row exists
					{
						// Add the new quantity to the existant quantity before and if it exceeds, alert and don't allow to replace the thing.
						var existant_quantity = parseFloat( existant_row.find( 'input.input-quantity' ).val() );
						var new_quantity = qty + existant_quantity;

						if ( option_type == "stock" ) // ... obviously, only if it ain't a free-to-sale item
						{
							if ( new_quantity > response.stock.quantity )
							{
								alert( messages.quantity_surpased.format( parseFloat( response.stock.quantity ).toCurrency( conf.qty_decimals ), conf.units[response.product.unit_measure] ) );
							}
							else
							{
								existant_row.find( 'input.input-quantity' ).val( new_quantity );
								existant_row.find( 'input.input-row-total' ).val( parseFloat( existant_row.find( 'input.input-unit-price' ).val() ) * new_quantity );
							}
						}
						else
						{
							existant_row.find( 'input.input-quantity' ).val( new_quantity );
							existant_row.find( 'input.input-row-total' ).val( parseFloat( existant_row.find( 'input.input-unit-price' ).val() ) * new_quantity );
						}

						existant_row.find( 'td.cell-quantity' ).html( parseFloat( existant_row.find( 'input.input-quantity' ).val() ).toCurrency( conf.qty_decimals ) + ' <small class="text-muted">' + conf.units[response.product.unit_measure] + '</small>' );
						existant_row.find( 'td.cell-row-total' ).text( parseFloat( existant_row.find( 'input.input-row-total' ).val() ).toCurrency( conf.decimals ) );
					}
					else // row must be new
					{
						if ( option_type == "stock" && qty > parseFloat( response.stock.quantity ) )
						{
							alert( messages.quantity_surpased.format( parseFloat( response.stock.quantity ).toCurrency( conf.qty_decimals ), conf.units[response.product.unit_measure] ) );
						}
						else
						{
							var new_row = clean_row.clone();

							new_row.find( 'input.input-description' ).val( response.product.name );
							new_row.find( 'input.input-tax' ).val( response.product.tax );
							new_row.find( 'input.input-quantity' ).val( qty );
							new_row.find( 'input.input-product-id' ).val( response.product.product_id );

							var title = conf.taxes[response.product.tax];

							if ( ! response.stock ) // in this case is only product, not stocked
							{
								new_row.find( 'input.input-unit-price' ).val( exchange_to_main_currency( response.product.price, response.product.currency ) );
								new_row.find( 'input.input-row-total' ).val( exchange_to_main_currency( response.product.price, response.product.currency ) * qty );
							}
							else
							{
								new_row.find( 'input.input-stock-id' ).val( response.stock.stock_id );
								new_row.find( 'input.input-sku' ).val( response.stock.sku );
								new_row.find( 'input.input-unit-price' ).val( exchange_to_main_currency( response.stock.unit_price, response.product.currency ) );
								new_row.find( 'input.input-row-total' ).val( exchange_to_main_currency( response.stock.unit_price, response.product.currency ) * qty );

								title += ' | ' + messages.sku_label + ': ' + response.stock.sku;
							}

							new_row.find( 'td.cell-description' ).html( '<span title="' + title + '">' + new_row.find( 'input.input-description' ).val() + '</span>' );
							new_row.find( 'td.cell-price' ).text( parseFloat( new_row.find( 'input.input-unit-price' ).val() ).toCurrency( conf.decimals ) );
							new_row.find( 'td.cell-quantity' ).html( parseFloat( new_row.find( 'input.input-quantity' ).val() ).toCurrency( conf.qty_decimals ) + ' <small class="text-muted">' + conf.units[response.product.unit_measure] + '</small>' );
							new_row.find( 'td.cell-row-total' ).text( parseFloat( new_row.find( 'input.input-row-total' ).val() ).toCurrency( conf.decimals ) );
							new_row.insertBefore( "table.product-list tbody .total-amount-row" );
						}
					}

					$( "select#product_selection" ).val( '' ).trigger( 'change' );

					rebuild_tooltips();
					calculate_total();
					repaint_order_rows();
				}
			});

			$( "#modal-set-quantity" ).modal( 'hide' );
		}
	};

	/**
	 * CALCULATE TOTAL
	 */

	function calculate_total()
	{
		var total = 0;

		if ( $( "table.product-list tbody tr" ).length > 0 )
		{
			$( '.input-row-total' ).each(function(){
				total += parseFloat( $( this ).val() );
			});
		}
		else
		{
			total = 0;
		}

		if ( isNaN( total ) || total == 0 || total.length <= 0 )
		{ 
			total = '';
		}
		else
		{
			total = total.toCurrency( conf.decimals );
		}

		$( "input#total_amount" ).val( total );
		$( "#modal-finish-sale" ).find( "input#meta_sale_total" ).val( total );

		calculate_taxes();
	}

	/**
	 * CALCULATE TAXES
	 */
	
	function calculate_taxes()
	{
		if ( $( "input#total_amount" ).val() != "" && parseFloat( $( "input#total_amount" ).val() ) > 0 )
		{
			var tax_total = 0;

			// Set tax totals to zero
			$.each( conf.taxes, function( ind, val ){
				$( "input#taxes_totals_" + ind ).val( '0' );
			});

			// Go thru the rows and add it to the input of every tax column
			$( "table.product-list tbody tr" ).each( function( ind, ele ){
				var product_tax 		= $( this ).find( '.input-tax' ).val();
				var prev_value 			= ( $( "input#taxes_totals_" + product_tax ).val() != 0 ) ? parseFloat( $( "input#taxes_totals_" + product_tax ).val() ) : 0;
				var product_row_total 	= parseFloat( $( this ).find( ".input-row-total" ).val() );
				$( "input#taxes_totals_" + product_tax ).val( parseFloat( prev_value + product_row_total ).toFixed( conf.decimals ) );
			});

			// Calculate every total and output
			$.each( conf.taxes, function( ind, val ){
				if ( $( "input#taxes_totals_" + ind ).val() != "" && parseFloat( $( "input#taxes_totals_" + ind ).val() ) > 0 )
				{
					// Do the math
					var the_value 		= parseFloat( $( "input#taxes_totals_" + ind ).val() );
					var the_multiplier 	= parseFloat( conf.tax_multipliers[ind] );
					var the_tax 		= parseFloat( the_value * the_multiplier );

					// Set the values of taxes liquidations
					$( "input#taxes_liquidation_" + ind ).val( the_tax.toFixed( conf.decimals ) );
					
					// Add tax to total tax
					tax_total += the_tax;
					
					// Set the text of span values
					$( "span.value-taxes-totals-" + ind ).text( the_value.toCurrency( conf.decimals ) );
					$( "span.value-taxes-liquidation-" + ind ).text( the_tax.toCurrency( conf.decimals ) );

					if ( the_tax == 0 ) $( "span.value-taxes-liquidation-" + ind ).html( '&mdash;' );
				}
				else
				{
					$( "input#taxes_totals_" + ind ).val( '0' );
					$( "input#taxes_liquidation_" + ind ).val( '0' );

					$( "span.value-taxes-totals-" + ind ).html( '&mdash;' );
					$( "span.value-taxes-liquidation-" + ind ).html( '&mdash;' );
				}
			});

			// Set total of taxes
			if ( tax_total > 0 )
			{
				$( "input#taxes_liquidation_total" ).val( parseFloat( tax_total ).toFixed( conf.decimals ) );
				$( "span.value-total_taxes" ).text( parseFloat( tax_total ).toCurrency( conf.decimals ) );
			}
			else
			{
				$( "input#taxes_liquidation_total" ).val( '0' );
				$( "span.value-total_taxes" ).html( '&mdash;' );				
			}
		}
		else
		{
			$( 'input[name^="taxes"]' ).val( '0' );
			$( 'span[class^="value-"]' ).html( '&mdash;' );
		}
	}

	/**
	 * REMOVE ITEM FROM THE LIST
	 */
	
	$( document ).on( 'click', '.btn-remove-product', function( ev ){
		ev.preventDefault();

		var parent_row = $( this ).parent().parent();
		var total = 0;

		// Now, remove the row
		parent_row.remove();
		rebuild_tooltips();

		// Add everything to total
		calculate_total();

		// Clear classes from order row (in case there is any)
		repaint_order_rows();
	});

	/**
	 * ---------------------------------------------------------------------------
	 * STUFF FOR ADD
	 * ---------------------------------------------------------------------------
	 */
	
	if ( $( 'body.sales-add, body[class*="sales-add-"]' ).length > 0 )
	{
		calculate_total();
	}

	/**
	 * ---------------------------------------------------------------------------
	 * STUFF FOR EDIT
	 * ---------------------------------------------------------------------------
	 */

	if ( $( 'body[class*="sales-edit-"]' ).length > 0 )
	{
		// Init the finish modal vars
		if ( $( "input#total_amount" ).val() != "" ) $( "#modal-finish-sale" ).find( "input#meta_sale_total" ).val( $( "input#total_amount" ).val() );
	}

	/**
	 * ---------------------------------------------------------------------------
	 * EDIT THE PRICE OF EVERY PRODUCT
	 * ---------------------------------------------------------------------------
	 */

	$( document ).on( 'dblclick', "td.cell-price", function( ev ){
		if ( $.inArray( 'edit_price', actions_enabled ) > -1 )
		{
			var row 		= $( this ).parent();
			var product_id 	= row.find( 'input.input-product-id' ).val();

			$.getJSON( conf.site_url + 'ajax/product_retrieve/' + product_id, function( product, status ) {
				if ( status == "success" && product )
				{
					// Set original magnitude on field
					$( "#modal-edit-magnitude-price" ).find( 'input#modal_edit_magnitude_price' ).inputmask( 'setvalue', parseFloat( row.find( 'input.input-unit-price' ).val() ) );

					// Set the product vars on their places
					$( "#modal-edit-magnitude-price" ).find( ".modal-body h5" ).text( product.name );
					$( "#modal-edit-magnitude-price" ).find( ".product-picture" ).html( '<img src="' + product.picture + '" class="picture-big w-100" />' );
					$( "#modal-edit-magnitude-price" ).find( ".input-group-text" ).text( conf.currencies[conf.currency] ); // on editing, we're already using the system currency

					// Hide modal event, and clean
					$( "#modal-edit-magnitude-price" ).on( 'hidden.bs.modal', function( ev ){
						$( "#modal-edit-magnitude-price" ).find( ".modal-body h5" ).text( '' );
						$( "#modal-edit-magnitude-price" ).find( ".product-picture" ).html( '' );
						$( "#modal-edit-magnitude-price" ).find( ".input-group-text" ).text( '' );
						$( "#modal-edit-magnitude-price" ).find( "input#modal_edit_magnitude_price" ).removeData();
						$( "#modal-edit-magnitude-price" ).find( "input#modal_edit_magnitude_price" ).inputmask( 'setvalue', '' );
					});

					// Focus on the field on shown
					$( "#modal-edit-magnitude-price" ).on( 'shown.bs.modal', function( ev ){
						$( "#modal-edit-magnitude-price" ).find( 'input#modal_edit_magnitude_price' ).focus().select();
					});

					// Events to overwrite
					$( "#modal-edit-magnitude-price" ).find( "button#confirm-edit-magnitude-price" ).on( 'click', function( ev ){
						var value 		= $( "#modal-edit-magnitude-price" ).find( 'input#modal_edit_magnitude_price' ).inputmask( 'unmaskedvalue' );
						var quantity 	= parseFloat( row.find( "input.input-quantity" ).val() );

						if ( isNaN( value ) || value <= 0 )
						{
							alert( messages.price_invalid );
						}
						else
						{
							row.find( "input.input-unit-price" ).val( value );
							row.find( "input.input-row-total" ).val( value * quantity );

							row.find( 'td.cell-price' ).text( parseFloat( row.find( "input.input-unit-price" ).val() ).toCurrency( conf.decimals ) );
							row.find( 'td.cell-row-total' ).text( parseFloat( row.find( "input.input-row-total" ).val() ).toCurrency( conf.decimals ) );

							$( "#modal-edit-magnitude-price" ).find( "button#confirm-edit-magnitude-price" ).off( 'click' );
							$( "#modal-edit-magnitude-price" ).modal( 'hide' );
							calculate_total();
							repaint_order_rows();
						}						
					});
					$( "#modal-edit-magnitude-price" ).find( 'input#modal_edit_magnitude_price' ).on( 'keyup', function( ev ){ if ( ev.keyCode == "13" ) $( "#modal-edit-magnitude-price" ).find( "button#confirm-edit-magnitude-price" ).trigger( 'click' ); });

					// Show the modal
					$( "#modal-edit-magnitude-price" ).modal( 'show' );
				}
			});
		}
		else
		{
			alert( messages.user_level_forbidden_action );
		}
	});

	/**
	 * ---------------------------------------------------------------------------
	 * EDIT THE QUANTITY OF EVERY PRODUCT
	 * ---------------------------------------------------------------------------
	 */
	
	$( document ).on( 'dblclick', "td.cell-quantity", function( ev ){
		if ( $.inArray( 'edit_quantity', actions_enabled ) > -1 )
		{
			var row 		= $( this ).parent();
			var sku			= row.find( "input.input-sku" ).val();
			var product_id	= row.find( "input.input-product-id" ).val();
			var type 		= ( sku != "" ) ? 'stock' : 'product';

			if ( type == "product" ) sku = product_id;

			$.getJSON( conf.site_url + 'ajax/stock_retrieve/' + sku + "?type=" + type, function( product, status ) {
				if ( status == "success" && product )
				{
					$( "#modal-edit-magnitude-quantity" ).find( 'input#modal_edit_magnitude_quantity' ).inputmask( 'remove' );
					$( "#modal-edit-magnitude-quantity" ).find( 'input#modal_edit_magnitude_quantity' ).inputmask( input_options.quantity );

					if ( type == "product" )
					{
						$( "#modal-edit-magnitude-quantity" ).find( 'input#modal_edit_magnitude_quantity' ).attr({ 
							'data-max-quantity': 0,
							'data-unit-measure': product.product.unit_measure,
						});
					}
					else if ( type == "stock" )
					{
						$( "#modal-edit-magnitude-quantity" ).find( 'input#modal_edit_magnitude_quantity' ).attr({ 
							'data-max-quantity': product.stock.quantity,
							'data-unit-measure': product.product.unit_measure,
						});

						// Add max quantity limiter in case we're dealing with a stocked product
						$( "#modal-edit-magnitude-quantity" ).find( 'input#modal_edit_magnitude_quantity' ).inputmask( 'option', { max: parseFloat( product.stock.quantity ) } );
					}

					// Set original magnitude on field
					$( "#modal-edit-magnitude-quantity" ).find( 'input#modal_edit_magnitude_quantity' ).inputmask( 'setvalue', parseFloat( row.find( 'input.input-quantity' ).val() ) );

					// Set the product vars on their places
					$( "#modal-edit-magnitude-quantity" ).find( ".modal-body h5" ).text( product.product.name );
					$( "#modal-edit-magnitude-quantity" ).find( ".product-picture" ).html( '<img src="' + product.product.picture + '" class="picture-big w-100" />' );
					$( "#modal-edit-magnitude-quantity" ).find( ".input-group-text" ).text( conf.units[product.product.unit_measure] );

					// Focus on the field on shown
					$( "#modal-edit-magnitude-quantity" ).on( 'shown.bs.modal', function( ev ){
						$( "#modal-edit-magnitude-quantity" ).find( 'input#modal_edit_magnitude_quantity' ).focus().select();
					});

					// Hide modal event, and clean
					$( "#modal-edit-magnitude-quantity" ).on( 'hidden.bs.modal', function( ev ){
						$( "#modal-edit-magnitude-quantity" ).find( ".modal-body h5" ).text( '' );
						$( "#modal-edit-magnitude-quantity" ).find( ".product-picture" ).html( '' );
						$( "#modal-edit-magnitude-quantity" ).find( ".input-group-text" ).text( '' );
						$( "#modal-edit-magnitude-quantity" ).find( "input#modal_edit_magnitude_quantity" ).removeData();
						$( "#modal-edit-magnitude-quantity" ).find( "input#modal_edit_magnitude_quantity" ).inputmask( 'setvalue', '' );
					});

					// Events to overwrite
					$( "#modal-edit-magnitude-quantity" ).find( "button#confirm-edit-magnitude-quantity" ).on( 'click', function( ev ){
						var unit_measure 	= $( "#modal-edit-magnitude-quantity" ).find( 'input#modal_edit_magnitude_quantity' ).data( 'unit-measure' );
						var value			= parseFloat( $( "#modal-edit-magnitude-quantity" ).find( 'input#modal_edit_magnitude_quantity' ).inputmask( 'unmaskedvalue' ) );
						var max_qty 		= parseFloat( $( "#modal-edit-magnitude-quantity" ).find( 'input#modal_edit_magnitude_quantity' ).data( 'max-quantity' ) );
						var price 			= parseFloat( row.find( "input.input-unit-price" ).val() );

						if ( isNaN( value ) || value <= 0 )
						{
							alert( messages.quantity_invalid );
						}
						else if ( value > max_qty && max_qty > 0 )
						{
							alert( messages.quantity_surpased.format( max_qty.toCurrency( conf.qty_decimals ), conf.units[unit_measure] ) );
						}
						else
						{
							row.find( "input.input-quantity" ).val( value );
							row.find( "input.input-row-total" ).val( value * price );

							row.find( 'td.cell-quantity' ).html( parseFloat( row.find( "input.input-quantity" ).val() ).toCurrency( conf.qty_decimals ) + ' <small class="text-muted">' + conf.units[unit_measure] + '</small>' );
							row.find( 'td.cell-row-total' ).text( parseFloat( row.find( "input.input-row-total" ).val() ).toCurrency( conf.decimals ) );

							calculate_total();
							repaint_order_rows();

							$( "#modal-edit-magnitude-quantity" ).find( "button#confirm-edit-magnitude-quantity" ).off( 'click' );
							$( "#modal-edit-magnitude-quantity" ).modal( 'hide' );
							ev.stopImmediatePropagation(); // Just do it once
						}
					});

					$( "#modal-edit-magnitude-quantity" ).find( 'input#modal_edit_magnitude_quantity' ).on( 'keyup', function( ev ){ 
						if ( ev.keyCode == "13" ) $( "#modal-edit-magnitude-quantity" ).find( "button#confirm-edit-magnitude-quantity" ).trigger( 'click' ); 
					});

					// Show the modal
					$( "#modal-edit-magnitude-quantity" ).modal( 'show' );
				}
			});
		}
		else
		{
			alert( messages.user_level_forbidden_action );
		}
	});

	/**
	 * CHECK IF SKU IS ALREADY PRESENT ON THE SALE LIST
	 */
	
	var product_id_already_present = function( product_id )
	{
		var response = false;

		if ( $( "table.product-list tbody tr:not(.total-amount-row)" ).length > 0 )
		{
			$( "table.product-list tbody tr:not(.total-amount-row)" ).each( function(){
				if ( $( this ).find( ".input-product-id" ).val() == product_id ) response = true;
			});
		}

		return response;
	}

	/**
	 * CLEAR THE STATUS OF THE PRODUCT ORDER WHEN DELETING IT FROM THE SALE LIST.
	 * ALSO RESTORES THE REMAINING QUANTITY WHERE IT BELONGS.
	 */
	
	var repaint_order_rows = function()
	{
		if ( $( "#order-card" ).length > 0 )
		{
			$( 'table.order-list tbody tr' ).each( function( indA, order_row ){
				var product_id 					= $( order_row ).find( "input.input-order-product-id" ).val();
				var unit_measure 				= $( order_row ).find( "input.input-order-unit-measure" ).val();
				var quantity 					= parseFloat( $( order_row ).find( "input.input-order-quantity" ).val() );
				var quantity_remaining 			= parseFloat( $( order_row ).find( "input.input-order-quantity-remaining" ).val() );
				var quantity_remaining_original = parseFloat( $( order_row ).find( "input.input-order-quantity-remaining" ).data( 'original-quantity' ) );
				var quantity_filled			 	= 0;

				if ( ! $( order_row ).hasClass( 'table-danger' ) ) $( order_row ).removeClass(); //remove the class from the order row for default

				if ( $( 'table.product-list tbody tr:not(.total-amount-row)' ).length > 0 )
				{
					$( 'table.product-list tbody tr:not(.total-amount-row)' ).each( function( indB, sale_row ){
						if ( product_id == $( sale_row ).find( "input.input-product-id" ).val() )
						{
							quantity_filled = quantity_filled + parseFloat( $( sale_row ).find( "input.input-quantity" ).val() );
						}
					});
				}

				if ( quantity_filled <= 0 )
				{
					quantity_remaining = quantity_remaining_original;
				}
				else if ( quantity_filled > 0 && quantity_filled < quantity_remaining_original )
				{
					quantity_remaining = quantity_remaining_original - quantity_filled;
					$( order_row ).removeClass().addClass( 'table-warning' );
				}
				else if ( quantity_filled >= quantity_remaining_original )
				{
					quantity_remaining = 0;
					$( order_row ).removeClass().addClass( 'table-success' );
				}

				$( order_row ).find( "input.input-order-quantity-remaining" ).val( quantity_remaining );
				$( order_row ).find( "td.order-cell-remaining" ).html( quantity_remaining.toCurrency( conf.qty_decimals ) + ' <small class="text-muted">' + conf.units[unit_measure] + '</small>' );
			});
		}
	}

	repaint_order_rows();

	/**
	 * ---------------------------------------------------------------------------
	 * STUFF FOR ORDER
	 * ---------------------------------------------------------------------------
	 */
	
	if ( $( "#order-card" ).length > 0 )
	{
		$( document ).on( 'click', '.btn-add-to-sales', function( ev ){
			ev.preventDefault();

			var row 				= $( this ).parent().parent();
			var row_id 				= row.prop( 'id' );
			var product_id			= row.find( "input.input-order-product-id" ).val();
			var unit_price			= parseFloat( row.find( "input.input-order-unit-price" ).val() );
			var quantity_remaining 	= parseFloat( row.find( "input.input-order-quantity-remaining" ).val() );
			var product_free 		= row.find( "input.input-order-product-free" ).val();
			var rows_to_add 		= [];

			if ( $( "select#pos_id" ).val() == "" )
			{
				alert( messages.select_pos_first );
				return;
			}

			if ( product_id_already_present( product_id ) == true )
			{
				alert( messages.product_id_already_present );
				return;
			}

			$.ajax({
				url: 		conf.site_url + 'ajax/search_stock',
				type: 		'GET',
				dataType: 	'json',
				data: {
					pos_id: $( "select#pos_id" ).val(),
					product: true,
					term: product_id,
				},
				success: function( response, status )
				{
					if ( status == "success" && response.results.length > 0 )
					{
						//row.removeClass();

						if ( ! $.isEmptyObject( response.results ) )
						{
							var filled = parseFloat( 0 );

							$.each( response.results, function( i, stock_row ){
								if ( stock_row.stock )
								{
									var stock_quantity = parseFloat( stock_row.stock.quantity );

									if ( stock_quantity >= quantity_remaining )
									{
										filled = filled + quantity_remaining;
										quantity_remaining = quantity_remaining - quantity_remaining;
									}
									else if ( stock_quantity < quantity_remaining )
									{
										filled = filled + stock_quantity;
										quantity_remaining = quantity_remaining - stock_quantity;
									}

									rows_to_add.push({
										'sku': 			stock_row.stock.sku,
										'stock_id': 	stock_row.stock.stock_id,
										'product_id': 	stock_row.stock.product_id,
										'description': 	stock_row.stock.name,
										'unit_price': 	unit_price,
										'quantity': 	filled,
										'row_total': 	parseFloat( unit_price * filled ),
										'unit_measure': stock_row.stock.unit_measure,
										'tax': 			stock_row.stock.tax,
									});

									if ( quantity_remaining <= 0 ) return false;
								}
								else if ( stock_row.product )
								{
									if ( stock_row.product.free == "1" && quantity_remaining > 0 )
									{
										rows_to_add.push({
											'sku': 			'',
											'stock_id': 	'',
											'product_id': 	stock_row.product.product_id,
											'description': 	stock_row.product.name,
											'unit_price': 	unit_price,
											'quantity': 	quantity_remaining,
											'row_total': 	parseFloat( unit_price * quantity_remaining ),
											'unit_measure': stock_row.product.unit_measure,
											'tax': 			stock_row.product.tax,
										});

										quantity_remaining = 0;
									}

									return false;
								}
							});
						}

						// Add rows here
						if ( rows_to_add.length > 0 )
						{
							for ( var index in rows_to_add )
							{
								var new_row = clean_row.clone();
								var title 	= conf.taxes[rows_to_add[index].tax];

								new_row.find( 'input.input-description' ).val( rows_to_add[index].description );
								new_row.find( 'input.input-tax' ).val( rows_to_add[index].tax );
								new_row.find( 'input.input-quantity' ).val( rows_to_add[index].quantity );
								new_row.find( 'input.input-product-id' ).val( rows_to_add[index].product_id );
								new_row.find( 'input.input-unit-price' ).val( rows_to_add[index].unit_price );
								new_row.find( 'input.input-row-total' ).val( rows_to_add[index].row_total );

								if ( rows_to_add[index].stock_id ) // in this case is only product, not stocked
								{
									new_row.find( 'input.input-stock-id' ).val( rows_to_add[index].stock_id );
									new_row.find( 'input.input-sku' ).val( rows_to_add[index].sku );

									title += ' | ' + messages.sku_label + ': ' + rows_to_add[index].sku;
								}

								new_row.find( 'td.cell-description' ).html( '<span title="' + title + '">' + new_row.find( 'input.input-description' ).val() + '</span>' );
								new_row.find( 'td.cell-price' ).text( parseFloat( new_row.find( 'input.input-unit-price' ).val() ).toCurrency( conf.decimals ) );
								new_row.find( 'td.cell-quantity' ).html( parseFloat( new_row.find( 'input.input-quantity' ).val() ).toCurrency( conf.qty_decimals ) + ' <small class="text-muted">' + conf.units[rows_to_add[index].unit_measure] + '</small>' );
								new_row.find( 'td.cell-row-total' ).text( parseFloat( new_row.find( 'input.input-row-total' ).val() ).toCurrency( conf.decimals ) );
								new_row.insertBefore( "table.product-list tbody .total-amount-row" );
							}

							// And subtract from the remaining in the order list
							row.find( "td.order-cell-remaining" ).html( quantity_remaining.toCurrency( conf.qty_decimals ) + ' <small class="text-muted">' + conf.units[rows_to_add[0].unit_measure] + '</small>' );
							row.find( "input.input-order-quantity-remaining" ).val( quantity_remaining );

							// Do total calcs
							rebuild_tooltips();
							calculate_total();
							repaint_order_rows();
						}
					}
					else
					{
						alert( messages.product_not_enough_for_sale.format( 
							conf.units[row.find( '.input-order-unit-measure' ).val()], 
							'<strong>' + row.find( '.input-order-description' ).val()  + '</strong>'
						) );

						row.addClass( 'table-danger' );
					}
				}
			});
		});
	}

	/**
	 * ---------------------------------------------------------------------------
	 * SEND THE FORM AND CHECK THE BILL NUMBER HASN'T BEEN USED
	 * ---------------------------------------------------------------------------
	 */

	$( "button#save-sale" ).on( 'click', function( ev ){
		ev.preventDefault();

		var pos_id 			= $( "select#pos_id" ).val();
		var print_order		= $( "input#print_order" ).val();
		var bill_number 	= $( "input#bill_number" ).val();
		var sale_id			= ( $( "input#sale_id" ).length > 0 ) ? $( "input#sale_id" ).val() : "";
		var to_send 		= {};

		$.getJSON( conf.site_url + 'ajax/get_csrf_hash', function( response, status ) {
			if ( status == "success" )
			{
				to_send[response.csrf_name] = response.csrf_hash;

				if ( pos_id != "" )			to_send.pos_id		= pos_id;
				if ( bill_number != "" )	to_send.bill_number = bill_number;
				if ( print_order != "" )	to_send.print_order = print_order;
				if ( sale_id != "" )		to_send.sale_id 	= sale_id;

				$.ajax({
					url: conf.site_url + 'ajax/generate_bill_number',
					type: 'POST',
					data: to_send,
					success: function( response )
					{
						if ( response.bill_number_exists_message ) 
						{
							confirm( response.bill_number_exists_message, function( boolean ){
								if ( boolean )
								{
									$( "input#print_order" ).val( response.print_order );
									$( "span.bill-number-output" ).text( response.bill_number );
									$( "input#bill_number" ).val( response.bill_number );

									$.getJSON( conf.site_url + 'ajax/get_csrf_hash', function( response, status ) {
										if ( status == "success" )
										{
											$( "form#form-sales" ).find( 'input[name="' + response.csrf_name + '"]' ).val( response.csrf_hash );
											$( "form#form-sales" ).submit();
										}
									});
								}
							});
						}
						else
						{
							$.getJSON( conf.site_url + 'ajax/get_csrf_hash', function( response, status ) {
								if ( status == "success" )
								{
									$( "form#form-sales" ).find( 'input[name="' + response.csrf_name + '"]' ).val( response.csrf_hash );
									$( "form#form-sales" ).submit();
								}
							});
						}
					},
					complete: rebuild_main_tokens,
				});
			}
		});
	});

	/**
	 * EXCHANGE CALCULATION
	 */

	var picked_foreign_currency = null;
	var delayer_time = 500;

	/**
	 * CALCULATES THE FOREIGN VARS
	 */

	var calculate_foreign_vars = function()
	{
		delayer( function(){
			if ( typeof picked_foreign_currency != null )
			{
				var sale_total 		= parseFloat( $( "#modal-finish-sale" ).find( "input#meta_sale_total" ).inputmask( 'unmaskedvalue' ) );
				var amount_given 	= parseFloat( $( "#modal-finish-sale" ).find( "input#meta_vars_amount_given" ).inputmask( 'unmaskedvalue' ) );
				var change 			= parseFloat( $( "#modal-finish-sale" ).find( "input#meta_vars_change" ).inputmask( 'unmaskedvalue' ) );

				if ( sale_total > 0 && amount_given > 0 )
				{
					var foreign_sale_total 		= parseFloat( sale_total / parseFloat( exchange_values[picked_foreign_currency].sale ) );
					var foreign_amount_given 	= parseFloat( amount_given / parseFloat( exchange_values[picked_foreign_currency].sale ) );
					var foreign_change 			= parseFloat( change / parseFloat( exchange_values[picked_foreign_currency].sale ) );

					$( "#modal-finish-sale" ).find( "input#meta_sale_total_foreign" ).inputmask( 'setvalue', foreign_sale_total );
					$( "#modal-finish-sale" ).find( "input#meta_amount_given_foreign" ).inputmask( 'setvalue', foreign_amount_given );
					$( "#modal-finish-sale" ).find( "input#meta_change_foreign" ).inputmask( 'setvalue', foreign_change );
				}
			}			
		}, delayer_time );
	}

	/**
	 * CALCULATE THE CHANGE
	 */

	var calculate_change = function()
	{
		var sale_total 		= parseFloat( $( "#modal-finish-sale" ).find( "input#meta_sale_total" ).inputmask( 'unmaskedvalue' ) );
		var amount_given 	= parseFloat( $( "#modal-finish-sale" ).find( "input#meta_vars_amount_given" ).inputmask( 'unmaskedvalue' ) );
		var change 			= 0;

		if ( sale_total > 0 && amount_given > sale_total ) change = parseFloat( amount_given - sale_total );

		$( "#modal-finish-sale" ).find( "input#meta_vars_change" ).inputmask( 'setvalue', change );
	}

	/**
	 * CALCULATE THE FOREIGN EXCHANGE AND RE DO THE AMOUNT GIVEN ACCORDING TO THE FOREIGN CURRENCY VARS
	 */

	var calculate_foreign_change = function()
	{
		delayer( function(){
			if ( typeof picked_foreign_currency != null )
			{
				var amount_given_foreign	= parseFloat( $( "#modal-finish-sale" ).find( "input#meta_amount_given_foreign" ).inputmask( 'unmaskedvalue' ) );
				var new_amount_given		= parseFloat( amount_given_foreign * parseFloat( exchange_values[picked_foreign_currency].sale ) );

				$( "#modal-finish-sale" ).find( "input#meta_vars_amount_given" ).inputmask( 'setvalue', new_amount_given );

				calculate_foreign_vars();
				calculate_change();
			}
		}, delayer_time );
	}

	/**
	 * FINISH SALE - MODAL EVENTS
	 */

	if ( $( "#modal-finish-sale" ).length > 0 )
	{
		$( "#modal-finish-sale" ).modal({
			backdrop: 'static',
			keyboard: false,
			show: false,
		});

		$( "#modal-finish-sale" ).on( 'show.modal.bs', function(){
			// Set first foreign currency
			for ( index in conf.currencies )
			{
				if ( index != conf.currency )
				{
					picked_foreign_currency = index;
					$( "#modal_finish_foreign" ).find( ".input-group-text, #modal_finish_amount_given_foreign button" ).text( conf.currencies[index] );
					break;
				}
			}

			// List of foreign currency
			$( "#modal_finish_foreign .foreign-currency-item" ).each( function(){
				var currency = $( this ).data( 'currency' );

				$( this ).on( 'click', function( ev ){
					ev.preventDefault();

					var to_replace = $( "#modal-finish-sale" ).find( "input#meta_sale_total" ).inputmask( 'unmaskedvalue' );
					if ( $( "#modal-finish-sale" ).find( "input#meta_vars_amount_given" ).data( 'original-value' ) != "" ) to_replace = $( "#modal-finish-sale" ).find( "input#meta_vars_amount_given" ).data( 'original-value' );

					picked_foreign_currency = currency;
					$( "#modal_finish_foreign" ).find( "span.input-group-text, button" ).text( conf.currencies[currency] );

					// Reset the given amount every time the foreign currency changes
					$( "#modal-finish-sale" ).find( "input#meta_vars_amount_given" ).val( to_replace );
					$( "#modal-finish-sale" ).find( "input#meta_vars_change" ).val( '' );

					calculate_change();
					calculate_foreign_vars();
				});
			});

			// Init change input in case there's no value on the field
			if ( $( "#modal-finish-sale" ).find( "input#meta_vars_amount_given" ).val() == "" )
			{
				$( "#modal-finish-sale" ).find( "input#meta_vars_amount_given" ).val( $( "#modal-finish-sale" ).find( "input#meta_sale_total" ).inputmask( 'unmaskedvalue' ) );
			}

			// Select amount given and foreign amount given on focus
			$( "#modal-finish-sale" ).find( "input#meta_vars_amount_given, input#meta_amount_given_foreign" ).on( 'focus', function(){
				$( this ).select();
			});

			// Select credit card or debit card and expand
			$( 'input[name="meta_vars[payment_method]"]' ).on( 'click', function(){
				$( "#modal_finish_cc_company, #modal_finish_dc_company" ).collapse( 'hide' );
				$( "#modal_finish_cc_company, #modal_finish_dc_company" ).find( '.btn-group-toggle' ).button( 'toggle' );
				$( "#modal_finish_cc_company, #modal_finish_dc_company" ).find( 'input' ).prop( 'checked', false );
				
				if ( $( this ).val() == "credit" ) $( "#modal_finish_cc_company" ).collapse( 'show' );
				if ( $( this ).val() == "debit" ) $( "#modal_finish_dc_company" ).collapse( 'show' );
			});

			// Check if debit or credit card is selected and show the sub-panel accordingly
			$( 'input[name="meta_vars[payment_method]"]' ).each( function(){
				if ( $( this ).is( ':checked' ) )
				{
					if ( $( this ).val() == "credit" ) $( "#modal_finish_cc_company" ).collapse( 'show' );
					if ( $( this ).val() == "debit" ) $( "#modal_finish_dc_company" ).collapse( 'show' );
				}
			});
			
			// Set minimum to the amount given box
			$( "#modal-finish-sale" ).find( "input#meta_vars_amount_given" ).inputmask( 'remove' );
			input_options.currency.min = parseFloat( $( "#modal-finish-sale" ).find( "input#meta_sale_total" ).inputmask( 'unmaskedvalue' ) );
			$( "#modal-finish-sale" ).find( "input#meta_vars_amount_given" ).inputmask( input_options.currency )

			// Foreign calcs
			$( "#modal-finish-sale" ).find( "input#meta_sale_total, input#meta_vars_amount_given, input#meta_vars_change" ).on( 'keyup change', calculate_change );
			$( "#modal-finish-sale" ).find( "input#meta_sale_total, input#meta_vars_amount_given, input#meta_vars_change" ).on( 'keyup change', calculate_foreign_vars );
			$( "#modal-finish-sale" ).find( "input#meta_amount_given_foreign" ).on( 'keyup change', calculate_foreign_change );

			calculate_foreign_vars();
		});

		$( "#finish-sale" ).on( 'click', function( ev ){
			ev.preventDefault();

			if ( $( "table.product-list tbody tr:not(.total-amount-row)" ).length <= 0 )
			{
				alert( messages.one_product_to_finish );
			}
			else if ( $( "input#name" ).val() == "" )
			{
				alert( messages.client_to_finish );
			}
			else
			{
				$( "#modal-finish-sale" ).modal( 'show' );
			}
		});
	}

});