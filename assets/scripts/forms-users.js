jQuery(function($){

	/**
	 * UPLOAD PICTURE
	 */

	build_file_upload = function()
	{
		$( document ).find( "#fileupload" ).fileupload({
			url: conf.site_url + 'files/upload',
			dataType: 'json',
			type: 'POST',
			// fileInput: 'files',
			// paramName: 'files[]',
			formData: {
				'file_types' 	: 'gif|jpg|png|jpeg|JPG|JPEG|PNG|GIF',
				'location' 		: '/uploads/avatars/',
				'controller' 	: 'users',
				'registry_id' 	: ( $( "#user_id" ).val() !== "" ) ? $( "#user_id" ).val() : null,
			},
			start: function(){
				$( '#progress .progress-bar' ).removeClass( 'bg-danger' ).removeClass( 'bg-success' );
			},
			progressall: function ( e, data ){
				var progress = parseInt( data.loaded / data.total * 100, 10 );
				$( '#progress .progress-bar' ).css( 'width', progress + '%' );
			},
			complete: function( object, status ){
				if ( status == "success" && object.responseJSON.upload )
				{
					$.ajax({
						url: conf.site_url + 'files/picture_processing',
						dataType: 'json',
						type: 'POST',
						data: object.responseJSON.upload,
						success: function( response )
						{
							if ( response.html_form )
							{
								$( document ).find( "#picture-card .form-row" ).remove();
								$( response.html_form ).insertAfter( '#picture-card h4' );
								build_file_delete();								
							}
						}
					});
				}
			},
			error: function( object, status, error ){
				if ( object.responseJSON.error ) alert( object.responseJSON.error );
				$( '#progress .progress-bar' ).prop( 'style', false );
			}
		});
	};

	/**
	 * DELETE PICTURE
	 */
	
	build_file_delete = function()
	{
		$( document ).on( 'click', '.fileinput-delete', function( ev ){
			 ev.preventDefault();

			 var file_id = $( "input#file_id" ).val();

			 if ( file_id !== "" )
			 {
			 	$.ajax({
			 		url: conf.site_url + 'files/delete',
			 		type: 'POST',
			 		dataType: 'json',
			 		data: { 
			 			file_id : file_id,
			 			view_name : 'files/model-single-picture',
			 		},
			 		success: function( response )
			 		{
			 			if ( response.html_form )
			 			{
							$( document ).find( "#picture-card .form-row" ).remove();
							$( response.html_form ).insertAfter( '#picture-card h4' );
							build_file_upload();
			 			}
			 		},
					error: function( object, status, error ){
						if ( object.responseJSON.error ) alert( object.responseJSON.error );
					}
			 	});			 	
			 }
		});
	};

	/**
	 * INIT
	 */
	build_file_upload();
	build_file_delete();
	
});