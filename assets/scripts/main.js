jQuery(function($){

	// Multiple bootstrap modal fix
	$.fn.modal.Constructor.prototype._enforceFocus = function(){};

	// REMOVE SELECT2 TITLE RENDER
	$( document ).ready( function(){
		checker = setInterval( function(){
			if ( jQuery.fn.select2 )
			{
				$( ".select2-selection__rendered" ).removeAttr( 'title' );
				clearInterval( checker );
			}
		}, 500 );
	});

	/**
	 * TOOLTIP FOR EVERYTHING WITH A TITLE
	 */
	
	var tooltip_ops = {};
	if ( ! conf.isMobile ) tooltip_ops = { trigger: 'hover' };
	$( "*[title]:not([class^=select2])" ).tooltip( tooltip_ops );

	/**
	 * PRINT BUTTON
	 */
	
	$( ".print-button" ).click( function( ev ){
		ev.preventDefault();
		print();
	});

	/**
	 * DISABLE CONTEXT MENU
	 */

	// $( document ).contextmenu(function() { return false; });
	
	/**
	 * AJAX LOADING MESSAGE
	 */
	$( document ).ajaxStart( function(){
		$( "#loading-screen" ).show();
	});

	$( document ).ajaxComplete( function(){
		$( "#loading-screen" ).hide();
	});	
});

/**
 * Rebuild tooltips
 *
 * @return {void}
 */
rebuild_tooltips = function()
{
	jQuery(function($){
		$( ".tooltip" ).remove();
		var tooltip_ops = {};
		if ( ! conf.isMobile ) tooltip_ops = { trigger: 'hover' };
		$( "*[title]:not([class^=select2])" ).tooltip( tooltip_ops );		
	});
}

/**
 * Format to currency
 *
 * @param {int} n decimals
 * @param {int} x length of sections
 */
Number.prototype.toCurrency = function( n, x )
{
	var re			= '\\d(?=(\\d{' + ( x || 3 ) + '})+' + ( n > 0 ? '\\.' : '$' ) + ')';
	var defaulted	= this.toFixed( Math.max( 0, ~~n ) ).replace( new RegExp( re, 'g' ), '$&,' );
	var split		= defaulted.split( '.' );
	var formatted	= split[0].replaceAll( ",", conf.thousands_separator );
	if ( typeof split[1] != "undefined" ) formatted += conf.decimal_point + split[1];
	return formatted;
};

/**
 * Precission round for no currencies Format
 * 
 * @param  {int}	decimals
 * @return {float}
 */
Number.prototype.precissionRound = function( decimals )
{
	var factor = Math.pow( 10, decimals );
	return Math.round( this * factor ) / factor;
};

/**
 * ALERT
 */
window.alert = function( message )
{
	jQuery(function($){
		var alert = $( "#modal-alert" );
		var real_body = alert.find( '.modal-body' ).html();
		alert.modal({
			backdrop: 'static'
		}).on( 'hidden.bs.modal', function(){
			$( this ).find( '.modal-body' ).html( real_body );
		});
		message = message.replace( /\n\n/g, "</p><p>" );
		alert.find( '.modal-body' ).html( '<p>' + message + '</p>' );
		alert.modal( 'show' );
		setTimeout( function(){ $( ".modal-backdrop:not(:first)" ).remove(); }, 100 ); // so it doesn't duplicate the backdrop in the presence of another modal.
	});
}

/**
 * CONFIRM
 */
window.confirm = function( message, callback, element )
{
	jQuery(function($){
		var real_body = $( '#modal-confirm .modal-body' ).html();
		$( '#modal-confirm' ).modal({
			backdrop: 'static'
		}).on( 'hide.bs.modal', function(){
			if ( callback ) callback( $( '#modal-confirm' ).data( 'confirm-yes' ), element );
		}).on( 'hidden.bs.modal', function(){
			$( this ).find( '.modal-body' ).html( real_body );
			$( this ).data( 'confirm-yes', false );
		}).data( 'confirm-yes', false );

		$( '#modal-confirm .modal-footer .btn-primary' ).off( 'click' );
		$( '#modal-confirm .modal-footer .btn-primary' ).one( 'click', function( ev ){
			$( '#modal-confirm' ).data( 'confirm-yes', true );
			$( '#modal-confirm' ).modal( 'hide' );
			// return false;
		});
		message = message.replace( /\n\n/g, "</p><p>" ).replace( /\n/g, "<br />" );
		$( '#modal-confirm .modal-body' ).html( '<p>' + message + '</p>' );
		$( '#modal-confirm' ).modal( 'show' );
		setTimeout( function(){ $( ".modal-backdrop:not(:first)" ).remove(); }, 100 ); // so it doesn't duplicate the backdrop in the presence of another modal.
	});
}

the_confirm_callback = function( boolean, element )
{
	jQuery(function($){
		if ( boolean )
		{
			if ( element.attr( 'href' ) ) window.location = element.attr( 'href' );
			if ( element.is( 'form' ) ) element.submit();
			if ( element.is( '[type="submit"]' ) || element.hasClass( 'btn-bulk' ) )
			{
				element = element.parentsUntil( 'form' ).parent();
				element[0].submit();
			}
		}
	});
}

/**
 * The default delayer for keyboard input
 */

var delayer = (function()
{
	var timer = 0;
	return function( callback, ms )
	{
		clearTimeout( timer );
		timer = setTimeout( callback, ms );
	};
})();

/**
 * Sprintf version for strings in JS
 */

String.prototype.format = String.prototype.format || function () {
	"use strict";
	var str = this.toString();
	if ( arguments.length )
	{
		var t = typeof arguments[0];
		var key;
		var args = ( "string" === t || "number" === t ) ? Array.prototype.slice.call( arguments ) : arguments[0];

		for ( key in args )
		{
			str = str.replace( new RegExp( "\\{" + key + "\\}", "gi" ), args[key] );
		}
	}

	return str;
};

/**
 * GET THE EXCHANGE JSON AND SET IT IN GLOBAL
 */

var exchange_values = null;

jQuery(function($){
	$.getJSON( conf.site_url + 'uploads/exchange.json?v=' + Date.now(), function( response, status ) {
		if ( status == "success" && response && ! $.isEmptyObject( response ) ) 
		{
			exchange_values = response;
		}
		else
		{
			alert( messages.exchange_file_not_loaded );
		}
	}).fail( function(){
		alert( messages.exchange_file_not_loaded );
	});
});

/**
 * EXCHANGE VALUE
 */

exchange_to_main_currency = function( value, currency )
{
	value = value || null;
	currency = currency || null;

	if ( value != null && currency != null )
	{
		value = parseFloat( value );

		if ( currency != conf.currency && exchange_values != null )
		{
			if ( typeof exchange_values[currency].sale !== "undefined" ) value = value * parseFloat( exchange_values[currency].sale );
		}
	}

	return ( value != null ) ? parseFloat( value ) : null;
};
