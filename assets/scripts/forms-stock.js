jQuery(function($){

	/**
	 * Checks the remote sku and do the procedure of showing stuff
	 * 
	 * @param  object  element The sku element
	 * @param  boolean force   Whether to force or show confirmation before proceding
	 * @return void
	 */
	check_remote_sku = function( element, force, event )
	{
		var force 			= force || false;
		var event 			= event || false;
		var tr 				= element.parent().parent();
		var sku 			= element;
		var sku_val 		= sku.val();
		var currency 		= tr.find( 'input.input-product-currency' ).val();
		var unit_measure 	= tr.find( 'input.input-product-unit-measure' ).val();

		delayer( function(){
			if ( sku_val !== "" )
			{
				$.getJSON( conf.site_url + 'ajax/stock_retrieve/' + sku_val, function( result, status ){
					if ( status == "success" && result )
					{
						if ( ! force )
						{
							confirm( messages.sku_duplicated_alert.format( 
								result.product.name, 
								parseFloat( ( result.stock.init_quantity ) ? result.stock.init_quantity : 0 ).toCurrency( conf.qty_decimals ) + " " + conf.units[result.product.unit_measure], 
								parseFloat( ( result.stock.quantity ) ? result.stock.quantity : 0 ).toCurrency( conf.qty_decimals ) + " " + conf.units[result.product.unit_measure] 
							), function( boolean ){
								if ( boolean )
								{
									tr.find( "td:nth-child(3)" ).html( '<small class="text-muted">' + conf.units[unit_measure] + '</small> ' + parseFloat( result.stock.quantity ).toCurrency( conf.qty_decimals ) );
									tr.find( "input.input-init-quantity" ).val( parseFloat( result.stock.quantity ) );

									tr.find( "td:nth-child(7)" ).text( result.stock.expiration_date_formatted );
									tr.find( "input.input-expiration-date" ).val( result.stock.expiration_date );

									tr.find( '.action-buttons button' ).prop( 'disabled', false );
								}
								else
								{
									tr.find( 'td:nth-child(3)' ).html( '' );
									tr.find( 'input.input-init-quantity' ).val( '' );
									tr.find( '.action-buttons button' ).prop( 'disabled', true );
									sku.val( '' );
								}
							});
						}
						else
						{
							tr.find( "td:nth-child(3)" ).html( '<small class="text-muted">' + conf.units[unit_measure] + '</small> ' + parseFloat( result.stock.quantity ).toCurrency( conf.qty_decimals ) );
							tr.find( "input.input-init-quantity" ).val( parseFloat( result.stock.quantity ) );
							tr.find( '.action-buttons button' ).prop( 'disabled', false );
						}
					}
					else
					{
						tr.find( "td:nth-child(3)" ).html( '<small class="text-muted">' + conf.units[unit_measure] + '</small> ' + parseFloat( 0 ).toCurrency( conf.qty_decimals ) );
						tr.find( "input.input-init-quantity" ).val( parseFloat( 0 ) );
						tr.find( '.action-buttons button' ).prop( 'disabled', false );
					}
				});
			}
			else
			{
				tr.find( "td:nth-child(3)" ).html( '' );
				tr.find( "input.input-init-quantity" ).val( '' );
				tr.find( '.action-buttons button' ).prop( 'disabled', true );
			}
		}, 2000 );

		if ( event )
		{
			event.preventDefault();
			event.stopPropagation();
			event.stopImmediatePropagation();			
		}
	};

	/**
	 * --------------------------------------------------------------------------------------
	 * ADD FROM PURCHASE
	 * --------------------------------------------------------------------------------------
	 */

	if ( $( "body" ).hasClass( 'add' ) )
	{
		/**
		 * ON SKU CHANGE
		 */
		
		$( document ).on( 'keyup', 'input.input-sku', function( event ){
			check_remote_sku( $( this ), false, event );
		});

		/**
		 * EDIT PRODUCT MODAL
		 */
		
		$( document ).on( 'click', ".btn-edit", function( ev ){
			var tr = $( this ).parent().parent().parent();

			// Setting up the modal
			$( "#modal-edit-product" ).find( '.table tbody tr:nth-child(1) td:last' ).text( tr.find( "td:nth-child(1)" ).text() );
			$( "#modal-edit-product" ).find( '.table tbody tr:nth-child(2) td:last' ).text( tr.find( ".input-sku" ).val() );
			$( "#modal-edit-product" ).find( '.table tbody tr:nth-child(3) td:last' ).html( tr.find( "td:nth-child(3)" ).html() );
			$( "#modal-edit-product" ).find( '.table tbody tr:nth-child(4) td:last' ).html( tr.find( "td:nth-child(4)" ).html() );

			$( "#modal-edit-product" ).find( '#modal_quantity_box .input-group-text' ).text( conf.units[tr.find( '.input-product-unit-measure' ).val()] );
			$( "#modal-edit-product" ).find( '#modal_price_box .input-group-text' ).text( conf.currencies[tr.find( '.input-product-currency' ).val()] );

			$( "#modal-edit-product" ).find( '#modal_quantity' ).val( tr.find( '.input-quantity' ).val() );
			$( "#modal-edit-product" ).find( '#modal_price' ).val( tr.find( '.input-unit-price' ).val() );
			$( "#modal-edit-product" ).find( '#modal_expiration_date' ).val( tr.find( "td:nth-child(7)" ).text() );

			$( "#modal-edit-product" ).find( "#confirm-edit-product" ).data( 'row-id', tr.prop( 'id' ) );

			$( "#modal-edit-product" ).modal( 'show' );

			ev.preventDefault();
			ev.stopPropagation();
			ev.stopImmediatePropagation();
		});

		/**
		 * RESET THE MODAL ON HIDING
		 */
		
		$( "#modal-edit-product" ).on( 'hidden.bs.modal', function( ev ){
			$( "#modal-edit-product" ).find( '.table tbody tr:nth-child(1) td:last' ).text( '' );
			$( "#modal-edit-product" ).find( '.table tbody tr:nth-child(2) td:last' ).text( '' );
			$( "#modal-edit-product" ).find( '.table tbody tr:nth-child(3) td:last' ).html( '' );
			$( "#modal-edit-product" ).find( '.table tbody tr:nth-child(4) td:last' ).html( '' );

			$( "#modal-edit-product" ).find( '#modal_quantity_box .input-group-text' ).text( '' );
			$( "#modal-edit-product" ).find( '#modal_price_box .input-group-text' ).text( '' );

			$( "#modal-edit-product" ).find( '#modal_quantity' ).val( '' );
			$( "#modal-edit-product" ).find( '#modal_price' ).val( '' );
			$( "#modal-edit-product" ).find( '#modal_expiration_date' ).val( '' );

			$( "#modal-edit-product" ).find( "#confirm-edit-product" ).data( 'row-id', '' );

			ev.preventDefault();
			ev.stopPropagation();
			ev.stopImmediatePropagation();
		});

		/**
		 * EDIT PRODUCT ACCEPT NEW DATA
		 */
		
		$( "#confirm-edit-product" ).on( 'click', function( ev ){
			var tr 						= $( "#" + $( this ).data( 'row-id' ) );
			var expiration_date 		= $( "#modal-edit-product" ).find( "input[name=modal_expiration_date]" ).val();
			var expiration_date_submit 	= $( "#modal-edit-product" ).find( "input[name=modal_expiration_date_submit]" ).val();
			var unit_price 				= parseFloat( $( "#modal-edit-product" ).find( "#modal_price" ).inputmask( 'unmaskedvalue' ) );
			var quantity 				= parseFloat( $( "#modal-edit-product" ).find( "#modal_quantity" ).inputmask( 'unmaskedvalue' ) );
			var currency 				= tr.find( ".input-product-currency" ).val();
			var unit_measure 			= tr.find( ".input-product-unit-measure" ).val();

			if ( isNaN( unit_price ) || unit_price <= 0 || isNaN( quantity ) || quantity <= 0 )
			{
				alert( messages.complete_fields_to_edit );
				return false;
			}

			// Quantity
			tr.find( "td:nth-child(5)" ).html( '<small class="text-muted">' + conf.units[unit_measure] + '</small> ' + quantity.toCurrency( conf.qty_decimals ) );
			tr.find( ".input-quantity" ).val( quantity );

			// Purchased quantity
			tr.find( ".input-purchase-quantity" ).val( quantity );

			// Sale price
			tr.find( "td:nth-child(6)" ).html( '<small class="text-muted">' + conf.currencies[currency] + '</small> ' + unit_price.toCurrency( conf.decimals ) );
			tr.find( ".input-unit-price" ).val( unit_price );

			// Expiration date
			if ( expiration_date != "" )
			{
				 tr.find( 'td:nth-child(7)' ).text( expiration_date );
				 tr.find( '.input-expiration-date' ).val( expiration_date_submit );
			}

			$( "#modal-edit-product" ).modal( 'hide' );

			ev.preventDefault();
			ev.stopPropagation();
			ev.stopImmediatePropagation();
		});

		/**
		 * DUPLICATE ROW
		 */
		
		$( document ).on( 'click', '.btn-duplicate', function( ev ){
			var tr = $( this ).parent().parent().parent();

			var new_tr = tr.clone();

			new_tr.attr( 'id', 'row-' + ( $( "table.product-list tbody tr" ).length + 1 ) );

			new_tr.find( "td:nth-child(3)" ).text( '' );
			new_tr.find( "td:nth-child(5)" ).text( '' );
			new_tr.find( "td:nth-child(6)" ).text( '' );
			new_tr.find( "td:nth-child(7)" ).text( '' );

			new_tr.find( ".input-sku" ).val( '' );
			new_tr.find( ".input-init-quantity" ).val( '' );
			new_tr.find( ".input-expiration-date" ).val( '' );
			new_tr.find( ".input-purchase-quantity" ).val( '' );
			new_tr.find( ".input-quantity" ).val( '' );
			new_tr.find( ".input-unit-price" ).val( '' );

			new_tr.find( ".input-sku" ).inputmask({
				mask: '9{*}',
				rightAlign: false,
				allowMinus: false,
				autoUnmask: false,
				removeMaskOnSubmit: false,
			});

			new_tr.insertAfter( tr );

			setTimeout( function(){
				new_tr.find( "button.dropdown-toggle" ).trigger( 'click' );
				new_tr.data( 'row-original', '' );
				new_tr.find( "button" ).prop( 'disabled', true );
				rebuild_tooltips();
			}, 500 );

			ev.preventDefault();
			ev.stopPropagation();
			ev.stopImmediatePropagation();			
		});

		/**
		 * STORE THE ROW
		 */
		
		$( document ).on( 'click', '.btn-store', function( ev ){			
			var tr 					= $( this ).parent().parent();
			var purchase_id 		= $( "#purchase_id" ).val();
			var purchase_meta_id 	= tr.find( ".input-purchase-meta-id" ).val();
			var expiration_date 	= tr.find( ".input-expiration-date" ).val();
			var product_id 			= tr.find( ".input-product-id" ).val();
			var sku 				= tr.find( ".input-sku" ).val();
			var purchase_quantity 	= parseFloat( tr.find( ".input-purchase-quantity" ).val() );
			var purchase_price 		= parseFloat( tr.find( ".input-purchase-price" ).val() );
			var unit_price 			= parseFloat( tr.find( ".input-unit-price" ).val() );
			var init_quantity 		= parseFloat( tr.find( ".input-init-quantity" ).val() );
			var quantity 			= parseFloat( tr.find( ".input-quantity" ).val() );

			if ( isNaN( purchase_price ) || purchase_price <= 0 || isNaN( unit_price ) || unit_price <= 0 || isNaN( quantity ) || quantity <= 0 )
			{
				alert( messages.complete_fields_to_add_stock );
				return false;
			}

			$.getJSON( conf.site_url + 'ajax/get_csrf_hash', function( hash, status ){
				if ( status == "success" )
				{
					var to_send = {
						purchase_meta_id	: purchase_meta_id,
						purchase_id			: purchase_id, 
						product_id 			: product_id,
						sku 				: sku,
						expiration_date		: expiration_date,
						purchase_price		: purchase_price,
						purchase_quantity	: purchase_quantity,
						unit_price			: unit_price,
						init_quantity		: init_quantity,
						quantity			: quantity,
					};

					to_send[hash.csrf_name] = hash.csrf_hash;

					if ( $( "table.product-list tbody tr" ).length == 1 ) to_send.last_row = 1;

					$.ajax({
						url 		: conf.site_url + 'ajax/store_stock_row',
						type 		: 'POST',
						dataType	: 'json',
						data 		: to_send,
						success 	: function( result, server_status )
						{
							console.log(result);
							if ( server_status == "success" && ! result.error )
							{
								alert( result.message );
								tr.fadeOut().remove();
								rebuild_tooltips();

								// Iterate thru rows to update quantities of repeated SKUs
								$( "table.product-list tbody tr input.input-sku" ).each( function(){
									check_remote_sku( $( this ), true );
								});

								// If it's the last row, then reload the page to hide the whole table
								if ( to_send.last_row && result.redirect ) window.location.replace( result.redirect );
							}
							else
							{
								alert( result.error );
							}
						}
					});
				}
			});

			ev.preventDefault();
			ev.stopPropagation();
			ev.stopImmediatePropagation();
		});

		/**
		 * RESET PURCHASE META WITH PREVIOUS VALUES
		 */
		
		$( document ).on( 'click', '.btn-rebuild', function( ev ){
			ev.preventDefault();
			var tr 					= $( this ).parent().parent().parent();
			var purchase_meta_id 	= tr.find( '.input-purchase-meta-id' ).val();
			var sku_val 			= tr.find( '.input-sku' ).val();
			var currency 			= tr.find( ".input-product-currency" ).val();
			var unit_measure 		= tr.find( ".input-product-unit-measure" ).val();

			if ( purchase_meta_id !== "" )
			{
				// Purchase meta
				$.getJSON( conf.site_url + 'ajax/purchase_meta_retrieve/' + purchase_meta_id, function( result, status ){
					if ( status == "success" )
					{
						// Purchase price
						tr.find( "td:nth-child(4)" ).html( '<small class="text-muted">' + conf.currencies[currency] + '</small> ' + parseFloat( result.purchase.unit_price ).toCurrency( conf.decimals ) );
						tr.find( ".input-purchase-price" ).val( parseFloat( result.purchase.unit_price ) );

						// Quantity
						tr.find( "td:nth-child(5)" ).html( '<small class="text-muted">' + conf.units[unit_measure] + '</small> ' + parseFloat( result.purchase.quantity ).toCurrency( conf.qty_decimals ) );
						tr.find( ".input-quantity" ).val( parseFloat( result.purchase.quantity ) );

						// Unit price
						tr.find( "td:nth-child(6)" ).html( '<small class="text-muted">' + conf.currencies[currency] + '</small> ' + parseFloat( result.product.price ).toCurrency( conf.decimals ) );
						tr.find( ".input-unit-price" ).val( parseFloat( result.product.price ) );
					}
				});

				// SKU
				$.getJSON( conf.site_url + 'ajax/stock_retrieve/' + sku_val, function( result, status ){
					if ( status == "success" && result )
					{						
						// Init quantity
						tr.find( "td:nth-child(3)" ).html( '<small class="text-muted">' + conf.units[unit_measure] + '</small> ' + parseFloat( result.stock.quantity ).toCurrency( conf.decimals ) );
						tr.find( ".input-init-quantity" ).val( parseFloat( result.stock.quantity ) );

						// Expiration date
						tr.find( "td:nth-child(7)" ).text( result.stock.expiration_date_formatted );
						tr.find( ".input-expiration-date" ).val( result.stock.expiration_date );
					}
					else
					{
						// Init quantity
						tr.find( "td:nth-child(3)" ).html( '<small class="text-muted">' + conf.units[unit_measure] + '</small> ' + parseFloat( 0 ).toCurrency( conf.qty_decimals ) );
						tr.find( "input.input-init-quantity" ).val( parseFloat( 0 ) );

						// Expiration date
						tr.find( "td:nth-child(7)" ).text( '' );
						tr.find( "input.input-expiration-date" ).val( '' );
					}
				});

				$( "#modal-edit-product" ).trigger( 'hidden.bs.modal' );
			}
		});

		/**
		 * DELETE CLONES
		 */
		
		$( document ).on( 'click', '.btn-erase', function( ev ){
			ev.preventDefault();
			var tr = $( this ).parent().parent().parent();

			if ( tr.data( 'row-original' ) )
			{
				alert( messages.original_row );
			}
			else
			{
				tr.remove();
			}
		});
	}

	/**
	 * --------------------------------------------------------------------------------------
	 * ADD GENERAL
	 * --------------------------------------------------------------------------------------
	 */

	// Move date modal to the bottom
	
	if ( $( ".picker" ).length > 0 )
	{
		$( ".picker" ).insertAfter( '#main-content-wrapper' );
	}

	/**
	 * --------------------------------------------------------------------------------------
	 * ADD FROM SELF
	 * --------------------------------------------------------------------------------------
	 */
	
	if ( $( "body" ).hasClass( 'self' ) )
	{
		/**
		 * Do the selection of product via ajax to display its settings accurately
		 * @return void
		 */
		var select_product_dropdown = function()
		{
			if ( $( "select#product_id" ).val() !== "" )
			{
				$.getJSON( conf.site_url + 'ajax/product_retrieve/' + $( "select#product_id" ).val(), function( result, status ) {
					if ( status == "success" && exchange_values )
					{
						$( "#unit_price" ).val( parseFloat( result.price * parseFloat( exchange_values[result.currency].sale ) ) );
						$( "#quantity" ).next().find( '.input-group-text' ).text( conf.units[result.unit_measure] );
						$( "#init_quantity" ).next().find( '.input-group-text' ).text( conf.units[result.unit_measure] );
					}
				});
			}
		};

		/**
		 * SELECT PRODUCT ID
		 */

		$( "select#product_id, #modal-add-new-product select" ).select2({
			language: conf.language_short,
		});

		$( "select#product_id" ).on( 'change', select_product_dropdown );

		// On init
		if ( $( "select#product_id" ).val() !== "" ) select_product_dropdown();

		/**
		 * ADD PRODUCT MODAL PRODUCT ID ON CHANGE
		 */

		var build_product_select = function( data )
		{
			var options = {};
				options.language = conf.language_short;

			if ( data && data.length > 0 ) options.data = data;

			$( "select#product_id" ).select2( options );
		}

		build_product_select();

		/**
		 * ADD NEW PRODUCT - OPEN MODAL
		 */
		
		$( "button#open-new-product-modal" ).on( 'click', function( ev ){
			$( "#modal-add-new-product" ).modal( 'show' );
		});

		/**
		 * INIT MODAL TO ADD NEW PRODUCT WITH ALL ITS BEHAVIORS
		 */

		$( "#modal-add-new-product" ).on( 'show.modal.bs', function(){
			// Pick currency
			$( '#modal_new_product_unit_price_box' ).find( '.dropdown-menu a' ).on( 'click', function( ev ){
				ev.preventDefault();
				var currency = $( this ).data( 'currency' );

				if ( currency != "" )
				{
					$( "input#modal_new_product_currency" ).val( currency );
					$( '#modal_new_product_unit_price_box' ).find( 'button' ).text( conf.currencies[currency] );

					// Change the input class
					if ( currency == conf.currency )
					{
						$( '#modal_new_product_unit_price_box' ).find( "input#modal_new_product_unit_price" ).inputmask( 'remove' );
						$( '#modal_new_product_unit_price_box' ).find( "input#modal_new_product_unit_price" ).inputmask( input_options.currency );
					}
					else
					{
						$( '#modal_new_product_unit_price_box' ).find( "input#modal_new_product_unit_price" ).inputmask( 'remove' );
						$( '#modal_new_product_unit_price_box' ).find( "input#modal_new_product_unit_price" ).inputmask( input_options.currency_foreign );
					}
				}
			});
			
			// Send the new product for storing... or not
			$( "#confirm-add-new-product" ).on( 'click', function( ev ){
				ev.preventDefault();

				var goon = false;

				$( "#modal-add-new-product" ).find( "input, select" ).each( function(){
					if ( $( this ).val() == "" )
					{
						goon = false;
						alert( messages.complete_fields_to_list );
					}
					else
					{
						goon = true;
					}
				});

				if ( goon )
				{
					$.getJSON( conf.site_url + 'ajax/get_csrf_hash', function( response, status ) {
						if ( status == "success" )
						{
							var to_send 					= {};
								to_send.name 				= $( "#modal-add-new-product" ).find( "#modal_new_product_name" ).val();
								to_send.unit_measure		= $( "#modal-add-new-product" ).find( "#modal_new_product_unit_measure" ).val();
								to_send.category_id			= $( "#modal-add-new-product" ).find( "#modal_new_product_category_id" ).val();
								to_send.price				= $( "#modal-add-new-product" ).find( "#modal_new_product_unit_price" ).inputmask( 'unmaskedvalue' );
								to_send.currency			= $( "#modal-add-new-product" ).find( "#modal_new_product_currency" ).val();
								to_send.tax					= $( "#modal-add-new-product" ).find( "#modal_new_product_tax" ).val();
								to_send[response.csrf_name] = response.csrf_hash;

							$.ajax({
								url: conf.site_url + 'ajax/store_product',
								type: 'POST',
								data: to_send,
								success: function( response )
								{
									if ( response != null && response != "" && ! response.error )
									{
										var product_id = response.item_row.product_id;

										$.getJSON( conf.site_url + 'ajax/product_dropdown_retrieve', function( response, status ) {
											if ( status == "success" && response != null && response != "" && ! response.error )
											{
												$( 'select#product_id' ).select2( 'destroy' );
												$( 'select#product_id option, select#product_id optgroup' ).remove();

												var product_list = new Array;
												$.map( response, function( ele, ind ) {
													if ( typeof ele == "string" )
													{
														product_list.push({
															"id": ind,
										  					"text": ( ele != "&nbsp;" ) ? ele : "\n",
														});
													}
													else if ( typeof ele == "object" )
													{
														var with_children = {
										  					"text": ind,
										  					"children": []
														};

														$.map( ele, function( ele_2, ind_2 ) {
															with_children.children.push({
																"id": ind_2,
											  					"text": ele_2,
															});
														});

														product_list.push( with_children );
													}
												});

												build_product_select( product_list );

												$( "select#product_id" ).val( product_id ).trigger( 'change' );
												$( "#modal-add-new-product" ).modal( 'hide' );

												// Rebuild inputs on the add new product modal
												$( "#modal-add-new-product" ).find( 'input, select' ).val( '' );
												$( "#modal-add-new-product" ).find( '#modal_new_product_currency' ).val( conf.currency );
												$( "#modal-add-new-product" ).find( '#modal_new_product_unit_price_box button' ).text( conf.currencies[conf.currency] );
											}
											else if ( response.error )
											{
												alert( response.error );
											}
										});
									}
									else if ( response.error )
									{
										alert( response.error );
									}
								},
								error: function( response )
								{
									alert( response.error );
								},
								complete: rebuild_main_tokens,
							});
						}
					});
				}
			});
		});

	}

});