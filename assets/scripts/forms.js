input_options = {
	'currency': {
		alias: 'currency',
		autoGroup: true,
		rightAlign: true,
		digits: conf.decimals,
		digitsOptional: false,
		prefix: '',
		alignRight: true,
		placeholder: parseFloat( 0 ).toCurrency( conf.decimals ),
		allowMinus: false,
		removeMaskOnSubmit: true,
		groupSeparator: conf.thousands_separator,
		radixPoint: ( conf.decimals > 0 ) ? conf.decimal_point : '',
		onUnMask: function( maskedValue, unMaskedValue ) // In case we change the default chars for grouping and decimaling, only if we have a comma... let's see if we can universalize this later
		{
			if ( typeof unMaskedValue != "undefined" && unMaskedValue.indexOf( "," ) > -1 )
			{
				unMaskedValue = unMaskedValue.replace( ',', '.' );
			}

			return unMaskedValue;
		}
	},
	'currency_ext': {
		alias: 'currency',
		autoGroup: true,
		rightAlign: true,
		digits: 5,
		digitsOptional: false,
		prefix: '',
		placeholder: parseFloat( 0 ).toCurrency( 2 ),
		allowMinus: false,
		removeMaskOnSubmit: true,
		groupSeparator: conf.thousands_separator,
		radixPoint: conf.decimal_point,
		onUnMask: function( maskedValue, unMaskedValue ) // In case we change the default chars for grouping and decimaling
		{
			if ( typeof unMaskedValue != "undefined" && unMaskedValue.indexOf( "," ) > -1 )
			{
				unMaskedValue = unMaskedValue.replace( ',', '.' );
			}

			return unMaskedValue;
		}
	},
	'currency_foreign': {
		alias: 'currency',
		autoGroup: true,
		rightAlign: true,
		digits: 2,
		digitsOptional: false,
		prefix: '',
		placeholder: parseFloat( 0 ).toCurrency( 2 ),
		allowMinus: false,
		removeMaskOnSubmit: true,
		groupSeparator: conf.thousands_separator,
		radixPoint: conf.decimal_point,
		onUnMask: function( maskedValue, unMaskedValue ) // In case we change the default chars for grouping and decimaling
		{
			if ( typeof unMaskedValue != "undefined" && unMaskedValue.indexOf( "," ) > -1 )
			{
				unMaskedValue = unMaskedValue.replace( ',', '.' );
			}

			return unMaskedValue;
		}
	},
	'integer': {
		alias: 'integer',
		autoGroup: true,
		rightAlign: true,
		digits: 0,
		placeholder: '0',
		allowMinus: false,
		autoUnmask: true,
		removeMaskOnSubmit: true,
		groupSeparator: conf.thousands_separator,
		radixPoint: '',
	},
	'raw_integer': {
		mask: '9{*}',
		rightAlign: true,
		allowMinus: false,
		autoUnmask: false,
		removeMaskOnSubmit: false,
	},
	'sku': {
		mask: '9{*}',
		rightAlign: false,
		allowMinus: false,
		autoUnmask: false,
		removeMaskOnSubmit: false,
	},
	'decimal': {
		alias: 'decimal',
		autoGroup: true,
		rightAlign: true,
		digits: ( conf.decimals > 0 ) ? conf.decimals : 2,
		digitsOptional: false,
		placeholder: parseFloat( 0 ).toCurrency( ( conf.decimals > 0 ) ? conf.decimals : 2 ),
		allowMinus: false,
		autoUnmask: true,
		removeMaskOnSubmit: true,
		groupSeparator: conf.thousands_separator,
		radixPoint: conf.decimal_point,
		onUnMask: function( maskedValue, unMaskedValue ) // In case we change the default chars for grouping and decimaling
		{
			if ( typeof unMaskedValue != "undefined" && unMaskedValue.indexOf( "," ) > -1 )
			{
				unMaskedValue = unMaskedValue.replace( ',', '.' );
			}

			return unMaskedValue;
		}
	},
	'quantity': {
		alias: 'decimal',
		autoGroup: true,
		rightAlign: true,
		digits: conf.qty_decimals,
		digitsOptional: false,
		placeholder: parseFloat( 0 ).toCurrency( ( conf.qty_decimals > 0 ) ? conf.qty_decimals : 2 ),
		allowMinus: false,
		autoUnmask: true,
		removeMaskOnSubmit: true,
		groupSeparator: conf.thousands_separator,
		radixPoint: ( conf.qty_decimals > 0 ) ? conf.decimal_point : '',
		onUnMask: function( maskedValue, unMaskedValue ) // In case we change the default chars for grouping and decimaling
		{
			if ( typeof unMaskedValue != "undefined" && unMaskedValue.indexOf( "," ) > -1 )
			{
				unMaskedValue = unMaskedValue.replace( ',', '.' );
			}

			return unMaskedValue;
		}
	},
	'ruc': {
		mask: '9{*}[-9{1}]',
		greedy: false,
		rightAlign: true,
		allowMinus: false,
		autoUnmask: false,
		removeMaskOnSubmit: false,
	},
	'bill': {
		mask: '9{3}-9{3}-9{*}',
		rightAlign: true,
		allowMinus: false,
		autoUnmask: false,
		removeMaskOnSubmit: false,
	},
	'phone': {
		mask: '*{*}',
		definitions: {
			'*': {
				validator: "[0-9\-]",
				cardinality: 1,
			}
		},
		rightAlign: false,
		allowMinus: false,
		autoUnmask: false,
		removeMaskOnSubmit: false,
	},
	'username': {
		mask: 'U{*}',
		definitions: {
			'U': {
				validator: "[a-z0-9]",
				casing: 'lower'
			}
		},
	},
	'ip': {
		alias: 'ip',
		removeMaskOnSubmit: false,
	},
	'date_birthday': {
		selectYears: 30,
		selectMonths: true,
		min: -35600, // 100 años
		max: -5340, // 15 años
		format: conf.date_format,
		formatSubmit: 'yyyy-mm-dd',
	},
	'date': {
		selectYears: 30,
		selectMonths: true,
		format: conf.date_format,
		formatSubmit: 'yyyy-mm-dd',
	},
	'time': {
		format: 'HH:i',
		interval: 5
	}
}
/*
input_options = {
	'currency': {
		alias: 'currency',
		autoGroup: true,
		rightAlign: true,
		digits: conf.decimals,
		digitsOptional: false,
		prefix: '',
		placeholder: '0.00',
		allowMinus: false,
		removeMaskOnSubmit: true,
	},
	'currency_ext': {
		alias: 'currency',
		autoGroup: true,
		rightAlign: true,
		digits: 5,
		digitsOptional: false,
		prefix: '',
		placeholder: '0.00',
		allowMinus: false,
		removeMaskOnSubmit: true,
	},
	'currency_foreign': {
		alias: 'currency',
		autoGroup: true,
		rightAlign: true,
		digits: 2,
		digitsOptional: false,
		prefix: '',
		placeholder: '0.00',
		allowMinus: false,
		removeMaskOnSubmit: true,
	},
	'integer': {
		alias: 'integer',
		autoGroup: true,
		rightAlign: true,
		digits: 0,
		groupSeparator: ',',
		placeholder: '0',
		allowMinus: false,
		autoUnmask: true,
		removeMaskOnSubmit: true,
	},
	'raw_integer': {
		mask: '9{*}',
		rightAlign: true,
		allowMinus: false,
		autoUnmask: false,
		removeMaskOnSubmit: false,
	},
	'sku': {
		mask: '9{*}',
		rightAlign: false,
		allowMinus: false,
		autoUnmask: false,
		removeMaskOnSubmit: false,
	},
	'decimal': {
		alias: 'decimal',
		autoGroup: true,
		rightAlign: true,
		digits: conf.decimals,
		digitsOptional: false,
		radixPoint: '.',
		groupSeparator: ',',
		placeholder: '0.00',
		allowMinus: false,
		autoUnmask: true,
		removeMaskOnSubmit: true,
	},
	'quantity': {
		alias: 'decimal',
		autoGroup: true,
		rightAlign: true,
		digits: conf.qty_decimals,
		digitsOptional: false,
		radixPoint: '.',
		groupSeparator: ',',
		placeholder: '0.00',
		allowMinus: false,
		autoUnmask: true,
		removeMaskOnSubmit: true,
	},
	'ruc': {
		mask: '9{*}[-9{1}]',
		greedy: false,
		rightAlign: true,
		allowMinus: false,
		autoUnmask: false,
		removeMaskOnSubmit: false,
	},
	'bill': {
		mask: '9{3}-9{3}-9{*}',
		rightAlign: true,
		allowMinus: false,
		autoUnmask: false,
		removeMaskOnSubmit: false,
	},
	'phone': {
		mask: '*{*}',
		definitions: {
			'*': {
				validator: "[0-9\-]",
				cardinality: 1,
			}
		},
		rightAlign: false,
		allowMinus: false,
		autoUnmask: false,
		removeMaskOnSubmit: false,
	},
	'ip': {
		alias: 'ip',
		removeMaskOnSubmit: false,
	},
	'date_birthday': {
		selectYears: 30,
		selectMonths: true,
		min: -35600, // 100 años
		max: -5340, // 15 años
		format: conf.date_format,
		formatSubmit: 'yyyy-mm-dd',
	},
	'date': {
		selectYears: 30,
		selectMonths: true,
		format: conf.date_format,
		formatSubmit: 'yyyy-mm-dd',
	},
	'time': {
		format: 'HH:i',
		interval: 5
	}
}
*/
actions_enabled = null;

jQuery(function($){
	
	/**
	 * -------------------------------------------------------------------------------------
	 * ACTIVE LABEL WHEN FOCUS THE FIELD
	 * -------------------------------------------------------------------------------------
	 */

	if ( $( "input[type=text], input[type=password], input[type=email], input[type=tel], textarea, select" ).length > 0 )
	{
		$( "input[type=text], input[type=password], input[type=email], input[type=tel], textarea, select" ).on( 'focus', function(){
			var parent = $( this ).parent();
			if ( parent.hasClass( 'input-group' ) ) parent = parent.parent();
			parent.find( '.control-label' ).addClass( 'label-focus' );
			parent.addClass( 'form-group-focus' );
		}).on( 'blur', function(){
			var parent = $( this ).parent();
			if ( parent.hasClass( 'input-group' ) ) parent = parent.parent();
			parent.find( '.control-label' ).removeClass( 'label-focus' );
			parent.removeClass( 'form-group-focus' );
		});

		// SELECT 2 behavior on open and close to change the label class
		$( 'select' ).on( 'select2:open', function( el ){
			var parent = $( el.currentTarget ).parent();
			parent.find( '.control-label' ).addClass( 'label-focus' );
			parent.addClass( 'form-group-focus' );
		}).on( 'select2:close', function( el ){
			var parent = $( el.currentTarget ).parent();
			parent.find( '.control-label' ).removeClass( 'label-focus' );
			parent.addClass( 'form-group-focus' );
		});
	}

	/**
	 * -------------------------------------------------------------------------------------
	 * ERROR ON FIELDS
	 * -------------------------------------------------------------------------------------
	 */
	
	if ( $( ".field-error" ).length > 0 )
	{
		$( ".field-error" ).each(function(){
			$( this ).parent().addClass( 'full-field-error' );
			$( this ).parent().find( 'input, select, textarea, label.btn' ).on( 'focus click change', function(){
				var parent = $( this ).parent();
				if ( parent.hasClass( 'input-group' ) || parent.hasClass( 'btn-group-toggle' ) ) parent = parent.parent();
				if ( parent.hasClass( 'btn-group' ) ) parent = parent.parent().parent();
				parent.removeClass( 'full-field-error' );
				parent.find( '.field-error' ).slideUp();
			});
		});
	}

	/**
	 * -------------------------------------------------------------------------------------
	 * INPUTMASK EXTENDS
	 * -------------------------------------------------------------------------------------
	 */

	if ( typeof Inputmask !== "undefined" )
	{
		Inputmask.extendAliases({
			'integer': {
				autoGroup: true,
				rightAlign: true,
				digits: 0,
				placeholder: '0',
				allowMinus: false,
				autoUnmask: true,
				removeMaskOnSubmit: true,
				groupSeparator: conf.thousands_separator,
				radixPoint: '',
			}
		});
	}

	/**
	 * -------------------------------------------------------------------------------------
	 * INPUTMASK OPTIONS
	 * -------------------------------------------------------------------------------------
	 */
	
	if ( $( ".input-currency" ).length > 0 ) $( ".input-currency" ).inputmask( input_options.currency );

	if ( $( ".input-currency-ext" ).length > 0 ) $( ".input-currency-ext" ).inputmask( input_options.currency_ext );
	
	if ( $( ".input-currency-foreign" ).length > 0 ) $( ".input-currency-foreign" ).inputmask( input_options.currency_foreign );

	if ( $( ".input-integer" ).length > 0 ) $( ".input-integer" ).inputmask( input_options.integer );

	if ( $( ".input-raw-integer" ).length > 0 ) $( ".input-raw-integer" ).inputmask( input_options.raw_integer );

	if ( $( ".input-sku" ).not( '[type="hidden"]' ).length > 0 ) $( ".input-sku" ).inputmask( input_options.sku );

	if ( $( ".input-decimal" ).length > 0 ) $( ".input-decimal" ).inputmask( input_options.decimal );

	if ( $( ".input-quantity" ).not( '[type="hidden"]' ).length > 0 ) $( ".input-quantity" ).inputmask( input_options.quantity );

	if ( $( ".input-ruc" ).length > 0 ) $( ".input-ruc" ).inputmask( input_options.ruc );

	if ( $( ".input-bill" ).length > 0 ) $( ".input-bill" ).inputmask( input_options.bill );

	if ( $( ".input-phone" ).length > 0 ) $( ".input-phone" ).inputmask( input_options.phone );

	if ( $( ".input-username" ).length > 0 ) $( ".input-username" ).inputmask( input_options.username );

	if ( $( ".input-ip-address" ).length > 0 ) $( ".input-ip-address" ).inputmask( input_options.ip );

	/**
	 * -------------------------------------------------------------------------------------
	 * PICK A DATE OPTIONS
	 * -------------------------------------------------------------------------------------
	 */
	
	if ( $( ".input-date-birthday" ).length > 0 ) $( ".input-date-birthday" ).pickadate( input_options.date_birthday );

	if ( $( ".input-date" ).length > 0 ) $( ".input-date" ).pickadate( input_options.date );

	if ( $( ".input-time" ).length > 0 )
	{
		$( ".input-time" ).pickatime( input_options.time );
/*
		$( ".input-time" ).timepicki({
			start_time : ['00','00'],
			show_meridian : false,
			step_size_minutes : 5,
			min_hour_value : 0,
			max_hour_value : 23,
			overflow_minutes : false,
			increase_direction : 'up',
			disable_keyboard_mobile : true
		});

		$( ".input-time" ).inputmask({ 
			regex: "^(0[0-9]|1[0-9]|2[0-3]):[0-5][0-9]$",
			allowMinus: false,
			autoUnmask: false,
			removeMaskOnSubmit: false,
		});
*/
	}

	/**
	 * -------------------------------------------------------------------------------------
	 * GET THE ENABLED ACTIONS FOR THE CURRENT USER
	 * -------------------------------------------------------------------------------------
	 */

	$.getJSON( conf.site_url + 'ajax/get_current_user_actions_enabled?controller=' + conf.current_controller, function( response, status ) {
		if ( status == "success" && response ) actions_enabled = response;
	});

});

/**
 * MAIN TOKEN REBUILDER FOR FORMS THAT USE EXTENSIVELY AJAX via POST
 */

rebuild_main_tokens = function()
{
	jQuery( function($){
		$.getJSON( conf.site_url + 'ajax/get_csrf_hash', function( response, status ) {
			if ( status == "success" ) $( 'input[name="' + response.csrf_name + '"]' ).val( response.csrf_hash );
		});
	});
}
