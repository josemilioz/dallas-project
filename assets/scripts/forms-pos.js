jQuery(function($){
	
	var row = $( "#users-card table tbody tr:first" ).clone();
	$( "#users-card table tbody tr:first" ).remove();

	/**
	 * SELECT 2 for USER ID
	 */

	$( "select#user_id" ).select2({
		language: conf.language_short,
	});

	/**
	 * ADD USER
	 */
	
	$( '.btn-add-user' ).on( 'click', function( ev ){
		ev.preventDefault();

		var user_name = $( "select#user_id option:selected" ).text();
		var user_id = parseInt( $( "select#user_id" ).val() );

		var new_row = row.clone();
			new_row.find( 'td:first' ).text( user_name );
			new_row.find( 'input' ).val( user_id );

		var disabled = [];

		$( "#users-card table tbody tr input" ).each( function(){
			disabled.push( parseInt( $( this ).val() ) );
		});

		if ( $( "#users-card table tbody tr" ).length <= 0 )
		{
			new_row.appendTo( '#users-card table tbody' );
		}
		else
		{
			if ( $.inArray( user_id, disabled ) < 0 )
			{
				new_row.appendTo( '#users-card table tbody' );
			}
		}

		rebuild_tooltips();
	});

	/**
	 * DELETE FROM LIST
	 */
	
	$( document ).on( 'click', '.btn-erase', function( ev ){
		ev.preventDefault();
		var parent = $( this ).parent().parent();
		parent.remove();
		rebuild_tooltips();
	});

	/**
	 * DELETE any empty option in both dropdowns
	 */
	$( 'select#categories_list option, select#categories_enabled option' ).each( function(){
		if ( $( this ).text() == "" || $( this ).html() == "&nbsp;" || $( this ).text() == " " )
		{
			$( this ).remove();
		}
	});

	/**
	 * MOVEMENTS BETWEEN OPTIONS
	 */
	
	// Button right
	$( 'button.put-right' ).click( function( ev ){
		ev.preventDefault();
		var sel_options = $( 'select#categories_list option:selected' );
		if ( sel_options.length > 0 )
		{
			$( 'select#categories_enabled' ).append( $( sel_options ).clone() );
			$( sel_options ).remove();
		}
	});

	// Button all right
	$( 'button.put-all-right' ).click( function( ev ){
		ev.preventDefault();
		var sel_options = $( 'select#categories_list option' );
		if ( sel_options.length > 0 )
		{
			$( 'select#categories_enabled' ).append( $( sel_options ).clone() );
			$( sel_options ).remove();
		}
	});

	// Button left
	$( 'button.put-left' ).click( function( ev ){
		ev.preventDefault();
		var sel_options = $( 'select#categories_enabled option:selected' );
		if ( sel_options.length > 0 )
		{
			$( 'select#categories_list' ).append( $( sel_options ).clone() );
			$( sel_options ).remove();
		}
	});

	// Button all left
	$( 'button.put-all-left' ).click( function( ev ){
		ev.preventDefault();
		var sel_options = $( 'select#categories_enabled option' );
		if ( sel_options.length > 0 )
		{
			$( 'select#categories_list' ).append( $( sel_options ).clone() );
			$( sel_options ).remove();
		}
	});

	/**
	 * SELECT all options from active on submit form
	 */
	
	$( 'button[type="submit"]' ).click( function( ev ){
		$( 'select#categories_enabled option' ).each( function(){
			this.selected = true;
		});
	});
});