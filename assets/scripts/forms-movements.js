jQuery(function($){

	/**
	 * UPLOAD FILE
	 */

	build_file_upload = function()
	{
		$( document ).find( "#fileupload" ).fileupload({
			url: conf.site_url + 'files/upload',
			dataType: 'json',
			type: 'POST',
			formData: {
				'location' 		: '/uploads/movements/',
				'controller' 	: 'movements',
				'registry_id' 	: ( $( "#movement_id" ).val() !== "" ) ? $( "#movement_id" ).val() : null,
			},
			start: function(){
				$( '#progress .progress-bar' ).removeClass( 'bg-danger' ).removeClass( 'bg-success' );
			},
			progressall: function ( e, data ){
				var progress = parseInt( data.loaded / data.total * 100, 10 );
				$( '#progress .progress-bar' ).css( 'width', progress + '%' );
			},
			complete: function( object, status ){
				var existant_ids = [];

				if ( $( 'input[name^="file_id"]' ).length > 0 )
				{
					$( 'input[name^="file_id"]' ).each( function( ind, val ){
						existant_ids.push( $( this ).val() );
					});
				}

				if ( status == "success" && object.responseJSON.upload )
				{
					$.ajax({
						url: 		conf.site_url + 'files/multiple_files_retrieving',
						dataType: 	'json',
						type: 		'POST',
						data: {
							existant_ids 	: existant_ids,
							new_upload 		: object.responseJSON.upload,
						},
						success: function( response )
						{
							if ( response.html_form )
							{
								$( document ).find( "#files-card .card-body *" ).remove();
								$( response.html_form ).appendTo( '#files-card .card-body' );
								rebuild_tooltips();
								build_file_upload();
								build_file_delete();
							}
						}
					});
				}
			},
			error: function( object, status, error ){
				if ( object.responseJSON.error ) alert( object.responseJSON.error );
				$( '#progress .progress-bar' ).prop( 'style', false );
			}
		});
	};

	/**
	 * DELETE FILE
	 */
	
	build_file_delete = function()
	{
		$( document ).on( 'click', '.btn-delete-file', function( ev ){
			ev.preventDefault();

			var file_id = $( this ).parent().find( 'input[name^="file_id"]' ).val();

			if ( file_id !== "" )
			{
				var remaining_ids = [];

				if ( $( 'input[name^="file_id"]' ).length > 0 )
				{
					$( 'input[name^="file_id"]' ).each( function( ind, val ){
						if ( $( this ).val() != file_id ) remaining_ids.push( $( this ).val() );
					});
				}

				$.ajax({
					url: 		conf.site_url + 'files/delete_on_multiple',
					type: 		'POST',
					dataType: 	'json',
					data: { 
						file_id 		: file_id,
						remaining_ids	: remaining_ids,
						view_name 		: 'files/model-multiple-files',
					},
					success: function( response )
					{
						if ( response.html_form )
						{
							$( document ).find( "#files-card .card-body *" ).remove();
							$( response.html_form ).appendTo( '#files-card .card-body' );
							rebuild_tooltips();
							build_file_upload();
						}
					},
					error: function( object, status, error ){
						if ( object.responseJSON.error ) alert( object.responseJSON.error );
					}
				});
			}
		});
	};

	/**
	 * INIT
	 */
	
	build_file_upload();
	build_file_delete();
	
	/**
	 * SELECT2 INIT
	 */
	
	$( "select#label, select#book" ).select2({
		language: conf.language_short,
		tags: true,
		createTag: function( params ){
			return {
				id: params.term,
				text: params.term,
				newOption: true
			}
		},
		templateResult: function( data ){
			var $result = $( "<span></span>" );
			$result.text( data.text );
			if ( data.text == "" ) $result.html( "&nbsp;" );
			if ( data.newOption ) $result.append( messages.tag_new );
			return $result;
		}
	});

	$( "select#assignee" ).select2({
		language: conf.language_short,
	});

	/**
	 * LIVE EXCHANGE CALCULATION
	 * Uses 'exchange_values' from main.js
	 */

	calculate_exchange = function()
	{
		if ( ! $.isEmptyObject( exchange_values ) )
		{
			var element 	= $( this );
			if ( ! element.is( 'input' ) ) element = $( "#amount" );
			var currency	= element.data( 'currency' );
			var not 		= element.attr( 'id' );
			var amount 		= ( element.inputmask( 'unmaskedvalue' ) !== "" ) ? parseFloat( element.inputmask( 'unmaskedvalue' ) ) : 0;
			var calculus 	= parseFloat( amount * parseFloat( exchange_values[currency].sale ) );

			if ( not !== "amount" ) $( "#amount" ).inputmask( 'setvalue', calculus );

			$( ".input-exchanges:not(#" + not + ")" ).each( function( index, element ){
				var currency 	= $( element ).data( 'currency' );
				var amount 		= ( $( "#amount" ).inputmask( 'unmaskedvalue' ) !== "" ) ? parseFloat( $( "#amount" ).inputmask( 'unmaskedvalue' ) ) : 0;
				var calculus 	= parseFloat( amount / parseFloat( exchange_values[currency].sale ) );
				$( this ).inputmask( 'setvalue', calculus );
			});
		}
	};

	
	$( document ).on( 'keyup', '.input-exchanges, #amount', calculate_exchange );
	var do_exchange_on_load = setInterval( function(){
		if ( ! $.isEmptyObject( exchange_values ) )
		{
			calculate_exchange();
			clearInterval( do_exchange_on_load );
		}
	}, 100 );
});