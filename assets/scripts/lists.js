jQuery(function($){

	/**
	 * -----------------------------------------------------------------------------------------------
	 * CONFIRM MESSAGES
	 * -----------------------------------------------------------------------------------------------
	 */
	
	$( '.btn-trash, .btn-delete, .btn-bulk, .btn-enable, .btn-disable, .btn-confirm' ).on( 'click', function( ev ){
		ev.preventDefault(); 
		ev.stopPropagation();
		ev.stopImmediatePropagation();
		var message = ( $( this ).data( 'confirm' ) ) ? $( this ).data( 'confirm' ) : '';
		confirm( message, the_confirm_callback, $( this ) );
	});

	/**
	 * -----------------------------------------------------------------------------------------------
	 * SELECT ALL
	 * -----------------------------------------------------------------------------------------------
	 */
	
	$( ".select_all_cb" ).click(function( ev ){
		var form = $( this ).parentsUntil( 'form' ).parent();
		form = $( form[0] );

		if ( $( this ).is( ":checked" ) )
		{
			form.find( 'input[type="checkbox"]' ).attr( 'checked', 'checked' );
		}
		else
		{
			form.find( 'input[type="checkbox"]' ).removeAttr( 'checked' );
		}
	});

	/**
	 * -----------------------------------------------------------------------------------------------
	 * BRIEFS
	 * -----------------------------------------------------------------------------------------------
	 */

	var modal_html_loading = $( "#modal-brief" ).find( '.modal-body' ).html();

	$( document ).on( 'click', 'a.brief',function( ev ){
		ev.preventDefault();
		var link_href = $( this ).attr( 'href' );
		$( "#modal-brief" ).on( 'hidden.bs.modal', function(){
			// Reset
			$( this ).find( '.modal-body' ).html( modal_html_loading );
			$( this ).find( '.modal-dialog' ).removeClass( 'modal-xl' ).addClass( 'modal-lg' );
		}).modal( 'show' );

		$( "#modal-brief" ).find( '.modal-body' ).load( link_href + " #brief-wrapper", function( body ){
			if ( $( body ).find( '#brief-wrapper' ).length <= 0 )
			{
				$( "#modal-brief" ).find( '.modal-body' ).html( '<h4 class="mb-2"><strong>' + $( body ).find( '#main-top-alert h4' ).text() + '</strong></h4><p class="mb-0">' + $( body ).find( '#main-top-alert p' ).text() + '</p>' );
			}
		});

		if ( $( this ).data( 'enlarge-modal' ) ) $( "#modal-brief" ).find( '.modal-dialog' ).addClass( 'modal-xl' );
		if ( $( this ).data( 'shorten-modal' ) ) $( "#modal-brief" ).find( '.modal-dialog' ).removeClass( 'modal-lg' );
		if ( $( this ).data( 'normal-modal' ) ) $( "#modal-brief" ).find( '.modal-dialog' ).addClass( 'modal-lg' );

		// Rebuild the tooltips
		$( document ).ajaxComplete( function() { rebuild_tooltips(); });
	});
});