jQuery(function($){

	/**
	 * ON FOCUS, CHANGE THE PARENT CLASS OF THE INPUT
	 */

	if ( $( "input[type=text], input[type=password], input[type=email]" ).length > 0 )
	{
		$( "input[type=text], input[type=password], input[type=email]" ).on( 'focus', function(){
			$( this ).parent().find( 'label' ).addClass( 'label-focus' );
		}).on( 'blur', function(){
			$( this ).parent().find( 'label' ).removeClass( 'label-focus' );
		}).on( 'keyup', function(){
			if ( $( this ).val() !== "" )
			{
				$( this ).parent().find( 'label' ).addClass( 'has-content' );
			}
			else
			{
				$( this ).parent().find( 'label' ).removeClass( 'has-content' );
			}
		});

		$( "input[type=text], input[type=password], input[type=email]" ).each( function(){
			if ( $( this ).val() !== "" )
			{
				$( this ).parent().find( 'label' ).addClass( 'label-focus has-content' );
			}
		});
	}
});