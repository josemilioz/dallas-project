-- MySQL dump 10.13  Distrib 5.7.26, for osx10.10 (x86_64)
--
-- Host: localhost    Database: dallasDB
-- ------------------------------------------------------
-- Server version	5.7.26

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `dall_auth_groups`
--

DROP TABLE IF EXISTS `dall_auth_groups`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `dall_auth_groups` (
  `id` mediumint(8) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(20) NOT NULL,
  `description` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `dall_auth_groups`
--

LOCK TABLES `dall_auth_groups` WRITE;
/*!40000 ALTER TABLE `dall_auth_groups` DISABLE KEYS */;
INSERT INTO `dall_auth_groups` VALUES (1,'superadmin','Super Administrator'),(2,'admin','Administrator'),(3,'manager','Manager'),(4,'accountant','Accountant'),(5,'member','Member'),(6,'clerk','Clerk');
/*!40000 ALTER TABLE `dall_auth_groups` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `dall_auth_login_attempts`
--

DROP TABLE IF EXISTS `dall_auth_login_attempts`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `dall_auth_login_attempts` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `ip_address` varchar(15) NOT NULL,
  `login` varchar(100) NOT NULL,
  `time` int(11) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `dall_auth_login_attempts`
--

LOCK TABLES `dall_auth_login_attempts` WRITE;
/*!40000 ALTER TABLE `dall_auth_login_attempts` DISABLE KEYS */;
/*!40000 ALTER TABLE `dall_auth_login_attempts` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `dall_auth_logs`
--

DROP TABLE IF EXISTS `dall_auth_logs`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `dall_auth_logs` (
  `log_id` bigint(32) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(11) unsigned DEFAULT NULL,
  `uri` varchar(255) DEFAULT NULL,
  `date` datetime DEFAULT NULL,
  `ip` varchar(30) DEFAULT NULL,
  `browser` varchar(100) DEFAULT NULL,
  `version` varchar(10) DEFAULT NULL,
  `os` varchar(50) DEFAULT NULL,
  `result` text,
  `type` varchar(10) DEFAULT NULL,
  `data` text,
  PRIMARY KEY (`log_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `dall_auth_logs`
--

LOCK TABLES `dall_auth_logs` WRITE;
/*!40000 ALTER TABLE `dall_auth_logs` DISABLE KEYS */;
/*!40000 ALTER TABLE `dall_auth_logs` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `dall_auth_sessions`
--

DROP TABLE IF EXISTS `dall_auth_sessions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `dall_auth_sessions` (
  `id` varchar(128) NOT NULL,
  `ip_address` varchar(45) NOT NULL,
  `timestamp` int(11) unsigned NOT NULL DEFAULT '0',
  `data` blob NOT NULL,
  PRIMARY KEY (`id`,`ip_address`),
  KEY `ci_sessions_timestamp` (`timestamp`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `dall_auth_sessions`
--

LOCK TABLES `dall_auth_sessions` WRITE;
/*!40000 ALTER TABLE `dall_auth_sessions` DISABLE KEYS */;
INSERT INTO `dall_auth_sessions` VALUES ('bp9ncbu4t1t704h4m1qchsho2drecnuv','::1',1602430690,_binary '__ci_last_regenerate|i:1602430688;identity|s:5:\"admin\";username|s:5:\"admin\";email|s:22:\"joseemilio@zaracho.com\";user_id|s:1:\"1\";old_last_login|s:10:\"1602428786\";');
/*!40000 ALTER TABLE `dall_auth_sessions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `dall_auth_settings`
--

DROP TABLE IF EXISTS `dall_auth_settings`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `dall_auth_settings` (
  `setting_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `key` varchar(255) DEFAULT NULL,
  `value` text,
  `created` datetime DEFAULT NULL,
  `creator` int(11) unsigned DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  `modifier` int(11) unsigned DEFAULT NULL,
  PRIMARY KEY (`setting_id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `dall_auth_settings`
--

LOCK TABLES `dall_auth_settings` WRITE;
/*!40000 ALTER TABLE `dall_auth_settings` DISABLE KEYS */;
INSERT INTO `dall_auth_settings` VALUES (1,'company_name','The Company','2020-10-09 10:41:07',1,NULL,NULL),(2,'language','en_US','2020-10-09 10:41:07',1,NULL,NULL),(3,'items_per_page','10','2020-10-09 10:41:07',1,NULL,NULL),(4,'pages_to_show','3','2020-10-09 10:41:07',1,NULL,NULL),(5,'stock','{\"min_warning\":\"10.00\",\"min_danger\":\"5.00\"}','2020-10-09 10:41:07',1,NULL,NULL),(6,'qty_decimals','2','2020-10-09 10:41:07',1,NULL,NULL),(7,'currency','USD','2020-10-09 10:41:07',1,NULL,NULL),(8,'decimals','2','2020-10-09 10:41:07',1,NULL,NULL),(9,'auto_update_hours','6','2020-10-09 10:41:07',1,NULL,NULL),(10,'auto_update_source','http://emaitre.net/_assets/cotizacion.xml','2020-10-09 10:41:07',1,NULL,NULL);
/*!40000 ALTER TABLE `dall_auth_settings` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `dall_auth_users`
--

DROP TABLE IF EXISTS `dall_auth_users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `dall_auth_users` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `ip_address` varchar(45) DEFAULT NULL,
  `username` varchar(100) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `salt` varchar(255) DEFAULT NULL,
  `activation_code` varchar(40) DEFAULT NULL,
  `forgotten_password_code` varchar(40) DEFAULT NULL,
  `forgotten_password_time` int(11) unsigned DEFAULT NULL,
  `remember_code` varchar(40) DEFAULT NULL,
  `restore_password` tinyint(1) unsigned DEFAULT NULL,
  `first_name` varchar(50) DEFAULT NULL,
  `last_name` varchar(50) DEFAULT NULL,
  `phone` varchar(20) DEFAULT NULL,
  `email` varchar(100) DEFAULT NULL,
  `language` varchar(5) DEFAULT NULL,
  `last_login` int(11) unsigned DEFAULT NULL,
  `meta_vars` text,
  `created` datetime DEFAULT NULL,
  `creator` int(11) unsigned DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  `modifier` int(11) unsigned DEFAULT NULL,
  `active` tinyint(1) unsigned NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `dall_auth_users`
--

LOCK TABLES `dall_auth_users` WRITE;
/*!40000 ALTER TABLE `dall_auth_users` DISABLE KEYS */;
INSERT INTO `dall_auth_users` VALUES (1,'127.0.0.1','admin','$2y$08$t4xdneLGung7OTtW4FnO9OL3Co/WMflr.ZnP5XUO5g/5W1XdoLRRe',NULL,NULL,NULL,NULL,NULL,NULL,'José Emilio','Zaracho Zarate','0985775407','joseemilio@zaracho.com','en_US',1602429001,NULL,'2017-08-29 21:27:37',0,'2020-01-16 17:22:55',1,1),(2,'127.0.0.1','marga','$2y$08$Xe3TcKEzfA8gpIxQDv.wVeOsV/OflQ8BFrjtMI4rKpk8Rp/I/lXGy',NULL,NULL,NULL,NULL,NULL,NULL,'Marga','Perez','','darkhun73r@gmail.com','es_ES',1602428943,NULL,'2017-08-29 21:27:37',0,'2018-12-15 19:37:55',1,1);
/*!40000 ALTER TABLE `dall_auth_users` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `dall_auth_users_groups`
--

DROP TABLE IF EXISTS `dall_auth_users_groups`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `dall_auth_users_groups` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(11) unsigned NOT NULL,
  `group_id` mediumint(8) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `uc_users_groups` (`user_id`,`group_id`),
  KEY `fk_pro_auth_users_groups_users1_idx` (`user_id`),
  KEY `fk_pro_auth_users_groups_groups1_idx` (`group_id`),
  CONSTRAINT `fk_pro_auth_users_groups_groups1` FOREIGN KEY (`group_id`) REFERENCES `dall_auth_groups` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  CONSTRAINT `fk_pro_auth_users_groups_users1` FOREIGN KEY (`user_id`) REFERENCES `dall_auth_users` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `dall_auth_users_groups`
--

LOCK TABLES `dall_auth_users_groups` WRITE;
/*!40000 ALTER TABLE `dall_auth_users_groups` DISABLE KEYS */;
INSERT INTO `dall_auth_users_groups` VALUES (3,1,1),(4,2,2);
/*!40000 ALTER TABLE `dall_auth_users_groups` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `dall_categories`
--

DROP TABLE IF EXISTS `dall_categories`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `dall_categories` (
  `category_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `description` text,
  `unit_measure` varchar(10) DEFAULT NULL,
  `unit_quantity` decimal(15,2) unsigned DEFAULT NULL,
  `currency` varchar(3) DEFAULT NULL,
  `tax` varchar(50) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `creator` int(11) unsigned DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  `modifier` int(11) unsigned DEFAULT NULL,
  `trash` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`category_id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `dall_categories`
--

LOCK TABLES `dall_categories` WRITE;
/*!40000 ALTER TABLE `dall_categories` DISABLE KEYS */;
INSERT INTO `dall_categories` VALUES (3,'Inoculantes','Bacterias para el crecimiento de plantas','doses',1.00,'USD','tax_10','2019-03-02 15:26:53',1,'2020-10-09 10:51:38',1,0),(4,'Coadyuvantes',NULL,NULL,NULL,NULL,NULL,'2019-03-02 15:27:23',1,NULL,NULL,0);
/*!40000 ALTER TABLE `dall_categories` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `dall_clients`
--

DROP TABLE IF EXISTS `dall_clients`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `dall_clients` (
  `client_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `first_name` varchar(255) DEFAULT NULL,
  `last_name` varchar(255) DEFAULT NULL,
  `ruc` varchar(50) DEFAULT NULL,
  `address` varchar(255) DEFAULT NULL,
  `city` varchar(50) DEFAULT NULL,
  `state` varchar(50) DEFAULT NULL,
  `country` varchar(50) DEFAULT NULL,
  `telephone` varchar(50) DEFAULT NULL,
  `cellphone` varchar(50) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `website` varchar(255) DEFAULT NULL,
  `birthday` date DEFAULT NULL,
  `seller_id` int(11) unsigned DEFAULT NULL,
  `meta_vars` text,
  `observations` text,
  `created` datetime DEFAULT NULL,
  `creator` int(11) unsigned DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  `modifier` int(11) unsigned DEFAULT NULL,
  `trash` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`client_id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `dall_clients`
--

LOCK TABLES `dall_clients` WRITE;
/*!40000 ALTER TABLE `dall_clients` DISABLE KEYS */;
INSERT INTO `dall_clients` VALUES (1,'Marcelo','Gallardo','34090034',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1,NULL,NULL,'2018-12-21 22:29:09',1,'2020-10-08 10:15:52',1,0),(2,'Abelina','Gutierrez','340930493',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2018-12-21 22:29:20',1,NULL,NULL,0),(3,'Abel','Acosta','340009444',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2018-12-21 22:30:20',1,NULL,NULL,0),(4,'Sergio','Trovato','4449343-3',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2018-12-21 22:30:32',1,NULL,NULL,0),(5,'Itapúa Poty SA',NULL,'80053424-2',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2019-01-23 15:07:30',1,'2019-03-01 18:11:02',1,0),(6,'Cosito',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2019-01-23 15:53:56',1,NULL,NULL,0),(7,'Cinderella',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2019-01-23 15:57:59',1,NULL,NULL,0),(8,'Perros','De La Calle SA',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2019-01-23 15:58:10',1,NULL,NULL,0),(9,'Yehimi','Alison','3498343-3',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2019-01-23 15:58:26',1,NULL,NULL,0),(10,'Carlitos','Gutierrez','349852-4',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2020-01-20 12:38:09',1,NULL,NULL,0);
/*!40000 ALTER TABLE `dall_clients` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `dall_files`
--

DROP TABLE IF EXISTS `dall_files`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `dall_files` (
  `file_id` bigint(32) unsigned NOT NULL AUTO_INCREMENT,
  `registry_id` int(11) unsigned DEFAULT NULL,
  `controller` varchar(255) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `name_ext` varchar(255) DEFAULT NULL,
  `original_name` varchar(255) DEFAULT NULL,
  `relative_location` varchar(255) DEFAULT NULL,
  `size` bigint(32) unsigned DEFAULT NULL,
  `mimetype` varchar(255) DEFAULT NULL,
  `extension` varchar(10) DEFAULT NULL,
  `order` int(11) unsigned DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `creator` int(11) unsigned DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  `modifier` int(11) unsigned DEFAULT NULL,
  `trash` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`file_id`),
  KEY `nullify_on_product_delete` (`registry_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `dall_files`
--

LOCK TABLES `dall_files` WRITE;
/*!40000 ALTER TABLE `dall_files` DISABLE KEYS */;
INSERT INTO `dall_files` VALUES (1,1,'users','1579205529','1579205529.jpg','avatar.jpg','/uploads/avatars/',174090,'image/jpeg','.jpg',NULL,'2020-01-16 17:12:09',1,NULL,NULL,0);
/*!40000 ALTER TABLE `dall_files` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `dall_movements`
--

DROP TABLE IF EXISTS `dall_movements`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `dall_movements` (
  `movement_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `date` date DEFAULT NULL,
  `type` varchar(50) DEFAULT NULL,
  `label` varchar(255) DEFAULT NULL,
  `book` varchar(255) DEFAULT NULL,
  `amount` decimal(15,2) unsigned DEFAULT NULL,
  `comments` text,
  `transaction_type` varchar(30) DEFAULT NULL,
  `transaction_id` int(11) unsigned DEFAULT NULL,
  `assignee` int(11) unsigned DEFAULT NULL,
  `meta_vars` text,
  `created` datetime DEFAULT NULL,
  `creator` int(11) unsigned DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  `modifier` int(11) unsigned DEFAULT NULL,
  `trash` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`movement_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `dall_movements`
--

LOCK TABLES `dall_movements` WRITE;
/*!40000 ALTER TABLE `dall_movements` DISABLE KEYS */;
INSERT INTO `dall_movements` VALUES (1,'2020-10-08','outcome','Pagos A Personal',NULL,5000000.00,NULL,NULL,NULL,NULL,NULL,'2020-10-08 14:50:48',1,NULL,NULL,0);
/*!40000 ALTER TABLE `dall_movements` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `dall_orders`
--

DROP TABLE IF EXISTS `dall_orders`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `dall_orders` (
  `order_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `client_id` int(11) unsigned DEFAULT NULL,
  `order_date` date DEFAULT NULL,
  `delivery_date` date DEFAULT NULL,
  `expiration_date` date DEFAULT NULL,
  `seller_id` int(11) unsigned DEFAULT NULL,
  `observations` text,
  `total_amount` decimal(15,2) unsigned DEFAULT NULL,
  `map_lat` varchar(100) DEFAULT NULL,
  `map_lon` varchar(100) DEFAULT NULL,
  `meta_vars` text,
  `sold` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `created` datetime DEFAULT NULL,
  `creator` int(11) unsigned DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  `modifier` int(11) unsigned DEFAULT NULL,
  `trash` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`order_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `dall_orders`
--

LOCK TABLES `dall_orders` WRITE;
/*!40000 ALTER TABLE `dall_orders` DISABLE KEYS */;
INSERT INTO `dall_orders` VALUES (2,7,'2020-04-02',NULL,NULL,1,NULL,3000000.00,NULL,NULL,NULL,0,'2020-04-02 20:19:23',1,'2020-04-22 13:23:10',1,0);
/*!40000 ALTER TABLE `dall_orders` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `dall_orders_products`
--

DROP TABLE IF EXISTS `dall_orders_products`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `dall_orders_products` (
  `order_id` int(11) unsigned DEFAULT NULL,
  `product_id` int(11) unsigned DEFAULT NULL,
  `description` text,
  `quantity` decimal(15,2) unsigned DEFAULT NULL,
  `quantity_remaining` decimal(15,2) unsigned DEFAULT NULL,
  `unit_price` decimal(15,2) unsigned DEFAULT NULL,
  `row_total` decimal(15,2) unsigned DEFAULT NULL,
  KEY `delete_on_products` (`product_id`),
  KEY `delete_on_orders` (`order_id`),
  CONSTRAINT `delete_on_orders` FOREIGN KEY (`order_id`) REFERENCES `dall_orders` (`order_id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  CONSTRAINT `delete_on_products` FOREIGN KEY (`product_id`) REFERENCES `dall_products` (`product_id`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `dall_orders_products`
--

LOCK TABLES `dall_orders_products` WRITE;
/*!40000 ALTER TABLE `dall_orders_products` DISABLE KEYS */;
INSERT INTO `dall_orders_products` VALUES (2,3,'Product 1',200.00,100.00,15000.00,3000000.00);
/*!40000 ALTER TABLE `dall_orders_products` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `dall_pos`
--

DROP TABLE IF EXISTS `dall_pos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `dall_pos` (
  `pos_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `printer` varchar(255) DEFAULT NULL,
  `assigned_ip` varchar(100) DEFAULT NULL,
  `type` varchar(50) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `creator` int(11) unsigned DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  `modifier` int(11) unsigned DEFAULT NULL,
  `trash` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`pos_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `dall_pos`
--

LOCK TABLES `dall_pos` WRITE;
/*!40000 ALTER TABLE `dall_pos` DISABLE KEYS */;
INSERT INTO `dall_pos` VALUES (1,'Box 1',NULL,NULL,NULL,'2018-12-29 09:40:21',1,'2020-01-17 17:36:46',1,0);
/*!40000 ALTER TABLE `dall_pos` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `dall_pos_categories`
--

DROP TABLE IF EXISTS `dall_pos_categories`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `dall_pos_categories` (
  `category_id` int(11) unsigned DEFAULT NULL,
  `pos_id` int(11) unsigned DEFAULT NULL,
  KEY `delete_pos_category_on_category` (`category_id`),
  KEY `delete_pos_category_on_pos` (`pos_id`),
  CONSTRAINT `delete_pos_category_on_category` FOREIGN KEY (`category_id`) REFERENCES `dall_categories` (`category_id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  CONSTRAINT `delete_pos_category_on_pos` FOREIGN KEY (`pos_id`) REFERENCES `dall_pos` (`pos_id`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `dall_pos_categories`
--

LOCK TABLES `dall_pos_categories` WRITE;
/*!40000 ALTER TABLE `dall_pos_categories` DISABLE KEYS */;
INSERT INTO `dall_pos_categories` VALUES (3,1),(4,1);
/*!40000 ALTER TABLE `dall_pos_categories` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `dall_pos_users`
--

DROP TABLE IF EXISTS `dall_pos_users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `dall_pos_users` (
  `user_id` int(11) unsigned DEFAULT NULL,
  `pos_id` int(11) unsigned DEFAULT NULL,
  KEY `delete_user_on_pos` (`user_id`),
  KEY `delete_pos_on_pos` (`pos_id`),
  CONSTRAINT `delete_pos_on_pos` FOREIGN KEY (`pos_id`) REFERENCES `dall_pos` (`pos_id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  CONSTRAINT `delete_user_on_pos` FOREIGN KEY (`user_id`) REFERENCES `dall_auth_users` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `dall_pos_users`
--

LOCK TABLES `dall_pos_users` WRITE;
/*!40000 ALTER TABLE `dall_pos_users` DISABLE KEYS */;
INSERT INTO `dall_pos_users` VALUES (2,1),(1,1);
/*!40000 ALTER TABLE `dall_pos_users` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `dall_print_orders`
--

DROP TABLE IF EXISTS `dall_print_orders`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `dall_print_orders` (
  `print_order_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `pos_id` int(11) unsigned DEFAULT NULL,
  `print_order` varchar(50) DEFAULT NULL,
  `authorization_number` varchar(50) DEFAULT NULL,
  `date_start` date DEFAULT NULL,
  `date_end` date DEFAULT NULL,
  `location` varchar(3) DEFAULT NULL,
  `expedition` varchar(3) DEFAULT NULL,
  `number_start` int(11) unsigned DEFAULT NULL,
  `number_end` int(11) unsigned DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `creator` int(11) unsigned DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  `modifier` int(11) unsigned DEFAULT NULL,
  `trash` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`print_order_id`),
  KEY `delete_print_order_on_pos` (`pos_id`),
  CONSTRAINT `delete_print_order_on_pos` FOREIGN KEY (`pos_id`) REFERENCES `dall_pos` (`pos_id`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `dall_print_orders`
--

LOCK TABLES `dall_print_orders` WRITE;
/*!40000 ALTER TABLE `dall_print_orders` DISABLE KEYS */;
INSERT INTO `dall_print_orders` VALUES (1,1,'23847330',NULL,'2018-12-01','2022-12-31','001','003',100,200,'2018-12-29 11:18:24',1,'2020-01-16 18:03:25',1,0);
/*!40000 ALTER TABLE `dall_print_orders` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `dall_products`
--

DROP TABLE IF EXISTS `dall_products`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `dall_products` (
  `product_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `category_id` int(11) unsigned DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `description` text,
  `unit_measure` varchar(10) DEFAULT NULL,
  `unit_quantity` decimal(15,2) unsigned DEFAULT NULL,
  `price` decimal(15,2) unsigned DEFAULT NULL,
  `currency` varchar(3) DEFAULT NULL,
  `tax` varchar(50) DEFAULT NULL,
  `min_stock_warning` decimal(15,2) unsigned DEFAULT NULL,
  `min_stock_danger` decimal(15,2) unsigned DEFAULT NULL,
  `free` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `meta_vars` text,
  `created` datetime DEFAULT NULL,
  `creator` int(11) unsigned DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  `modifier` int(11) unsigned DEFAULT NULL,
  `trash` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`product_id`),
  KEY `nullify_category_on_delete` (`category_id`),
  CONSTRAINT `nullify_category_on_delete` FOREIGN KEY (`category_id`) REFERENCES `dall_categories` (`category_id`) ON DELETE SET NULL ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `dall_products`
--

LOCK TABLES `dall_products` WRITE;
/*!40000 ALTER TABLE `dall_products` DISABLE KEYS */;
INSERT INTO `dall_products` VALUES (3,3,'Product 1',NULL,'mts',1.00,750.00,'USD','tax_10',10.00,5.00,1,NULL,'2019-01-17 15:18:09',1,'2020-10-09 10:51:13',1,0),(4,3,'Product 2',NULL,'doses',1.00,12.00,'USD','tax_10',10.00,5.00,0,NULL,'2019-01-17 15:18:45',1,'2020-04-26 12:40:48',1,0);
/*!40000 ALTER TABLE `dall_products` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `dall_purchases`
--

DROP TABLE IF EXISTS `dall_purchases`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `dall_purchases` (
  `purchase_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `purchase_date` date DEFAULT NULL,
  `delivery_date` date DEFAULT NULL,
  `expiration_date` date DEFAULT NULL,
  `bill_number` varchar(100) DEFAULT NULL,
  `provider` varchar(255) DEFAULT NULL,
  `observations` text,
  `total_amount` decimal(15,2) unsigned DEFAULT NULL,
  `meta_vars` text,
  `created` datetime DEFAULT NULL,
  `creator` int(11) unsigned DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  `modifier` int(11) unsigned DEFAULT NULL,
  `trash` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `stocked` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`purchase_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `dall_purchases`
--

LOCK TABLES `dall_purchases` WRITE;
/*!40000 ALTER TABLE `dall_purchases` DISABLE KEYS */;
/*!40000 ALTER TABLE `dall_purchases` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `dall_purchases_products`
--

DROP TABLE IF EXISTS `dall_purchases_products`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `dall_purchases_products` (
  `purchase_meta_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `purchase_id` int(11) unsigned DEFAULT NULL,
  `product_id` int(11) unsigned DEFAULT NULL,
  `description` text,
  `quantity` decimal(15,2) unsigned DEFAULT NULL,
  `unit_price` decimal(15,2) unsigned DEFAULT NULL,
  `row_total` decimal(15,2) unsigned DEFAULT NULL,
  `stocked` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`purchase_meta_id`),
  KEY `delete_product_on_order` (`product_id`),
  KEY `delete_order_on_order` (`purchase_id`),
  CONSTRAINT `delete_purchase_on_product` FOREIGN KEY (`product_id`) REFERENCES `dall_products` (`product_id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  CONSTRAINT `delete_purchase_on_purchase` FOREIGN KEY (`purchase_id`) REFERENCES `dall_purchases` (`purchase_id`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `dall_purchases_products`
--

LOCK TABLES `dall_purchases_products` WRITE;
/*!40000 ALTER TABLE `dall_purchases_products` DISABLE KEYS */;
/*!40000 ALTER TABLE `dall_purchases_products` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `dall_sales`
--

DROP TABLE IF EXISTS `dall_sales`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `dall_sales` (
  `sale_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `order_id` int(11) unsigned DEFAULT NULL,
  `sale_date` date DEFAULT NULL,
  `client_id` int(11) unsigned DEFAULT NULL,
  `seller_id` int(11) unsigned DEFAULT NULL,
  `pos_id` int(11) unsigned DEFAULT NULL,
  `print_order` int(11) unsigned DEFAULT NULL,
  `expiration_date` date DEFAULT NULL,
  `bill_number` varchar(100) DEFAULT NULL,
  `type` varchar(20) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `ruc` varchar(50) DEFAULT NULL,
  `address` varchar(255) DEFAULT NULL,
  `telephone` varchar(50) DEFAULT NULL,
  `total_amount` decimal(15,2) unsigned DEFAULT NULL,
  `taxes` text,
  `meta_vars` text,
  `created` datetime DEFAULT NULL,
  `creator` int(11) unsigned DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  `modifier` int(11) unsigned DEFAULT NULL,
  `trash` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `billed` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`sale_id`),
  UNIQUE KEY `bill_number` (`bill_number`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `dall_sales`
--

LOCK TABLES `dall_sales` WRITE;
/*!40000 ALTER TABLE `dall_sales` DISABLE KEYS */;
INSERT INTO `dall_sales` VALUES (1,NULL,'2020-04-22',NULL,1,1,23847330,NULL,'001-003-0000100','full','Casual Client','44444444-7',NULL,NULL,1500000.00,'{\"totals\":{\"tax_no\":\"0\",\"tax_5\":\"0\",\"tax_10\":\"1500000\"},\"liquidation\":{\"tax_no\":\"0\",\"tax_5\":\"0\",\"tax_10\":\"136364\",\"total\":\"136364\"}}','{\"amount_given\":\"1500000\",\"change\":\"\",\"payment_method\":\"cash\"}','2020-04-22 18:48:10',1,'2020-04-24 18:01:54',1,0,1);
/*!40000 ALTER TABLE `dall_sales` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `dall_sales_stock`
--

DROP TABLE IF EXISTS `dall_sales_stock`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `dall_sales_stock` (
  `sale_id` int(11) unsigned DEFAULT NULL,
  `stock_id` bigint(32) unsigned DEFAULT NULL,
  `sku` varchar(100) DEFAULT NULL,
  `product_id` int(11) unsigned DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `quantity` decimal(15,2) unsigned DEFAULT NULL,
  `unit_price` decimal(15,2) unsigned DEFAULT NULL,
  `row_total` decimal(15,2) unsigned DEFAULT NULL,
  `tax` varchar(50) DEFAULT NULL,
  KEY `delete_sale_on_sale` (`sale_id`),
  KEY `delete_product_on_sale` (`product_id`),
  KEY `delete_stock_on_sale` (`stock_id`),
  CONSTRAINT `delete_product_on_sale` FOREIGN KEY (`product_id`) REFERENCES `dall_products` (`product_id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  CONSTRAINT `delete_sale_on_sale` FOREIGN KEY (`sale_id`) REFERENCES `dall_sales` (`sale_id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  CONSTRAINT `delete_stock_on_sale` FOREIGN KEY (`stock_id`) REFERENCES `dall_stock` (`stock_id`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `dall_sales_stock`
--

LOCK TABLES `dall_sales_stock` WRITE;
/*!40000 ALTER TABLE `dall_sales_stock` DISABLE KEYS */;
INSERT INTO `dall_sales_stock` VALUES (1,NULL,NULL,3,'Product 1',100.00,15000.00,1500000.00,'tax_10');
/*!40000 ALTER TABLE `dall_sales_stock` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `dall_stock`
--

DROP TABLE IF EXISTS `dall_stock`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `dall_stock` (
  `stock_id` bigint(32) unsigned NOT NULL AUTO_INCREMENT,
  `product_id` int(11) unsigned DEFAULT NULL,
  `purchase_id` int(11) unsigned DEFAULT NULL,
  `code` varchar(100) DEFAULT NULL,
  `sku` varchar(100) DEFAULT NULL,
  `quantity` decimal(15,2) unsigned DEFAULT NULL,
  `init_quantity` decimal(15,2) unsigned DEFAULT NULL,
  `purchase_quantity` decimal(15,2) unsigned DEFAULT NULL,
  `purchase_price` decimal(15,2) unsigned DEFAULT NULL,
  `unit_price` decimal(15,2) unsigned DEFAULT NULL,
  `expiration_date` date DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `creator` int(11) unsigned DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  `modifier` int(11) unsigned DEFAULT NULL,
  `trash` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`stock_id`),
  KEY `delete_product_on_stock` (`product_id`),
  KEY `delete_purchase_on_stock` (`purchase_id`),
  CONSTRAINT `delete_product_on_stock` FOREIGN KEY (`product_id`) REFERENCES `dall_products` (`product_id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  CONSTRAINT `delete_purchase_on_stock` FOREIGN KEY (`purchase_id`) REFERENCES `dall_purchases` (`purchase_id`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `dall_stock`
--

LOCK TABLES `dall_stock` WRITE;
/*!40000 ALTER TABLE `dall_stock` DISABLE KEYS */;
INSERT INTO `dall_stock` VALUES (1,3,NULL,NULL,'900',30.00,100.00,100.00,NULL,1500.00,NULL,'2020-01-17 18:13:30',1,'2020-04-03 21:54:34',1,0);
/*!40000 ALTER TABLE `dall_stock` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2020-10-11 12:38:13
