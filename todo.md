# TO DO

### PRODUCTS

* Play with the arrangements of the pictures.

### SALES

* Make a config item that allows you to restore stock quantity on sale deletion.

### REPORTS

* The whole thing.

### SEARCH ENGINE

* The whole thing.

### CREDIT NOTE

* The whole thing, considering almost the same features as the sales module.

### REMISSION NOTE

* The whole thing, considering almost the same features as the sales module.