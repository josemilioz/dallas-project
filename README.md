## Overview

Project **DALLAS** will be a Point-of-sale environment completed with business tools, adjusted for any kind of businness, with the ability of printing receipts and bills.

It will also work as a framework for the future apps developments from the same nature within the possible uses we can take from the tool.

The very same project contemplates the fact of creating a very complete API, in order to let **DALLAS** connect with several projects in hand that will be developed in the future, like Shopping Carts, CMS tools, etc.

## Technology to be used

* [HTML 5](http://www.w3schools.com/html5/)
* [CSS 3](http://www.w3schools.com/css3/)
* [Sass](http://sass-lang.com/)
* [CodeIgniter](http://codeigniter.com/)
* [Boilerplate](http://html5boilerplate.com/)
* [Twitter Bootstrap](http://getbootstrap.com/)

## General Configuration

*	**Database Name:**		dallasDB
*	**Database Username:**	root
*	**Database Password:**	root
*	**Database Host:**		localhost
*	**Main MAMP url:**		http://dallas.local/

## User's Config

Install the dallasDB.sql into your db server.

Use the following user to login:

*	**Username:**		admin
*	**Password:**		admin

----------------------------

## 'Production' Server Database

db names must have dallas_ prefix

user: metamor2_dallas
pass: @KilombitoPrecioso997#